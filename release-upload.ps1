# This scripts expects a file with a gitlab api token in the following location.
# At token can be retrieved here: https://gitlab.com/-/profile/personal_access_tokens
$tokenFile = "$HOME\.gitlab-release-token"
if (! (Test-Path -Path $tokenFile -PathType Leaf)) {
    throw "Gitlab token file does not exist (expected at " + $tokenFile + ")"
}
$token = Get-Content -Path $tokenFile -TotalCount 1

if ($versionName)
{}
else
{
  $versionName = Read-Host -Prompt 'Enter new 3-number version name, separated with dots (e.g. "0.0.1")'
}

$version = $versionName
$tag = "v$version"

# This is the project id of the kieselstein-terminal-maui project (It is visible just below the project title on the gitlab repo page)
$projectId = "45170536"
$releaseUri = "https://gitlab.com/api/v4/projects/$projectId/releases"
$packageRegistryUrl = "https://gitlab.com/api/v4/projects/$projectId/packages/generic/kieselstein-terminal/$version"

#$headers = @{
#    'Content-Type'  = 'application/json'
#    'PRIVATE-TOKEN' = $token 
#}

#$ListReleases = Invoke-WebRequest -Uri $releaseUri -Headers $headers -Method Get 
#Write-Output "The following Releases are already available:"
#Write-Output $ListReleases

$gitRef = git rev-parse HEAD
Write-Output "gitRef=$gitRef"
Write-Output "version=$versionName"
Write-Output "tag=$tag"

$winZipName = "kieselsteinerp-terminal-win-$version.zip"
$apkName = "org.kieselsteinerp.terminal-Signed-$version.apk"

# Creating Archives
$tmpDir = "release-tmp"
mkdir $tmpDir
Compress-Archive -Path ".\Terminal\bin\Release\net8.0-windows10.0.19041.0\win10-x64\AppPackages" -Force -DestinationPath $tmpDir/$winZipName
Copy-Item ".\Terminal\bin\Release\net8.0-android\publish\org.kieselsteinerp.terminal-Signed.apk" -Destination $tmpDir/$apkName

# Upload Files
curl.exe --header "PRIVATE-TOKEN: $token" --upload-file $tmpDir/$winZipName "$packageRegistryUrl/$winZipName"
curl.exe --header "PRIVATE-TOKEN: $token" --upload-file $tmpDir/$apkName "$packageRegistryUrl/$apkName"
Remove-Item $tmpDir -Recurse -Force

$JSON = @"
{
  "name": "Release $version",
  "tag_name": "$tag",
  "ref": "$gitRef",
  "assets":{
      "links":[
         {
            "name":"windows package",
            "url":"$packageRegistryUrl/$winZipName"
         },
         {
            "name":"android packag",
            "url":"$packageRegistryUrl/$apkName"
         }
      ]
   }
}
"@

Write-Output "Data for release request:"
Write-Output $JSON


#$releaseRequestData = "{`"name`":`"Release $version`",`"tag_name`":`"$tag`",`"ref`":`"$gitRef`",`"assets`": { `"links`": [{ `"name`":`"windows package`",`"url`":`"$packageRegistryUrl/$winZipName`"}, { `"name`":`"android package`",`"url`":`"$packageRegistryUrl/$apkName`"}] } }"
#Write-Output "Data for release request:"
#Write-Output $releaseRequestData

# releasing (see https://docs.gitlab.com/ee/api/releases/index.html#create-a-release)
# tipps from https://stackoverflow.com/questions/56327501/create-release-on-gitlab-with-powershell
Invoke-RestMethod -Method Post -Headers @{ 'PRIVATE-TOKEN'="$token" } -Body $JSON -ContentType "application/json" -Uri $releaseUri
Read-Host -Prompt "Press Enter to continue"

#curl.exe -g --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $token" `
#     --request POST "$releaseUri" `
#     --data $releaseRequestData

# making shure, we also get the new tag
git pull