﻿using FluentAssertions;
using KieselsteinErp.Terminal.Services.UiState;


namespace TerminalTests.UiState
{
    public class LatchBackedUiReadyServiceTest
    {
        private readonly LatchBackedUiReadyService _service;

        public LatchBackedUiReadyServiceTest()
        {
            _service = new LatchBackedUiReadyService();
        }


        [Fact]
        public void Initial_state_is_not_ready()
        {
            _service.IsReady().Should().BeFalse();
        }

        [Fact]
        public void After_set_ready_gui_is_indicated_as_ready()
        {
            _service.TriggerGuiReady();
            _service.IsReady().Should().BeTrue();
        }

        [Fact]
        public void Multiple_Triggers_do_not_harm()
        {
            _service.TriggerGuiReady();
            _service.TriggerGuiReady();
            _service.IsReady().Should().BeTrue();
        }
    }
}
