﻿# Automated Testing of the Kieselstein Terminal

For the moment, only tests for the c# components are considered. No investigations yet done on how to run on different platforms nor on the build pipeline. To be seen.

## Choices

No long evaluation done, but just a quick investigation. The following are the actual choices. In case there are better options to be discovered, we should be able to adopt.

### unit testing
There seem to be 3 popular testing frameworks for c#: NUnit, MSTest and xUnit. A rough comparison can be found here: https://www.lambdatest.com/blog/nunit-vs-xunit-vs-mstest/. A biased one here: https://xunit.net/docs/comparisons
At least this article claims that xUnit is leaner than the others and leads to cleaner tests. Furthermore, the Exception assertion syntax seems to be more fine-grained. 
Therefore, the choice for [xUnit](https://xunit.net/) at the moment. 

Setup was perfomed according this tutorial: https://www.youtube.com/watch?v=C9vIDLQwc7M

### Mocking
Again there are 3 different popular frameworks for c#: Moq, NSubstitute, FakeItEasy. Quick read here (which compares the syntax): https://www.danclarke.com/comparing-dotnet-mocking-libraries.
NSubstitue seems to be the most concise and also looks really similar to mockito -> choosing [NSubstitue](https://nsubstitute.github.io/) for the moment.

### Fluent assertions
As the assertions from xUnit are not really that nice ... searched for some fluent assertion library (like assertj in java). Found this one: https://fluentassertions.com



