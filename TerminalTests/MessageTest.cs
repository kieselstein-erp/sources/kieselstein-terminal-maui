﻿using FluentAssertions;
using KieselsteinErp.Terminal.Messages;

namespace TerminalTests
{
  public class MessageTest
  {
    [Fact]
    public void Message_2_instances_equal()
    {
      // Act
      Message m1 = new Message("Text",MessageType.Info);
      Message m2 = new Message("Text", MessageType.Info);

      // Assert
      m1.Should().BeEquivalentTo(m2); 
    }

  }
}
