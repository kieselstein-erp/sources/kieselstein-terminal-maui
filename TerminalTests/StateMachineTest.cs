﻿using Castle.Core.Logging;
using FluentAssertions;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Resources.Localization;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Database;
using KieselsteinErp.Terminal.Services.Item;
using KieselsteinErp.Terminal.Services.Machine;
using KieselsteinErp.Terminal.Services.Order;
using KieselsteinErp.Terminal.Services.Production;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.Staff;
using KieselsteinErp.Terminal.Services.StateMachine;
using KieselsteinErp.Terminal.Services.User;
using KieselsteinErp.Terminal.Services.Worktime;
using NSubstitute;
using NSubstitute.ReceivedExtensions;
using Open.Observable;
using System.Reactive.Linq;

namespace TerminalTests
{
  public class StateMachineTest
  {
    private readonly Microsoft.Extensions.Logging.ILogger<StateMachine> _logger;
    private readonly IStaffService _staffService;
    private readonly IWorktimeService _worktimeService;
    private readonly IMessageService _messageService;
    private readonly IProductionRestService _productionService;
    private readonly IMachineService _machineService;
    private readonly IOrderService _orderService;
    private readonly IItemV1Service _itemV1Service;
    private readonly IUserService _userService;
    private readonly IDatabaseService _databaseService;
    private readonly ISettingsService _settingsService;
    private readonly StateMachine _stateMachine;

    public StateMachineTest()
    {
      _logger = Substitute.For<Microsoft.Extensions.Logging.ILogger<StateMachine>>();
      _staffService = Substitute.For<IStaffService>();
      _worktimeService = Substitute.For<IWorktimeService>();
      _messageService = Substitute.For<IMessageService>();
      _productionService = Substitute.For<IProductionRestService>();
      _machineService = Substitute.For<IMachineService>();
      _userService = Substitute.For<IUserService>();
      _orderService = Substitute.For<IOrderService>();
      _itemV1Service = Substitute.For<IItemV1Service>();
      _settingsService = Substitute.For<ISettingsService>();
      _stateMachine = new StateMachine(_logger, _worktimeService, _staffService, _messageService, _productionService, _machineService, _userService, _databaseService, _orderService, _itemV1Service, _settingsService);
    }
    // ---------------------------------- //
    // Doesnt matter if online or offline //
    // ---------------------------------- //
    [Fact]
    public async void StateMachineThrowsExceptionIfCodeEventNull()
    {
      // Arrange
      var exceptionText = "CodeEvent must not be null";
      //_stateMachine.OnCodeReceived(Arg.Any<CodeEvent>()).Throws(new Exception(exceptionText));

      // Act 
      Func<Task> action = async () => await _stateMachine.OnCodeReceived(null);

      // Assert
      await action.Should().ThrowAsync<Exception>().WithMessage(exceptionText);

    }

    [Fact]
    public async void StateMachineChangeStateToIdentified()
    {
      // Arrange
      var observable = _stateMachine.State();
      var staff = new StaffEntry() { firstName = "Hans", name = "Test", id = 1, identityCnr = "123" };
      _staffService.FindStaffByIdentyCnr("123").Returns(staff);
      var expected = new ObservableValue<KieselsteinErp.Terminal.Services.StateMachine.State>();
      expected.Init(new KieselsteinErp.Terminal.Services.StateMachine.State(StateType.Identified, "123", staff));

      // Act
      Func<Task> action = async () => await _stateMachine.OnCodeReceived(new UserAuthEvent("123"));
      await action();

      // Assert
      observable.Should().NotBeNull().And.BeEquivalentTo(expected);
      //observable.Should().BeEquivalentTo(expected);
    }

    [Fact]
    public async void StateMachineBookWorktimeButNotIdentified()
    {
      // Arrange
      var staff = new StaffEntry() { firstName = "Hans", name = "Test", id = 1, identityCnr = "123" };
      _staffService.FindStaffByIdentyCnr("123").Returns(staff);
      var expected = AppResources.Prompt_Login;

      // Act
      Func<Task> action = async () => await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Coming));
      await action();

      // Assert
      _worktimeService.DidNotReceive().BookWorktime(new WorkTimeEvent(WorkTimeEventType.Coming, false), "123", Arg.Any<DateTime>());
      _messageService.Received().Info(expected);
    }

    // -------------------- //
    // Testing while online //
    // -------------------- //
    [Fact]
    public async void StateMachineBookWorktimeWhenIdentified()
    {
      // Arrange
      // example for raising an OnLogonChanged event
      _userService.OnLogonChanged += Raise.Event<EventHandler<bool>>(new object[] { _userService, true });
      var staff = new StaffEntry() { firstName = "Hans", name = "Test", id = 1, identityCnr = "123" };
      _staffService.FindStaffByIdentyCnr("123").Returns(staff);
      Func<Task> init = async () => await _stateMachine.OnCodeReceived(new UserAuthEvent("123"));
      await init();

      // Act
      Func<Task> action = async () => await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Coming));
      await action();

      // Assert
      _worktimeService.Received().BookWorktime(new WorkTimeEvent(WorkTimeEventType.Coming, false), "123", Arg.Any<DateTime>());
    }

    [Fact]
    public async void BookLotActivity()
    {
      // Arrange
      _userService.OnLogonChanged += Raise.Event<EventHandler<bool>>(new object[] { _userService, true });
      const string LOT = "23/0000001";
      const string ACTIVITY = "SAEGEN";
      const string DONELOT = "23/0000002";
      const string ACTIVITY2 = "FRAESEN";

      var staff = new StaffEntry() { firstName = "Hans", name = "Test", id = 1, identityCnr = "123" };
      _staffService.FindStaffByIdentyCnr("123").Returns(staff);
      _productionService.LotValid(LOT).Returns(LotState.PARTIALLYDONE);
      _productionService.LotValid(DONELOT).Returns(LotState.DONE);
      Func<Task> init = async () => await _stateMachine.OnCodeReceived(new UserAuthEvent("123"));
      await init();

      // Act
      Func<Task> scanLot = async () => await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Lot, false, LOT));
      Func<Task> scanActivity = async () => await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Activity, false, ACTIVITY));
      Func<Task> scanDoneLot = async () => await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Lot, false, DONELOT));
      Func<Task> scanActivity2 = async () => await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Activity, false, ACTIVITY2));
      await scanActivity();
      await scanLot();
      await scanActivity2();
      await scanDoneLot();

      // Assert
      _worktimeService.Received().BookWorktime(string.Format(AppResources.BookLotActivity, LOT, ACTIVITY), "$L" + LOT + ACTIVITY, 0, staff.identityCnr, Arg.Any<DateTime>());
      _worktimeService.DidNotReceive().BookWorktime(string.Format(AppResources.BookLotActivity, LOT, ACTIVITY2), "$L" + LOT + ACTIVITY2, 0, staff.identityCnr, Arg.Any<DateTime>());
      _worktimeService.DidNotReceive().BookWorktime(string.Format(AppResources.BookLotActivity, DONELOT, ACTIVITY2), "$L" + DONELOT + ACTIVITY2, 0, staff.identityCnr, Arg.Any<DateTime>());
      _messageService.Received().Error(string.Format(AppResources.Error_Lot_Already_Done, DONELOT));
    }

    [Fact]
    public async void BookLotWorkstepNoMachineOverwrite()
    {
      // Arrange
      _userService.OnLogonChanged += Raise.Event<EventHandler<bool>>(new object[] { _userService, true });
      const string WORKSTEP = "$V23/0000001A110";
      const string WORKSTEP2 = "$V23/0000001B110";
      const string MACHINENUMBER = "C1";
      const int MACHINEID = 13;
      const string LOT = "23/0000001";

      var staff = new StaffEntry() { firstName = "Hans", name = "Test", id = 1, identityCnr = "123" };
      _staffService.FindStaffByIdentyCnr("123").Returns(staff);
      _productionService.LotValid(LOT).Returns(LotState.EDITED);
      _machineService.GetMachineIdByNumber("C1").Returns(MACHINEID);
      Func<Task> init = async () => await _stateMachine.OnCodeReceived(new UserAuthEvent("123"));
      await init();

      // Act
      Func<Task> machineSelect = async () => await _stateMachine.OnCodeReceived(new MachineEvent(MachineEventType.MachineSelect, MACHINENUMBER));
      Func<Task> scanLot = async () => await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Lot, false, LOT));
      Func<Task> scanLotWorkstep = async () => await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.LotWorkstep, false, WORKSTEP));
      Func<Task> scanLotWorkstep2 = async () => await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.LotWorkstep, false, WORKSTEP2));
      await scanLotWorkstep();
      await machineSelect();
      await scanLot();
      await scanLotWorkstep2();

      // Assert
      _messageService.DidNotReceive().Error(string.Format(AppResources.Error_Machine_Not_Found, MACHINENUMBER));
      _messageService.DidNotReceive().Info(AppResources.Machine_Override_Ignored);
      _worktimeService.Received().BookWorktime(string.Format(AppResources.BookLotWorkstep, WORKSTEP.Replace("$V", "")), WORKSTEP, 0, staff.identityCnr, Arg.Any<DateTime>());
      _worktimeService.Received().BookWorktime(string.Format(AppResources.BookLotWorkstep, WORKSTEP2.Replace("$V", "")), WORKSTEP2, 0, staff.identityCnr, Arg.Any<DateTime>());
    }

    [Fact]
    public async void BookLotWorkstepMachineOverwrite()
    {
      // Arrange
      _userService.OnLogonChanged += Raise.Event<EventHandler<bool>>(new object[] { _userService, true });
      const string WORKSTEP = "$V23/000000A1110";
      const string WORKSTEPOVERWRITE = "$V23/0000001??10";
      const string MACHINENUMBER = "C1";
      const int MACHINEID = 13;

      var staff = new StaffEntry() { firstName = "Hans", name = "Test", id = 1, identityCnr = "123" };
      _staffService.FindStaffByIdentyCnr("123").Returns(staff);
      _machineService.GetMachineIdByNumber("C1").Returns(MACHINEID);
      Func<Task> init = async () => await _stateMachine.OnCodeReceived(new UserAuthEvent("123"));
      await init();

      // Act
      Func<Task> machineSelect = async () => await _stateMachine.OnCodeReceived(new MachineEvent(MachineEventType.MachineSelect, MACHINENUMBER));
      Func<Task> scanLotWorkstep = async () => await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.LotWorkstep, false, WORKSTEP));
      Func<Task> scanLotWorkstepOverwrite = async () => await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.LotWorkstep, false, WORKSTEPOVERWRITE));

      await machineSelect();
      await scanLotWorkstep();
      await machineSelect();
      await scanLotWorkstepOverwrite();

      // Assert
      _messageService.DidNotReceive().Error(string.Format(AppResources.Error_Machine_Not_Found, MACHINENUMBER));
      _messageService.Received().Info(AppResources.Machine_Override_Ignored);
      _worktimeService.Received().BookWorktime(string.Format(AppResources.BookLotWorkstep, WORKSTEP.Replace("$V", "")), WORKSTEP, 0, staff.identityCnr, Arg.Any<DateTime>());
      _worktimeService.Received().BookWorktime(string.Format(AppResources.BookLotWorkstepMachine, WORKSTEPOVERWRITE.Replace("$V", ""), MACHINENUMBER), WORKSTEPOVERWRITE, MACHINEID, staff.identityCnr, Arg.Any<DateTime>());
    }

    [Fact]
    public async void BookLotWorkstepMachineNotFound()
    {
      // Arrange
      _userService.OnLogonChanged += Raise.Event<EventHandler<bool>>(new object[] { _userService, true });
      const string WORKSTEP = "$V23/000000A1110";
      const string MACHINENUMBER = "C1";
      const int MACHINEIDNOTFOUND = 0;

      var staff = new StaffEntry() { firstName = "Hans", name = "Test", id = 1, identityCnr = "123" };
      _staffService.FindStaffByIdentyCnr("123").Returns(staff);
      _machineService.GetMachineIdByNumber("C1").Returns(MACHINEIDNOTFOUND);
      Func<Task> init = async () => await _stateMachine.OnCodeReceived(new UserAuthEvent("123"));
      await init();

      // Act
      Func<Task> machineSelect = async () => await _stateMachine.OnCodeReceived(new MachineEvent(MachineEventType.MachineSelect, MACHINENUMBER));
      Func<Task> scanLotWorkstep = async () => await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.LotWorkstep, false, WORKSTEP));
      await machineSelect();
      await scanLotWorkstep();

      // Assert
      _messageService.Received().Error(string.Format(AppResources.Error_Machine_Not_Found, MACHINENUMBER));
      _messageService.DidNotReceive().Info(AppResources.Machine_Override_Ignored);
      _worktimeService.Received().BookWorktime(string.Format(AppResources.BookLotWorkstep, WORKSTEP.Replace("$V", "")), WORKSTEP, 0, staff.identityCnr, Arg.Any<DateTime>());
    }

    [Fact]
    public async void BookLotDeliverWithoutLot()
    {
      // Arrange
      _userService.OnLogonChanged += Raise.Event<EventHandler<bool>>(new object[] { _userService, true });
      var staff = new StaffEntry() { firstName = "Hans", name = "Test", id = 1, identityCnr = "123" };
      _staffService.FindStaffByIdentyCnr("123").Returns(staff);
      Func<Task> init = async () => await _stateMachine.OnCodeReceived(new UserAuthEvent("123"));
      await init();

      // Act
      Func<Task> machineSelect = async () => await _stateMachine.OnCodeReceived(new ProductionEvent(ProductionEventType.Deliver));
      await machineSelect();

      // Assert
      _messageService.Received().Error(AppResources.Error_Scan_Lot_First);
    }

    [Fact]
    public async void BookLotDeliver()
    {
      // Arrange
      _userService.OnLogonChanged += Raise.Event<EventHandler<bool>>(new object[] { _userService, true });
      const string LOT = "23/0000001";
      const decimal AMOUNT = (decimal)2.5;
      const decimal DEFECTAMOUNT = 0;

      var staff = new StaffEntry() { firstName = "Hans", name = "Test", id = 1, identityCnr = "123" };
      _staffService.FindStaffByIdentyCnr("123").Returns(staff);
      _productionService.LotValid(LOT).Returns(LotState.INPRODUCTION);
      _productionService.BookDeliver(AMOUNT, DEFECTAMOUNT, LOT, false, string.Empty).Returns(new BookingResult(true, "message"));
      Func<Task> init = async () => await _stateMachine.OnCodeReceived(new UserAuthEvent("123"));
      await init();

      // Act
      Func<Task> scanLot = async () => await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Lot, false, LOT));
      Func<Task> scanDeliver = async () => await _stateMachine.OnCodeReceived(new ProductionEvent(ProductionEventType.Deliver));
      Func<Task> deliverAmount = async () => await _stateMachine.OnCodeReceived(new ProductionEvent(ProductionEventType.DeliverAmount, false, AMOUNT, DEFECTAMOUNT, LOT, false, string.Empty));
      await scanLot();
      await scanDeliver();
      await deliverAmount();

      // Assert
      _messageService.DidNotReceive().Error(Arg.Any<string>());
      _productionService.Received().BookDeliver(AMOUNT, DEFECTAMOUNT, LOT, false, string.Empty);
    }

    [Fact]
    public async void BookLotDeliverFailed()
    {
      // Arrange
      _userService.OnLogonChanged += Raise.Event<EventHandler<bool>>(new object[] { _userService, true });
      const string LOT = "23/0000001";
      const decimal AMOUNT = (decimal)999999.9;
      const decimal DEFECTAMOUNT = 0;

      var staff = new StaffEntry() { firstName = "Hans", name = "Test", id = 1, identityCnr = "123" };
      _staffService.FindStaffByIdentyCnr("123").Returns(staff);
      _productionService.LotValid(LOT).Returns(LotState.INPRODUCTION);
      _productionService.BookDeliver(AMOUNT, DEFECTAMOUNT, LOT, false, string.Empty).Returns(new BookingResult(false, "error"));
      Func<Task> init = async () => await _stateMachine.OnCodeReceived(new UserAuthEvent("123"));
      await init();

      // Act
      Func<Task> scanLot = async () => await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Lot, false, LOT));
      Func<Task> scanDeliver = async () => await _stateMachine.OnCodeReceived(new ProductionEvent(ProductionEventType.Deliver));
      Func<Task> deliverAmount = async () => await _stateMachine.OnCodeReceived(new ProductionEvent(ProductionEventType.DeliverAmount, false, AMOUNT, DEFECTAMOUNT, LOT, false, string.Empty));
      await scanLot();
      await scanDeliver();
      await deliverAmount();

      // Assert
      _messageService.DidNotReceive().Error(AppResources.Error_Scan_Lot_First);
      _productionService.Received().BookDeliver(AMOUNT, DEFECTAMOUNT, LOT, false, string.Empty);
      _messageService.Received().Error("error");
    }

    [Fact]
    public async void BookLotDeliverCancled()
    {
      // Arrange
      _userService.OnLogonChanged += Raise.Event<EventHandler<bool>>(new object[] { _userService, true });
      const string LOT = "23/0000001";
      const string ACTIVITY = "SAEGEN";
      const decimal AMOUNT = (decimal)2.5;
      const decimal DEFECTAMOUNT = 0;

      var staff = new StaffEntry() { firstName = "Hans", name = "Test", id = 1, identityCnr = "123" };
      _staffService.FindStaffByIdentyCnr("123").Returns(staff);
      _productionService.LotValid(LOT).Returns(LotState.INPRODUCTION);
      _productionService.BookDeliver(AMOUNT, DEFECTAMOUNT, LOT, false, string.Empty).Returns(new BookingResult(true, "message"));
      Func<Task> init = async () => await _stateMachine.OnCodeReceived(new UserAuthEvent("123"));
      await init();

      // Act
      Func<Task> scanLot = async () => await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Lot, false, LOT));
      Func<Task> scanDeliver = async () => await _stateMachine.OnCodeReceived(new ProductionEvent(ProductionEventType.Deliver));
      Func<Task> scanActivity = async () => await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Activity, false, ACTIVITY));
      await scanLot();
      await scanDeliver();
      await scanActivity();

      // Assert
      _messageService.DidNotReceive().Error(Arg.Any<string>());
      _productionService.DidNotReceive().BookDeliver(AMOUNT, DEFECTAMOUNT, LOT, false, string.Empty);
      _worktimeService.DidNotReceive().BookWorktime(string.Format(AppResources.BookLotActivity, LOT, ACTIVITY), "$L" + LOT + ACTIVITY, 0, staff.identityCnr, Arg.Any<DateTime>());
    }
  }
}
