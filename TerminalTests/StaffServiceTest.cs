using FluentAssertions;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services.Database;
using KieselsteinErp.Terminal.Services.Staff;
using KieselsteinErp.Terminal.Services.User;
using NSubstitute;
using NSubstitute.ExceptionExtensions;

namespace TerminalTests
{
	public class StaffServiceTest
	{
		private readonly IStaffRestService _staffProvider;
		private readonly IUserService _userService;
		private readonly IDatabaseService _databaseService;
		private readonly StaffService _staffService;

		public StaffServiceTest()
		{
			_staffProvider = Substitute.For<IStaffRestService>();
			_userService = Substitute.For<IUserService>();
			_databaseService = Substitute.For<IDatabaseService>();
			_staffService = new StaffService(_staffProvider, _userService, _databaseService);
		}

		//TODO: Test for online and offline. Currently only offline
		[Fact]
		public void StaffService_Returns_Null_If_No_Staff_Provided()
		{
			// Arrange
			_databaseService.GetStaffOffline().Returns(new List<StaffEntry>());

			// Act
			var staff = _staffService.FindStaffByIdentyCnr("");

			// Assert
			staff.Should().BeNull();
		}

		[Fact]
		public void StaffService_Does_Not_Handle_StaffProvider_Exception()
		{
			// Arrange
			var exceptionText = "No Staff Provided.";
			_databaseService.GetStaffOffline().Throws(new Exception(exceptionText));

			// Act 
			Action action = () => _staffService.FindStaffByIdentyCnr("");

			// Assert (not fully true ... the act also happens here ;-)
			action.Should().Throw<Exception>().WithMessage(exceptionText);
		}

	}
}