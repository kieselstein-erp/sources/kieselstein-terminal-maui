using BarcodeSimulator.Properties;
using Microsoft.VisualBasic;
using System;
using System.Diagnostics;
using System.IO.Ports;
using System.Security.Policy;
using System.Text.Json;

namespace BarcodeSimulator
{
  public partial class frmSimulator : Form
  {
    private SerialPort _port;
    private bool _changed;

    public frmSimulator()
    {
      InitializeComponent();
    }

    private void frmSimulator_Load(object sender, EventArgs e)
    {
      cboPorts.Items.Clear();
      string[] ports = SerialPort.GetPortNames();
      foreach (string p in ports)
      {
        cboPorts.Items.Add(p);
      }
      if (!string.IsNullOrEmpty(Settings.Default.Port))
        cboPorts.SelectedItem = Settings.Default.Port;
      LoadCodes(lstCodes, Settings.Default.Codes);
      _changed = false;
    }

    private void LoadCodes(ListBox lstCodes, string codes)
    {
      if (!string.IsNullOrEmpty(codes))
      {
        string[] s = JsonSerializer.Deserialize<string[]>(codes);
        lstCodes.Items.AddRange(s);
      }
    }

    private void btnAdd_Click(object sender, EventArgs e)
    {
      lstCodes.Items.Add(txtCode.Text);
      _changed = true;
    }

    private void btnRemove_Click(object sender, EventArgs e)
    {
      string[] toDelete = new string[lstCodes.SelectedItems.Count];
      lstCodes.SelectedItems.CopyTo(toDelete, 0);
      foreach (string s in toDelete)
        lstCodes.Items.Remove(s);
      _changed = true;
    }

    private void btnSend_Click(object sender, EventArgs e)
    {
      using (_port = new SerialPort(cboPorts.Text, 9600, Parity.None, 8, StopBits.One))
      {
        _port.WriteTimeout = 1000;
        _port.Open();
        for (int i = lstCodes.SelectedItems.Count; i > 0; i--)
        {
          string s = lstCodes.SelectedItems[i-1].ToString();
          if (!string.IsNullOrEmpty(s))
          {
            _port.Write(s + "\r");
            Stopwatch sw = Stopwatch.StartNew();
            while (_port.BytesToWrite > 0)
            {
              Application.DoEvents();
              if (sw.ElapsedMilliseconds > _port.WriteTimeout)
              {
                MessageBox.Show($"{_port.PortName} Write Timeout");
                break;
              }
            }
            Thread.Sleep(1000);
          }
        }
        _port.Close();
      }
    }

    private void cboPorts_SelectedIndexChanged(object sender, EventArgs e)
    {
      _changed = true;
    }

    private void frmSimulator_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (_changed)
      {
        if (MessageBox.Show("Aktuelle Werte speichern?", "Barcode Simulator", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
        {
          Settings.Default.Codes = CodesToJson(lstCodes);
          Settings.Default.Port = cboPorts.Text;
          Settings.Default.Save();
        }
      }
    }

    private string CodesToJson(ListBox lstCodes)
    {
      string[] codes = new string[lstCodes.Items.Count];
      lstCodes.Items.CopyTo(codes, 0);
      return JsonSerializer.Serialize(codes);
    }

    private void txtCode_TextChanged(object sender, EventArgs e)
    {
      btnAdd.Enabled = (!string.IsNullOrEmpty(txtCode.Text));
    }

    private void lstCodes_SelectedIndexChanged(object sender, EventArgs e)
    {
      btnRemove.Enabled = lstCodes.SelectedIndices.Count > 0;
      btnSend.Enabled = lstCodes.SelectedIndices.Count > 0;
    }

    private void lstCodes_DoubleClick(object sender, EventArgs e)
    {
      txtCode.Text = (string)lstCodes.SelectedItem;
    }

    private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      try
      {
        var si = new ProcessStartInfo(linkLabel1.Text);
        si.UseShellExecute = true;
        Process.Start(si);
        linkLabel1.LinkVisited = true;
      }
      catch (Exception ex)
      {
        MessageBox.Show("Website not available\n" + ex.Message);
      }
    }
  }
}