﻿namespace BarcodeSimulator
{
  partial class frmSimulator
  {
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    ///  Required method for Designer support - do not modify
    ///  the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      lstCodes = new ListBox();
      btnAdd = new Button();
      btnRemove = new Button();
      btnSend = new Button();
      txtCode = new TextBox();
      cboPorts = new ComboBox();
      label1 = new Label();
      linkLabel1 = new LinkLabel();
      label2 = new Label();
      tableLayoutPanel1 = new TableLayoutPanel();
      panel1 = new Panel();
      panel2 = new Panel();
      panel3 = new Panel();
      panel4 = new Panel();
      panel5 = new Panel();
      tableLayoutPanel1.SuspendLayout();
      panel1.SuspendLayout();
      panel2.SuspendLayout();
      panel3.SuspendLayout();
      panel4.SuspendLayout();
      panel5.SuspendLayout();
      SuspendLayout();
      // 
      // lstCodes
      // 
      lstCodes.Dock = DockStyle.Fill;
      lstCodes.FormattingEnabled = true;
      lstCodes.ItemHeight = 15;
      lstCodes.Location = new Point(3, 3);
      lstCodes.Name = "lstCodes";
      tableLayoutPanel1.SetRowSpan(lstCodes, 2);
      lstCodes.SelectionMode = SelectionMode.MultiExtended;
      lstCodes.Size = new Size(186, 514);
      lstCodes.TabIndex = 0;
      lstCodes.SelectedIndexChanged += lstCodes_SelectedIndexChanged;
      lstCodes.DoubleClick += lstCodes_DoubleClick;
      // 
      // btnAdd
      // 
      btnAdd.Enabled = false;
      btnAdd.Location = new Point(3, 3);
      btnAdd.Name = "btnAdd";
      btnAdd.Size = new Size(75, 23);
      btnAdd.TabIndex = 1;
      btnAdd.Text = "Add";
      btnAdd.UseVisualStyleBackColor = true;
      btnAdd.Click += btnAdd_Click;
      // 
      // btnRemove
      // 
      btnRemove.Enabled = false;
      btnRemove.Location = new Point(3, 32);
      btnRemove.Name = "btnRemove";
      btnRemove.Size = new Size(75, 23);
      btnRemove.TabIndex = 2;
      btnRemove.Text = "Remove";
      btnRemove.UseVisualStyleBackColor = true;
      btnRemove.Click += btnRemove_Click;
      // 
      // btnSend
      // 
      btnSend.Enabled = false;
      btnSend.Location = new Point(3, 3);
      btnSend.Name = "btnSend";
      btnSend.Size = new Size(75, 23);
      btnSend.TabIndex = 3;
      btnSend.Text = "Send";
      btnSend.UseVisualStyleBackColor = true;
      btnSend.Click += btnSend_Click;
      // 
      // txtCode
      // 
      txtCode.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      txtCode.Location = new Point(3, 4);
      txtCode.Name = "txtCode";
      txtCode.Size = new Size(180, 23);
      txtCode.TabIndex = 4;
      txtCode.TextChanged += txtCode_TextChanged;
      // 
      // cboPorts
      // 
      cboPorts.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      cboPorts.FormattingEnabled = true;
      cboPorts.Location = new Point(69, 3);
      cboPorts.Name = "cboPorts";
      cboPorts.Size = new Size(114, 23);
      cboPorts.TabIndex = 5;
      cboPorts.SelectedIndexChanged += cboPorts_SelectedIndexChanged;
      // 
      // label1
      // 
      label1.AutoSize = true;
      label1.Location = new Point(3, 6);
      label1.Name = "label1";
      label1.Size = new Size(60, 15);
      label1.TabIndex = 6;
      label1.Text = "COM Port";
      // 
      // linkLabel1
      // 
      linkLabel1.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      linkLabel1.Location = new Point(3, 87);
      linkLabel1.Name = "linkLabel1";
      linkLabel1.Size = new Size(280, 48);
      linkLabel1.TabIndex = 7;
      linkLabel1.TabStop = true;
      linkLabel1.Text = "https://freevirtualserialports.com/";
      linkLabel1.LinkClicked += linkLabel1_LinkClicked;
      // 
      // label2
      // 
      label2.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      label2.Location = new Point(3, 0);
      label2.Name = "label2";
      label2.Size = new Size(280, 77);
      label2.TabIndex = 8;
      label2.Text = "Note: Use 2 virtual COM ports with a virtual local bridge to connect this tool to the terminal.\r\n\r\ne.g. HHD Virtual Serial Port Tools";
      // 
      // tableLayoutPanel1
      // 
      tableLayoutPanel1.ColumnCount = 3;
      tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
      tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 100F));
      tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
      tableLayoutPanel1.Controls.Add(lstCodes, 0, 0);
      tableLayoutPanel1.Controls.Add(panel1, 0, 2);
      tableLayoutPanel1.Controls.Add(panel2, 1, 2);
      tableLayoutPanel1.Controls.Add(panel3, 1, 0);
      tableLayoutPanel1.Controls.Add(panel4, 2, 0);
      tableLayoutPanel1.Controls.Add(panel5, 1, 1);
      tableLayoutPanel1.Dock = DockStyle.Fill;
      tableLayoutPanel1.Location = new Point(0, 0);
      tableLayoutPanel1.Name = "tableLayoutPanel1";
      tableLayoutPanel1.RowCount = 3;
      tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
      tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
      tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
      tableLayoutPanel1.Size = new Size(484, 561);
      tableLayoutPanel1.TabIndex = 9;
      // 
      // panel1
      // 
      panel1.Controls.Add(label1);
      panel1.Controls.Add(cboPorts);
      panel1.Dock = DockStyle.Fill;
      panel1.Location = new Point(3, 523);
      panel1.Name = "panel1";
      panel1.Size = new Size(186, 35);
      panel1.TabIndex = 1;
      // 
      // panel2
      // 
      panel2.Controls.Add(btnSend);
      panel2.Dock = DockStyle.Fill;
      panel2.Location = new Point(195, 523);
      panel2.Name = "panel2";
      panel2.Size = new Size(94, 35);
      panel2.TabIndex = 2;
      // 
      // panel3
      // 
      panel3.Controls.Add(btnAdd);
      panel3.Controls.Add(btnRemove);
      panel3.Dock = DockStyle.Fill;
      panel3.Location = new Point(195, 3);
      panel3.Name = "panel3";
      panel3.Size = new Size(94, 254);
      panel3.TabIndex = 3;
      // 
      // panel4
      // 
      panel4.Controls.Add(txtCode);
      panel4.Dock = DockStyle.Fill;
      panel4.Location = new Point(295, 3);
      panel4.Name = "panel4";
      panel4.Size = new Size(186, 254);
      panel4.TabIndex = 4;
      // 
      // panel5
      // 
      tableLayoutPanel1.SetColumnSpan(panel5, 2);
      panel5.Controls.Add(label2);
      panel5.Controls.Add(linkLabel1);
      panel5.Dock = DockStyle.Fill;
      panel5.Location = new Point(195, 263);
      panel5.Name = "panel5";
      panel5.Size = new Size(286, 254);
      panel5.TabIndex = 5;
      // 
      // frmSimulator
      // 
      AutoScaleDimensions = new SizeF(7F, 15F);
      AutoScaleMode = AutoScaleMode.Font;
      ClientSize = new Size(484, 561);
      Controls.Add(tableLayoutPanel1);
      MaximizeBox = false;
      MinimumSize = new Size(500, 400);
      Name = "frmSimulator";
      Text = "Barcode Simulator";
      FormClosing += frmSimulator_FormClosing;
      Load += frmSimulator_Load;
      tableLayoutPanel1.ResumeLayout(false);
      panel1.ResumeLayout(false);
      panel1.PerformLayout();
      panel2.ResumeLayout(false);
      panel3.ResumeLayout(false);
      panel4.ResumeLayout(false);
      panel4.PerformLayout();
      panel5.ResumeLayout(false);
      ResumeLayout(false);
    }

    #endregion

    private ListBox lstCodes;
    private Button btnAdd;
    private Button btnRemove;
    private Button btnSend;
    private TextBox txtCode;
    private ComboBox cboPorts;
    private Label label1;
    private LinkLabel linkLabel1;
    private Label label2;
    private TableLayoutPanel tableLayoutPanel1;
    private Panel panel1;
    private Panel panel2;
    private Panel panel3;
    private Panel panel4;
    private Panel panel5;
  }
}