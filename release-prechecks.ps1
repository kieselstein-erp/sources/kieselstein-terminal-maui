if ($NOCHECK) {
    Write-Warning "NOCHECK is set => Ignoring git-checks. Be careful!"
}

$shortStatus = git status --short
Write-Output "short status: $shortStatus"
if ($shortStatus -And (! $NOCHECK)) { 
   throw 'Git working dir is not clean! No Release allowed!'
} 

$pushDryRunOut = git push --dry-run 2>&1
Write-Output "DryRun output: $pushDryRunOut"
if ((! $pushDryRunOut -like "Everything up-to-date") -And (! $NOCHECK) ) {
    throw 'git not pushed. No Release allowed!'
}

Write-Output "Ready to Release"