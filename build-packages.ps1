$ErrorActionPreference = 'Stop'

$versionNumber = git rev-list HEAD --count
Write-Output "versionNumber: $versionNumber"

$versionName = Read-Host -Prompt 'Enter new 3-number version name, separated with dots (e.g. "0.0.1")'
#git describe --tags --match "v*" --dirty
Write-Output "versionName: $versionName"

$manifestFile = '.\Terminal\Platforms\Windows\Package.appxmanifest'

#
# Incrementing the version number in the manifest file. 
# There seems no way around this (placeholders do not work there) 
#
$xml = [xml](Get-Content -Path $manifestFile)
$node = $xml.Package.Identity | where {$_.Name -eq 'maui-package-name-placeholder'} 
$node.Version = "$versionName.0"
$xml.Save($manifestFile)

# committing the incremented version number
git add $manifestFile
git commit -m"preparing release of versionNumber: $versionNumber, versionName: $versionName."
git push

$prepTasks = @(
    @{ Label = "Remove Windows packages"; Command = {  Remove-Item -ErrorAction SilentlyContinue -recurse -path ".\Terminal\bin\Release\net8.0-windows10.0.19041.0\win10-x64\AppPackages"}}
    @{ Label = "Clean Android"; Command = { dotnet clean -c Release -f net8.0-android Terminal } }
    @{ Label = "Clean Windows"; Command = { dotnet clean -c Release -f net8.0-windows10.0.19041.0 Terminal } }
    @{ Label = "Clean Testing"; Command = { dotnet clean -c Release -f net8.0 } }
    @{ Label = "Test Project"; Command = { dotnet test -c Release -f net8.0 } }
)

function runTasks($tasks) {
    foreach ($task in $tasks) {
        # Write status message
        Write-Host $task['Label'] -ForegroundColor Blue
  
        # Invoke command
        & $task['Command']
  
        # Test exit code
        if ($LASTEXITCODE -gt 0) {
            Write-Host "Task '$($task['Label'])' failed with exit code ${LASTEXITCODE}"
            exit 1
        }
    }
}

runTasks($prepTasks)

dotnet publish -c Release -f net8.0-android -p:ApplicationDisplayVersion=$versionName -p:ApplicationVersion=$versionNumber Terminal
dotnet publish -c Release -f net8.0-windows10.0.19041.0 -p:ApplicationDisplayVersion=$versionName -p:ApplicationVersion=$versionNumber Terminal
Write-Output "Success build-packages ( versionName: $versionName versionNumber: $versionNumber )"
