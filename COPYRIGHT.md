# Copyright notice

Copyright 2023 by the [authors](https://gitlab.com/kieselstein-erp/sources/kieselstein-terminal-maui/-/graphs/main?ref_type=heads). Applies to all files in this repository.
