﻿using KieselsteinErp.Terminal.Views.App;
using MetroLog.Maui;

namespace KieselsteinErp.Terminal;

public partial class AppShellMobile : Shell
{
  public AppShellMobile()
  {
    InitializeComponent();

    Routing.RegisterRoute("LogonPage/MetroLogPage", typeof(MetroLogPage));
    Routing.RegisterRoute("LogonPage/AppMenuPage", typeof(AppMenuPage));
    Routing.RegisterRoute("AppMenuPage/InventorySelectionPage", typeof(InventorySelectionPage));
    Routing.RegisterRoute("InventorySelectionPage/InventoryPage", typeof(InventoryPage));
    Routing.RegisterRoute("AppMenuPage/ProductionPage", typeof(ProductionPage));
    Routing.RegisterRoute("AppMenuPage/TimeTrackingPage", typeof(TimeTrackingPage));
    Routing.RegisterRoute("AppMenuPage/ItemsPage", typeof(ItemsPage));
    Routing.RegisterRoute("AppMenuPage/PurchaseOrderPage", typeof(PurchaseOrderPage));
    Routing.RegisterRoute("AppMenuPage/DeliveryPage", typeof(DeliveryPage));
    Routing.RegisterRoute("AppMenuPage/OrderDeliveryPage", typeof(OrderDeliveryPage));
    Routing.RegisterRoute("AppMenuPage/CollectiveOrderDeliveryPage", typeof(CollectiveOrderDeliveryPage));
  }

  private void ShellContent_ChildAdded(object sender, ElementEventArgs e)
  {

  }
}
