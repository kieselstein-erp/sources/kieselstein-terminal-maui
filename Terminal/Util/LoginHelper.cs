﻿using System.Text;

namespace KieselsteinErp.Terminal.Util
{
  public class LoginHelper
  {
    public static string Decrypt(string username, string password)
    {
      int j = 0;
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < password.Length; i++)
      {
        int c = (int)(password[i]) ^ (int)(username[j++]);
        sb.Append(ChrW(c));
        if (j >= username.Length)
          j = 0;
      }
      return sb.ToString();
    }

    public static string FromHex(string input)
    {
      List<byte> bytes = new List<byte>();
      for (int i = 0;i < input.Length; i+=2)
      {
        bytes.Add(Convert.ToByte(input.Substring(i, 2), 16));
      }
      return Encoding.UTF8.GetString(bytes.ToArray());
    }

    private static char ChrW(int code)
    {
      return Convert.ToChar((int)(code & 0xffff));
    }
  }
}
