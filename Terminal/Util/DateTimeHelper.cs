﻿using System;

namespace KieselsteinErp.Terminal.Util
{
  public static class DateTimeHelper
  {
    public static DateTime FromMillisecondsSinceUnixEpoch(long milliseconds)
    {
      DateTimeOffset offset = DateTimeOffset.FromUnixTimeMilliseconds(milliseconds);
      return offset.LocalDateTime;
    }

    public static long ToMillisecondsSinceUnixEpoch(DateTime dateTime)
    {
      DateTimeOffset offset = new DateTimeOffset(dateTime);
      return offset.ToUnixTimeMilliseconds();
    }

    public static long ToMillisecondsSinceUnixEpoch(DateOnly date)
    {
      return ToMillisecondsSinceUnixEpoch(new DateTime(date,new TimeOnly(0,0)));
    }
  }
}
