﻿using KieselsteinErp.Terminal.Services.Machine;
using KieselsteinErp.Terminal.Services.Partslist;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.ViewModels;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Util
{
  public static class PageUIHelper
  {
    public static async Task<string> MakeTitle(ISettingsService settingsService, IPartslistService partslistService, IMachineService machineService)
    {
      string title = string.Empty;
      if (settingsService.ManufacturingGroupId != null)
      {
        var entry = await partslistService.GetProductionGroupEntryAsync((int)settingsService.ManufacturingGroupId);
        if (entry != null)
          title += entry.description;
      }
      string machineGroup = string.Empty;
      if (settingsService.MachineGroupId != null)
      {
        var entry = await machineService.GetMachineGroupEntryAsync((int)settingsService.MachineGroupId);
        if (entry != null)
          title += string.IsNullOrEmpty(title) ? entry.description : $" {(char)0x2022} {entry.description}";
      }
      string machine = string.Empty;
      if (settingsService.MachineId != null)
      {
        var entry = await machineService.GetMachineEntryAsync((int)settingsService.MachineId);
        if (entry != null)
          title += string.IsNullOrEmpty(title) ? entry.description : $" {(char)0x2022} {entry.description}";
      }
      if (string.IsNullOrEmpty(title))
        title = AllMachines;
      return $"Terminal: {title}".Trim();
    }

    public static string MakeTitle(Machine machine)
    {
      return $"Terminal: {machine?.ProductionGroupName} {(char)0x2022} {machine?.MachineGroupName} {(char)0x2022} {machine?.MachineName}";
    }

  }
}
