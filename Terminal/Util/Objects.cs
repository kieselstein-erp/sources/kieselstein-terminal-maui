﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KieselsteinErp.Terminal.Util
{
    public class Objects
    {
        public static  T ArgNotNull<T>(T anObject, string argName) {
            ArgumentNullException.ThrowIfNull(anObject, argName);
            return anObject;
        }
    }
}
