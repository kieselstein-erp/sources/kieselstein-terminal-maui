﻿namespace KieselsteinErp.Terminal.Util
{
  public class DecimalHelper
  {
    public static decimal? ConvertDecimal(string amountString)
    {
      if (amountString == null) return null;
      if (decimal.TryParse(amountString, out decimal ret))
        return ret;
      else
        return null;
    }

    internal static string ConvertString(decimal? value)
    {
      if (value == null) return string.Empty;
      return ((decimal)value).ToString("N3");
    }

    public static string CheckDecimalSeparator(string value)
    {
      if (MauiProgram.CurrentCulture != null && MauiProgram.DefaultCulture != null)
      {
        if (!MauiProgram.DefaultCulture.NumberFormat.NumberDecimalSeparator.Equals(MauiProgram.CurrentCulture.NumberFormat.NumberDecimalSeparator))
        {
          if (value.EndsWith(MauiProgram.DefaultCulture.NumberFormat.NumberDecimalSeparator))
            value = value.Replace(MauiProgram.DefaultCulture.NumberFormat.NumberDecimalSeparator, MauiProgram.CurrentCulture.NumberFormat.NumberDecimalSeparator);
        }
      }

      return value;
    }
  }
}
