﻿using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Net;
using Microsoft.Extensions.Logging;

namespace KieselsteinErp.Terminal.Util
{
  public class IpHelper
  {
    public static IPAddress GetIpAddress()
    {
      if (DeviceInfo.Platform == DevicePlatform.WinUI)
      {
        var iplist = (from a in Dns.GetHostAddresses(Dns.GetHostName())
                      where a.AddressFamily == AddressFamily.InterNetwork
                      select a).ToArray();
        return iplist[0];
      }
      else
      {
        var result = new List<IPAddress>();
        try
        {
          var upAndNotLoopbackNetworkInterfaces = NetworkInterface.GetAllNetworkInterfaces().Where(n => n.NetworkInterfaceType != NetworkInterfaceType.Loopback
                                                                                                        && n.OperationalStatus == OperationalStatus.Up);
          foreach (var networkInterface in upAndNotLoopbackNetworkInterfaces)
          {
            var iPInterfaceProperties = networkInterface.GetIPProperties();

            var unicastIpAddressInformation = iPInterfaceProperties.UnicastAddresses.FirstOrDefault(u => u.Address.AddressFamily == AddressFamily.InterNetwork);
            if (unicastIpAddressInformation == null) continue;

            result.Add(unicastIpAddressInformation.Address);
          }
          return result[0];
        }
        catch (Exception ex)
        {
          var loggerFactory = Application.Current.MainPage.Handler.MauiContext.Services.GetService<ILoggerFactory>();
          ILogger logger = loggerFactory.CreateLogger<IpHelper>();
          logger?.LogWarning($"Unable to find IP: {ex.Message}");
          return null;
        }
      }
    }
  }
}
