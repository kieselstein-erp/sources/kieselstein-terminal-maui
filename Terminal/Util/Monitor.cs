﻿//#if WINDOWS
using Microsoft.Maui.Controls;
using Microsoft.Maui.Controls.PlatformConfiguration;
using SkiaSharp;
using System.ComponentModel;
using System.Data;
using System.Runtime.InteropServices;
using System.Threading;
//#endif

namespace KieselsteinErp.Terminal.Util
{

  public sealed class Monitor
  {
    //#if WINDOWS

    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
      public int Left;        // x position of upper-left corner
      public int Top;         // y position of upper-left corner
      public int Right;       // x position of lower-right corner
      public int Bottom;      // y position of lower-right corner
    }

    private Monitor(IntPtr handle)
    {
      Handle = handle;
      var mi = new MONITORINFOEX();
      mi.cbSize = Marshal.SizeOf(mi);
      if (!GetMonitorInfo(handle, ref mi))
        throw new Win32Exception(Marshal.GetLastWin32Error());

      DeviceName = mi.szDevice.ToString();
      Bounds = new Rect { Left = mi.rcMonitor.Left, Top = mi.rcMonitor.Top, Right = mi.rcMonitor.Right, Bottom = mi.rcMonitor.Bottom };
      WorkingArea = new Rect { Left = mi.rcWork.Left, Top = mi.rcWork.Top, Right = mi.rcWork.Right, Bottom = mi.rcWork.Bottom };
      IsPrimary = mi.dwFlags.HasFlag(MONITORINFOF.MONITORINFOF_PRIMARY);
      try
      {
        uint dpiX, dpiY;
        GetDpiForMonitor(handle, DpiType.Effective, out dpiX, out dpiY);
        DpiRatio = (double)dpiX / (double)96;
      }
      catch
      {
        // fallback for Windows 7 and older - not 100% reliable
        DpiRatio = 1;
      }
    }

    public IntPtr Handle { get; }
    public bool IsPrimary { get; }
    public Rect WorkingArea { get; }
    public Rect Bounds { get; }
    public string DeviceName { get; }
    public double DpiRatio { get; }

    public static IEnumerable<Monitor> All
    {
      get
      {
        var all = new List<Monitor>();
        EnumDisplayMonitors(IntPtr.Zero, IntPtr.Zero, (m, h, rc, p) =>
        {
          all.Add(new Monitor(m));
          return true;
        }, IntPtr.Zero);
        return all;
      }
    }

    public static Monitor GetMonitor(Point p)
    {
      var monitors = Util.Monitor.All.ToArray();
      if (monitors.Length == 1) return monitors[0];

      foreach (Monitor monitor in monitors)
      {
        if (monitor.Bounds.Contains(p))
          return monitor;
      }
      foreach (Monitor monitor in monitors)
        if (monitor.IsPrimary) return monitor;
      
      throw new InvalidOperationException("No Monitor found");
    }

    public override string ToString() => DeviceName;
    public static IntPtr GetNearestFromWindow(IntPtr hwnd) => MonitorFromWindow(hwnd, MFW.MONITOR_DEFAULTTONEAREST);
    public static IntPtr GetDesktopMonitorHandle() => GetNearestFromWindow(GetDesktopWindow());
    public static IntPtr GetShellMonitorHandle() => GetNearestFromWindow(GetShellWindow());
    public static Monitor FromWindow(IntPtr hwnd, MFW flags = MFW.MONITOR_DEFAULTTONULL)
    {
      var h = MonitorFromWindow(hwnd, flags);
      return h != IntPtr.Zero ? new Monitor(h) : null;
    }

    [Flags]
    public enum MFW
    {
      MONITOR_DEFAULTTONULL = 0x00000000,
      MONITOR_DEFAULTTOPRIMARY = 0x00000001,
      MONITOR_DEFAULTTONEAREST = 0x00000002,
    }

    [Flags]
    public enum MONITORINFOF
    {
      MONITORINFOF_NONE = 0x00000000,
      MONITORINFOF_PRIMARY = 0x00000001,
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    private struct MONITORINFOEX
    {
      public int cbSize;
      public RECT rcMonitor;
      public RECT rcWork;
      public MONITORINFOF dwFlags;

      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
      public string szDevice;
    }

    [DllImport("Shcore.dll")]
    private static extern IntPtr GetDpiForMonitor([In] IntPtr hmonitor, [In] DpiType dpiType, [Out] out uint dpiX, [Out] out uint dpiY);

    public enum DpiType
    {
      Effective = 0,
      Angular = 1,
      Raw = 2,
    }

    private delegate bool MonitorEnumProc(IntPtr monitor, IntPtr hdc, IntPtr lprcMonitor, IntPtr lParam);

    [DllImport("user32")]
    private static extern IntPtr GetDesktopWindow();

    [DllImport("user32")]
    private static extern IntPtr GetShellWindow();

    [DllImport("user32")]
    private static extern bool EnumDisplayMonitors(IntPtr hdc, IntPtr lprcClip, MonitorEnumProc lpfnEnum, IntPtr dwData);

    [DllImport("user32")]
    private static extern IntPtr MonitorFromWindow(IntPtr hwnd, MFW flags);

    [DllImport("user32", CharSet = CharSet.Unicode)]
    private static extern bool GetMonitorInfo(IntPtr hmonitor, ref MONITORINFOEX info);
//#endif
  }
}
