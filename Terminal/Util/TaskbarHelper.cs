﻿#if WINDOWS
using System.Runtime.InteropServices;
#endif

namespace KieselsteinErp.Terminal.Util
{
  public static class TaskbarHelper
  {
#if WINDOWS
    [DllImport("user32.dll")]
    public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

    [DllImport("user32.dll", SetLastError = true)]
    static extern bool GetWindowRect(IntPtr hwnd, out RECT lpRect);

    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
      public int Left;        // x position of upper-left corner
      public int Top;         // y position of upper-left corner
      public int Right;       // x position of lower-right corner
      public int Bottom;      // y position of lower-right corner
    }

    private static IntPtr GetTaskBarHandle()
    {
      //Get the handle of the task bar
      IntPtr taskBarHandle;
      taskBarHandle = FindWindow("Shell_traywnd", "");
      return taskBarHandle;
    }

    public static RECT GetTaskBarRect()
    {
      RECT rct;

      //Get the taskbar window rect in screen coordinates
      GetWindowRect(GetTaskBarHandle(), out rct);
      return rct;
    }
#endif
  }
}
