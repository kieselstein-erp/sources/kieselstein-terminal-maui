﻿namespace KieselsteinErp.Terminal.Models
{
  public record PopupResult
  {
    public PopupResult(bool canceled, string entry)
    {
      this.Canceled = canceled;
      this.Entry = entry;
    }
    public bool Canceled { get; }
    public string Entry { get; }

    public decimal EntryAsDecimal()
    {
      decimal result = 0;
      decimal.TryParse(this.Entry, out result);
      return result;
    }
  }
}
