﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Models
{
  public class DeliveryPosition
  {
    private readonly DeliveryPositionEntry _deliveryPosition;
    private readonly DeliveryTextPositionEntry _deliveryTextPosition;
    private readonly DeliveryPositionType _positionType;
    private readonly int _id;
    private readonly int _iSort;
    public enum DeliveryPositionType
    {
      item,
      text
    }

    public DeliveryPosition(int id, int iSort, DeliveryPositionEntry position)
    {
      _id = id;
      _iSort = iSort;
      _deliveryPosition = position;
      _positionType = DeliveryPositionType.item;
    }

    public DeliveryPosition(int id, int iSort, DeliveryTextPositionEntry textPosition)
    {
      _id = id;
      _iSort = iSort;
      _deliveryTextPosition = textPosition;
      _positionType = DeliveryPositionType.text;
    }

    public DeliveryPositionType PositionType { get { return _positionType; } }

    public string ItemCnr { get { return (_positionType == DeliveryPositionType.item ? _deliveryPosition.itemCnr : null); } }

    public string Description { get { return (_positionType == DeliveryPositionType.item ? _deliveryPosition.description : _deliveryTextPosition.text); } }

    public decimal? Amount { get { return (_positionType == DeliveryPositionType.item ? _deliveryPosition.amount : null); } }

    public string UnitCnr {  get { return (_positionType == DeliveryPositionType.item ? _deliveryPosition.unitCnr : null); } }

    public string Identities { get { return (_positionType == DeliveryPositionType.item ? BuildIdentityText()  : null); } }

    private string BuildIdentityText()
    {
      try
      {
        string s = string.Empty;
        switch (_deliveryPosition.itemProperty)
        {
          case ItemPropertyEnumConstants.NOIDENTIY:
            return null;
          case ItemPropertyEnumConstants.SERIALNR:
            {
              foreach (var identity in _deliveryPosition.itemIdentity.entries)
                s += $"{identity.identity}, ";
              return s;
            }

          case ItemPropertyEnumConstants.BATCHNR:
            {
              foreach (var identity in _deliveryPosition.itemIdentity.entries)
                s += $"{identity.amount:N3} x {identity.identity}, ";
              return s;
            }
        }
      }
      catch (Exception)
      {
        return null;
      }
      return null;
    }

    public int Id { get { return _id; } }

    public int ISort { get { return _iSort; } }

    public bool IsTextPosition { get { return (_positionType == DeliveryPositionType.text); } }

    public bool IsItemPosition { get { return (_positionType == DeliveryPositionType.item); } }
  }
}

