﻿using KieselsteinErp.Terminal.Models.ScanLabels;

namespace KieselsteinErp.Terminal.Models
{
  public class ItemVDAMessageModel
	{
		public ItemVDAMessageModel(IScanLabel scanLabel)
		{
			ScanLabel = scanLabel;
		}

		public IScanLabel ScanLabel { get; set; }
	}
}
