﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Models
{
  public class BookMultipleWorkstepsQuery
  {
    public BookMultipleWorkstepsQuery(StaffEntry staffEntry, List<TimeDistributionWithMachine> timeDistributions)
    {
      Staff = staffEntry;
      TimeDistributions = timeDistributions;
    }

    public BookMultipleWorkstepsQuery(StaffEntry staffEntry, List<TimeDistributionWithMachine> timeDistributions, List<PieceConfirmation> pieceConfirmations) : this(staffEntry, timeDistributions)
    {
      PieceConfirmations = pieceConfirmations;
    }

    public StaffEntry Staff { get; private set; }
    public List<TimeDistributionWithMachine> TimeDistributions { get; private set; }
    public List<PieceConfirmation> PieceConfirmations { get; private set; }
  }
}
