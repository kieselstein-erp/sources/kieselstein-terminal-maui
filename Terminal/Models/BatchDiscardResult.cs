﻿using Kieselstein.Models;
using System.Net;

namespace KieselsteinErp.Terminal.Models
{
  public class BatchDiscardResult : BookingResult
  {
    private readonly ProductionEntryList _productionEntryList;
    public BatchDiscardResult() { }

    public BatchDiscardResult(bool success, string errorMsg) : base(success, errorMsg) { }

    public BatchDiscardResult(bool success, ProductionEntryList productionEntryList, string message, HttpStatusCode statusCode) : base(success, message, statusCode)
    {
      _productionEntryList = productionEntryList;
    }

    public ProductionEntryList ProductionEntryList { get { return _productionEntryList; } }

  }
}
