﻿using Newtonsoft.Json;

namespace KieselsteinErp.Terminal.Models
{
  public class StorageLocation
  {
    private readonly string _cnr;

    [JsonConstructor()]
    public StorageLocation(string cnr)
    {
      _cnr = cnr;
    }

    [JsonProperty(PropertyName = "cnr")]
    public string Cnr { get { return _cnr; } }

  }
}
