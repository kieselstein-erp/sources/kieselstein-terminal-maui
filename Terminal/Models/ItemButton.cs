﻿using CommunityToolkit.Mvvm.ComponentModel;
using Newtonsoft.Json;

namespace KieselsteinErp.Terminal.Models
{
  public partial class ItemButton : ObservableObject
  {
    private readonly string _name;
    private readonly string _itemCnr;
    private readonly string _productionOrderNumber;
    private readonly string _buttonName;

    public ItemButton(string name, string itemCnr)
    {
      _name = name;
      _itemCnr = itemCnr;
      _buttonName = name;
    }

    [JsonConstructor()]
    public ItemButton(string name, string itemCnr, string productionOrderNumber) : this(name, itemCnr)
    {
      _productionOrderNumber = productionOrderNumber;
      _buttonName = _productionOrderNumber == null ? _name : $"{_productionOrderNumber} {_name}";
    }

    [JsonProperty(PropertyName = "name")]
    public string Name { get { return _name; } }

    [JsonProperty(PropertyName = "cnr")]
    public string ItemCnr { get { return _itemCnr; } }

    [JsonProperty(PropertyName = "productionordercnr")]
    public string ProductionOrderNumber { get { return _productionOrderNumber; } }

    public string ButtonName { get { return _buttonName; } }

    [ObservableProperty]
    private bool _isEnabled = false;


  }
}
