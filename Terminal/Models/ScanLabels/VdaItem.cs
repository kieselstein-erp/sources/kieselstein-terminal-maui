﻿using System.Text;

namespace KieselsteinErp.Terminal.Models.ScanLabels
{

  public class VdaItem
  {
    public string Cnr;
    public string DateCode; // Nullable(Of Date)
    public Nullable<DateTime> ExpDate;
    public string BatchNumber1;
    public string BatchNumber2;
    public decimal Amount { get; private set; }
    public string PurchaseOrderNumber;
    public string DeliverNoteNumber;

    public string PurchaseOrderNumberUnformatted()
    {
      if (PurchaseOrderNumber == "")
        return "";
      string bn = PurchaseOrderNumber;
      if (!string.IsNullOrEmpty(bn) && bn.TrimStart().StartsWith("BS"))
        bn = bn.Replace("BS", "").TrimStart();
      return bn;
    }

    public string ToText()
    {
      StringBuilder sb = new StringBuilder();
      sb.Append(Cnr);
      sb.Append(", ");
      sb.Append(DateCode);
      sb.Append(", ");
      sb.Append(ExpDate);
      sb.Append(", ");
      sb.Append(BatchNumber1);
      sb.Append(", ");
      sb.Append(BatchNumber2);
      sb.Append(", ");
      sb.Append(Amount);
      sb.Append(", ");
      sb.Append(PurchaseOrderNumber);
      sb.Append(", ");
      sb.Append(DeliverNoteNumber);
      return sb.ToString();
    }

    public string BatchNumberReelId()
    {
      System.Text.StringBuilder sb = new System.Text.StringBuilder(BatchNumber1);
      if (DateCode != null)
      {
        sb.Append("|");
        sb.Append(DateCode);
        if (BatchNumber2 != "")
        {
          sb.Append("|");
          sb.Append(BatchNumber2);
        }
      }
      else if (BatchNumber2 != "")
      {
        sb.Append("||");
        sb.Append(BatchNumber2);
      }
      return sb.ToString();
    }

    internal void SetAmount(decimal? value)
    {
      if (value != null)
        Amount = (decimal)value;
    }
  }
}
