﻿namespace KieselsteinErp.Terminal.Models.ScanLabels
{
  public enum ELogType
  {
    NOTINITIALIZED,
    DEBUG,
    INFO,
    WARN,
    ERROR
  }

  public enum LabelType
  {
    LabelType_VDA4992,
    LabelType_MATLabel2_6,
    LabelType_HV1D
  }

  public enum EHvBarcodeTyp
  {
    ItemNumber,
    StorageLocation,
    LotNumber,
    PurchaseOrderNumber,
    OrderNumber,
    DeliverNoteNumber,
    CustomerNumber,
    IdCardNumber,
    Logon,
    Unknown
  }

  public interface IScanLabel
  {
    LabelType LabelType { get; }
    EHvBarcodeTyp HvBarcodeTyp { get; }
    object DecodeValue(string identifier);
    object DecodeValue(int index);
    string Value { get; }
    bool HasData();
    void Init(string rawdata);
    string RawData { get; }
    VdaItem ToVdaItem();
    string ToText();
  }
}