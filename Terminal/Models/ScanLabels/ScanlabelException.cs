﻿using System;

namespace KieselsteinErp.Terminal.Models.ScanLabels
{
public class ScanLabelException
  {
    public class FormatException : Exception
    {
      public FormatException() :base("VDA Format Error")
      {
      }

      public FormatException(string message) : base(message)
      {
      }

      public FormatException(string message, Exception innerException) : base(message, innerException)
      {
      }
    }

    public class HeaderNotFoundException : Exception
    {
      public HeaderNotFoundException() : base("VDA Header not found")
      {
      }

      public HeaderNotFoundException(string message) : base(message)
      {
      }

      public HeaderNotFoundException(string message, Exception innerException) : base(message, innerException)
      {
      }
    }
  }

}

