﻿using System.Diagnostics;
using System.Text;

namespace KieselsteinErp.Terminal.Models.ScanLabels
{

  public class ScanLabel_MATLabel2_6 : IScanLabel
  {
    private const char FS = (char)28;
    private const char GS = (char)29;
    private const char RS = (char)30;
    private const char US = (char)31;
    private const char ALTGS = '@';

    public string[] dataIdentifier = ["12S", "P", "1P", "31P", "12V", "10V", "2P", "20P", "6D", "14D", "30P", "Z", "K", "16K", "V", "3S", "Q", "20T", "1T", "2T", "1Z"];
    // Anmerkung: 13V hat laut Spec Datatyp N, kann aber mit einem DUNS Identifier beginnen -> Alphanumerisch 
    // Anmerkung: SP05702 Feld 6D als String umdefiniert
    public string[] dataType = ["Integer", "String", "String", "String", "String", "String", "String", "String", "String", "Date", "String", "String", "String", "String", "String", "String", "12ISO3", "Integer", "String", "String", "String"];
    public static readonly string messageHeader = "[)>" + RS + "06" + GS + "12S";
    private static readonly string altMessageHeader = "[)>" + ALTGS + "06" + ALTGS + "12S";
    private string _rawData;
    private Dictionary<string, object> fields;

    private const LabelType _labelType = LabelType.LabelType_MATLabel2_6;

    public LabelType LabelType
    {
      get => _labelType;
    }

    public object DecodeValue(string dataidentifier)
    {
      if (fields.ContainsKey(dataidentifier))
        return fields[dataidentifier];
      else
        return null;
    }

    public object DecodeValue(int index)
    {
      if (index < 0 | index > dataIdentifier.Length - 1)
        return null;
      if (fields.ContainsKey(dataIdentifier[index]))
        return fields[dataIdentifier[index]];
      else
        return null;
    }

    public static bool IsValid(string rawdata)
    {
      if (rawdata.StartsWith(messageHeader))
        return true;
      else if (rawdata.StartsWith(altMessageHeader))
        return true;
      else
        return false;
    }

    public void Init(string rawdata)
    {
      _rawData = rawdata;
      if (rawdata.StartsWith(altMessageHeader))
      {
        string[] s = rawdata.Split(ALTGS);
        if (s.Length < 2)
          throw new ScanLabelException.FormatException();
        else if (s[1] == "06")
          ParseVDA(s, 2);
        else
          throw new ScanLabelException.FormatException(string.Format("invalid format indicator {0}", s[1]));
      }
      else if (rawdata.StartsWith(messageHeader))
      {
        string[] s;
        if (rawdata.Contains(RS))
        {
          s = rawdata.Split(RS);
          if (s.Length < 1)
            throw new ScanLabelException.FormatException();
          else
          {
            s = s[1].Split(GS);
            if (s[0] == "06")
              ParseVDA(s);
            else
              throw new ScanLabelException.FormatException(string.Format("invalid format indicator {0}", s[0]));
          }
        }
        else
        {
          Debug.Assert(false);
          throw new ScanLabelException.HeaderNotFoundException();
        }
      }
      else
        throw new ScanLabelException.HeaderNotFoundException();
    }

    private bool ParseVDA(string[] value, int startIndex = 1)
    {
      Stopwatch sw = new Stopwatch();
      sw.Start();
      fields = new Dictionary<string, object>();
      int identID = 0;
      for (var i = startIndex; i <= value.Length - 1; i++)
      {
        identID = 0;
        if (value[i] != "")
        {
          while (identID < dataIdentifier.Length)
          {
            if (value[i].StartsWith(dataIdentifier[identID]))
            {
              // fields.Add(dataIdentifier(identID), value(i).Substring(dataIdentifier(identID).Length))
              SetField(identID, value[i].Substring(dataIdentifier[identID].Length));
              identID += 1;
              break;
            }
            identID += 1;
          }
        }
      }
      sw.Stop();
      Debug.WriteLine(sw.ElapsedMilliseconds);
      return true;
    }

    public bool HasData()
    {
      if (fields == null)
        return false;
      return fields.Count > 0;
    }

    private void SetField(int index, string data)
    {
      switch (dataType[index])
      {
        case "Integer":
          {
            try
            {
              fields.Add(dataIdentifier[index], int.Parse(data));
            }
            catch (Exception)
            {
              fields.Add(dataIdentifier[index], (int)0);
            }

            break;
          }

        case "String":
          {
            fields.Add(dataIdentifier[index], data);
            break;
          }

        case "Date":
          {
            try
            {
              fields.Add(dataIdentifier[index], DateTime.ParseExact(data, "yyyyMMdd", null));
            }
            catch (Exception)
            {
              fields.Add(dataIdentifier[index], null);
            }

            break;
          }

        case "Decimal":
          {
            fields.Add(dataIdentifier[index], decimal.Parse(data, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture));
            break;
          }

        case "12ISO3":
          {
            if ((data.Length > 6) && !IsNumeric(data.Substring(data.Length - 5, 3)))
            {
              string s = data.Substring(0, data.Length - 6) + "." + data.Substring(data.Length - 3);
              fields.Add(dataIdentifier[index], decimal.Parse(s, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture));
              // Kompatibilität zu VDA4992
              fields.Add("3Q", data.Substring(data.Length - 6, 3));
            }
            else
              fields.Add(dataIdentifier[index], decimal.Parse(data, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture));
            break;
          }

        default:
          {
            Debug.Assert(false);
            break;
          }
      }
    }

    private bool IsNumeric(string value)
    {
      return value.All(char.IsNumber);
    }

    public string ToText()
    {
      StringBuilder sb = new StringBuilder();
      for (var i = 0; i <= dataIdentifier.Length - 1; i++)
      {
        sb.Append(dataIdentifier[i]);
        sb.Append("\t");
        if (fields.ContainsKey(dataIdentifier[i]))
          sb.Append(fields[dataIdentifier[i]]);
        sb.Append("\r\n");
      }
      return sb.ToString();
    }

    public VdaItem ToVdaItem()
    {
      VdaItem vdaItem = new VdaItem();
      try
      {
        vdaItem.Cnr = (string)DecodeValue("P");
        vdaItem.BatchNumber1 = (string)DecodeValue("1T");
        vdaItem.BatchNumber2 = (string)DecodeValue("2T");
        vdaItem.DateCode = (string)DecodeValue("6D");
        vdaItem.ExpDate = (DateTime?)DecodeValue("14D");
        vdaItem.SetAmount((decimal?)DecodeValue("Q"));
        vdaItem.PurchaseOrderNumber = (string)DecodeValue("K");
        vdaItem.DeliverNoteNumber = (string)DecodeValue("16K");
      }
      catch (Exception ex)
      {
        Debug.Print(ex.Message);
        throw new Exception("Fehler im VDA Barcode", ex);
      }
      return vdaItem;
    }

    public string RawData
    {
      get => _rawData;
    }

    public EHvBarcodeTyp HvBarcodeTyp => throw new NotImplementedException();

    public string Value => throw new NotImplementedException();
  }
}
