﻿using System.Diagnostics;
using System.Text;

namespace KieselsteinErp.Terminal.Models.ScanLabels
{
  public class ScanLabel_HV_1D : IScanLabel
  {
    public const char SEP = '|';
    public string[] dataIdentifier = ["P", "1T", "Q"];
    public string[] dataType = ["String", "String", "Decimal"];

    private static readonly string messageHeader = "$";
    private string _rawData;
    private Dictionary<string, object> fields;

    private const LabelType _labelType = LabelType.LabelType_HV1D;

    public LabelType LabelType
    {
      get
      {
        return _labelType;
      }
    }


    private EHvBarcodeTyp _enmHvBarcodeTyp;
    private string _value;

    public EHvBarcodeTyp HvBarcodeTyp
    {
      get
      {
        return _enmHvBarcodeTyp;
      }
    }

    public string Value
    {
      get => _value;
    }

    public object DecodeValue(string dataidentifier)
    {
      if (fields.ContainsKey(dataidentifier))
        return fields[dataidentifier];
      else
        return null;
    }

    public object DecodeValue(int index)
    {
      if (index < 0 | index > dataIdentifier.Length - 1)
        return null;
      if (fields == null)
        return null;
      if (fields.ContainsKey(dataIdentifier[index]))
        return fields[dataIdentifier[index]];
      else
        return null;
    }

    public bool IsValid(string rawdata)
    {
      return (rawdata.StartsWith(messageHeader));
    }

    public void Init(string rawdata)
    {
      if (rawdata.EndsWith("\r\n"))
        rawdata = rawdata.Substring(0, rawdata.Length - 2);

      _rawData = rawdata;

      if (rawdata.StartsWith(messageHeader))
      {
        if (rawdata.StartsWith("$I"))
        {
          string[] s = rawdata.Substring(2).Split(SEP);
          if (s.Length == 0)
            throw new ScanLabelException.FormatException();
          else
            ParseHV(s);
        }
        else if (rawdata.StartsWith("$ABLIEFERN"))
        {
          _enmHvBarcodeTyp = EHvBarcodeTyp.Unknown;
          _value = rawdata;
        }
        else
          switch (rawdata.Substring(0, 2))
          {
            case "$L"  // Losnummer
           :
              {
                _enmHvBarcodeTyp = EHvBarcodeTyp.LotNumber;
                _value = rawdata.Substring(2);
                break;
              }

            case "$O"  // lagerort
     :
              {
                _enmHvBarcodeTyp = EHvBarcodeTyp.StorageLocation;
                _value = rawdata.Substring(2);
                break;
              }

            case "$B"  // Bestellungsnummer
     :
              {
                _enmHvBarcodeTyp = EHvBarcodeTyp.PurchaseOrderNumber;
                _value = rawdata.Substring(2);
                break;
              }

            case "$A"  // Auftragsnummer
     :
              {
                _enmHvBarcodeTyp = EHvBarcodeTyp.OrderNumber;
                _value = rawdata.Substring(2);
                break;
              }

            case "$D"  // Lieferscheinnummer
     :
              {
                _enmHvBarcodeTyp = EHvBarcodeTyp.DeliverNoteNumber;
                _value = rawdata.Substring(2);
                break;
              }

            case "$K"  // Kundennummer für Lieferschein an Barkunde
     :
              {
                _enmHvBarcodeTyp = EHvBarcodeTyp.CustomerNumber;
                _value = rawdata.Substring(2);
                break;
              }

            case "$P"  // Personalausweis
     :
              {
                _enmHvBarcodeTyp = EHvBarcodeTyp.IdCardNumber;
                _value = rawdata.Substring(2);
                break;
              }

            case "$C"  // Anmeldung
     :
              {
                _enmHvBarcodeTyp = EHvBarcodeTyp.Logon;
                _value = rawdata.Substring(2);
                break;
              }

            default:
              {
                _enmHvBarcodeTyp = EHvBarcodeTyp.Unknown;
                _value = rawdata;
                break;
              }
          }
      }
      else
        throw new ScanLabelException.HeaderNotFoundException();
    }

    private bool ParseHV(string[] value)
    {
      fields = new Dictionary<string, object>();
      //int identID = 0;
      for (var i = 0; i <= value.Length - 1; i++)
      {
        switch (i)
        {
          case 0:
            {
              SetField(0, value[0]);
              _value = value[0];
              break;
            }

          case 1:
            {
              SetField(1, value[1]);
              break;
            }
        }
      }
      SetField(2, new decimal(1).ToString());
      return true;
    }

    public bool HasData()
    {
      if (fields == null)
        return false;
      return fields.Count > 0;
    }

    private void SetField(int index, string data)
    {
      switch (dataType[index])
      {
        case "Integer":
          {
            fields.Add(dataIdentifier[index], int.Parse(data));
            break;
          }

        case "String":
          {
            fields.Add(dataIdentifier[index], data);
            break;
          }

        case "Date":
          {
            fields.Add(dataIdentifier[index], DateTime.ParseExact(data, "yyyyMMdd", null));
            break;
          }

        case "Decimal":
          {
            fields.Add(dataIdentifier[index], decimal.Parse(data, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture));
            break;
          }

        default:
          {
            Debug.Assert(false);
            break;
          }
      }
    }

    public string ToText()
    {
      StringBuilder sb = new StringBuilder();
      for (var i = 0; i <= dataIdentifier.Length - 1; i++)
      {
        sb.Append(dataIdentifier[i]);
        sb.Append("\t");
        if (fields.ContainsKey(dataIdentifier[i]))
          sb.Append(fields[dataIdentifier[i]]);
        sb.Append("\r\n");
      }
      return sb.ToString();
    }

    public VdaItem ToVdaItem()
    {
      VdaItem vdaItem = new VdaItem();
      try
      {
        vdaItem.Cnr = (string)DecodeValue("P");
        vdaItem.BatchNumber1 = (string)DecodeValue("1T");
        // o.BatchNumber2 = Me.value("2T")
        // o.DateCode = Me.value("16D")
        // o.ExpDate = Me.value("14D")
        vdaItem.SetAmount((decimal?)DecodeValue("Q"));
        // o.PurchaseOrderNumber = Me.value("K")
        // o.DeliveryNoteNumber = Me.value("2S")
      }
      catch (Exception ex)
      {
        Debug.Print(ex.Message);
        throw new Exception("Fehler im VDA Barcode", ex);
      }
      return vdaItem;
    }

    public string RawData
    {
      get => _rawData;
    }
  }
}

