﻿using System.Diagnostics;
using System.Text;

namespace KieselsteinErp.Terminal.Models.ScanLabels
{

  public class ScanLabel_VDA4992 : IScanLabel
  {
    private const char FS = (char)28;
    private const char GS = (char)29;
    private const char RS = (char)30;
    private const char US = (char)31;

    public string[] dataIdentifier = ["12P", "9K", "P", "1P", "31P", "12V", "10V", "2P", "20P", "16D", "14D", "30P", "Z", "V", "13V", "3S", "Q", "3Q", "1T", "2T", "K", "2S", "1Z"];
    // Anmerkung: 13V hat laut Spec Datatyp N, kann aber mit einem DUNS Identifier beginnen -> Alphanumerisch 
    public string[] dataType = ["Integer", "Integer", "String", "String", "String", "Integer", "String", "String", "String", "Date", "Date", "String", "String", "String", "String", "String", "Decimal", "String", "String", "String", "String", "String", "String"];
    public static readonly string messageHeader = "[)>" + RS + "06" + GS + "12P";
    private string _rawData;
    private Dictionary<string, object> fields;

    private const LabelType _labelType = LabelType.LabelType_VDA4992;

    public LabelType LabelType
    {
      get => _labelType;
    }

    public object DecodeValue(string dataidentifier)
    {
      if (fields.ContainsKey(dataidentifier))
        return fields[dataidentifier];
      else
        return null;
    }


    public object DecodeValue(int index)
    {
      if (index < 0 | index > dataIdentifier.Length - 1)
        return null;
      if (fields.ContainsKey(dataIdentifier[index]))
        return fields[dataIdentifier[index]];
      else
        return null;
    }

    public static bool IsValid(string rawdata)
    {
      return (rawdata.StartsWith(messageHeader));
    }

    public void Init(string rawdata)
    {
      _rawData = rawdata;
      if (rawdata.StartsWith(messageHeader))
      {
        string[] s = rawdata.Split(RS);
        if (s.Length < 1)
          throw new ScanLabelException.FormatException();
        else
        {
          string[] s1 = s[1].Split(GS);
          if (s1[0] == "06")
            ParseVDA(s1);
          else
            throw new ScanLabelException.FormatException(string.Format("invalid format indicator {0}", s1[0]));
        }
      }
      else
        throw new ScanLabelException.HeaderNotFoundException();
    }

    private bool ParseVDA(string[] value)
    {
      Stopwatch sw = new Stopwatch();
      sw.Start();
      fields = new Dictionary<string, object>();
      int identID = 0;
      for (var i = 1; i <= value.Length - 1; i++)
      {
        identID = 0;
        if (value[i] != "")
        {
          while (identID < dataIdentifier.Length)
          {
            if (value[i].StartsWith(dataIdentifier[identID]))
            {
              // fields.Add(dataIdentifier(identID), value(i).Substring(dataIdentifier(identID).Length))
              SetField(identID, value[i].Substring(dataIdentifier[identID].Length));
              identID += 1;
              break;
            }
            identID += 1;
          }
        }
      }
      sw.Stop();
      Debug.WriteLine(sw.ElapsedMilliseconds);
      return true;
    }

    public bool HasData()
    {
      if (fields == null)
        return false;
      return fields.Count > 0;
    }

    private void SetField(int index, string data)
    {
      switch (dataType[index])
      {
        case "Integer":
          {
            fields.Add(dataIdentifier[index], int.Parse(data));
            break;
          }

        case "String":
          {
            fields.Add(dataIdentifier[index], data);
            break;
          }

        case "Date":
          {
            fields.Add(dataIdentifier[index], DateTime.ParseExact(data, "yyyyMMdd", null));
            break;
          }

        case "Decimal":
          {
            fields.Add(dataIdentifier[index], decimal.Parse(data, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture));
            break;
          }

        default:
          {
            Debug.Assert(false);
            break;
          }
      }
    }

    public string ToText()
    {
      StringBuilder sb = new StringBuilder();
      for (var i = 0; i <= dataIdentifier.Length - 1; i++)
      {
        sb.Append(dataIdentifier[i]);
        sb.Append("\t");
        if (fields.ContainsKey(dataIdentifier[i]))
          sb.Append(fields[dataIdentifier[i]]);
        sb.Append("\r\n");
      }
      return sb.ToString();
    }

    public VdaItem ToVdaItem()
    {
      VdaItem vdaItem = new VdaItem();
      try 
      { 
      vdaItem.Cnr = (string)DecodeValue("P");
      vdaItem.BatchNumber1 = (string)DecodeValue("1T");
      vdaItem.BatchNumber2 = (string)DecodeValue("2T");
      vdaItem.DateCode = DecodeValue("16D") == null ? null : ((DateTime)DecodeValue("6D")).ToString("d");
      vdaItem.ExpDate = (DateTime?)DecodeValue("14D");
      vdaItem.SetAmount((decimal?)DecodeValue("Q"));
      vdaItem.PurchaseOrderNumber = (string)DecodeValue("K");
      vdaItem.DeliverNoteNumber = (string)DecodeValue("2S");
    }
      catch (Exception ex)
      {
        Debug.Print(ex.Message);
        throw new Exception("Fehler im VDA Barcode", ex);
  }
      return vdaItem;
    }

    public string RawData
    {
      get => _rawData;
    }

    public EHvBarcodeTyp HvBarcodeTyp => throw new NotImplementedException();

    public string Value => throw new NotImplementedException();
  }

}
