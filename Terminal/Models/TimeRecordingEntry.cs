﻿namespace KieselsteinErp.Terminal.Models
{
    public class TimeRecordingEntry
    {
        private int _forStaffId;
        private string _forStaffCnr;
        private string _userId;
        private int _year;
        private int _month;
        private int _day;
        private int _hour;
        private int _minute;
        private int _second;
        private string _where;

        public int forStaffId { get => _forStaffId; set => _forStaffId = value; }
        public string forStaffCnr { get => _forStaffCnr; set => _forStaffCnr = value; }
        public string userId { get => _userId; set => _userId = value; }
        public int year { get => _year; set => _year = value; }
        public int month { get => _month; set => _month = value; }
        public int day { get => _day; set => _day = value; }
        public int hour { get => _hour; set => _hour = value; }
        public int minute { get => _minute; set => _minute = value; }
        public int second { get => _second; set => _second = value; }
        public string where { get => _where; set => _where = value; }
    }
}

