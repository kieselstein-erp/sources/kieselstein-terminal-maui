﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Services.StateMachine;

namespace KieselsteinErp.Terminal.Models
{
  public class PieceConfirmationQuery
  {
    public string Personalnummer { get; set; }
    public WorkTimeEvent WorkTimeEvent { get; set; }
    public OpenWorkEntryList OpenWorkEntryList { get; set; }
    public ProductionRecordingEntry ProductionRecordingEntry { get; set; }
    public bool IsGhostShiftConfirmation { get; set; }
    public GhostShiftInfo GhostShiftInfo { get; set; }
  }
}
