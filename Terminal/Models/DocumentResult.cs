﻿using System.Net;

namespace KieselsteinErp.Terminal.Models
{
  public class DocumentResult
  {
    private readonly bool _success;
    private readonly string _message;
    private readonly HttpStatusCode _statusCode;
    //private readonly string _documentContent;
    private readonly string _fileName;
    private byte[] _rawBytes;

    public DocumentResult(bool success, string message, HttpStatusCode httpStatusCode) 
    {
      _success = success;
      _message = message;
      _statusCode = httpStatusCode;
    }

    //public DocumentResult(string documentContent, string filename)
    //{
    //  _success = true;
    //  _documentContent = documentContent;
    //  _fileName = filename;
    //}

    public DocumentResult(byte[] rawBytes, string fileName)
    {
      _rawBytes = rawBytes;
      _fileName = fileName;
      _success = true;
    }

    public string Message { get => _message; }
    public bool Success { get => _success; }
    public HttpStatusCode StatusCode { get => _statusCode; }
    //public string DocumentContent { get => _documentContent; }
    public string FileName { get => _fileName; }
    public byte[] DocumentAsBytes { get => _rawBytes; }
  }
}
