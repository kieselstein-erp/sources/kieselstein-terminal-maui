﻿using CommunityToolkit.Mvvm.ComponentModel;
using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Models
{
  public partial class PieceConfirmation : ObservableObject
  {
    private readonly OpenWorkEntry _openWorkEntry;

    public PieceConfirmation() { }

    public PieceConfirmation(OpenWorkEntry entry)
    {
      _openWorkEntry = entry;
    }

    public string ProductionCnr { get { return _openWorkEntry.productionCnr; } set { _openWorkEntry.productionCnr = value; } }
    public string PartlistItemCnr { get => _openWorkEntry.partlistItemCnr; }

    public int Id { get => _openWorkEntry.id; }
    public int WorkNumber { get => _openWorkEntry.workNumber; }
    public string WorkName { get => _openWorkEntry.workItemDescription; }
    public decimal OpenQuantity { get => _openWorkEntry.openQuantity; }
    public string WorkstepNumberFormatted => _openWorkEntry.subWorkstepNumber == null
      ? _openWorkEntry.workNumber.ToString("D")
      : _openWorkEntry.workNumber.ToString("D") + "." + ((int)_openWorkEntry.subWorkstepNumber).ToString("D");

    [ObservableProperty]
    private decimal? _goodAmount;

    [ObservableProperty]
    private decimal? _badAmount;

    [ObservableProperty]
    private bool _finished;

  }
}
