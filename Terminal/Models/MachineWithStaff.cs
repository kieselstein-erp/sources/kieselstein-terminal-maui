﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Models
{
  public class MachineWithStaff
  {
    public MachineEntry MachineEntry { get; set; }
    public string StaffName { get; set; }
  }
}
