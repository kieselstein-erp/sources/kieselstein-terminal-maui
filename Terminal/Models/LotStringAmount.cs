﻿namespace KieselsteinErp.Terminal.Models
{
	public record LotStringAmount
	{
		public LotStringAmount(decimal amount, string lotString)
		{
			this.amount = amount;
			this.lotString = lotString;
		}
		public decimal amount { get; }
		public string lotString { get; }
	}
}
