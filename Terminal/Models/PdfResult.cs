﻿using System.Net;

namespace KieselsteinErp.Terminal.Models
{
  public class PdfResult
  {
    private readonly bool _success;
    private readonly string _message;
    private readonly HttpStatusCode _statusCode;
    private readonly string _pdfString;

    public PdfResult(bool success, string message, HttpStatusCode httpStatusCode) 
    {
      _success = success;
      _message = message;
      _statusCode = httpStatusCode;
    }

    public PdfResult(string pdfString)
    {
      _success = true;
      _pdfString = pdfString;
    }
    
    public string Message { get => _message; }
    public bool Success { get => _success; }
    public HttpStatusCode StatusCode { get => _statusCode; }
    public string PdfString { get => _pdfString; }

  }
}
