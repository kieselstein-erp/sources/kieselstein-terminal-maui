﻿using System.Net;

namespace KieselsteinErp.Terminal.Models
{
  public class PrintResult
  {
    private bool _success = false;
    private int? _errorCode = null;
    private HttpStatusCode _statusCode;
    private string _message;

    public PrintResult(bool success, HttpStatusCode statusCode, string message)
    {
      _success = success;
      _statusCode = statusCode;
      _message = message;
    }

    public PrintResult(bool success, HttpStatusCode statusCode, string message, int? errorCode) : this(success, statusCode, message)
    {
      _errorCode = errorCode;
    }

    public bool Success { get => _success; }
    public HttpStatusCode StatusCode { get => _statusCode; }
    public int? ErrorCode { get => _errorCode; }
    public string Message { get => _message; }
  }
}
