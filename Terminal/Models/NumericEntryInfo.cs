﻿namespace KieselsteinErp.Terminal.Models
{
  public class NumericEntryInfo
	{
		public NumericEntryInfo() { }
		public NumericEntryInfo(int Id, int decimalPlaces, decimal amount, decimal minValue, decimal maxValue, string unit, string placeholder, string labelText, double relativeLabelWidth)
		{
			DecimalPlaces = decimalPlaces;
			Amount = amount;
			MinValue = minValue;
			MaxValue = maxValue;
			Unit = unit;
			Placeholder = placeholder;
			LabelText = labelText;
			RelativeLabelWidth = relativeLabelWidth;
		}
		public int Id { get; set; }
		public int DecimalPlaces { get; set; }
		public decimal Amount { get; set; }
		public decimal MinValue { get; set; }
		public decimal MaxValue { get; set; }
		public string Unit { get; set; }
		public string Placeholder { get; set; }
		public string LabelText { get; set; }
		public double RelativeLabelWidth { get; set; }
	}
}
