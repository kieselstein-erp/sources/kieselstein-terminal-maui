﻿namespace KieselsteinErp.Terminal.Models
{
    public class StaffEntry : BaseEntryId
    {
        public string personalNr { get; set; }
        public string identityCnr { get; set; }
        public string shortMark { get; set; }
        public string name { get; set; }
        public string firstName { get; set; }

        public string ToName()
        {
            if (firstName == null && name == null)
                return string.Empty;
            else
                if (firstName == null)
                return name;
            else
                return firstName + " " + name;
        }
    }
}
