﻿using Kieselstein.Models;
using System.Collections.ObjectModel;

namespace KieselsteinErp.Terminal.Models
{
  public class StockStockPlaceEntry
  {
    public StockStockPlaceEntry(int stockId, string stockName, StockPlaceEntry stockPlaceEntry) 
    { 
      StockId = stockId;
      StockName = stockName;
      StockPlaceEntry = stockPlaceEntry;
      StockItems = [];
      foreach (ItemV1Entry item in stockPlaceEntry.items.entries)
        StockItems.Add(new StockItem(stockId, stockPlaceEntry.id, item));
    }

    public int StockId { get; set; }
    public string StockName { get; set; }
    public StockPlaceEntry StockPlaceEntry { get; set; }
    public ObservableCollection<StockItem> StockItems { get; set; }
  }

  public class StockItem
  {
    public StockItem(int stockId, int stockPlaceId, ItemV1Entry itemV1Entry)
    {
      ItemStockId = stockId;
      ItemStockPlaceId = stockPlaceId;
      ItemV1Entry = itemV1Entry;
    }

    public ItemV1Entry ItemV1Entry { get; set; }
    public int ItemStockId { get; set; }
    public int ItemStockPlaceId { get; set; }
  }
}
