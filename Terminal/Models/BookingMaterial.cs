﻿using CommunityToolkit.Mvvm.ComponentModel;
using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Models
{
  public partial class BookingMaterial : ObservableObject
  {
    public BookingMaterial(BookingMaterial material) : this(material.Id, material.Cnr, material.Name, material.Description, material.Amount, material.AmountIssued, material.BelatedWithdrawn, material.WithdrawalAmount, material.Unit, material.SerialChargeNr, material.IdentityAmountEntries)
    {
      BookingError = material.BookingError;
      ErrorMsg = material.ErrorMsg;
    }
    public BookingMaterial(int? id, string cnr, string name, string description, decimal amount, decimal amountIssued, bool belatedWithdrawn, decimal? withdrawalAmount, string unit, SerialChargeNr serialChargeNr, List<IdentityAmountEntry> identityAmountEntries = null)
    {
      Id = id;
      Cnr = cnr;
      Name = name;
      Description = description;
      Amount = amount;
      AmountIssued = amountIssued;
      BelatedWithdrawn = belatedWithdrawn;
      WithdrawalAmount = withdrawalAmount;
      Unit = unit;
      SerialChargeNr = serialChargeNr;
      IdentityAmountEntries = identityAmountEntries;
    }
    public BookingMaterial(BookingMaterial material, bool bookingError, string errorMsg)
        : this(material.Id, material.Cnr, material.Name, material.Description, material.Amount, material.AmountIssued, material.BelatedWithdrawn, material.WithdrawalAmount, material.Unit, material.SerialChargeNr, material.IdentityAmountEntries)
    {
      BookingError = bookingError;
      ErrorMsg = errorMsg;
    }
    public int? Id { get; set; }
    public string Cnr { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public decimal Amount { get; set; }
    public decimal AmountIssued { get; set; }
    public bool BelatedWithdrawn { get; set; }
    [ObservableProperty]
    private decimal? withdrawalAmount;
    public bool BookingError { get; set; }
    [ObservableProperty]
    private string errorMsg;
    public string Unit { get; set; }
    private SerialChargeNr serialChargeNr;
    public SerialChargeNr SerialChargeNr { get => serialChargeNr; set => SetProperty<SerialChargeNr>(ref serialChargeNr, value); }
    public List<IdentityAmountEntry> IdentityAmountEntries { get; set; }
    public string Type { get => BelatedWithdrawn ? "N" : "S"; }
  }
}
