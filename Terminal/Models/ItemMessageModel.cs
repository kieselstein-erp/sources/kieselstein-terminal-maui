﻿namespace KieselsteinErp.Terminal.Models
{
  public class ItemMessageModel
  {
    public ItemMessageModel(string itemCnr)
    {
      ItemCnr = itemCnr;
    }

    public ItemMessageModel(string itemCnr, bool isUnidentified) : this(itemCnr)
    {
      IsUnidentified = isUnidentified;
    }

    public ItemMessageModel(string itemCnr, string batchNr) : this(itemCnr)
    {
      BatchNr = batchNr;
    }

    public ItemMessageModel(string itemCnr, string batchNr, decimal amount) : this(itemCnr, batchNr)
    {
      Amount = amount;
      HasAmount = true;
    }

    public string ItemCnr { get; private set; }
    public string BatchNr { get; private set; }
    public decimal Amount { get; private set; }
    public bool HasAmount { get; private set; }
    public bool IsUnidentified { get; private set; }
  }
}
