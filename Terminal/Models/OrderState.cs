﻿namespace KieselsteinErp.Terminal.Models
{
	public enum OrderState
	{
		Angelegt,
		Erledigt,
		Offen,
		Storniert,
		Teilerledigt,
		NOTFOUND
	}
}
