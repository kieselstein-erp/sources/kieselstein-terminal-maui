﻿using CommunityToolkit.Mvvm.ComponentModel;
using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Models
{
  public partial class OpenWorkEntryWithMachines : ObservableObject
  {
    [ObservableProperty]
    private OpenWorkEntry _openWorkEntry;
    [ObservableProperty]
    private List<MachineEntry> _machines;
    [ObservableProperty]
    private int _selectedMachineIndex = -1;
    public string WorkstepNumberFormatted => OpenWorkEntry.subWorkstepNumber == null
          ? OpenWorkEntry.workNumber.ToString("D")
          : OpenWorkEntry.workNumber.ToString("D") + "." + ((int)OpenWorkEntry.subWorkstepNumber).ToString("D");
  }
}
