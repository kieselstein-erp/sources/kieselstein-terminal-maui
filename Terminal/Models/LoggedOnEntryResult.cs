﻿using Kieselstein.Models;
using System.Net;

namespace KieselsteinErp.Terminal.Models
{
	public class LoggedOnEntryResult
	{

		private LoggedOnEntry _loggedOnEntry;
		private string _errorMsg;
		private HttpStatusCode _httpStatusCode;

		public LoggedOnEntryResult() { }

		public LoggedOnEntryResult(LoggedOnEntry loggedOnEntry)
		{
			_loggedOnEntry = loggedOnEntry;
		}

		public LoggedOnEntry loggedOnEntry { get => _loggedOnEntry; set => _loggedOnEntry = value; }
		public string errorMsg { get => _errorMsg; set => _errorMsg = value; }
		public HttpStatusCode httpStatusCode { get => _httpStatusCode; set => _httpStatusCode = value; }
	}
}
