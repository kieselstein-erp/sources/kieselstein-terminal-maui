﻿using Kieselstein.Models;
using System.Net;

namespace KieselsteinErp.Terminal.Models
{
  public class TimeBalanceResult
  {
    private readonly bool _success;
    private readonly string _message;
    private readonly HttpStatusCode _statusCode;
    private readonly TimeBalanceEntry _timeBalanceEntry;

    public TimeBalanceResult(bool success, string message, HttpStatusCode httpStatusCode)
    {
      _success = success;
      _message = message;
      _statusCode = httpStatusCode;
    }

    public TimeBalanceResult(TimeBalanceEntry timeBalanceEntry)
    {
      _success = true;
      _timeBalanceEntry = timeBalanceEntry;
    }

    public string Message { get => _message; }
    public bool Success { get => _success; }
    public HttpStatusCode StatusCode { get => _statusCode; }
    public TimeBalanceEntry TimebalanceEntry { get => _timeBalanceEntry; }
  }
}
