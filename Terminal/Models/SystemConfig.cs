﻿namespace KieselsteinErp.Terminal.Models
{
  public class SystemConfig
  {
    public List<ItemButton> AdditionalBookingButtons { get; set; }
    public List<StorageLocation> StorageLocations { get; set; }
  }
}
