﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Models
{
  public class BookMaterialToLotMessageModel
	{
		public BookMaterialToLotMessageModel(ProductionEntry productionEntry, ProductionTargetMaterialEntryList materialEntryList, string batchnr)
		{
			ProductionEntry = productionEntry;
			MaterialEntryList = materialEntryList;
			Batchnr = batchnr;
		}

		public ProductionEntry ProductionEntry { get; set; }

		public ProductionTargetMaterialEntryList MaterialEntryList { get; set; }

		public string Batchnr { get; set; }
	}
}
