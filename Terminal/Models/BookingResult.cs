﻿using System.Net;

namespace KieselsteinErp.Terminal.Models
{
  public class BookingResult
  {
    private bool _success;
    private string _message;
    private HttpStatusCode _statusCode;
    private int? _resultId;

    public BookingResult() { }

    public BookingResult(bool success, string errorMsg)
    {
      _success = success;
      _message = errorMsg;
    }

    public BookingResult(bool success, int id)
    {
      _success = success;
      _resultId = id;
    }

    public BookingResult(bool success, string message, bool printRequested, bool printSuccess) : this(success, message)
    {
      PrintRequested = printRequested;
      PrintSuccess = printSuccess;
    }

    public BookingResult(bool success, string message, HttpStatusCode statusCode) : this(success, message)
    {
      _statusCode = statusCode;
    }

    public bool PrintRequested { get; set; }
    public bool PrintSuccess { get; set; }
    public string Message { get => _message; set => _message = value; }
    public bool Success { get => _success; set => _success = value; }
    public HttpStatusCode StatusCode { get => _statusCode; set => _statusCode = value; }
    public int? ResultId { get => _resultId; }
  }
}
