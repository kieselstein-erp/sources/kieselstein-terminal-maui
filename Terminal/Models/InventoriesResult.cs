﻿using Kieselstein.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KieselsteinErp.Terminal.Models
{
  public class InventoriesResult
  {
    private bool _success;
    private string _message;
    private List<InventoryEntry> _entries;

    public InventoriesResult() { }

    public InventoriesResult(bool success, string errorMsg)
    {
      _success = success;
      _message = errorMsg;
    }

    public InventoriesResult(bool success, List<InventoryEntry> entries)
    {
      _success = success;
      _entries = entries;
    }

    public string Message { get => _message; set => _message = value; }
    public bool Success { get => _success; set => _success = value; }
    public List<InventoryEntry > Entries { get => _entries; set => _entries = value; }

  }
}
