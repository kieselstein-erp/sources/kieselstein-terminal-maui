﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Models
{
  public class Inventory
  {
    public Inventory(InventoryEntry entry)
    {
      Id = entry.id;
      Name = entry.name;
      StockId = entry.stockId;
      Stockname = entry.stockEntry.name;
      StockType = entry.stockEntry.typeCnr;
    }

    public int Id { get; set; }
    public string Name { get; set; }
    public int StockId { get; set; }
    public string Stockname { get; set; }
    public string StockType { get; set; }

  }
}
