﻿namespace KieselsteinErp.Terminal.Models;

public enum LotState
{
	NEW,
	EDITED,
	INPRODUCTION,
	PARTIALLYDONE,
	DONE,
	CANCELLED,
	STOPPED,
	NOTFOUND
}