﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Models
{
  public record PopupDeliveryResult
  {
    public PopupDeliveryResult(bool canceled, List<IdentityAmountEntry> entries, decimal badAmount, bool boolValue)
    {
      Canceled = canceled;
      Entries = entries;
      BadAmount = badAmount;
      BoolValue = boolValue;
    }
    public bool Canceled { get; }
    public bool BoolValue { get; }
    public List<IdentityAmountEntry> Entries { get; }
    public decimal BadAmount { get; }
  }
}
