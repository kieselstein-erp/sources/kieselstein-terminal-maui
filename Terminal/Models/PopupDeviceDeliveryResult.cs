﻿using KieselsteinErp.Terminal.Kieselstein.Models;

namespace KieselsteinErp.Terminal.Models
{
  public record PopupDeviceDeliveryResult
  {
    public PopupDeviceDeliveryResult(bool canceled, string serialNumber, List<ItemIdentityAmountEntry> entries, bool boolValue)
    {
      Canceled = canceled;
      SerialNumber = serialNumber;
      Entries = entries;
      BoolValue = boolValue;
    }
    public bool Canceled { get; }
    public bool BoolValue { get; }
    public string SerialNumber { get; }
    public List<ItemIdentityAmountEntry> Entries { get; }
  }
}
