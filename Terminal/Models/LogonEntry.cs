﻿namespace KieselsteinErp.Terminal.Models
{
    public class LogonEntry
    {
        private string _username;
        private string _password;
        private string _client;
        private string _localeString;

        public LogonEntry() { }

        public LogonEntry(string username, string password, string client, string localeString)
        {
            Username = username;
            Password = password;
            Client = client;
            LocaleString = localeString;
        }

        public string Username { get => _username; set => _username = value; }
        public string Password { get => _password; set => _password = value; }
        public string Client { get => _client; set => _client = value; }
        public string LocaleString { get => _localeString; set => _localeString = value; }
    }
}
