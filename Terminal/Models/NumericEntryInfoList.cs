﻿namespace KieselsteinErp.Terminal.Models
{
  public class NumericEntryInfoList
  {
    public NumericEntryInfoList(List<int> decimalPlaces, List<decimal> amount, List<decimal> minValue, List<decimal> maxValue, List<string> unit, List<string> placeholder, List<string> labelText, int entryCount, double relativeLabelWidth)
    {
      Infos = new List<NumericEntryInfo>();
      NumericEntryInfo info;
      for (int i = 0; i < entryCount; i++)
      {
        info = new NumericEntryInfo();
        info.Id = i;
        info.DecimalPlaces = decimalPlaces[i];
        info.Amount = amount[i];
        info.MinValue = minValue[i];
        info.MaxValue = maxValue[i];
        info.Unit = unit[i];
        info.LabelText = labelText[i];
        info.Placeholder = placeholder[i];
        info.RelativeLabelWidth = relativeLabelWidth;
        Infos.Add(info);
      }
    }
    public NumericEntryInfoList(int decimalPlaces, decimal amount, decimal minValue, decimal maxValue, string unit, List<string> placeholder, List<string> labelText, int entryCount)
    {
      Infos = new List<NumericEntryInfo>();
      NumericEntryInfo info;
      for (int i = 0; i < entryCount; i++)
      {
        info = new NumericEntryInfo() { Id = i, DecimalPlaces = decimalPlaces, Amount = amount, MinValue = minValue, MaxValue = maxValue, Unit = unit };
        info.LabelText = labelText[i];
        info.Placeholder = placeholder[i];
        Infos.Add(info);
      }
    }
    public NumericEntryInfoList(int decimalPlaces, decimal amount, decimal minValue, decimal maxValue, string unit, List<string> placeholder, List<string> labelText, int entryCount, double relativeLabelWidth) : this(decimalPlaces, amount, minValue, maxValue, unit, placeholder, labelText, entryCount)
    {
      foreach (var info in Infos)
        info.RelativeLabelWidth = relativeLabelWidth;
    }

    public List<NumericEntryInfo> Infos { get; set; }
  }
}
