﻿using Kieselstein.Models;
using System.Net;

namespace KieselsteinErp.Terminal.Models
{
  public class CommentMediaListResult
  {
    private readonly bool _success;
    private readonly string _message;
    private readonly HttpStatusCode _statusCode;
    private readonly List<ItemCommentMediaInfoEntry> _entries;

    public CommentMediaListResult(bool success, string message, HttpStatusCode httpStatusCode)
    {
      _success = success;
      _message = message;
      _statusCode = httpStatusCode;
    }

    public CommentMediaListResult(ItemCommentMediaInfoEntryList itemCommentMediaInfoEntryList)
    {
      _success = true;
      _entries = itemCommentMediaInfoEntryList.entries;
    }

    public string Message { get => _message; }
    public bool Success { get => _success; }
    public HttpStatusCode StatusCode { get => _statusCode; }
    public List<ItemCommentMediaInfoEntry> CommentMediaEntries { get => _entries; }
  }
}
