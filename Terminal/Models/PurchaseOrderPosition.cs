﻿using Kieselstein.Models;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace KieselsteinErp.Terminal.Models
{
  public class PurchaseOrderPosition : PurchaseOrderPositionEntry, INotifyPropertyChanged
  {
    public PurchaseOrderPosition(PurchaseOrderPositionEntry position)
    {
      base.id=position.id;
      base.quantity=position.quantity;
      base.openQuantity=position.openQuantity;
      base.itemEntry=position.itemEntry;
      base.itemId=position.itemId;
    }

    public event PropertyChangedEventHandler PropertyChanged;

    private void NotifyPropertyChanged([CallerMemberName] String propertyName ="")
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    public decimal OpenAmount
    {
      get { return base.openQuantity; }
      set { base.openQuantity = value; NotifyPropertyChanged(nameof(OpenAmount)); }
    }

  }
}
