﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Terminal.Models
{
    class Benutzer
    {
        public Benutzer(string Vorname, string Name, string Code)
        {
            this.Vorname=Vorname;
            this.Name=Name;
            this.Code=Code;   
        }

        public string Vorname { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

    }
}
