﻿using Kieselstein.Models;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace KieselsteinErp.Terminal.Models
{
  public class DeliverableOrderPosition : DeliverableOrderPositionEntry, INotifyPropertyChanged
  {
    public event PropertyChangedEventHandler PropertyChanged;

    public DeliverableOrderPosition(DeliverableOrderPositionEntry position)
    {
      base.id = position.id;
      base.amount = position.amount;
      base.openAmount = position.openAmount;
      base.itemCnr = position.itemCnr;
      base.itemId = position.itemId;
      base.description = position.description;
      base.unitCnr = position.unitCnr;
      base.status = position.status;
      base.itemProperty = position.itemProperty;
      base.itemIdentity = position.itemIdentity;
      base.stockinfoEntry = position.stockinfoEntry;
    }

    private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    public decimal OpenAmount
    {
      get { return base.openAmount; }
      set { base.openAmount = value; NotifyPropertyChanged(nameof(OpenAmount)); }
    }
  }
}
