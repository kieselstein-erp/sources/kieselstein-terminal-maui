﻿using Kieselstein.Models;
using System.Net;

namespace KieselsteinErp.Terminal.Models
{
  public class DocumentListResult
  {
    private readonly bool _success;
    private readonly string _message;
    private readonly HttpStatusCode _statusCode;
    private readonly List<DocumentInfoEntry> _entries;

    public DocumentListResult(bool success, string message, HttpStatusCode httpStatusCode)
    {
      _success = success;
      _message = message;
      _statusCode = httpStatusCode;
    }

    public DocumentListResult(DocumentInfoEntryList documentInfoEntryList)
    {
      _success = true;
      _entries = documentInfoEntryList.entries;
    }

    public string Message { get => _message; }
    public bool Success { get => _success; }
    public HttpStatusCode StatusCode { get => _statusCode; }
    public List<DocumentInfoEntry> DocumentInfoEntries { get => _entries; }

  }
}
