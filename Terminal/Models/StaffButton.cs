﻿namespace KieselsteinErp.Terminal.Models
{
  public class StaffButton
  {
    public string Name { get; set; }
    public string Personalnummer { get; set; }
    public string Identity { get; set; }
  }
}
