﻿using CommunityToolkit.Mvvm.ComponentModel;
using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Models
{
  public partial class DeviceAssemblyEntry : ObservableObject
  {
    public int? Id { get; set; }
    public ItemV1Entry Item { get; set; }
    public string ItemCnr { get => Item.cnr; }
    public string ItemName { get => Item.name; }

    [ObservableProperty]
    private string _serialnumber;

    public List<string> Serialnumbers { get; set; }
  }
}
