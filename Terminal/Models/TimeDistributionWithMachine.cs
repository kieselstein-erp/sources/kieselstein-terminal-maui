﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Models
{
  public class TimeDistributionWithMachine
  {
    private TimeDistributionEntry _timeDistributionEntry;
    private MachineEntry _machineEntry;
    private Color _foreColor = Colors.Black;
    
    public TimeDistributionWithMachine(TimeDistributionEntry timeDistributionEntry, MachineEntry machineEntry)
    {
      _timeDistributionEntry = timeDistributionEntry;
      _machineEntry = machineEntry;
    }

    public TimeDistributionEntry TimeDistributionEntry { get { return _timeDistributionEntry; } }
    public MachineEntry MachineEntry { get { return _machineEntry; } }
    public Color ForeColor { get { return _foreColor; } set { _foreColor = value; } }
    public bool IsDateTimeVisible { get { return _timeDistributionEntry.tTimeMs != 0; } }
    public string WorkstepNumberFormatted => _timeDistributionEntry.productionWorkstepEntry.subWorkstepNumber == null
          ? _timeDistributionEntry.productionWorkstepEntry.workstepNumber.ToString("D")
          : _timeDistributionEntry.productionWorkstepEntry.workstepNumber.ToString("D") + "." + ((int)_timeDistributionEntry.productionWorkstepEntry.subWorkstepNumber).ToString("D");
  }
}
