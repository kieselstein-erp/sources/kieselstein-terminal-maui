﻿namespace KieselsteinErp.Terminal.Models
{
	public record PopupListResult
	{
		public PopupListResult(bool canceled, List<decimal> entries, bool boolValue)
		{
			Canceled = canceled;
			Entries = entries;
			BoolValue = boolValue;
		}
		public bool Canceled { get; }
		public bool BoolValue { get; }
		public List<decimal> Entries { get; }
	}
}
