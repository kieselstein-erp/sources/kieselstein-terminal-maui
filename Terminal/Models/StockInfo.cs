﻿using CommunityToolkit.Mvvm.ComponentModel;
using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Models
{
  public partial class StockInfo : ObservableObject
  {
    private readonly ItemV1Entry _item;
    private readonly List<StockPlaceEntry> _places;
    private readonly StockAmountEntry _stockAmountEntry;

    public StockInfo(ItemV1Entry item, StockAmountEntry stockAmountEntry, List<StockPlaceEntry> places)
    {
      _item = item;
      _stockAmountEntry = stockAmountEntry;
      _places = places;
      UpdateStockPlacesAsString();
      this.PropertyChanged += StockInfo_PropertyChanged;
    }

    private void StockInfo_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
      if (e.PropertyName ==  nameof(StockPlaces))
        UpdateStockPlacesAsString();
    }

    public string StockName { get => _stockAmountEntry.stock.name; }
    public int StockId { get => _stockAmountEntry.stock.id; }
    public string StockType { get => _stockAmountEntry.stock.typeCnr; }
    public string UnitCnr { get => _item.unitCnr; }

    [ObservableProperty]
    private string _stockPlacesAsString;

    private void UpdateStockPlacesAsString()
    {
      string stockPlaces = string.Empty;
      if (_places != null)
      {
        foreach (var place in _places)
        {
          stockPlaces += place.name;
          stockPlaces += ", ";
        }
      }
      if (stockPlaces.EndsWith(", "))
        stockPlaces = stockPlaces.Substring(0, stockPlaces.Length - 2);
      StockPlacesAsString = stockPlaces;
    }

    public string BatchNumbersAsString
    {
      get
      {
        string batchNumbers = string.Empty;
        if (_stockAmountEntry !=null && _stockAmountEntry.itemIdentityList != null) 
        foreach (var batch in _stockAmountEntry.itemIdentityList.entries)
        {
          batchNumbers += batch.identity;
          batchNumbers += " ";
        }
        return batchNumbers;
      }
    }

    public decimal Amount { get => _stockAmountEntry.amount; }
    public List<ItemIdentityEntry> Identities { get => _stockAmountEntry.itemIdentityList.entries; }

    public List<StockPlaceEntry> StockPlaces { get => _places; }

    public void RemoveStockPlace(StockPlaceEntry entry)
    {
      if (StockPlaces != null && StockPlaces.Contains(entry))
      {
        StockPlaces.Remove(entry);
        UpdateStockPlacesAsString();
      }
    }

    public void AddStockPlace(StockPlaceEntry entry)
    {
      if (StockPlaces != null)
      {
        StockPlaces.Add(entry);
        UpdateStockPlacesAsString();
      }
    }
  }
}
