﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Services.Worktime;

namespace KieselsteinErp.Terminal.Models
{
  public record PopupRequestsResult
    {
    
    public PopupRequestsResult(bool canceled)
    {
      if (!canceled)
        throw new ArgumentException("Only canceled = true allowed");
      Canceled = canceled;
    }


    public PopupRequestsResult(bool canceled, SpecialTimeType type, SpecialTimesEntry entry)
    {
      Canceled = canceled;
      Type = type;
      Entry = entry;
    }
    public bool Canceled { get; }
    public SpecialTimeType Type { get; }
    public SpecialTimesEntry Entry { get; }
  }
}
