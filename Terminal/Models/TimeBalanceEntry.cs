using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TimeBalanceEntry
    {
        public double timeBalance { get; set; }

        public double availableVacation { get; set; }

        public string unitVacation { get; set; }
    }
}