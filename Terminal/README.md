# Datalogic Memor 1 Anbindung

## Memor 1
 is an Android Device with 2-D Scanengine and Wi-Fi (Android 8.1)

    Note: Last Updated Android security patches level at October, 2020.

## Memor 10
 a newer one Device with Andoid 10
    
    Note: is actual active supported

## Memor 11 / 20
newer Device with Andoid 11

    Note: is actual active supported

## API

An API for this device is available [MAUI / Xamarin SDK](https://datalogic.github.io/xamarin/overview/)

How to receive barcode data in app? [FAQ](https://datalogic.github.io/xamarin/faq)


## Architectur draft

![architecture](./images/architecture-draft.drawio.svg)

