﻿using System.Diagnostics;

namespace KieselsteinErp.Terminal.Controls
{
  public class ViewCellEx : ViewCell
  {
    protected override void OnBindingContextChanged()
    {
      if (BindingContext == null || Parent?.BindingContext == null || BindingContext != Parent.BindingContext)
      {
        Debug.Print($"Set Context to {BindingContext?.GetType().Name}");
        base.OnBindingContextChanged();
      }
      else
        Debug.Print($"Ignore Context {BindingContext?.GetType().Name}");
    }
  }
}
