﻿using System.ComponentModel;
using System.Diagnostics;
using Microsoft.Extensions.Logging;

namespace KieselsteinErp.Terminal.Controls
{
  public class ZoomableImage : ContentView
  {
    private double _currentScale = 1;
    private double _startScale = 1;
    private double _xOffset = 0;
    private double _yOffset = 0;
    private bool _secondDoubleTapp = false;
    private double _xOffsetCorrection = 0;
    private double _yOffsetCorrection = 0;

    public ZoomableImage()
    {
      var pinchGesture = new PinchGestureRecognizer();
      pinchGesture.PinchUpdated += PinchUpdated;
      GestureRecognizers.Add(pinchGesture);

      var panGesture = new PanGestureRecognizer();
      panGesture.PanUpdated += OnPanUpdated;
      GestureRecognizers.Add(panGesture);

      var tapGesture = new TapGestureRecognizer { NumberOfTapsRequired = 2 };
      tapGesture.Tapped += DoubleTapped;
      GestureRecognizers.Add(tapGesture);

    }

    protected override void OnChildAdded(Element child)
    {
      base.OnChildAdded(child);
      if (child is Image)
        child.PropertyChanged += OnPropertyChangedChild;
    }

    protected override void OnChildRemoved(Element child, int oldLogicalIndex)
    {
      base.OnChildRemoved(child, oldLogicalIndex);
      if (child is Image)
        child.PropertyChanged -= OnPropertyChangedChild;
    }

    private void OnPropertyChangedChild(object sender, PropertyChangedEventArgs e)
    {
      Debug.Print($"PropertyChangedChild {e.PropertyName}");
      if (e.PropertyName == "Source")
        ResetLayout();
    }

    private void ResetLayout()
    {
      _currentScale = 1;
      _startScale = 1;
      _xOffset = 0;
      _yOffset = 0;
      _xOffsetCorrection = 0;
      _yOffsetCorrection = 0;
      Content.Scale = _currentScale;
      Content.TranslationX = _xOffset;
      Content.TranslationY = _yOffset;
      _secondDoubleTapp = false;
      var parent = this.Parent;
      if (parent is ScrollView)
      {
        ScrollView scrollView = parent as ScrollView;
        if (scrollView.ScrollY != 0)
          scrollView.ScrollToAsync(0, 0, false);
      }
    }

    private void PinchUpdated(object sender, PinchGestureUpdatedEventArgs e)
    {
      switch (e.Status)
      {
        case GestureStatus.Started:
          _startScale = Content.Scale;
          Content.AnchorX = 0;
          Content.AnchorY = 0;
          break;
        case GestureStatus.Running:
          {
            _currentScale += (e.Scale - 1) * _startScale;
            _currentScale = Math.Max(1, _currentScale);

            var renderedX = Content.X + _xOffset;
            var deltaX = renderedX / Width;
            var deltaWidth = Width / (Content.Width * _startScale);
            var originX = (e.ScaleOrigin.X - deltaX) * deltaWidth;

            var renderedY = Content.Y + _yOffset;
            var deltaY = renderedY / Height;
            var deltaHeight = Height / (Content.Height * _startScale);
            var originY = (e.ScaleOrigin.Y - deltaY) * deltaHeight;

            var targetX = _xOffset - (originX * Content.Width) * (_currentScale - _startScale);
            var targetY = _yOffset - (originY * Content.Height) * (_currentScale - _startScale);

            Content.TranslationX = Math.Min(0, Math.Max(targetX, -Content.Width * (_currentScale - 1)));
            Content.TranslationY = Math.Min(0, Math.Max(targetY, -Content.Height * (_currentScale - 1)));

            Content.Scale = _currentScale;
            break;
          }
        case GestureStatus.Completed:
          _xOffset = Content.TranslationX;
          _yOffset = Content.TranslationY;
          break;
      }
    }

    private double _actualY;
    private double _actualScrolledY;
    public void OnPanUpdated(object sender, PanUpdatedEventArgs e)
    {
      if (Content.Scale == 1)
      {
        var parent = this.Parent;
        if (parent is ScrollView)
        {
          ScrollView scrollView = parent as ScrollView;
          switch (e.StatusType)
          {
            case GestureStatus.Started:
              _actualY = e.TotalY;
              _actualScrolledY = scrollView.ScrollY;
              break;
            case GestureStatus.Running:
              double delta = (e.TotalY - _actualY);
              Debug.Print($"delta:{delta} new:{_actualY + delta} actY: {e.TotalY}");
              scrollView.ScrollToAsync(e.TotalX, _actualScrolledY - e.TotalY, true);
              _actualY = e.TotalY;
              break;
            case GestureStatus.Completed:
              _actualScrolledY = scrollView.ScrollY;
              break;
          }
        }
        return;
      }

      switch (e.StatusType)
      {
        case GestureStatus.Running:

          var newX = (e.TotalX * Scale) + _xOffset - _xOffsetCorrection;
          var newY = (e.TotalY * Scale) + _yOffset - _yOffsetCorrection;

          var width = (Content.Width * Content.Scale);
          var height = (Content.Height * Content.Scale);

          var canMoveX = width > Application.Current.MainPage.Width;
          var canMoveY = height > Application.Current.MainPage.Height;

          if (canMoveX)
          {
            var minX = (width - (Application.Current.MainPage.Width / 2)) * -1;
            var maxX = Math.Min(Application.Current.MainPage.Width / 2, width / 2);

            if (newX < minX)
            {
              newX = minX;
            }

            if (newX > maxX)
            {
              newX = maxX;
            }
          }
          else
          {
            newX = 0;
          }

          if (canMoveY)
          {
            var minY = (height - (Application.Current.MainPage.Height / 2)) * -1;
            var maxY = Math.Min(Application.Current.MainPage.Width / 2, height / 2);

            if (newY < minY)
            {
              newY = minY;
            }

            if (newY > maxY)
            {
              newY = maxY;
            }
          }
          else
          {
            newY = 0;
          }
          if (_xOffsetCorrection == 0 & _yOffsetCorrection == 0)
          {
            _xOffsetCorrection = newX - _xOffset;
            _yOffsetCorrection = newY - _yOffset;
            Content.TranslationX = newX - _xOffsetCorrection;
            Content.TranslationY = newY - _yOffsetCorrection;
            //Console.WriteLine(string.Format("OnPanUpdated Running: _xOffsetCorrection:{0} _yOffsetCorrection:{1}", _xOffsetCorrection, _yOffsetCorrection));
            //Console.WriteLine(string.Format("OnPanUpdated Running: X:{0} Y:{1}", Content.TranslationX, Content.TranslationY));
          }
          else
          {
            Content.TranslationX = newX;
            Content.TranslationY = newY;
            //Console.WriteLine(string.Format("OnPanUpdated Running: X:{0} Y:{1}", Content.TranslationX, Content.TranslationY));
          }
          break;
        case GestureStatus.Completed:
          _xOffset = Content.TranslationX;
          _yOffset = Content.TranslationY;
          _currentScale = Content.Scale;
          _xOffsetCorrection = 0;
          _yOffsetCorrection = 0;
          break;
        case GestureStatus.Started:
          break;
        case GestureStatus.Canceled:
          break;
        default:
          throw new ArgumentOutOfRangeException();
      }
    }

    public async void DoubleTapped(object sender, EventArgs e)
    {
      try
      {
        var multiplicator = Math.Pow(2, 1.0 / 10.0);
        _startScale = Content.Scale;
        Content.AnchorX = 0;
        Content.AnchorY = 0;

        for (var i = 0; i < 10; i++)
        {
          if (!_secondDoubleTapp) //if it's not the second double tapp we enlarge the scale
          {
            _currentScale *= multiplicator;
          }
          else //if it's the second double tap we make the scale smaller again 
          {
            _currentScale /= multiplicator;
          }

          var renderedX = Content.X + _xOffset;
          var deltaX = renderedX / Width;
          var deltaWidth = Width / (Content.Width * _startScale);
          var originX = (0.5 - deltaX) * deltaWidth;

          var renderedY = Content.Y + _yOffset;
          var deltaY = renderedY / Height;
          var deltaHeight = Height / (Content.Height * _startScale);
          var originY = (0.5 - deltaY) * deltaHeight;

          var targetX = _xOffset - (originX * Content.Width) * (_currentScale - _startScale);
          var targetY = _yOffset - (originY * Content.Height) * (_currentScale - _startScale);

          Content.TranslationX = Math.Min(0, Math.Max(targetX, -Content.Width * (_currentScale - 1)));
          Content.TranslationY = Math.Min(0, Math.Max(targetY, -Content.Height * (_currentScale - 1)));

          Content.Scale = _currentScale;
          await Task.Delay(10).ConfigureAwait(true);
        }
        _secondDoubleTapp = !_secondDoubleTapp;
        _xOffset = Content.TranslationX;
        _yOffset = Content.TranslationY;
      }
      catch (Exception ex)
      {
        var loggerFactory = Application.Current.MainPage.Handler.MauiContext.Services.GetService<ILoggerFactory>();
        ILogger logger = loggerFactory?.CreateLogger<ZoomableImage>();
        logger?.LogWarning(ex.Message);
        logger?.LogTrace(ex.StackTrace);
      }
    }
  }
}
