﻿using System.Globalization;
using KieselsteinErp.Terminal.Util;

namespace KieselsteinErp.Terminal.Controls
{
  public class NumericEntry : ContentView, ITextInput
  {
    private TouchEntry entry;
    private Label label;
    private Label unitLabel;

    #region "bindable properties"

    public static readonly BindableProperty BackgroundColorWhenValidProperty = BindableProperty.Create(
                nameof(BackgroundColorWhenValid), typeof(Color),
                typeof(NumericEntry), defaultValue: Colors.Transparent,
                propertyChanged: OnBackgroundColorWhenValidPropertyChanged);

    private static void OnBackgroundColorWhenValidPropertyChanged(BindableObject bindable, object oldValue, object newValue)
    {
      if (bindable is NumericEntry numericEntry)
        numericEntry.BackgroundColorWhenValid = (Color)newValue;
    }
    public Color BackgroundColorWhenValid { get; set; }

    public static readonly BindableProperty BackgroundColorWhenInvalidProperty = BindableProperty.Create(
                nameof(BackgroundColorWhenInvalid), typeof(Color),
                typeof(NumericEntry), defaultValue: Colors.LightSalmon,
                propertyChanged: OnBackgroundColorWhenInvalidPropertyChanged);

    private static void OnBackgroundColorWhenInvalidPropertyChanged(BindableObject bindable, object oldValue, object newValue)
    {
      if (bindable is NumericEntry numericEntry)
        numericEntry.BackgroundColorWhenInvalid = (Color)newValue;
    }
    public Color BackgroundColorWhenInvalid { get; set; }

    //---------------------------------------------------------------------------------------------------------//
    //Since NumericEntry is a ContentView it does not have the properties of a Entry. So they need to be added.//
    //Completed
    public static readonly BindableProperty CompletedCommandProperty =
                    BindableProperty.Create(nameof(CompletedCommand), typeof(Command), typeof(NumericEntry));

    public Command CompletedCommand
    {
      get { return (Command)GetValue(CompletedCommandProperty); }
      set { SetValue(CompletedCommandProperty, value); }
    }

    //Completed Event
    public event EventHandler Completed;
    
    //LabelWidth
    public static readonly BindableProperty RelativeLabelWidthProperty = BindableProperty.Create(
            nameof(RelativeLabelWidth), typeof(double),
            typeof(NumericEntry), defaultValue: (double)0,
            propertyChanged: OnRelativeLabelWidthPropertyChanged);

    public double RelativeLabelWidth
    {
      get => (double)GetValue(RelativeLabelWidthProperty);
      set => SetValue(RelativeLabelWidthProperty, value);
    }
    private static void OnRelativeLabelWidthPropertyChanged(BindableObject bindable, object oldValue, object newValue)
    {
      if (bindable is NumericEntry numericEntry)
      {
        ((Grid)numericEntry.Content).ColumnDefinitions[0].Width = new GridLength((double)newValue, GridUnitType.Star);
        ((Grid)numericEntry.Content).ColumnDefinitions[1].Width = new GridLength(1 - (double)newValue, GridUnitType.Star);
      }
    }
    //LabelText
    public static readonly BindableProperty LabelTextProperty = BindableProperty.Create(
        nameof(LabelText), typeof(string),
        typeof(NumericEntry), defaultValue: null,
        propertyChanged: OnLabelTextPropertyChanged);

    public string LabelText
    {
      get => (string)GetValue(LabelTextProperty);
      set => SetValue(LabelTextProperty, value);
    }
    private static void OnLabelTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
    {
      if (bindable is NumericEntry numericEntry)
      {
        numericEntry.label.Text = (string)newValue;
      }
    }
    //Unit
    public static readonly BindableProperty UnitProperty = BindableProperty.Create(
    nameof(Unit), typeof(string),
    typeof(NumericEntry), defaultValue: string.Empty,
    propertyChanged: OnUnitPropertyChanged);

    public string Unit
    {
      get => (string)GetValue(UnitProperty);
      set => SetValue(UnitProperty, value);
    }

    private static void OnUnitPropertyChanged(BindableObject bindable, object oldValue, object newValue)
    {
      if (bindable is NumericEntry numericEntry)
        numericEntry.unitLabel.Text = (string)newValue;
    }
    //MinValue
    public static readonly BindableProperty MinValueProperty = BindableProperty.Create(
    nameof(MinValue), typeof(decimal),
    typeof(NumericEntry), defaultValue: 0m);

    public decimal MinValue
    {
      get => (decimal)GetValue(MinValueProperty);
      set => SetValue(MinValueProperty, value);
    }
    //MaxValue
    public static readonly BindableProperty MaxValueProperty = BindableProperty.Create(
            nameof(MaxValue), typeof(decimal),
            typeof(NumericEntry), defaultValue: 99999999m);

    public decimal MaxValue
    {
      get => (decimal)GetValue(MaxValueProperty);
      set => SetValue(MaxValueProperty, value);
    }
    //DecimalPlaces
    public static readonly BindableProperty DecimalPlacesProperty = BindableProperty.Create(
        nameof(DecimalPlaces), typeof(int),
        typeof(NumericEntry), defaultValue: 0);

    public int DecimalPlaces
    {
      get => (int)GetValue(DecimalPlacesProperty);
      set => SetValue(DecimalPlacesProperty, value);
    }
    //Text
    public static readonly BindableProperty TextProperty = BindableProperty.Create(
                    nameof(Text), typeof(string),
                    typeof(NumericEntry), default(string),
                    BindingMode.TwoWay);
    public string Text
    {
      get => (string)GetValue(TextProperty);
      set => SetValue(TextProperty, value);
    }
    //FontSize
    public static readonly BindableProperty FontSizeProperty = BindableProperty.Create(
                    nameof(FontSize),
                    typeof(double),
                    typeof(NumericEntry),
                    propertyChanged: OnFontSizePropertyChanged);

    public double FontSize
    {
      get => (double)GetValue(FontSizeProperty);
      set => SetValue(FontSizeProperty, value);
    }
    private static void OnFontSizePropertyChanged(BindableObject bindable, object oldValue, object newValue)
    {
      if (bindable is NumericEntry numericEntry && numericEntry.entry != null)
      {
        numericEntry.entry.FontSize = (double)newValue;
        numericEntry.unitLabel.FontSize = (double)newValue;
        numericEntry.label.FontSize = (double)newValue;
      }
    }

    //VerticalTextAlignment
    public static readonly BindableProperty VerticalTextAlignmentProperty = BindableProperty.Create(
                    nameof(VerticalTextAlignment),
                    typeof(TextAlignment),
                    typeof(NumericEntry),
                    defaultValue: TextAlignment.Start,
                    propertyChanged: OnVerticalTextAlignmentPropertyChanged);

    public TextAlignment VerticalTextAlignment
    {
      get => (TextAlignment)GetValue(VerticalTextAlignmentProperty);
      set => SetValue(VerticalTextAlignmentProperty, value);
    }

    private static void OnVerticalTextAlignmentPropertyChanged(BindableObject bindable, object oldValue, object newValue)
    {
      if (bindable is NumericEntry numericEntry && numericEntry.entry != null)
      {
        numericEntry.entry.VerticalTextAlignment = (TextAlignment)newValue;
      }
    }

    //Placeholder
    public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create(
                    nameof(Placeholder),
                    typeof(string),
                    typeof(NumericEntry),
                    defaultValue: null,
                    propertyChanged: OnPlaceholderPropertyChanged);

    public string Placeholder
    {
      get => (string)GetValue(PlaceholderProperty);
      set => SetValue(PlaceholderProperty, value);
    }

    private static void OnPlaceholderPropertyChanged(BindableObject bindable, object oldValue, object newValue)
    {
      if (bindable is NumericEntry numericEntry && numericEntry.entry != null)
      {
        numericEntry.entry.Placeholder = (string)newValue;
      }
    }

    //FontAttributes
    public static readonly BindableProperty FontAttributesProperty = BindableProperty.Create(
                    nameof(FontAttributes),
                    typeof(FontAttributes),
                    typeof(NumericEntry),
                    defaultValue: FontAttributes.None,
                    propertyChanged: OnFontAttributesPropertyChanged);

    public FontAttributes FontAttributes
    {
      get => (FontAttributes)GetValue(FontAttributesProperty);
      set => SetValue(FontAttributesProperty, value);
    }

    private static void OnFontAttributesPropertyChanged(BindableObject bindable, object oldValue, object newValue)
    {
      if (bindable is NumericEntry numericEntry && numericEntry.entry != null)
      {
        numericEntry.entry.FontAttributes = (FontAttributes)newValue;
        numericEntry.unitLabel.FontAttributes = (FontAttributes)newValue;
        numericEntry.label.FontAttributes = (FontAttributes)newValue;
      }
    }

    //HorizontalTextAlignment
    public static readonly BindableProperty HorizontalTextAlignmentProperty = BindableProperty.Create(
                    nameof(HorizontalTextAlignment),
                    typeof(TextAlignment),
                    typeof(NumericEntry),
                    defaultValue: TextAlignment.End,
                    propertyChanged: OnHorizontalTextAlignmentPropertyChanged);

    public TextAlignment HorizontalTextAlignment
    {
      get => (TextAlignment)GetValue(HorizontalTextAlignmentProperty);
      set => SetValue(HorizontalTextAlignmentProperty, value);
    }

    private static void OnHorizontalTextAlignmentPropertyChanged(BindableObject bindable, object oldValue, object newValue)
    {
      if (bindable is NumericEntry numericEntry)
      {
        numericEntry.HorizontalTextAlignment = (TextAlignment)newValue;
      }
    }

    //IsEnabled
#pragma warning disable CS0108 // Element blendet vererbte Element aus; fehlendes 'new'-Schlüsselwort
    public static readonly BindableProperty IsEnabledProperty = BindableProperty.Create(
#pragma warning restore CS0108 // Element blendet vererbte Element aus; fehlendes 'new'-Schlüsselwort
    nameof(IsEnabled),
    typeof(bool),
    typeof(NumericEntry),
    defaultValue: true,
    propertyChanged: OnIsEnabledPropertyChanged);

#pragma warning disable CS0108 // Element blendet vererbte Element aus; fehlendes 'new'-Schlüsselwort
    public bool IsEnabled
#pragma warning restore CS0108 // Element blendet vererbte Element aus; fehlendes 'new'-Schlüsselwort
    {
      get => (bool)GetValue(IsEnabledProperty);
      set => SetValue(IsEnabledProperty, value);
    }

    public bool IsSpellCheckEnabled => ((ITextInput)entry).IsSpellCheckEnabled;

    public bool IsTextPredictionEnabled => ((ITextInput)entry).IsTextPredictionEnabled;

    public bool IsReadOnly => ((ITextInput)entry).IsReadOnly;

    public Keyboard Keyboard => ((ITextInput)entry).Keyboard;

    public int MaxLength => ((ITextInput)entry).MaxLength;

    public int CursorPosition { get => ((ITextInput)entry).CursorPosition; set => ((ITextInput)entry).CursorPosition = value; }
    public int SelectionLength { get => ((ITextInput)entry).SelectionLength; set => ((ITextInput)entry).SelectionLength = value; }

    public Color TextColor { get => ((ITextStyle)entry).TextColor; set => entry.TextColor = value; }

    public Microsoft.Maui.Font Font => ((ITextStyle)entry).Font;

    public double CharacterSpacing => ((ITextStyle)entry).CharacterSpacing;

    public Color PlaceholderColor { get => ((IPlaceholder)entry).PlaceholderColor; set => ((IPlaceholder)entry).PlaceholderColor = value; }

    private static void OnIsEnabledPropertyChanged(BindableObject bindable, object oldValue, object newValue)
    {
      if (bindable is NumericEntry numericEntry && numericEntry.entry != null)
      {
        numericEntry.entry.IsEnabled = (bool)newValue;
      }
    }

#endregion

    //---------------------------------------------------------------------------------------------------------//

    private CultureInfo _ci;

    public NumericEntry()
    {
      _ci = CultureInfo.CurrentCulture;

      SetupLayout();
    }

    private void SetupLayout()
    {
      label = new Label();
      label.VerticalOptions = LayoutOptions.Center;
      entry = new TouchEntry
      {
        Keyboard = Keyboard.Numeric,
        ClearButtonVisibility = ClearButtonVisibility.WhileEditing
      };
      entry.SetBinding(Entry.TextProperty, new Binding(nameof(Text), source: this));

      entry.TextChanged += OnTextChanged;
      entry.Completed += EntryCompleted;
      entry.Focused += OnFocused;

      unitLabel = new Label();
      unitLabel.VerticalOptions = LayoutOptions.Center;

      Grid grid = new Grid();
      grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto }); // Column for the Label
      grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star }); // Column for the Entry
      grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto }); // Column for the UnitLabel
      grid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

      // Add the Entry and Label to the Grid
      grid.Add(label, 0, 0);
      grid.Add(entry, 1, 0);
      grid.Add(unitLabel, 2, 0);

      Content = grid;
    }

    private void OnFocused(object sender, FocusEventArgs e)
    {
      entry.Focus();
      SelectAll();
    }

    private void EntryCompleted(object sender, EventArgs e)
    {
      this.Completed?.Invoke(this,e);

      if (CompletedCommand != null && CompletedCommand.CanExecute(null))
      {
        CompletedCommand.Execute(null);
      }
    }
    private void OnTextChanged(object sender, TextChangedEventArgs e)
    {
      string newValue = e.NewTextValue;
      string oldValue = e.OldTextValue;

      if (string.IsNullOrEmpty(newValue))
      {
        SetBackgroundColor(BackgroundColorWhenInvalid);
        return;
      }

      newValue = DecimalHelper.CheckDecimalSeparator(newValue);

      if (!IsValidNumericInput(newValue))
      {
        entry.Text = oldValue;
        return;
      }

      if (decimal.TryParse(newValue, _ci, out decimal valueDecimal))
      {
        if (BackgroundColorWhenValid != null && BackgroundColorWhenInvalid != null)
        {
          if (!IsValueInRange(valueDecimal))
            SetBackgroundColor(BackgroundColorWhenInvalid);
          else
            SetBackgroundColor(BackgroundColorWhenValid);
        }
        var decimalDelimiterIndex = newValue.IndexOf(_ci.NumberFormat.NumberDecimalSeparator);
        var hasDecimalDelimiter = decimalDelimiterIndex >= 0;
        var decimalPlaces = hasDecimalDelimiter ? newValue.Substring(decimalDelimiterIndex + 1, newValue.Length - decimalDelimiterIndex - 1).Length : 0;

        if (hasDecimalDelimiter && DecimalPlaces == 0)
          entry.Text = oldValue;
        else if (decimalPlaces > DecimalPlaces)
          entry.Text = newValue.Remove(newValue.Length - 1);
        else
          entry.Text = newValue;

      }
      else if (!newValue.Equals(""))
        entry.Text = oldValue;
    }

    private bool IsValidNumericInput(string text)
    {
      if (string.IsNullOrWhiteSpace(text))
        return false;

      char dot = _ci.NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
      foreach (char c in text)
      {
        if (!char.IsDigit(c) && c != dot && (c != '-' || (c == '-' && MinValue >= 0)))
          return false;
      }

      return true;
    }

    private bool IsValueInRange(decimal value)
    {
      return value >= MinValue && value <= MaxValue;
    }

    private void SetBackgroundColor(Color color)
    {
      entry.BackgroundColor = color;
    }

    public void SelectAll()
    {
      entry.CursorPosition = 0;
      entry.SelectionLength = entry.Text == null ? 0 : entry.Text.Length;
    }
  }
}