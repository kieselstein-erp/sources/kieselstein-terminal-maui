﻿using KieselsteinErp.Terminal.Services.Readers;
using KieselsteinErp.Terminal.Services.Settings;
using MetroLog.Maui;
using Microsoft.Extensions.Logging;

#if WINDOWS
using Windows.UI.ViewManagement;
#endif

namespace KieselsteinErp.Terminal;

public partial class App : Application
{
  private readonly ILogger<App> _logger;

  public App(ILogger<App> logger)
  {
    _logger = logger;
    // message processor injected, so that it is created.

    ////    Microsoft.Maui.Handlers.EntryHandler.Mapper.AppendToMapping("MyCustomization", (handler, view) =>
    ////    {
    ////      if (view is TouchEntry)
    ////      {
    ////#if WINDOWS

    ////        handler.PlatformView.GotFocus += async (s, e) =>
    ////        {
    ////          await KeyboardExtensions.ShowKeyboardAsync(view, default);
    ////          TouchKeyboardStatus("GotFocus");
    ////        };
    ////        handler.PlatformView.LostFocus += async (s, e) =>
    ////        {
    ////          await KeyboardExtensions.HideKeyboardAsync(view, default);
    ////          TouchKeyboardStatus("LostFocus");
    ////        };
    ////#endif
    ////      }
    ////    });

    // Workaround for Picker Title is always displayes above entry
#if WINDOWS10_0_19041_0_OR_GREATER
    Microsoft.Maui.Handlers.PickerHandler.Mapper.AppendToMapping(nameof(IPicker.Title), (handler, view) =>
    {
      if (handler.PlatformView is not null && view is Picker pick && !String.IsNullOrWhiteSpace(pick.Title))
      {
        handler.PlatformView.HeaderTemplate = new Microsoft.UI.Xaml.DataTemplate();
        handler.PlatformView.PlaceholderText = pick.Title;
        pick.Title = null;
      }
    });
#endif

    InitializeComponent();
#if ANDROID || IOS
    if (DeviceInfo.Idiom == DeviceIdiom.Tablet)
      MainPage = new AppShell();
    else
      MainPage = new AppShellMobile();
#else
    MainPage = new AppShell();
#endif
    LogController.InitializeNavigation(
            page => MainPage!.Navigation.PushModalAsync(page),
            () => MainPage!.Navigation.PopModalAsync());
  }

  //#if WINDOWS
  //  [DllImport("user32.dll")]
  //  [return: MarshalAs(UnmanagedType.Bool)]
  //  static extern bool IsWindowVisible(IntPtr hWnd);
  //  private string TouchKeyboardStatus(string where)
  //  {
  //    Process[] pname = Process.GetProcessesByName("tabtip");
  //    string msg = string.Empty;
  //    if (pname != null && pname.Length > 0)
  //    { // ok running
  //      IntPtr handle = pname[0].MainWindowHandle;
  //      bool visible = IsWindowVisible(handle);
  //      msg = $"{where}: Touch-Keyboard is running, Window {handle.ToString()} is visible={visible.ToString()}";
  //    }
  //    else
  //      msg = $"{where}: Touch-Keyboard is missing";

  //    _logger.LogInformation(msg);
  //    return msg;
  //  }
  //#endif

#if WINDOWS
  protected override Window CreateWindow(IActivationState activationState)
  {
    var settings = new SettingsPreferencesProvider();
    var window = base.CreateWindow(activationState);
    window.Width = settings.WindowSize.Width;
    window.Height = settings.WindowSize.Height;
    var monitor = Util.Monitor.GetMonitor(new Point(settings.WindowPosition.X, settings.WindowPosition.Y));
    double ratio = 1;

    if (settings.IsSizable && !settings.IsFullScreen)
    {
      if (DeviceDisplay.Current.MainDisplayInfo.Density != monitor.DpiRatio)
        ratio = DeviceDisplay.Current.MainDisplayInfo.Density / monitor.DpiRatio;

      window.X = settings.WindowPosition.X / ratio;
      window.Y = settings.WindowPosition.Y / ratio;
      window.SizeChanged += Window_SizeChanged;
    }

    //  window.Destroying += Window_Destroying;
    //  window.HandlerChanged += Window_HandlerChanged;
    // window.DisplayDensityChanged += Window_DisplayDensityChanged;

    return window;
  }

  //private void Window_HandlerChanged(object sender, EventArgs e)
  //{
  //  var win = sender as Window;
  //  if (win.Handler == null) return;

  ////  Microsoft.UI.Xaml.Window window = (Microsoft.UI.Xaml.Window)App.Current.Windows.First<Window>().Handler.PlatformView;
  ////  window.ExtendsContentIntoTitleBar=false;
    
  ////  //UI.SetBorderAndTitleBar(false,false);

  //  IntPtr windowHandle = WinRT.Interop.WindowNative.GetWindowHandle(window);
  //  Microsoft.UI.WindowId WindowId = Microsoft.UI.Win32Interop.GetWindowIdFromWindow(windowHandle);
  //  Microsoft.UI.Windowing.AppWindow appWindow = Microsoft.UI.Windowing.AppWindow.GetFromWindowId(WindowId);
  //  Microsoft.UI.Windowing.FullScreenPresenter presenter = (Microsoft.UI.Windowing.FullScreenPresenter)appWindow.Presenter;
  //  //presenter.SetBorderAndTitleBar(true,false);
  ////  //presenter.IsResizable = false;
  ////  //presenter.HasTitleBar
  //}
  //private void Window_DisplayDensityChanged(object sender, DisplayDensityChangedEventArgs e) {
  //  Debug.WriteLine(e.ToString());
  //  //var win = (sender as Window);
  //  //win.Width = win.Width * e.DisplayDensity;
  //  //win.Height = win.Height * e.DisplayDensity;
  //}

  private void Window_SizeChanged(object sender, EventArgs e)
  {
    // simulate a window move without titlebar: if size changed set width and height to values from settings
    var window = sender as Window;
    if (window != null)
    {
      var settings = new SettingsPreferencesProvider();
      window.Height = settings.WindowSize.Height;
      window.Width = settings.WindowSize.Width;
    }
  }

  private void Window_Destroying(object sender, EventArgs e)
  {
    System.Diagnostics.Debug.Print("Close");
    var deviceManager = Application.Current.MainPage.Handler.MauiContext.Services.GetService<IDeviceManager>();
    deviceManager.CloseAll();
  }


#endif
}
