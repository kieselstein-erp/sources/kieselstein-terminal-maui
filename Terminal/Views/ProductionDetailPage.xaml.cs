using KieselsteinErp.Terminal.ViewModels;

namespace KieselsteinErp.Terminal.Views;

//[XamlCompilation(XamlCompilationOptions.Skip)]
public partial class ProductionDetailPage : ContentPage
{
  public ProductionDetailPage(ProductionDetailPageViewModel viewModel)
  {
    InitializeComponent();
    this.BindingContext = viewModel;
  }
}