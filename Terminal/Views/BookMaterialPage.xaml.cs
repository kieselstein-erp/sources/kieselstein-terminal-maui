using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.ViewModels;
using System.Diagnostics;

namespace KieselsteinErp.Terminal.Views;

public partial class BookMaterialPage : ContentPage
{
	public BookMaterialPage(BookMaterialPageViewModel viewModel)
	{
		InitializeComponent();
		BindingContext = viewModel;
		viewModel.ScrollToIndex += OnScrollToIndex;
		this.InvalidateMeasure();
	}

	private void OnScrollToIndex(object sender, BookingMaterial material)
	{
		MainThread.BeginInvokeOnMainThread(() => { Collection.ScrollTo((material), ScrollToPosition.Start, true); });
	}

	private void TapGestureRecognizer_Tapped(object sender, TappedEventArgs e)
	{
		Debug.Print("test");
	}
}
