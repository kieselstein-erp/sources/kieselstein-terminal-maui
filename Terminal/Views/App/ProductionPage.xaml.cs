using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.ViewModels.App;
using Microsoft.Extensions.Logging;

namespace KieselsteinErp.Terminal.Views.App;

public partial class ProductionPage : ContentPage
{
  public ProductionPage(ProductionPageViewModel viewModel, IMessageService messageService, ILogger<ProductionPage> logger)
  {
    try
    {
      InitializeComponent();
      this.BindingContext = viewModel;
    }
    catch (Exception ex)
    {
      logger.LogError(ex.Message, ex);
    }
  }
}