using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.ViewModels.App;
using Microsoft.Extensions.Logging;

namespace KieselsteinErp.Terminal.Views.App;

public partial class TimeTrackingPage : ContentPage
{
  public TimeTrackingPage(TimeTrackingPageViewModel viewModel, IMessageService messageService, ILogger<ProductionPage> logger)
  {
    try
    {
      InitializeComponent();
      this.BindingContext = viewModel;
    }
    catch (Exception ex)
    {
      logger.LogError(ex.Message, ex);
    }
  }
}