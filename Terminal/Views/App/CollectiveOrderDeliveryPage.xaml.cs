using KieselsteinErp.Terminal.ViewModels.App;
using Microsoft.Extensions.Logging;

namespace KieselsteinErp.Terminal.Views.App;

public partial class CollectiveOrderDeliveryPage : ContentPage
{
	public CollectiveOrderDeliveryPage(CollectiveOrderDeliveryPageViewModel viewModel, ILogger<CollectiveOrderDeliveryPage> logger)
	{
    try
    {
      InitializeComponent();
      this.BindingContext = viewModel;
    }
    catch (Exception ex)
    {
      logger.LogError(ex.Message, ex);
    }
  }
}