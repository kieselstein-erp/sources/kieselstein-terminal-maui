using KieselsteinErp.Terminal.ViewModels.App;
using Microsoft.Extensions.Logging;

namespace KieselsteinErp.Terminal.Views.App;

public partial class OrderDeliveryPage : ContentPage
{
	public OrderDeliveryPage(OrderDeliveryPageViewModel viewModel, ILogger<OrderDeliveryPage> logger)
	{
    try
    {
      InitializeComponent();
      this.BindingContext = viewModel;
    }
    catch (Exception ex)
    {
      logger.LogError(ex.Message, ex);
    }
  }
}