using KieselsteinErp.Terminal.ViewModels.App;
using Microsoft.Extensions.Logging;

namespace KieselsteinErp.Terminal.Views.App;

public partial class InventoryPage : ContentPage
{
  public InventoryPage(InventoryPageViewModel viewModel, ILogger<InventoryPage> logger)
  {
    try
    {
      InitializeComponent();
      this.BindingContext = viewModel;
    }
    catch (Exception ex)
    {
      logger.LogError(ex.Message, ex);
    }
  }
}