using KieselsteinErp.Terminal.Services.UiState;
using KieselsteinErp.Terminal.ViewModels.App;

namespace KieselsteinErp.Terminal.Views.App;

public partial class LogonPage : ContentPage
{
  private readonly LatchBackedUiReadyService _uiReadyService;

  public LogonPage(LogonPageViewModel viewModel, LatchBackedUiReadyService uiReadyService)
  {
    InitializeComponent();
    _uiReadyService = uiReadyService;
    BindingContext = viewModel;

  }

  private void VerticalStackLayout_Loaded(object sender, EventArgs e)
  {
    _uiReadyService.TriggerGuiReady();
  }

}