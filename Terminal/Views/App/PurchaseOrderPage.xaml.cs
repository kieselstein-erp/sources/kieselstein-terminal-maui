using KieselsteinErp.Terminal.ViewModels.App;
using Microsoft.Extensions.Logging;

namespace KieselsteinErp.Terminal.Views.App;

public partial class PurchaseOrderPage : ContentPage
{
  public PurchaseOrderPage(PurchaseOrderPageViewModel viewModel, ILogger<PurchaseOrderPage> logger)
  {
    try
    {
      InitializeComponent();
      this.BindingContext = viewModel;
    }
    catch (Exception ex)
    {
      logger.LogError(ex.Message, ex);
    }
  }
}