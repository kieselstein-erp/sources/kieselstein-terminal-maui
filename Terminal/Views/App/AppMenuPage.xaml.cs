using KieselsteinErp.Terminal.ViewModels.App;
using Microsoft.Extensions.Logging;

namespace KieselsteinErp.Terminal.Views.App;

public partial class AppMenuPage : ContentPage
{
  public AppMenuPage(AppMenuPageViewModel viewModel, ILogger<AppMenuPage> logger)
  {
    try
    {
      InitializeComponent();
      this.BindingContext = viewModel;
    }
    catch (Exception ex)
    {
      logger.LogError(ex.Message, ex);
    }
  }
}