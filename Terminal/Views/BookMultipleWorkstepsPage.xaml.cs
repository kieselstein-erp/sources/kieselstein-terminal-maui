using KieselsteinErp.Terminal.ViewModels;

namespace KieselsteinErp.Terminal.Views;

public partial class BookMultipleWorkstepsPage : ContentPage
{
  public BookMultipleWorkstepsPage(BookMultipleWorkstepsPageViewModel viewModel)
  {
    InitializeComponent();
    BindingContext = viewModel;
  }
}