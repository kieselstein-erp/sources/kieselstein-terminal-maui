using KieselsteinErp.Terminal.ViewModels;

namespace KieselsteinErp.Terminal.Views;

public partial class SettingsPage : ContentPage
{
	public SettingsPage(SettingsPageViewModel viewModel)
	{
		InitializeComponent();
		BindingContext = viewModel;
	}
}