﻿using KieselsteinErp.Terminal.Services.UiState;
using KieselsteinErp.Terminal.ViewModels;

namespace KieselsteinErp.Terminal.Views;

public partial class MainPage : ContentPage
{
	private readonly LatchBackedUiReadyService _uiReadyService;

	public MainPage(MainPageViewModel viewModel, LatchBackedUiReadyService uiReadyService)
	{
		InitializeComponent();
		_uiReadyService = uiReadyService;
		BindingContext = viewModel;
	}

	private void ScrollView_Loaded(object sender, EventArgs e)
	{
		_uiReadyService.TriggerGuiReady();

	}
}

