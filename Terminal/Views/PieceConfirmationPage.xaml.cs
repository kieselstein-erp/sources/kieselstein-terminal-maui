using KieselsteinErp.Terminal.ViewModels;

namespace KieselsteinErp.Terminal.Views;

public partial class PieceConfirmationPage : ContentPage
{
  public PieceConfirmationPage(PieceConfirmationPageViewModel viewmodel)
  {
    InitializeComponent();
    this.BindingContext = viewmodel;
  }
}