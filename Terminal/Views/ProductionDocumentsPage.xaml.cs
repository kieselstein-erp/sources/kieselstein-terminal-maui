using KieselsteinErp.Terminal.ViewModels;

namespace KieselsteinErp.Terminal.Views;

public partial class ProductionDocumentsPage : ContentPage
{
  public ProductionDocumentsPage(ProductionDocumentsPageViewModel viewModel)
  {
    InitializeComponent();
    this.BindingContext = viewModel;
  }
}