using KieselsteinErp.Terminal.ViewModels;

namespace KieselsteinErp.Terminal.Views;

public partial class MachineListPage : ContentPage
{
  public MachineListPage(MachineListPageViewModel viewModel)
  {
    InitializeComponent();
    BindingContext = viewModel;
  }
}