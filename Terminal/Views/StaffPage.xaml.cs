using CommunityToolkit.Mvvm.Messaging;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.ViewModels;

namespace KieselsteinErp.Terminal.Views;

public partial class StaffPage : ContentPage
{
  public StaffPage(StaffPageViewModel viewModel)
  {
    InitializeComponent();
    BindingContext = viewModel;

    WeakReferenceMessenger.Default.Register<RefreshMessage>(this, HandleRefresh);
  }

  private void HandleRefresh(object recipient, RefreshMessage message)
  {
    //return;
    MainThread.InvokeOnMainThreadAsync(() =>
    {
      (this as IView).InvalidateMeasure();
      (collectionView as IView).InvalidateMeasure();
      (collectionView.Parent as IView).InvalidateMeasure();
      //  try
      //  {
      //	this.ForceLayout();
      //  } catch (Exception ex) { }
    });
  }
}