using KieselsteinErp.Terminal.ViewModels;

namespace KieselsteinErp.Terminal.Views;

public partial class ProductionOverviewPage : ContentPage
{
  public ProductionOverviewPage(ProductionOverviewPageViewModel viewModel)
  {
    InitializeComponent();
    BindingContext = viewModel;
  }
}