﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Item;
using KieselsteinErp.Terminal.Services.Machine;
using KieselsteinErp.Terminal.Services.Partslist;
using KieselsteinErp.Terminal.Services.Production;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.Staff;
using KieselsteinErp.Terminal.Services.StateMachine;
using KieselsteinErp.Terminal.Services.Worktime;
using KieselsteinErp.Terminal.Util;
using KieselsteinErp.Terminal.Views;
using Microsoft.Extensions.Logging;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Reactive.Linq;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.ViewModels
{
  public class Machine
  {
    public int Id { get; set; }
    public int? ProductionGroupId { get; set; }
    public int MachineGroupId { get; set; }
    public string ProductionGroupName { get; set; }
    public string MachineGroupName { get; set; }
    public string MachineName { get; set; }
    public string PersonShortName { get; set; }
    public string ProductionOrder { get; set; }
    public string Workstep { get; set; }
    public DateTime? StartDate { get; set; }

    public override int GetHashCode()
    {
      return HashCode.Combine(Id, ProductionGroupName, MachineGroupName, MachineName, PersonShortName, ProductionOrder, Workstep, StartDate);
    }
  }

  public class MachineGroup : List<Machine>
  {
    public string Name { get; private set; }

    public MachineGroup(string name, List<Machine> list) : base(list)
    {
      Name = name;
    }
  }

  public partial class ProductionOverviewPageViewModel : ObservableObject, IQueryAttributable
  {
    // update timer for actual Date-Time display
    private readonly IDispatcherTimer _timer;
    private readonly IDispatcherTimer _refreshTimer;
    private readonly ILogger<ProductionOverviewPageViewModel> _logger;
    private readonly ISettingsService _settingsService;
    private readonly IPartslistService _partslistService;
    private readonly IMachineService _machineService;
    private readonly IStaffService _staffService;
    private readonly IStateMachine _stateMachine;
    private readonly IProductionRestService _productionRestService;
    private readonly IItemV1Service _itemV1Service;
    private readonly IWorktimeService _worktimeService;
    private readonly IMessageService _messageService;
    [ObservableProperty]
    private string _labelDateTime = DateTime.Now.ToString();
    [ObservableProperty]
    private string _title;
    [ObservableProperty]
    private string _onlineString;
    [ObservableProperty]
    private bool _isLoadingCircleVisible = false;
    [ObservableProperty]
    private StaffEntry _staff;
    [ObservableProperty]
    private string _staffName;
    [ObservableProperty]
    private bool _isSelectionListVisible = false;
    [ObservableProperty]
    private List<string> _availableMachines;
    [ObservableProperty]
    private int _selectedMachine;

    ObservableCollection<MachineGroup> _factory = new();
    public ObservableCollection<MachineGroup> Factory { get { return _factory; } }

    private ObservableCollection<OpenWorkEntryWithMachines> _openWorkEntries = new();
    public ObservableCollection<OpenWorkEntryWithMachines> OpenWorkEntries { get { return _openWorkEntries; } }

    public List<MachineEntry> MachineEntries;

    private List<ProductionGroupEntry> _productionGroups;
    private List<MachineGroupEntry> _machineGroups;
    private int? _productionGroupId = null;

    public ProductionOverviewPageViewModel(ILogger<ProductionOverviewPageViewModel> logger, ISettingsService settingsService, IPartslistService partslistService, IMachineService machineService, IStaffService staffService, 
      IStateMachine stateMachine, IProductionRestService productionRestService, IItemV1Service itemV1Service, IWorktimeService worktimeService, IMessageService messageService)
    {
      _logger = logger;
      _settingsService = settingsService;
      _partslistService = partslistService;
      _machineService = machineService;
      _staffService = staffService;
      _stateMachine = stateMachine;
      _productionRestService = productionRestService;
      _itemV1Service = itemV1Service;
      _worktimeService = worktimeService;
      _messageService = messageService;

      MakeTitle();
      _timer = Application.Current.Dispatcher.CreateTimer();
      _timer.Interval = TimeSpan.FromSeconds(1);
      _timer.Tick += (s, e) =>
      {
        LabelDateTime = DateTime.Now.ToString();
      };

      SetProductionGroup();

      if (settingsService.RefreshIntervallPaperlessManufacturing > 0)
      {
        _refreshTimer = Application.Current.Dispatcher.CreateTimer();
        _refreshTimer.Interval = TimeSpan.FromMinutes(settingsService.RefreshIntervallPaperlessManufacturing);
        _refreshTimer.Tick += RefreshTimer_Tick;
      }
      _stateMachine.State().Subscribe(OnStateChange);

    }

    private void SetProductionGroup()
    {
      int? machineGroupId = null;
      if (_settingsService.MachineId != null)
      {
        Task<MachineEntry> task = Task.Run(() => _machineService.GetMachineEntryAsync((int)_settingsService.MachineId));
        MachineEntry machine = task.Result;
        if (machine != null)
          machineGroupId = machine.machineGroupId;
      }
      else
        machineGroupId = _settingsService.MachineGroupId;

      string productionGroup = null;
      if (machineGroupId != null)
      {
        Task<MachineGroupEntry> task = Task.Run(() => _machineService.GetMachineGroupEntryAsync((int)machineGroupId));
        MachineGroupEntry machineGroup = task.Result;
        if (machineGroup != null)
        {
          productionGroup = machineGroup.productionGroupDescription;
        }
      }
      if (string.IsNullOrEmpty(productionGroup))
        _productionGroupId = _settingsService.ManufacturingGroupId;
      else
      {
        Task<List<ProductionGroupEntry>> task = Task.Run(() => _partslistService.GetProductionGroupEntriesAsync());
        List<ProductionGroupEntry> productionGroupEntries = task.Result;
        ProductionGroupEntry productionGroupEntry = productionGroupEntries.Find(o => o.description == productionGroup);
        if (productionGroupEntry != null)
          _productionGroupId = productionGroupEntry.id;
      }
    }

    private async void RefreshTimer_Tick(object sender, EventArgs e)
    {
      await RefreshList();
    }

    private async void MakeTitle()
    {
      Title = await PageUIHelper.MakeTitle(_settingsService, _partslistService, _machineService);
    }

    private async Task RefreshList()
    {
      IsLoadingCircleVisible = true;
      try
      {
        IEnumerable<IGrouping<string, Machine>> result = await GetData();
        if (result != null)
        {

          foreach (var item in result)
          {
            MachineGroup group = new MachineGroup(item.Key, item.ToList());
            MachineGroup old = Factory.First(o => o.Name == item.Key);
            if (old != null && !HashCompare(old, group))
            {
              Factory[Factory.IndexOf(old)] = group;
            }
          }
        }
      }
      finally
      { 
        IsLoadingCircleVisible = false; 
      }
    }

    private bool HashCompare(MachineGroup old, MachineGroup group)
    {
      if (old.Count != group.Count) return false;
      for (int i=0; i<old.Count; i++)
        if (old[i].GetHashCode() != group[i].GetHashCode()) return false;

      return true;
    }

    private async Task CreateList()
    {
      try
      {
        IsLoadingCircleVisible = true;
        Factory.Clear();

        IEnumerable<IGrouping<string, Machine>> result = await GetData();

        if (result != null)
        {
          foreach (var o in result)
          {
            Factory.Add(new MachineGroup(o.Key, o.ToList()));
          }
        }
      }
      finally
      {
        IsLoadingCircleVisible = false;
      }
    }

    private async Task<IEnumerable<IGrouping<string, Machine>>> GetData()
    {
      PlanningView planningView = await _machineService.GetPlanningViewAsync(_productionGroupId, _settingsService.DaysForPlanning, _settingsService.PaperlessManufacturingRequestLimit);
      List<MachineEntry> machines = await _machineService.GetMachineEntriesForPlanningAsync();
      if (_settingsService.MachineId != null)
        machines = machines.Where(o => o.id == _settingsService.MachineId).ToList();
      else if (_settingsService.MachineGroupId != null)
        machines = machines.Where(o => o.machineGroupId == _settingsService.MachineGroupId).ToList();
      else if (_settingsService.ManufacturingGroupId != null)
      {
        List<MachineGroupEntry> machineGroups = await _machineService.GetMachineGroupEntriesAsync(_settingsService.ManufacturingGroupId);
        machines = machines.Where(o => machineGroups.Exists(e => e.id == o.machineGroupId)).ToList();
      }
      _productionGroups = await _partslistService.GetProductionGroupEntriesAsync();
      _machineGroups = await _machineService.GetMachineGroupEntriesAsync();

      SetAvailableMachines(machines);

      List<Machine> result = [];
      foreach (MachineEntry machine in machines)
      {
        MachineGroupEntry machineGroup = _machineGroups.Find(o => o.id == machine.machineGroupId);

        MachineEntry startetMachine = null;
        OpenWorkEntry openWorkEntry = null;
        if (planningView != null)
        {
          if (planningView.machineList != null)
            startetMachine = planningView.machineList.entries.Find(o => o.id == machine.id);

          if (planningView.openWorkList != null)
          {
            if (startetMachine != null)
              openWorkEntry = planningView.openWorkList.entries.Find(o => o.id == startetMachine.productionWorkplanId);
          }
        }
        result.Add(new Machine
        {
          Id = machine.id,
          MachineGroupId = machine.machineGroupId,
          ProductionGroupId = _productionGroupId,
          ProductionGroupName = machineGroup.productionGroupDescription,
          MachineGroupName = machineGroup.description,
          MachineName = machine.description,
          ProductionOrder = openWorkEntry?.productionCnr,
          Workstep = openWorkEntry?.workNumber.ToString(),
          StartDate = startetMachine == null ? null : startetMachine.starttime == 0 ? null : DateTimeHelper.FromMillisecondsSinceUnixEpoch(startetMachine.starttime),
          PersonShortName = startetMachine == null ? null : startetMachine.personalIdStarter == 0 ? null : _staffService.GetStaffShortMark(startetMachine.personalIdStarter)
        });

      }

      UpdateOpenWork(planningView, machines);

      if (result != null)
      {
        result = result.OrderBy(o => o.ProductionGroupName).ThenBy(o => o.MachineGroupName).ThenBy(o => o.MachineName).ToList();
        if (result.Count > 0)
        {
          var grouped = result.GroupBy(o => o.ProductionGroupName);
          return grouped;
        }
      }
      return null;
    }

    private void SetAvailableMachines(List<MachineEntry> machines)
    {
      List<string> available = [];
      available.Add(null);
      foreach (var machine in machines)
        available.Add(machine.identificationNumber);
      AvailableMachines = available;
    }

    private void UpdateOpenWork(PlanningView planningView, List<MachineEntry> machines)
    {
      List<OpenWorkEntry> list = [];
      if (planningView.openWorkList != null)
        foreach (OpenWorkEntry workEntry in planningView.openWorkList.entries)
        {
          //if (!(openWorkEntry != null && workEntry == openWorkEntry))
          //  if (workEntry.machineId == Machine.Id)
          list.Add(workEntry);
        }
      list = list.OrderBy(x => x.workItemStartDate).ToList();
      OpenWorkEntries.Clear();
      foreach (OpenWorkEntry workEntry in list)
      {
        List<MachineEntry> machinesToUse = null;
        int selectedMachineIndex = -1;
        if (workEntry.machineId != 0)
        {
          MachineEntry m = machines.FirstOrDefault(o => o.id == workEntry.machineId);
          if (m != null)
          {
            machinesToUse = machines.FindAll(o => o.machineGroupId == m.machineGroupId);
            selectedMachineIndex = machinesToUse.FindIndex(o => o.id == workEntry.machineId);
          }
        }
          
        OpenWorkEntryWithMachines openWorkEntryWithMachines = new() { OpenWorkEntry = workEntry, Machines = machinesToUse, SelectedMachineIndex = selectedMachineIndex };
        OpenWorkEntries.Add(openWorkEntryWithMachines);
      }
    }

    public void ApplyQueryAttributes(IDictionary<string, object> query)
    {
      if (query == null) return;
      if (query.ContainsKey(nameof(StaffEntry)))
        Staff = (StaffEntry)query[nameof(StaffEntry)];
      SetStaffName();
      if (query.ContainsKey(nameof(MachineEntries)))
        MachineEntries = (List<MachineEntry>)query[nameof(MachineEntries)];
    }

    private void SetStaffName()
    {
      if (Staff == null)
        StaffName = Prompt_Login;
      else
        StaffName = $"{Staff.firstName} {Staff.name}".Trim();
    }

    private void OnStateChange(State state)
    {
      Staff = state.StaffEntry;
      SetStaffName();
    }

    [RelayCommand]
    private async Task Appearing()
    {
      _timer.Start();
      if (Factory == null || Factory.Count == 0)
        await CreateList();
      else
        await RefreshList();

      _refreshTimer?.Start();
      IsSelectionListVisible = false;
    }

    [RelayCommand]
    private void Disappearing()
    {
      try
      {
        _timer?.Stop();
      }
      catch (Exception ex)
      {
        _logger.LogWarning(ex, "Timer stop");
      }
      if (_refreshTimer != null)

        try
        {
          _refreshTimer?.Stop();
        }
        catch (Exception ex)
        {
          _logger.LogWarning(ex, "RefreshTimer stop");
        }
    }

    [RelayCommand]
    private async Task Refresh()
    {
      await RefreshList();
    }

    [RelayCommand]
    private void FrameTapped(Machine machine)
    {
      IsLoadingCircleVisible = true;
      var navigationParameter = new Dictionary<string, object> { { nameof(Machine), machine }, { nameof(StaffEntry), Staff } };
      MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync("//" + nameof(ProductionDetailPage), navigationParameter); });
      //IsLoadingCircleVisible = false;
    }

    [RelayCommand]
    private async Task Logout()
    {
      await _stateMachine.OnCodeReceived(new UserAuthEvent(null));

      await MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync("//" + nameof(MainPage)); });
    }

    [RelayCommand]
    private void NewWorkstep()
    {
      if (OpenWorkEntries == null || OpenWorkEntries.Count == 0)
      {
        _messageService.Info("There are no open worksteps");
        return;
      }
      IsSelectionListVisible = true;
    }

    [RelayCommand]
    private void CloseSelection()
    {
      IsSelectionListVisible = false;
    }

    [RelayCommand]
    private async Task OpenworkTapped(OpenWorkEntryWithMachines openWorkEntry)
    {
      try
      {
        IsLoadingCircleVisible = true;
        Debug.Print($"Selected machine {SelectedMachine}, machine workentry {openWorkEntry.OpenWorkEntry.machineCnr}");
        ProductionEntry productionEntry = await _productionRestService.GetProductionEntry(openWorkEntry.OpenWorkEntry.productionCnr);
        if (productionEntry == null) return;
        ItemV1Entry itemV1Entry = await _itemV1Service.GetItemV1EntryAsync(openWorkEntry.OpenWorkEntry.workItemCnr, false);
        ProductionRecordingEntry entry = _productionRestService.BuildProductionrecordingEntry(productionEntry.id, openWorkEntry.SelectedMachineIndex != -1 ? openWorkEntry.Machines[openWorkEntry.SelectedMachineIndex].id : openWorkEntry.OpenWorkEntry.machineId, 
          openWorkEntry.OpenWorkEntry.id, itemV1Entry.id, Staff.id);
        BookingResult bookingResult = await _worktimeService.BookProductionRecordingAsync(entry);
        if (bookingResult.Success)
        {
          _messageService.ShowToast(string.Format(Workstep0ForProductionOrder1Booked, openWorkEntry.OpenWorkEntry.workNumber, openWorkEntry.OpenWorkEntry.productionCnr), CommunityToolkit.Maui.Core.ToastDuration.Short);
          await CreateList();
        }
        else
          _messageService.Error(bookingResult.Message);
      }
      finally
      {
        IsSelectionListVisible = false;
        IsLoadingCircleVisible = false;
      }
    }
  }
}
