﻿using CommunityToolkit.Maui.Storage;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Item;
using KieselsteinErp.Terminal.Services.Production;
using SkiaSharp;
using System.Collections.ObjectModel;
using System.Text;
using System.Web;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.ViewModels
{
  public class Document
  {
    public enum DocumentType
    {
      ItemMasterSheet,              // Item mastersheet (Artikelstammblatt)
      ProductionOrderFinalCosting,  // Production order final costing (Losnachkalkulation)
      ProductionInformationSheet,   // Production information sheet (Produktionsinformationen)
      Document,                     // from jackrabbit document storage
      Comment                       // from production comments
    }

    private DocumentInfoEntry _documentInfo;
    private DocumentType _documentType;
    private string _name;
    private ItemCommentMediaInfoEntry _comment;

    public Document(DocumentInfoEntry entry)
    {
      _documentInfo = entry;
      _documentType = DocumentType.Document;
    }

    public Document(string name, DocumentType documentType)
    {
      _name = name;
      _documentType = documentType;
    }

    public Document(ItemCommentMediaInfoEntry comment)
    {
      _comment = comment;
      _documentType = DocumentType.Comment;
    }

    public string DocumentName
    {
      get =>
        _documentType == DocumentType.Document ? $"{_documentInfo.name}" :
        _documentType == DocumentType.Comment ? $"{_comment.commentTypeDescription}" : _name;
    }
    public string FileName
    {
      get =>
        _documentType == DocumentType.Document ? $"({_documentInfo.filename})" :
        _documentType == DocumentType.Comment ? $"({Path.GetFileName(_comment.filename)})" : "";
    }

    public DocumentInfoEntry DocumentInfoEntry { get => _documentInfo; }
    public DocumentType Type { get => _documentType; }
    public ItemCommentMediaInfoEntry CommentMediaInfoEntry { get => _comment; }
  }

  public partial class ProductionDocumentsPageViewModel : ObservableObject, IQueryAttributable
  {
    private readonly IProductionRestService _productionRestService;
    private readonly IMessageService _messageService;
    private readonly IItemV1Service _itemV1Service;

    private ProductionWorkstep _production;
    private List<DocumentInfoEntry> _documents;
    private List<ItemCommentMediaInfoEntry> _comments;
    private ProductionEntry _productionEntry = null;
    private int _pageCount = 1;
    private PdfResult _pdfResult = null;

    [ObservableProperty]
    private string _pdfTitle;
    [ObservableProperty]
    private byte[] _pdfImage;
    [ObservableProperty]
    private bool _isPdfReportVisible = false;
    [ObservableProperty]
    private bool _isLoadingCircleVisible = false;
    [ObservableProperty]
    string _title = ProductionDocuments;
    [ObservableProperty]
    private bool _isMultiPagePdf = false;
    [ObservableProperty]
    private bool _isPageUpEnabled = false;
    [ObservableProperty]
    private bool _isPageDownEnabled = false;
    [ObservableProperty]
    private int _actualPage = 1;

    private ObservableCollection<Document> _documentNames = [];
    public ObservableCollection<Document> DocumentNames { get => _documentNames; }

    public ProductionDocumentsPageViewModel(IProductionRestService productionRestService, IMessageService messageService, IItemV1Service itemV1Service)
    {
      _productionRestService = productionRestService;
      _messageService = messageService;
      _itemV1Service = itemV1Service;

    }


    private async void InitAvailableDocuments()
    {
      IsLoadingCircleVisible = true;
      try
      {
        DocumentNames.Clear();
        if (_productionEntry != null && _productionEntry.itemEntry != null)
          DocumentNames.Add(new Document(KieselsteinErp.Terminal.Resources.Localization.AppResources.ItemMasterSheet, Document.DocumentType.ItemMasterSheet));
        DocumentNames.Add(new Document(ProductionOrderInformationReport, Document.DocumentType.ProductionInformationSheet));
        DocumentNames.Add(new Document(ProductionOrderFinalCostingReport, Document.DocumentType.ProductionOrderFinalCosting));
        DocumentListResult result = await _productionRestService.GetDocumentInfoListAsync(_production.ProductionId);
        if (result.Success)
        {
          _documents = result.DocumentInfoEntries;
          foreach (DocumentInfoEntry document in _documents)
          {
            DocumentNames.Add(new Document(document));
          }
        }
        else
          _messageService.Error(result.Message);
        CommentMediaListResult comments = await _productionRestService.GetItemCommentMediaInfoEntriesAsync(_production.ProductionId);
        if (comments.Success)
        {
          _comments = comments.CommentMediaEntries;
          foreach (ItemCommentMediaInfoEntry comment in _comments)
          {
            DocumentNames.Add(new Document(comment));
          }
        }
      }
      finally
      { IsLoadingCircleVisible = false; }
    }

    public void ApplyQueryAttributes(IDictionary<string, object> query)
    {
      var old = _production;
      _production = (ProductionWorkstep)query[nameof(ProductionWorkstep)];
      if (old?.ProductionId != _production?.ProductionId) IsPdfReportVisible = false;

      Title = string.Format(DocumentsForProductionOrder0, _production.ProductionOrderNumber);

      //CreateList();
    }

    [RelayCommand]
    private async Task Appearing()
    {
      if (_productionEntry == null || (_productionEntry.id != _production?.ProductionId))
        _productionEntry = await _productionRestService.GetProductionEntry(_production.ProductionOrderNumber);

      InitAvailableDocuments();
    }

    [RelayCommand]
    private void Disappearing()
    {

    }

    [RelayCommand]
    private async Task Close()
    {
      await Shell.Current.Navigation.PopAsync();
    }

    [RelayCommand]
    private async Task ListTapped(Document selected)
    {
      PdfImage = null;
      IsLoadingCircleVisible = true;
      try
      {
        Document.DocumentType type = selected.Type;
        PdfResult pdfResult = null;
        switch (type)
        {
          case Document.DocumentType.ItemMasterSheet:
            // item master sheet
            if (_productionEntry == null)
            {
              _messageService.Warn(string.Format(ProductionOrder0NotFound, _production.ProductionOrderNumber));
              return;
            }
            ItemMasterSheet itemMasterSheet = await _itemV1Service.GetItemMasterSheet(_productionEntry.itemEntry.id);
            //SaveToFile(itemMasterSheet.pdfContent);


            if (InitPageCount(new PdfResult(itemMasterSheet.pdfContent), KieselsteinErp.Terminal.Resources.Localization.AppResources.ItemMasterSheet))
              ShowPdf();
            break;

          case Document.DocumentType.ProductionInformationSheet:
            // production information sheet
            pdfResult = await _productionRestService.GetProductionOrderInformation(_production.ProductionId);
            if (InitPageCount(pdfResult, ProductionOrderInformationReport))
              ShowPdf();
            break;

          case Document.DocumentType.ProductionOrderFinalCosting:
            // production order final costing
            pdfResult = await _productionRestService.GetProductionOrderFinalCosting(_production.ProductionId);
            if (InitPageCount(pdfResult, ProductionOrderFinalCostingReport))
              ShowPdf();
            break;

          default:
            await ShowDocument(selected);
            break;
        }
      }
      finally
      { IsLoadingCircleVisible = false; }
    }

    private async void SaveToFile(string pdfContent)
    {
      using var stream = new MemoryStream(Convert.FromBase64String(pdfContent));
      var fileSaverResult = await FileSaver.Default.SaveAsync("test.pdf", stream, null);
    }

    private async Task ShowDocument(Document selected)
    {
      IsMultiPagePdf = false;
      if (selected == null) return;

      DocumentResult result = null;
      string name = string.Empty;

      if (selected.Type == Document.DocumentType.Document)
      {
        DocumentInfoEntry entry = selected.DocumentInfoEntry;
        name = entry.name;
        if (entry != null)
          result = await _productionRestService.GetDocumenAsync(_production.ProductionId, entry.cnr);
      }
      else if (selected.Type == Document.DocumentType.Comment)
      {
        ItemCommentMediaInfoEntry comment = selected.CommentMediaInfoEntry;
        name = comment.commentTypeDescription;
        if (comment != null)
          result = await _productionRestService.GetCommentAsync(_production.ProductionId, comment.id);
      }

      if (result.Success)
      {
        string fileName = HttpUtility.UrlDecode(result.FileName);
        PdfTitle = $"{name} ({fileName})";
        if (fileName.ToLower().EndsWith(".png") || fileName.ToLower().EndsWith(".jpg") || fileName.ToLower().EndsWith(".jpeg"))
        {
          //File.WriteAllBytes("C:\\Temp\\" + result.FileName, result.DocumentAsBytes);
          if (result.DocumentAsBytes != null && result.DocumentAsBytes.Length > 0)
          {
            PdfImage = result.DocumentAsBytes;
            IsPdfReportVisible = true;
          }
        }
        else if (fileName.ToLower().EndsWith(".pdf"))
        {
          if (InitPageCount(result))
              ShowPdf();
          else
          {
            SKImage skImage = SKImage.FromBitmap(PDFtoImage.Conversion.ToImage(result.DocumentAsBytes));
            SKData encoded = skImage.Encode();
            using (MemoryStream memory = new MemoryStream())
            {
              encoded.AsStream().CopyTo(memory);
              PdfImage = memory.ToArray();
              IsPdfReportVisible = true;
            }
          }
        }
      }
      else
      {
        IsPdfReportVisible = false;
        _messageService.Error(result.Message);
      }
    }

    private bool InitPageCount(DocumentResult documentResult)
    {
      if (documentResult.Success)
      {
        if (documentResult.DocumentAsBytes.Length > 0)
        {
          _pdfResult = new PdfResult(Convert.ToBase64String(documentResult.DocumentAsBytes));
          _pageCount = Encoding.UTF8.GetString(documentResult.DocumentAsBytes).Split(new string[] { "/Type/Page" }, StringSplitOptions.None).Count() - 2;
          IsMultiPagePdf = _pageCount > 1;
          ActualPage = 1;
          SwitchPageButtons();
          return true;
        }
        else
          _messageService.Warn(EmptyReport);
      }
      return false;
    }

    private bool InitPageCount(PdfResult pdfResult, string title)
    {
      if (pdfResult.Success)
      {
        PdfTitle = title;
        if (pdfResult.PdfString.Length > 0)
        {
          _pdfResult = pdfResult;
          _pageCount = Encoding.UTF8.GetString(Convert.FromBase64String(pdfResult.PdfString)).Split(new string[] { "/Type/Page" }, StringSplitOptions.None).Count() - 2;
          IsMultiPagePdf = _pageCount > 1;
          ActualPage = 1;
          SwitchPageButtons();
          return true;
        }
        else
          _messageService.Warn(EmptyReport);
      }
      else
      {
        _pdfResult = null;
        PdfTitle = string.Empty;
        IsMultiPagePdf = false;
        _pageCount = 0;
        _messageService.Error(_pdfResult.Message);
      }
      return false;
    }

    private void ShowPdf()
    {
      if (!string.IsNullOrEmpty(_pdfResult.PdfString))
      {
        IsLoadingCircleVisible = true;
        Task.Run(() =>
        {
          try
          {
            SKImage skImage = SKImage.FromBitmap(PDFtoImage.Conversion.ToImage(_pdfResult.PdfString, page: ActualPage - 1));
            SKData encoded = skImage.Encode();
            using (MemoryStream memory = new MemoryStream())
            {
              encoded.AsStream().CopyTo(memory);
              PdfImage = memory.ToArray();
              IsPdfReportVisible = true;
            }
          }
          catch (Exception ex)
          {
            _messageService.Error(ex.Message);
          }
          finally
          {
            IsLoadingCircleVisible = false;
          }
        });
      }
    }

    [RelayCommand]
    private void NextPage()
    {
      if (ActualPage >= _pageCount) return;
      ActualPage++;
      SwitchPageButtons();
      ShowPdf();
    }

    [RelayCommand]
    private void PreviousPage()
    {
      IsPageDownEnabled = ActualPage > 1;
      if (ActualPage <= 1) return;
      ActualPage--;
      SwitchPageButtons();
      ShowPdf();
    }

    private void SwitchPageButtons()
    {
      IsPageUpEnabled = IsMultiPagePdf && ActualPage < _pageCount;
      IsPageDownEnabled = IsMultiPagePdf && ActualPage > 1;
    }

    [RelayCommand]
    private void ClosePdfReport()
    {
      IsPdfReportVisible = false;
    }
  }
}
