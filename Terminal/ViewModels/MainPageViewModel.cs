using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Popups;
using KieselsteinErp.Terminal.Resources.Localization;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Item;
using KieselsteinErp.Terminal.Services.Machine;
using KieselsteinErp.Terminal.Services.OfflineBooking;
using KieselsteinErp.Terminal.Services.Production;
using KieselsteinErp.Terminal.Services.Readers;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.Staff;
using KieselsteinErp.Terminal.Services.StateMachine;
using KieselsteinErp.Terminal.Services.UiState;
using KieselsteinErp.Terminal.Services.User;
using KieselsteinErp.Terminal.Services.Worktime;
using KieselsteinErp.Terminal.Util;
using KieselsteinErp.Terminal.Views;
using SkiaSharp;
using System.Collections.ObjectModel;
using System.Diagnostics;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.ViewModels
{

  public partial class MainPageViewModel : ObservableObject
  {

    private readonly IMessageService _messageService;
    private readonly ISettingsService _settingsService;
    private readonly IStaffService _staffService;
    private readonly IStateMachine _stateMachine;
    private readonly IUserService _userService;
    private readonly IDeviceManager _deviceManager;
    private readonly IWorktimeService _workTimeService;
    private readonly IUiReadyService _uiReadyService;
    private readonly IOfflineBookingService _offlineBookingService;
    private readonly IMachineService _machineService;
    private readonly IProductionService _productionService;
    private readonly IItemV1Service _itemV1Service;
    private readonly IProductionRestService _productionRestService;

    // only injected so that it is created ... TODO find a better way to achieve this
    private readonly DialogMessageProcessor _dialogMessageProcessor;

    // update timer for actual Date-Time display
    private readonly IDispatcherTimer _timer;

    [ObservableProperty]
    private string _labelDateTime;

    [ObservableProperty]
    private string _name;

    [ObservableProperty]
    private bool _isBuchenAllowed;

    [ObservableProperty]
    private string _onlineString;

    [ObservableProperty]
    private string _title = string.Format(Title_Application, AppInfo.Current.VersionString);

    [ObservableProperty]
    private bool _isSaldoVisible = false;

    [ObservableProperty]
    private bool _isPieceVisible = false;

    [ObservableProperty]
    private byte[] _image;

    [ObservableProperty]
    private bool _isMonthlyReportVisible = false;

    [ObservableProperty]
    private bool _isSaldoReportVisible = false;

    [ObservableProperty]
    private int _saldoOffset = 0;

    [ObservableProperty]
    private bool _isSaldoIncreaseVisible = false;

    [ObservableProperty]
    private bool _isSaldoDecreaseVisible = true;

    [ObservableProperty]
    private string _timeBalance = string.Empty;

    [ObservableProperty]
    private string _availableVacation = string.Empty;

    [ObservableProperty]
    private bool _isLoadingCircleVisible = false;

    [ObservableProperty]
    private bool _isWorktimeVisible = false;

    [ObservableProperty]
    private bool _isMachinesVisible = false;

    [ObservableProperty]
    private bool _isSizable = false;

    [ObservableProperty]
    private bool _isRequestsVisible = false;

    [ObservableProperty]
    private byte[] _pdfImage;

    [ObservableProperty]
    private bool _isPdfReportVisible = false;

    [ObservableProperty]
    private string _pdfTitle = string.Empty;

    [ObservableProperty]
    private bool _isPaperlessManufacturing = false;

    [ObservableProperty]
    private bool _isTimeDistributionsVisible = false;

    [ObservableProperty]
    private bool _isMachineListVisible = false;


    private ObservableCollection<TimeDistributionWithMachine> _timeDistributionEntries = new ObservableCollection<TimeDistributionWithMachine>();
    public ObservableCollection<TimeDistributionWithMachine> TimeDistributionEntries { get => _timeDistributionEntries; set => SetProperty(ref _timeDistributionEntries, value); }

    private ObservableCollection<ZeitdatenEntry> _worktimeEntries = new ObservableCollection<ZeitdatenEntry>();
    public ObservableCollection<ZeitdatenEntry> WorktimeEntries { get => _worktimeEntries; set => SetProperty(ref _worktimeEntries, value); }

    private ObservableCollection<MachineEntry> _machineEntries = new ObservableCollection<MachineEntry>();
    public ObservableCollection<MachineEntry> MachineEntries { get => _machineEntries; set => SetProperty(ref _machineEntries, value); }

    private State _state;
    private bool _isOnline;

    public MainPageViewModel(
                    IMessageService messageService,
                    ISettingsService settingsService,
                    IUserService userService,
                    IStaffService staffService,
                    IStateMachine stateMachine,
                    IDeviceManager deviceManager,
                    IWorktimeService worktimeService,
                    IUiReadyService uiReadyService,
                    IOfflineBookingService offlineBookingService,
                    IMachineService machineService,
                    IProductionService productionService,
                    IItemV1Service itemV1Service,
                    IProductionRestService productionRestService)
    {
      _messageService = messageService;
      _settingsService = settingsService;
      _userService = userService;
      _staffService = staffService;
      _stateMachine = stateMachine;
      _deviceManager = deviceManager;
      _workTimeService = worktimeService;
      _uiReadyService = uiReadyService;
      _offlineBookingService = offlineBookingService;
      _machineService = machineService;
      _productionService = productionService;
      _itemV1Service = itemV1Service;
      _productionRestService = productionRestService;

      IsSaldoVisible = _settingsService.IsSaldoVisible;
      IsPieceVisible = _settingsService.IsPieceVisible;
      IsRequestsVisible = _settingsService.IsRequestsVisible;
      IsPaperlessManufacturing = _settingsService.IsPaperlessManufacturing;
      IsMachineListVisible = _settingsService.IsMachineVisible;

      // xaml OnPlatform only supports static resources, platform dependend visualibity is handled here
      // Note: xaml Binding like { OnPlatform WinUI = { Binding IsSizable }} works in debug mode, fails in release
      IsSizable = (DeviceInfo.Current.Platform == DevicePlatform.WinUI ? _settingsService.IsSizable : false);

      stateMachine.State().Subscribe(OnStateChange);

      _userService.OnLogonChanged += (s, e) =>
      {
        _isOnline = e;
        if (e)
        {
          OnlineString = Connection_State_Online;
          GotoStaffView();
        }
        else
          OnlineString = Connection_State_Offline;
      };
      if (_isOnline = !string.IsNullOrEmpty(_userService.Userid))
        OnlineString = Connection_State_Online;
      else
        OnlineString = Connection_State_Offline;

      _timer = Application.Current.Dispatcher.CreateTimer();
      _timer.Interval = TimeSpan.FromSeconds(1);
      _timer.Tick += (s, e) =>
      {
        LabelDateTime = DateTime.Now.ToString();
      };

      WeakReferenceMessenger.Default.Register<PdfMessage>(this, HandlePdfMessage);
      WeakReferenceMessenger.Default.Register<SaldoMessage>(this, HandleSaldoMessage);
      WeakReferenceMessenger.Default.Register<DeliverMessage>(this, HandleDeliverMessage);
      WeakReferenceMessenger.Default.Register<LoadingCircleMessage>(this, HandleLoadingCircleMessage);
      WeakReferenceMessenger.Default.Register<PieceConfirmationMessage>(this, HandlePieceConfirmationMessage);
      WeakReferenceMessenger.Default.Register<VersionMessage>(this, HandleVersionMessage);
      WeakReferenceMessenger.Default.Register<BookMultipleWorkstepsMessage>(this, HandleMultipleWorkstepsMessage);
      WeakReferenceMessenger.Default.Register<RefreshWorkTimeMessage>(this, HandleRefreshWorkTime);

    }

    private async void HandleMultipleWorkstepsMessage(object recipient, BookMultipleWorkstepsMessage message)
    {
      await MainThread.InvokeOnMainThreadAsync(async () =>
      {
        if (message != null && message.Value)
        {
          IsLoadingCircleVisible = true;
          BookMultipleWorkstepsQuery query = new BookMultipleWorkstepsQuery( _state.StaffEntry, [.. TimeDistributionEntries], message.PieceConfirmations);
          await Shell.Current.GoToAsync(nameof(BookMultipleWorkstepsPage), new Dictionary<string, object> { [nameof(BookMultipleWorkstepsQuery)] = query });
          IsLoadingCircleVisible = false;
        }
      });
    }

    private async void HandleVersionMessage(object recipient, VersionMessage message)
    {
      await MainThread.InvokeOnMainThreadAsync(() =>
      {
        _messageService.ShowToast(message.Value + $"\r\n({message.ActualVersion} < {message.MinVersion})", CommunityToolkit.Maui.Core.ToastDuration.Long);
      });
    }

    private async void HandlePdfReportMessage(object recipient, PdfReportMessage message)
    {
      await MainThread.InvokeOnMainThreadAsync(() =>
      {
        DoHandlePdfReportMessage(message);
      });
    }

    private async void DoHandlePdfReportMessage(PdfReportMessage message)
    {
      IsLoadingCircleVisible = true;
      try
      {
        switch (message.Type)
        {
          case PdfReportMessage.EType.ItemMasterSheet:
            {
              ItemV1Entry itemV1Entry = await _itemV1Service.GetItemV1EntryAsync(message.Parameter, false);
              if (itemV1Entry == null)
                _messageService.Error(string.Format(Item0NotFound, message.Parameter));
              else
              {
                ItemMasterSheet report = await _itemV1Service.GetItemMasterSheet(itemV1Entry.id);
                if (report?.pdfContent?.Length > 0)
                {
                  try
                  {
                    ShowPdf(AppResources.ItemMasterSheet, report.pdfContent);
                  }
                  catch (Exception ex)
                  {
                    _messageService.Error(ex.Message);
                  }
                }
                else
                  _messageService.Warn(EmptyReport);
              }
            }
            break;

          case PdfReportMessage.EType.ProductionOrderFinalCosting:
            {
              ProductionEntry productionEntry = await _productionRestService.GetProductionEntry(message.Parameter);
              if (productionEntry == null)
                _messageService.Error(string.Format(Error_Lot_Not_Found, message.Parameter));
              else
              {
                PdfResult result = await _productionRestService.GetProductionOrderFinalCosting(productionEntry.id);
                if (result.Success)
                  if (result.PdfString.Length > 0)
                  {
                    try
                    {
                      ShowPdf(ProductionOrderFinalCostingReport, result.PdfString);
                    }
                    catch (Exception ex)
                    {
                      _messageService.Error(ex.Message);
                    }
                  }
                  else
                    _messageService.Warn(EmptyReport);
                else
                  _messageService.Error(result.Message);
              }
            }
            break;

          case PdfReportMessage.EType.ProductionInformationSheet:
            {
              ProductionEntry productionEntry = await _productionRestService.GetProductionEntry(message.Parameter);
              if (productionEntry == null)
                _messageService.Error(string.Format(Error_Lot_Not_Found, message.Parameter));
              else
              {
                PdfResult result = await _productionRestService.GetProductionOrderInformation(productionEntry.id);
                if (result.Success)
                  if (result.PdfString.Length > 0)
                  {
                    try
                    {
                      ShowPdf(ProductionOrderInformationReport, result.PdfString);
                    }
                    catch (Exception ex)
                    {
                      _messageService.Error(ex.Message);
                    }
                  }
                  else
                    _messageService.Warn(EmptyReport);
                else
                  _messageService.Error(result.Message);
              }
            }
            break;
        }
      }
      finally
      {
        IsLoadingCircleVisible = false;
      }
    }

    private void ShowPdf(string title, string pdfContent)
    {
      if (!string.IsNullOrEmpty(pdfContent))
      {
        PdfTitle = title;
        SKImage skImage = SKImage.FromBitmap(PDFtoImage.Conversion.ToImage(pdfContent));
        SKData encoded = skImage.Encode();
        using (MemoryStream memory = new MemoryStream())
        {
          encoded.AsStream().CopyTo(memory);
          PdfImage = memory.ToArray();
          IsPdfReportVisible = true;
        }
      }
    }

    [RelayCommand]
    private void ClosePdfReport()
    {
      IsPdfReportVisible = false;
      PdfImage = null;
    }

    private async void HandlePieceConfirmationMessage(object recipient, PieceConfirmationMessage message)
    {
      PieceConfirmationQuery pieceConfirmationQuery = new()
      {
        Personalnummer = _state.StaffEntry.personalNr,
        WorkTimeEvent = new WorkTimeEvent(message.WorkTimeEventType, message.NoUnidentify),
        OpenWorkEntryList = message.Value,
        ProductionRecordingEntry = message.WorktimeToBook,
        IsGhostShiftConfirmation = message.IsGhostShiftConfirmation,
        GhostShiftInfo = message.GhostShiftInfo
      };

      //Dictionary<string, object> query = new Dictionary<string, object>
      //{
      //  [nameof(StaffEntry.personalNr)] = _state.StaffEntry.personalNr,
      //  [nameof(WorkTimeEvent)] = new WorkTimeEvent(message.WorkTimeEventType, message.NoUnidentify),
      //  [nameof(OpenWorkEntryList)] = message.Value,
      //  [nameof(ProductionRecordingEntry)] = message.WorktimeToBook
      //};
      await MainThread.InvokeOnMainThreadAsync(async () =>
      {
        await Shell.Current.GoToAsync(nameof(PieceConfirmationPage), new Dictionary<string, object> { [nameof(PieceConfirmationQuery)] = pieceConfirmationQuery });
      });
    }

    private void HandleLoadingCircleMessage(object recipient, LoadingCircleMessage message)
    {
      if (message != null)
        IsLoadingCircleVisible = message.Value;
    }

    private async void HandleBookMaterialToLotMessage(object recipient, BookMaterialToLotMessage message)
    {
      IsLoadingCircleVisible = true;
      await MainThread.InvokeOnMainThreadAsync(async () =>
      {
        await Shell.Current.GoToAsync(nameof(BookMaterialPage), new Dictionary<string, object> { { "message", message } });
        IsLoadingCircleVisible = false;
      });
    }

    [RelayCommand]
    private void Logout()
    {
      _stateMachine.OnCodeReceived(new UserAuthEvent(null));
    }

    [RelayCommand]
    private void ScrollViewLoaded()
    {
      GotoStaffView();
    }

    [RelayCommand]
    private void Appearing()
    {
      Debug.WriteLine("MainPage Appearing");
      
      if (!WeakReferenceMessenger.Default.IsRegistered<BookMaterialToLotMessage>(this))
        WeakReferenceMessenger.Default.Register<BookMaterialToLotMessage>(this, HandleBookMaterialToLotMessage);
      
      if (!WeakReferenceMessenger.Default.IsRegistered<PdfReportMessage>(this))
        WeakReferenceMessenger.Default.Register<PdfReportMessage>(this, HandlePdfReportMessage);

      if (!WeakReferenceMessenger.Default.IsRegistered<ChangeAmountMessage>(this))
        WeakReferenceMessenger.Default.Register<ChangeAmountMessage>(this, HandleChangeAmountMessage);

      _timer.Start();

      //GotoStaffView();
    }

    [RelayCommand]
    private void Disappearing()
    {
      Debug.WriteLine("MainPage Disappearing");
      //WeakReferenceMessenger.Default.Unregister<PdfMessage>(this);
      WeakReferenceMessenger.Default.Unregister<BookMaterialToLotMessage>(this);
      WeakReferenceMessenger.Default.Unregister<PdfReportMessage>(this);
      WeakReferenceMessenger.Default.Unregister<ChangeAmountMessage>(this);
      try
      {
        _timer?.Stop();
      }
      catch (Exception ex)
      {
        Debug.WriteLine(ex.ToString());
      }
    }

    private async void HandleRefreshWorkTime(object recipient, RefreshWorkTimeMessage message)
    {
      try
      {
        IsLoadingCircleVisible = true;
        await DisplayTimeDistributions();
        await DisplayBookings();
        await DisplayMachines();
      }
      finally { IsLoadingCircleVisible = false; }
    }

    private async Task DisplayTimeDistributions()
    {
      IsTimeDistributionsVisible = false;
      await GetAllDistributions();
      IsTimeDistributionsVisible = (TimeDistributionEntries != null && TimeDistributionEntries.Count > 0);
    }

    private async Task GetAllDistributions()
    {
      List<TimeDistributionEntry> entries = await _workTimeService.GetTimeDistributionEntriesAsync(_state.StaffEntry.id, null);
      await MainThread.InvokeOnMainThreadAsync(async () =>
      {
        if (TimeDistributionEntries.Count > 0)
          TimeDistributionEntries.Clear();
        if (entries != null)
        {
          foreach (var entry in entries)
          {
            MachineEntry machine = null;
            int machineId = entry.productionWorkstepEntry.machineId;
            if (entry.machineIdToUse != null && entry.machineIdToUse != 0)
              machineId = (int)entry.machineIdToUse;
            if (machineId != 0)
            {
              machine = await _machineService.GetMachineEntryAsync(machineId);
            }
            TimeDistributionWithMachine withMachine = new(entry, machine);
            TimeDistributionEntries.Add(withMachine);
          }
        }
      });
    }

    private async Task DisplayBookings()
    {
      IsWorktimeVisible = false;
      WorktimeEntries = await GetAllEntries(DateTime.Now);
      IsWorktimeVisible = (WorktimeEntries != null && WorktimeEntries.Count > 0);
    }

    private async Task<ObservableCollection<ZeitdatenEntry>> GetAllEntries(DateTime dt)
    {
      int maxDays = 2;
      string personalNr = _state.StaffEntry.personalNr; // needed local, because async method and state could change

      ObservableCollection<ZeitdatenEntry> newEntries = new();
      for (int day = 0; day < maxDays; day++)
      {
        dt = dt.AddDays(-day);
        List<ZeitdatenEntry> zeitdatenEntries = await _workTimeService.GetWorktimeEntriesAsync(dt.Day, dt.Month, dt.Year, personalNr);
        if (zeitdatenEntries != null)
          for (int i = zeitdatenEntries.Count() - 1; i >= 0; i--)
          {
            newEntries.Add(zeitdatenEntries[i]);
            if (zeitdatenEntries[i].activityCnr != null && zeitdatenEntries[i].activityCnr.ToUpper().Equals(IWorktimeService.ActivityComming)) return newEntries;
          }
      }
      return newEntries;
    }

    private async Task DisplayMachines()
    {
      IsMachinesVisible = false;
      List<MachineEntry> machineEntries = await _machineService.GetMachineEntriesForPersonAsync(true, _state.StaffEntry.id);
      ObservableCollection<MachineEntry> newEntries = new();
      if (machineEntries != null)
        for (int i = machineEntries.Count() - 1; i >= 0; i--)
          newEntries.Add(machineEntries[i]);

      IsMachinesVisible = (machineEntries != null && machineEntries.Count > 0);
      MachineEntries = newEntries;
    }

    [RelayCommand]
    private void CloseReport()
    {
      Image = null;
      IsMonthlyReportVisible = false;
      IsWorktimeVisible = (WorktimeEntries != null && WorktimeEntries.Count > 0);
      IsMachinesVisible = (MachineEntries != null && MachineEntries.Count > 0);
      _stateMachine.OnCodeReceived(new UserAuthEvent(null));
    }

    private void HandlePdfMessage(object recipient, PdfMessage message)
    {
      if (!string.IsNullOrEmpty(message.Value))
      {
        SKImage skImage = SKImage.FromBitmap(PDFtoImage.Conversion.ToImage(message.Value));
        SKData encoded = skImage.Encode();
        using (MemoryStream memory = new MemoryStream())
        {
          encoded.AsStream().CopyTo(memory);
          Image = memory.ToArray();
          IsMonthlyReportVisible = true;
          IsWorktimeVisible = false;
          IsMachinesVisible = false;
        }
      }
      IsLoadingCircleVisible = false;
    }

    private void HandleDeliverMessage(object recipient, DeliverMessage message)
    {
      MainThread.BeginInvokeOnMainThread(async () => { await DoDeliver(message); });
    }

    private async Task DoDeliver(DeliverMessage message)
    {
      await _productionService.DoDeliver(message);
    }

    private void HandleSaldoMessage(object recipient, SaldoMessage message)
    {
      TimeBalance = message.Value.timeBalance.ToString();
      AvailableVacation = message.Value.availableVacation.ToString() + " " + message.Value.unitVacation.ToString();
      IsSaldoReportVisible = true;
      IsLoadingCircleVisible = false;
    }


    private void HandleChangeAmountMessage(object recipient, ChangeAmountMessage message)
    {
      MainThread.InvokeOnMainThreadAsync(async () => { await DoChangeAmount(message); });
    }

    private async Task DoChangeAmount(ChangeAmountMessage message)
    {
      await _productionService.DoChangeAmount(message);
    }

    [RelayCommand]
    async Task Settings()
    {
      try
      {
        IsLoadingCircleVisible = true;
        await SettingsHelper.ShowSettings(_settingsService.SettingsPassword);
      }
      finally { IsLoadingCircleVisible = false; }
    }

    [RelayCommand]
    async Task KommtBuchung()
    {
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Coming)));
    }

    [RelayCommand]
    async Task GehtBuchung()
    {
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Going)));
    }

    [RelayCommand]
    async Task PauseBuchung()
    {
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Pausing)));
    }

    [RelayCommand]
    async Task Saldo()
    {
      IsLoadingCircleVisible = true;
      IsBuchenAllowed = false;
      SaldoOffset = 0;
      IsSaldoIncreaseVisible = false;
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Balance)));
      //      IsLoadingCircleVisible = false;
    }

    [RelayCommand]
    private async Task Requests()
    {
      PopupRequestsResult res = (PopupRequestsResult)await Application.Current.MainPage.ShowPopupAsync(new RequestsPopup(_settingsService));
      if (res != null && !res.Canceled)
      {
        string toastMessage = string.Empty;
        switch (res.Type)
        {
          case SpecialTimeType.Holiday:
            toastMessage = string.Format("Vacation requested from {0} to {1}", DateTimeHelper.FromMillisecondsSinceUnixEpoch(res.Entry.fromDateMs), DateTimeHelper.FromMillisecondsSinceUnixEpoch(res.Entry.toDateMs));
            break;
          case SpecialTimeType.TimeCompensation:
            toastMessage = string.Format("Time compensation requested from {0} to {1}", DateTimeHelper.FromMillisecondsSinceUnixEpoch(res.Entry.fromDateMs), DateTimeHelper.FromMillisecondsSinceUnixEpoch(res.Entry.toDateMs));
            break;
          case SpecialTimeType.Illness:
            toastMessage = string.Format("Sick leave requested from {0} to {1}", DateTimeHelper.FromMillisecondsSinceUnixEpoch(res.Entry.fromDateMs), DateTimeHelper.FromMillisecondsSinceUnixEpoch(res.Entry.toDateMs));
            break;
        }

        BookingResult result = await _workTimeService.BookSpecialTime(toastMessage, _state.StaffEntry.id, res.Entry);
        if (result.Success)
        {
          _messageService.ShowToast(toastMessage, CommunityToolkit.Maui.Core.ToastDuration.Long);
          await _stateMachine.OnCodeReceived(new UserAuthEvent(null));
        }
      }
    }

    [RelayCommand]
    private async Task PieceConfirmation()
    {
      IsLoadingCircleVisible = true;
      IsBuchenAllowed = false;
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.PieceConfirmation)));
      IsBuchenAllowed = true;
    }

    [RelayCommand]
    private async Task ProductionOverview()
    {
      //var existingPages = Shell.Current.Navigation.NavigationStack.ToList();
      var parameter = new Dictionary<string, object> { { nameof(StaffEntry), _state.StaffEntry }, { nameof(MachineEntries), MachineEntries.ToList() } };
      await MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync("//" + nameof(ProductionOverviewPage), parameter); });
    }

    [RelayCommand]
    private async Task MachineList()
    {
      await MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync(nameof(MachineListPage), new Dictionary<string,object>() { [nameof(StaffEntry)] = _state.StaffEntry }); });
    }

    [RelayCommand]
    private async Task SaldoIncrease()
    {
      IsLoadingCircleVisible = true;
      SaldoOffset += 1;
      IsSaldoIncreaseVisible = !(SaldoOffset == 0);
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.MonthlyReport, true, SaldoOffset)));
    }

    [RelayCommand]
    private async Task SaldoDecrease()
    {
      IsLoadingCircleVisible = true;
      SaldoOffset -= 1;
      IsSaldoIncreaseVisible = !(SaldoOffset == 0);
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.MonthlyReport, true, SaldoOffset)));
    }

    [RelayCommand]
    private async Task SaldoDetails()
    {
      IsSaldoReportVisible = false;
      SaldoOffset = 0;
      IsSaldoIncreaseVisible = false;
      IsLoadingCircleVisible = true;
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.MonthlyReport, true, 0)));
    }

    [RelayCommand]
    private async Task SaldoClose()
    {
      IsSaldoReportVisible = false;
      await Task.Run(() => _stateMachine.OnCodeReceived(new UserAuthEvent(null)));
    }

    [RelayCommand]
    private async Task GotoStaffView()
    {
      bool valid = _isOnline ? !string.IsNullOrEmpty(_userService.Userid) : _staffService.GetStaffCountOffline() > 0;
      if (_settingsService.CheckInPage && valid && (_state == null || _state?.Type == StateType.Unidentified))
      {
        if (Shell.Current.CurrentPage is not StaffPage)
        {
          await Task.Delay(200);
          await MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync("///" + nameof(StaffPage)); });
        }
      }
      else if (_settingsService.CheckInPage && !string.IsNullOrEmpty(_state?.UserId) && Shell.Current.CurrentPage is not MainPage)
        await MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync("///" + nameof(MainPage)); });
    }

    [RelayCommand]
    private async Task StoreSizeAndPosition()
    {
      if (await Application.Current.MainPage.DisplayAlert("", SaveWindowPosition, Yes, No))
      {
        var window = Application.Current.MainPage.Window;
        _settingsService.WindowSize = new Size(window.Width, window.Height);
        _settingsService.WindowPosition = new Point(window.X, window.Y);
      }
    }

    private async void OnStateChange(State state)
    {
      _state = state;
      IsBuchenAllowed = (state.Type == StateType.Identified);
      if (state.StaffEntry != null)
      {
        Name = _staffService.GetFormatedStaffName(state.StaffEntry);
        DisplayTimeDistributions();
        DisplayBookings();
        DisplayMachines();
      }
      else
      {
        Name = null;
        IsTimeDistributionsVisible = false;
        IsWorktimeVisible = false;
        IsMachinesVisible = false;
      }
      if (Shell.Current.CurrentPage is ProductionDetailPage || Shell.Current.CurrentPage is ProductionOverviewPage || Shell.Current.CurrentPage is ProductionDocumentsPage) return;
      Debug.Print("Goto from " + Shell.Current.CurrentPage.GetType().Name);
      await GotoStaffView();
    }
  }
}
