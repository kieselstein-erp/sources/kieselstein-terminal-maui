﻿using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Models.ScanLabels;
using KieselsteinErp.Terminal.Popups;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.PurchaseOrder;
using KieselsteinErp.Terminal.Services.Readers;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.StateMachine;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.ViewModels.App
{
  public partial class PurchaseOrderPageViewModel : ObservableObject
  {
    private readonly IMessageService _messageService;
    private readonly ISettingsService _settingsService;
    private readonly IStateMachine _stateMachine;
    private readonly IDeviceManager _deviceManager;
    private readonly IPurchaseOrderRestService _purchaseOrderRestService;

    [ObservableProperty]
    private string _orderNumber;
    [ObservableProperty]
    private bool _isLoadingCircleVisible;
    [ObservableProperty]
    private bool _isOrderValid =false;
    [ObservableProperty]
    private List<string> _searchResults;
    [ObservableProperty]
    private bool _IsListVisible = false;
    [ObservableProperty]
    private string _selectedItem;
    [ObservableProperty]
    private PurchaseOrderEntry _purchaseOrderEntry;
    [ObservableProperty]
    private string _deliveryNoteNumber;
    [ObservableProperty]
    private bool _isBookingAllowed = false;

    ObservableCollection<PurchaseOrderPosition> _purchaseOrderPositions = new();
    public ObservableCollection<PurchaseOrderPosition> PurchaseOrderPositions { get { return _purchaseOrderPositions; } }

    private List<PurchaseOrderEntry> _purchaseOrders;
    private int? _goodsReceiptId = null;

    public PurchaseOrderPageViewModel(IMessageService messageService, ISettingsService settingsService, IStateMachine stateMachine, IDeviceManager deviceManager, IPurchaseOrderRestService purchaseOrderRestService)
    {
      _messageService = messageService;
      _settingsService = settingsService;
      _stateMachine = stateMachine;
      _deviceManager = deviceManager;
      _purchaseOrderRestService = purchaseOrderRestService;

      this.PropertyChanged += OnPropertyChanged;

    }

    private void HandleItemMessage(object recipient, ItemVDAMessage message)
    {
      MainThread.InvokeOnMainThreadAsync(async () =>
      {
        await DoHandleItemVda(message);
      });
    }

    private void HandleItemMessage(object recipient, ItemMessage message)
    {
      MainThread.InvokeOnMainThreadAsync(async () =>
      {
        if (IsOrderValid)
          await DoHandleItem(message);
      });
    }

    private async Task DoHandleItem(ItemMessage message)
    {
      if (!IsBookingAllowed)
      {
        if (PurchaseOrderEntry == null)
          _messageService.Info(PleaseSelectAnPurchaseOrder);
        else
          _messageService.Info(PleaseEnterDeliverNoteNumberFirst);
        return;
      }

      PurchaseOrderPosition purchaseOrderPosition = PurchaseOrderPositions.FirstOrDefault(o => o.itemEntry.cnr == message.Value.ItemCnr);
      if (purchaseOrderPosition == null)
        _messageService.Info(string.Format(NoOrderPositionForItem0, message.Value.ItemCnr));
      else
      {
        decimal amount = 0;
        if (!purchaseOrderPosition.itemEntry.hasSerialnr && !purchaseOrderPosition.itemEntry.hasChargenr)
        {
          // book
          await Deliver(purchaseOrderPosition);
          return;
        }
        
        CreateGoodsReceiptPositionEntry createGoodsReceiptPositionEntry = BuildCreateGoodsReceiptpositionEntry(purchaseOrderPosition);
        if (purchaseOrderPosition.itemEntry.hasSerialnr && !string.IsNullOrEmpty(message.Value.BatchNr))
        {
          // book with serial number
          amount = AddIdentitiesToCreateGoodsReceiptPosition(amount, createGoodsReceiptPositionEntry, new List<IdentityAmountEntry> { new IdentityAmountEntry { amount = 1, identity = message.Value.BatchNr } });
        }
        else if (purchaseOrderPosition.itemEntry.hasChargenr && !string.IsNullOrEmpty(message.Value.BatchNr))
        {
          if (message.Value.Amount != 0)
          {
            // book woth batch number and amount
            amount = AddIdentitiesToCreateGoodsReceiptPosition(amount, createGoodsReceiptPositionEntry, new List<IdentityAmountEntry> { new IdentityAmountEntry { amount = message.Value.Amount, identity = message.Value.BatchNr } });
          }
        }
        else if (purchaseOrderPosition.itemEntry.hasSerialnr || purchaseOrderPosition.itemEntry.hasChargenr)
        {
          WeakReferenceMessenger.Default.UnregisterAll(this);
          // show popup
          var res = await Application.Current.MainPage.ShowPopupAsync(new IdentityAmountEntryPopup(_messageService, false, purchaseOrderPosition.itemEntry.hasChargenr, purchaseOrderPosition.itemEntry.cnr, null, null));
          List<IdentityAmountEntry> result = (List<IdentityAmountEntry>)res;
          if (result != null && result.Count > 0)
          {
            amount = AddIdentitiesToCreateGoodsReceiptPosition(amount, createGoodsReceiptPositionEntry, result);
          }
          RegisterBarcodeMessages();
        }
        if (amount != 0)
          await Book(purchaseOrderPosition, createGoodsReceiptPositionEntry);
      }
    }

    private async Task DoHandleItemVda(ItemVDAMessage message)
    {
      VdaItem item = message.Value.ScanLabel.ToVdaItem();
      string orderNumber = item.PurchaseOrderNumberUnformatted();
      if (!string.IsNullOrEmpty(orderNumber) && OrderNumber != null && orderNumber != OrderNumber)
      {
        var ret = await Application.Current.MainPage.DisplayAlert(item.Cnr, string.Format(TheOrderNumberOfTheItemIsDifferentGoToPurchaseOrder0, item.PurchaseOrderNumber), Yes, Cancel);
        if (!ret) return;
      }
      if (!string.IsNullOrEmpty(orderNumber) && (OrderNumber == null || orderNumber != OrderNumber))
      {
        ClearAll();
        _purchaseOrders = await _purchaseOrderRestService.GetPurchaseOrdersAsync(orderNumber);
        if (_purchaseOrders == null || _purchaseOrders.Count == 0)
        {
          _messageService.Warn(string.Format(PurchaseOrder0NotFound, item.PurchaseOrderNumber));
          OrderNumber = null;
          ClearAll();
          return;
        }
        else if (_purchaseOrders.Count == 1)
        {
          OrderNumber = _purchaseOrders[0].cnr;
        }
        else
        {
          _messageService.Error(string.Format(MultiplePurchaseOrdersForNumber0, orderNumber));
          return;
        }
        IsOrderValid = await LoadPurchaseOrderPositions();
      }
      if (!IsOrderValid)
      {
        _messageService.Warn(PleaseSelectAnPurchaseOrder);
        return;
      }

      PurchaseOrderPosition purchaseOrderPosition = PurchaseOrderPositions.FirstOrDefault(o => o.itemEntry.cnr == item.Cnr);
      if (purchaseOrderPosition == null)
        _messageService.Info(string.Format(NoOrderPositionForItem0, item.Cnr));
      else
      {
        CreateGoodsReceiptPositionReelEntry createGoodsReceiptPositionReelEntry = new CreateGoodsReceiptPositionReelEntry()
        {
          amount = item.Amount,
          purchaseOrderPositionId = purchaseOrderPosition.id,
          deliverySlipCnr = item.DeliverNoteNumber,
          dateCode = item.DateCode,
          expirationDate = item.BatchNumber1,
          barcode = message.Value.ScanLabel.RawData,
          goodsReceiptId = null
        };
        if (item.DeliverNoteNumber != null)
          DeliveryNoteNumber = item.DeliverNoteNumber;

        if (item.Amount != 0)
          await Book(purchaseOrderPosition, createGoodsReceiptPositionReelEntry);
      }
    }

    [RelayCommand]
    private void Appearing()
    {
      Debug.Print($"Appearing {this.GetType().Name}");
      OrderNumber = null;
      ClearAll();
      RegisterBarcodeMessages();
    }

    private void RegisterBarcodeMessages()
    {
      WeakReferenceMessenger.Default.Register<ItemMessage>(this, HandleItemMessage);
      WeakReferenceMessenger.Default.Register<ItemVDAMessage>(this, HandleItemMessage);
      WeakReferenceMessenger.Default.Register<UnidentifiedBarcodeMessage>(this, HandleUnidenifiedMessage);
      WeakReferenceMessenger.Default.Register<PurchaseOrderNumberMessage>(this, HandlePurchaseOrderNumberMessage);
    }

    private void HandlePurchaseOrderNumberMessage(object recipient, PurchaseOrderNumberMessage message)
    {
      MainThread.InvokeOnMainThreadAsync(async () =>
      {
        ClearAll();
        await DoHandleUnidentified(message.Value);
      });
    }

    private void HandleUnidenifiedMessage(object recipient, UnidentifiedBarcodeMessage message)
    {
      MainThread.InvokeOnMainThreadAsync(async () =>
      {
        await DoHandleUnidentified(message.Value);
      });
    }

    private async Task DoHandleUnidentified(string code) {
      if (IsOrderValid) return;

      if (code.StartsWith("BS"))
        code = code[2..].Trim();

      if (!string.IsNullOrEmpty(code))
      {
        OrderNumber = code;
        List<PurchaseOrderEntry> purchaseOrders = await _purchaseOrderRestService.GetPurchaseOrdersAsync(code + "%");
        if (purchaseOrders != null && purchaseOrders.Count == 1)
        {
          IsOrderValid = await LoadPurchaseOrderPositions(purchaseOrders[0]);
          if (IsOrderValid)
            OrderNumber = purchaseOrders[0].cnr;
        }
        else if (purchaseOrders != null && purchaseOrders.Count > 1)
        {
          await SearchAsync(code);
          return;
        }

        if (!IsOrderValid)
          _messageService.Warn(string.Format(PurchaseOrder0NotFound, code));
      }
    }

    [RelayCommand]
    private void Disappearing() 
    {
      Debug.Print($"Disappearing {this.GetType().Name}");
      WeakReferenceMessenger.Default.UnregisterAll(this);
    }

    private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (e.PropertyName == nameof(OrderNumber))
        if (string.IsNullOrEmpty(OrderNumber))
          ClearAll();

      if (e.PropertyName == nameof(SearchResults))
        IsListVisible = (SearchResults == null ? false : SearchResults.Count > 0);

      if (e.PropertyName == nameof(DeliveryNoteNumber))
      {
        IsBookingAllowed = !string.IsNullOrEmpty(DeliveryNoteNumber);
        _goodsReceiptId = null;
      }
    }

    private void ClearAll()
    {
      //OrderNumber = null;
      SearchResults = null;
      PurchaseOrderEntry = null;
      _purchaseOrders = null;
      _goodsReceiptId = null;
      PurchaseOrderPositions.Clear();
      IsLoadingCircleVisible = false;
      IsOrderValid = false;
      DeliveryNoteNumber = null;
    }

    [RelayCommand]
    private async Task SearchAsync(string text)
    {
      ClearAll();
      _purchaseOrders = await _purchaseOrderRestService.GetPurchaseOrdersAsync(text + "%");
      if (_purchaseOrders != null)
      {
        if (_purchaseOrders.Count > 0)
        {
          List<string> items = new List<string>();
          foreach (var item in _purchaseOrders)
            items.Add(item.cnr + " " + item.supplierName);
          SearchResults = items;
        }
        else
        {
          _messageService.Info(NoOrdersWereFoundForTheSearchTerm);
        }

#if ANDROID
        var activity = Platform.CurrentActivity;
        var token = activity.CurrentFocus?.WindowToken;
        if (activity.HasWindowFocus)
          activity.Window.DecorView.ClearFocus();
#endif
      }
    }



    [RelayCommand]
    private async Task ListTapped()
    {
      string[] s = SelectedItem.Split(' ');
      OrderNumber = s[0].Trim();
      IsOrderValid = await LoadPurchaseOrderPositions();
      SearchResults = null;
    }

    private async Task<bool> LoadPurchaseOrderPositions(PurchaseOrderEntry purchaseOrderEntry = null)
    {
      if (!string.IsNullOrEmpty(OrderNumber))
      {
        if (purchaseOrderEntry == null)
          PurchaseOrderEntry = _purchaseOrders.FirstOrDefault(o => o.cnr == OrderNumber);
        else
          PurchaseOrderEntry = purchaseOrderEntry;

        if (PurchaseOrderEntry != null)
        {
          List<PurchaseOrderPositionEntry> purchaseOrderPositions = await _purchaseOrderRestService.GetPurchaseOrderItems(PurchaseOrderEntry.id);
          if (purchaseOrderPositions != null)
          {
            foreach (var position in purchaseOrderPositions)
            {
              PurchaseOrderPositions.Add(new PurchaseOrderPosition(position));
            }
            return true;
          }
        }
      }
      return false;
    }

    [RelayCommand]
    private async Task Scan()
    {
      if (_deviceManager.IsScanDevice())
        _deviceManager.PressScanButton(true);
      else
        await Application.Current.MainPage.ShowPopupAsync(new BarcodeCameraPopup());
    }

    [RelayCommand]
    private async Task Deliver(PurchaseOrderPosition purchaseOrderPosition)
    {
      decimal amount = 0;
      if (!IsBookingAllowed)
        _messageService.Info(PleaseEnterDeliverNoteNumberFirst);
      else
      {
        Debug.Print(purchaseOrderPosition.id.ToString());

        CreateGoodsReceiptPositionEntry createGoodsReceiptPositionEntry = BuildCreateGoodsReceiptpositionEntry(purchaseOrderPosition);

        if (purchaseOrderPosition.itemEntry.hasSerialnr || purchaseOrderPosition.itemEntry.hasChargenr)
        {
          var res = await Application.Current.MainPage.ShowPopupAsync(new IdentityAmountEntryPopup(_messageService, false, purchaseOrderPosition.itemEntry.hasChargenr, purchaseOrderPosition.itemEntry.cnr, null, null));
          List<IdentityAmountEntry> result = (List<IdentityAmountEntry>)res;
          if (result != null && result.Count > 0)
          {
            amount = AddIdentitiesToCreateGoodsReceiptPosition(amount, createGoodsReceiptPositionEntry, result);
          }
        }
        else
        {
          var res = await Application.Current.MainPage.ShowPopupAsync(new DecimalEntryPopup(_messageService, 3, purchaseOrderPosition.openQuantity, string.Empty));
          PopupResult result = (PopupResult)res;
          if (!result.Canceled)
          {
            amount = Convert.ToDecimal(result.Entry);
            createGoodsReceiptPositionEntry.amount = amount;
          }
        }
        if (amount != 0)
        {
          await Book(purchaseOrderPosition, createGoodsReceiptPositionEntry);
        }
      }
    }

    private static decimal AddIdentitiesToCreateGoodsReceiptPosition(decimal amount, CreateGoodsReceiptPositionEntry createGoodsReceiptPositionEntry, List<IdentityAmountEntry> result)
    {
      createGoodsReceiptPositionEntry.itemIdentity = new ItemIdentityEntryList();
      List<ItemIdentityEntry> entries = [];
      foreach (var item in result)
      {
        entries.Add(new ItemIdentityEntry { identity = item.identity, amount = item.amount });
        amount += item.amount;
      }
      createGoodsReceiptPositionEntry.itemIdentity.entries = entries;
      createGoodsReceiptPositionEntry.amount = amount;
      return amount;
    }

    private CreateGoodsReceiptPositionEntry BuildCreateGoodsReceiptpositionEntry(PurchaseOrderPositionEntry purchaseOrderPositionEntry)
    {
      return new CreateGoodsReceiptPositionEntry()
      {
        deliverySlipCnr = DeliveryNoteNumber,
        purchaseOrderPositionId = purchaseOrderPositionEntry.id,
        goodsReceiptId = _goodsReceiptId
      };
    }

    private async Task Book(PurchaseOrderPosition purchaseOrderPosition, CreateGoodsReceiptPositionEntry createGoodsReceiptPositionEntry)
    {
      CreatedGoodsReceiptPositionEntry createdGoodsReceiptPositionEntry = await _purchaseOrderRestService.CreateGoodsReceiptPosition(createGoodsReceiptPositionEntry);
      if (createdGoodsReceiptPositionEntry != null)
      {
        purchaseOrderPosition.OpenAmount = createdGoodsReceiptPositionEntry.openQuantity;
        _goodsReceiptId = createdGoodsReceiptPositionEntry.goodsReceiptId;
        _messageService.ShowToast(string.Format(GoodsReceiptSavedFor0, purchaseOrderPosition.itemEntry.cnr), CommunityToolkit.Maui.Core.ToastDuration.Short);
      }
    }

    private async Task Book(PurchaseOrderPosition purchaseOrderPosition, CreateGoodsReceiptPositionReelEntry createGoodsReceiptPositionReelEntry)
    {
      CreatedGoodsReceiptPositionReelEntry createdGoodsReceiptPositionReelEntry = await _purchaseOrderRestService.CreateGoodsReceiptPosition(createGoodsReceiptPositionReelEntry);
      if (createdGoodsReceiptPositionReelEntry != null)
      {
        purchaseOrderPosition.OpenAmount = createdGoodsReceiptPositionReelEntry.openQuantity;
        _goodsReceiptId = createdGoodsReceiptPositionReelEntry.goodsReceiptId;
        _messageService.ShowToast(string.Format(GoodsReceiptSavedFor0, purchaseOrderPosition.itemEntry.cnr), CommunityToolkit.Maui.Core.ToastDuration.Short);
      }
    }

  }
}
