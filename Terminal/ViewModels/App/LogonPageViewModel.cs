﻿using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Popups;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Audio;
using KieselsteinErp.Terminal.Services.Readers;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.Staff;
using KieselsteinErp.Terminal.Services.StateMachine;
using KieselsteinErp.Terminal.Services.User;
using KieselsteinErp.Terminal.Util;
using KieselsteinErp.Terminal.Views.App;
using Microsoft.Extensions.Logging;
using System.ComponentModel;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.ViewModels.App
{
  public partial class LogonPageViewModel : ObservableObject
  {
    private readonly IUserService _userService;
    private readonly IStaffService _staffService;
    private readonly IMessageService _messageService;
    private readonly ISettingsService _settingsService;
    private readonly IStateMachine _stateMachine;
    private readonly IDeviceManager _deviceManager;
    private readonly IAudioService _audioService;
    private readonly ILogger<LogonPageViewModel> _logger;

    [ObservableProperty]
    private string _username;

    [ObservableProperty]
    private string _password;

    [ObservableProperty]
    private bool _enableLogon = false;

    [ObservableProperty]
    private bool _isLoadingCircleVisible = false;

    public LogonPageViewModel(IUserService userService, IStaffService staffService, IMessageService messageService, ISettingsService settingsService, 
      IStateMachine stateMachine, IDeviceManager deviceManager, IAudioService audioService, ILogger<LogonPageViewModel> logger)
    {
      _userService = userService;
      _staffService = staffService;
      _messageService = messageService;
      _settingsService = settingsService;
      _stateMachine = stateMachine;
      _deviceManager = deviceManager;
      _audioService = audioService;
      _logger = logger;

      this.PropertyChanged += OnPropertyChanged;

      _userService.OnLogonChanged += (s, e) =>
      {
        IsLoadingCircleVisible = true;
        if (e)
        {
          StaffEntry staff = _staffService.FindStaffByUserid(_userService.Userid);
          if (staff != null)
          {
            if (staff.identityCnr == null)
              _messageService.Error(LogonPageViewModel_UserIsNotAllowedToLogonIdentityNull);
            else
            {
              _stateMachine.OnCodeReceived(new UserAuthEvent(staff.identityCnr));
              MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync("//" + nameof(AppMenuPage)); });
            }
          } 
          else
            _messageService.Error(NoStaffForUserFound);
        }
        IsLoadingCircleVisible = false;
      };
      if (MauiProgram.CurrentCulture != null && MauiProgram.DefaultCulture != null)
      {
        if (MauiProgram.CurrentCulture != MauiProgram.DefaultCulture)
        {
          _logger.LogWarning(string.Format(SystemCulture0ShouldBeEqualUICulture1, MauiProgram.DefaultCulture.DisplayName, MauiProgram.CurrentCulture.DisplayName));
        }
      }
    }

    [RelayCommand]
    private void Appearing()
    {
      WeakReferenceMessenger.Default.Register<UserLogonMessage>(this, HandleLogonMessage);
      WeakReferenceMessenger.Default.Register<VersionMessage>(this, HandleVersionMessage);
    }

    private async void HandleVersionMessage(object recipient, VersionMessage message)
    {
      await MainThread.InvokeOnMainThreadAsync(() =>
      {
        _messageService.ShowToast(message.Value + $"\r\n({message.ActualVersion} < {message.MinVersion})", CommunityToolkit.Maui.Core.ToastDuration.Long);
      });
    }

    [RelayCommand]
    private void Disappearing()
    {
      WeakReferenceMessenger.Default.UnregisterAll(this);
    }

    private void HandleLogonMessage(object recipient, UserLogonMessage message)
    {
      IsLoadingCircleVisible = true;
      
      if (!string.IsNullOrEmpty(message.Value))
      {
        if (message.Value.Contains("|"))
        {
          string[] s = message.Value.Split('|');
          Username = s[0];
          Password = LoginHelper.Decrypt(Username, LoginHelper.FromHex(s[1]));
          if (!string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password))
            Logon();
        }
      }
      IsLoadingCircleVisible = false;
    }

    private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (!string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password))
        EnableLogon = true;
      else
        EnableLogon = false;
    }


    [RelayCommand]
    private void Logon()
    {
      IsLoadingCircleVisible = true;
      _userService.LogonApp(Username, Password);
      Password = string.Empty;
      IsLoadingCircleVisible = false;
    }

    [RelayCommand]
    async Task Settings()
    {
      await SettingsHelper.ShowSettings(_settingsService.SettingsPassword);
    }

    [RelayCommand]
    async Task Scan()
    {
      if (_deviceManager.IsScanDevice())
        _deviceManager.PressScanButton(true);
      else
        await Application.Current.MainPage.ShowPopupAsync(new BarcodeCameraPopup());
    }
  }
}
