﻿using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Popups;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Production;
using KieselsteinErp.Terminal.Services.Readers;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.StateMachine;
using KieselsteinErp.Terminal.Services.Worktime;
using System.ComponentModel;

namespace KieselsteinErp.Terminal.ViewModels.App
{
  public partial class ProductionPageViewModel : ObservableObject
  {
    private readonly IMessageService _messageService;
    private readonly ISettingsService _settingsService;
    private readonly IProductionRestService _productionRestService;
    private readonly IStateMachine _stateMachine;
    private readonly IDeviceManager _deviceManager;
    private readonly IProductionService _productionService;

    [ObservableProperty]
    private string _lotNumber;
    [ObservableProperty]
    private string _itemCnr;
    [ObservableProperty]
    private string _status;
    [ObservableProperty]
    private decimal? _quantity;
    [ObservableProperty]
    private decimal? _deliveredAmount;
    [ObservableProperty]
    private bool _isLotValid = false;
    [ObservableProperty]
    private bool _IsListVisible = false;
    [ObservableProperty]
    private List<string> _searchResults;
    [ObservableProperty]
    private string _selectedItem;
    [ObservableProperty]
    private bool _isLoadingCircleVisible;

    private ProductionEntry _productionEntry;

    public ProductionPageViewModel(IMessageService messageService, ISettingsService settingsService, IProductionRestService productionRestService, IStateMachine stateMachine, IDeviceManager deviceManager, IProductionService productionService)
    {
      _messageService = messageService;
      _settingsService = settingsService;
      _productionRestService = productionRestService;
      _stateMachine = stateMachine;
      _deviceManager = deviceManager;
      _productionService = productionService;

      this.PropertyChanged += OnPropertyChanged;

    }

    [RelayCommand]
    private void Appearing()
    {
      WeakReferenceMessenger.Default.Register<DeliverMessage>(this, HandleDeliverMessage);
      WeakReferenceMessenger.Default.Register<LotMessage>(this, HandleLotMessage);
      WeakReferenceMessenger.Default.Register<ChangeAmountMessage>(this, HandleChangeAmountMessage);
    }

    [RelayCommand]
    private void Disappearing()
    {
      WeakReferenceMessenger.Default.UnregisterAll(this);
      ClearAll();
      LotNumber = null;
    }

    private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (e.PropertyName == nameof(LotNumber))
        if (string.IsNullOrEmpty(LotNumber))
          ClearAll();

      if (e.PropertyName == nameof(SearchResults))
        IsListVisible = (SearchResults == null ? false : SearchResults.Count > 0);
    }

    private void ClearAll()
    {
      _productionEntry = null;
      SearchResults = null;
      IsLotValid = false;
      SetValues();
      IsLoadingCircleVisible = false;
    }

    private void HandleChangeAmountMessage(object recipient, ChangeAmountMessage message)
    {
      MainThread.BeginInvokeOnMainThread(async () => { await DoChangeAmount(message); });
    }

    private async Task DoChangeAmount(ChangeAmountMessage message)
    {
      IsLoadingCircleVisible = false;
      await _productionService.DoChangeAmount(message);
      HandleLotMessage(this, null);
    }

    private void HandleLotMessage(object recipient, LotMessage message)
    {
      if (message == null)
      {
        LotNumber = null;
        _productionEntry = null;
        SetValues();
      }
      else
      {
        LotNumber = message.Value;
        _productionEntry = message.productionEntry;
        SetValues();
      }
    }

    private void SetValues()
    {
      IsLotValid = (_productionEntry != null);
      ItemCnr = _productionEntry?.itemEntry?.cnr;
      Status = _productionEntry?.status;
      Quantity = _productionEntry?.amount;
      DeliveredAmount = _productionEntry?.deliveredAmount;
    }

    private void HandleDeliverMessage(object recipient, DeliverMessage message)
    {
      MainThread.BeginInvokeOnMainThread(async () => { await DoDeliver(message); });
    }

    private async Task DoDeliver(DeliverMessage message)
    {
      IsLoadingCircleVisible = false;
      await _productionService.DoDeliver(message, _productionEntry);
      HandleLotMessage(this, null);
    }

    [RelayCommand]
    private void ChangeLotQuantity()
    {
      IsLoadingCircleVisible = true;
      _stateMachine.OnCodeReceived(new ProductionEvent(ProductionEventType.ChangeAmount));
    }

    [RelayCommand]
    private void Deliver()
    {
      IsLoadingCircleVisible = true;
      _stateMachine.OnCodeReceived(new ProductionEvent(ProductionEventType.Deliver));
    }

    [RelayCommand]
    private async Task SearchAsync(string text)
    {
      IsLotValid = false;
      List<ProductionEntry> productionEntries = await _productionRestService.GetProductionEntries(text + "%", 50);
      if (productionEntries != null)
      {
        List<string> items = new List<string>();
        foreach (var item in productionEntries)
          items.Add(item.cnr + " " + item.itemCnr);
        SearchResults = items;

#if ANDROID
        var activity = Platform.CurrentActivity;
        var token = activity.CurrentFocus?.WindowToken;
        if (activity.HasWindowFocus)
          activity.Window.DecorView.ClearFocus();
#endif
      }
    }

    [RelayCommand]
    private async Task ListTapped()
    {
      string[] s = SelectedItem.Split(' ');
      LotNumber = s[0].Trim();
      if (!string.IsNullOrEmpty(LotNumber))
      {
        await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Lot, true, LotNumber));
      }
      SearchResults = null;
    }

    [RelayCommand]
    private async Task Scan()
    {
      if (_deviceManager.IsScanDevice())
        _deviceManager.PressScanButton(true);
      else
        await Application.Current.MainPage.ShowPopupAsync(new BarcodeCameraPopup());
    }
  }
}
