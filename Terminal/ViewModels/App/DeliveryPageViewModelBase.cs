﻿using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Models.ScanLabels;
using KieselsteinErp.Terminal.Popups;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Delivery;
using KieselsteinErp.Terminal.Services.Item;
using KieselsteinErp.Terminal.Services.Readers;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.ViewModels.App
{
  public enum PrintType
  {
    PRINT_NOTE,
    PRINT_LABEL,
    PRINT_GOODS_ISSUE
  }

  public abstract partial class DeliveryPageViewModelBase : ObservableObject
  {
    protected readonly IMessageService _messageService;
    protected readonly IDeviceManager _deviceManager;
    protected readonly IDeliveryService _deliveryService;
    protected readonly IItemV1Service _itemV1Service;

    [ObservableProperty]
    private bool _isLoadingCircleVisible;
    [ObservableProperty]
    private bool _isBookingAllowed = false;
    [ObservableProperty]
    private int _deliveryNoteIndex = -1;
    [ObservableProperty]
    private bool _isListVisible = false;
    [ObservableProperty]
    private List<string> _searchResults;
    [ObservableProperty]
    private string _selectedItem;
    [ObservableProperty]
    private bool _isDeliveryNoteValid = false;
    [ObservableProperty]
    private string _deliveryNoteNumber = null;
    [ObservableProperty]
    private DeliveryEntry _deliveryEntry = null;

    protected DeliverableOrderEntry _deliverableOrderEntry;
    protected ObservableCollection<string> _deliverOrderNumbers = [];
    public ObservableCollection<string> DeliverOrderNumbers { get { return _deliverOrderNumbers; } }

    protected DeliveryPageViewModelBase(IMessageService messageService, IDeviceManager deviceManager, IDeliveryService deliveryService, IItemV1Service itemV1Service)
    {
      _messageService = messageService;
      _deviceManager = deviceManager;
      _deliveryService = deliveryService;
      _itemV1Service = itemV1Service;
    }

    DeliveryPageViewModelBase() { }

    [RelayCommand]
    private void Disappearing()
    {
      WeakReferenceMessenger.Default.UnregisterAll(this);
    }

    protected virtual void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (e.PropertyName == nameof(SearchResults))
        IsListVisible = (SearchResults != null && SearchResults.Count > 0);

      if (e.PropertyName == nameof(DeliveryNoteNumber))
        IsBookingAllowed = !string.IsNullOrEmpty(DeliveryNoteNumber);

      if (e.PropertyName == nameof(DeliveryNoteIndex))
        IsDeliveryNoteValid = (DeliveryNoteIndex > 0 || (DeliveryNoteIndex == -1 && string.IsNullOrWhiteSpace(DeliveryNoteNumber)));
    }

    protected int? GetDeliveryId()
    {
      int? deliveryId = null;
      if (DeliveryNoteIndex > 0)
        deliveryId = _deliverableOrderEntry.deliveryEntries.entries[DeliveryNoteIndex - 1].id;
      else if (DeliveryNoteIndex == -1 && DeliveryEntry != null)
        deliveryId = DeliveryEntry.id;
      return deliveryId;
    }

    [RelayCommand]
    private async Task Print(PrintType type)
    {
      PrintResult res = null;
      string msg = string.Empty;
      if (GetDeliveryId() == null) return;

      int id = (int)GetDeliveryId();
      switch (type)
      {
        case PrintType.PRINT_NOTE:
          msg = DeliveryNotePrinted;
          res = await _deliveryService.Print(id);
          break;

        case PrintType.PRINT_LABEL:
          msg = DispatchLabelPrinted;
          res = await _deliveryService.PrintDispatchLabel(id, false);
          break;

        case PrintType.PRINT_GOODS_ISSUE:
          msg = GoodsIssueLabelPrinted;
          res = await _deliveryService.PrintGoodsIssueLabel(id, false);
          break;
      }
      if (!res.Success)
      {
        if (res.StatusCode == HttpStatusCode.ExpectationFailed && res.ErrorCode != null && res.ErrorCode == 30)
          _messageService.Warn(TheStatusOfTheDeliveryNoteNEWDoesNotAllowPrinting);
        else if (!string.IsNullOrEmpty(res.Message))
          _messageService.Warn(res.Message);
        else
          _messageService.Warn(PrintingFailed);
      }
      else
      {
        if (type == PrintType.PRINT_NOTE)
          DeliveryNoteNumber = null;
        _messageService.ShowToast(msg, CommunityToolkit.Maui.Core.ToastDuration.Short);
      }
    }

    protected virtual CreateDeliveryPositionEntry BuildCreateDeliveryPositionEntry(int orderId, DeliverableOrderPositionEntry orderPosition, decimal amount)
    {
      int? deliveryId = GetDeliveryId();
      ItemIdentityEntryList itemIdentity = new()
      {
        entries = []
      };
      return new CreateDeliveryPositionEntry() { amount = amount, orderId = orderId, orderPositionId = orderPosition.id, deliveryId = deliveryId, itemIdentity = itemIdentity };
    }

    [RelayCommand]
    private async Task Scan()
    {
      if (_deviceManager.IsScanDevice())
        _deviceManager.PressScanButton(true);
      else
        await Application.Current.MainPage.ShowPopupAsync(new BarcodeCameraPopup());
    }

    protected async Task Book(DeliverableOrderPosition orderPosition, CreateDeliveryPositionEntry createDeliveryPositionEntry)
    {
      CreatedDeliveryPositionEntry createdDeliveryPositionEntry = await _deliveryService.DeliverPositionFromOrder(createDeliveryPositionEntry);
      if (createdDeliveryPositionEntry != null)
      {
        orderPosition.OpenAmount = createdDeliveryPositionEntry.openAmount;
        AddSetDeliveryNote(createdDeliveryPositionEntry.deliveryId, createdDeliveryPositionEntry.deliveryCnr);
        _messageService.ShowToast(string.Format(Item0HasBeenAddedToTheDeliveryNote, orderPosition.itemCnr), CommunityToolkit.Maui.Core.ToastDuration.Short);
      }
    }

    private void AddSetDeliveryNote(int deliveryId, string deliveryCnr)
    {
      int index = _deliverOrderNumbers.IndexOf(deliveryCnr);
      if (index == -1) // new delivery note
      {
        _deliverableOrderEntry.deliveryEntries.entries.Add(new DeliveryOrderEntry { deliveryCnr = deliveryCnr, id = deliveryId, status = DeliveryOrderEntrystatusConstants.OPEN });
        _deliverOrderNumbers.Add(deliveryCnr);
        DeliveryNoteIndex = _deliverOrderNumbers.IndexOf(deliveryCnr);
      }
      else
        DeliveryNoteIndex = index;
    }


    protected decimal AddIdentitiesToCreateDeliveryPosition(decimal amount, CreateDeliveryPositionEntry createDeliveryPositionEntry, List<IdentityAmountEntry> result)
    {
      decimal newamount = 0;
      createDeliveryPositionEntry.itemIdentity = new ItemIdentityEntryList();
      List<ItemIdentityEntry> entries = [];
      foreach (var item in result)
      {
        entries.Add(new ItemIdentityEntry { identity = item.identity, amount = item.amount });
        newamount += item.amount;
      }
      //Debug.Assert(newamount == amount);
      createDeliveryPositionEntry.itemIdentity.entries = entries;
      createDeliveryPositionEntry.amount = newamount;
      return newamount;
    }

    protected async Task<List<IdentityAmountEntry>> GetAvailableIdentitiesForItem(string itemCnr)
    {
      List<IdentityAmountEntry> availableIdentities = [];
      List<StockAmountEntry> itemStocks = await _itemV1Service.GetItemStocks(itemCnr);
      if (itemStocks != null && itemStocks.Count > 0)
      {
        foreach (var itemStock in itemStocks)
        {
          if (itemStock.itemIdentityList != null && itemStock.itemIdentityList.entries.Count > 0)
            foreach (var identity in itemStock.itemIdentityList.entries)
              availableIdentities.Add(new IdentityAmountEntry { amount = identity.amount, identity = identity.identity });
        }
      }

      return availableIdentities;
    }

    protected async Task<string> FindAvailableBatchNumber(VdaItem item)
    {
      string batchToUse = item.BatchNumberReelId();

      List<IdentityAmountEntry> identityAmountEntries = await GetAvailableIdentitiesForItem(item.Cnr);
      if (identityAmountEntries != null)
      {
        // has reelid batch => return default batch
        if (identityAmountEntries.Find(o => o.identity == batchToUse) != null) return batchToUse;

        // has batchnumber1 => return it
        if (identityAmountEntries.Find(o => o.identity == item.BatchNumber1) != null) return item.BatchNumber1;
      }
      // return default
      return batchToUse;
    }

  }
}
