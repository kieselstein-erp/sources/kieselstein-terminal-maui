﻿using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Popups;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Item;
using KieselsteinErp.Terminal.Services.Readers;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.StateMachine;
using KieselsteinErp.Terminal.Services.Stock;
using Microsoft.Extensions.Logging;
using System.Collections.ObjectModel;
using System.ComponentModel;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.ViewModels.App
{
  public partial class ItemsPageViewModel : ObservableObject
  {
    private readonly ILogger<ItemsPageViewModel> _logger;
    private readonly IMessageService _messageService;
    private readonly ISettingsService _settingsService;
    private readonly IStateMachine _stateMachine;
    private readonly IItemV1Service _itemV1Service;
    private readonly IDeviceManager _deviceManager;
    private readonly IStockService _stockService;

    [ObservableProperty]
    private string _itemCnr;
    [ObservableProperty]
    private List<ItemV1Entry> _searchResults;
    [ObservableProperty]
    private ItemV1Entry _selectedItem;
    [ObservableProperty]
    private bool _IsListVisible = false;
    [ObservableProperty]
    private bool _isLoadingCircleVisible;
    [ObservableProperty]
    private bool _isValid = false;
    [ObservableProperty]
    private ItemV1Entry _item;
    [ObservableProperty]
    private bool _isPlaceEditMode = false;
    [ObservableProperty]
    private bool _isStockInfoValid = false;
    [ObservableProperty]
    private StockInfo _actualStockInfo = null;
    [ObservableProperty]
    private bool _isBatchDetailVisible = false;
    [ObservableProperty]
    private bool _hasSerialOrBatch = false;
    [ObservableProperty]
    private bool _isStockPlaceListVisible = false;
    [ObservableProperty]
    private string _stockPlaceName = null;

    ObservableCollection<StockInfo> _stockInfos = new();
    public ObservableCollection<StockInfo> StockInfos { get { return _stockInfos; } }

    ObservableCollection<StockPlaceEntry> _stockPlaces = new();
    public ObservableCollection<StockPlaceEntry> StockPlaces { get { return _stockPlaces; } }

    ObservableCollection<ItemIdentityEntry> _itemIdentities = new();
    public ObservableCollection<ItemIdentityEntry> ItemIdentities { get { return _itemIdentities; } }

    ObservableCollection<StockStockPlaceEntry> _stockStockPlaces = new();
    public ObservableCollection<StockStockPlaceEntry> StockStockPlaces { get { return _stockStockPlaces; } }


    public ItemsPageViewModel(ILogger<ItemsPageViewModel> logger, IMessageService messageService, ISettingsService settingsService, IStateMachine stateMachine, IItemV1Service itemV1Service, IDeviceManager deviceManager, IStockService stockService)
    {
      _logger = logger;
      _messageService = messageService;
      _settingsService = settingsService;
      _stateMachine = stateMachine;
      _itemV1Service = itemV1Service;
      _deviceManager = deviceManager;
      _stockService = stockService;

      ClearAll();
      this.PropertyChanged += OnPropertyChanged;
    }

    [RelayCommand]
    private void Appearing()
    {
      ClearAll();
      ItemCnr = string.Empty;
      WeakReferenceMessenger.Default.Register<StockPlaceMessage>(this, HandleStockPlaceMessage);
      WeakReferenceMessenger.Default.Register<ItemMessage>(this, HandleItemMessage);
      WeakReferenceMessenger.Default.Register<ItemVDAMessage>(this, HandleItemMessage);
    }

    [RelayCommand]
    private void Disappearing()
    {
      WeakReferenceMessenger.Default.UnregisterAll(this);
    }

    private void HandleStockPlaceMessage(object recipient, StockPlaceMessage message)
    {
      MainThread.InvokeOnMainThreadAsync(async () =>
      {
        await DoHandleStockPlace(message.Value);
      });
    }

    private async Task DoHandleStockPlace(string placeName)
    {
      if (Item == null)
        GetAllStockplaceInfos(placeName);
      else if (ActualStockInfo == null)
        _messageService.Warn(DoHandleStockPlace_PleaseSelectStorage);
      else
        if (!CheckPlaceUsed(placeName))
        await DoAddPlace(ActualStockInfo.StockId, placeName);
    }

    private async void GetAllStockplaceInfos(string placeName)
    {
      try
      {
        IsLoadingCircleVisible = true;
        IsStockPlaceListVisible = false;
        StockPlaceName = placeName;
        StockStockPlaces.Clear();
        StockEntryList stocks = await _stockService.GetStockEntries();
        if (stocks == null)
          _messageService.Warn(NoStockInformation);
        foreach (StockEntry stock in stocks.entries)
        {
          StockPlaceEntry stockPlaceEntry = await _stockService.GetStockPlaceEntryByCnr(stock.id, placeName);
          if (stockPlaceEntry != null)
            StockStockPlaces.Add(new StockStockPlaceEntry(stock.id, stock.name, stockPlaceEntry));
        }
      }
      finally { IsLoadingCircleVisible = false; }

      if (StockStockPlaces.Count == 0)
        _messageService.Warn(string.Format(NoInformationFoundForStorageLocation0, placeName));
      else
        IsStockPlaceListVisible = true;
    }

    private async void UpdateStockplaceInfos(int stockId, string placeName, string itemCnr)
    {
      //All entries must be updated using this method because the quantity of ItemV1Entry is the total quantity and not only for stock
      StockPlaceEntry newStockPlaceEntry = await _stockService.GetStockPlaceEntryByCnr(stockId, placeName);
      if (newStockPlaceEntry != null)
      {
        ItemV1Entry itemV1Entry = newStockPlaceEntry.items.entries.FirstOrDefault(o => o.cnr == itemCnr);
        if (itemV1Entry != null)
        {
          StockStockPlaceEntry stockStockPlaceEntry = StockStockPlaces.FirstOrDefault(s => s.StockId == stockId);
          stockStockPlaceEntry?.StockItems.Add(new StockItem(stockId, newStockPlaceEntry.id, itemV1Entry));
        }
      }
    }

    private async void HandleItemMessage(object recipient, ItemMessage message)
    {
      if (IsStockPlaceListVisible)
        AddItemToPlace(message.Value.ItemCnr);
      else
      {
        await LoadItemInfo(message.Value.ItemCnr);
#if ANDROID
        MainThread.BeginInvokeOnMainThread(() => {
          var activity = Platform.CurrentActivity;
          var token = activity.CurrentFocus?.WindowToken;
          if (activity.HasWindowFocus)
            activity.Window.DecorView.ClearFocus();
        });
#endif
      }
    }

    private async void HandleItemMessage(object recipient, ItemVDAMessage message)
    {
      if (IsStockPlaceListVisible)
        AddItemToPlace(message.Value.ScanLabel.ToVdaItem().Cnr);
      else
        await LoadItemInfo(message.Value.ScanLabel.ToVdaItem().Cnr);
    }

    private async void AddItemToPlace(string itemCnr)
    {
      if (StockStockPlaces.Count == 1)
      {
        await AddItemToPlace(StockStockPlaces[0].StockId, itemCnr);
      }
      else
      {
        List<string> stockNames = [];
        foreach (StockStockPlaceEntry entry in StockStockPlaces)
        {
          stockNames.Add(entry.StockName);
        }
        string stockName = await Application.Current.MainPage.DisplayActionSheet(PleaseChooseStock, Cancel, null, [.. stockNames]);
        StockStockPlaceEntry stockStockPlaceEntry = StockStockPlaces.FirstOrDefault(s => s.StockName == stockName);
        if (stockStockPlaceEntry != null)
          await AddItemToPlace(stockStockPlaceEntry.StockId, itemCnr);
      }
    }

    private async Task AddItemToPlace(int stockId, string itemCnr)
    {
      StockStockPlaceEntry stockStockPlaceEntry = StockStockPlaces.FirstOrDefault(s => s.StockId == stockId);
      if (stockStockPlaceEntry != null)
      {
        StockItem stockItem = stockStockPlaceEntry.StockItems.FirstOrDefault(s => s.ItemV1Entry.cnr == itemCnr);
        if (stockItem != null)
        {
          _messageService.Info(string.Format(TheStorageLocation0IsAlreadyAssigned, StockPlaceName));
          return;
        }

        await DoAddPlace(stockId, StockPlaceName, itemCnr, stockStockPlaceEntry);
      }
    }

    private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (e.PropertyName == nameof(ItemCnr))
        if (string.IsNullOrEmpty(ItemCnr))
          ClearAll();

      if (e.PropertyName == nameof(Item))
      {
        IsValid = (Item != null);
        if (!IsValid)
        {
          IsBatchDetailVisible = false;
          IsPlaceEditMode = false;
        }
      }

      if (e.PropertyName == nameof(SearchResults))
        IsListVisible = (SearchResults == null ? false : SearchResults.Count > 0);

      if (e.PropertyName == nameof(IsPlaceEditMode))
        IsStockInfoValid = (!IsPlaceEditMode && StockInfos != null);

      if (e.PropertyName == nameof(IsBatchDetailVisible))
        IsStockInfoValid = (!IsBatchDetailVisible && StockInfos != null);
    }

    private void ClearAll()
    {
      SearchResults = null;
      Item = null;
      StockInfos.Clear();
      ItemIdentities.Clear();
      StockPlaces.Clear();
      StockStockPlaces.Clear();
      StockPlaceName = null;
      IsLoadingCircleVisible = false;
      IsValid = false;
      IsStockInfoValid = false;
      IsPlaceEditMode = false;
      IsBatchDetailVisible = false;
      IsStockPlaceListVisible = false;
    }

    [RelayCommand]
    private void CloseStockPlaceList()
    {
      IsStockPlaceListVisible = false;
      StockStockPlaces.Clear();
    }

    [RelayCommand]
    private async Task SearchAsync(string text)
    {
      ClearAll();
      ItemV1EntryList itemV1EntryList = await _itemV1Service.GetItemsWithCnrOrTextOrDeliverCnr(text + "%");
      if (itemV1EntryList != null)
        SearchResults = itemV1EntryList.entries;
#if ANDROID
        var activity = Platform.CurrentActivity;
        var token = activity.CurrentFocus?.WindowToken;
        if (activity.HasWindowFocus)
          activity.Window.DecorView.ClearFocus();
#endif

    }

    [RelayCommand]
    private async Task ListTapped()
    {
      //string[] s = SelectedItem.Split(' ');
      await LoadItemInfo(SelectedItem.cnr);
      SearchResults = null;
    }

    private async Task LoadItemInfo(string itemCnr)
    {
      ClearAll();
      ItemCnr = itemCnr;
      if (!string.IsNullOrEmpty(itemCnr))
      {
        Item = await _itemV1Service.GetItemV1EntryAsync(ItemCnr, true);
        if (Item != null)
        {
          HasSerialOrBatch = (Item.hasSerialnr || Item.hasChargenr);
          List<StockAmountEntry> itemStocks = await _itemV1Service.GetItemStocks(ItemCnr);
          if (itemStocks != null)
          {
            foreach (StockAmountEntry stockAmountEntry in itemStocks)
            {

              StockInfoEntry stockInfoEntry = Item.stockplaceInfoEntries.entries.FirstOrDefault(o => o.stockEntry.id == stockAmountEntry.stock.id);
              if (stockInfoEntry != null && stockInfoEntry.stockplaceEntries != null)
                StockInfos.Add(new StockInfo(Item, stockAmountEntry, stockInfoEntry.stockplaceEntries.entries));
              else
                StockInfos.Add(new StockInfo(Item, stockAmountEntry, []));
            }
            IsStockInfoValid = true;
          }
        }
      }
    }

    [RelayCommand]
    private async Task Scan()
    {
      if (_deviceManager.IsScanDevice())
        _deviceManager.PressScanButton(true);
      else
        await Application.Current.MainPage.ShowPopupAsync(new BarcodeCameraPopup());
    }

    [RelayCommand]
    private void PlaceEdit(StockInfo stockInfo)
    {
      IsLoadingCircleVisible = true;
      _ = Task.Run(() =>
      {
        ActualStockInfo = stockInfo;
        IsPlaceEditMode = true;
        StockPlaces.Clear();
        if (stockInfo.StockPlaces != null)
        {
          foreach (StockPlaceEntry stockPlace in stockInfo.StockPlaces)
          {
            StockPlaces.Add(stockPlace);
          }
        }
        IsLoadingCircleVisible = false;
      });
    }

    [RelayCommand]
    private async Task AddPlace()
    {
      string name = await Application.Current.MainPage.DisplayPromptAsync(StockPlace, EnterName);

      if (!CheckPlaceUsed(name))
        await DoAddPlace(ActualStockInfo.StockId, name);
    }

    private bool CheckPlaceUsed(string name)
    {
      if (!string.IsNullOrEmpty(name))
      {
        if (StockPlaces.FirstOrDefault(o => o.name == name) == null)
          return false;
        else
          _messageService.Info(string.Format(TheStorageLocation0IsAlreadyAssigned, name));
      }
      return true;
    }

    private async Task DoAddPlace(int stockId, string placeName, string itemCnr, StockStockPlaceEntry stockStockPlaceEntry)
    {
      ItemV1Entry item = await _itemV1Service.GetItemV1EntryAsync(itemCnr, true);
      if (item == null)
      {
        _messageService.Warn(string.Format(Item0NotFound, itemCnr));
        return;
      }
      BookingResult result = await _stockService.SetStockPlace(stockId, new StockPlacePostEntry() { itemId = item.id, stockplaceName = placeName });
      if (result != null)
      {
        if (result.Success)
        {
          UpdateStockplaceInfos(stockId, placeName, itemCnr);
          _messageService.ShowToast(string.Format(Item0AddedToStorageLocation1, item.cnr, placeName), CommunityToolkit.Maui.Core.ToastDuration.Short);
        }
        else
          _messageService.Error(result.Message);
      }
    }

    private async Task DoAddPlace(int stockId, string placeName)
    {
      BookingResult result = await _stockService.SetStockPlace(stockId, new StockPlacePostEntry() { itemId = Item.id, stockplaceName = placeName });
      if (result != null)
      {
        if (result.Success)
        {
          StockPlaceEntry entry = new()
          {
            id = (int)result.ResultId,
            name = placeName
          };
          StockPlaces.Add(entry);
          ActualStockInfo.AddStockPlace(entry);
          _messageService.ShowToast(string.Format(Item0AddedToStorageLocation1, Item.cnr, placeName), CommunityToolkit.Maui.Core.ToastDuration.Short);
        }
        else
          _messageService.Error(result.Message);
      }
    }

    [RelayCommand]
    private async Task PlaceDeleteByItem(StockItem stockItem)
    {
      var result = await _stockService.PlaceRemove(stockItem.ItemStockId, stockItem.ItemStockPlaceId, stockItem.ItemV1Entry.id);
      if (result != null && result.Success)
      {
        var entry = StockStockPlaces.FirstOrDefault(s => s.StockId == stockItem.ItemStockId);
        entry.StockItems.Remove(stockItem);
        _messageService.ShowToast(string.Format(Item0RemovedFromStorageLocation1, stockItem.ItemV1Entry.cnr, StockPlaceName), CommunityToolkit.Maui.Core.ToastDuration.Short);
      }
      else
        PlaceDeleteMessage(stockItem.ItemV1Entry.cnr, StockPlaceName, result);
    }

    [RelayCommand]
    private async Task PlaceDelete(StockPlaceEntry stockPlaceEntry)
    {
      var result = await _stockService.PlaceRemove(ActualStockInfo.StockId, stockPlaceEntry.id, Item.id);
      if (result != null && result.Success)
      {
        StockPlaces.Remove(stockPlaceEntry);
        ActualStockInfo.RemoveStockPlace(stockPlaceEntry);
        _messageService.ShowToast(string.Format(Item0RemovedFromStorageLocation1, Item.cnr, stockPlaceEntry.name), CommunityToolkit.Maui.Core.ToastDuration.Short);
      }
      else
        PlaceDeleteMessage(Item.cnr, stockPlaceEntry.name, result);
    }

    private void PlaceDeleteMessage(string itemCnr, string stockPlaceName, BookingResult result)
    {
      if (result.Message != null)
        _messageService.Error(string.Format(RemoveItem0FromStorageLocation1Failed2, itemCnr, stockPlaceName, result.Message));
      else
        _messageService.Error(string.Format(RemoveItem0FromStorageLocation1FailedErrorcode2, itemCnr, stockPlaceName, result?.StatusCode));
    }

    [RelayCommand]
    private void ClosePlaces()
    {
      IsPlaceEditMode = false;
    }

    [RelayCommand]
    private void BatchDetail(StockInfo stockInfo)
    {
      IsLoadingCircleVisible = true;
      Task.Run(() =>
      {
        ActualStockInfo = stockInfo;
        IsBatchDetailVisible = true;
        ItemIdentities.Clear();
        foreach (ItemIdentityEntry itemIdentityEntry in stockInfo.Identities)
        {
          ItemIdentities.Add(itemIdentityEntry);
        }
        IsLoadingCircleVisible = false;
      });
    }

    [RelayCommand]
    private void CloseBatchDetail()
    {
      IsBatchDetailVisible = false;
    }

    [RelayCommand]
    private async Task PrintLabel(Object labelInfo)
    {
      BookingResult bookingResult = null;
      decimal amount;
      PopupResult result = null;
      switch (labelInfo)
      {
        case StockInfo stockInfo:
          amount = stockInfo.Amount;
          result = (PopupResult)await Application.Current.MainPage.ShowPopupAsync<DecimalEntryPopup>(new DecimalEntryPopup(_messageService, 3, amount, string.Empty));
          //PopupResult result = res as PopupResult;
          if (!result.Canceled)
          {
            amount = result.EntryAsDecimal();
            bookingResult = await _itemV1Service.PrintLabel(Item.id, amount, null, 1, stockInfo.StockId);
          }
          else
            return;
          break;

        case ItemIdentityEntry itemIdentityEntry:
          amount = itemIdentityEntry.amount;
          result = (PopupResult)await Application.Current.MainPage.ShowPopupAsync<DecimalEntryPopup>(new DecimalEntryPopup(_messageService, 3, amount, string.Empty));
          if (!result.Canceled)
          {
            amount = result.EntryAsDecimal();
            bookingResult = await _itemV1Service.PrintLabel(Item.id, amount, itemIdentityEntry.identity, 1, null);
          }
          break;

        default:
          _logger.LogError(UnknownTypeForLabelPrinting);
          break;
      }

      if (bookingResult != null)
      {
        if (bookingResult.Success)
          _messageService.ShowToast(string.Format(LabelPrintedForItem0, Item.cnr), CommunityToolkit.Maui.Core.ToastDuration.Short);
        else
          _messageService.Error(bookingResult.Message);
      }
      else
      {
        _messageService.Error(PrintLabelFailedUnknownError);
      }
    }

    [RelayCommand]
    private async Task DiscardRemaining(ItemIdentityEntry itemIdentityEntry)
    {
      BatchDiscardResult batchDiscardResult = await _itemV1Service.DiscardRemaining(Item.id, itemIdentityEntry.identity);
      if (batchDiscardResult != null)
      {
        if (batchDiscardResult.Success)
        {
          ItemIdentities.Remove(itemIdentityEntry);
          string msg = string.Empty;
          if (batchDiscardResult.ProductionEntryList.entries.Count == 0)
            msg = string.Format(_0DiscardedRNNoLotsAreAffected, itemIdentityEntry.identity);
          else
          {
            msg = string.Format(_0DiscardedRNFollowingLotsAreAffectedRN, itemIdentityEntry.identity);
            foreach (var item in batchDiscardResult.ProductionEntryList.entries)
            {
              msg += item.cnr;
              msg += ", ";
            }
            if (msg.EndsWith(", "))
              msg = msg.Substring(0, msg.Length - 2);
          }
          _messageService.Info(msg);
        }
        else
          _messageService.Error(batchDiscardResult.Message);
      }
      else
      {
        _messageService.Error(string.Format(DiscardBatch0FailedUnknownError, itemIdentityEntry.identity));
      }
    }
  }
}
