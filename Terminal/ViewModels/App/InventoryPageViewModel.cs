﻿using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Popups;
using KieselsteinErp.Terminal.Resources.Localization;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Inventory;
using KieselsteinErp.Terminal.Services.Item;
using KieselsteinErp.Terminal.Services.Readers;
using KieselsteinErp.Terminal.Services.Stock;
using System.ComponentModel;
using System.Net;
using System.Text;
using KieselsteinErp.Terminal.Models.ScanLabels;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;
using KieselsteinErp.Terminal.Util;

namespace KieselsteinErp.Terminal.ViewModels.App
{
  public partial class InventoryPageViewModel : ObservableObject, IQueryAttributable
  {
    private readonly IMessageService _messageService;
    private readonly IItemV1Service _itemV1Service;
    private readonly IStockService _stockService;
    private readonly IInventoryService _inventoryService;
    private readonly IDeviceManager _deviceManager;

    [ObservableProperty]
    private string _name;
    [ObservableProperty]
    private string _itemCnr;
    [ObservableProperty]
    private string _itemName;

    private decimal? Amount { get => DecimalHelper.ConvertDecimal(AmountString); set => AmountString = DecimalHelper.ConvertString(value); }

    [ObservableProperty]
    private string _amountString;

    [ObservableProperty]
    private bool _isBookingEnabled = false;
    [ObservableProperty]
    private string _serialNumber;
    [ObservableProperty]
    private string _batchNumber;
    [ObservableProperty]
    private string _unit;
    [ObservableProperty]
    private bool _hasSerial;
    [ObservableProperty]
    private bool _hasCharge;
    [ObservableProperty]
    private bool _isLoadingCircleVisible;
    [ObservableProperty]
    private string _stockPlace;
    [ObservableProperty]
    private ItemV1Entry _selectedItem;
    [ObservableProperty]
    private bool _printLabel;
    [ObservableProperty]
    private bool _allowAmountEdit;
    [ObservableProperty]
    private bool _searchListVisible = false;
    [ObservableProperty]
    private ItemV1Entry _actualItemEntry;

    private Inventory _inventory;
    private StockPlaceEntry _stockPlaceEntry;
    private List<IdentityAmountEntry> _identityAmountEntries;

    [ObservableProperty]
    private List<ItemV1Entry> _searchResults;

    private bool _disableItemMessage = false;

    public InventoryPageViewModel(IMessageService messageService, IItemV1Service itemV1Service, IStockService stockService, IInventoryService inventoryService, IDeviceManager deviceManager)
    {
      _messageService = messageService;
      _itemV1Service = itemV1Service;
      _stockService = stockService;
      _inventoryService = inventoryService;
      _deviceManager = deviceManager;

      ClearAll();
      this.PropertyChanged += OnPropertyChanged;
      SerialNumber = SerialNo;
      BatchNumber = BatchNo;
    }

    private async void HandleStockPlaceMessage(object recipient, StockPlaceMessage message)
    {
      _stockPlaceEntry = await _stockService.GetStockPlaceEntryByCnr(_inventory.StockId, message.Value);
      if (_stockPlaceEntry != null)
        StockPlace = _stockPlaceEntry.name;
      else
        _messageService.Warn(string.Format(Stockplace0NotFound, message.Value));
    }

    [RelayCommand]
    private void Appearing()
    {
      ClearAll();
      WeakReferenceMessenger.Default.Register<UnidentifiedBarcodeMessage>(this, HandleUnidentifiedBarcodeMessage);
      WeakReferenceMessenger.Default.Register<ItemMessage>(this, HandleItemMessage);
      WeakReferenceMessenger.Default.Register<ItemVDAMessage>(this, HandleItemMessage);
      WeakReferenceMessenger.Default.Register<StockPlaceMessage>(this, HandleStockPlaceMessage);
    }

    [RelayCommand]
    private void Disappearing()
    {
      WeakReferenceMessenger.Default.UnregisterAll(this);
    }

    private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      switch (e.PropertyName)
      {
        case nameof(ItemCnr):
        case nameof(AmountString):
        case nameof(BatchNumber):
        case nameof(SerialNumber):
          if (HasSerial)
            IsBookingEnabled = ((!string.IsNullOrEmpty(ItemCnr)) && (!string.IsNullOrEmpty(SerialNumber)) && (Amount != null));
          else if (HasCharge)
            IsBookingEnabled = ((!string.IsNullOrEmpty(ItemCnr)) && (!string.IsNullOrEmpty(BatchNumber)) && (Amount != null));
          else
            IsBookingEnabled = ((!string.IsNullOrEmpty(ItemCnr)) && (Amount != null));
          break;
      }
      if (e.PropertyName == nameof(ItemCnr))
        if (string.IsNullOrEmpty(ItemCnr))
        {
          ClearAll();
        }
    }

    private async void HandleItemMessage(object recipient, ItemVDAMessage message)
    {
      if (_disableItemMessage) return;

      ClearAll();
      VdaItem vdaItem = message.Value.ScanLabel.ToVdaItem();
      ItemCnr = vdaItem.Cnr;
      var found = await LoadItemInfo(ItemCnr);
      if (found)
        if (!string.IsNullOrEmpty(vdaItem.BatchNumber1))
        {
          if (HasSerial)
          {
            _identityAmountEntries = new List<IdentityAmountEntry> { new IdentityAmountEntry() { amount = 1, identity = vdaItem.BatchNumber1 } };
            SerialNumber = vdaItem.BatchNumber1;
            Amount = 1;
            AllowAmountEdit = false;
          }
          else if (HasCharge)
          {
            _identityAmountEntries = new List<IdentityAmountEntry> { new IdentityAmountEntry() { amount = vdaItem.Amount, identity = vdaItem.BatchNumberReelId() } };
            BatchNumber = vdaItem.BatchNumberReelId();
            Amount = vdaItem.Amount;
            AllowAmountEdit = false;
          }
        }
        else
        {
          AllowAmountEdit = true;
          if (vdaItem.Amount != 0)
            Amount = vdaItem.Amount;
        }
    }

    private async void HandleItemMessage(object recipient, ItemMessage message)
    {
      if (_disableItemMessage) return;

      ClearAll();
      ItemCnr = message.Value.ItemCnr;
      var found = await LoadItemInfo(ItemCnr);
      if (found)
        if (!string.IsNullOrEmpty(message.Value.BatchNr))
          if (HasSerial)
          {
            _identityAmountEntries = new List<IdentityAmountEntry> { new IdentityAmountEntry() { amount = 1, identity = message.Value.BatchNr } };
            if (!string.IsNullOrEmpty(message.Value.BatchNr))
              SerialNumber = message.Value.BatchNr;
            Amount = 1;
            AllowAmountEdit = false;
          }
          else if (HasCharge)
          {
            if (!string.IsNullOrEmpty(message.Value.BatchNr))
            {
              if (message.Value.HasAmount)
              {
                _identityAmountEntries = new List<IdentityAmountEntry> { new IdentityAmountEntry() { amount = message.Value.Amount, identity = message.Value.BatchNr } };
                Amount = message.Value.Amount;
              }
              BatchNumber = message.Value.BatchNr;
              AllowAmountEdit = false;
            }
          }
          else
            AllowAmountEdit = true;
    }

    private async void HandleUnidentifiedBarcodeMessage(object recipient, UnidentifiedBarcodeMessage message)
    {
      if (_disableItemMessage) return;

      ClearAll();
      ItemCnr = message.Value;
      var found = await LoadItemInfo(ItemCnr);
    }

    private async Task<bool> LoadItemInfo(string itemCnr)
    {
      IsLoadingCircleVisible = true;
      ActualItemEntry = await _itemV1Service.GetItemV1EntryAsync(itemCnr, false);
      if (ActualItemEntry != null)
      {
        ItemName = ActualItemEntry.name;
        Unit = ActualItemEntry.unitCnr;
        HasSerial = ActualItemEntry.hasSerialnr;
        HasCharge = ActualItemEntry.hasChargenr;
        AllowAmountEdit = !HasSerial && !HasCharge;
      }
      else
      {
        _messageService.Warn(string.Format(Item0NotFound, itemCnr));
      }
      IsLoadingCircleVisible = false;
#if ANDROID
      var activity = Platform.CurrentActivity;
      var token = activity.CurrentFocus?.WindowToken;
      if (activity.HasWindowFocus)
        activity.Window.DecorView.ClearFocus();
#endif
      return (ActualItemEntry != null);
    }

    public void ApplyQueryAttributes(IDictionary<string, object> query)
    {
      _inventory = query["Inventory"] as Inventory;
      Name = _inventory.Name + " " + _inventory.Stockname;
    }

    [RelayCommand]
    private async Task EditSerial()
    {
      try
      {
        _disableItemMessage = true;
        object res = null;
        await MainThread.InvokeOnMainThreadAsync(async () =>
        {
          res = await Application.Current.MainPage.ShowPopupAsync(new IdentityAmountEntryPopup
                      (_messageService, true, HasCharge, ItemCnr, null, _identityAmountEntries));
        });

        List<IdentityAmountEntry> result = (List<IdentityAmountEntry>)res;
        if (result != null)
        {
          decimal amount = 0;
          StringBuilder s = new StringBuilder();
          foreach (IdentityAmountEntry identityAmount in result)
          {
            amount += identityAmount.amount;
            s.Append($"{identityAmount.identity}, ");
          }
          _identityAmountEntries = result;
          Amount = amount;
          SerialNumber = s.ToString();
          if (SerialNumber.EndsWith(", "))
            SerialNumber = SerialNumber.Substring(0, SerialNumber.Length - 2);
        }
      }
      finally
      {
        _disableItemMessage = false;
      }
    }

    [RelayCommand]
    private async Task EditBatch()
    {
      try
      {
        _disableItemMessage = true;
        object res = null;
        string b = null;
        if (_identityAmountEntries == null || _identityAmountEntries.Count == 0)
          if (BatchNumber != BatchNo)
            b = BatchNumber;

        await MainThread.InvokeOnMainThreadAsync(async () =>
        {
          res = await Application.Current.MainPage.ShowPopupAsync(new IdentityAmountEntryPopup
                      (_messageService, true, HasCharge, ItemCnr, b, _identityAmountEntries));
        });

        List<IdentityAmountEntry> result = (List<IdentityAmountEntry>)res;
        if (result != null)
        {
          decimal amount = 0;
          StringBuilder s = new StringBuilder();
          foreach (IdentityAmountEntry identityAmount in result)
          {
            amount += identityAmount.amount;
            s.Append($"{identityAmount.amount} x {identityAmount.identity}, ");
          }
          _identityAmountEntries = result;
          Amount = amount;
          BatchNumber = s.ToString();
          if (BatchNumber.EndsWith(", "))
            BatchNumber = BatchNumber.Substring(0, BatchNumber.Length - 2);
        }
      }
      finally 
      {  
        _disableItemMessage = false; 
      }

    }

    [RelayCommand]
    private async Task Book()
    {
      if (_stockPlaceEntry != null)
      {
        var entry = _stockPlaceEntry.items.entries.FirstOrDefault((o) => o.id == ActualItemEntry.id);
        if (entry == null)
        {
          var result = await _stockService.SetStockPlace(_inventory.StockId, new StockPlacePostEntry() { stockplaceId = _stockPlaceEntry.id, itemId = ActualItemEntry.id });
          if (result.Success)
          {
            _messageService.ShowToast(string.Format(Stockplace0AssignedTo1, StockPlace, ItemCnr), CommunityToolkit.Maui.Core.ToastDuration.Short);
            // reload stockplaceinfos
            HandleStockPlaceMessage(this, new StockPlaceMessage(_stockPlaceEntry.name));
          }
          else
          {
            _messageService.Warn(result.Message);
          }
        }
      }

      if (!HasSerial && !HasCharge && _identityAmountEntries == null)
        _identityAmountEntries = new List<IdentityAmountEntry>();

      InventoryDataEntry data = new InventoryDataEntry() { itemCnr = ItemCnr, amount = (decimal)Amount, identities = _identityAmountEntries };
      Book(data, false);
    }

    [RelayCommand]
    private async Task Scan()
    {
      if (_deviceManager.IsScanDevice())
        _deviceManager.PressScanButton(true);
      else
        await Application.Current.MainPage.ShowPopupAsync(new BarcodeCameraPopup());
    }

    [RelayCommand]
    private async Task SearchItemsAsync(string text)
    {
      IsLoadingCircleVisible = true;
      try
      {
        ItemV1EntryList itemV1EntryList = await _itemV1Service.GetItemsWithCnrOrTextOrDeliverCnr(text + "%");
        SearchListVisible = itemV1EntryList != null && itemV1EntryList.entries.Count > 0;
        if (itemV1EntryList != null)
          SearchResults = itemV1EntryList.entries;

        if (SearchResults.Count == 0)
          _messageService.Warn(string.Format(Item0NotFound, text));
      }
      finally { IsLoadingCircleVisible = false; }
#if ANDROID
      var activity = Platform.CurrentActivity;
      var token = activity.CurrentFocus?.WindowToken;
      if (activity.HasWindowFocus)
        activity.Window.DecorView.ClearFocus();
#endif
    }

    [RelayCommand]
    private async Task ListTapped()
    {
      ItemCnr = SelectedItem.cnr;
      await LoadItemInfo(ItemCnr);
      SearchResults = null;
      SearchListVisible = false;
    }

    [RelayCommand]
    private void AmountCompleted()
    {
      if (HasCharge && Amount != null)
      {
        if (string.IsNullOrEmpty(BatchNumber)) return;

        if (_identityAmountEntries == null)
          _identityAmountEntries = new List<IdentityAmountEntry>();

        if (_identityAmountEntries.Count == 0)
          _identityAmountEntries.Add(new IdentityAmountEntry { amount = (decimal)Amount, identity = BatchNumber });
      }
    }

    private async void DoPrintLabel(int itemId, int stockId, InventoryDataEntry data)
    {
      if (PrintLabel)
      {
        if (HasCharge || HasSerial)
        {
          foreach (IdentityAmountEntry id in data.identities)
          {
            BookingResult res = await _itemV1Service.PrintLabel(itemId, id.amount, id.identity, 1, stockId);
            if (!res.Success)
              _messageService.Warn(res.Message);
          }
        }
        else
        {
          BookingResult res = await _itemV1Service.PrintLabel(ActualItemEntry.id, (decimal)Amount, string.Empty, 1, _inventory.StockId);
          if (!res.Success)
            _messageService.Warn(res.Message);
        }
      }
    }

    private async void Book(InventoryDataEntry data, bool allowLargeDiff)
    {
      var result = await _inventoryService.BookInventoryEntry(_inventory.Id, data, allowLargeDiff);
      if (result.Success)
      {
        _messageService.ShowToast(string.Format(InventoryEntryFor0Booked, ItemCnr), CommunityToolkit.Maui.Core.ToastDuration.Long);
        DoPrintLabel(ActualItemEntry.id, _inventory.StockId, data);
        ClearAll();
      }
      else if (result.StatusCode == HttpStatusCode.BadRequest)
      {
        if (await Application.Current.MainPage.DisplayAlert(AppResources.Inventory, DeviationFromActualQuantityToTargetQuantityTooLargeNMakeTheBookingAnyway, Yes, No))
        {
          Book(data, true);
        }
        else
        {
          _messageService.Warn(BookingCanceled);
        }
      }
      else if (result.StatusCode == HttpStatusCode.UnprocessableEntity)
      {
        PopupResult res = (PopupResult)await Application.Current.MainPage.ShowPopupAsync(new MultipleButtonsPopup(AppResources.Inventory, result.Message, new string[] { Addition, Correction, Cancel }));
        switch (res.Entry)
        {
          case "1":
            await Update(data, false);
            break;
          case "2":
            await Update(data, true);
            break;
          case "3":
            _messageService.Warn(BookingCanceled);
            break;
        }
      }
      else
        _messageService.Error(result.Message);
    }

    private async Task Update(InventoryDataEntry data, bool changeAmountTo)
    {
      var result = await _inventoryService.ChangeInventoryEntry(_inventory.Id, data, changeAmountTo);
      if (result.Success)
      {
        _messageService.ShowToast(string.Format(InventoryEntryFor0Booked, ItemCnr), CommunityToolkit.Maui.Core.ToastDuration.Long);
        DoPrintLabel(ActualItemEntry.id, _inventory.StockId, data);
        ClearAll();
      }
      else
        _messageService.Error(result.Message);
    }

    private void ClearAll()
    {
      SearchResults = null;
      ClearItem();
      ClearFields();
      AllowAmountEdit = false;
      SearchListVisible = false;
    }

    private void ClearFields()
    {
      ItemCnr = null;
      Amount = null;
      SerialNumber = "";
      BatchNumber = "";
    }

    private void ClearItem()
    {
      ActualItemEntry = null;
      _identityAmountEntries = null;
      ItemName = null;
      HasSerial = false;
      HasCharge = false;
      Unit = null;
    }
  }
}
