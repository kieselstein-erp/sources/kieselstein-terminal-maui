﻿using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Models.ScanLabels;
using KieselsteinErp.Terminal.Popups;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Delivery;
using KieselsteinErp.Terminal.Services.Item;
using KieselsteinErp.Terminal.Services.Readers;
using System.Collections.ObjectModel;
using System.ComponentModel;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.ViewModels.App
{

  public partial class DeliveryPageViewModel : DeliveryPageViewModelBase
  {

    [ObservableProperty]
    private bool _isValid = false;
    [ObservableProperty]
    private string _customerCnr = null;
    [ObservableProperty]
    private bool hasCustomerCnr = false;

    private readonly ObservableCollection<DeliveryPosition> _positions = [];
    public ObservableCollection<DeliveryPosition> Positions { get { return _positions; } }

    private List<DeliveryEntry> _deliveries;

    private DeliveryData DeliveryData { get; set; }

    private readonly string CREATE_NEW = CREATENEW;

    public DeliveryPageViewModel(IMessageService messageService, IDeviceManager deviceManager, IDeliveryService deliveryService, IItemV1Service itemV1Service) : base(messageService, deviceManager, deliveryService, itemV1Service)
    {
      this.PropertyChanged += OnPropertyChanged;
    }

    private void HandleCustomerNumberMessage(object recipient, CustomerNumberMessage message)
    {
      MainThread.InvokeOnMainThreadAsync(async () =>
      {
        await DoHandleCustomerNumberMessage(message);
      });
    }

    private async Task DoHandleCustomerNumberMessage(CustomerNumberMessage message)
    {
      ClearAll();
      _deliveries = await GetCustomerDeliveries(message.Value);
      CustomerCnr = message.Value;
      List<string> items = [CREATE_NEW];
      foreach (var item in _deliveries)
        items.Add(item.deliveryCnr + " " + item.customerName);
      SearchResults = items;
    }

    private void HandleDeliveryNoteMessage(object recipient, DeliveryNoteMessage message)
    {
      MainThread.InvokeOnMainThreadAsync(async () =>
      {
        await DoHandleDeliveryNoteMessag(message);
      });
    }

    private async Task DoHandleDeliveryNoteMessag(DeliveryNoteMessage message)
    {
      ClearAll();
      _deliveries = await GetDeliveries(message.Value);
      if (_deliveries.Count > 0)
        await GetDetail(message.Value);
    }

    private void HandleItemMessage(object recipient, ItemVDAMessage message)
    {
      MainThread.InvokeOnMainThreadAsync(async () =>
      {
        await DoHandleItemVda(message);
      });
    }

    private async Task DoHandleItemVda(ItemVDAMessage message)
    {
      VdaItem vdaItem = message.Value.ScanLabel.ToVdaItem();
      string batchNumberToUse = await FindAvailableBatchNumber(vdaItem);
      await AddPosition(vdaItem.Cnr, vdaItem.Amount, batchNumberToUse);
    }

    private void HandleItemMessage(object recipient, ItemMessage message)
    {
      MainThread.InvokeOnMainThreadAsync(async () =>
      {
        await DoHandleItem(message);
      });
    }

    private async Task DoHandleItem(ItemMessage message)
    {
      if (message.Value.HasAmount)
        await AddPosition(message.Value.ItemCnr, message.Value.Amount, message.Value.BatchNr);
      else
        await AddPosition(message.Value.ItemCnr, null, message.Value.BatchNr);
    }

    protected override void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      base.OnPropertyChanged(sender, e);
      if (e.PropertyName == nameof(DeliveryNoteNumber) && string.IsNullOrEmpty(DeliveryNoteNumber))
        ClearAll();

      if (e.PropertyName == nameof(DeliveryEntry))
        IsDeliveryNoteValid = DeliveryEntry != null && DeliveryEntry.id != 0;

      if (e.PropertyName == nameof(CustomerCnr))
        HasCustomerCnr = !string.IsNullOrEmpty(CustomerCnr);
      
    }

    [RelayCommand]
    private void Appearing()
    {
      DeliveryNoteNumber = null;
      ClearAll();
      RegisterBarcodeMessages();
    }

    private void RegisterBarcodeMessages()
    {
      WeakReferenceMessenger.Default.Register<ItemMessage>(this, HandleItemMessage);
      WeakReferenceMessenger.Default.Register<ItemVDAMessage>(this, HandleItemMessage);
      WeakReferenceMessenger.Default.Register<DeliveryNoteMessage>(this, HandleDeliveryNoteMessage);
      WeakReferenceMessenger.Default.Register<CustomerNumberMessage>(this, HandleCustomerNumberMessage);
    }

    private void ClearAll()
    {
      SearchResults = null;
      DeliveryEntry = null;
      _deliveries = null;
      Positions.Clear();
      IsLoadingCircleVisible = false;
      IsValid = false;
      DeliveryNoteNumber = null;
      CustomerCnr = null;
    }

    [RelayCommand]
    private async Task SearchAsync(string text)
    {
      if (text == CREATE_NEW) return;

      ClearAll();
      _deliveries = await GetDeliveries(text);
      if (_deliveries.Count == 0) return;

      List<string> items = [];
      foreach (var item in _deliveries)
        items.Add(item.deliveryCnr + " " + item.customerName);
      SearchResults = items;

#if ANDROID
        var activity = Platform.CurrentActivity;
        var token = activity.CurrentFocus?.WindowToken;
        if (activity.HasWindowFocus)
          activity.Window.DecorView.ClearFocus();
#endif
    }

    private async Task<List<DeliveryEntry>> GetDeliveries(string text)
    {
      DeliveryEntryList deliveryEntryList = await _deliveryService.GetDeliveryEntryList(text + "%", null, null, DeliveryDocumentStatusConstants.NEW);
      if (deliveryEntryList == null || deliveryEntryList.entries.Count == 0)
      {
        _messageService.Info(NoDeliveryNotesAvailableForDelivery);
        return [];
      }
      return deliveryEntryList.entries;
    }

    private async Task<List<DeliveryEntry>> GetCustomerDeliveries(string customerNumber)
    {
      DeliveryEntryList deliveryEntryList = await _deliveryService.GetDeliveryEntryListForCustomer(customerNumber);
      if (deliveryEntryList == null || deliveryEntryList.entries.Count == 0)
      {
        _messageService.Info(string.Format(NoDeliveryNotesFoundForCustomerNumber0, customerNumber));
        return [];
      }
      return deliveryEntryList.entries;
    }

    [RelayCommand]
    private async Task ListTapped()
    {
      if (SelectedItem != CREATE_NEW)
      {
        string[] s = SelectedItem.Split(' ');
        await GetDetail(s[0].Trim());
      }
      else
        await GetDetail(SelectedItem);
      
      SearchResults = null;
    }

    private async Task GetDetail(string number)
    {
      DeliveryNoteNumber = number;
      if (!string.IsNullOrEmpty(number))
      {
        DeliveryEntry = _deliveries.Find(x => x.deliveryCnr == number);
        DeliverableDeliveryEntry deliverableDeliveryEntry = await _deliveryService.DeliverableDeliveryEntry(number);
        if (deliverableDeliveryEntry == null)
          _messageService.Info(string.Format(DeliveryNote0DoesNotAllowBooking, number));
        else
        {
          DeliveryData = await _deliveryService.GetDeliveryData(deliverableDeliveryEntry.id);
          if (DeliveryData != null)
          {
            IsValid = true;
            foreach (PositionDataEntry entry in DeliveryData.positionData.entries)
            {
              if (entry.dataType == PositionDataTypeConstants.ITEM)
              {
                var pos = DeliveryData.itemPositions.entries.Find(x => x.id == entry.id);
                if (pos != null)
                  Positions.Add(new DeliveryPosition(entry.id, entry.isort, pos));
              }
              else if (entry.dataType == PositionDataTypeConstants.TEXT)
              {
                var pos = DeliveryData.textPositions.entries.Find(x => x.id == entry.id);
                if (pos != null)
                  Positions.Add(new DeliveryPosition(entry.id, entry.isort, pos));
              }
            }
          }
        }
      }
    }

    [RelayCommand]
    private async Task Delete(DeliveryPosition deliveryPosition)
    {
      BookingResult result = await _deliveryService.DeletePosition(deliveryPosition.Id);
      if (result.Success)
        Positions.Remove(deliveryPosition);
      else
        _messageService.Error(result.Message);
    }

    [RelayCommand]
    private async Task Add()
    {
      object res = null;
      await MainThread.InvokeOnMainThreadAsync(async () => { res = await Application.Current.MainPage.ShowPopupAsync(new ItemSearchPopup(_itemV1Service)); });
      if (res != null)
      {
        PopupResult result = (PopupResult)res;
        if (!result.Canceled)
          await AddPosition(result.Entry, null, null);
      }
    }

    private decimal AddIdentitiesToCreateDeliveryPosition(decimal amount, CreateDeliveryPositionEntry createDeliveryPositionEntry, List<IdentityAmountEntry> result)
    {
      createDeliveryPositionEntry.itemIdentity = new ItemIdentityEntryList();
      List<ItemIdentityEntry> entries = [];
      foreach (var item in result)
      {
        entries.Add(new ItemIdentityEntry { identity = item.identity, amount = item.amount });
        amount += item.amount;
      }
      createDeliveryPositionEntry.itemIdentity.entries = entries;
      createDeliveryPositionEntry.amount = amount;
      return amount;
    }

    private async Task AddPosition(string itemCnr, decimal? amount, string identitycnr)
    {
      ItemV1Entry itemEntry = await _itemV1Service.GetItemV1EntryAsync(itemCnr, true);
      if (itemEntry == null)
      {
        _messageService.Info(string.Format(Item0NotFound, itemCnr));
        return;
      }

      if (!string.IsNullOrEmpty(CustomerCnr) && DeliveryEntry == null)
      {
        await AddCustomerPosition(itemEntry, amount, identitycnr);
      }
      else
        await AddPosition(itemEntry, amount, identitycnr);
    }

    private async Task AddCustomerPosition(ItemV1Entry itemEntry, decimal? amount, string identitycnr)
    {
      int customerNr = int.Parse(CustomerCnr);
      int? deliveryId = DeliveryEntry?.id;
      CreateItemCustomerDeliveryPositionEntry createItemCustomerDeliveryPositionEntry = null;
      if (itemEntry.hasChargenr || itemEntry.hasSerialnr)
      {
        if (itemEntry.hasSerialnr && !string.IsNullOrEmpty(identitycnr))
        {
          // item with serialnumber
          amount = 1;
          ItemIdentityEntryList list = CreateIdentityList(identitycnr, 1);
          createItemCustomerDeliveryPositionEntry = new() { customerNr = customerNr, deliveryId = deliveryId, itemId = itemEntry.id, amount = 1, itemIdentity = list };
        }
        else if (itemEntry.hasChargenr && !string.IsNullOrEmpty(identitycnr) && amount != null)
        {
          // item with amount and batchnumber
          ItemIdentityEntryList list = CreateIdentityList(identitycnr, (decimal)amount);
          createItemCustomerDeliveryPositionEntry = new() { customerNr = customerNr, deliveryId = deliveryId, itemId = itemEntry.id, amount = (decimal)amount, itemIdentity = list };
        }
        else
        {
          // item needs serial or batch number
          List<IdentityAmountEntry> availableIdentities = null;
          if (string.IsNullOrEmpty(identitycnr))
            availableIdentities = await GetAvailableIdentitiesForItem(itemEntry.cnr);

          WeakReferenceMessenger.Default.UnregisterAll(this);
          var res = await Application.Current.MainPage.ShowPopupAsync(new IdentityAmountEntryPopup(_messageService, true, itemEntry.hasChargenr, itemEntry.cnr, identitycnr, null, availableIdentities));
          List<IdentityAmountEntry> result = (List<IdentityAmountEntry>)res;
          if (result != null && result.Count > 0)
          {
            amount = 0;
            ItemIdentityEntryList itemIdentityEntryList = new();
            List<ItemIdentityEntry> entries = [];
            foreach (var item in result)
            {
              entries.Add(new ItemIdentityEntry { identity = item.identity, amount = item.amount });
              amount += item.amount;
            }
            itemIdentityEntryList.entries = entries;
            createItemCustomerDeliveryPositionEntry = new() { customerNr = customerNr, deliveryId = deliveryId, itemId = itemEntry.id, amount = (decimal)amount, itemIdentity = itemIdentityEntryList };
          }
          RegisterBarcodeMessages();
        }
      }
      else
      {
        // item without serial or batch number
        var res = await Application.Current.MainPage.ShowPopupAsync(new DecimalEntryPopup(_messageService, 3, string.Empty));
        PopupResult result = (PopupResult)res;
        if (!result.Canceled)
        {
          amount = Convert.ToDecimal(result.Entry);
          ItemIdentityEntryList itemIdentityEntryList = new()
          {
            entries = []
          };
          createItemCustomerDeliveryPositionEntry = new() { customerNr = customerNr, deliveryId = deliveryId, itemId = itemEntry.id, amount = (decimal)amount, itemIdentity = itemIdentityEntryList };
        }

      }

      if (amount != null)
      {
        CreatedDeliveryPositionEntry createdDeliveryPositionEntry = await _deliveryService.CreateCustomerPosition(DeliveryData.deliveryId, createItemCustomerDeliveryPositionEntry);
        if (createdDeliveryPositionEntry != null)
        {
          DeliveryNoteNumber = createdDeliveryPositionEntry.deliveryCnr;
          _deliveries = await GetDeliveries(DeliveryNoteNumber);
          await GetDetail(DeliveryNoteNumber);
        }
      }
    }

    private async Task AddPosition(ItemV1Entry itemEntry, decimal? amount, string identitycnr)
    {
      CreateItemDeliveryPositionEntry createItemDeliveryPositionEntry = null;
      if (itemEntry.hasChargenr || itemEntry.hasSerialnr)
      {
        if (itemEntry.hasSerialnr && !string.IsNullOrEmpty(identitycnr))
        {
          // item with serialnumber
          amount = 1;
          ItemIdentityEntryList list = CreateIdentityList(identitycnr, 1);
          createItemDeliveryPositionEntry = new() { deliveryId = DeliveryData.deliveryId, itemCnr = itemEntry.cnr, amount = 1, itemIdentity = list };
        }
        else if (itemEntry.hasChargenr && !string.IsNullOrEmpty(identitycnr) && amount != null)
        {
          // item with amount and batchnumber
          ItemIdentityEntryList list = CreateIdentityList(identitycnr, (decimal)amount);
          createItemDeliveryPositionEntry = new() { deliveryId = DeliveryData.deliveryId, itemCnr = itemEntry.cnr, amount = (decimal)amount, itemIdentity = list };
        }
        else
        {
          // item needs serial or batch number
          List<IdentityAmountEntry> availableIdentities = null;
          if (string.IsNullOrEmpty(identitycnr))
            availableIdentities = await GetAvailableIdentitiesForItem(itemEntry.cnr);
          
          WeakReferenceMessenger.Default.UnregisterAll(this);
          var res = await Application.Current.MainPage.ShowPopupAsync(new IdentityAmountEntryPopup(_messageService, true, itemEntry.hasChargenr, itemEntry.cnr, identitycnr, null, availableIdentities));
          List<IdentityAmountEntry> result = (List<IdentityAmountEntry>)res;
          if (result != null && result.Count > 0)
          {
            amount = 0;
            ItemIdentityEntryList itemIdentityEntryList = new();
            List<ItemIdentityEntry> entries = [];
            foreach (var item in result)
            {
              entries.Add(new ItemIdentityEntry { identity = item.identity, amount = item.amount });
              amount += item.amount;
            }
            itemIdentityEntryList.entries = entries;
            createItemDeliveryPositionEntry = new() { deliveryId = DeliveryData.deliveryId, itemCnr = itemEntry.cnr, amount = (decimal)amount, itemIdentity = itemIdentityEntryList };
          }
          RegisterBarcodeMessages();
        }
      }
      else
      {
        // item without serial or batch number
        var res = await Application.Current.MainPage.ShowPopupAsync(new DecimalEntryPopup(_messageService, 3, string.Empty));
        PopupResult result = (PopupResult)res;
        if (!result.Canceled)
        {
          amount = Convert.ToDecimal(result.Entry);
          ItemIdentityEntryList itemIdentityEntryList = new()
          {
            entries = []
          };
          createItemDeliveryPositionEntry = new() { deliveryId = DeliveryData.deliveryId, itemCnr = itemEntry.cnr, amount = (decimal)amount, itemIdentity = itemIdentityEntryList };
        }

      }

      if (amount != null)
      {
        CreatedDeliveryPositionEntry createdDeliveryPositionEntry = await _deliveryService.CreatePosition(createItemDeliveryPositionEntry);
        if (createdDeliveryPositionEntry != null)
        {
          DeliveryPosition position = Positions.FirstOrDefault(o => o.Id == createdDeliveryPositionEntry.deliveryPositionId);
          if (position != null)
            Positions.Remove(position);

          DeliveryPositionEntry entry = new()
          {
            id = createdDeliveryPositionEntry.deliveryPositionId,
            itemId = itemEntry.id,
            itemCnr = itemEntry.cnr,
            description = itemEntry.name,
            itemIdentity = createdDeliveryPositionEntry.itemIdentity,
            amount = createdDeliveryPositionEntry.deliveredAmount,
            unitCnr = itemEntry.unitCnr
          };
          if (itemEntry != null)
            entry.itemProperty = itemEntry.hasChargenr ? ItemPropertyEnumConstants.BATCHNR : itemEntry.hasSerialnr ? ItemPropertyEnumConstants.SERIALNR : ItemPropertyEnumConstants.NOIDENTIY;

          Positions.Add(new DeliveryPosition(createdDeliveryPositionEntry.deliveryPositionId, -1, entry));
        }
      }
    }

    private ItemIdentityEntryList CreateIdentityList(string batchNr, decimal amount)
    {
      ItemIdentityEntryList itemIdentityEntryList = new ItemIdentityEntryList();
      List<ItemIdentityEntry> entries = [];
      entries.Add(new ItemIdentityEntry { identity = batchNr, amount = amount });
      itemIdentityEntryList.entries = entries;
      return itemIdentityEntryList;
    }

  }
}
