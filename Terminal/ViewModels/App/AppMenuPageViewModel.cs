﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.User;
using KieselsteinErp.Terminal.Views.App;
using System.ComponentModel;

namespace KieselsteinErp.Terminal.ViewModels.App
{
  public partial class AppMenuPageViewModel : ObservableObject
  {
    private readonly IUserService _userService;
    private readonly IMessageService _messageService;
    private readonly ISettingsService _settingsService;
    
    [ObservableProperty]
    private string _username;

    [ObservableProperty]
    private string _password;

    [ObservableProperty]
    private bool _enableLogon = false;

    [ObservableProperty]
    private bool _isLoadingCircleVisible = false;

    public AppMenuPageViewModel(IUserService userService, IMessageService messageService, ISettingsService settingsService)
    {
      _userService = userService;
      _messageService = messageService;
      _settingsService = settingsService;

      this.PropertyChanged += OnPropertyChanged;

      _userService.OnLogonChanged += (s, e) =>
      {
        if (e)
          MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync("///" + nameof(AppMenuPage)); });
      };

    }

    private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (!string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password))
        EnableLogon = true;
      else
        EnableLogon = false;
    }

    [RelayCommand]
    private void Appearing()
    {
      IsLoadingCircleVisible = false;
    }

    [RelayCommand]
    private void Items()
    {
      IsLoadingCircleVisible = true;
      MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync(nameof(ItemsPage)); });
    }

    [RelayCommand]
    private void InventorySelection()
    {
      IsLoadingCircleVisible = true;
      MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync(nameof(InventorySelectionPage)); });
    }

    [RelayCommand]
    private void Production()
    {
      IsLoadingCircleVisible = true;
      MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync(nameof(ProductionPage)); });
    }

    [RelayCommand]
    void Logout()
    {
      IsLoadingCircleVisible = true;
      _userService.LogoutApp();
      MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync("///" + nameof(LogonPage)); });
    }

    [RelayCommand]
    void TimeTracking()
    { 
      IsLoadingCircleVisible = true;
      MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync(nameof(TimeTrackingPage)); });
    }

    [RelayCommand]
    void PurchaseOrder()
    {
      IsLoadingCircleVisible = true;
      MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync(nameof(PurchaseOrderPage)); });
    }

    [RelayCommand]
    void Delivery()
    {
      IsLoadingCircleVisible = true;
      MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync(nameof(DeliveryPage)); });
    }

    [RelayCommand]
    void OrderDelivery()
    {
      IsLoadingCircleVisible = true;
      MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync(nameof(OrderDeliveryPage)); });
    }

    [RelayCommand]
    void CollectiveDelivery()
    {
      IsLoadingCircleVisible = true;
      MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync(nameof(CollectiveOrderDeliveryPage)); });
    }
  }
}
