﻿using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Popups;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Delivery;
using KieselsteinErp.Terminal.Services.Item;
using KieselsteinErp.Terminal.Services.Order;
using KieselsteinErp.Terminal.Services.Readers;
using System.Collections.ObjectModel;
using System.ComponentModel;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.ViewModels.App
{
  public partial class CollectiveOrderDeliveryPageViewModel : DeliveryPageViewModelBase 
  {
    private readonly IOrderService _orderService;

    [ObservableProperty]
    private string _orderNumber;
    [ObservableProperty]
    private bool _isOrderValid = false;
    [ObservableProperty]
    private OrderEntry _orderEntry;

    private readonly ObservableCollection<DeliverableOrderPosition> _orderPositions = [];
    public ObservableCollection<DeliverableOrderPosition> OrderPositions { get { return _orderPositions; } }

    private List<OrderEntry> _orders;

    public CollectiveOrderDeliveryPageViewModel(IMessageService messageService, IDeviceManager deviceManager, IOrderService orderService, 
      IDeliveryService deliveryService, IItemV1Service itemV1Service) : base(messageService, deviceManager, deliveryService, itemV1Service)
    {
      _orderService = orderService;

      this.PropertyChanged += OnPropertyChanged;
    }

    private void HandleOrderMessage(object recipient, OrderMessage message)
    {
      MainThread.InvokeOnMainThreadAsync(async () =>
      {
        await DoHandleOrderMessage(message);
      });
    }

    private async Task DoHandleOrderMessage(OrderMessage message)
    {
      await GetOrderDetail(message.Value, message.OrderEntry);
    }

    private void HandleItemMessage(object recipient, ItemVDAMessage message)
    {
      MainThread.InvokeOnMainThreadAsync(async () =>
      {
        await DoHandleItemVda(message);
      });
    }

    private async Task DoHandleItemVda(ItemVDAMessage message)
    {
      var item = message.Value.ScanLabel.ToVdaItem();
      DeliverableOrderPosition orderPosition = OrderPositions.FirstOrDefault(o => o.itemCnr == item.Cnr);
      if (orderPosition == null)
      {
        _messageService.Info(string.Format(NoOrderPositionForItem0, item.Cnr));
      }
      else
      {
        if (item.Amount > orderPosition.OpenAmount)
        {
          var ret = await Application.Current.MainPage.DisplayAlert(item.Cnr, string.Format(TheScannedQuantity0N3IsGreaterThanTheOpenDeliveryQuantityRNBookAnyway, item.Amount), Yes, Cancel);
          if (!ret) return;
        }
        string batchNumberToUse = null;
        if (!orderPosition.itemProperty.Equals(ItemPropertyEnumConstants.NOIDENTIY))
          batchNumberToUse = await FindAvailableBatchNumber(item);

        await Deliver(orderPosition, Math.Min(orderPosition.openAmount, item.Amount), batchNumberToUse);
      }
    }

    private void HandleItemMessage(object recipient, ItemMessage message)
    {
      MainThread.InvokeOnMainThreadAsync(async () =>
      {
        await DoHandleItem(message);
      });
    }

    private async Task DoHandleItem(ItemMessage message)
    {
      string itemCnr = message.Value.ItemCnr;
      DeliverableOrderPosition orderPosition = OrderPositions.FirstOrDefault(o => o.itemCnr == itemCnr);
      if (orderPosition == null)
      {
        _messageService.Info(string.Format(NoOrderPositionForItem0, itemCnr));
      }
      else if (orderPosition.openAmount == 0)
        _messageService.Info(string.Format(NoOpenAmountForItem0, itemCnr));
      else
      {
        if (message.Value.HasAmount && !string.IsNullOrEmpty(message.Value.BatchNr))
        {
          if (message.Value.Amount > orderPosition.openAmount)
          {
            var ret = await Application.Current.MainPage.DisplayAlert(message.Value.ItemCnr, string.Format(TheScannedQuantity0N3IsGreaterThanTheOpenDeliveryQuantityRNBookAnyway, message.Value.Amount), Yes, Cancel);
            if (!ret) return;
          }
          await Deliver(orderPosition, Math.Min(orderPosition.openAmount, message.Value.Amount), message.Value.BatchNr);
        }
        else if (!string.IsNullOrEmpty(message.Value.BatchNr))
        {
          await Deliver(orderPosition, message.Value.BatchNr);
        }
        else
          await Deliver(orderPosition);
      }
    }

    protected override void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      base.OnPropertyChanged(sender, e);
      if (e.PropertyName == nameof(OrderNumber))
        if (string.IsNullOrEmpty(OrderNumber))
          ClearAll();
    }

    [RelayCommand]
    private void Appearing()
    {
      DeliveryNoteNumber = null;
      IsDeliveryNoteValid = false;
      OrderNumber = null;
      ClearAll();
      RegisterBarcodeMessages();
    }

    private void RegisterBarcodeMessages()
    {
      WeakReferenceMessenger.Default.Register<ItemMessage>(this, HandleItemMessage);
      WeakReferenceMessenger.Default.Register<ItemVDAMessage>(this, HandleItemMessage);
      WeakReferenceMessenger.Default.Register<OrderMessage>(this, HandleOrderMessage);
    }

    private void ClearAll()
    {
      SearchResults = null;
      OrderEntry = null;
      _orders = null;
      OrderPositions.Clear();
      IsLoadingCircleVisible = false;
      IsOrderValid = false;
      DeliveryNoteNumber = null;
      //DeliverOrderNumbers.Clear();
      _deliverableOrderEntry = null;
    }


    [RelayCommand]
    private async Task SearchOrderAsync(string text)
    {
      ClearAll();
      _orders = await _orderService.GetOrdersAsync(text + "%");
      if (_orders != null)
      {
        if (_orders.Count > 0)
        {
          List<string> items = [];
          foreach (var item in _orders)
            items.Add(item.cnr + " " + item.customerName);
          SearchResults = items;
        }
        else
        {
          _messageService.Info(NoOrderConfirmationsWereFoundForTheSearchTerm);
        }

#if ANDROID
        var activity = Platform.CurrentActivity;
        var token = activity.CurrentFocus?.WindowToken;
        if (activity.HasWindowFocus)
          activity.Window.DecorView.ClearFocus();
#endif
      }
    }

    [RelayCommand]
    private async Task ListTapped()
    {
      string[] s = SelectedItem.Split(' ');
      await GetOrderDetail(s[0].Trim());
      SearchResults = null;
    }

    private async Task GetOrderDetail(string orderNumber, OrderEntry orderEntry = null)
    {
      if (string.IsNullOrEmpty(orderNumber) || (OrderEntry != null && OrderEntry.id == orderEntry.id))
        return;

      OrderNumber = orderNumber;
      if (!string.IsNullOrEmpty(OrderNumber))
      {
        if (orderEntry == null && _orders != null)
          orderEntry = _orders.FirstOrDefault(o => o.cnr == OrderNumber);

        OrderEntry = orderEntry;
        OrderPositions.Clear();

        string actualDeliverOrderNumber = DeliveryNoteIndex > 0 ? DeliverOrderNumbers[DeliveryNoteIndex] : string.Empty;
        DeliverOrderNumbers.Clear();

        if (OrderEntry != null)
        {
          _deliverableOrderEntry = await _orderService.GetDeliverableOrderEntryByCnr(OrderNumber);
          if (_deliverableOrderEntry != null)
          {
            IsOrderValid = true;

            if (_deliverableOrderEntry.positionEntries.entries.Count == 0)
              _messageService.Info(NoOpenPositionsAvailable);
            else
              foreach (var entry in _deliverableOrderEntry.positionEntries.entries)
              {
                OrderPositions.Add(new DeliverableOrderPosition(entry));
              }

            DeliverOrderNumbers.Add(NEW);

            if (_deliverableOrderEntry.deliveryEntries.entries.Count > 0)
              foreach (var entry in _deliverableOrderEntry.deliveryEntries.entries)
              {
                if (entry.status == DeliveryDocumentStatusConstants.OPEN || entry.status == DeliveryDocumentStatusConstants.NEW)
                  DeliverOrderNumbers.Add(entry.deliveryCnr);
              }
            else
            {
              DeliveryEntryList deliveryEntryList = await _deliveryService.GetDeliveryEntryList("", "", "", DeliveryEntrystatusConstants.NEW);
              if (deliveryEntryList != null && deliveryEntryList.entries.Count > 0)
              {
                foreach (DeliveryEntry entry in deliveryEntryList.entries)
                {
                  if (entry.art != null && entry.art.Equals("A") && entry.customerId == orderEntry.customerIdDeliveryAddress)
                  {
                    _deliverableOrderEntry.deliveryEntries.entries.Add(new DeliveryOrderEntry { id = entry.id, deliveryCnr = entry.deliveryCnr, status = entry.status });
                    DeliverOrderNumbers.Add(entry.deliveryCnr);
                  }
                }
              }
            }
            if (!string.IsNullOrEmpty(actualDeliverOrderNumber))
              DeliveryNoteIndex = DeliverOrderNumbers.IndexOf(actualDeliverOrderNumber);
            else
            {
              if (DeliverOrderNumbers.Count == 2)
              {
                DeliveryNoteIndex = 2;
                return;
              }
              if (DeliverOrderNumbers.Count > 1)
              {
                DeliveryNoteIndex = 0;
                _messageService.ShowToast(String.Format(_0DeliveryNotesAreAvailableForBooking, DeliverOrderNumbers.Count - 1), CommunityToolkit.Maui.Core.ToastDuration.Short);
              }
            }
          }
        }
      }
    }

    [RelayCommand]
    private async Task Deliver(DeliverableOrderPosition orderPosition)
    {
      await Deliver(orderPosition, null);
    }

    private async Task Deliver(DeliverableOrderPosition orderPosition, string identitycnr)
    {
      decimal amount = 0;

      CreateDeliveryPositionEntry createDeliveryPositionEntry = BuildCreateDeliveryPositionEntry(_deliverableOrderEntry.id, orderPosition, 0);

      ItemV1Entry itemEntry = await _itemV1Service.GetItemV1EntryAsync(orderPosition.itemCnr, false);
      if (itemEntry == null)
      {
        _messageService.Error(string.Format(Item0NotFound, orderPosition.itemCnr));
        return;
      }

      List<IdentityAmountEntry> availableIdentities = null;
      if (string.IsNullOrEmpty(identitycnr))
        availableIdentities = await GetAvailableIdentitiesForItem(itemEntry.cnr);

      if (itemEntry.hasSerialnr && !string.IsNullOrEmpty(identitycnr))
        amount = AddIdentitiesToCreateDeliveryPosition(1, createDeliveryPositionEntry, [new IdentityAmountEntry { amount = 1, identity = identitycnr }]);
      else if (itemEntry.hasSerialnr || itemEntry.hasChargenr)
      {
        WeakReferenceMessenger.Default.UnregisterAll(this);
        try
        {
          var res = await Application.Current.MainPage.ShowPopupAsync(new IdentityAmountEntryPopup(_messageService, true, itemEntry.hasChargenr, itemEntry.cnr, identitycnr, null, availableIdentities));
          List<IdentityAmountEntry> result = (List<IdentityAmountEntry>)res;
          if (result != null && result.Count > 0)
          {
            amount = AddIdentitiesToCreateDeliveryPosition(amount, createDeliveryPositionEntry, result);
          }
        }
        finally
        {
          RegisterBarcodeMessages();
        }
      }
      else
      {
        var res = await Application.Current.MainPage.ShowPopupAsync(new DecimalEntryPopup(_messageService, 3, orderPosition.openAmount, string.Empty));
        PopupResult result = (PopupResult)res;
        if (!result.Canceled)
        {
          amount = Convert.ToDecimal(result.Entry);
        }
      }
      if (amount != 0)
      {
        createDeliveryPositionEntry.amount = amount;
        await Book(orderPosition, createDeliveryPositionEntry);
      }
    }


    private async Task Deliver(DeliverableOrderPosition orderPosition, decimal amount, string identity)
    {
      CreateDeliveryPositionEntry createDeliveryPositionEntry = BuildCreateDeliveryPositionEntry(_deliverableOrderEntry.id, orderPosition, amount);
      if (!string.IsNullOrEmpty(identity))
      {
        List<IdentityAmountEntry> identities = [new IdentityAmountEntry { amount = amount, identity = identity }];
        amount = AddIdentitiesToCreateDeliveryPosition(amount, createDeliveryPositionEntry, identities);
      }
      if (amount != 0)
      {
        await Book(orderPosition, createDeliveryPositionEntry);
      }
    }
  }
}
