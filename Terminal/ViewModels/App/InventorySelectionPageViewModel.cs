﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Inventory;
using KieselsteinErp.Terminal.Views.App;
using System.Collections.ObjectModel;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.ViewModels.App
{
  public partial class InventorySelectionPageViewModel : ObservableObject
  {
    private readonly IMessageService _messageService;
    private readonly IInventoryService _inventoryService;

    [ObservableProperty]
    private bool _isLoadingCircleVisible = true;

    ObservableCollection<Inventory> _inventories = new();
    public ObservableCollection<Inventory> Inventories { get { return _inventories; } }

    public InventorySelectionPageViewModel(IMessageService messageService, IInventoryService inventoryService)
    {
      _messageService = messageService;
      _inventoryService = inventoryService;
    }

    private async Task InitInventories()
    {
      _inventories.Clear();
      InventoriesResult result = await _inventoryService.GetInventories();
      if (result.Success)
        if (result.Entries.Count == 0)
          _messageService.ShowToast(NoOpenInventoriesAvailable, CommunityToolkit.Maui.Core.ToastDuration.Long);
        else
        {
          result.Entries.Reverse();
          foreach (InventoryEntry entry in result.Entries)
            _inventories.Add(new Inventory(entry));
        }
      else
        _messageService.Error(result.Message);
      
      IsLoadingCircleVisible = false;
    }

    [RelayCommand]
    private void Appearing()
    {
      IsLoadingCircleVisible = true;
      Task.Run(async () => { await InitInventories(); });
    }

    [RelayCommand]
    private void FrameTapped(Inventory inventory)
    {
      IsLoadingCircleVisible = true;
      var navigationParameter =new Dictionary<string, object> { { "Inventory", inventory } };
      MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync(nameof(InventoryPage), navigationParameter); });
      //IsLoadingCircleVisible = false;
    }
  }
}
