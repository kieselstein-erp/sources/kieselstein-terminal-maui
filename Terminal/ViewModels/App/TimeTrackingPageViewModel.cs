﻿using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Popups;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Machine;
using KieselsteinErp.Terminal.Services.Readers;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.Staff;
using KieselsteinErp.Terminal.Services.StateMachine;
using KieselsteinErp.Terminal.Services.User;
using KieselsteinErp.Terminal.Services.Worktime;
using KieselsteinErp.Terminal.Util;
using SkiaSharp;
using System.Collections.ObjectModel;

namespace KieselsteinErp.Terminal.ViewModels.App
{
  public partial class TimeTrackingPageViewModel : ObservableObject
  {
    private readonly IMessageService _messageService;
    private readonly ISettingsService _settingsService;
    private readonly IStaffService _staffService;
    private readonly IStateMachine _stateMachine;
    private readonly IUserService _userService;
    private readonly IDeviceManager _deviceManager;
    private readonly IWorktimeService _workTimeService;
    private readonly IMachineService _machineService;


    [ObservableProperty]
    private bool _isBuchenAllowed = true;

    [ObservableProperty]
    private bool _isSaldoVisible = true;

    [ObservableProperty]
    private byte[] _image;

    [ObservableProperty]
    private bool _isLoadingCircleVisible = false;

    [ObservableProperty]
    private bool _isWorktimeVisible = false;

    [ObservableProperty]
    private bool _isRequestsVisible = false;

    [ObservableProperty]
    private bool _isSaldoReportVisible = false;

    [ObservableProperty]
    private bool _isMonthlyReportVisible = false;

    [ObservableProperty]
    private int _saldoOffset = 0;

    [ObservableProperty]
    private bool _isSaldoIncreaseVisible = false;

    [ObservableProperty]
    private bool _isSaldoDecreaseVisible = true;

    [ObservableProperty]
    private string _timeBalance = string.Empty;

    [ObservableProperty]
    private string _availableVacation = string.Empty;


    ObservableCollection<ZeitdatenEntry> _worktimeEntries = new ObservableCollection<ZeitdatenEntry>();
    public ObservableCollection<ZeitdatenEntry> WorktimeEntries { get => _worktimeEntries; set => SetProperty(ref _worktimeEntries, value); }

    ObservableCollection<MachineEntry> _machineEntries = new ObservableCollection<MachineEntry>();
    public ObservableCollection<MachineEntry> MachineEntries { get => _machineEntries; set => SetProperty(ref _machineEntries, value); }

    private StaffEntry _staffEntry;

    public TimeTrackingPageViewModel(
                    IMessageService messageService,
                    ISettingsService settingsService,
                    IUserService userService,
                    IStaffService staffService,
                    IStateMachine stateMachine,
                    IDeviceManager deviceManager,
                    IWorktimeService worktimeService,
                    IMachineService machineService)
    {
      _messageService = messageService;
      _settingsService = settingsService;
      _userService = userService;
      _staffService = staffService;
      _stateMachine = stateMachine;
      _deviceManager = deviceManager;
      _workTimeService = worktimeService;
      _machineService = machineService;


      IsSaldoVisible = _settingsService.IsSaldoVisible;
      IsRequestsVisible = _settingsService.IsRequestsVisible;
      _staffEntry = _staffService.FindStaffByUserid(_userService.Userid);

    }

    private void HandlePieceConfirmationMessage(object recipient, PieceConfirmationMessage message)
    {
      throw new NotImplementedException();
    }

    private void HandleLoadingCircleMessage(object recipient, LoadingCircleMessage message)
    {
      if (message != null)
        IsLoadingCircleVisible = message.Value;
    }

    private void HandleSaldoMessage(object recipient, SaldoMessage message)
    {
      TimeBalance = message.Value.timeBalance.ToString();
      AvailableVacation = message.Value.availableVacation.ToString() + " " + message.Value.unitVacation.ToString();
      IsSaldoReportVisible = true;
      IsLoadingCircleVisible = false;
    }

    private void HandlePdfMessage(object recipient, PdfMessage message)
    {
      if (!string.IsNullOrEmpty(message.Value))
      {
        SKImage skImage = SKImage.FromBitmap(PDFtoImage.Conversion.ToImage(pdfAsBase64String: message.Value, options: new(Dpi: 300, WithAspectRatio: true)));
        SKData encoded = skImage.Encode();
        Image = null;
        using (MemoryStream memory = new MemoryStream())
        {
          encoded.AsStream().CopyTo(memory);
          Image = memory.ToArray();
          IsMonthlyReportVisible = true;
          IsWorktimeVisible = false;
        }
      }
      IsLoadingCircleVisible = false;
    }

    [RelayCommand]
    private async Task Appearing()
    {
      await DisplayBookings();
      WeakReferenceMessenger.Default.Register<RefreshMessage>(this, HandleRefresh);
      IsBuchenAllowed = true;
      IsLoadingCircleVisible = false;
      WeakReferenceMessenger.Default.Register<SaldoMessage>(this, HandleSaldoMessage);
      WeakReferenceMessenger.Default.Register<PdfMessage>(this, HandlePdfMessage);
      WeakReferenceMessenger.Default.Register<LoadingCircleMessage>(this, HandleLoadingCircleMessage);
      WeakReferenceMessenger.Default.Register<PieceConfirmationMessage>(this, HandlePieceConfirmationMessage);
    }

    [RelayCommand]
    private void Disappearing()
    {
      WeakReferenceMessenger.Default.UnregisterAll(this);
      IsSaldoReportVisible = false;
      IsMonthlyReportVisible = false;
      Image = null;
    }

    private void HandleRefresh(object recipient, RefreshMessage message)
    {
      IsLoadingCircleVisible = false;
      MainThread.BeginInvokeOnMainThread(async () => await DisplayBookings());
    }


    private async Task DisplayBookings()
    {
      IsWorktimeVisible = false;
      WorktimeEntries = await GetAllEntries(DateTime.Now);
      IsWorktimeVisible = (WorktimeEntries != null && WorktimeEntries.Count > 0);
    }

    [RelayCommand]
    private async Task KommtBuchung()
    {
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Coming)));
    }

    [RelayCommand]
    private async Task PauseBuchung()
    {
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Pausing)));
    }

    [RelayCommand]
    private async Task GehtBuchung()
    {
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Going)));
    }

    [RelayCommand]
    private async Task Saldo()
    {
      IsWorktimeVisible = false;
      IsLoadingCircleVisible = true;
      IsBuchenAllowed = false;
      SaldoOffset = 0;
      IsSaldoIncreaseVisible = false;
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Balance)));
    }

    [RelayCommand]
    private async Task SaldoDetails()
    {
      //MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync(nameof(TimeTrackingReportPage)); });
      IsSaldoReportVisible = false;
      SaldoOffset = 0;
      IsSaldoIncreaseVisible = false;
      IsLoadingCircleVisible = true;
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.MonthlyReport, true, 0)));
    }

    [RelayCommand]
    private async Task SaldoIncrease()
    {
      IsLoadingCircleVisible = true;
      SaldoOffset += 1;
      IsSaldoIncreaseVisible = !(SaldoOffset == 0);
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.MonthlyReport, true, SaldoOffset)));
    }

    [RelayCommand]
    private async Task SaldoDecrease()
    {
      IsLoadingCircleVisible = true;
      SaldoOffset -= 1;
      IsSaldoIncreaseVisible = !(SaldoOffset == 0);
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.MonthlyReport, true, SaldoOffset)));
    }

    [RelayCommand]
    void SaldoClose()
    {
      IsSaldoReportVisible = false;
      IsWorktimeVisible = (WorktimeEntries != null && WorktimeEntries.Count > 0);
      IsBuchenAllowed = true;
      SaldoOffset = 0;
    }

    [RelayCommand]
    private void CloseReport()
    {
      Image = null;
      IsMonthlyReportVisible = false;
      IsWorktimeVisible = (WorktimeEntries != null && WorktimeEntries.Count > 0);
      IsBuchenAllowed = true;
    }

    [RelayCommand]
    private async Task Scan()
    {
      if (_deviceManager.IsScanDevice())
        _deviceManager.PressScanButton(true);
      else
        await Application.Current.MainPage.ShowPopupAsync(new BarcodeCameraPopup());
    }

    [RelayCommand]
    private async Task Requests()
    {
      PopupRequestsResult res = (PopupRequestsResult)await Application.Current.MainPage.ShowPopupAsync(new RequestsPopup(_settingsService));
      if (res != null && !res.Canceled) 
      {
        string toastMessage = string.Empty;
        switch (res.Type)
        {
          case SpecialTimeType.Holiday:
            toastMessage = string.Format("Vacation requested from {0} to {1}", DateTimeHelper.FromMillisecondsSinceUnixEpoch(res.Entry.fromDateMs), DateTimeHelper.FromMillisecondsSinceUnixEpoch(res.Entry.toDateMs));
            break;
          case SpecialTimeType.TimeCompensation:
            toastMessage = string.Format("Time compensation requested from {0} to {1}", DateTimeHelper.FromMillisecondsSinceUnixEpoch(res.Entry.fromDateMs), DateTimeHelper.FromMillisecondsSinceUnixEpoch(res.Entry.toDateMs));
            break;
          case SpecialTimeType.Illness:
            toastMessage = string.Format("Sick leave requested from {0} to {1}", DateTimeHelper.FromMillisecondsSinceUnixEpoch(res.Entry.fromDateMs), DateTimeHelper.FromMillisecondsSinceUnixEpoch(res.Entry.toDateMs));
            break;
        }

        BookingResult result = await _workTimeService.BookSpecialTime(toastMessage, null, res.Entry);
        if (result.Success)
          _messageService.ShowToast(toastMessage, CommunityToolkit.Maui.Core.ToastDuration.Long);
      }
    }

    private async Task<ObservableCollection<ZeitdatenEntry>> GetAllEntries(DateTime dt)
    {
      int maxDays = 2;
      ObservableCollection<ZeitdatenEntry> newEntries = new();
      for (int day = 0; day < maxDays; day++)
      {
        dt = dt.AddDays(-day);
        List<ZeitdatenEntry> zeitdatenEntries = await _workTimeService.GetWorktimeEntriesAsync(dt.Day, dt.Month, dt.Year, _staffEntry.personalNr);
        if (zeitdatenEntries != null)
          for (int i = zeitdatenEntries.Count() - 1; i >= 0; i--)
          {
            newEntries.Add(zeitdatenEntries[i]);
            if (zeitdatenEntries[i].activityCnr != null && zeitdatenEntries[i].activityCnr.ToUpper().Equals(IWorktimeService.ActivityComming)) return newEntries;
          }
      }
      return newEntries;
    }
  }
}
