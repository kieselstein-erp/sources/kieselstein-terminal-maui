﻿using CommunityToolkit.Maui.Storage;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using InTheHand.Net.Bluetooth;
using InTheHand.Net.Sockets;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Resources.Localization;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Machine;
using KieselsteinErp.Terminal.Services.Partslist;
using KieselsteinErp.Terminal.Services.Readers;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Util;
using LocalizationResourceManager.Maui;
using MetroLog.Maui;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using UraniumUI.Icons.MaterialIcons;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;
using static KieselsteinErp.Terminal.Services.Settings.ISettingsService;

namespace KieselsteinErp.Terminal.ViewModels
{
  public partial class SettingsPageViewModel : ObservableObject
  {
    private readonly ILocalizationResourceManager _localizationResourceManager;
    private readonly ISettingsService _settingsService;
    private readonly IMachineService _machineService;
    private readonly IPartslistService _partslistService;
    private readonly ILogger _logger;
    private readonly IMessageService _messageService;

    #region "observables"
    [ObservableProperty]
    string serverUrl;
    [ObservableProperty]
    string _nfcReaderType;
    [ObservableProperty]
    string _nfcReaderSerialPort;
    [ObservableProperty]
    string _barcodeReaderType;
    [ObservableProperty]
    string _barcodeReaderSerialPort;
    [ObservableProperty]
    string _currentLocale;
    [ObservableProperty]
    ObservableCollection<string> _availablePortNames;
    [ObservableProperty]
    ObservableCollection<string> _availableNfcTypes;
    [ObservableProperty]
    bool _nfcReaderEnabled;
    [ObservableProperty]
    ObservableCollection<string> _availableBarcodeReaderTypes;
    [ObservableProperty]
    ObservableCollection<string> _availableLocales;
    [ObservableProperty]
    string _logonLocale;
    [ObservableProperty]
    string _logonMandant;
    [ObservableProperty]
    bool _checkInPage;
    [ObservableProperty]
    bool _saldoButton;
    [ObservableProperty]
    bool _pieceButton;
    [ObservableProperty]
    string _settingsPassword;
    [ObservableProperty]
    bool _isPasswordHidden = true;
    [ObservableProperty]
    string _eyeIconSource = MaterialRegular.Visibility_off;
    [ObservableProperty]
    bool _isFullScreen;
    [ObservableProperty]
    bool _isSizable;
    [ObservableProperty]
    int _windowWidth;
    [ObservableProperty]
    int _windowHeight;
    [ObservableProperty]
    string _title;
    [ObservableProperty]
    bool _isSaveEnabled = false;
    [ObservableProperty]
    string _identifierDeliveryNote;
    [ObservableProperty]
    bool _bookDeliveryBadAmount;
    [ObservableProperty]
    bool _isTerminal = true;
    [ObservableProperty]
    string _version;
    [ObservableProperty]
    string _bluetoothDevicename;
    [ObservableProperty]
    string _bluetoothDeviceAddressString;
    [ObservableProperty]
    bool _deviceSerialnumbers = false;
    [ObservableProperty]
    bool _requestsButton;
    [ObservableProperty]
    bool _isPaperlessManufacturing = false;
    [ObservableProperty]
    List<ProductionGroupEntry> _productionGroups = [];
    [ObservableProperty]
    ProductionGroupEntry _productionGroup = null;
    [ObservableProperty]
    List<MachineGroupEntry> _machineGroups = [];
    [ObservableProperty]
    MachineGroupEntry _machineGroup = null;
    [ObservableProperty]
    List<MachineEntry> _machines = [];
    [ObservableProperty]
    MachineEntry _machine = null;
    [ObservableProperty]
    int _refreshIntervalPaperlessManufacturing = 0; // 0 = off
    [ObservableProperty]
    int _daysForPlanning = 14;
    [ObservableProperty]
    int _paperlessManufacturingRequestLimit = 200;
    [ObservableProperty]
    bool _isNfcReverse = false;
    [ObservableProperty]
    bool _itemBarcodeWithoutPrefix = false;
    [ObservableProperty]
    bool _allowSickRequest = false;
    [ObservableProperty] 
    ObservableCollection<string> _availableSortOrders;
    [ObservableProperty]
    string _currentSortOrder = null;
    [ObservableProperty]
    bool _isLoadingCircleVisible = false;
    [ObservableProperty]
    bool _machineButton = false;
    #endregion

    private bool _settingChangedMsg = false;

    public SettingsPageViewModel(ILogger<SettingsPageViewModel> logger, IMessageService messageService, ILocalizationResourceManager localizationResourceManager,
            ISettingsService settingsService, IEnumerable<IReader> readers, IMachineService machineService, IPartslistService partslistService)
    {
      _logger = logger;
      _messageService = messageService;
      _settingsService = settingsService;
      _machineService = machineService;
      _partslistService = partslistService;

      // Init localization
      _localizationResourceManager = localizationResourceManager;
      AvailableLocales = new ObservableCollection<string>(LocaleService.GetAvailableLocales());
      CurrentLocale = _localizationResourceManager.CurrentCulture.Name;

      // Init sort orders
      string[] sortOrders = Enum.GetNames(typeof(EStaffSortedBy));
      AvailableSortOrders = new ObservableCollection<string>();
      foreach (string sortOrder in sortOrders)
      {
        AvailableSortOrders.Add(AppResources.ResourceManager.GetString(sortOrder));
      }

      Version = string.Format("Version {0}", AppInfo.VersionString);
      IsTerminal = !((DeviceInfo.Platform == DevicePlatform.Android) && (DeviceInfo.Idiom == DeviceIdiom.Phone));
      if (DeviceInfo.Platform == DevicePlatform.WinUI)
      {
        SerialNfcReader serialNfcReader = (SerialNfcReader)readers.OfType<SerialNfcReader>().First();
        var availablePorts = new ObservableCollection<string>(serialNfcReader.GetAvailablePorts());
        availablePorts.Insert(0, " ");
        LoadSettings(availablePorts);
      }
      else
        LoadSettings(null);

      if (DeviceInfo.Idiom != DeviceIdiom.Phone)
        _ = InitPaperlessProduction();

      IPAddress iPAddress = IpHelper.GetIpAddress();
      string ip = FormatIPv4(iPAddress);

      Title = Title_Settings + " (" + DeviceInfo.Current.Name + "  IP:" + ip + ")";
      this.PropertyChanged += OnPropertyChanged;
    }

    private async Task InitPaperlessProduction()
    {
      IsLoadingCircleVisible = true;
      ProductionGroups = await _partslistService.GetProductionGroupEntriesAsync();
      MachineGroups = await _machineService.GetMachineGroupEntriesForPlanningAsync();
      Machines = await _machineService.GetMachineEntriesForPlanningAsync();
      if (_settingsService.ManufacturingGroupId != null)
        ProductionGroup = ProductionGroups.Find(o => o.id == _settingsService.ManufacturingGroupId);
      if (_settingsService.MachineGroupId != null)
        MachineGroup = MachineGroups.Find(o => o.id == _settingsService.MachineGroupId);
      if (_settingsService.MachineId != null)
        Machine = Machines.Find(o => o.id == _settingsService.MachineId);
      IsSaveEnabled = false;
      _settingChangedMsg = false;
      IsLoadingCircleVisible = false;
    }

    private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (e.PropertyName == nameof(IsLoadingCircleVisible)) return;

      if (e.PropertyName != nameof(IsSaveEnabled))
        IsSaveEnabled = true;

      if (e.PropertyName != nameof(IsSaveEnabled) && e.PropertyName != nameof(CurrentLocale) && e.PropertyName != nameof(IdentifierDeliveryNote) && e.PropertyName != nameof(DeviceSerialnumbers) && e.PropertyName != nameof(AllowSickRequest))
        _settingChangedMsg = true;
    }

    private void LoadSettings(ObservableCollection<string> availablePorts)
    {
      ServerUrl = _settingsService.ServerUrl;
      LogonLocale = _settingsService.LogonLocale;
      LogonMandant = _settingsService.LogonMandant;
      CheckInPage = _settingsService.CheckInPage;
      SaldoButton = _settingsService.IsSaldoVisible;
      PieceButton = _settingsService.IsPieceVisible;
      RequestsButton = _settingsService.IsRequestsVisible;
      SettingsPassword = _settingsService.SettingsPassword;
      IsFullScreen = _settingsService.IsFullScreen;
      IsSizable = _settingsService.IsSizable && !IsFullScreen;
      WindowWidth = (int)_settingsService.WindowSize.Width;
      WindowHeight = (int)_settingsService.WindowSize.Height;
      IdentifierDeliveryNote = _settingsService.IdentifierDeliveryNote;
      BookDeliveryBadAmount = _settingsService.BookDeliveryBadAmount;
      BluetoothDevicename = _settingsService.BluetoothDeviceName;
      BluetoothDeviceAddressString = _settingsService.BluetoothDeviceAddress;
      DeviceSerialnumbers = _settingsService.DeviceAssemblyDelivery;
      IsPaperlessManufacturing = _settingsService.IsPaperlessManufacturing;
      RefreshIntervalPaperlessManufacturing = _settingsService.RefreshIntervallPaperlessManufacturing;
      DaysForPlanning = _settingsService.DaysForPlanning;
      PaperlessManufacturingRequestLimit = _settingsService.PaperlessManufacturingRequestLimit;
      IsNfcReverse = _settingsService.IsNfcReverse;
      ItemBarcodeWithoutPrefix = _settingsService.AllowItemBarcodeWithoutPrefix;
      AllowSickRequest = _settingsService.AllowSickRequest;
      CurrentSortOrder = AppResources.ResourceManager.GetString(_settingsService.StaffSortedBy.ToString());
      MachineButton = _settingsService.IsMachineVisible;

      if (DeviceInfo.Platform == DevicePlatform.WinUI)
      {
        NfcReaderType = _settingsService.NfcReaderType;
        NfcReaderSerialPort = _settingsService.NfcReaderSerialPort;

        BarcodeReaderType = _settingsService.BarcodeReaderType;
        BarcodeReaderSerialPort = _settingsService.BarcodeReaderSerialPort;

        // Liste erst nach ausgewähltem Objekt setzen, damit die Selection in der UI funktioniert
        // Achtung: auch im xaml die Reihung beachten (Liste vor Element)
        AvailablePortNames = availablePorts;

        AvailableNfcTypes = new ObservableCollection<string>(SerialNfcReader.GetAvailableSerialNfcReaderTypes());

        AvailableBarcodeReaderTypes = new ObservableCollection<string>(SerialBarcodeReader.GetAvailableSerialBarcodereaderTypes());
      }
      else if (DeviceInfo.Platform == DevicePlatform.Android)
      {
        NfcReaderEnabled = _settingsService.NfcReaderEnabled;
      }
      IsSaveEnabled = false;
      _settingChangedMsg = false;
    }

    private string FormatIPv4(IPAddress iPAddress)
    {
      var ips = iPAddress.ToString().Split('.').Select(x => x.PadLeft(3, '0'));
      string ipFormated = "";
      foreach (var ip in ips)
        ipFormated += (ipFormated.Equals("")) ? ip : "." + ip;
      return ipFormated;
    }

    [RelayCommand]
    void ShowPassword()
    {
      IsPasswordHidden = !IsPasswordHidden;
      if (IsPasswordHidden)
        EyeIconSource = MaterialRegular.Visibility_off;
      else
        EyeIconSource = MaterialRegular.Visibility;
    }

    [RelayCommand]
    void SaveSettings()
    {
      _settingsService.ServerUrl = ServerUrl;
      _settingsService.LogonLocale = LogonLocale;
      _settingsService.LogonMandant = LogonMandant;
      _settingsService.CheckInPage = CheckInPage;
      _settingsService.IsSaldoVisible = SaldoButton;
      _settingsService.IsPieceVisible = PieceButton;
      _settingsService.IsRequestsVisible = RequestsButton;
      _settingsService.SettingsPassword = SettingsPassword;
      _settingsService.IsFullScreen = IsFullScreen;
      _settingsService.IsSizable = IsSizable;
      _settingsService.WindowSize = new Size(WindowWidth, WindowHeight);
      _settingsService.IdentifierDeliveryNote = IdentifierDeliveryNote;
      _settingsService.BookDeliveryBadAmount = BookDeliveryBadAmount;
      _settingsService.DeviceAssemblyDelivery = DeviceSerialnumbers;
      _settingsService.IsPaperlessManufacturing = IsPaperlessManufacturing;
      _settingsService.DaysForPlanning = DaysForPlanning;
      _settingsService.PaperlessManufacturingRequestLimit = PaperlessManufacturingRequestLimit;
      _settingsService.IsNfcReverse = IsNfcReverse;
      _settingsService.AllowItemBarcodeWithoutPrefix = ItemBarcodeWithoutPrefix;
      _settingsService.AllowSickRequest = AllowSickRequest;
      _settingsService.IsMachineVisible = MachineButton;

      foreach(string s in Enum.GetNames(typeof(EStaffSortedBy)))
      {
        if (AppResources.ResourceManager.GetString(s).Equals(CurrentSortOrder))
        {
          _settingsService.StaffSortedBy = Converter.EnumConverter.GetAs<EStaffSortedBy>(s);
          break;
        }
      }

      if (IsPaperlessManufacturing)
      {
        _settingsService.ManufacturingGroupId = ProductionGroup?.id;
        _settingsService.MachineGroupId = MachineGroup?.id;
        _settingsService.MachineId = Machine?.id;
        _settingsService.RefreshIntervallPaperlessManufacturing = RefreshIntervalPaperlessManufacturing;
      }

      if (DeviceInfo.Platform == DevicePlatform.WinUI)
      {
        _settingsService.NfcReaderType = NfcReaderType.Trim();
        _settingsService.NfcReaderSerialPort = NfcReaderSerialPort.Trim();
        _settingsService.BarcodeReaderType = BarcodeReaderType.Trim();
        _settingsService.BarcodeReaderSerialPort = BarcodeReaderSerialPort.Trim();
      }
      else if (DeviceInfo.Platform == DevicePlatform.Android)
      {
        _settingsService.NfcReaderEnabled = NfcReaderEnabled;
      }
      _settingsService.BluetoothDeviceAddress = BluetoothDeviceAddressString;
      _settingsService.BluetoothDeviceName = BluetoothDevicename;

      IsSaveEnabled = false;
      if (_settingChangedMsg)
        _messageService.Info(KieselsteinErp.Terminal.Resources.Localization.AppResources.RestartTheProgramToApplyTheChangedSettings);
      _settingChangedMsg = false;
      //await App.Current.MainPage.Navigation.PopModalAsync();
    }

    [RelayCommand]
    async Task CloseSettings()
    {
      if (await DiscardChangesAsync())
        await KieselsteinErp.Terminal.App.Current.MainPage.Navigation.PopModalAsync();
    }

    private async Task<bool> DiscardChangesAsync()
    {
      if (IsSaveEnabled)
      {
        bool ret = await Application.Current.MainPage.DisplayAlert(Title_Settings, DiscardChanges, Yes, No);
        return ret;
      }
      else
        return true;
    }

    [RelayCommand]
    void ShowLog()
    {
      var logController = new LogController();
      logController.GoToLogsPageCommand.Execute(null);
    }

    [RelayCommand]
    async Task Export()
    {
      Dictionary<string, object> settings = SettingsHelper.GetSettingProperties(_settingsService);
      if (settings != null)
      {
        string settingsString = JsonConvert.SerializeObject(settings);
        if (!string.IsNullOrEmpty(settingsString))
        {
          MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(settingsString ?? ""));
          FileSaverResult result = await CommunityToolkit.Maui.Storage.FileSaver.SaveAsync("settings.json", ms, default);
          if (result.IsSuccessful)
          {
            _messageService.Info(string.Format(TheFileWasSavedSuccessfullyToLocation0, result.FilePath));
          }
          else if (result.FilePath != null)
          {
            _messageService.Warn(string.Format(TheFileWasNotSavedSuccessfullyWithError0, result.Exception.Message));
          }
        }
      }
    }

    [RelayCommand]
    async Task Import()
    {
      FileResult result = await FilePicker.PickAsync(new PickOptions
      {
        PickerTitle = PickASettingsJsonFile
      });

      try
      {
        using Stream fileStream = System.IO.File.OpenRead(result.FullPath);
        using StreamReader reader = new StreamReader(fileStream);
        var c = await reader.ReadToEndAsync();
        Dictionary<string, object> settings = JsonConvert.DeserializeObject<Dictionary<string, object>>(c);
        string[] errors = SettingsHelper.SetSettingProperties(_settingsService, settings);
        if (errors?.Length > 0)
        {
          string message = string.Empty;
          foreach (string s in errors)
            message += s + ",";
          _messageService.Error(string.Format(RestoreSettingsFailedErrors01, errors.Length, message));
        }
        else
        {
          _messageService.Info(TheFileWasLoadedAndApplied);
        }
      }
      catch (Exception ex)
      {
        _messageService.Error(ex.Message);
      }

    }

    [RelayCommand]
    async Task SearchBluetooth()
    {
      BluetoothDeviceInfo device = null;
      var picker = new BluetoothDevicePicker();
      picker.ClassOfDevices.Add(new ClassOfDevice(DeviceClass.PeripheralHandheldBarcodeScanner, ServiceClass.Capturing));
      device = await picker.PickSingleDeviceAsync();
      if (device != null)
      {
        if (!device.Authenticated)
        {
          bool paired = BluetoothSecurity.PairRequest(device.DeviceAddress, null);
          await Task.Delay(1000);
        }
        device.Refresh();
        BluetoothDevicename = device.DeviceName;
        BluetoothDeviceAddressString = device.DeviceAddress.ToString();
      }
      else
      {
        BluetoothDevicename = string.Empty;
        BluetoothDeviceAddressString = string.Empty;
      }
    }

    [RelayCommand]
    void Clear(string type)
    {
      switch (type)
      {
        case "0":
          ProductionGroup = null;
          break;
        case "1":
          MachineGroup = null;
          break;
        case "2":
          Machine = null;
          break;
      }
    }

    /// <summary>
    /// Delegate called on locale change
    /// 
    /// Switch current culture to selected culture
    /// </summary>
    /// <param name="value"></param>
    partial void OnCurrentLocaleChanged(string value)
    {
      if (value != null)
        _localizationResourceManager.CurrentCulture = new System.Globalization.CultureInfo(value);
    }

    [RelayCommand]
    async Task ExitApplicationAsync()
    {
      if (await DiscardChangesAsync())
        System.Diagnostics.Process.GetCurrentProcess().CloseMainWindow();
    }
  }

  //public class Port { public string Name { get; set; } }

}
