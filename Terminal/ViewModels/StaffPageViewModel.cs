﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.Staff;
using KieselsteinErp.Terminal.Services.StateMachine;
using KieselsteinErp.Terminal.Services.User;
using KieselsteinErp.Terminal.Views;
using System.Collections.ObjectModel;
using System.Diagnostics;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;
using static KieselsteinErp.Terminal.Services.Settings.ISettingsService;

namespace KieselsteinErp.Terminal.ViewModels
{
  public partial class StaffPageViewModel : ObservableObject
  {

    private readonly IMessageService _messageService;
    private readonly IUserService _userService;
    private readonly IStaffService _staffService;
    private readonly IStateMachine _stateMachine;
    private readonly ISettingsService _settingsService;

    ObservableCollection<StaffButton> _staffButtons = new ObservableCollection<StaffButton>();
    public ObservableCollection<StaffButton> StaffButtons { get { return _staffButtons; } }

    // update timer for actual Date-Time display
    private readonly IDispatcherTimer _timer;

    [ObservableProperty]
    private string labelDateTime;

    [ObservableProperty]
    private string title = string.Format(Title_Application, AppInfo.Current.VersionString);

    [ObservableProperty]
    private string onlineString;

    [ObservableProperty]
    private bool _isLoadingCircleVisible = false;

    private bool _isOnline;

    public StaffPageViewModel(
      IMessageService messageService,
      IUserService userService,
      IStaffService staffService,
      IStateMachine stateMachine,
      ISettingsService settingsService)
    {
      _messageService = messageService;
      _userService = userService;
      _staffService = staffService;
      _stateMachine = stateMachine;
      _settingsService = settingsService;

      _timer = Application.Current.Dispatcher.CreateTimer();
      _timer.Interval = TimeSpan.FromSeconds(1);
      _timer.Tick += (s, e) =>
      {
        LabelDateTime = DateTime.Now.ToString();
      };
      _userService.OnLogonChanged += (s, e) =>
      {
        _isOnline = e;
        if (e)
        {
          OnlineString = Connection_State_Online;
          RefreshStaff();
        }
        else
          OnlineString = Connection_State_Offline;
      };
      if (_isOnline = !string.IsNullOrEmpty(_userService.Userid))
        OnlineString = Connection_State_Online;
      else
        OnlineString = Connection_State_Offline;
    }

    private void RefreshStaff()
    {
      if (!string.IsNullOrEmpty(_userService.Userid))
      {
        IEnumerable<StaffEntry> filtered = GetSortedAndFilteredStaff();
        MainThread.BeginInvokeOnMainThread(() =>
        {
          StaffButtons.Clear();
          foreach (StaffEntry staff in filtered)
          {
            StaffButtons.Add(new StaffButton() { Name = $"{staff.firstName} {staff.name}".Trim(), Personalnummer = staff.personalNr, Identity = staff.identityCnr });
            Debug.Print(StaffButtons.Count + " | " + staff.name);
          }
          WeakReferenceMessenger.Default.Send(new RefreshMessage(true));
        });
      }
    }

    private IEnumerable<StaffEntry> GetSortedAndFilteredStaff()
    {
      StaffEntry[] staffs = _staffService.GetStaffEntries();
      EStaffSortedBy staffOrderBy = _settingsService.StaffSortedBy;
      switch (staffOrderBy)
      {
        case EStaffSortedBy.FirstName:
          Array.Sort(staffs, (x, y) => ("" + x.firstName).CompareTo("" + y.firstName));
          break;
        case EStaffSortedBy.Name_FirstName:
          staffs = staffs.OrderBy(a => a.name).ThenBy(a => a.firstName).ToArray();
          break;
        case EStaffSortedBy.StaffNumber:
          Array.Sort(staffs, (x, y) => x.personalNr.CompareTo(y.personalNr));
          break;
        default:
          Array.Sort(staffs, (x, y) => x.name.CompareTo(y.name));
          break;
      }
      var filtered = staffs.Where((x) => !string.IsNullOrEmpty(x.identityCnr));
      return filtered;
    }

    [RelayCommand]
    void Appearing()
    {
      Debug.WriteLine("StaffPage Appearing");
      bool valid = _isOnline ? !string.IsNullOrEmpty(_userService.Userid) : _staffService.GetStaffCountOffline() > 0;
      if (valid)
      {

        if (StaffButtons.Count == 0)
        {
          //StaffEntry[] staffs = _staffService.GetStaffEntries();
          //Array.Sort(staffs, (x, y) => x.name.CompareTo(y.name));
          //var filtered = staffs.Where((x) => !string.IsNullOrEmpty(x.identityCnr));
          IEnumerable<StaffEntry> filtered = GetSortedAndFilteredStaff();
          foreach (StaffEntry staff in filtered)
          {
            StaffButtons.Add(new StaffButton() { Name = $"{staff.firstName} {staff.name}".Trim(), Personalnummer = staff.personalNr, Identity = staff.identityCnr });
            Debug.Print(StaffButtons.Count + " | " + staff.name);
          }
        }
      }

      _timer.Start();
    }

    [RelayCommand]
    void Disappearing()
    {
      Debug.WriteLine("StaffPage Disappearing");
      try
      {
        _timer?.Stop();
      }
      catch (Exception ex)
      {
        Debug.WriteLine(ex.ToString());
      }
    }


    [RelayCommand]
    async Task Settings()
    {
      try
      {
        IsLoadingCircleVisible = true;
        await SettingsHelper.ShowSettings(_settingsService.SettingsPassword);
      }
      finally { IsLoadingCircleVisible = false; }
    }

    [RelayCommand]
    async Task StaffBtn(string identity)
    {
      Debug.Print($"Button {identity}");
      try
      {
        await _stateMachine.OnCodeReceived(new UserAuthEvent(identity));
        await Shell.Current.GoToAsync("///" + nameof(MainPage));
      }
      catch (Exception ex)
      {
        Debug.Print(ex.ToString());
      }
    }
  }
}
