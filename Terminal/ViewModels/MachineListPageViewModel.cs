﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Machine;
using KieselsteinErp.Terminal.Services.Staff;
using KieselsteinErp.Terminal.Services.Worktime;
using System.Collections.ObjectModel;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.ViewModels
{
  public partial class MachineListPageViewModel : ObservableObject, IQueryAttributable
  {
    private readonly IMachineService _machineService;
    private readonly IWorktimeService _worktimeService;
    private readonly IStaffService _staffService;
    private readonly IMessageService _messageService;
    private StaffEntry _staffEntry;
    private bool _needWorkTimeRefresh = false;

    [ObservableProperty]
    private bool _isLoadingCircleVisible = false;

    ObservableCollection<MachineWithStaff> _machines = new();
    public ObservableCollection<MachineWithStaff> Machines { get { return _machines; } }

    public MachineListPageViewModel(IMachineService machineService, IWorktimeService worktimeService, IStaffService staffService, IMessageService messageService) 
    { 
      _machineService = machineService;
      _worktimeService = worktimeService;
      _staffService = staffService;
      _messageService = messageService;
    }

    public void ApplyQueryAttributes(IDictionary<string, object> query)
    {
      _staffEntry = query[nameof(StaffEntry)] as StaffEntry;
    }

    private async Task LoadMachines()
    {
      try
      {
        IsLoadingCircleVisible = true;
        Machines.Clear();
        _needWorkTimeRefresh = false;
        List<MachineEntry> machines = await _machineService.GetMachineEntriesAsync();
        foreach (MachineEntry machine in machines)
          if (machine.starttime != 0)
          {
            MachineWithStaff machineWithStaff = new MachineWithStaff() { MachineEntry = machine, StaffName = GetStaffName(machine) };
            Machines.Add(machineWithStaff);
          }
        if (Machines.Count == 0)
          _messageService.Info(NoRunningMachinesFound);
      }
      finally { IsLoadingCircleVisible = false; }
    }

    private string GetStaffName(MachineEntry machine)
    {
      string staffName;
      try
      {
        staffName = _staffService.GetFormatedStaffName(machine.personalIdStarter);
      }
      catch (Exception)
      {
        staffName = "Unknown";
      }

      return staffName;
    }

    [RelayCommand]
    private async Task Appearing()
    {
      await LoadMachines();
    }

    [RelayCommand]
    private void Disappearing()
    {
      if (_needWorkTimeRefresh)
        WeakReferenceMessenger.Default.Send(new RefreshWorkTimeMessage(true));
    }

    [RelayCommand]
    private async Task MachineStop(MachineWithStaff machine)
    {
      BookingResult result = await _worktimeService.StartStopMachineAsync(true, machine.MachineEntry.id, machine.MachineEntry.productionWorkplanId, _staffEntry.id, DateTime.Now);
      if (result.Success)
      {
        Machines.Remove(machine);
        _needWorkTimeRefresh = true;
        _messageService.ShowToast(string.Format("Machine {0} was stopped", machine.MachineEntry.description), CommunityToolkit.Maui.Core.ToastDuration.Short);
      }
      else
        _messageService.Error(result.Message);
    }

    [RelayCommand]
    private async Task Close() 
    {
      await Shell.Current.Navigation.PopModalAsync();
    }

  }
}
