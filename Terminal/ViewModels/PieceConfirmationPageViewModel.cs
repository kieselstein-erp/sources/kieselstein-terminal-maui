﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Resources.Localization;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Production;
using KieselsteinErp.Terminal.Services.Staff;
using KieselsteinErp.Terminal.Services.StateMachine;
using KieselsteinErp.Terminal.Services.Worktime;
using KieselsteinErp.Terminal.Util;
using Microsoft.Extensions.Logging;
using System.Collections.ObjectModel;

namespace KieselsteinErp.Terminal.ViewModels
{
  public partial class PieceConfirmationPageViewModel : ObservableObject, IQueryAttributable
  {
    private readonly ILogger _logger;
    private readonly IMessageService _messageService;
    private readonly IProductionRestService _productionService;
    private readonly IWorktimeService _worktimeService;
    private readonly IStateMachine _stateMachine;
    private readonly IStaffService _staffService;
    private string _forStaffCnr;
    private WorkTimeEvent _workTimeEvent = new WorkTimeEvent(WorkTimeEventType.Stopping);
    private ProductionRecordingEntry _worktimeToBook = null;

    [ObservableProperty]
    private bool _isLoadingCircleVisible = false;
    [ObservableProperty]
    private string _title;
    [ObservableProperty]
    private bool _isGhostShift = false;

    private GhostShiftInfo _ghostShiftInfo;

    ObservableCollection<PieceConfirmation> _pieceConfirmations = new ObservableCollection<PieceConfirmation>();
    public ObservableCollection<PieceConfirmation> PieceConfirmations { get { return _pieceConfirmations; } }


    public PieceConfirmationPageViewModel(ILogger<PieceConfirmationPageViewModel> logger, IMessageService messageService, IProductionRestService productionService,
      IWorktimeService worktimeService, IStateMachine stateMachine, IStaffService staffService)
    {
      _logger = logger;
      _messageService = messageService;
      _productionService = productionService;
      _worktimeService = worktimeService;
      _stateMachine = stateMachine;
      _staffService = staffService;
    }

    public void ApplyQueryAttributes(IDictionary<string, object> query)
    {
      try
      {
        IsLoadingCircleVisible = true;
        PieceConfirmationQuery pieceConfirmationQuery = (PieceConfirmationQuery)query[nameof(PieceConfirmationQuery)];
        _forStaffCnr = pieceConfirmationQuery.Personalnummer;
        _workTimeEvent = pieceConfirmationQuery.WorkTimeEvent;
        _worktimeToBook = pieceConfirmationQuery.ProductionRecordingEntry;
        OpenWorkEntryList openWorkEntryList = pieceConfirmationQuery.OpenWorkEntryList;

        if (pieceConfirmationQuery.IsGhostShiftConfirmation)
        {
          IsGhostShift = true;
          _ghostShiftInfo = pieceConfirmationQuery.GhostShiftInfo;

          if (pieceConfirmationQuery.OpenWorkEntryList.entries != null && pieceConfirmationQuery.OpenWorkEntryList.entries.Count == 1)
          {
            OpenWorkEntry entry = openWorkEntryList.entries[0];
            Title = AppResources.PieceConfirmation + " " + string.Format(AppResources.ForGhostShiftMachine01, entry.machineCnr, entry.machineDescription);
          }
          else
            Title = AppResources.PieceConfirmation + " " + string.Format(AppResources.ForGhostShiftMachine01, "??", AppResources.Unknown);
        }
        else
          Title = AppResources.PieceConfirmation;

        //_forStaffCnr = query[nameof(StaffEntry.personalNr)] as string;
        //_workTimeEvent = (WorkTimeEvent)query[nameof(WorkTimeEvent)];
        //_worktimeToBook = (ProductionRecordingEntry)query[nameof(ProductionRecordingEntry)];

        //OpenWorkEntryList openWorkEntryList = null;
        //if (query.ContainsKey(nameof(OpenWorkEntryList)))
        //  openWorkEntryList = (OpenWorkEntryList)query[nameof(OpenWorkEntryList)];

        InitList(openWorkEntryList);
      }
      finally { IsLoadingCircleVisible = false; }
    }

    private async void InitList(OpenWorkEntryList openWorkEntryList)
    {
      if (openWorkEntryList == null)
        openWorkEntryList = await _productionService.OpenWorkEntriesForStaff(_forStaffCnr, null, null, false);

      MainThread.BeginInvokeOnMainThread(() =>
      {
        // need to clear, otherwise the listelements are doubled on first showing
        _pieceConfirmations.Clear();
        foreach (var entry in openWorkEntryList.entries)
          _pieceConfirmations.Add(new PieceConfirmation(entry));
      });
    }

    [RelayCommand]
    private async Task Ok()
    {
      BookingResult result = await BookAllWorkTimes();
      if (result.Success)
      {
        if (IsGhostShift)
          CalculateEndTimeAndStopMachine();

        if (_worktimeToBook != null)
          await BookWorkTime(_worktimeToBook);

        _messageService.ShowToast(result.Message, CommunityToolkit.Maui.Core.ToastDuration.Short);
        await Shell.Current.Navigation.PopModalAsync();

        if (_workTimeEvent.NoUnidentify) return;
        await _stateMachine.OnCodeReceived(new UserAuthEvent(null));
      }
      else
        _messageService.Error(result.Message);
    }

    private void CalculateEndTimeAndStopMachine()
    {
      if (_ghostShiftInfo.MachineEntry == null)
      {
        _logger.LogError("MachineEntry for ghost shift is null");
        return;
      }
      if (_ghostShiftInfo.JobTimeMs == 0)
      {
        _logger.LogError("Jobtime for workstep ghost shift = 0");
        return;
      }
      decimal amount = 0;
      foreach (PieceConfirmation entry in _pieceConfirmations)
      {
        if (entry.GoodAmount != null)
          amount += (decimal)entry.GoodAmount;
        if (entry.BadAmount != null)
          amount += (decimal)entry.BadAmount;
      }
      decimal total = Math.Round(amount * _ghostShiftInfo.JobTimeMs, 0);
      DateTime startTime = DateTimeHelper.FromMillisecondsSinceUnixEpoch(_ghostShiftInfo.MachineEntry.starttime);
      DateTime endTime = startTime.AddMilliseconds(Convert.ToDouble(total));
      if (endTime > DateTime.Now)
        endTime = DateTime.Now;
      StaffEntry staff = _staffService.FindStaffByStaffCnr(_forStaffCnr);
      _worktimeService.StartStopMachineAsync(true, _ghostShiftInfo.MachineEntry.id, _ghostShiftInfo.MachineEntry.productionWorkplanId, staff.id, endTime);
    }

    private async Task<BookingResult> BookAllWorkTimes()
    {
      GoodBadEntryList goodBadEntryList = new();
      foreach (PieceConfirmation entry in _pieceConfirmations)
        goodBadEntryList.entries.Add(new GoodBadEntry(entry.Id, entry.GoodAmount == null ? 0 : (decimal)entry.GoodAmount, entry.BadAmount == null ? 0 : (decimal)entry.BadAmount, entry.Finished));

      return await _worktimeService.BookWorktimeGoodBad(_workTimeEvent, _forStaffCnr, goodBadEntryList, DateTime.Now);
    }

    [RelayCommand]
    private async Task StartWorkstepsAgain()
    {
      BookingResult result = await BookAllWorkTimes();
      List<PieceConfirmation> list = [];
      if (result.Success)
      {
        _messageService.ShowToast(result.Message, CommunityToolkit.Maui.Core.ToastDuration.Short);

        foreach (PieceConfirmation entry in _pieceConfirmations)
        {
          if (!entry.Finished)
            list.Add(entry);
        }
        if (list.Count > 0)
        {
          await Shell.Current.Navigation.PopModalAsync();
          await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.ContinueWorksteps, true, list));
        }
        else
        {
          await Shell.Current.Navigation.PopModalAsync();
          if (_workTimeEvent.NoUnidentify) return;
          await _stateMachine.OnCodeReceived(new UserAuthEvent(null));
        }
      }
      else
        _messageService.Error(result.Message);
    }

    private async Task BookWorkTime(ProductionRecordingEntry worktimeToBook)
    {
      var actual = DateTime.Now;
      // The time must be adjusted after booking the piece confirmation to maintain the order of bookings
      worktimeToBook.hour = actual.Hour;
      worktimeToBook.minute = actual.Minute;
      worktimeToBook.second = actual.Second;
      BookingResult bookingResult = await _worktimeService.BookProductionRecordingAsync(worktimeToBook);
      if (!bookingResult.Success)
        _messageService.Error(bookingResult.Message);
    }

    [RelayCommand]
    async Task Cancel()
    {
      await Shell.Current.Navigation.PopModalAsync();
    }

    [RelayCommand]
    async Task Continue()
    {
      if (_worktimeToBook != null)
      {
        BookingResult bookingResult = await _worktimeService.BookProductionRecordingAsync(_worktimeToBook);
        if (bookingResult.Success)
          _messageService.ShowToast(_ghostShiftInfo.ToastMessage, CommunityToolkit.Maui.Core.ToastDuration.Short);
        else
          _messageService.Error(bookingResult.Message);
      }
      await Shell.Current.Navigation.PopModalAsync();

      if (_workTimeEvent.NoUnidentify) return;
      await _stateMachine.OnCodeReceived(new UserAuthEvent(null));
    }
  }
}
