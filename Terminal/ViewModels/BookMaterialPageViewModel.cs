﻿using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Models.ScanLabels;
using KieselsteinErp.Terminal.Popups;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Item;
using KieselsteinErp.Terminal.Services.Production;
using System.Collections.ObjectModel;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.ViewModels
{
  public partial class BookMaterialPageViewModel : ObservableObject, IQueryAttributable
  {
    private readonly IProductionRestService _productionService;
    private readonly IMessageService _messageService;
    private readonly IItemV1Service _itemV1Service;
    private ProductionEntry _productionEntry;
    private bool _isActivePage = false;

    ObservableCollection<BookingMaterial> _bookingMaterials = new();
    public ObservableCollection<BookingMaterial> BookingMaterials { get { return _bookingMaterials; } }

    public event EventHandler<BookingMaterial> ScrollToIndex;

    [ObservableProperty]
    private string title = string.Format(BookLotMaterialsFor0, "");

    public BookMaterialPageViewModel(IProductionRestService productionService, IMessageService messageService, IItemV1Service itemV1Service)
    {
      _productionService = productionService;
      _messageService = messageService;
      _itemV1Service = itemV1Service;
      _isActivePage = true;
      WeakReferenceMessenger.Default.Register<ItemMessage>(this, HandleItemMessage);
      WeakReferenceMessenger.Default.Register<UnidentifiedBarcodeMessage>(this, HandleUnidentifiedBarcodeMessage);
      WeakReferenceMessenger.Default.Register<ItemVDAMessage>(this, HandleItemVDAMessage);
    }

    private void Disappearing()
    {
      _isActivePage = false;
      WeakReferenceMessenger.Default.UnregisterAll(this);
    }

    public void ApplyQueryAttributes(IDictionary<string, object> query)
    {
      BookMaterialToLotMessage message = query["message"] as BookMaterialToLotMessage;

      _productionEntry = message.Value.ProductionEntry;
      Title = string.Format(BookLotMaterialsFor0, _productionEntry.cnr);
      List<ProductionTargetMaterialEntry> materialEntries = message.Value.MaterialEntryList.entries;
      if (materialEntries != null)
      {
        foreach (var materialEntry in materialEntries)
        {
          SerialChargeNr scNr = materialEntry.itemEntry.hasChargenr ? SerialChargeNr.ChargeNr : (materialEntry.itemEntry.hasSerialnr ? SerialChargeNr.SerialNr : SerialChargeNr.None);
          BookingMaterials.Add(new BookingMaterial(
              materialEntry.id,
              materialEntry.itemEntry.cnr,
              materialEntry.itemEntry.name,
              materialEntry.itemEntry.description,
              materialEntry.amount,
              materialEntry.amountIssued,
              materialEntry.belatedWithdrawn,
              0,
              materialEntry.unitCnr.Trim(),
              scNr
              ));
        }
      }
    }

    private void HandleUnidentifiedBarcodeMessage(object recipient, UnidentifiedBarcodeMessage message)
    {
      HandleItemMessage(recipient, new ItemMessage(message.Value, null));
    }

    private async Task<BookingMaterial> GetAdditionalMaterial(string itemCnr)
    {
      await MainThread.InvokeOnMainThreadAsync(async () =>
      {
        bool result = await Application.Current.MainPage.DisplayAlert(Bookmaterial, string.Format(BookAdditionalItem0ToLot, itemCnr), Yes, No);
        if (result)
        {
          ItemV1Entry itemV1 = await _itemV1Service.GetItemV1EntryAsync(itemCnr, false);

          if (itemV1 != null)
          {
            BookingMaterial material = new BookingMaterial(null, itemV1.cnr, itemV1.name, itemV1.description, 0, 0, true, null, itemV1.unitCnr, itemV1.hasSerialnr ? SerialChargeNr.SerialNr : itemV1.hasChargenr ? SerialChargeNr.ChargeNr : SerialChargeNr.None);
            BookingMaterials.Add(material);
          }
        }
      });
      return BookingMaterials.FirstOrDefault(material => material.Cnr.Equals(itemCnr));
    }

    private async void HandleItemMessage(object recipient, ItemMessage message)
    {
      if (!_isActivePage)
        return;

      _isActivePage = false;

      await HandleItem(message.Value.ItemCnr, message.Value.Amount, message.Value.BatchNr);

      _isActivePage = true;
    }

    private async void HandleItemVDAMessage(object recipient, ItemVDAMessage message)
    {
      if (!_isActivePage)
        return;

      _isActivePage = false;

      VdaItem item = message.Value.ScanLabel.ToVdaItem();
      ItemV1Entry itemV1Entry = await _itemV1Service.GetItemV1EntryAsync(item.Cnr, false);
      if (itemV1Entry != null)
      {
        await HandleItem(item.Cnr, item.Amount, itemV1Entry.hasChargenr ? item.BatchNumberReelId() : item.BatchNumber1);
        _isActivePage = true;
      }
    }

    private async Task HandleItem(string itemCnr, decimal? amount, string batchNumber)
    {
      BookingMaterial material = BookingMaterials.FirstOrDefault(material => material.Cnr.Equals(itemCnr));
      if (material == null)
      {
        material = await GetAdditionalMaterial(itemCnr);
        if (material == null)
        {
          _messageService.Error(MaterialNotFound);
          _isActivePage = true;
          return;
        }
      }

      if (material.SerialChargeNr == SerialChargeNr.None)
      {
        ItemV1Entry itemEntry = await _itemV1Service.GetItemV1EntryAsync(material.Cnr, false);
        material.SerialChargeNr = itemEntry.hasChargenr ? SerialChargeNr.ChargeNr : itemEntry.hasSerialnr ? SerialChargeNr.SerialNr : SerialChargeNr.None;
      }
      ScrollToIndex?.Invoke(this, material);

      object res = null;
      if (material.SerialChargeNr == SerialChargeNr.None)
      {
        await MainThread.InvokeOnMainThreadAsync(async () => { res = await Application.Current.MainPage.ShowPopupAsync(new DecimalEntryPopup(_messageService, 3, 0, 99999999, string.Empty)); });
        PopupResult result = (PopupResult)res;

        if (!result.Canceled)
        {
          material.WithdrawalAmount = result.EntryAsDecimal();
        }
      }
      else
      {
        if (batchNumber != null && amount != null)
          AddIdentityAmount(material, batchNumber, amount);

        //Withdrawal amount must be 0 without any IdentityAmountEntries
        if (material.IdentityAmountEntries == null || material.IdentityAmountEntries.Count == 0)
          material.WithdrawalAmount = 0;
        await MainThread.InvokeOnMainThreadAsync(async () =>
        {
          res = await Application.Current.MainPage.ShowPopupAsync(new IdentityAmountEntryPopup
                      (_messageService, true, material.SerialChargeNr == SerialChargeNr.ChargeNr, itemCnr, batchNumber, material.IdentityAmountEntries));
        });

        List<IdentityAmountEntry> result = (List<IdentityAmountEntry>)res;
        if (result != null)
        {
          decimal sum = 0;
          foreach (IdentityAmountEntry identityAmount in result)
            sum += identityAmount.amount;
          material.IdentityAmountEntries = result;
          material.WithdrawalAmount = sum;
        }
      }
    }

    private void AddIdentityAmount(BookingMaterial material, string batchNumber, decimal? amount)
    {
      IdentityAmountEntry identityAmountEntry = new() { identity = (string)batchNumber, amount = (decimal)amount };
      if (material.IdentityAmountEntries != null)
        material.IdentityAmountEntries.Add(identityAmountEntry);
      else
        material.IdentityAmountEntries = [identityAmountEntry];
    }


    [RelayCommand]
    private void FrameTapped(BookingMaterial bookingMaterial)
    {
      HandleItemMessage(this, new ItemMessage(bookingMaterial.Cnr, null));
    }

    [RelayCommand]
    private async Task Cancel()
    {
      Disappearing();
      await Shell.Current.Navigation.PopModalAsync();
    }

    [RelayCommand]
    private async Task Ok()
    {
      //Book
      int failedAmount = 0;
      int goodAmount = 0;
      ObservableCollection<BookingMaterial> ErrorMaterials = new ObservableCollection<BookingMaterial>();
      for (int i = BookingMaterials.Count - 1; i >= 0; i--)
      {
        if (BookingMaterials[i].WithdrawalAmount != 0)
        {
          BookingResult result = await _productionService.BookMaterialWithdrawal(
              _productionEntry.cnr, _productionEntry.targetStockId, BookingMaterials[i].Id, BookingMaterials[i].Cnr, (decimal)BookingMaterials[i].WithdrawalAmount, BookingMaterials[i].IdentityAmountEntries);
          if (!result.Success)
          {
            failedAmount++;
            var newMaterial = new BookingMaterial(BookingMaterials[i], true, result.Message);
            ErrorMaterials.Add(newMaterial);
          }
          else
            goodAmount++;
        }
      }
      BookingMaterials.Clear();
      foreach (BookingMaterial material in ErrorMaterials)
        BookingMaterials.Add(material);

      if (failedAmount == 0)
      {
        if (goodAmount == 0)
          _messageService.Info(string.Format(NoBookingsMadeForLot0, _productionEntry.cnr));
        else
          _messageService.ShowToast(string.Format(BookedMaterialForLot, _productionEntry.cnr), CommunityToolkit.Maui.Core.ToastDuration.Short);

        Disappearing();
        await Shell.Current.Navigation.PopModalAsync();
      }
      else
      {
        _messageService.Error(string.Format(_0OutOf1BookingsFailed, failedAmount, failedAmount + goodAmount));
      }
    }
  }
}
