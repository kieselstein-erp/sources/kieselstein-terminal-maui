﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Machine;
using KieselsteinErp.Terminal.Services.Production;
using KieselsteinErp.Terminal.Services.StateMachine;
using KieselsteinErp.Terminal.Services.Worktime;
using Microsoft.Extensions.Logging;
using System.Collections.ObjectModel;
using System.Text;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.ViewModels
{
  public partial class BookMultipleWorkstepsPageViewModel : ObservableObject, IQueryAttributable
  {
    private readonly IProductionRestService _productionService;
    private readonly IMessageService _messageService;
    private readonly IWorktimeService _worktimeService;
    private readonly IMachineService _machineService;
    private readonly IStateMachine _stateMachine;
    private readonly ILogger<BookMultipleWorkstepsPageViewModel> _logger;
    private StaffEntry _staff;

    ObservableCollection<TimeDistributionWithMachine> _timeDistributions = new();
    public ObservableCollection<TimeDistributionWithMachine> TimeDistributions { get { return _timeDistributions; } }
    
    [ObservableProperty]
    private MachineEntry _machineToUse = null;
    [ObservableProperty]
    private bool _machineOverride = false;
    [ObservableProperty]
    private bool _isLoadingCircleVisible = false;

    public BookMultipleWorkstepsPageViewModel(IProductionRestService productionService, IMessageService messageService, IWorktimeService worktimeService, IMachineService machineService, IStateMachine stateMachine, ILogger<BookMultipleWorkstepsPageViewModel> logger)
    {
      _productionService = productionService;
      _messageService = messageService;
      _worktimeService = worktimeService;
      _machineService = machineService;
      _stateMachine = stateMachine;
      _logger = logger;

      WeakReferenceMessenger.Default.Register<BookMultipleWorkstepsMessage>(this, HandleMultipleWorkstepsMessage);
      WeakReferenceMessenger.Default.Register<WorkTimeEvent>(this, HandleWorkTimeEvent);
      WeakReferenceMessenger.Default.Register<MachineOverrideMessage>(this, HandleMachineOverrideMessage);
    }

    private async void HandleMachineOverrideMessage(object recipient, MachineOverrideMessage message)
    {
      MachineToUse = await _machineService.GetMachineEntryAsync(message.Value);
      MachineOverride = (MachineToUse != null);
    }

    public async void ApplyQueryAttributes(IDictionary<string, object> query)
    {
      try
      {
        IsLoadingCircleVisible = true;
        BookMultipleWorkstepsQuery parameters = (BookMultipleWorkstepsQuery)query[nameof(BookMultipleWorkstepsQuery)];
        _staff = parameters.Staff;
        if (parameters.PieceConfirmations != null)
        {

          foreach (PieceConfirmation pieceConfirmation in parameters.PieceConfirmations)
          {
            try
            {
              ProductionEntry productionEntry = await _productionService.GetProductionEntry(pieceConfirmation.ProductionCnr, true);
              ProductionWorkstepEntry productionWorkstepEntry = productionEntry.productionWorksteps.entries.Find(o => o.id == pieceConfirmation.Id);
              MachineEntry machineEntry = null;
              if (productionWorkstepEntry.machineId != 0)
                machineEntry = await _machineService.GetMachineEntryAsync(productionWorkstepEntry.machineId);
              TimeDistributionEntry timeDistributionEntry = new TimeDistributionEntry() { productionEntry = productionEntry, productionWorkstepEntry = productionWorkstepEntry };
              TimeDistributionWithMachine entry = new(timeDistributionEntry, machineEntry);
              TimeDistributions.Add(entry);
            }
            catch (Exception e)
            {
              _logger.LogError(e, "Error map piece confirmation to timedistribution ({message})", e.Message);
            }
          }
        }
        else
        {
          foreach (TimeDistributionWithMachine entry in parameters.TimeDistributions)
          {
            entry.ForeColor = Colors.Blue;
            TimeDistributions.Add(entry);
          }
        }
      }
      finally { IsLoadingCircleVisible = false; }
    }

    private async void HandleWorkTimeEvent(object recipient, WorkTimeEvent message)
    {
      await MainThread.InvokeOnMainThreadAsync(async () =>
      {
        await DoHandleWorkTimeEvent(message);
      });
    }

    private async Task DoHandleWorkTimeEvent(WorkTimeEvent message)
    {
      if (message.EventType == WorkTimeEventType.LotWorkstep)
      {
        BarcodeInfo barcodeInfo = await _worktimeService.GetBarcodeInfoAsync(message.ReaderString);
        if (barcodeInfo.productionEntry == null || barcodeInfo.productionWorkstepEntry == null)
        {
          _messageService.Warn(string.Format(UnknownLotOrWorkstepInBarcode0, message.ReaderString));
          return;
        }
        if (barcodeInfo.productionEntry.status != ProductionStatusConstants.PARTIALLYDONE && barcodeInfo.productionEntry.status != ProductionStatusConstants.INPRODUCTION)
        {
          _messageService.Warn(string.Format(TheStatus0OfLot1DoesNotAllowBooking, barcodeInfo.productionEntry.status, barcodeInfo.productionEntry.cnr));
          return;
        }
        if (barcodeInfo.productionWorkstepEntry.hasFinished)
        {
          _messageService.Warn(string.Format(TheWorkStep01OfLot2HasAlreadyBeenCompleted, barcodeInfo.WorkstepNumberFormatted, barcodeInfo.productionWorkstepEntry.itemEntry.name, barcodeInfo.productionEntry.cnr));
          return;
        }
        TimeDistributionWithMachine found = TimeDistributions.FirstOrDefault((o) => o.TimeDistributionEntry.productionEntry.id == barcodeInfo.productionEntry.id && o.TimeDistributionEntry.productionWorkstepEntry.id == barcodeInfo.productionWorkstepEntry.id);
        if (found != null)
          _messageService.Info(LotWorkstepAlreadyInList);
        else
        {
          TimeDistributionEntry timeDistributionEntry = new TimeDistributionEntry { productionEntry = barcodeInfo.productionEntry, productionWorkstepEntry = barcodeInfo.productionWorkstepEntry };

          MachineEntry machineEntry = null;
          if (MachineOverride)
            machineEntry = MachineToUse;
          else if (barcodeInfo.machineId != 0)
            machineEntry = await _machineService.GetMachineEntryAsync(barcodeInfo.machineId);
          else
            machineEntry = await _machineService.GetMachineEntryAsync(barcodeInfo.productionWorkstepEntry.machineId);

          TimeDistributionWithMachine entry = new TimeDistributionWithMachine(timeDistributionEntry, machineEntry);
          TimeDistributions.Add(entry);
          MachineOverride = false;
        }
      }
    }

    private async void HandleMultipleWorkstepsMessage(object recipient, BookMultipleWorkstepsMessage message)
    {
      if (message != null && !message.Value)
      {
        await MainThread.InvokeOnMainThreadAsync(async () =>
        {
          await Book();
          Disappearing();
          await Shell.Current.Navigation.PopModalAsync();
          await _stateMachine.OnCodeReceived(new UserAuthEvent(null));
        });
      }
    }

    private void Disappearing()
    {
      WeakReferenceMessenger.Default.UnregisterAll(this);
    }

    [RelayCommand]
    private async Task Cancel()
    {
      Disappearing();
      await Task.Run(() => _stateMachine.OnCodeReceived(new ProductionEvent(ProductionEventType.BookMultipleWorksteps)));
      await Shell.Current.Navigation.PopModalAsync();
      WeakReferenceMessenger.Default.Send(new RefreshWorkTimeMessage(true));
    }

    [RelayCommand]
    private async Task Ok()
    {
      await Book();
      Disappearing();
      await Task.Run(() => _stateMachine.OnCodeReceived(new ProductionEvent(ProductionEventType.BookMultipleWorksteps)));
      await Shell.Current.Navigation.PopModalAsync();
      await _stateMachine.OnCodeReceived(new UserAuthEvent(null));
    }

    private async Task Book()
    {
      List<string> errors = [];
      DateTime bookingTime = DateTime.Now;
      int booked = 0;
      foreach (TimeDistributionWithMachine entry in TimeDistributions)
      {
        if (entry.TimeDistributionEntry.id == 0)
        {
          // book entry
          TimeDistributionRecordingEntry recordingEntry = new TimeDistributionRecordingEntry
          {
            day = bookingTime.Day,
            month = bookingTime.Month,
            year = bookingTime.Year,
            hour = bookingTime.Hour,
            minute = bookingTime.Minute,
            second = bookingTime.Second,
            forStaffId = _staff.id,
            productionId = entry.TimeDistributionEntry.productionEntry.id,
            productionWorkplanId = entry.TimeDistributionEntry.productionWorkstepEntry.id,
            machineId = entry.MachineEntry?.id,
            workItemId = entry.TimeDistributionEntry.productionWorkstepEntry.itemEntry.id
          };
          BookingResult bookingResult = await _worktimeService.BookTimeDistributionAsync(recordingEntry);
          if (bookingResult.Success)
            booked++;
          else
            errors.Add(string.Format(BookingErrorLot0Workstep12, entry.TimeDistributionEntry.productionEntry.cnr, entry.TimeDistributionEntry.productionWorkstepEntry.workstepNumber, bookingResult.Message));
        }
      }
      if (errors.Count > 0)
      {
        StringBuilder sb = new();
        foreach (var error in errors)
        {
          sb.AppendLine(error);
        }
        _messageService.Error(sb.ToString());
      }
      else
      {
        if (booked > 0)
          _messageService.ShowToast(string.Format(_0TimeDistributionsBookedFor1, booked, _staff.name), CommunityToolkit.Maui.Core.ToastDuration.Long);
      }
    }

    [RelayCommand]
    private void Delete(object value)
    {
      if (value != null)
      {
        TimeDistributionWithMachine entry = value as TimeDistributionWithMachine;
        int row = TimeDistributions.IndexOf(entry);
        if (row == TimeDistributions.Count - 1)
          TimeDistributions.Remove(entry);
        else
        {
          // The alternative row style is incorrect if a row is removed, so remove all trailing ones and re-add them
          TimeDistributions.Remove(entry);
          List<TimeDistributionWithMachine> temp = new();
          for (int i = row; i < TimeDistributions.Count; i++)
          {
            temp.Add(TimeDistributions.ElementAt(i));
          }
          for (int i = row; TimeDistributions.Count > row;)
          {
            TimeDistributions.RemoveAt(row);
          }
          foreach (var t in temp)
          {
            TimeDistributions.Add(t);
          }
        }
      }
    }

    [RelayCommand]
    private void ClearMachineToUse()
    {
      MachineOverride = false;
    }

  }
}
