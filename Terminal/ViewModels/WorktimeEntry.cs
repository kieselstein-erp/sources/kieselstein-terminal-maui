﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.ViewModels
{
  public class WorktimeEntry
	{
		public WorktimeEntry() { }
		public WorktimeEntry(ZeitdatenEntry z) { ActivityCnr = z.activityCnr; Description = z.description; Time = z.time; }
		public string ActivityCnr { get; set; }
		public string Description { get; set; }
		public string Time { get; set; }
	}
}
