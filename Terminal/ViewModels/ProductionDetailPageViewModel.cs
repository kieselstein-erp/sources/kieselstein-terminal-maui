﻿using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Popups;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Device;
using KieselsteinErp.Terminal.Services.Item;
using KieselsteinErp.Terminal.Services.Machine;
using KieselsteinErp.Terminal.Services.Partslist;
using KieselsteinErp.Terminal.Services.Production;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.Staff;
using KieselsteinErp.Terminal.Services.StateMachine;
using KieselsteinErp.Terminal.Services.Stock;
using KieselsteinErp.Terminal.Services.Worktime;
using KieselsteinErp.Terminal.Util;
using KieselsteinErp.Terminal.Views;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Diagnostics;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.ViewModels
{
  public partial class ProductionWorkstep : ObservableObject
  {
    public int ProductionId { get; set; }
    public int MachineId { get; set; }
    public int WorkstepId { get; set; }
    public int WorkitemId { get; set; }
    public string ProductionOrderNumber { get; set; }
    public string Workstep { get; set; }
    public string Description { get; set; }
    public DateTime Started { get; set; }
    public string StartedFrom { get; set; }
    public string BOMNumber { get; set; }
    public string BOMName { get; set; }
    public string CustomerName { get; set; }
    public int TargetStockId { get; set; }

    [ObservableProperty]
    private bool _isTakeOverEnabled = false;
    [ObservableProperty]
    private Color _backColor = Colors.Transparent;
    public OpenWorkEntry WorkEntry { get; set; }
  }

  public partial class ButtonEnable : ObservableObject
  {
    [ObservableProperty]
    private bool _machineStopEnable = false;
    [ObservableProperty]
    private bool _deliverEnable = false;
    [ObservableProperty]
    private bool _productionOrderAmountEnable = false;
    [ObservableProperty]
    private bool _materialEnable = false;
    [ObservableProperty]
    private bool _documentsEnable = false;
    [ObservableProperty]
    private bool _takeOverEnable = false;
    [ObservableProperty]
    private bool _finishedEnable = false;
    [ObservableProperty]
    private bool _newWorkstepEnable = false;
    [ObservableProperty]
    private bool _autonomousEnable = false;
    [ObservableProperty]
    private bool _pieceConfirmationEnable = false;

    public void DisableAll()
    {
      MachineStopEnable = false;
      DeliverEnable = false;
      ProductionOrderAmountEnable = false;
      MaterialEnable = false;
      DocumentsEnable = false;
      TakeOverEnable = false;
      FinishedEnable = false;
      NewWorkstepEnable = false;
      AutonomousEnable = false;
      PieceConfirmationEnable = false;
    }
  }

  public partial class ProductionDetailPageViewModel : ObservableObject, IQueryAttributable
  {
    // update timer for actual Date-Time display
    private readonly IDispatcherTimer _timer;
    private readonly IDispatcherTimer _refreshTimer;
    private readonly ILogger<ProductionDetailPageViewModel> _logger;
    private readonly ISettingsService _settingsService;
    private readonly IProductionService _productionService;
    private readonly IProductionRestService _productionRestService;
    private readonly IPartslistService _partslistService;
    private readonly IMachineService _machineService;
    private readonly IStateMachine _stateMachine;
    private readonly IStaffService _staffService;
    private readonly IItemV1Service _itemV1Service;
    private readonly IMessageService _messageService;
    private readonly IWorktimeService _worktimeService;
    private readonly IDeviceRestService _deviceRestService;
    private readonly IStockService _stockService;
    [ObservableProperty]
    private string _labelDateTime = DateTime.Now.ToString();
    [ObservableProperty]
    private string _title;
    [ObservableProperty]
    private string _onlineString;

    [ObservableProperty]
    private StaffEntry _staff;
    [ObservableProperty]
    private string _staffName;
    [ObservableProperty]
    private Machine _machine;
    [ObservableProperty]
    private ProductionWorkstep _selectedProduction;
    [ObservableProperty]
    private ButtonEnable _buttonEnables = new();
    [ObservableProperty]
    private bool _isLoadingCircleVisible = false;
    [ObservableProperty]
    private bool _isSelectionListVisible = false;
    [ObservableProperty]
    private string _actualWork = null;
    [ObservableProperty]
    private bool _isMyWork = false;
    [ObservableProperty]
    private List<StorageLocation> _storageLocations = [];

    private ZeitdatenEntry _actualWorkEntry = null;

    private ObservableCollection<ProductionWorkstep> _productions = new();
    public ObservableCollection<ProductionWorkstep> Productions { get { return _productions; } }

    private ObservableCollection<OpenWorkEntry> _openWorkEntries = new();
    public ObservableCollection<OpenWorkEntry> OpenWorkEntries { get { return _openWorkEntries; } }

    private ObservableCollection<ItemButton> _bookingButtons = new();
    public ObservableCollection<ItemButton> BookingButtons { get { return _bookingButtons; } }

    private ObservableCollection<ItemButton> _lotBookingButtons = new();
    public ObservableCollection<ItemButton> LotBookingButtons { get { return _lotBookingButtons; } }

    public ProductionDetailPageViewModel(ILogger<ProductionDetailPageViewModel> logger, ISettingsService settingsService, IProductionService productionService, IProductionRestService productionRestService, IPartslistService partslistService,
      IMachineService machineService, IStateMachine stateMachine, IStaffService staffService, IItemV1Service itemV1Service, IMessageService messageService, IWorktimeService worktimeService,
      IDeviceRestService deviceRestService, IStockService stockService)
    {
      _logger = logger;
      _settingsService = settingsService;
      _productionService = productionService;
      _productionRestService = productionRestService;
      _partslistService = partslistService;
      _machineService = machineService;
      _stateMachine = stateMachine;
      _staffService = staffService;
      _itemV1Service = itemV1Service;
      _messageService = messageService;
      _worktimeService = worktimeService;
      _deviceRestService = deviceRestService;
      _stockService = stockService;

      _timer = Application.Current.Dispatcher.CreateTimer();
      _timer.Interval = TimeSpan.FromSeconds(1);
      _timer.Tick += (s, e) =>
      {
        LabelDateTime = DateTime.Now.ToString();
      };

      Init();

      _stateMachine.State().Subscribe(OnStateChange);

      if (settingsService.RefreshIntervallPaperlessManufacturing > 0)
      {
        _refreshTimer = Application.Current.Dispatcher.CreateTimer();
        _refreshTimer.Interval = TimeSpan.FromMinutes(settingsService.RefreshIntervallPaperlessManufacturing);
        _refreshTimer.Tick += RefreshTimer_Tick;
      }

      WeakReferenceMessenger.Default.Register<LoadingCircleMessage>(this, HandleLoadingCircleMessage);
    }

    private async void RefreshTimer_Tick(object sender, EventArgs e)
    {
      await RefreshList();
    }

    private async void Init()
    {
      await InitAdditionalBookingButtons();
      await InitStorageLocations();
    }

    private async Task InitAdditionalBookingButtons()
    {
      DeviceConfigEntry deviceConfigEntry = await _deviceRestService.GetDeviceConfigEntryAsync("WIN-Terminal", "PAPIERLOSE_FERTIGUNG", null);
      if (deviceConfigEntry != null)
      {
        try
        {
          SystemConfig systemConfig = JsonConvert.DeserializeObject<SystemConfig>(deviceConfigEntry.systemConfig);
          if (systemConfig != null)
          {
            List<ItemButton> list = systemConfig.AdditionalBookingButtons;
            if (list != null)
            {
              foreach (ItemButton itemButton in list)
              {
                if (string.IsNullOrEmpty(itemButton.ProductionOrderNumber))
                  BookingButtons.Add(itemButton);
                else
                {
                  itemButton.IsEnabled = true;
                  LotBookingButtons.Add(itemButton);
                }
              }
            }
          }
        }
        catch (Exception e)
        {
          _logger.LogWarning(e, "Error in deviceconfig for booking buttons");
        }
      }
    }

    private async Task InitStorageLocations()
    {
      DeviceConfigEntry deviceConfigEntry = await _deviceRestService.GetDeviceConfigEntryAsync("WIN-Terminal", IpHelper.GetIpAddress().ToString(), null);
      if (deviceConfigEntry != null)
      {
        try
        {
          SystemConfig systemConfig = JsonConvert.DeserializeObject<SystemConfig>(deviceConfigEntry.systemConfig);
          if (systemConfig != null)
            if (systemConfig.StorageLocations?.Count > 0)
              foreach (StorageLocation location in systemConfig.StorageLocations)
                StorageLocations.Add(location);
        }
        catch (Exception e)
        {
          _logger.LogWarning(e, "Error in deviceconfig for storage location");
        }
      }
    }

    private void HandleLoadingCircleMessage(object recipient, LoadingCircleMessage message)
    {
      IsLoadingCircleVisible = message.Value;
    }

    public void ApplyQueryAttributes(IDictionary<string, object> query)
    {
      Staff = (StaffEntry)query[nameof(StaffEntry)];
      SetStaffName();
      Machine = (Machine)query[nameof(Machine)];
      Title = PageUIHelper.MakeTitle(Machine);
      CreateList();
    }

    private void SetStaffName()
    {
      if (Staff == null)
        StaffName = Prompt_Login;
      else
        StaffName = $"{Staff.firstName} {Staff.name}".Trim();
    }

    private void OnStateChange(State state)
    {
      Staff = state.StaffEntry;
      SetStaffName();
    }

    private OpenWorkEntry _openWorkEntry = null;
    private ItemV1Entry _itemV1Entry = null;
    private MachineEntry _machineEntry = null;
    private async Task RefreshList()
    {
      try
      {
        IsLoadingCircleVisible = true;

        PlanningView planningView = await _machineService.GetPlanningViewAsync(Machine.ProductionGroupId, _settingsService.DaysForPlanning, _settingsService.PaperlessManufacturingRequestLimit);
        MachineEntry entry = planningView.machineList.entries.Find(o => o.id == Machine.Id);
        OpenWorkEntry openWorkEntry = null;
        if (entry != null && entry.productionWorkplanId != 0)
        {
          openWorkEntry = planningView.openWorkList.entries.Find(o => o.id == entry.productionWorkplanId);
          if (openWorkEntry == null)
            Productions.Clear();
          else if (openWorkEntry.id != _openWorkEntry.id || openWorkEntry.workItemCnr != _openWorkEntry.workItemCnr || openWorkEntry.workNumber != _openWorkEntry.workNumber
            || entry.starttime != _machineEntry.starttime || entry.personalIdStarter != _machineEntry.personalIdStarter)
          {
            _openWorkEntry = openWorkEntry;
            _machineEntry = entry;
            Productions.Clear();
            await AddProductions(DateTimeHelper.FromMillisecondsSinceUnixEpoch(entry.starttime), entry.personalIdStarter, openWorkEntry);
          }
        }
        else
        {
          _openWorkEntry = null;
          _machineEntry = null;
          Productions.Clear();
        }

        UpdateOpenWork(planningView, openWorkEntry);

        ActualWork = await GetActualWork(Staff);
        EnableButtons();
      }
      finally
      {
        IsLoadingCircleVisible = false;
      }
    }

    private async void CreateList()
    {
      try
      {
        IsLoadingCircleVisible = true;
        SelectedProduction = null;
        Productions.Clear();
        OpenWorkEntries.Clear();
        PlanningView planningView = await _machineService.GetPlanningViewAsync(Machine.ProductionGroupId, _settingsService.DaysForPlanning, _settingsService.PaperlessManufacturingRequestLimit);
        _machineEntry = planningView.machineList.entries.Find(o => o.id == Machine.Id);
        _openWorkEntry = null;
        if (_machineEntry != null && _machineEntry.productionWorkplanId != 0)
        {
          _openWorkEntry = planningView.openWorkList.entries.Find(o => o.id == _machineEntry.productionWorkplanId);
          if (_openWorkEntry != null)
          {
            await AddProductions(DateTimeHelper.FromMillisecondsSinceUnixEpoch(_machineEntry.starttime), _machineEntry.personalIdStarter, _openWorkEntry);
          }
        }

        UpdateOpenWork(planningView, _openWorkEntry);

        ActualWork = await GetActualWork(Staff);
        EnableButtons();
      }
      finally
      {
        IsLoadingCircleVisible = false;
      }
    }

    private async Task<string> GetActualWork(StaffEntry staff)
    {
      if (staff != null)
      {
        List<ZeitdatenEntry> list = await _worktimeService.GetWorktimeEntriesAsync(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, staff.personalNr, 1);
        list.Reverse();
        if (list != null && list.Count > 0)
        {
          _actualWorkEntry = list[0];
          EnableTakeOver(_actualWorkEntry);
          return $"{string.Format("{0:d}", _actualWorkEntry.date)} {_actualWorkEntry.time} {_actualWorkEntry.activityCnr} {_actualWorkEntry.description}";
        }
        else
        {
          EnableTakeOver(null);
        }
      }
      IsMyWork = false;
      return null;
    }

    private async Task AddProductions(DateTime started, int personalId, OpenWorkEntry openWorkEntry)
    {
      string staffName = _staffService.GetFormatedStaffName(personalId);
      ProductionEntry productionEntry = await _productionRestService.GetProductionEntry(openWorkEntry.productionCnr);
      if (_itemV1Entry == null || _itemV1Entry.cnr != openWorkEntry.workItemCnr)
        _itemV1Entry = await _itemV1Service.GetItemV1EntryAsync(openWorkEntry.workItemCnr, false);

      Productions.Add(new ProductionWorkstep
      {
        ProductionId = productionEntry.id,
        TargetStockId = productionEntry.targetStockId,
        ProductionOrderNumber = openWorkEntry.productionCnr,
        Workstep = openWorkEntry.workNumber.ToString(),
        WorkitemId = _itemV1Entry.id,
        Description = openWorkEntry.workItemDescription,
        Started = started,
        StartedFrom = staffName,
        BOMNumber = openWorkEntry.partlistCnr,
        BOMName = openWorkEntry.partlistDescription,
        CustomerName = openWorkEntry.customerShortDescription,
        IsTakeOverEnabled = !(staffName == StaffName),
        MachineId = openWorkEntry.machineId,
        WorkstepId = openWorkEntry.id,
        WorkEntry = openWorkEntry
      });
    }

    private void UpdateOpenWork(PlanningView planningView, OpenWorkEntry openWorkEntry)
    {
      List<OpenWorkEntry> list = [];
      foreach (OpenWorkEntry workEntry in planningView.openWorkList.entries)
      {
        if (!(openWorkEntry != null && workEntry == openWorkEntry))
          if (workEntry.machineId == Machine.Id)
            list.Add(workEntry);
      }
      list = list.OrderBy(x => x.workItemStartDate).ToList();
      OpenWorkEntries.Clear();
      foreach (OpenWorkEntry workEntry in list)
      {
        OpenWorkEntries.Add(workEntry);
      }
    }

    [RelayCommand]
    private async Task Refresh()
    {
      await RefreshList();
    }

    [RelayCommand]
    private void Appearing()
    {
      WeakReferenceMessenger.Default.Register<BookMaterialToLotMessage>(this, HandleBookMaterialToLotMessage);
      WeakReferenceMessenger.Default.Register<RefreshMessage>(this, HandleRefresh);
      _timer.Start();
      _refreshTimer?.Start();
      IsSelectionListVisible = false;
    }

    private async void HandleRefresh(object recipient, RefreshMessage message)
    {
      if (message.Value)
        await MainThread.InvokeOnMainThreadAsync(async () => { await RefreshList(); });
    }

    private async void HandleBookMaterialToLotMessage(object recipient, BookMaterialToLotMessage message)
    {
      IsLoadingCircleVisible = true;
      await MainThread.InvokeOnMainThreadAsync(async () =>
      {
        await Shell.Current.GoToAsync(nameof(BookMaterialPage), new Dictionary<string, object> { { "message", message } });
        IsLoadingCircleVisible = false;
      });
    }

    [RelayCommand]
    private void Disappearing()
    {
      WeakReferenceMessenger.Default.Unregister<BookMaterialToLotMessage>(this);
      WeakReferenceMessenger.Default.Unregister<RefreshMessage>(this);
      try
      {
        _timer?.Stop();
      }
      catch (Exception ex)
      {
        _logger.LogWarning(ex, "Timer stop");
      }
      if (_refreshTimer != null)

        try
        {
          _refreshTimer?.Stop();
        }
        catch (Exception ex)
        {
          _logger.LogWarning(ex, "RefreshTimer stop");
        }

      SelectedProduction = null;
      //EnableButtons();
    }

    [RelayCommand]
    private async Task GoBack()
    {
      await MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync("//" + nameof(ProductionOverviewPage)); });
    }

    [RelayCommand]
    private async Task Logout()
    {
      await _stateMachine.OnCodeReceived(new UserAuthEvent(null));
      //var existingPages = Shell.Current.Navigation.NavigationStack.ToList();

      await MainThread.InvokeOnMainThreadAsync(async () => { await Shell.Current.GoToAsync("//" + nameof(MainPage)); });
    }

    [RelayCommand]
    private void ItemSelected()
    {
      EnableButtons();
      if (DeviceInfo.Platform == DevicePlatform.Android)
        SelectedProduction.BackColor = Colors.Wheat;
    }

    [RelayCommand]
    private async Task MachineStop()
    {
      if (SelectedProduction == null) return;

      BookingResult bookingResult = await _worktimeService.StartStopMachineAsync(true, Machine.Id, SelectedProduction.WorkstepId, Staff.id, DateTime.Now);
      if (bookingResult.Success)
      {
        _messageService.ShowToast(string.Format(Machine0Stopped, Machine.MachineName), CommunityToolkit.Maui.Core.ToastDuration.Short);
        CreateList();
      }
      else
        _messageService.Error(bookingResult.Message);
    }

    [RelayCommand]
    private async Task Deliver()
    {
      if (SelectedProduction == null) return;

      string productionOrderNumber = SelectedProduction.ProductionOrderNumber;
      ProductionEntry entry = await _productionRestService.GetProductionEntry(productionOrderNumber);
      string preferredType = null;
      // for field "preferedType" api call item/list must be used! otherwise it is always null
      if ((entry != null) && (entry.itemEntry != null))
      {
        var items = await _itemV1Service.GetItemsAsync(entry.itemEntry.cnr, null);
        if (items != null && items.entries.Count > 0)
          preferredType = ((ItemV1Entry)items.entries[0]).preferredType;
      }
      await _productionService.DoDeliver(new DeliverMessage(productionOrderNumber, preferredType, entry?.itemEntry, true));
    }

    [RelayCommand]
    private async Task ProductionOrderAmount()
    {
      if (SelectedProduction == null) return;

      try
      {
        IsLoadingCircleVisible = true;
        string productionOrderNumber = SelectedProduction.ProductionOrderNumber;
        decimal? amount = await _productionRestService.LotAmount(productionOrderNumber);
        if (amount == null)
          _messageService.Error(string.Format(Error_Lot_Not_Found, productionOrderNumber));
        else
          await _productionService.DoChangeAmount(new ChangeAmountMessage((decimal)amount, productionOrderNumber), true);
      }
      finally
      {
        IsLoadingCircleVisible = false;
      }
    }

    [RelayCommand]
    private async Task Material()
    {
      if (SelectedProduction == null) return;

      string productionOrderNumber = SelectedProduction.ProductionOrderNumber;
      IsLoadingCircleVisible = true;
      await _stateMachine.OnCodeReceived(new ProductionEvent(ProductionEventType.BookMaterialToLot, true, productionOrderNumber));
    }

    [RelayCommand]
    private async Task Documents()
    {
      if (SelectedProduction == null) return;
      IsLoadingCircleVisible = true;
      try
      {
        Routing.RegisterRoute($"{nameof(ProductionDetailPage)}/{nameof(ProductionDocumentsPage)}", typeof(ProductionDocumentsPage));
        await Shell.Current.GoToAsync(nameof(ProductionDocumentsPage), new Dictionary<string, object> { { nameof(ProductionWorkstep), SelectedProduction } });
      }
      catch (Exception ex)
      {
        Debug.Print(ex.Message);
      }
      finally
      { IsLoadingCircleVisible = false; }
    }

    [RelayCommand]
    private async Task TakeOver(object production)
    {
      ProductionWorkstep p = (ProductionWorkstep)production;
      ProductionEntry productionEntry = await _productionRestService.GetProductionEntry(p.ProductionOrderNumber);
      if (productionEntry == null) return;

      ProductionRecordingEntry entry = new()
      {
        productionId = productionEntry.id,
        machineId = Machine.Id,
        productionWorkplanId = p.WorkstepId,
        workItemId = p.WorkitemId,
        forStaffId = Staff.id,
        year = DateTime.Now.Year,
        month = DateTime.Now.Month,
        day = DateTime.Now.Day,
        hour = DateTime.Now.Hour,
        minute = DateTime.Now.Minute,
        second = DateTime.Now.Second,
        where = DeviceInfo.Current.Name
      };
      BookingResult bookingResult = await _worktimeService.BookProductionRecordingAsync(entry);
      if (bookingResult.Success)
      {
        _messageService.ShowToast(string.Format(Workstep0ForProductionOrder1Booked, p.Workstep, p.ProductionOrderNumber), CommunityToolkit.Maui.Core.ToastDuration.Short);
        await RefreshList();
      }
      else
        _messageService.Error(bookingResult.Message);
      EnableTakeOver(_actualWorkEntry);
      EnableButtons();
    }

    [RelayCommand]
    private async Task Finished()
    {
      if (SelectedProduction == null) return;

      OpenWorkUpdateEntry openWorkUpdateEntry = new()
      {
        productionCnr = SelectedProduction.ProductionOrderNumber,
        id = SelectedProduction.WorkstepId
      };

      if (StorageLocations != null && StorageLocations.Count > 0)
      {
        PopupResult res = (PopupResult)await Application.Current.MainPage.ShowPopupAsync(new ButtonsSelectionPopup(StorageLocations));
        if (res != null && !res.Canceled)
        {
          StockPlaceEntry stockPlace = await _stockService.GetStockPlaceEntryByCnr(SelectedProduction.TargetStockId, res.Entry);
          if (stockPlace == null)
          {
            _messageService.Warn(string.Format(Stockplace0DoesNotExist, res.Entry));
            return;
          }
          else
            openWorkUpdateEntry.storageLocationId = stockPlace.id;
        }
      }

      BookingResult bookingResult = await _productionRestService.WorkstepDoneAsync(openWorkUpdateEntry);
      if (bookingResult.Success)
      {
        _messageService.ShowToast(string.Format(ProductionOrder0Workstep1Done, SelectedProduction.ProductionOrderNumber, SelectedProduction.Workstep), CommunityToolkit.Maui.Core.ToastDuration.Short);
        CreateList();
      }
      else
        _messageService.Error(bookingResult.Message);
    }

    [RelayCommand]
    private void NewWorkstep()
    {
      if (OpenWorkEntries == null || OpenWorkEntries.Count == 0)
      {
        _messageService.Info(string.Format(ThereAreNoOpenWorkstepsForMachine0, Machine.MachineName));
        return;
      }
      IsSelectionListVisible = true;
    }

    [RelayCommand]
    private void CloseSelection()
    {
      IsSelectionListVisible = false;
    }

    [RelayCommand]
    private async Task OpenworkTapped(OpenWorkEntry openWorkEntry)
    {
      try
      {
        ProductionEntry productionEntry = await _productionRestService.GetProductionEntry(openWorkEntry.productionCnr);
        if (productionEntry == null) return;
        ItemV1Entry itemV1Entry = await _itemV1Service.GetItemV1EntryAsync(openWorkEntry.workItemCnr, false);
        ProductionRecordingEntry entry = _productionRestService.BuildProductionrecordingEntry(productionEntry.id, Machine.Id, openWorkEntry.id, itemV1Entry.id, Staff.id);
        BookingResult bookingResult = await _worktimeService.BookProductionRecordingAsync(entry);
        if (bookingResult.Success)
        {
          _messageService.ShowToast(string.Format(Workstep0ForProductionOrder1Booked, openWorkEntry.workNumber, openWorkEntry.productionCnr), CommunityToolkit.Maui.Core.ToastDuration.Short);
          CreateList();
        }
        else
          _messageService.Error(bookingResult.Message);
      }
      finally
      {
        IsSelectionListVisible = false;
      }
    }

    [RelayCommand]
    private async Task Autonomous()
    {
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.Autonomous, true)));
    }

    [RelayCommand]
    private async Task PieceConfirmation()
    {
      IsLoadingCircleVisible = true;
      ProductionWorkstep workstep = SelectedProduction;
      ProductionEntry productionEntry = await _productionRestService.GetProductionEntry(workstep.ProductionOrderNumber);
      ProductionRecordingEntry worktimeToBook = _productionRestService.BuildProductionrecordingEntry(productionEntry.id, Machine.Id, workstep.WorkstepId, workstep.WorkitemId, Staff.id);
      EnableButtons(true);
      await Task.Run(() => _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.PieceConfirmation, true, workstep, worktimeToBook)));
      EnableButtons();
    }


    [RelayCommand]
    private async Task BookButton(ItemButton itemButton)
    {
      if (itemButton != null)
      {
        string productionOrderNumber;
        if (!string.IsNullOrEmpty(itemButton.ProductionOrderNumber))
          productionOrderNumber = itemButton.ProductionOrderNumber;
        else
          productionOrderNumber = SelectedProduction.ProductionOrderNumber;

        await _stateMachine.OnCodeReceived(new WorkTimeEvent(WorkTimeEventType.LotActivity, true, $"$W{productionOrderNumber}  {itemButton.ItemCnr}"));
      }
    }

    private void EnableTakeOver(ZeitdatenEntry entry)
    {
      foreach (ProductionWorkstep workstep in Productions)
      {
        workstep.IsTakeOverEnabled = CompareWork(entry, workstep);
        workstep.BackColor = workstep.IsTakeOverEnabled ? Colors.LightSalmon : Colors.LightGreen;
      }
      if (SelectedProduction != null)
      {
        SelectedProduction.IsTakeOverEnabled = CompareWork(entry, SelectedProduction);
        SelectedProduction.BackColor = SelectedProduction.IsTakeOverEnabled ? Colors.LightSalmon : Colors.LightGreen;
      }
    }

    private bool CompareWork(ZeitdatenEntry entry, ProductionWorkstep selectedProduction)
    {
      if (entry?.description == null) return true;
      if (selectedProduction.StartedFrom != StaffName) return true;
      if (entry.description.Contains(selectedProduction.ProductionOrderNumber) && entry.description.Contains(":" + selectedProduction.Workstep) &&
        selectedProduction.WorkEntry.partlistItemDescription == null ? entry.description.Contains("null") : entry.description.Contains(selectedProduction.WorkEntry.partlistItemDescription)) return false;
      return true;
    }

    private void EnableButtons(bool disableOverride = false)
    {
      bool bLoggedOn = StaffName != Prompt_Login;
      ButtonEnables.DisableAll();
      if (SelectedProduction != null && !disableOverride)
      {
        ButtonEnables.MachineStopEnable = SelectedProduction.Started != DateTime.MinValue;
        ButtonEnables.DeliverEnable = !string.IsNullOrEmpty(SelectedProduction.ProductionOrderNumber);
        ButtonEnables.ProductionOrderAmountEnable = !string.IsNullOrEmpty(SelectedProduction.ProductionOrderNumber);
        ButtonEnables.MaterialEnable = !string.IsNullOrEmpty(SelectedProduction.ProductionOrderNumber);
        ButtonEnables.DocumentsEnable = !string.IsNullOrEmpty(SelectedProduction.ProductionOrderNumber);
        ButtonEnables.TakeOverEnable = StaffName != SelectedProduction.StartedFrom;
        ButtonEnables.FinishedEnable = !string.IsNullOrEmpty(SelectedProduction.Workstep);
        ButtonEnables.AutonomousEnable = !string.IsNullOrEmpty(SelectedProduction.Workstep) && !CompareWork(_actualWorkEntry, SelectedProduction) && !SelectedProduction.IsTakeOverEnabled;
        ButtonEnables.PieceConfirmationEnable = !string.IsNullOrEmpty(SelectedProduction.Workstep);
      }
      ButtonEnables.NewWorkstepEnable = bLoggedOn && !disableOverride;

      foreach (ItemButton b in BookingButtons)
      {
        b.IsEnabled = SelectedProduction != null;
      }
    }


  }
}
