﻿namespace KieselsteinErp.Terminal.Converter
{
  public class EnumConverter
	{
		public static T GetAs<T>(string value)
		{
			object result;
			if (!Enum.TryParse(typeof(T), value, out result))
				throw new Exception("Invalid String to Enum conversion");
			return (T)result;
		}
		public static T GetAs<T, U>(U value)
		{
			object result;
			if (!Enum.TryParse(typeof(T), value.ToString(), out result))
				throw new Exception("Invalid Enum to Enum conversion");
			return (T)result;
		}
	}
}
