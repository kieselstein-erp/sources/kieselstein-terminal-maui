﻿using KieselsteinErp.Terminal.Util;
using System.Globalization;

namespace KieselsteinErp.Terminal.Converter
{
  public class LongToDateTimeConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is long ticks)
      {
        return DateTimeHelper.FromMillisecondsSinceUnixEpoch(ticks);
      }
      // Fallback value, could also be something else
      return DateTime.MinValue;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return Binding.DoNothing;
    }
  }
}
