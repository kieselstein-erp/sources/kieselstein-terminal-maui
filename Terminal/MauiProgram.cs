using Camera.MAUI;
using CommunityToolkit.Maui;
using CommunityToolkit.Maui.Core;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Resources.Localization;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Audio;
using KieselsteinErp.Terminal.Services.Database;
using KieselsteinErp.Terminal.Services.Delivery;
using KieselsteinErp.Terminal.Services.Device;
using KieselsteinErp.Terminal.Services.Inventory;
using KieselsteinErp.Terminal.Services.Item;
using KieselsteinErp.Terminal.Services.Machine;
using KieselsteinErp.Terminal.Services.OfflineBooking;
using KieselsteinErp.Terminal.Services.Order;
using KieselsteinErp.Terminal.Services.Partslist;
using KieselsteinErp.Terminal.Services.Production;
using KieselsteinErp.Terminal.Services.PurchaseOrder;
using KieselsteinErp.Terminal.Services.Readers;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.Staff;
using KieselsteinErp.Terminal.Services.StateMachine;
using KieselsteinErp.Terminal.Services.Stock;
using KieselsteinErp.Terminal.Services.UiState;
using KieselsteinErp.Terminal.Services.User;
using KieselsteinErp.Terminal.Services.Worktime;
using KieselsteinErp.Terminal.ViewModels;
using KieselsteinErp.Terminal.ViewModels.App;
using KieselsteinErp.Terminal.Views;
using KieselsteinErp.Terminal.Views.App;
using LocalizationResourceManager.Maui;
using MetroLog.Maui;
using MetroLog.MicrosoftExtensions;
using MetroLog.Operators;
using Microsoft.Extensions.Logging;
using Plugin.Maui.Audio;
using System.Diagnostics;
using System.Globalization;
using UraniumUI;

#if WINDOWS
using Microsoft.Maui.LifecycleEvents;
using Microsoft.UI;
using Microsoft.UI.Windowing;
using Windows.Graphics;
#endif

namespace KieselsteinErp.Terminal;

public static class MauiProgram
{
  public static CultureInfo DefaultCulture { get; set; }
  public static CultureInfo CurrentCulture { get; set; }
  public static MauiApp CreateMauiApp()
  {
    var builder = MauiApp.CreateBuilder();

    builder.UseMauiApp<App>();
    builder.UseMauiCommunityToolkitCore();
    builder
        .UseMauiApp<App>()
        .UseMauiCameraView()
        .UseMauiCommunityToolkit()
        .ConfigureFonts(fonts =>
        {
          fonts.AddFont("opensans-regular.ttf", "OpenSansRegular");
          fonts.AddFont("opensans-semibold.ttf", "OpenSansSemibold");
          fonts.AddMaterialIconFonts();
        })
    .UseLocalizationResourceManager(settings =>
    {
      //settings.InitialCulture(CultureInfo.GetCultureInfo("de"));
      settings.AddResource(AppResources.ResourceManager);
      settings.RestoreLatestCulture(true);
      try
      {
        DefaultCulture = (settings as LocalizationResourceManager.Maui.LocalizationResourceManager).DefaultCulture;
        CurrentCulture = (settings as LocalizationResourceManager.Maui.LocalizationResourceManager).CurrentCulture;
      }
      catch (Exception ex)
      {
        Debug.WriteLine(ex);
      }
    });

#if WINDOWS
var settings =  new SettingsPreferencesProvider();
  if (settings.IsFullScreen)
  {
    builder.ConfigureLifecycleEvents(events =>
    {
      events.AddWindows(wndLifeCycleBuilder =>
      {
        wndLifeCycleBuilder.OnWindowCreated(window =>
        {
          window.ExtendsContentIntoTitleBar= false;
          IntPtr windowHandle = WinRT.Interop.WindowNative.GetWindowHandle(window);
          Microsoft.UI.WindowId windowId = Microsoft.UI.Win32Interop.GetWindowIdFromWindow(windowHandle);
          var appWindow = Microsoft.UI.Windowing.AppWindow.GetFromWindowId(windowId);
          appWindow.SetPresenter(Microsoft.UI.Windowing.AppWindowPresenterKind.FullScreen);
          appWindow.Closing += (s, e) => { FreeDevices(); };
        });
      });
    });
  } 
  else 
  {
    builder.ConfigureLifecycleEvents(events =>
    {
      events.AddWindows(wndLifeCycleBuilder =>
      {
        wndLifeCycleBuilder.OnWindowCreated(window =>
        {
          window.ExtendsContentIntoTitleBar= false;
          IntPtr windowHandle = WinRT.Interop.WindowNative.GetWindowHandle(window);
          Microsoft.UI.WindowId windowId = Microsoft.UI.Win32Interop.GetWindowIdFromWindow(windowHandle);
          var appWindow = Microsoft.UI.Windowing.AppWindow.GetFromWindowId(windowId);
          appWindow.SetPresenter(Microsoft.UI.Windowing.AppWindowPresenterKind.Overlapped);
          if (!settings.IsSizable)
            appWindow.MoveAndResize(new Windows.Graphics.RectInt32(0,0,(int)settings.WindowSize.Width,(int)settings.WindowSize.Height));

          switch (appWindow.Presenter)
          {
            case OverlappedPresenter overlappedPresenter:
              if (settings.IsSizable)
              {
                overlappedPresenter.SetBorderAndTitleBar(true, false);
                overlappedPresenter.IsResizable = true;
                overlappedPresenter.IsMinimizable = true;
              } 
              else
              {
                overlappedPresenter.SetBorderAndTitleBar(false, false);
                overlappedPresenter.IsResizable = false;
              }
              //overlappedPresenter.Maximize();
              break;
          }
          appWindow.Destroying += (s, e) => { FreeDevices(); };
        });
      });
    });
  }

   void FreeDevices()
    {
      var deviceManager = Application.Current.MainPage.Handler.MauiContext.Services.GetService<IDeviceManager>();
      if (deviceManager != null)
        deviceManager.CloseAll();
    }
#endif

    builder.Logging
#if DEBUG
            .AddTraceLogger(
                options =>
                {
                  options.MinLevel = LogLevel.Trace;
                  options.MaxLevel = LogLevel.Critical;
                }) // Will write to the Debug Output
#endif
            .AddInMemoryLogger(
                options =>
                {
                  options.MaxLines = 1024;
                  options.MinLevel = LogLevel.Debug;
                  options.MaxLevel = LogLevel.Critical;
                })
            .AddStreamingFileLogger(
                options =>
                {
                  options.RetainDays = 7;
                  options.FolderPath = Path.Combine(
                      FileSystem.CacheDirectory,
                      "KieselsteinTerminalLogs");
                })
            .AddConsoleLogger(
                options =>
                {
                  options.MinLevel = LogLevel.Information;
                  options.MaxLevel = LogLevel.Critical;
                });
    builder.Services.AddSingleton(LogOperatorRetriever.Instance);
    var logController = new LogController();
    logController.IsShakeEnabled = true;
    //builder.Services.AddSingleton<BluetoothLEService>();

    //builder.
    if ((DeviceInfo.Platform == DevicePlatform.Android) && (DeviceInfo.Idiom == DeviceIdiom.Phone))
    {
      // App pages
      builder.Services
        .AddSingleton<LogonPage>()
        .AddSingleton<LogonPageViewModel>()
        .AddSingleton<AppMenuPage>()
        .AddSingleton<AppMenuPageViewModel>()
        .AddSingleton<InventorySelectionPage>()
        .AddSingleton<InventorySelectionPageViewModel>()
        .AddSingleton<InventoryPage>()
        .AddSingleton<InventoryPageViewModel>()
        .AddSingleton<ProductionPage>()
        .AddSingleton<ProductionPageViewModel>()
        .AddSingleton<TimeTrackingPage>()
        .AddSingleton<TimeTrackingPageViewModel>()
        .AddSingleton<ItemsPage>()
        .AddSingleton<ItemsPageViewModel>()
        .AddSingleton<PurchaseOrderPage>()
        .AddSingleton<PurchaseOrderPageViewModel>()
        .AddSingleton<DeliveryPage>()
        .AddSingleton<DeliveryPageViewModel>()
        .AddSingleton<OrderDeliveryPage>()
        .AddSingleton<OrderDeliveryPageViewModel>()
        .AddSingleton<CollectiveOrderDeliveryPage>()
        .AddSingleton<CollectiveOrderDeliveryPageViewModel>();

    }
    else
    {
      builder.Services
        .AddSingleton<MainPage>()
        .AddSingleton<MainPageViewModel>()

        .AddSingleton<StaffPage>()
        .AddSingleton<StaffPageViewModel>()

        .AddSingleton<ProductionOverviewPage>()
        .AddSingleton<ProductionOverviewPageViewModel>()

        .AddSingleton<ProductionDetailPage>()
        .AddSingleton<ProductionDetailPageViewModel>()

        .AddSingleton<ProductionDocumentsPage>()
        .AddSingleton<ProductionDocumentsPageViewModel>()

        .AddTransient<BookMaterialPage>()
        .AddTransient<BookMaterialPageViewModel>()

        .AddTransient<BookMultipleWorkstepsPage>()
        .AddTransient<BookMultipleWorkstepsPageViewModel>()

        .AddTransient<MachineListPage>()
        .AddTransient<MachineListPageViewModel>();

    }

    builder.Services
        .AddSingleton<DialogMessageProcessor>()
        .AddSingleton<ITranslator, Translator>()
        .AddSingleton<ISettingsService, SettingsPreferencesProvider>()
        .AddSingleton<IUserService, UserService>()
        .AddSingleton<IUserRestService, UserRestService>()
        .AddSingleton<IStaffService, StaffService>()
        .AddSingleton<IStaffRestService, StaffRestService>()
        .AddSingleton<IWorktimeService, WorktimeRestService>()
        .AddSingleton<IProductionRestService, ProductionRestService>()
        .AddSingleton<IMachineRestService, MachineRestService>()
        .AddSingleton<IMachineService, MachineService>()
        .AddSingleton<IOfflineBookingService, OfflineBookingService>()
        .AddSingleton<IOrderService, OrderRestService>()
        .AddSingleton<IItemV1Service, ItemV1RestService>()
        .AddSingleton<IStockService, StockService>()
        .AddSingleton<IStockRestService, StockRestService>()
        .AddSingleton<IStateMachine, StateMachine>()
        .AddSingleton<IInventoryService, InventoryRestService>()
        .AddSingleton<IDatabaseService, DatabaseService>()
        .AddSingleton<IPurchaseOrderRestService, PurchaseOrderRestService>()
        .AddSingleton<IDeliveryService, DeliveryRestService>()
        .AddSingleton<IPartslistService, PartslistRestService>()
        .AddSingleton<IDeviceRestService, DeviceRestService>()

    /*
     * This one we have to register twice (once as the class itself, and once as the interface), as we would
     * like to look it up through both.
     * Registering the interface has to be done through the factory parameter, otherwise we would not get the same instance.
     * If this procedure is not clear, have a look at: https://andrewlock.net/how-to-register-a-service-with-multiple-interfaces-for-in-asp-net-core-di/
     */
    .AddSingleton<LatchBackedUiReadyService>()
    .AddSingleton<IUiReadyService>(x => x.GetRequiredService<LatchBackedUiReadyService>())

    /* the same here */
    .AddSingleton<QueuedMessageService>()
    .AddSingleton<IMessageService>(x => x.GetRequiredService<QueuedMessageService>())

    .AddSingleton<IReader, CameraReader>()
    .AddSingleton<IDecoder, Decoder>()

    .AddSingleton<IAudioManager>(AudioManager.Current)
    .AddSingleton<IAudioService, AudioService>()
    .AddSingleton<IProductionService, ProductionService>()

    .AddTransient<SettingsPage>()
    .AddTransient<SettingsPageViewModel>()

    .AddTransient<PieceConfirmationPage>()
    .AddTransient<PieceConfirmationPageViewModel>();

    if ((DeviceInfo.Platform == DevicePlatform.Android) || (DeviceInfo.Platform == DevicePlatform.iOS))
    {
      builder.Services.AddSingleton<IReader, NfcReader>();
      if ((DeviceInfo.Manufacturer == "Datalogic") && (DeviceInfo.Model == "Memor 1"))
        builder.Services.AddSingleton<IReader, DatalogicReader>();
      if ((DeviceInfo.Manufacturer == "Zebra Technologies") && ((DeviceInfo.Model == "TC21") || (DeviceInfo.Model == "TC22") 
        || (DeviceInfo.Model == "TC26") || (DeviceInfo.Model == "TC27")))
        builder.Services.AddSingleton<IReader, ZebraReader>();
      if (DeviceInfo.Platform == DevicePlatform.Android)
        builder.Services.AddSingleton<IReader, BluetoothBarcodeReader>();
    }
    else if (DeviceInfo.Platform == DevicePlatform.WinUI)
    {
      builder.Services.AddSingleton<IReader, SerialBarcodeReader>().AddSingleton<IReader, SerialNfcReader>();
      builder.Services.AddSingleton<IReader, BluetoothBarcodeReader>();
    }
    builder.Services.AddSingleton<IDeviceManager, DeviceManager>();

    var mauiApp = builder.Build();

    // looked up to get instantiated.
    mauiApp.Services.GetService<DialogMessageProcessor>();

    return mauiApp;
  }
}
