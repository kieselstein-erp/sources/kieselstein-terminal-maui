﻿using System.Collections;

namespace KieselsteinErp.Terminal.Resources.Templates
{
	public class AlternatingItemTemplateSelector : DataTemplateSelector
	{
		public DataTemplate EvenTemplate { get; set; }
		public DataTemplate OddTemplate { get; set; }

		protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
		{
			var listView = container as ListView;
			if (listView != null)
			{
				var index = GetItemIndex(item, listView.ItemsSource);
				return index % 2 == 0 ? EvenTemplate : OddTemplate;
			}

			return null;
		}

		private int GetItemIndex(object item, IEnumerable itemsSource)
		{
			if (item == null || itemsSource == null)
				return -1;

			int index = 0;
			foreach (var currentItem in itemsSource)
			{
				if (ReferenceEquals(currentItem, item))
					return index;

				index++;
			}

			return -1;
		}
	}
}
