﻿using KieselsteinErp.Terminal.Views;
using MetroLog.Maui;

namespace KieselsteinErp.Terminal;

public partial class AppShell : Shell
{
  public AppShell()
  {
    InitializeComponent();

    Routing.RegisterRoute("MainPage/MetroLogPage", typeof(MetroLogPage));

    Routing.RegisterRoute(nameof(BookMaterialPage), typeof(BookMaterialPage));

    Routing.RegisterRoute(nameof(PieceConfirmationPage), typeof(PieceConfirmationPage));

    Routing.RegisterRoute(nameof(ProductionOverviewPage), typeof(ProductionOverviewPage));

    Routing.RegisterRoute(nameof(ProductionDetailPage), typeof(ProductionDetailPage));

    Routing.RegisterRoute(nameof(BookMultipleWorkstepsPage), typeof(BookMultipleWorkstepsPage));

    Routing.RegisterRoute(nameof(MachineListPage), typeof(MachineListPage));
    
    //Routing.RegisterRoute($"{nameof(ProductionDetailPage)}/{nameof(ProductionDocumentsPage)}", typeof(ProductionDocumentsPage));

  }

  private void ShellContent_ChildAdded(object sender, ElementEventArgs e)
  {

  }
}
