﻿namespace KieselsteinErp.Terminal.Services
{
  public interface ITranslator
    {

        string Translated(string message);

        string Translated(object message);

    }
}
