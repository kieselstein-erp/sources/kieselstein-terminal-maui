﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Services.Database.Models;
using KieselsteinErp.Terminal.Services.OfflineBooking;

namespace KieselsteinErp.Terminal.Services.Database
{
	public interface IDatabaseService
	{
		Task<int> InsertOrReplaceItemAsync(DBItem item);
		Task<int> DeleteItemAsync(DBItem item);
		Task<int> GetStaffCountOffline();
		Task<List<StaffEntry>> GetStaffOffline();
		Task<int> SaveStaffOffline(List<StaffEntry> staffEntries);
		Task<MachineEntryList> GetMachinesOffline();
		Task<int> SaveMachinesOffline(MachineEntryList machineEntries);
		Task<bool> SaveBookingOffline(string barcode, int machineOverwrite, int bookingType, DateTime now, string identityCnr);
		Task<List<BookingDBItem>> getBookingItems();
		Task<int> getFailedBookings();
	}
}
