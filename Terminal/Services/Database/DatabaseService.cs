﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Services.Database.Models;
using SQLite;

namespace KieselsteinErp.Terminal.Services.Database
{
  public class DatabaseService : IDatabaseService
  {
    SQLiteAsyncConnection _dbConnection;

    public DatabaseService()
    {
    }

    async Task Init()
    {
      if (_dbConnection is not null)
        return;

      _dbConnection = new SQLiteAsyncConnection(DatabaseConstants.DatabasePath, DatabaseConstants.Flags);
      await _dbConnection.CreateTableAsync<StaffDBItem>();
      await _dbConnection.CreateTableAsync<MachineDBItem>();
      await _dbConnection.CreateTableAsync<BookingDBItem>();
    }
    public async Task<int> InsertOrReplaceItemAsync(DBItem item)
    {
      await Init();
      return await _dbConnection.InsertOrReplaceAsync(item);
    }
    public async Task<int> DeleteItemAsync(DBItem item)
    {
      await Init();
      return await _dbConnection.DeleteAsync(item);
    }

    public async Task<int> SaveStaffOffline(List<StaffEntry> staffEntries)
    {
      await Init();
      int saved = 0;
      await _dbConnection.DeleteAllAsync<StaffDBItem>();
      StaffDBItemList staffDBItems = new StaffDBItemList(staffEntries);
      foreach (StaffDBItem item in staffDBItems.Items)
        saved += await InsertOrReplaceItemAsync(item);
      return saved;
    }

    public async Task<List<StaffEntry>> GetStaffOffline()
    {
      await Init();
      StaffDBItemList staffDBItems = new StaffDBItemList();
      staffDBItems.Items = await _dbConnection.Table<StaffDBItem>().ToListAsync(); ;
      return staffDBItems.ConvertToStaffEntryList();
    }

    public async Task<int> GetStaffCountOffline()
    {
      await Init();
      var res = await _dbConnection.ExecuteScalarAsync<int>($"SELECT count(*) FROM {typeof(StaffDBItem).Name};");
      return res;
    }

    public async Task<MachineEntryList> GetMachinesOffline()
    {
      MachineDBItemList machineDBItems = new()
      {
        Items = await _dbConnection.Table<MachineDBItem>().ToListAsync()
      };

      return machineDBItems.ConvertToMachineEntryList();
    }

    public async Task<int> SaveMachinesOffline(MachineEntryList machineEntries)
    {
      await Init();
      int saved = 0;
      MachineDBItemList machineDBItems = new(machineEntries);
      foreach (MachineDBItem machineDBItem in machineDBItems.Items)
        saved += await InsertOrReplaceItemAsync(machineDBItem);
      return saved;
    }

    public async Task<bool> SaveBookingOffline(string barcode, int machineOverwrite, int bookingType, DateTime now, string identityCnr)
    {
      return (await _dbConnection.InsertAsync(new BookingDBItem(barcode, machineOverwrite, bookingType, now, identityCnr)) > 0);
    }

    public async Task<List<BookingDBItem>> getBookingItems()
    {
      await Init();
      return await _dbConnection.QueryAsync<BookingDBItem>($"SELECT * FROM {typeof(BookingDBItem).Name} WHERE Failed IS 0");
    }

    public async Task<int> getFailedBookings()
    {
      await Init();
      var res = await _dbConnection.ExecuteScalarAsync<int>($"SELECT count(*) FROM {typeof(BookingDBItem).Name} WHERE Failed IS 1;");
      return res;
    }
  }
}
