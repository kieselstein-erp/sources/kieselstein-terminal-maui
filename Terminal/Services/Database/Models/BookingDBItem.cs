﻿using KieselsteinErp.Terminal.Services.StateMachine;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KieselsteinErp.Terminal.Services.Database.Models
{
	public class BookingDBItem : DBItem
	{
		public BookingDBItem() { }

		public BookingDBItem(BookingDBItem item)
		{
			ID = item.ID;
			Barcode = item.Barcode;
			MachineOverwrite = item.MachineOverwrite;
			BookingType = item.BookingType;
			Timestamp = item.Timestamp;
			StaffIdentityCnr = item.StaffIdentityCnr;
			Failed = item.Failed;
			FailedMessage = item.FailedMessage;
		}

		public BookingDBItem(string barcode, int machineOverwrite, int bookingType, DateTime timestamp, string staffIdentityCnr)
		{
			Barcode = barcode;
			MachineOverwrite = machineOverwrite;
			BookingType = bookingType;
			Timestamp = timestamp;
			StaffIdentityCnr = staffIdentityCnr;
		}

		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string StaffIdentityCnr { get; set; }
		public string Barcode { get; set; }
		public int MachineOverwrite { get; set; }
		public int BookingType { get; set; }
		public bool Failed { get; set; }
		public string FailedMessage { get; set; }
		public DateTime Timestamp { get; set; }

	}
}
