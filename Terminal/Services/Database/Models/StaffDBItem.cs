﻿using Kieselstein.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KieselsteinErp.Terminal.Services.Database.Models
{
	public class StaffDBItem : DBItem

	{
		public StaffDBItem() { }
		public StaffDBItem(StaffEntry entry) 
		{
			ID = entry.id;
			PersonalNr = entry.personalNr;
			IdentityCnr = entry.identityCnr;
			Name = entry.name;
			FirstName = entry.firstName;
		}
		[PrimaryKey]
		public int ID { get; set; }
		public string PersonalNr { get; set; }
		[Unique]
		public string IdentityCnr { get; set; }
		public string Name { get; set; }
		public string FirstName { get; set; }
		public DateTime Update	 { get; set; }
		public bool BookComing { get; set; }
		public bool NoDisplay { get; set; }
		public bool BookingAllowed { get; set; }
		public decimal MachineGroup { get; set; }

		public StaffEntry ConvertToStaffEntry()
		{
			return new StaffEntry() { id = ID, identityCnr = IdentityCnr, firstName = FirstName, name = Name, personalNr = PersonalNr };
		}
	}
}
