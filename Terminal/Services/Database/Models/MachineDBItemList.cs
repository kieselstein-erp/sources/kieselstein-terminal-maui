﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Services.Database.Models
{
  public class MachineDBItemList
  {
    public MachineDBItemList()
    {
    }

    public MachineDBItemList(MachineEntryList machineEntries)
    {
      Items = [];
      if (machineEntries.entries == null) return;

      DateTime now = DateTime.Now;      
      foreach (MachineEntry entry in machineEntries.entries)
        Items.Add(new MachineDBItem(entry) { Update = now });
    }

    public List<MachineDBItem> Items { get; set; }

    public MachineEntryList ConvertToMachineEntryList()
    {
      MachineEntryList machineEntries = new()
      {
        entries = []
      };

      foreach (MachineDBItem machineDBItem in Items)
        machineEntries.entries.Add(machineDBItem.ConvertToMachineEntry());
      return machineEntries;
    }
  }
}
