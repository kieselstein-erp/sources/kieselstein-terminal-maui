﻿using Kieselstein.Models;
using SQLite;

namespace KieselsteinErp.Terminal.Services.Database.Models
{
  public class MachineDBItem : DBItem
	{
		public MachineDBItem() { }
		public MachineDBItem(MachineEntry entry)
		{
			ID = entry.id;
			IdentificationNumber = entry.identificationNumber;
			Description = entry.description;
			MachineGroupId = entry.machineGroupId;
			MachineGroupShortDescription = entry.machineGroupShortDescription;
		}
		[PrimaryKey]
		public int ID { get; set; }
		[Unique]
		public string IdentificationNumber { get; set; }
		public string Description { get; set; }
		public int MachineGroupId { get; set; }
		public string MachineGroupShortDescription { get; set; }
		public DateTime Update { get; set; }

		public MachineEntry ConvertToMachineEntry()
		{
			return new MachineEntry() { id = ID, identificationNumber = IdentificationNumber, description = Description, machineGroupId = MachineGroupId, machineGroupShortDescription = MachineGroupShortDescription };
		}
	}
}
