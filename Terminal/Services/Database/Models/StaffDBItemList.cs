﻿using Kieselstein.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KieselsteinErp.Terminal.Services.Database.Models
{
	internal class StaffDBItemList
	{
		public StaffDBItemList()
		{
		}

		public StaffDBItemList(List<StaffEntry> staffEntries) 
		{ 
			DateTime now = DateTime.Now;
			Items = new List<StaffDBItem>();
			foreach (StaffEntry entry in staffEntries)
			{
				Items.Add(new StaffDBItem(entry) {Update = now});
			}
		}
		public List<StaffDBItem> Items { get; set; }

		public List<StaffEntry> ConvertToStaffEntryList()
		{
			List<StaffEntry> staffEntries = new List<StaffEntry>();
			foreach (StaffDBItem staffDBItem in Items)
				staffEntries.Add(staffDBItem.ConvertToStaffEntry());
			return staffEntries;
		}
	}
}
