﻿using CommunityToolkit.Maui.Views;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Popups;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.StateMachine;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.Production
{
  public class ProductionService : IProductionService
  {
    private readonly ISettingsService _settingsService;
    private readonly IMessageService _messageService;
    private readonly IStateMachine _stateMachine;
    private readonly IProductionRestService _productionService;

    public ProductionService(ISettingsService settingsService, IMessageService messageService, IStateMachine stateMachine, IProductionRestService productionService)
    {
      _settingsService = settingsService;
      _messageService = messageService;
      _stateMachine = stateMachine;
      _productionService = productionService;
    }

    public async Task DoDeliver(DeliverMessage message)
    {
      ProductionEntry productionEntry = await _productionService.GetProductionEntry(message.Value);
      if (productionEntry == null)
        _messageService.Error(string.Format(Error_Lot_Not_Found, message.Value));
      else
        await DoDeliver(message, productionEntry);
    }

    public async Task DoDeliver(DeliverMessage message, ProductionEntry productionEntry)
    {
      //IsLoadingCircleVisible = false;
      string info = string.Empty;
      if (!string.IsNullOrEmpty(_settingsService.IdentifierDeliveryNote) && (message.PreferedType == _settingsService.IdentifierDeliveryNote))
        info = PleaseEncloseDeliveryPapers;

      bool needDeviceAssemblies = false;
      ProductionTargetMaterialEntryList productionTargetMaterialEntryList = null;
      if (_settingsService.DeviceAssemblyDelivery && message.ItemV1Entry != null && message.ItemV1Entry.hasSerialnr)
      {
        productionTargetMaterialEntryList = await _productionService.GetMaterialList(productionEntry.id);
        if (productionTargetMaterialEntryList != null && productionTargetMaterialEntryList.entries != null)
          foreach (ProductionTargetMaterialEntry productionTargetMaterialEntry in productionTargetMaterialEntryList.entries)
          {
            if (productionTargetMaterialEntry.itemEntry.hasSerialnr)
            {
              needDeviceAssemblies = true;
              break;
            }
          }
      }

      if (needDeviceAssemblies) // deliver with additional device assemblies serialnumbers
      {
        List<DeviceAssemblyEntry> assemblies = new List<DeviceAssemblyEntry>();
        foreach (ProductionTargetMaterialEntry productionTargetMaterialEntry in productionTargetMaterialEntryList.entries)
        {
          if (productionTargetMaterialEntry.itemEntry.hasSerialnr)
          {
            AvailableSerialnumbersList availableSerialnumbers = await _productionService.GetAvailableSerialnumbers(productionEntry.id, productionTargetMaterialEntry.id);
            assemblies.Add(new DeviceAssemblyEntry { Item = productionTargetMaterialEntry.itemEntry, Serialnumbers = availableSerialnumbers.entries });
          }
        }
        string serialNumber = string.Empty;
        var res = await Application.Current.MainPage.ShowPopupAsync(new DeviceDeliveryIdentityAmountEntryPopup(_messageService, message.ItemV1Entry.cnr, serialNumber, assemblies));
        PopupDeviceDeliveryResult result = (PopupDeviceDeliveryResult)res;
        if (result != null)
        {
          decimal amount = 1;
          List<IdentityAmountEntry> identities = new List<IdentityAmountEntry>();
          identities.Add(new IdentityAmountEntry { amount = 1, identity = result.SerialNumber });
          await Task.Run(() => _stateMachine.OnCodeReceived(new ProductionEvent(ProductionEventType.DeliverAmountIdentities, message.NoUnidentify, amount, 0, message.Value, result.BoolValue, identities, result.Entries, string.Empty)));
        }
      }
      else if (message.ItemV1Entry != null && (message.ItemV1Entry.hasChargenr || message.ItemV1Entry.hasSerialnr))
      {
        List<IdentityAmountEntry> identities = new List<IdentityAmountEntry>();
        var res = await Application.Current.MainPage.ShowPopupAsync(new DeliveryIdentityAmountEntryPopup(_messageService,
          message.ItemV1Entry.hasChargenr, string.Format(DeliveryFor01, message.Value, message.ItemV1Entry.cnr), null, identities));
        PopupDeliveryResult result = (PopupDeliveryResult)res;
        if (result != null)
        {
          decimal amount = 0;
          foreach (IdentityAmountEntry identityAmount in result.Entries)
            amount += identityAmount.amount;
          if (amount == 0)
            _messageService.Info(DeliveryNotProcessed + " " + AmountWas0);
          else
            await Task.Run(() => _stateMachine.OnCodeReceived(new ProductionEvent(ProductionEventType.DeliverAmountIdentities, message.NoUnidentify, amount, result.BadAmount, message.Value, result.BoolValue, result.Entries, string.Empty)));
        }
      }
      else
      {
        if (_settingsService.BookDeliveryBadAmount)
        {
          var res = await Application.Current.MainPage.ShowPopupAsync(new DecimalEntryListPopup(_messageService,
          new NumericEntryInfoList(3, 0, 0, 99999999, null, new List<string>() { EnterGoodAmount, EnterDefectAmount },
          new List<string>() { Good, Defect }, 2, 0.4),
          info));
          PopupListResult result = (PopupListResult)res;
          if (!result.Canceled)
          {
            if (result.Entries[0] + result.Entries[1] == 0)
              _messageService.Info(DeliveryNotProcessed + " " + AmountWas0);
            else
              await Task.Run(() => _stateMachine.OnCodeReceived(new ProductionEvent(ProductionEventType.DeliverAmount, message.NoUnidentify, result.Entries[0], result.Entries[1], message.Value, result.BoolValue, string.Empty))); // TODO: _state.UserId)));
          }
        }
        else
        {
          decimal amount = 0;
          var res = await Application.Current.MainPage.ShowPopupAsync(new DecimalEntryPopup(_messageService, 3, amount, 0, 99999999, info));
          PopupResult result = (PopupResult)res;
          if (!result.Canceled)
          {
            if (result.EntryAsDecimal() == 0)
              _messageService.Info(DeliveryNotProcessed + " " + AmountWas0);
            else
              await Task.Run(() => _stateMachine.OnCodeReceived(new ProductionEvent(ProductionEventType.DeliverAmount, message.NoUnidentify, result.EntryAsDecimal(), message.Value, string.Empty)));
          }
        }
      }
    }

    public async Task DoChangeAmount(ChangeAmountMessage message, bool NoUnidentify = false)
    {
      var res = await Application.Current.MainPage.ShowPopupAsync(new DecimalEntryPopup(_messageService, 0, message.Value.amount, string.Empty));
      PopupResult result = (PopupResult)res;
      if (!result.Canceled)
      {
        await Task.Run(() => _stateMachine.OnCodeReceived(new ProductionEvent(NoUnidentify ? ProductionEventType.DoChangeAmount : ProductionEventType.DoChangeAmount, true, Convert.ToDecimal(result.Entry), message.Value.lotString)));
      }
    }
  }
}
