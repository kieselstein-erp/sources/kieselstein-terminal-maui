﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Kieselstein.Models;
using KieselsteinErp.Terminal.Models;

namespace KieselsteinErp.Terminal.Services.Production
{
  public interface IProductionRestService
  {
    Task<BookingResult> BookDeliver(decimal amount, decimal defectAmount, string lotString, bool printLabel, string staffCnr);
    Task<BookingResult> BookDeliverIdentities(decimal amount, decimal defectAmount, string lotString, bool printLabel, string staffCnr, List<IdentityAmountEntry> identities);
    Task<BookingResult> BookDeliverIdentities(decimal amount, decimal defectAmount, string lotString, bool printLabel, string staffCnr, List<IdentityAmountEntry> identities, List<ItemIdentityAmountEntry> assemblies);
    Task<BookingResult> BookDeliver(decimal amount, string lotString, string idCard);
    Task<LotState> LotValid(string lotString);
    LotState LotValid(ProductionEntry productionEntry);
    Task<decimal?> LotAmount(string lotString);
    Task<BookingResult> ChangeLotAmount(string lotString, decimal amount);
    Task<ProductionEntry> GetProductionEntry(string lotString, bool addWorkSteps = false);
    Task<ProductionTargetMaterialEntryList> GetMaterialList(int id);
    Task<BookingResult> BookMaterialWithdrawal(string lotCnr, int stockId, int? targerMaterialId, string itemCnr, decimal amount, List<IdentityAmountEntry> identityAmountEntries);
    Task<OpenWorkEntryList> OpenWorkEntriesForStaff(string staffCnr, DateTime? filter_startdate, DateTime? filter_enddate, bool filter_mainworksteponly);
    Task<List<ProductionEntry>> GetProductionEntries(string text, int limit);
    Task<ProductionActualMaterialEntryList> GetActualMaterialList(int productionId);
    Task<AvailableSerialnumbersList> GetAvailableSerialnumbers(int productionId, int targetmaterialId);
    Task<PdfResult> GetProductionOrderFinalCosting(int productionId);
    Task<PdfResult> GetProductionOrderInformation(int productionId);
    Task<BookingResult> WorkstepDoneAsync(OpenWorkUpdateEntry openWorkUpdateEntry);
    Task<DocumentListResult> GetDocumentInfoListAsync(int productionId);
    Task<DocumentResult> GetDocumenAsync(int productionId, string documentCnr);
    Task<CommentMediaListResult> GetItemCommentMediaInfoEntriesAsync(int productionId);
    Task<DocumentResult> GetCommentAsync(int productionId, int itemcommentId);
    ProductionRecordingEntry BuildProductionrecordingEntry(int productionId, int machineId, int productionWorkplanId, int workItemId, int forStaffId);
    Task<OpenWorkEntryList> OpenWorkEntries(string filter_cnr, DateTime? filter_startdate, DateTime? filter_enddate, bool filter_inproductiononly, int? limit);
  }
}
