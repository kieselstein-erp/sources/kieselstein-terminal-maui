﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services.Item;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.User;
using KieselsteinErp.Terminal.Util;
using Microsoft.Extensions.Logging;
using RestSharp;
using System.Text.Json;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.Production
{
  public class ProductionRestService : RestBase, IProductionRestService
  {
    private readonly ILogger _logger;
    private readonly IMessageService _messageService;
    private readonly IItemV1Service _itemV1Service;

    public ProductionRestService(ILogger<ProductionRestService> logger, ISettingsService settingsService,
            IUserService userService,
            IItemV1Service itemV1Service,
            IMessageService messageService) : base(settingsService, userService)
    {
      _logger = logger;
      _messageService = messageService;
      _itemV1Service = itemV1Service;
    }

    public async Task<BookingResult> BookDeliver(decimal amount, decimal defectAmount, string lotString, bool printLabel, string staffCnr)
    {
      ProductionEntry productionEntry = await GetProductionEntry(lotString);
      DeliveryPostEntry deliveryPostEntry = BuildDeliveryPostEntry(amount, defectAmount, staffCnr);

      string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(deliveryPostEntry);
      string toastMessage = string.Format(BookedDeliver, amount, lotString);
      BookingResult deliveryResult = await PostAsync(strJson, $"production/{productionEntry.id}/delivery", toastMessage);
      if (deliveryResult.Success && printLabel)
      {
        var request = new RestRequest("production/printlabels", Method.Get)
          .AddQueryParameter("printLabel", true)
          .AddQueryParameter("userid", Token())
          .AddQueryParameter("productionCnr", lotString)
          .AddQueryParameter("printLabelQuantity", amount)
          .AddQueryParameter("printLabelCopies", 1);

        return await PrintProductionLabelAsync(request, deliveryResult);
      }

      else
        return deliveryResult;
    }

    public async Task<BookingResult> BookDeliverIdentities(decimal amount, decimal defectAmount, string lotString, bool printLabel, string staffCnr, List<IdentityAmountEntry> identities)
    {
      return await BookDeliverIdentities(amount, defectAmount, lotString, printLabel, staffCnr, identities, null);
    }

    public async Task<BookingResult> BookDeliverIdentities(decimal amount, decimal defectAmount, string lotString, bool printLabel, string staffCnr, List<IdentityAmountEntry> identities, List<ItemIdentityAmountEntry> assemblies)
    {
      DeliveryPostEntry deliveryPostEntry = BuildDeliveryPostEntry(amount, defectAmount, identities, assemblies, staffCnr);
      ProductionEntry productionEntry = await GetProductionEntry(lotString);

      string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(deliveryPostEntry);
      string toastMessage = string.Format(BookedDeliver, amount, lotString);
      BookingResult deliveryResult = await PostAsync(strJson, $"production/{productionEntry.id}/delivery", toastMessage);
      if (deliveryResult.Success && printLabel)
      {
        var request = new RestRequest("production/printlabels", Method.Get)
          .AddQueryParameter("printLabel", true)
          .AddQueryParameter("userid", Token())
          .AddQueryParameter("productionCnr", lotString)
          .AddQueryParameter("printLabelQuantity", amount)
          .AddQueryParameter("printLabelCopies", 1);

        return await PrintProductionLabelAsync(request, deliveryResult);
      }

      else
        return deliveryResult;
    }

    public async Task<BookingResult> BookDeliver(decimal amount, string lotString, string idCard)
    {
      var request = new RestRequest("personal/losablieferung", Method.Post)
        //.AddStringBody("", ContentType.Plain)
        .AddQueryParameter("productionCnr", lotString)
        .AddQueryParameter("amount", amount)
        .AddQueryParameter("idCard", idCard)
        .AddQueryParameter("station", DeviceInfo.Current.Name)
        .AddQueryParameter("userid", Token());
      string toastMessage = string.Format(BookedDeliver, amount, lotString);

      try
      {
        //request.AddHeader("Content-Type", "application/json");

        var response = ClientSoapcalls.ExecutePost(request);

        if (response.IsSuccessful)
        {
          SoapCallPersonalResult result = JsonSerializer.Deserialize<SoapCallPersonalResult>(response.Content);
          if (result.resultCode >= 0)
            return new BookingResult(true, toastMessage);
          else
            return new BookingResult(false, string.Format(Error_Message_Action, Booking) + string.Format("Request returned Resultcode {0}", result.resultCode));
        }
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
            {
              return new BookingResult(false, msg);
            }
          }
          return new BookingResult(false, string.Format(Error_Message_Action, Booking) + string.Format("Request returned Statuscode {0}", response.StatusCode));
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
      }
      return new BookingResult(false, string.Format(Error_Message_Action, Booking));
    }

    private async Task<BookingResult> PrintProductionLabelAsync(RestRequest request, BookingResult deliveryResult)
    {
      PrintProductionLabelEntry response = null;
      try
      {
        response = await Client.GetAsync<PrintProductionLabelEntry>(request);
      }
      catch (Exception ex)
      {
        _messageService.Error(ex.Message);
      }
      deliveryResult.PrintRequested = true;
      deliveryResult.PrintSuccess = response == null ? false : response.printLabel;
      return deliveryResult;
    }

    public async Task<BookingResult> ChangeLotAmount(string lotString, decimal amount)
    {
      var request = new RestRequest("production/changeproductionordersize")
          .AddQueryParameter("userid", Token())
          .AddQueryParameter("productionCnr", lotString)
          .AddQueryParameter("productionordersize", amount);


      try
      {
        var response = await Client.ExecutePutAsync(request);

        if (response.IsSuccessful)
        {

          return new BookingResult(true, string.Format(ChangeAmountSuccess, lotString, amount));
        }
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
            {
              return new BookingResult(false, msg);
            }
          }
          return new BookingResult(false, string.Format(Error_Message_Action, ChangeAmount) + string.Format("Request returned Statuscode {0}", response.StatusCode));
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
      }
      return new BookingResult(false, string.Format(Error_Message_Action, ChangeAmount));
    }

    public async Task<ProductionEntry> GetProductionEntry(string lotString, bool addWorkSteps = false)
    {
      var request = new RestRequest("production")
          .AddQueryParameter("userid", Token())
          .AddQueryParameter("addPartlistItem", true)
          .AddQueryParameter("addDeliveredAmount", true)
          .AddQueryParameter("productionCnr", lotString);
      if (addWorkSteps)
        request.AddQueryParameter("addProductionWorksteps", true);
      try
      {
        var response = await Client.GetAsync<ProductionEntry>(request);
        if (response != null && response.itemEntry != null)
        {
          ItemV1Entry itemV1Entry = await _itemV1Service.GetItemV1EntryAsync(response.itemEntry.cnr, false);
          if (itemV1Entry != null)
          {
            response.itemEntry = itemV1Entry;
          }
        }
        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<List<ProductionEntry>> GetProductionEntries(string lotString, int limit)
    {
      var request = new RestRequest("production/list/{userid}")
          .AddUrlSegment("userid", Token())
          .AddQueryParameter("filter_cnr", lotString)
          .AddQueryParameter("limit", limit);
      try
      {
        var response = await Client.GetAsync<ProductionEntryList>(request);
        return response.entries;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }


    public async Task<LotState> LotValid(string lotString)
    {
      ProductionEntry productionEntry = await GetProductionEntry(lotString);
      return LotValid(productionEntry);
    }

    public LotState LotValid(ProductionEntry productionEntry)
    {
      if (productionEntry == null)
        return LotState.NOTFOUND;
      object res;
      if (!Enum.TryParse(typeof(LotState), productionEntry.status, out res))
        throw new Exception("Invalid lot state");

      return (LotState)res;
    }

    public async Task<decimal?> LotAmount(string lotString)
    {
      ProductionEntry productionEntry = await GetProductionEntry(lotString);
      return (decimal?)productionEntry?.amount;
    }

    public async Task<ProductionTargetMaterialEntryList> GetMaterialList(int id)
    {
      try
      {
        var request = new RestRequest($"production/{id}/targetmaterial").AddQueryParameter("userid", Token());

        var response = await Client.GetAsync<ProductionTargetMaterialEntryList>(request);
        return response;
        //if (response.IsSuccessful)
        //{
        //	return ((ProductionTargetMaterialEntryList) response.Content).entries;
        //}
        //else
        //{
        //	if (response.Headers?.Count > 0)
        //	{
        //		string msg = BuildMessageFromHeader(response);
        //		if (!string.IsNullOrEmpty(msg))
        //		{
        //			return null;
        //		}
        //	}
        //	return null;
        //}
      }
      catch (Exception ex)
      {
        _messageService.Error(ex.Message);
      }
      return new ProductionTargetMaterialEntryList();
    }

    public async Task<ProductionActualMaterialEntryList> GetActualMaterialList(int productionId)
    {
      try
      {
        var request = new RestRequest($"production/{productionId}/actualmaterial").AddQueryParameter("userid", Token());

        var response = await Client.GetAsync<ProductionActualMaterialEntryList>(request);
        return response;
      }
      catch (Exception ex)
      {
        _messageService.Error(ex.Message);
      }
      return new ProductionActualMaterialEntryList();
    }

    public async Task<AvailableSerialnumbersList> GetAvailableSerialnumbers(int productionId, int targetmaterialId)
    {
      try
      {
        var request = new RestRequest($"production/{productionId}/availableserialnumbers").AddQueryParameter("targetmaterialId", targetmaterialId).AddQueryParameter("userid", Token());

        var response = await Client.GetAsync<AvailableSerialnumbersList>(request);
        return response;
      }
      catch (Exception ex)
      {
        _messageService.Error(ex.Message);
      }
      return new AvailableSerialnumbersList();
    }

    public Task<BookingResult> BookMaterialWithdrawal(string lotCnr, int stockId, int? targetMaterialId, string itemCnr, decimal amount, List<IdentityAmountEntry> identityAmountEntries)
    {
      string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(new MaterialWithdrawalEntry()
      { lotCnr = lotCnr, stockId = stockId, targetMaterialId = targetMaterialId, itemCnr = itemCnr, amount = amount, identities = identityAmountEntries });
      string toastMessage = "success";
      string resource = "production/materialwithdrawal";
      var request = new RestRequest(resource, Method.Post).AddJsonBody(strJson).AddQueryParameter("userId", Token());
      return PostAsync(request, toastMessage);
    }

    private DeliveryPostEntry BuildDeliveryPostEntry(decimal amount, decimal defectAmount, string staffCnr)
    {
      DeliveryPostEntry deliveryPostEntry = new DeliveryPostEntry()
      {
        amount = amount,
        scrapAmount = defectAmount != 0 ? defectAmount : null,
        userId = Token()
      };
      if (!string.IsNullOrEmpty(staffCnr))
        deliveryPostEntry.timeRecordingEntry = new TimeRecordingEntry { forStaffCnr = staffCnr };

      return deliveryPostEntry;
    }

    private DeliveryPostEntry BuildDeliveryPostEntry(decimal amount, decimal defectAmount, List<IdentityAmountEntry> identities, List<ItemIdentityAmountEntry> assemblies, string staffCnr)
    {
      DeliveryPostEntry deliveryPostEntry = new DeliveryPostEntry()
      {
        amount = amount,
        scrapAmount = defectAmount != 0 ? defectAmount : null,
        identities = identities,
        userId = Token(),
        deviceIdentities = assemblies
      };
      return deliveryPostEntry;
    }

    private async Task<BookingResult> PostAsync(RestRequest request, string toastMessage)
    {
      // Kieselstein API call /barcode needs this additional header "Content-Type", "application/json"
      // default header is "ContentType",  "application/json; charset=utf-8" response is 500 internal server error
      // and also field names must start with lower case
      try
      {
        request.AddHeader("Content-Type", "application/json");

        var response = Client.ExecutePost(request);

        if (response.IsSuccessful)
        {
          return new BookingResult(true, toastMessage);
        }
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
            {
              return new BookingResult(false, msg);
            }
          }
          return new BookingResult(false, string.Format(Error_Message_Action, Booking) + string.Format("Request returned Statuscode {0}", response.StatusCode));
        }
      }
      catch (HttpRequestException e)
      {
        return new BookingResult(false, string.Format(Error_Message_Action, e.Message));
      }
      catch (Exception ex)
      {
        return new BookingResult(false, string.Format(Error_Message_Action, ex.Message));
      }
    }

    private async Task<BookingResult> PostAsync(string strJson, string resource, string toastMessage)
    {
      var request = new RestRequest(resource, Method.Post).AddJsonBody(strJson);
      return await PostAsync(request, toastMessage);
    }

    public async Task<OpenWorkEntryList> OpenWorkEntriesForStaff(string staffCnr, DateTime? filterStartdate, DateTime? filterEnddate, bool filterMainworkstepOnly)
    {
      try
      {
        var request = new RestRequest($"production/openworkforstaff").AddQueryParameter("userid", Token()).AddQueryParameter("forStaffCnr", staffCnr);

        if (filterStartdate != null)
          request.AddQueryParameter("filter_startdate", DateTimeHelper.ToMillisecondsSinceUnixEpoch(filterStartdate.Value));

        if (filterEnddate != null)
          request.AddQueryParameter("filter_enddate", DateTimeHelper.ToMillisecondsSinceUnixEpoch(filterEnddate.Value));

        if (filterMainworkstepOnly)
          request.AddQueryParameter("filter_mainworksteponly", filterMainworkstepOnly);

        var response = await Client.GetAsync<OpenWorkEntryList>(request);
        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
      }
      return null;
    }

    public async Task<PdfResult> GetProductionOrderFinalCosting(int productionId)
    {
      var request = new RestRequest("production/{productionid}/productionorderfinalcosting")
        .AddUrlSegment("productionid", productionId)
        .AddQueryParameter("userid", Token());

      try
      {
        var response = await Client.ExecuteGetAsync<ProductionOrderFinalCosting>(request);
        if (response.IsSuccessful)
          return new PdfResult(response.Data.pdfContent);
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
            {
              return new PdfResult(false, msg, response.StatusCode);
            }
          }
          return new PdfResult(false, string.Format(Error_Message_Action, "GetProductionOrderFinalCosting") + string.Format("Request returned Statuscode {0}", response.StatusCode), response.StatusCode);
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return new PdfResult(false, ex.Message, System.Net.HttpStatusCode.BadRequest);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return new PdfResult(false, ex.Message, System.Net.HttpStatusCode.BadRequest);
      }
    }

    public async Task<PdfResult> GetProductionOrderInformation(int productionId)
    {
      var request = new RestRequest("production/{productionid}/productionorderinformation")
        .AddUrlSegment("productionid", productionId)
        .AddQueryParameter("userid", Token());

      try
      {
        var response = await Client.ExecuteGetAsync<ProductionOrderInformation>(request);
        if (response.IsSuccessful)
          return new PdfResult(response.Data.pdfContent);
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
            {
              return new PdfResult(false, msg, response.StatusCode);
            }
          }
          return new PdfResult(false, string.Format(Error_Message_Action, "GetProductionOrderInformation") + string.Format("Request returned Statuscode {0}", response.StatusCode), response.StatusCode);
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return new PdfResult(false, ex.Message, System.Net.HttpStatusCode.BadRequest);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return new PdfResult(false, ex.Message, System.Net.HttpStatusCode.BadRequest);
      }
    }

    public async Task<BookingResult> WorkstepDoneAsync(OpenWorkUpdateEntry openWorkUpdateEntry)
    {
      try
      {
        OpenWorkUpdateEntryList openWorkUpdateEntryList = new OpenWorkUpdateEntryList();
        openWorkUpdateEntryList.entries=new List<OpenWorkUpdateEntry> { openWorkUpdateEntry };
        string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(openWorkUpdateEntryList);
        var request = new RestRequest("production/doneopenworklist", Method.Put)
          .AddJsonBody(strJson)
          .AddQueryParameter("userid", Token());

        // Kieselstein API call /barcode needs this additional header "Content-Type", "application/json"
        // default header is "ContentType",  "application/json; charset=utf-8" response is 500 internal server error
        // and also field names must start with lower case

        request.AddHeader("Content-Type", "application/json");

        var response = await Client.ExecutePutAsync(request);

        if (response.IsSuccessful)
        {
          return new BookingResult(true, "");
        }
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
            {
              //_messageService.Error(msg);
              return new BookingResult(false, msg);
            }
          }
        }
        return new BookingResult(false, string.Format(Error_Message_Action, string.Format("production order number {0} workstep id {1}", openWorkUpdateEntry.productionCnr, openWorkUpdateEntry.id)) + "Request returned Statuscode " + response.StatusCode);
      }
      catch (HttpRequestException e)
      {
        _logger.LogError(e.Message, e);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
      }
      return new BookingResult(false, string.Format(Error_Message_Action, string.Format("production order number {0} workstep id {1}", openWorkUpdateEntry.productionCnr, openWorkUpdateEntry.id)));
    }

    public async Task<DocumentListResult> GetDocumentInfoListAsync(int productionId)
    {
      var request = new RestRequest("production/{productionid}/document/list")
        .AddUrlSegment("productionid", productionId)
        .AddQueryParameter("userid", Token());

      try
      {
        var response = await Client.ExecuteGetAsync<DocumentInfoEntryList>(request);
        if (response.IsSuccessful)
          return new DocumentListResult(response.Data);
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
            {
              return new DocumentListResult(false, msg, response.StatusCode);
            }
          }
          return new DocumentListResult(false, string.Format(Error_Message_Action, "GetDocumentInfoListAsync") + string.Format("Request returned Statuscode {0}", response.StatusCode), response.StatusCode);
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return new DocumentListResult(false, ex.Message, System.Net.HttpStatusCode.BadRequest);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return new DocumentListResult(false, ex.Message, System.Net.HttpStatusCode.BadRequest);
      }
    }

    public async Task<DocumentResult> GetDocumenAsync(int productionId, string documentCnr)
    {
      var request = new RestRequest("production/{productionid}/document")
        .AddUrlSegment("productionid", productionId)
        .AddQueryParameter("documentcnr", documentCnr)
        .AddQueryParameter("userid", Token());

      try
      {
        var response = await Client.ExecuteGetAsync(request);
        if (response.IsSuccessful)
        {
          string fileName = string.Empty;
          if (response.ContentType == "application/octet-stream")
          {
            string contentDisposition = null;
            try
            {
              contentDisposition = (string)response.ContentHeaders.First(o => o.Name.ToLower() == "content-disposition").Value;
            }
            catch (Exception e)
            {
              _logger.LogWarning(e.Message);
            }
            if (contentDisposition != null)
              if (contentDisposition.Contains('='))
                fileName = contentDisposition.Substring(contentDisposition.IndexOf('=') + 1);
              else
                fileName = contentDisposition;
          }
          return new DocumentResult(response.RawBytes, fileName);
        }
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
            {
              return new DocumentResult(false, msg, response.StatusCode);
            }
          }
          return new DocumentResult(false, string.Format(Error_Message_Action, "GetDocument") + string.Format("Request returned Statuscode {0}", response.StatusCode), response.StatusCode);
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return new DocumentResult(false, ex.Message, System.Net.HttpStatusCode.BadRequest);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return new DocumentResult(false, ex.Message, System.Net.HttpStatusCode.BadRequest);
      }
    }

    public async Task<CommentMediaListResult> GetItemCommentMediaInfoEntriesAsync(int productionId)
    {
      var request = new RestRequest("production/{productionid}/commentmedia/list")
        .AddUrlSegment("productionid", productionId)
        .AddQueryParameter("userid", Token());

      try
      {
        var response = await Client.ExecuteGetAsync<ItemCommentMediaInfoEntryList>(request);
        if (response.IsSuccessful)
          return new CommentMediaListResult(response.Data);
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
            {
              return new CommentMediaListResult(false, msg, response.StatusCode);
            }
          }
          return new CommentMediaListResult(false, string.Format(Error_Message_Action, "GetDocumentInfoListAsync") + string.Format("Request returned Statuscode {0}", response.StatusCode), response.StatusCode);
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return new CommentMediaListResult(false, ex.Message, System.Net.HttpStatusCode.BadRequest);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return new CommentMediaListResult(false, ex.Message, System.Net.HttpStatusCode.BadRequest);
      }
    }

    public async Task<DocumentResult> GetCommentAsync(int productionId, int itemcommentId)
    {
      var request = new RestRequest("production/{productionid}/commentmedia/{itemcommentid}")
        .AddUrlSegment("productionid", productionId)
        .AddUrlSegment("itemcommentid", itemcommentId)
        .AddQueryParameter("userid", Token());

      try
      {
        var response = await Client.ExecuteGetAsync(request);
        if (response.IsSuccessful)
        {
          string fileName = string.Empty;
          if (response.ContentType == "application/octet-stream")
          {
            string contentDisposition = null;
            try
            {
              contentDisposition = (string)response.ContentHeaders.First(o => o.Name.ToLower() == "content-disposition").Value;
            }
            catch (Exception e)
            {
              _logger.LogWarning(e.Message);
            }
            if (contentDisposition != null)
              if (contentDisposition.Contains('='))
                fileName = contentDisposition.Substring(contentDisposition.IndexOf('=') + 1);
              else
                fileName = contentDisposition;
          }
          return new DocumentResult(response.RawBytes, fileName);
        }
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
            {
              return new DocumentResult(false, msg, response.StatusCode);
            }
          }
          return new DocumentResult(false, string.Format(Error_Message_Action, "GetDocument") + string.Format("Request returned Statuscode {0}", response.StatusCode), response.StatusCode);
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return new DocumentResult(false, ex.Message, System.Net.HttpStatusCode.BadRequest);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return new DocumentResult(false, ex.Message, System.Net.HttpStatusCode.BadRequest);
      }
    }

    public ProductionRecordingEntry BuildProductionrecordingEntry(int productionId, int machineId, int productionWorkplanId, int workItemId, int forStaffId)
    {
      ProductionRecordingEntry entry = new()
      {
        productionId = productionId,
        machineId = machineId,
        productionWorkplanId = productionWorkplanId,
        workItemId = workItemId,
        forStaffId = forStaffId,
        year = DateTime.Now.Year,
        month = DateTime.Now.Month,
        day = DateTime.Now.Day,
        hour = DateTime.Now.Hour,
        minute = DateTime.Now.Minute,
        second = DateTime.Now.Second,
        where = DeviceInfo.Current.Name
      };
      return entry;
    }

    public async Task<OpenWorkEntryList> OpenWorkEntries(string filterCnr, DateTime? filterStartdate, DateTime? filterEnddate, bool filterInproductionOnly, int? limit)
    {
      try
      {
        var request = new RestRequest($"production/openwork").AddQueryParameter("userid", Token());

        if (filterCnr != null)
          request.AddQueryParameter("filter_cnr", filterCnr);

        if (filterStartdate != null)
          request.AddQueryParameter("filter_startdate", DateTimeHelper.ToMillisecondsSinceUnixEpoch(filterStartdate.Value));

        if (filterEnddate != null)
          request.AddQueryParameter("filter_enddate", DateTimeHelper.ToMillisecondsSinceUnixEpoch(filterEnddate.Value));

        if (filterInproductionOnly)
          request.AddQueryParameter("filter_inproductiononly", filterInproductionOnly);

        if (limit != null)
          request.AddQueryParameter("limit", (int)limit);

        var response = await Client.GetAsync<OpenWorkEntryList>(request);
        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
      }
      return null;
    }

  }
}
