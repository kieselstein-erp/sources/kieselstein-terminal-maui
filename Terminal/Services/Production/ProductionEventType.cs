﻿namespace KieselsteinErp.Terminal.Services.Production;

public enum ProductionEventType
{
  Deliver,
  DeliverAmount,
  DeliverAmountIdentities,
  ChangeAmount,
  DoChangeAmount,
  BookMaterialToLot,
  BookMultipleWorksteps
}