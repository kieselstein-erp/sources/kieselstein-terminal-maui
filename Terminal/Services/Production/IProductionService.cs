﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;

namespace KieselsteinErp.Terminal.Services.Production
{
  public interface IProductionService
  {
    Task DoDeliver(DeliverMessage message);
    Task DoDeliver(DeliverMessage message, ProductionEntry productionEntry);
    Task DoChangeAmount(ChangeAmountMessage message, bool noUnidentify = false);
  }
}