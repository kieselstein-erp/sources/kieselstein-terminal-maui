﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KieselsteinErp.Terminal.Services.OfflineBooking
{
    public enum OfflineBookingType
    {
        LotActivity,
        Workstep,
        Order,
        Coming,
        Pausing,
        Going
    }
}
