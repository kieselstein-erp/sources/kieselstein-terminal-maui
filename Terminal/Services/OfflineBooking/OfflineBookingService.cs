﻿using KieselsteinErp.Terminal.Converter;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services.Database;
using KieselsteinErp.Terminal.Services.Database.Models;
using KieselsteinErp.Terminal.Services.StateMachine;
using KieselsteinErp.Terminal.Services.User;
using KieselsteinErp.Terminal.Services.Worktime;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.OfflineBooking
{
  public class OfflineBookingService : IOfflineBookingService
  {
    public event EventHandler<bool> IsBookingOfflineItems;

    private readonly IUserService _userService;
    private readonly IDatabaseService _databaseService;
    private readonly IMessageService _messageService;
    private readonly IWorktimeService _workTimeService;
    private bool _isOnline = true;

    public OfflineBookingService(IUserService userService, IDatabaseService databaseService, IMessageService messageService, IWorktimeService worktimeService)
    {
      _userService = userService;
      _databaseService = databaseService;
      _messageService = messageService;
      _workTimeService = worktimeService;
      _messageService = messageService;

      _userService.OnLogonChanged += (s, e) =>
      {
        _isOnline = e;
        if (e)
          DoOfflineBookings();
      };
      if (!string.IsNullOrEmpty(_userService.Userid) && _isOnline)
        DoOfflineBookings();

    }

    private async void DoOfflineBookings()
    {
      //Get Bookings
      List<BookingDBItem> bookingDBItems = await _databaseService.getBookingItems();
      int failedBookings = await _databaseService.getFailedBookings();
      if (bookingDBItems.Count == 0)
      {
        //Failed message
        return;
      }
      //Event
      if (IsBookingOfflineItems != null)
        IsBookingOfflineItems(this, true);
      //Book
      int failed = 0;
      foreach (BookingDBItem bookingItem in bookingDBItems)
      {
        BookingResult bookingResult;
        switch ((OfflineBookingType)bookingItem.BookingType)
        {
          case OfflineBookingType.Coming:
          case OfflineBookingType.Pausing:
          case OfflineBookingType.Going:
            WorkTimeEvent workTimeEvent = new WorkTimeEvent( EnumConverter.GetAs<WorkTimeEventType, OfflineBookingType>((OfflineBookingType)bookingItem.BookingType));
            bookingResult = await _workTimeService.BookWorktime(workTimeEvent, bookingItem.StaffIdentityCnr, bookingItem.Timestamp);
            if (!bookingResult.Success) failed += 1;
            break;
          default:
            bookingResult = await _workTimeService.BookWorktime(null, bookingItem.Barcode, bookingItem.MachineOverwrite, bookingItem.StaffIdentityCnr, bookingItem.Timestamp);
            if (!bookingResult.Success) failed += 1;
            break;
        }
        //Delete succeeded and modify failed DB entries
        if (bookingResult.Success)
          await _databaseService.DeleteItemAsync(bookingItem);
        else
        {
          BookingDBItem errorItem = new BookingDBItem(bookingItem) { Failed = true, FailedMessage = bookingResult.Message };
          await _databaseService.InsertOrReplaceItemAsync(errorItem);
        }
      }
      //Finished
      int bookings = bookingDBItems.Count;
      string msg = (failedBookings > 0) ? String.Format(OfflineBookingsSuccessful, bookings - failed, bookings)
          + "\n" + String.Format(FailedOfflineBookings, failedBookings + failed)
          : String.Format(OfflineBookingsSuccessful, bookings - failed, bookings);
      if (failedBookings + failed > 0)
        _messageService.Warn(msg);
      else
        _messageService.Info(msg);
      if (IsBookingOfflineItems != null)
        IsBookingOfflineItems(this, false);
    }
  }
}
