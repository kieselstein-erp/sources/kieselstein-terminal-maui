﻿using CommunityToolkit.Maui.Views;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Popups;
using KieselsteinErp.Terminal.Resources.Localization;
using KieselsteinErp.Terminal.Views;
using System.Diagnostics;
using System.Numerics;
using ZXing;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.Settings
{
  public class SettingsHelper
  {
    static bool _isSettingsActiv = false;

    public static async Task<bool> ShowSettings(string password)
    {
      if (_isSettingsActiv) return false;

      try
      {
        _isSettingsActiv = true;

        bool cancled = false;
        string enteredPassword = string.Empty;
        if (!string.IsNullOrWhiteSpace(password))
        {
          var result = await Application.Current.MainPage.ShowPopupAsync(new PasswordEntryPopup());
          enteredPassword = ((PopupResult)result).Entry;
          cancled = ((PopupResult)result).Canceled;
        }
        if (!cancled)
        {
          if (string.IsNullOrEmpty(password) || (!string.IsNullOrEmpty(enteredPassword) && enteredPassword.Equals(password)))
          {
            SettingsPage page = App.Current.Handler.MauiContext.Services.GetService<SettingsPage>();
            await Shell.Current.Navigation.PushModalAsync(page);
            return true;
          }
          else
            await Application.Current.MainPage.DisplayAlert(AppResources.Error, InvalidPassword, Ok);
        }
        _isSettingsActiv = false;
        return false;
      }
      finally { _isSettingsActiv = false; }
    }

    public static Dictionary<string, object> GetSettingProperties(ISettingsService settingsService)
    {
      Dictionary<string, object> ret = new Dictionary<string, object>();
      // Get the interface we are interested in
      var Interface = settingsService.GetType().GetInterface("ISettingsService");
      if (Interface != null)
      {
        // Get the properties from the interface, instead of our source.
        var propertyList = Interface.GetProperties();
        foreach (var property in propertyList)
        {
          Debug.Print("ISettingsService" + " : " + property.Name + "Value " + property.GetValue(settingsService, null));
          ret.Add(property.Name, property.GetValue(settingsService, null));
        }
      }
      else
        Debug.Print("Warning: Interface does not belong to object.");

      return ret;
    }

    internal static string[] SetSettingProperties(ISettingsService settingsService, Dictionary<string, object> settings)
    {
      List<string> errors = [];
      var Interface = settingsService.GetType().GetInterface("ISettingsService");
      if (Interface != null)
      {
        // Get the properties from the interface, instead of our source.
        var propertyList = Interface.GetProperties();
        foreach (var property in propertyList)
        {
          if (settings.ContainsKey(property.Name))
          {
            try
            {
              if (property.PropertyType == typeof(Size))
              {
                if (Size.TryParse((string)settings[property.Name], out Size size))
                  property.SetValue(settingsService, size);
              }
              else if (property.PropertyType == typeof(Point))
              {
                if (Point.TryParse((string)settings[property.Name], out Point point))
                  property.SetValue(settingsService, point);
              }
              else if (property.PropertyType == typeof(Int32) && settings[property.Name].GetType() == typeof(Int64))
              {
                int? result = Convert.ToInt32(settings[property.Name]);
                property.SetValue(settingsService, result);

              }
              else if (property.PropertyType == typeof(ISettingsService.EStaffSortedBy) && settings[property.Name].GetType() == typeof(Int64))
              {
                property.SetValue(settingsService, Enum.Parse(typeof(ISettingsService.EStaffSortedBy), settings[property.Name].ToString()));
              }
              else
                property.SetValue(settingsService, settings[property.Name]);
            } 
            catch (Exception ex)
            {
              errors.Add(string.Format("Set Property {0} failed: {1}", property.Name, ex.Message));
            }
          }
        }
      }
      else
        errors.Add("Warning: Interface does not belong to object.");

      return errors.ToArray();
    }
  }
}
