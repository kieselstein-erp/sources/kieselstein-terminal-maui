﻿namespace KieselsteinErp.Terminal.Services.Settings
{
  public interface ISettingsService
  {
    string BarcodeReaderSerialPort { get; set; }
    string BarcodeReaderType { get; set; }
    string NfcReaderSerialPort { get; set; }
    string NfcReaderType { get; set; }
    bool NfcReaderEnabled { get; set; }
    string ServerUrl { get; set; }
    string LogonLocale { get; set; }
    string LogonMandant { get; set; }
    bool CheckInPage { get; set; }
    bool IsSaldoVisible { get; set; }
    string SettingsPassword { get; set; }
    bool IsFullScreen { get; set; }
    Size WindowSize { get; set; }
    string IdentifierDeliveryNote { get; set; } 
    bool BookDeliveryBadAmount { get; set; }
    bool IsPieceVisible { get; set; }
    bool IsSizable { get; set; }
    Point WindowPosition { get; set; }
    string BluetoothDeviceAddress { get; set; }
    string BluetoothDeviceName { get; set; }
    bool DeviceAssemblyDelivery { get; set; }
    public bool IsRequestsVisible { get; set; }
    public bool IsPaperlessManufacturing { get; set; }
    public int? ManufacturingGroupId { get; set; }
    public int? MachineGroupId { get; set; }
    public int? MachineId { get; set; }
    public int RefreshIntervallPaperlessManufacturing { get; set; }
    public int DaysForPlanning {  get; set; }
    public int PaperlessManufacturingRequestLimit { get; set; }
    public bool IsNfcReverse { get; set; }
    public bool AllowItemBarcodeWithoutPrefix { get; set; }
    public bool AllowSickRequest {  get; set; }
    public bool IsMachineVisible { get; set; }

    public enum EStaffSortedBy
    {
      Name,
      FirstName,
      Name_FirstName,
      StaffNumber
    }

    public EStaffSortedBy StaffSortedBy { get; set; }
  }
}