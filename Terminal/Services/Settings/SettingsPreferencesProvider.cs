﻿using KieselsteinErp.Terminal.Converter;
using static KieselsteinErp.Terminal.Services.Settings.ISettingsService;

namespace KieselsteinErp.Terminal.Services.Settings
{
  public sealed class SettingsPreferencesProvider : ISettingsService
  {
    private const string IdServerUrl = "ServerUrl";
    private const string ServerUrlDefault = "http://localhost:8080";

    private const string IdNfcReaderType = "NfcReaderType";
    private const string NfcReaderTypeDefault = "PROMAG_PCR300";

    private const string IdNfcReaderEnabled = "NfcReaderEnabled";
    private const bool NfcReaderEnabledDefault = false;

    private const string IdNfcReaderSerialPort = "NfcReaderSerialPort";
    private const string NfcReaderSerialPortDefault = "";

    private const string IdBarcodeReaderType = "BarcodeReaderType";
    private const string BarcodeReaderTypeDefault = "Barcode_1D";

    private const string IdBarcodeReaderSerialPort = "BarcodeReaderSerialPort";
    private const string BarcodeReaderSerialPortDefault = "";

    private const string IdLogonLocale = "LogonLocale";
    private const string LogonLocaleDefault = "deAT";

    private const string IdLogonMandant = "LogonMandant";
    private const string LogonMandantDefault = "001";

    private const string IdCheckInPage = "CheckInPage";
    private const bool CheckInPageDefault = false;

    private const string IdSaldoVisible = "SaldoVisible";
    private const bool SaldoVisibleDefault = false;

    private const string IdSettingsPassword = "SettingsPassword";
    private const string SettingsPasswordDefault = "";

    private const string IdIsFullScreen = "FullScreen";
    private const bool FullScreenDefault = false;

    private const string IdWindowWidth = "WindowWidth";
    private const double WindowWidthDefault = 900;
    private const string IdWindowHeight = "WindowHeight";
    private const double WindowHeightDefault = 600;

    private const string IdIdentifierDeliveryNote = "IdentifierDeliveryNote";
    private const string IdentifierDeliveryNoteDefault = "";

    private const string IdBookDeliveryBadAmount = "BookDeliveryBadAmount";
    private const bool BookDeliveryBadAmountDefault = false;

    private const string IdPieceVisible = "PieceVisible";
    private const bool PieceVisibleDefault = false;

    private const string IdIsSizable = "Sizable";
    private const bool IsSizableDefault = false;

    private const string IdWindowX = "WindowX";
    private const double WindowXDefault = 0;

    private const string IdWindowY = "WindowY";
    private const double WindowYDefault = 0;


    private const string IdBluetoothDeviceAddress = "BluetoothDeviceAddress";
    private const string BluetoothDeviceAddressDefault = "";

    private const string IdBluetoothDeviceName = "BluetoothDeviceName";
    private const string BluetoothDeviceNameDefault = "";

    private const string IdDeviceAssemblyDelivery = "DeviceAssemblyDelivery";
    private const bool DeviceAssemblyDeliveryDefault = false;

    private const string IdRequestsVisible = "RequestsVisible";
    private const bool RequestsVisibleDefault = false;

    private const string IdPaperlessManufacturing = "PaperlessManufacturing";
    private const bool PaperlessManufacturingDefault = false;

    private const string IdManufacturingGroupId = "ManufacturingGroupId";
    private const string ManufacturingGroupIdDefault = null;

    private const string IdMachineGroupId = "MachineGroupId";
    private const string MachineGroupIdDefault = null;

    private const string IdMachineId = "MachineId";
    private const string MachineIdDefault = null;

    private const string IdRefreshIntervalPaperlessManufacturing = "RefreshIntervalPaperlessManufacturing";
    private const int RefreshIntervalPaperlessManufacturingDefault = 0;

    private const string IdDaysForPlanning = "DaysForPlanning";
    private const int DaysForPlanningDefault = 14;

    private const string IdPaperlessManufacturingRequestLimit = "PaperlessManufacturingRequestLimit";
    private const int PaperlessManufacturingRequestLimitDefault = 200;

    private const string IdIsNfcReverse = "NfcReverse";
    private const bool IsNfcReverseDefault = false;

    private const string IdAllowItemBarcodeWithoutPrefix = "ItemBarcodeWithoutPrefix";
    private const bool AllowItemBarcodeWithoutPrefixDefault = false;

    private const string IdAllowSickRequest = "AllowSickRequest";
    private const bool AllowSickRequestDefault = true;

    private const string IdStaffSortedBy = "StaffSortedBy";
    private const EStaffSortedBy StaffSortedByDefault = EStaffSortedBy.Name;

    private const string IdIsMachineVisible = "MachineVisible";
    private const bool IsMachineVisibleDefault = false;
    public string ServerUrl
    {
      get => Preferences.Get(IdServerUrl, ServerUrlDefault);
      set => Preferences.Set(IdServerUrl, value);
    }

    /// <summary>
    /// Windows Platform only
    /// </summary>
    public string NfcReaderType
    {
      get => Preferences.Get(IdNfcReaderType, NfcReaderTypeDefault);
      set => Preferences.Set(IdNfcReaderType, value);
    }
    /// <summary>
    /// Windows Platform only
    /// </summary>
    public string NfcReaderSerialPort
    {
      get => Preferences.Get(IdNfcReaderSerialPort, NfcReaderSerialPortDefault);
      set => Preferences.Set(IdNfcReaderSerialPort, value);
    }

    /// <summary>
    /// Android Platform only
    /// </summary>
    public bool NfcReaderEnabled
    {
      get => Preferences.Get(IdNfcReaderEnabled, NfcReaderEnabledDefault);
      set => Preferences.Set(IdNfcReaderEnabled, value);
    }
    /// <summary>
    /// Windows Platform only
    /// </summary>
    public string BarcodeReaderType
    {
      get => Preferences.Get(IdBarcodeReaderType, BarcodeReaderTypeDefault);
      set => Preferences.Set(IdBarcodeReaderType, value);
    }

    /// <summary>
    /// Windows Platform only
    /// </summary>
    public string BarcodeReaderSerialPort
    {
      get => Preferences.Get(IdBarcodeReaderSerialPort, BarcodeReaderSerialPortDefault);
      set => Preferences.Set(IdBarcodeReaderSerialPort, value);
    }
    public string LogonLocale
    {
      get => Preferences.Get(IdLogonLocale, LogonLocaleDefault);
      set => Preferences.Set(IdLogonLocale, value);
    }

    public string LogonMandant
    {
      get => Preferences.Get(IdLogonMandant, LogonMandantDefault);
      set => Preferences.Set(IdLogonMandant, value);
    }

    public bool CheckInPage
    {
      get => Preferences.Get(IdCheckInPage, CheckInPageDefault);
      set => Preferences.Set(IdCheckInPage, value);
    }

    public bool IsSaldoVisible
    {
      get => Preferences.Get(IdSaldoVisible, SaldoVisibleDefault);
      set => Preferences.Set(IdSaldoVisible, value);
    }

    public string SettingsPassword
    {
      get => Preferences.Get(IdSettingsPassword, SettingsPasswordDefault);
      set => Preferences.Set(IdSettingsPassword, value);
    }

    /// <summary>
    /// Windows Platform only
    /// </summary>
    public bool IsFullScreen
    {
      get => Preferences.Get(IdIsFullScreen, FullScreenDefault);
      set => Preferences.Set(IdIsFullScreen, value);
    }

    /// <summary>
    /// Windows Platform only
    /// </summary>
    public Size WindowSize
    {
      get => new Size(Preferences.Get(IdWindowWidth, WindowWidthDefault), Preferences.Get(IdWindowHeight, WindowHeightDefault));
      set
      {
        Preferences.Set(IdWindowWidth, value.Width >= WindowWidthDefault ? value.Width : WindowWidthDefault);
        Preferences.Set(IdWindowHeight, value.Height >= WindowHeightDefault ? value.Height : WindowHeightDefault);
      }
    }

    /// <summary>
    /// Windows Platform only
    /// </summary>
    public Size WindowSizeMin
    {
      get => new Size(WindowWidthDefault, WindowHeightDefault);
    }

    /// <summary>
    /// Windows Platform only
    /// </summary>
    public Point WindowPosition
    {
      get => new Point(Preferences.Get(IdWindowX, WindowXDefault), Preferences.Get(IdWindowY, WindowYDefault));
      set
      {
        Preferences.Set(IdWindowX, value.X);
        Preferences.Set(IdWindowY, value.Y);
      }
    }

    /// <summary>
    /// Windows Platform only
    /// </summary>
    public bool IsSizable
    {
      get => Preferences.Get(IdIsSizable, IsSizableDefault);
      set => Preferences.Set(IdIsSizable, value);
    }

    public string IdentifierDeliveryNote
    {
      get => Preferences.Get(IdIdentifierDeliveryNote, IdentifierDeliveryNoteDefault);
      set => Preferences.Set(IdIdentifierDeliveryNote, value);
    }

    public bool BookDeliveryBadAmount
    {
      get => Preferences.Get(IdBookDeliveryBadAmount, BookDeliveryBadAmountDefault);
      set => Preferences.Set(IdBookDeliveryBadAmount, value);
    }

    public bool IsPieceVisible
    {
      get => Preferences.Get(IdPieceVisible, PieceVisibleDefault);
      set => Preferences.Set(IdPieceVisible, value);
    }


    public string BluetoothDeviceAddress
    {
      get => Preferences.Get(IdBluetoothDeviceAddress, BluetoothDeviceAddressDefault);
      set => Preferences.Set(IdBluetoothDeviceAddress, value);
    }

    public string BluetoothDeviceName
    {
      get => Preferences.Get(IdBluetoothDeviceName, BluetoothDeviceNameDefault);
      set => Preferences.Set(IdBluetoothDeviceName, value);
    }

    public bool DeviceAssemblyDelivery
    {
      get => Preferences.Get(IdDeviceAssemblyDelivery, DeviceAssemblyDeliveryDefault);
      set => Preferences.Set(IdDeviceAssemblyDelivery, value);
    }

    public bool IsRequestsVisible
    {
      get => Preferences.Get(IdRequestsVisible, RequestsVisibleDefault);
      set => Preferences.Set(IdRequestsVisible, value);
    }

    public bool IsPaperlessManufacturing
    {
      get => Preferences.Get(IdPaperlessManufacturing, PaperlessManufacturingDefault);
      set => Preferences.Set(IdPaperlessManufacturing, value);
    }
    public int? ManufacturingGroupId
    {
      get => Preferences.Get(IdManufacturingGroupId, ManufacturingGroupIdDefault) == null ? (int?)null : int.Parse(Preferences.Get(IdManufacturingGroupId, ManufacturingGroupIdDefault));
      set => Preferences.Set(IdManufacturingGroupId, value?.ToString());
    }
    public int? MachineGroupId
    {
      get => Preferences.Get(IdMachineGroupId, MachineGroupIdDefault) == null ? (int?)null : int.Parse(Preferences.Get(IdMachineGroupId, MachineGroupIdDefault));
      set => Preferences.Set(IdMachineGroupId, value?.ToString());
    }
    public int? MachineId
    {
      get => Preferences.Get(IdMachineId, MachineIdDefault) == null ? (int?)null : int.Parse(Preferences.Get(IdMachineId, MachineIdDefault));
      set => Preferences.Set(IdMachineId, value?.ToString());
    }
    public int RefreshIntervallPaperlessManufacturing
    {
      get => Preferences.Get(IdRefreshIntervalPaperlessManufacturing, RefreshIntervalPaperlessManufacturingDefault);
      set => Preferences.Set(IdRefreshIntervalPaperlessManufacturing, value);
    }

    public int DaysForPlanning
    {
      get => Preferences.Get(IdDaysForPlanning, DaysForPlanningDefault);
      set => Preferences.Set(IdDaysForPlanning, value);
    }

    public int PaperlessManufacturingRequestLimit 
    {
      get => Preferences.Get(IdPaperlessManufacturingRequestLimit, PaperlessManufacturingRequestLimitDefault); 
      set => Preferences.Set(IdPaperlessManufacturingRequestLimit, value); 
    }


    public bool IsNfcReverse
    {
      get => Preferences.Get(IdIsNfcReverse, IsNfcReverseDefault);
      set => Preferences.Set(IdIsNfcReverse, value);
    }

    public bool AllowItemBarcodeWithoutPrefix
    {
      get => Preferences.Get(IdAllowItemBarcodeWithoutPrefix, AllowItemBarcodeWithoutPrefixDefault);
      set => Preferences.Set(IdAllowItemBarcodeWithoutPrefix , value);
    }

    public bool AllowSickRequest
    {
      get => Preferences.Get(IdAllowSickRequest, AllowSickRequestDefault);
      set => Preferences.Set(IdAllowSickRequest , value);
    }

    public EStaffSortedBy StaffSortedBy
    {
      get => EnumConverter.GetAs<EStaffSortedBy>(Preferences.Get(IdStaffSortedBy, StaffSortedByDefault.ToString()));
      set => Preferences.Set(IdStaffSortedBy, value.ToString());
    }

    public bool IsMachineVisible
    {
      get => Preferences.Get(IdIsMachineVisible, IsMachineVisibleDefault);
      set => Preferences.Set(IdIsMachineVisible, value);
    }
  }
}