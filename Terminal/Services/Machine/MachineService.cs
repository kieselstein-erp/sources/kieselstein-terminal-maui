﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Services.Database;
using KieselsteinErp.Terminal.Services.User;

namespace KieselsteinErp.Terminal.Services.Machine
{
  public class MachineService : IMachineService
	{
		private MachineEntryList _machineEntries;
		private readonly IMachineRestService _machineRestService;
		private readonly IUserService _userService;
		private readonly IDatabaseService _databaseService;
		private bool _isOnline;

		public MachineService(IMachineRestService machineRestService, IUserService userService, IDatabaseService databaseService)
		{
			_machineRestService = machineRestService;
			_userService = userService;
			_databaseService = databaseService;

			_userService.OnLogonChanged += (s, e) => { _isOnline = e; };
		}
		public int GetMachineIdByNumber(string machineIdNumber)
		{
			if (_machineEntries == null)
				FetchMachines();

			var machine = _machineEntries.entries.Find(b => b.identificationNumber == machineIdNumber);
			if (machine == null)
			{
				FetchMachines();
				machine = _machineEntries.entries.Find(b => b.identificationNumber == machineIdNumber);
			}
			if (machine == null)
				return 0;
			return machine.id;
		}

		private void FetchMachines()
		{
			if (_isOnline)
			{
				_machineEntries = _machineRestService.GetMachineEntryList(_userService.Userid);
				_databaseService.SaveMachinesOffline(_machineEntries);
			}
			else
			{
				var task = Task.Run(async () => await _databaseService.GetMachinesOffline());
				var newEntries = task.GetAwaiter().GetResult();
				_machineEntries = newEntries;
			}
		}

    public async Task<List<MachineEntry>> GetMachineEntriesForPersonAsync(bool onlyStarted, int personalIdStarter)
    {
      PlanningView planningview = await _machineRestService.GetPlanningViewAsync();
	  List<MachineEntry> machineEntries = planningview.machineList?.entries?.ToList();

	  if (machineEntries == null)
		return null;
	  else
	  {
		List<MachineEntry> filtered = null;
		if (personalIdStarter != 0)
		  filtered = machineEntries.Where(e => e.personalIdStarter == personalIdStarter).ToList();
		  
		if (onlyStarted)
		  if (filtered == null)
			filtered = machineEntries.Where(o => o.starttime > 0).ToList();
		  else
			filtered = filtered.Where(o => o.starttime > 0).ToList();

		if (filtered != null)
		  return filtered;
		else
		  return machineEntries;
	  }
    }

    public async Task<List<MachineGroupEntry>> GetMachineGroupEntriesAsync(int? productiongroupId = null)
    {
	  return await _machineRestService.GetMachineGroupEntriesAsync(productiongroupId);
    }

    public async Task<List<MachineGroupEntry>> GetMachineGroupEntriesForPlanningAsync(int? productiongroupId = null)
    {
      return await _machineRestService.GetMachineGroupEntriesAsync(productiongroupId, true);
    }


    public async Task<List<MachineEntry>> GetMachineEntriesAsync()
    {
      return await _machineRestService.GetMachineEntriesAsync();
    }

	public async Task<List<MachineEntry>> GetMachineEntriesForPlanningAsync()
	{
	  return await _machineRestService.GetMachineEntriesAsync(true);
	}

    public async Task<MachineEntry> GetMachineEntryAsync(int id)
    {
	  List<MachineEntry> entries = await GetMachineEntriesAsync();
	  if (entries == null) return null;
	  return entries.Find(e => e.id == id);
    }

    public async Task<MachineGroupEntry> GetMachineGroupEntryAsync(int id)
    {
      List<MachineGroupEntry> entries = await GetMachineGroupEntriesAsync();
      if (entries == null) return null;
      return entries.Find(e => e.id == id);
    }

    public async Task<List<MachineEntry>> GetMachineEntriesForMachineAsync(bool onlyStarted, int? productiongroupId, int machineId)
    {
      PlanningView planningview = await _machineRestService.GetPlanningViewAsync(productiongroupId);
      List<MachineEntry> machineEntries = planningview.machineList?.entries?.ToList();

      if (machineEntries == null)
        return [];
      else
      {
        List<MachineEntry> filtered = null;
        if (machineId != 0)
          filtered = machineEntries.Where(e => e.id == machineId).ToList();

        if (onlyStarted)
          if (filtered == null)
            filtered = machineEntries.Where(o => o.starttime > 0).ToList();
          else
            filtered = filtered.Where(o => o.starttime > 0).ToList();

        if (filtered != null)
          return filtered;
        else
          return machineEntries;
      }
    }

    public async Task<PlanningView> GetPlanningViewAsync(int? productiongroupId = null, int? days = null, int limit = 50)
    {
	  return await _machineRestService.GetPlanningViewAsync(productiongroupId, days, limit);
    }

    public async Task<List<MachineEntry>> GetMachineEntriesGostShiftAsync()
    {
      return await _machineRestService.GetMachineEntriesGostShiftAsync();
    }
  }
}
