﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.User;
using KieselsteinErp.Terminal.Util;
using Microsoft.Extensions.Logging;
using RestSharp;
using System.Diagnostics;

namespace KieselsteinErp.Terminal.Services.Machine
{
  public class MachineRestService : RestBase, IMachineRestService
  {
    private readonly ILogger _logger;
    private readonly ISettingsService _settingsService;
    private readonly IUserService _userService;

    public MachineRestService(ILogger<MachineRestService> logger, ISettingsService settingsService, IUserService userService) : base(settingsService, userService)
    {
      _logger = logger;
      _settingsService = settingsService;
      _userService = userService;
    }

    public async Task<List<MachineEntry>> GetMachineEntriesAsync(bool filterPlanning = false)
    {
      var request = new RestRequest("machine")
        .AddQueryParameter("filter_planningview", filterPlanning)
        .AddQueryParameter("userid", Token());

      try
      {
        var response = await Client.GetAsync<MachineEntryList>(request);
        return response.entries;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return [];
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return [];
      }
    }

    public async Task<List<MachineEntry>> GetMachineEntriesGostShiftAsync()
    {
      var request = new RestRequest("machine/ghostshift")
        .AddQueryParameter("userid", Token());
      try
      {
        var response = await Client.GetAsync<MachineEntryList>(request);
        return response.entries;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return [];
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return [];
      }
    }

    public MachineEntryList GetMachineEntryList(string userId)
    {
      var request = new RestRequest("machine")
          .AddQueryParameter("userid", Token());
      try
      {
        var response = Client.Get<MachineEntryList>(request);
        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return new MachineEntryList() { entries = [] };
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return new MachineEntryList() { entries = [] };
      }
    }

    public async Task<List<MachineGroupEntry>> GetMachineGroupEntriesAsync(int? productiongroupId = null, bool filterPlanning = false)
    {
      var request = new RestRequest("machine/groups")
        .AddQueryParameter("filter_planningview", filterPlanning )
        .AddQueryParameter("userid", Token());

      if (productiongroupId != null)
        request.AddQueryParameter("filter_productiongroupid", (int)productiongroupId);
      
      try
      {
        var response = await Client.GetAsync<MachineGroupEntryList>(request);
        return response.entries;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return [];
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return [];
      }
    }

    /// <summary>
    /// Parameter für Tage (days) und max. Anzahl (limit)
    /// </summary>
    /// <param name="productiongroupId"></param>
    /// <returns></returns>

    public async Task<PlanningView> GetPlanningViewAsync(int? productiongroupId = null, int? days = null, int limit = 50)
    {
      var request = new RestRequest("machine/planningview")
          .AddQueryParameter("startIndex", 0)
          .AddQueryParameter("limit", limit)
          .AddQueryParameter("userid", Token());

      //          .AddQueryParameter("filter_startdate", true)
      //
      // Attention:
      // If "filter_startdate" is True, the parameter "dateMs" must also be set to the desired start time.
      // If this is not set, the time of the query is assumed and no worksteps in the past are displayed.
      //
      // If "filter_startdate" is not specified or is False, all open worksteps in the past are taken into account.
      // 

      if (productiongroupId != null)
        request.AddQueryParameter("filter_productiongroupid", (int)productiongroupId);
      if (days != null)
        request.AddQueryParameter("days", (int)days);
      
      try
      {
        Stopwatch sw = new Stopwatch();
        sw.Start();
        var response = await ClientLTO.GetAsync<PlanningView>(request);
        sw.Stop();
        Debug.Print($"GetplanningView {sw.ElapsedMilliseconds / 1000} s");
        
        if (sw.ElapsedMilliseconds > 10000)
          _logger.LogWarning("PlanningViewResponseTime > 10s");
        if (response != null && response.openWorkList != null && response.openWorkList.entries.Count >= limit)
          _logger.LogWarning(string.Format("PlannningViewResponse OpenWorkEntries count > {0}", limit));
        
        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return new PlanningView();
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return new PlanningView();
      }
    }
  }
}
