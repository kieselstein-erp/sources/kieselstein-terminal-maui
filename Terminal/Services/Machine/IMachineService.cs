﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Services.Machine
{
  public interface IMachineService
  {
    int GetMachineIdByNumber(string machineIdNumber);
    Task<List<MachineEntry>> GetMachineEntriesForPersonAsync(bool onlyStarted, int personalIdStarter);
    Task<List<MachineEntry>> GetMachineEntriesForMachineAsync(bool onlyStarted, int? productiongroupId, int machineId);
    Task<List<MachineGroupEntry>> GetMachineGroupEntriesAsync(int? manufacturingGroupId = null);
    Task<List<MachineGroupEntry>> GetMachineGroupEntriesForPlanningAsync(int? productiongroupId = null);
    Task<MachineGroupEntry> GetMachineGroupEntryAsync(int id);
    Task<List<MachineEntry>> GetMachineEntriesAsync();
    Task<List<MachineEntry>> GetMachineEntriesForPlanningAsync();
    Task<MachineEntry> GetMachineEntryAsync(int id);
    Task<PlanningView> GetPlanningViewAsync(int? productiongroupId = null, int? days = null, int limit = 50);
    Task<List<MachineEntry>> GetMachineEntriesGostShiftAsync();
  }
}
