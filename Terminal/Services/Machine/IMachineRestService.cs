﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Services.Machine
{
  public interface IMachineRestService
  {
    MachineEntryList GetMachineEntryList(string userId);
    Task<PlanningView> GetPlanningViewAsync(int? productiongroupId = null, int? days = null, int limit = 50);
    Task<List<MachineGroupEntry>> GetMachineGroupEntriesAsync(int? productiongroupId = null, bool filterPlanning = false);
    Task<List<MachineEntry>> GetMachineEntriesAsync(bool filterPlanning = false);
    Task<List<MachineEntry>> GetMachineEntriesGostShiftAsync();
  }
}
