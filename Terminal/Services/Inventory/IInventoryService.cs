﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;

namespace KieselsteinErp.Terminal.Services.Inventory
{
  public interface IInventoryService
  {
    Task<InventoriesResult> GetInventories();
    Task<BookingResult> BookInventoryEntry(int inventoryid, InventoryDataEntry inventoryDataEntry, bool allowLargeDifference);
    Task<BookingResult> ChangeInventoryEntry(int inventoryid, InventoryDataEntry inventoryDataEntry, bool changeAmountTo);
  }
}
