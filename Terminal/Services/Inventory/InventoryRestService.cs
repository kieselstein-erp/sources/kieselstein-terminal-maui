﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.User;
using RestSharp;
using System.Net;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.Inventory
{
  public class InventoryRestService : RestBase, IInventoryService
  {
    public InventoryRestService(ISettingsService settingsService, IUserService userService) : base(settingsService, userService, false)
    {
    }

    public async Task<BookingResult> BookInventoryEntry(int inventoryid, InventoryDataEntry inventoryDataEntry, bool allowLargeDifference)
    {
      string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(inventoryDataEntry);
      var request = new RestRequest($"inventory/{inventoryid}/entry", Method.Post)
        .AddJsonBody(strJson)
        .AddHeader("Content-Type", "application/json")
        .AddQueryParameter("userid", Token())
        .AddQueryParameter("largeDifference", allowLargeDifference);
      var response = await Client.ExecutePostAsync(request);

      if (response.IsSuccessful)
      {
        return new BookingResult(true, null);
      }
      else
      {
        switch (response.StatusCode)
        {
          case HttpStatusCode.BadRequest:
            {
              if (GetHeaderValue(response, HV_ERROR_CODE) == HV_ERROR_CODE_VALIDATION_FAILED.ToString())
                if (GetHeaderValue(response, HV_ERROR_KEY) == "amount")
                  switch (GetHeaderValue(response, HV_ERROR_VALUE))
                  {
                    case "negativeInventoryAmount":
                      return new BookingResult(false, NegativeStockLevelAtTheTimeOfInventoryQuantityIsNotPermitted);

                    case "largeDifference":
                      return new BookingResult(false, DeviationFromActualQuantityToTargetQuantityTooLargeNMakeTheBookingAnyway, response.StatusCode);
                  }

              return new BookingResult(false, BuildMessageFromHeader(response));
            }

          case HttpStatusCode.NotFound:
            {
              if (GetHeaderValue(response, HV_ERROR_CODE) == HV_ERROR_CODE_UNKNOWN_ENTITY.ToString())
                if (GetHeaderValue(response, HV_ERROR_KEY) == "itemCnr")
                  return new BookingResult(false, string.Format(Item0NotFound, GetHeaderValue(response, HV_ERROR_VALUE)));

              return new BookingResult(false, BuildMessageFromHeader(response));
            }

          case HttpStatusCode.UnprocessableEntity:
            {
              return new BookingResult(false,
                string.Format(ThereIsAlreadyABookingForTheItemNQuantityAlreadyRecorded0NCurrentQuantity1, 
                  GetHeaderValue(response, HV_ERROR_VALUE), 
                  inventoryDataEntry.amount.ToString()),
                response.StatusCode);
            }

          default:
            {
              if (response.Headers?.Count > 0)
              {
                string msg = BuildMessageFromHeader(response);

                if (!string.IsNullOrEmpty(msg))
                {
                  return new BookingResult(false, msg);
                }
              }
              return new BookingResult(false, string.Format(Error_Message_Action, Booking) + string.Format(RequestReturnedStatuscode0, response.StatusCode));
            }
        }
      }
    }

    public async Task<BookingResult> ChangeInventoryEntry(int inventoryid, InventoryDataEntry inventoryDataEntry, bool changeAmountTo)
    {
      string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(inventoryDataEntry);
      var request = new RestRequest($"inventory/{inventoryid}/entry", Method.Put)
        .AddJsonBody(strJson)
        .AddHeader("Content-Type", "application/json")
        .AddQueryParameter("userid", Token())
        .AddQueryParameter("changeAmountTo", changeAmountTo);
      var response = await Client.ExecutePutAsync(request);

      if (response.IsSuccessful)
      {
        return new BookingResult(true, null);
      }
      else
      {
        if (response.Headers?.Count > 0)
        {
          string msg = BuildMessageFromHeader(response);

          if (!string.IsNullOrEmpty(msg))
          {
            return new BookingResult(false, msg);
          }
        }
        return new BookingResult(false, string.Format(Error_Message_Action, Booking) + string.Format(RequestReturnedStatuscode0, response.StatusCode));
      }
    }

    public async Task<InventoriesResult> GetInventories()
    {
      var request = new RestRequest("inventory").AddQueryParameter("userid", Token());
      //request.AddHeader("Content-Type", "application/json");
      try
      {
        var response = await Client.ExecuteGetAsync<List<InventoryEntry>>(request);
        if (response.IsSuccessful)
          return new InventoriesResult(true, response.Data);
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
            {
              return new InventoriesResult(false, msg);
            }
          }
          return new InventoriesResult(false, string.Format(Error_Message_Action, Resources.Localization.AppResources.GetInventories) + " " + string.Format(RequestReturnedStatuscode0, response.StatusCode));
        }
      }
      catch (HttpRequestException ex)
      {
        System.Diagnostics.Debug.WriteLine(ex.ToString());
        // eventuell Statuscode auswerten?
        return new InventoriesResult(false, ex.Message);
      }
      catch (Exception ex)
      {
        System.Diagnostics.Debug.WriteLine(ex.ToString());
        return new InventoriesResult(false, ex.Message);
      }
    }
  }
}
