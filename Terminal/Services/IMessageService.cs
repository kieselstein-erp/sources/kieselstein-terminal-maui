﻿using CommunityToolkit.Maui.Core;

namespace KieselsteinErp.Terminal.Services
{
  public interface IMessageService
  {
    void ShowToast(string text, ToastDuration duration);

    void Info(string message);

    void Warn(string message);

    void Error(string message);

  }
}
