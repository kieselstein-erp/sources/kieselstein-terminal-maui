﻿using LocalizationResourceManager.Maui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KieselsteinErp.Terminal.Resources.Localization;
using Microsoft.Extensions.Logging;

namespace KieselsteinErp.Terminal.Services
{
    public class Translator : ITranslator
    {
        private readonly ILocalizationResourceManager _localizationResourceManager;
        private readonly ILogger<Translator> _logger;

        public Translator(ILocalizationResourceManager localizationResourceManager, ILogger<Translator> logger)
        {
            _localizationResourceManager = localizationResourceManager;
            _logger = logger;
        }

        public string Translated(string message)
        {
            ArgumentNullException.ThrowIfNull(message, nameof(message));

            var currentCulture = _localizationResourceManager.CurrentCulture;
            var translated = AppResources.ResourceManager.GetString(message, currentCulture);

            if (translated == null)
            {
                _logger.LogWarning("No translation for message '{0}' into '{1}'", message, currentCulture);
                return message;
            }

            return translated ;
        }

        public string Translated(object message)
        {
            return Translated(message?.ToString());
        }
    }
}
