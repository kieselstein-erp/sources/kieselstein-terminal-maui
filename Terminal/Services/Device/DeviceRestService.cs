﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.User;
using Microsoft.Extensions.Logging;
using RestSharp;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.Device
{
  public class DeviceRestService : RestBase, IDeviceRestService
  {
    private readonly ILogger _logger;

    public DeviceRestService(ILogger<DeviceRestService> logger, ISettingsService settingsService, IUserService userService) : base(settingsService, userService)
    {
      _logger = logger;
    }

    public async Task<DeviceConfigEntry> GetDeviceConfigEntryAsync(string deviceType, string deviceCnr, string deviceTag)
    {
      var request = new RestRequest("device/config").AddQueryParameter("userid", Token());
      if (!string.IsNullOrEmpty(deviceType))
        request.AddQueryParameter("devicetype", deviceType);
      if (!string.IsNullOrEmpty(deviceCnr))
        request.AddQueryParameter("devicecnr", deviceCnr);
      if (!string.IsNullOrEmpty(deviceTag))
        request.AddQueryParameter("devicetag", deviceTag);

      try
      {
        var response = await Client.ExecuteGetAsync<DeviceConfigEntry>(request);
        if (response.IsSuccessful)
          return response.Data;
        else
        {
          _logger.LogWarning(string.Format("No device config found for devicecnr {0}", deviceCnr));
          return null;
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<BookingResult> PutDeviceConfigAsync(DeviceConfigEntry deviceConfigEntry)
    {
      try
      {
        string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(deviceConfigEntry);
        var request = new RestRequest("device/config", Method.Put)
          .AddJsonBody(strJson)
          .AddQueryParameter("userid", Token());

        request.AddHeader("Content-Type", "application/json");

        var response = await Client.ExecutePutAsync(request);

        if (response.IsSuccessful)
        {
          return new BookingResult(true, "");
        }
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
            {
              return new BookingResult(false, msg);
            }
          }
          return new BookingResult(false, string.Format(Error_Message_Action, "Put device config") + "Request returned Statuscode " + response.StatusCode);
        }
      }
      catch (HttpRequestException e)
      {
        _logger.LogError(e.Message, e);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
      }
      return new BookingResult(false, string.Format(Error_Message_Action, "Put device config"));
    }
  }
}
