﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;

namespace KieselsteinErp.Terminal.Services.Device
{
  public interface IDeviceRestService
  {
    Task<DeviceConfigEntry> GetDeviceConfigEntryAsync(string deviceType, string deviceCnr, string deviceTag);
    Task<BookingResult> PutDeviceConfigAsync(DeviceConfigEntry deviceConfigEntry);
  }
}
