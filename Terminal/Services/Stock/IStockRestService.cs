﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;

namespace KieselsteinErp.Terminal.Services.Stock
{
  public interface IStockRestService
    {
    Task<StockEntryList> GetStocksAsync();

    Task<StockPlaceEntry> GetStockPlaceByName(int stockid,  string name);
    Task<BookingResult> SetStockPlace(int stockid, StockPlacePostEntry stockPlacePostEntry);
    Task<BookingResult> PlaceRemove(int stockId, int placeId, int itemId);

  }
}
