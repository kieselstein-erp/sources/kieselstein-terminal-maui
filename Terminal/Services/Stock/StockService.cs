﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;

namespace KieselsteinErp.Terminal.Services.Stock
{
  public class StockService : IStockService
	{
		private StockEntryList _stockEntries;
		private readonly IStockRestService _stockRestService;
		public StockService(IStockRestService stockRestService)
		{
			_stockRestService = stockRestService;
		}

		public async Task<StockEntry> GetStockEntryByCnr(string cnr)
		{
			StockEntry stockEntry = _stockEntries.entries.Find(entry => entry.typeCnr == cnr);
			if (stockEntry == null) {
				_stockEntries = await _stockRestService.GetStocksAsync();
				stockEntry = _stockEntries.entries.Find(entry => entry.typeCnr == cnr);
			}
			return stockEntry;
		}

    public Task<StockPlaceEntry> GetStockPlaceEntryByCnr(int stockid, string cnr)
    {
      return _stockRestService.GetStockPlaceByName(stockid, cnr);
    }

    public Task<BookingResult> SetStockPlace(int stockid, StockPlacePostEntry stockPlacePostEntry)
    {
      return _stockRestService.SetStockPlace(stockid, stockPlacePostEntry);
    }

    public Task<BookingResult> PlaceRemove(int stockId, int placeId, int itemId)
    {
      return _stockRestService.PlaceRemove(stockId, placeId, itemId);
    }

    public Task<StockEntryList> GetStockEntries()
    {
      return _stockRestService.GetStocksAsync();
    }
  }

}
