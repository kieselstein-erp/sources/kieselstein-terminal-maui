﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;

namespace KieselsteinErp.Terminal.Services.Stock
{
  public interface IStockService
  {
    Task<StockEntryList> GetStockEntries();
    Task<StockEntry> GetStockEntryByCnr(string cnr);
    Task<StockPlaceEntry> GetStockPlaceEntryByCnr(int stockid, string cnr);
    Task<BookingResult> SetStockPlace(int stockid, StockPlacePostEntry stockPlacePostEntry);
    Task<BookingResult> PlaceRemove(int stockId, int placeId, int itemId);

  }
}
