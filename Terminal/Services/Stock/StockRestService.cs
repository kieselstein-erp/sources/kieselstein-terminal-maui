﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.User;
using Microsoft.Extensions.Logging;
using RestSharp;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.Stock
{
  public class StockRestService : RestBase, IStockRestService
  {
    private readonly ILogger _logger;

    public StockRestService(ILogger<StockRestService> logger, ISettingsService settingsService, IUserService userService) : base(settingsService, userService)
    {
      _logger = logger;
    }

    public async Task<StockPlaceEntry> GetStockPlaceByName(int stockid, string name)
    {
      var request = new RestRequest($"stock/{stockid}/place")
          .AddQueryParameter("stockplacename", name)
          .AddQueryParameter("addStockAmountInfos", true)
          .AddQueryParameter("userid", Token());
      try
      {
        var response = await Client.GetAsync<StockPlaceEntry>(request);
        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<StockEntryList> GetStocksAsync()
    {
      var request = new RestRequest("stock")
          .AddQueryParameter("userid", Token());
      try
      {
        var response = await Client.GetAsync<StockEntryList>(request);
        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<BookingResult> PlaceRemove(int stockId, int placeId, int itemId)
    {
      var request = new RestRequest("stock/{stockid}/place/{placeid}", Method.Delete)
        .AddUrlSegment("stockid", stockId)
        .AddUrlSegment("placeid", placeId)
        .AddQueryParameter("itemid", itemId)
        .AddQueryParameter("userid", Token())
        .AddHeader("Content-Type", "application/json");
      try
      {
        var response = await Client.ExecuteAsync(request);
        if (response.IsSuccessful)
          return new BookingResult(true, null);
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
              return new BookingResult(false, msg);
          }
          return new BookingResult(false, response.ErrorMessage, response.StatusCode);
        }
      }
      catch (HttpRequestException ex)
      {
        return new BookingResult(false, ex.Message, (System.Net.HttpStatusCode)ex.StatusCode);
      }
      catch (Exception ex)
      {
        return new BookingResult(false, ex.Message);
      }
    }

    public async Task<BookingResult> SetStockPlace(int stockid, StockPlacePostEntry stockPlacePostEntry)
    {
      string body = Newtonsoft.Json.JsonConvert.SerializeObject(stockPlacePostEntry);
      var request = new RestRequest($"stock/{stockid}/place", Method.Post)
          .AddQueryParameter("userid", Token())
          .AddHeader("Content-Type", "application/json")
          .AddJsonBody(body);
      try
      {
        var response = await Client.ExecutePostAsync<int>(request);
        if (response.IsSuccessful)
        {
          return new BookingResult(true, response.Data);
        }
        else
        {
          if (response.Headers?.Count > 0)
          {
            if (response.StatusCode == System.Net.HttpStatusCode.NotFound && GetHeaderValue(response, "x-hv-error-code") == "4" && GetHeaderValue(response, "x-hv-error-key") == "name")
              return new BookingResult(false, string.Format(Stockplace0DoesNotExist, stockPlacePostEntry.stockplaceName));
            else
            {
              string msg = BuildMessageFromHeader(response);
              if (!string.IsNullOrEmpty(msg))
              {
                return new BookingResult(false, msg);
              }
            }
          }
          return new BookingResult(false, string.Format(Error_Message_Action, Booking) + string.Format(RequestReturnedStatuscode0, response.StatusCode));
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return new BookingResult(false, string.Format(Error_Message_Action, ex.Message));
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return new BookingResult(false, string.Format(Error_Message_Action, ex.Message));
      }
    }
  }
}
