﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Services.PurchaseOrder
{
  public interface IPurchaseOrderRestService
    {
    public Task<List<PurchaseOrderEntry>> GetPurchaseOrdersAsync(string filterCnr = "", int limit = 200);
    //public Task<PurchaseOrderEntry> GetPurchaseOrder(string purchaseOrderCnr);
    public Task<List<PurchaseOrderPositionEntry>> GetPurchaseOrderItems(int purchaseOrderid);
    public PurchaseOrderPositionEntry GetPurchaseOrderItem(int purchaseOrderid, string itemCnr);
    public Task<CreatedGoodsReceiptPositionReelEntry> CreateGoodsReceiptPosition(CreateGoodsReceiptPositionReelEntry createGoodsReceiptPositionReelEntry);
    public Task<CreatedGoodsReceiptPositionEntry> CreateGoodsReceiptPosition(CreateGoodsReceiptPositionEntry createGoodsReceiptPositionEntry);

    }
}
