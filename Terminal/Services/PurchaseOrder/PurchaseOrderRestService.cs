﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.User;
using Microsoft.Extensions.Logging;
using RestSharp;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.PurchaseOrder
{
  public class PurchaseOrderRestService : RestBase, IPurchaseOrderRestService
  {
    private readonly ILogger _logger;
    private readonly IMessageService _messageService;

    public PurchaseOrderRestService(ILogger<PurchaseOrderRestService> logger, IUserService userService, ISettingsService settingsService,
      IMessageService messageService) : base(settingsService, userService)
    {
      _logger = logger;
      _messageService = messageService;
    }

    public async Task<CreatedGoodsReceiptPositionReelEntry> CreateGoodsReceiptPosition(CreateGoodsReceiptPositionReelEntry createGoodsReceiptPositionReelEntry)
    {
      string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(createGoodsReceiptPositionReelEntry);
      var request = new RestRequest("purchaseorder/goodsreceiptreel", Method.Post)
        .AddQueryParameter("filter_receiptpossible", true)
        .AddQueryParameter("userid", Token())
        .AddHeader("Content-Type", "application/json")
        .AddJsonBody(strJson);

      try
      {
        var response = await Client.ExecutePostAsync<CreatedGoodsReceiptPositionReelEntry>(request);
        if (response.IsSuccessful)
          return response.Data;
        else
        {
          string msg = string.Empty;
          if (response.Headers?.Count > 0)
            msg = BuildMessageFromHeader(response);
          else
            msg = string.Format(Error_Message_Action, "Goods receipt") + " " + string.Format(RequestReturnedStatuscode0, response.StatusCode);
          _messageService.Error(msg);
          return null;
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<CreatedGoodsReceiptPositionEntry> CreateGoodsReceiptPosition(CreateGoodsReceiptPositionEntry createGoodsReceiptPositionEntry)
    {
      string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(createGoodsReceiptPositionEntry);
      var request = new RestRequest("purchaseorder/goodsreceipt", Method.Post)
        .AddQueryParameter("filter_receiptpossible", true)
        .AddQueryParameter("userid", Token())
        .AddHeader("Content-Type", "application/json")
        .AddJsonBody(strJson);

      try
      {
        var response = await Client.ExecutePostAsync<CreatedGoodsReceiptPositionEntry>(request);
        if (response.IsSuccessful)
          return response.Data;
        else
        {
          string msg = string.Empty;
          if (response.Headers?.Count > 0)
            msg = BuildMessageFromHeader(response);
          else
            msg = string.Format(Error_Message_Action, "Goods receipt") + " " + string.Format(RequestReturnedStatuscode0, response.StatusCode);
          _messageService.Error(msg);
          return null;
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public PurchaseOrderPositionEntry GetPurchaseOrderItem(int purchaseOrderid, string itemCnr)
    {
      throw new NotImplementedException();
    }

    //public async Task<PurchaseOrderEntry> GetPurchaseOrder(string purchaseOrderCnr)
    //{
    //  var request = new RestRequest("purchaseorder")
    //    .AddQueryParameter("filter_receiptpossible", true)
    //    .AddQueryParameter("purchaseOrderCnr", purchaseOrderCnr)
    //    .AddQueryParameter("userid", Token());
    //  try
    //  {
    //    var response = await Client.GetAsync<PurchaseOrderEntry>(request);
    //    return response;
    //  }
    //  catch (HttpRequestException ex)
    //  {
    //    _logger.LogError(ex.Message, ex);
    //    return null;
    //  }
    //  catch (Exception ex)
    //  {
    //    _logger.LogError(ex.Message, ex);
    //    return null;
    //  }

    //}

    public async Task<List<PurchaseOrderPositionEntry>> GetPurchaseOrderItems(int purchaseOrderid)
    {
      var request = new RestRequest("purchaseorder/itemlist")
        .AddQueryParameter("filter_receiptpossible", true)
        .AddQueryParameter("purchaseOrderid", purchaseOrderid)
        .AddQueryParameter("userid", Token());
      try
      {
        var response = await Client.GetAsync<PurchaseOrderPositionEntryList>(request);
        return response.entries;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return new List<PurchaseOrderPositionEntry>();
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return new List<PurchaseOrderPositionEntry>();
      }
    }

    public async Task<List<PurchaseOrderEntry>> GetPurchaseOrdersAsync(string filterCnr = "", int limit = 200)
    {
      var request = new RestRequest("purchaseorder/list")
        .AddQueryParameter("filter_cnr", (filterCnr == "" ? null : filterCnr))
        .AddQueryParameter("limit", limit)
        .AddQueryParameter("filter_status", "OPEN,CONFIRMED,PARTLYDONE")
        .AddQueryParameter("userid", Token());
      try
      {
        var response = await Client.GetAsync<PurchaseOrderEntryList>(request);
        return response.entries;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }

    }

  }
}
