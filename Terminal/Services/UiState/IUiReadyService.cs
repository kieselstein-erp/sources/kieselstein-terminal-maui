﻿namespace KieselsteinErp.Terminal.Services.UiState
{
  /// <summary>
  /// A simple service, that indicates if the UI is ready to do some actions ('appeared')
  /// </summary>
  public interface IUiReadyService
    {

        bool IsReady();

        void AwaitReady();


    }
}
