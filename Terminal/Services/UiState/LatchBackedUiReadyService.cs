﻿namespace KieselsteinErp.Terminal.Services.UiState
{

  /// <summary>
  /// This ui state service uses an internal latch t
  /// </summary>
  public class LatchBackedUiReadyService : IUiReadyService
    {
        private readonly ManualResetEventSlim _latch = new ManualResetEventSlim(false);

        public bool IsReady()
        {
            return _latch.IsSet;
        }

        public void AwaitReady()
        {
            _latch.Wait();
        }

        public void TriggerGuiReady()
        {
            _latch.Set();

        }
    }
}
