﻿using KieselsteinErp.Terminal.Resources.Localization;

namespace KieselsteinErp.Terminal.Services
{

    public class LocaleService
    {
        private static readonly List<string> Locales = new() { "en-US", "de-AT", "de-DE", "de-CH", "it", "pl", "cs", "sl" };

        private LocaleService()
        {
            throw new Exception("Only static methods");
        }



        public static string[] GetAvailableLocales()
        {
            return Locales.ToArray();
        }

     


    }
}
