﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.User;
using Microsoft.Extensions.Logging;
using RestSharp;
using System.Globalization;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.Item
{
  public class ItemV1RestService : RestBase, IItemV1Service
  {
    private readonly ILogger _logger;

    public ItemV1RestService(ILogger<ItemV1RestService> logger, ISettingsService settingsService, IUserService userService) : base(settingsService, userService, true)
    {
      _logger = logger;
    }

    public async Task<ItemV1EntryList> GetItemsAsync(string filterCnr, string filterText, string filterDeliveryCnr = null)
    {
      var request = new RestRequest("item/listv1")
          .AddQueryParameter("userid", Token());
      if (!string.IsNullOrEmpty(filterCnr))
        request.AddQueryParameter("filter_cnr", filterCnr);
      if (!string.IsNullOrEmpty(filterText))
        request.AddQueryParameter("filter_textsearch", filterText);
      if(!string.IsNullOrEmpty(filterDeliveryCnr))
        request.AddQueryParameter("filter_deliverycnr", filterDeliveryCnr);

      try
      {
        var response = await Client.GetAsync<ItemV1EntryList>(request);
        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<ItemV1EntryList> GetItemsWithCnrOrTextAsync(string filter)
    {
      ItemV1EntryList list = await GetItemsAsync(filter, null);
      if (list == null || list.entries.Count == 0)
        list = await GetItemsAsync(null, filter);
      return list;
    }

    public async Task<ItemV1EntryList> GetItemsWithCnrOrTextOrDeliverCnr(string filter)
    {
      ItemV1EntryList list = await GetItemsWithCnrOrTextAsync(filter);
      
      ItemV1EntryList list1 = await GetItemsAsync(null, null, filter);
      if (list1 == null || list1.entries == null || list1.entries.Count == 0) return list;

      if (list == null || list.entries == null || list.entries.Count == 0)  return list1;
        
      foreach (var item in list1.entries)
        if (list.entries.Find(o => o.id == item.id) == null)
          list.entries.Add(item);

      list.entries = list.entries.OrderBy(item => item.cnr).ToList();
      return list;
    }

    public async Task<List<StockAmountEntry>> GetItemStocks(string itemCnr)
    {
      var request = new RestRequest("item/stocks")
        .AddQueryParameter("itemCnr", itemCnr)
        .AddQueryParameter("returnItemInfo", false)
        .AddQueryParameter("userid", Token());
      try
      {
        var response = await Client.GetAsync<List<StockAmountEntry>>(request);
        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return new List<StockAmountEntry>();
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return new List<StockAmountEntry>();
      }
    }

    public async Task<ItemV1Entry> GetItemV1EntryAsync(string cnr, bool addStockInfos)
    {
      var request = new RestRequest("item/itemv1")
          .AddQueryParameter("userid", Token())
          .AddQueryParameter("itemCnr", cnr)
          .AddQueryParameter("addStockAmountInfos", addStockInfos)
          .AddQueryParameter("addStockPlaceInfos", addStockInfos);
      try
      {
        var response = await Client.GetAsync<ItemV1Entry>(request);
        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<BookingResult> PrintLabel(int itemId, decimal? amount, string chargenr, int? copies, int? stockid)
    {
      var request = new RestRequest("item/{itemid}/printlabel")
        .AddUrlSegment("itemid", itemId)
        .AddQueryParameter("userid", Token());
      if (amount != null)
        request.AddQueryParameter("amount", ((decimal)amount).ToString("F3", CultureInfo.InvariantCulture));
      if (!string.IsNullOrEmpty(chargenr))
        request.AddQueryParameter("chargenr", chargenr);
      if (copies != null)
        request.AddQueryParameter("copies", (int)copies);
      if (stockid != null)
        request.AddQueryParameter("identitystockid", (int)stockid);
      var response = await Client.ExecuteGetAsync(request);
      if (response.IsSuccessful)
      {
        return new BookingResult(true, null);
      }
      else
      {
        string msg = BuildMessageFromHeader(response);
        if (!string.IsNullOrEmpty(msg))
        {
          return new BookingResult(false, msg);
        }
        else
          return new BookingResult(false, string.Format(RequestReturnedStatuscode0,response.StatusCode));
      }
    }

    public async Task<BatchDiscardResult> DiscardRemaining(int itemId, string chargenr)
    {
      var request = new RestRequest("item/{itemid}/discardremaining", Method.Put)
        .AddUrlSegment("itemid", itemId)
        .AddQueryParameter("chargenr", chargenr)
        .AddQueryParameter("userid", Token());
        //.AddHeader("Content-Type", "application/json");

      var response = await Client.ExecutePutAsync<ProductionEntryList>(request);
      if (response.IsSuccessful)
      {
        return new BatchDiscardResult(true, response.Data, null, response.StatusCode);
      }
      else
      {
        string msg = BuildMessageFromHeader(response);
        if (!string.IsNullOrEmpty(msg))
        {
          return new BatchDiscardResult(false, msg);
        }
        else
          return new BatchDiscardResult(false, string.Format(RequestReturnedStatuscode0, response.StatusCode));
      }

    }

    public async Task<ItemMasterSheet> GetItemMasterSheet(int itemId)
    {
      var request = new RestRequest("item/{itemid}/itemmastersheet")
          .AddUrlSegment("itemid", itemId)
          .AddQueryParameter("userid", Token());

      try
      {
        var response = await Client.GetAsync<ItemMasterSheet>(request);
        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

  }
}
