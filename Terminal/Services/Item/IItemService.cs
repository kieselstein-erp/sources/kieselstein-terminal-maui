﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Services.Item
{
	public interface IItemService
	{
		Task<ItemEntry> GetItemEntryAsync(string cnr);
	}
}
