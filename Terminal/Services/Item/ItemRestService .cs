﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.User;
using RestSharp;

namespace KieselsteinErp.Terminal.Services.Item
{
	public class ItemRestService : RestBase, IItemService
	{
		public ItemRestService(ISettingsService settingsService, IUserService userService) : base(settingsService, userService, false) { }
		public async Task<ItemEntry> GetItemEntryAsync(string cnr)
		{
			var request = new RestRequest("item")
				.AddQueryParameter("userid", Token())
				.AddQueryParameter("itemCnr", cnr);
			try
			{
				var response = await Client.GetAsync<ItemEntry>(request);
				return response;
			}
			catch (HttpRequestException ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.ToString());
				// eventuell Statuscode auswerten?
				return null;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.ToString());
				return null;
			}
		}
	}
}
