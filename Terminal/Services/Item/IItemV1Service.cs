﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Kieselstein.Models;
using KieselsteinErp.Terminal.Models;

namespace KieselsteinErp.Terminal.Services.Item
{
  public interface IItemV1Service
  {
    Task<ItemV1Entry> GetItemV1EntryAsync(string cnr, bool addStockPlaceInfos);
    Task<ItemV1EntryList> GetItemsAsync(string filterCnr, string filterText, string filterDeliveryCnr = null);
    Task<ItemV1EntryList> GetItemsWithCnrOrTextAsync(string filter);
    Task<ItemV1EntryList> GetItemsWithCnrOrTextOrDeliverCnr(string filter);
    Task<BookingResult> PrintLabel(int itemId, decimal? amount, string chargenr, int? copies, int? stockid);
    Task<List<StockAmountEntry>> GetItemStocks(string itemCnr);
    Task<BatchDiscardResult> DiscardRemaining(int itemId, string chargenr);
    Task<ItemMasterSheet> GetItemMasterSheet(int itemId);
  }
}
