﻿namespace KieselsteinErp.Terminal.Services
{
    public class CodeReadEventArgs
    {
        public string Code { get; set; }
    }
}
