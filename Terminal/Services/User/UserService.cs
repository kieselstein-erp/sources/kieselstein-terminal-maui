﻿using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.UiState;

namespace KieselsteinErp.Terminal.Services.User
{
  public class UserService : IUserService, IDisposable
  {
    public readonly Version _minApiVersion = new(1,0,14);

    public event EventHandler<bool> OnLogonChanged;

    private readonly IUserRestService _userRestService;
    private readonly ISettingsService _settingsService;
    private readonly IMessageService _messageService;
    private readonly IUiReadyService _uiReadyService;
    private string _userid;
    private Timer _logonTimer;
    private Timer _checkOnlineTimer;
    private bool _disposed;
    private const long RetryIntervalLogon = 30000;
    private const long RetryIntervalCheckOnline = 10000;
    private bool _messageShown;

    private string _username = "Terminal";
    private string _password = "terminal";

    private bool _isAppLogon = false;

    public UserService(IUserRestService userRestService, ISettingsService settingsService, IMessageService messageService, IUiReadyService uiReadyService)
    {
      _userRestService = userRestService;
      _settingsService = settingsService;
      _messageService = messageService;
      _uiReadyService = uiReadyService;

      //Initialize();
      if (!(DeviceInfo.Idiom == DeviceIdiom.Phone))
        _logonTimer = new Timer(new TimerCallback(OnLogonTimer), null, 100, Timeout.Infinite);
    }

    /// <summary>
    /// try to get a token (=userid) from server
    /// retry if failed every RetryInterval
    /// </summary>
    private async void Initialize()
    {
      try
      {
        LoggedOnEntryResult loggedOnEntryResult = await _userRestService.LogonAsync(new LogonEntry()
        {
          username = _username,
          password = _password,
          client = _settingsService.LogonMandant,
          localeString = _settingsService.LogonLocale
        });
        _userid = loggedOnEntryResult?.loggedOnEntry?.token;

        if (loggedOnEntryResult != null)
        {

          if (!string.IsNullOrWhiteSpace(_userid))
          {
            LocalPingResult localPingResult = await _userRestService.LocalPing();

            _uiReadyService.AwaitReady();

            if (localPingResult != null)
              CheckApiVersion(localPingResult.apiVersion, _minApiVersion);

            OnLogonChanged?.Invoke(this, true);
            EnableCheckOnline();
          }
          else
          {
            DisableCheckOnline();
            if (!_messageShown)
            {
              _messageService.Error(loggedOnEntryResult.errorMsg);
              _messageShown = true;
            }
            if (!_isAppLogon)
              EnableLogonTimer();
          }
        }
      }
      catch (Exception ex)
      {
        _messageService.Error(ex.Message);
      }
    }

    private void CheckApiVersion(string apiVersion, Version minApiVersion)
    {
      if (string.IsNullOrEmpty(apiVersion)) return;

      try
      {
        if (apiVersion.Contains("-"))
          apiVersion = apiVersion.Substring(0, apiVersion.IndexOf("-"));

        Version actual = Version.Parse(apiVersion);
        if (actual.CompareTo(minApiVersion) < 0)
          WeakReferenceMessenger.Default.Send(new VersionMessage(actual, minApiVersion, KieselsteinErp.Terminal.Resources.Localization.AppResources.RestApiVersionToLessPleaseUpdate));
      }
      catch (Exception)
      {
        WeakReferenceMessenger.Default.Send(new VersionMessage(null, minApiVersion, string.Format("Error parsing API Version {0}", apiVersion)));
      }
    }

    public bool IsAppLogon { get => _isAppLogon; private set => _isAppLogon = value; }

    private void EnableLogonTimer()
    {
      if (_logonTimer == null)
        _logonTimer = new Timer(new TimerCallback(OnLogonTimer), null, RetryIntervalLogon, Timeout.Infinite);
      else
        _logonTimer.Change(RetryIntervalLogon, Timeout.Infinite);
    }

    private void DisableLogonTimer()
    {
      _logonTimer?.Change(Timeout.Infinite, Timeout.Infinite);
    }

    private void DisableCheckOnline()
    {
      _checkOnlineTimer?.Change(Timeout.Infinite, Timeout.Infinite);
    }

    private void EnableCheckOnline()
    {
      if (_checkOnlineTimer == null)
        _checkOnlineTimer = new Timer(new TimerCallback(OnCheckTimer), null, RetryIntervalCheckOnline, Timeout.Infinite);
      else
        _checkOnlineTimer.Change(RetryIntervalCheckOnline, Timeout.Infinite);
    }

    /// <summary>
    /// ping restapi for online check every 10s
    /// </summary>
    /// <param name="state"></param>
    private async void OnCheckTimer(object state)
    {
      LocalPingResult result = await _userRestService.LocalPing();
      if (result == null)
      {
        OnLogonChanged(this, false);
        EnableLogonTimer();
      }

      EnableCheckOnline();
    }

    /// <summary>
    /// if logon failed retry every 30s
    /// </summary>
    /// <param name="state"></param>
    private void OnLogonTimer(object state)
    {
      DisableLogonTimer();
      Initialize();
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
      if (_disposed)
      {
        return;
      }

      if (disposing)
      {
        DisableLogonTimer();
        _logonTimer.Dispose();
        DisableCheckOnline();
        _checkOnlineTimer.Dispose();
      }
      _disposed = true;
    }

    public void LogonApp(string username, string password)
    {
      _isAppLogon = true;
      _messageShown = false;
      _username = username;
      _password = password;
      _logonTimer = new Timer(new TimerCallback(OnLogonTimer), null, 100, Timeout.Infinite);
    }

    public async void LogoutApp()
    {
      if (await _userRestService.LogoutAsync(Userid))
        Userid = null;

    }

    public async void LogonIdCard(string idCard)
    {
      LoggedOnEntryResult loggedOnEntryResult = await _userRestService.LogonIdCard(_settingsService.LogonMandant, _settingsService.LogonLocale, idCard);
      _userid = loggedOnEntryResult?.loggedOnEntry?.token;

      if (loggedOnEntryResult != null)
      {

        if (!string.IsNullOrWhiteSpace(_userid))
        {
          _uiReadyService.AwaitReady();
          OnLogonChanged?.Invoke(this, true);
          EnableCheckOnline();
        }
        else
        {
          DisableCheckOnline();
          if (!_messageShown)
          {
            _messageService.Error(loggedOnEntryResult.errorMsg);
            _messageShown = true;
          }
          //EnableLogonTimer();
        }
      }
    }

    public string Userid
    {
      get => _userid;
      private set => _userid = value;
    }

  }
}
