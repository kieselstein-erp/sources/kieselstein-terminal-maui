﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services.Settings;
using Microsoft.Extensions.Logging;
using RestSharp;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.User
{
  public class UserRestService : RestBase, IDisposable, IUserRestService
  {
    private readonly ILogger _logger;

    public UserRestService(ILogger<UserRestService> logger, ISettingsService settingsService) : base(settingsService, null)
    {
      _logger = logger;
    }

    public async Task<LocalPingResult> LocalPing()
    {
      if (Connectivity.Current.NetworkAccess < NetworkAccess.Local)
        return null;

      try
      {
        LocalPingResult response = await Client.GetJsonAsync<LocalPingResult>("system/localping/");
        return response;
      }
      catch (HttpRequestException e)
      {
        if (e.StatusCode == null)
        {
          return null;
        }
        else
        {
          return null;
        }
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }

    }

    public async Task<LoggedOnEntryResult> LogonAsync(LogonEntry logonEntry)
    {
      if (Connectivity.Current.NetworkAccess < NetworkAccess.Local)
        return new LoggedOnEntryResult() { errorMsg = NoNetworkAccess };

      if (logonEntry == null)
        logonEntry = new LogonEntry() { username = "Terminal", password = "terminal", client = "001", localeString = "deAT" };

      try
      {
        var response = await Client.PostJsonAsync<LogonEntry, LoggedOnEntry>("logon/", logonEntry);
        return new LoggedOnEntryResult(response);
      }
      catch (HttpRequestException e)
      {
        if (e.StatusCode == null)
        {
          return new LoggedOnEntryResult() { errorMsg = e.Message };
        }
        else
        {
          return new LoggedOnEntryResult() { errorMsg = e.Message, httpStatusCode = (System.Net.HttpStatusCode)e.StatusCode };
        }
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }


    /// <summary>
    /// logon with ID-card
    /// 
    /// Attention: 
    ///   user and password credentials has to be set in restapi context.xml
    ///   rights of credetial user are used for rest-calls
    /// </summary>
    /// <param name="client">tenant to use</param>
    /// <param name="locale">locale to use</param>
    /// <param name="idCard">ID-card</param>
    /// <returns></returns>
    public async Task<LoggedOnEntryResult> LogonIdCard(string client, string locale, string idCard)
    {
      if (Connectivity.Current.NetworkAccess < NetworkAccess.Local)
        return new LoggedOnEntryResult() { errorMsg = NoNetworkAccess };

      LogonIdCardEntry logonIdCardEntry = new LogonIdCardEntry { client = client, localeString = locale };
      string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(logonIdCardEntry);
      var request = new RestRequest("logonidcard/{id}", Method.Post)
        .AddUrlSegment("id", idCard)
        .AddJsonBody(strJson)
        .AddHeader("Content-Type", "application/json");

      var response = await Client.ExecutePostAsync<LoggedOnEntry>(request);

      if (response.IsSuccessful)
        return new LoggedOnEntryResult(response.Data);
      else
      {
        string msg = null;

        if (response.Headers.Count > 0)
          msg = BuildMessageFromHeader(response);
        if (msg != null)
        {
          return new LoggedOnEntryResult { errorMsg = msg };
        }
        else
          return new LoggedOnEntryResult { errorMsg = string.Format(RequestReturnedStatuscode0, response.StatusCode) };
      }
    }

    public async Task<bool> LogoutAsync(string userid)
    {
      if (string.IsNullOrWhiteSpace(userid))
        return true;

      if (Connectivity.Current.NetworkAccess < NetworkAccess.Local)
      {
        return false;
      }

      try
      {
        var request = new RestRequest("logout").AddQueryParameter("userid", userid);
        var response = await Client.GetAsync(request);
        return true;
      }
      catch (HttpRequestException e)
      {
        _logger.LogError(e.Message, e);
        return false;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return false;
      }

    }

  }
}
