﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;

namespace KieselsteinErp.Terminal.Services.User
{
  public interface IUserRestService
  {
    Task<LoggedOnEntryResult> LogonAsync(LogonEntry logonEntry);
    Task<bool> LogoutAsync(string userid);
    Task<LocalPingResult> LocalPing();
    Task<LoggedOnEntryResult> LogonIdCard(string client, string locale, string idCard);
  }
}