﻿namespace KieselsteinErp.Terminal.Services.User
{
  public interface IUserService
  {
    string Userid { get; }

    event EventHandler<bool> OnLogonChanged;

    void LogonApp(string username, string password);

    void LogoutApp();

    void LogonIdCard(string idCard);

    bool IsAppLogon { get; }
  }
}