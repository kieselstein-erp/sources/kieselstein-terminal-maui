﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Services.Database;
using KieselsteinErp.Terminal.Services.User;

namespace KieselsteinErp.Terminal.Services.Staff
{
  public class StaffService : IStaffService
  {
    private List<StaffEntry> _staffEntries = null;
    private readonly IStaffRestService _staffRestService;
    private readonly IUserService _userService;
    private DateTime _lastLoaded;
    private bool _isOnline;
    private bool _onlineChanged;
    private readonly IDatabaseService _databaseService;

    public StaffService(IStaffRestService staffRestService, IUserService user, IDatabaseService databaseService)
    {
      _staffRestService = staffRestService;
      _userService = user;
      _databaseService = databaseService;

      _userService.OnLogonChanged += (s, e) => { _onlineChanged = true; _isOnline = e; };
    }

    private void FetchStaff()
    {
      // Reload staff every 2 hours or when empty or when online status changed to online
      if (((DateTime.Now - _lastLoaded).TotalHours < 2) && (_staffEntries != null) && !_onlineChanged)
        return;

      if (_isOnline)
      {
        _onlineChanged = false;
        _staffEntries = _staffRestService.GetStaffEntries(_userService.Userid);
        _lastLoaded = DateTime.Now;
        _databaseService.SaveStaffOffline(_staffEntries);
      }
      else
      {
        var task = Task.Run(async () => await _databaseService.GetStaffOffline());
        _staffEntries = task.GetAwaiter().GetResult();
      }
    }

    public StaffEntry FindStaffByIdentyCnr(string identityCnr)
    {
      FetchStaff();

      var staff = _staffEntries.Find(b => b.identityCnr == identityCnr);
      if (staff == null && _isOnline)
      {
        // get new staff when online
        staff = _staffRestService.GetStaff(_userService.Userid, identityCnr);
        if (staff != null && !_staffEntries.Contains(staff))
        {
          _staffEntries.Add(staff);
          //TODO: Add staff to DB
          _lastLoaded = DateTime.Now;
        }
      }

      return staff;
    }

    public int GetStaffCountOffline()
    {
      var task = Task.Run(async () => await _databaseService.GetStaffCountOffline());
      var result = task.GetAwaiter().GetResult();
      return result;

    }

    public StaffEntry[] GetStaffEntries()
    {
      FetchStaff();
      return _staffEntries.ToArray();
    }

    public string GetFormatedStaffName(string indentityCnr)
    {
      return GetFormatedStaffName(FindStaffByIdentyCnr(indentityCnr));
    }

    public string GetFormatedStaffName(int staffId)
    {
      return GetFormatedStaffName(FindStaffById(staffId));
    }

    public string GetFormatedStaffName(StaffEntry staffEntry)
    {
      if (staffEntry == null) return string.Empty;
      if (staffEntry.firstName == null && staffEntry.name == null)
        return string.Empty;
      else if (staffEntry.firstName == null)
        return staffEntry.name;
      else
        return staffEntry.firstName + " " + staffEntry.name;
    }

    public StaffEntry FindStaffByUserid(string userid)
    {
      var staff = _staffRestService.GetStaffByUserId(userid);
      return staff;
    }

    public StaffEntry FindStaffByStaffCnr(string staffCnr)
    {
      var staff = _staffEntries.Find(b => b.personalNr == staffCnr);
      return staff;
    }

    public StaffEntry FindStaffById(int staffId)
    {
      var staff = _staffEntries.Find(b => b.id == staffId);
      return staff;
    }

    public string GetStaffShortMark(int staffId)
    {
      return _staffEntries.Find(o => o.id == staffId)?.shortMark;
    }
  }
}

