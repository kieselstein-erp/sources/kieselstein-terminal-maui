﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.User;
using Microsoft.Extensions.Logging;
using RestSharp;

namespace KieselsteinErp.Terminal.Services.Staff
{
  public class StaffRestService : RestBase, IStaffRestService
  {
    private readonly ILogger _logger;

    public StaffRestService(ILogger<StaffRestService> logger, ISettingsService settingsService, IUserService userService) : base(settingsService, userService, true) 
    {
      _logger = logger;
    }

    /// <summary>
    /// Get all staff from kieselstein server
    /// </summary>
    /// <param name="userid"></param>
    /// <returns> List of persons</returns>
    public async Task<List<StaffEntry>> GetStaffEntriesAsync(string userid)
    {
      //TODO: limit
      var request = new RestRequest("staff/list").AddQueryParameter("userid", userid).AddQueryParameter("limit", 500);
      try
      {
        var response = await Client.GetAsync<List<StaffEntry>>(request);
        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return new List<StaffEntry>();
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return new List<StaffEntry>();
      }
    }


    public List<StaffEntry> GetStaffEntries(string userid)
    {
      //TODO: limit, 0 ... get all
      var request = new RestRequest("staff/list").AddQueryParameter("userid", userid).AddQueryParameter("limit", 0);
      try
      {
        var response = Client.Get<List<StaffEntry>>(request);
        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return new List<StaffEntry>();
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return new List<StaffEntry>();
      }
    }

    public StaffEntry GetStaff(string userid, string identityCard)
    {
      var request = new RestRequest("staff/" + userid).AddQueryParameter("forStaffIdCard", identityCard);
      try
      {
        var response = Client.Get<StaffEntry>(request);
        return response;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public StaffEntry GetStaffByUserId(string userid)
    {
      var request = new RestRequest("staff/" + userid);
      try
      {
        var response = Client.Get<StaffEntry>(request);
        return response;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }
  }
}
