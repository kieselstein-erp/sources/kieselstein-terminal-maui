﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Services.Staff
{
  public interface IStaffRestService
  {
    Task<List<StaffEntry>> GetStaffEntriesAsync(string userid);

    List<StaffEntry> GetStaffEntries(string userid);

    StaffEntry GetStaff(string userid, string identityCard);

    StaffEntry GetStaffByUserId(string userId);

  }
}