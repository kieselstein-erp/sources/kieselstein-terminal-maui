﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Services.Staff
{
  public interface IStaffService
  {
    public int GetStaffCountOffline();
    StaffEntry FindStaffByIdentyCnr(string identityCnr);
    StaffEntry FindStaffByUserid(string userid);
    StaffEntry FindStaffByStaffCnr(string staffCnr);
    StaffEntry[] GetStaffEntries();

    string GetFormatedStaffName(string identityCnr);
    string GetFormatedStaffName(int staffId);
    string GetFormatedStaffName(StaffEntry staffEntry);
    string GetStaffShortMark(int staffId);

  }
}