﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;

namespace KieselsteinErp.Terminal.Services.Order
{
  public interface IOrderService
  {
    Task<OrderEntry> GetOrderEntryByCnr(string orderCnr);
    Task<OrderState> GetOrderStateByCnr(string orderCnr);
    OrderState GetOrderState(OrderEntry orderEntry);
    Task<DeliverableOrderEntry> GetDeliverableOrderEntryByCnr(string orderCnr);
    Task<List<OrderEntry>> GetOrdersAsync(string filterCnr, string filterCustomer = null, string filterProject = null);
  }
}
