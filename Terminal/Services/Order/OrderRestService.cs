﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.User;
using Microsoft.Extensions.Logging;
using RestSharp;

namespace KieselsteinErp.Terminal.Services.Order
{
  public class OrderRestService : RestBase, IOrderService
  {
    private readonly ILogger _logger;
    private readonly IMessageService _messageService;

    public OrderRestService(ILogger<OrderRestService> logger, ISettingsService settingsService, IUserService userService, IMessageService messageService) : base(settingsService, userService)
    {
      _logger = logger;
      _messageService = messageService;
    }

    public async Task<DeliverableOrderEntry> GetDeliverableOrderEntryByCnr(string orderCnr)
    {
      var request = new RestRequest("order/deliverable")
        .AddQueryParameter("orderCnr", orderCnr)
        .AddQueryParameter("userid", Token());
      try
      {
        DeliverableOrderEntry response = await Client.GetAsync<DeliverableOrderEntry>(request);
        return response;

      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<List<OrderEntry>> GetOrdersAsync(string filterCnr, string filterCustomer = null, string filterProject = null)
    {
      var request = new RestRequest("order")
        .AddQueryParameter("userid", Token());

      if (!string.IsNullOrEmpty(filterCnr))
        request.AddQueryParameter("filter_cnr", filterCnr);

      if (!string.IsNullOrEmpty(filterCustomer))
        request.AddQueryParameter("filter_customer", filterCustomer);

      if (!string.IsNullOrEmpty(filterProject))
        request.AddQueryParameter("filter_project", filterProject);

      try
      {
        List<OrderEntry> response = await Client.GetAsync<List<OrderEntry>>(request);
        return response;

      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<OrderState> GetOrderStateByCnr(string orderCnr)
    {
      OrderEntry orderEntry = await GetOrderEntryByCnr(orderCnr);
      return GetOrderState(orderEntry);
    }

    public OrderState GetOrderState(OrderEntry orderEntry)
    {
      if (orderEntry == null)
        return OrderState.NOTFOUND;
      object res;
      if (!Enum.TryParse(typeof(OrderState), orderEntry.orderState.Trim(), out res))
        throw new Exception("Invalid order state");

      return (OrderState)res;
    }

    public async Task<OrderEntry> GetOrderEntryByCnr(string orderCnr)
    {
      var request = new RestRequest("order")
          .AddQueryParameter("userid", Token())
          .AddQueryParameter("filter_cnr", orderCnr);
      try
      {
        List<OrderEntry> response = await Client.GetAsync<List<OrderEntry>>(request);
        if (response.Count == 1)
          return response.First();
        return null;

      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }
  }
}
