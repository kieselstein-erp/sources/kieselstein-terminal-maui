﻿using KieselsteinErp.Terminal.Messages;

namespace KieselsteinErp.Terminal.Services.Audio
{
  public interface IAudioService
    {
    void PlayInfo();
    void PlayWarning();
    void PlayError();
    void Play(MessageType type);
    }
}
