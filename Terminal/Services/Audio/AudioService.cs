﻿using KieselsteinErp.Terminal.Messages;
using Microsoft.Extensions.Logging;
using Plugin.Maui.Audio;

namespace KieselsteinErp.Terminal.Services.Audio
{
  public class AudioService : IAudioService
  {
    private const string ERROR_SOUND = "a-nasty-sound-if-you-choose-the-wrong-one-149895.mp3";
    private const string INFO_SOUND = "notification-sound-7062.mp3";
    private const string WARN_SOUND = "warning-sound-6686.mp3";

    private readonly IAudioManager _audioManager;
    private readonly ILogger _logger;
    public AudioService(IAudioManager audioManager, ILogger<AudioService> logger)
    {
      _audioManager = audioManager;
      _logger = logger;
    }

    public void Play(MessageType type)
    {
      switch (type)
      {
        case MessageType.Info:
          PlayInfo();
          break;

        case MessageType.Warning:
          PlayWarning();
          break;

        case MessageType.Error:
          PlayError();
          break;
      }
    }

    public async void PlayError()
    {
      try
      {
        var audioPlayer = _audioManager.CreateAsyncPlayer(await FileSystem.OpenAppPackageFileAsync(ERROR_SOUND));
        await audioPlayer.PlayAsync(default);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
      }
    }

    public async void PlayInfo()
    {
      try
      {
        var audioPlayer = _audioManager.CreateAsyncPlayer(await FileSystem.OpenAppPackageFileAsync(INFO_SOUND));
      await audioPlayer.PlayAsync(default);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
      }
    }

    public async void PlayWarning()
    {
      try
      {
        var audioPlayer = _audioManager.CreateAsyncPlayer(await FileSystem.OpenAppPackageFileAsync(WARN_SOUND));
      await audioPlayer.PlayAsync(default);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message);
      }
    }
  }
}
