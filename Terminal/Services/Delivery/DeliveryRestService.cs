﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Resources.Localization;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.User;
using Microsoft.Extensions.Logging;
using RestSharp;
using System.Net;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.Delivery
{
  public class DeliveryRestService : RestBase, IDeliveryService
  {
    private const int EJBExceptionLP_FEHLER_LIEFERSCHEIN_ZU_VIELE_ANGELEGTE = 7014;

    private readonly ILogger _logger;
    private readonly IMessageService _messageService;

    public DeliveryRestService(ILogger<DeliveryRestService> logger, ISettingsService settingsService, IUserService userService, IMessageService messageService) : base(settingsService, userService)
    {
      _logger = logger;
      _messageService = messageService;
    }

    public async Task<CreatedDeliveryPositionEntry> CreateCustomerPosition(int deliveryId, CreateItemCustomerDeliveryPositionEntry createItemCustomerDeliveryPositionEntry)
    {
      string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(createItemCustomerDeliveryPositionEntry);

      var request = new RestRequest("delivery/customerposition", Method.Post)
        .AddHeader("Content-Type", "application/json")
        .AddJsonBody(strJson)
        .AddQueryParameter("userid", Token());

      try
      {
        var response = await Client.ExecutePostAsync<CreatedDeliveryPositionEntry>(request);
        if (response.IsSuccessful)
          return response.Data;
        else
        {
          string msg = string.Empty;
          if (response.Headers?.Count > 0)
          {
            if (response.StatusCode == System.Net.HttpStatusCode.ExpectationFailed && GetHeaderValue(response, "x-hv-error-code") == "8" && GetHeaderValue(response, "x-hv-error-code-extended") == EJBExceptionLP_FEHLER_LIEFERSCHEIN_ZU_VIELE_ANGELEGTE.ToString())
              msg = KieselsteinErp.Terminal.Resources.Localization.AppResources.AMaximumOf10OpenDeliveryNotesPerCustomerArePermitted;
            else
              msg = BuildMessageFromHeader(response);
          }
          else
            msg = string.Format(Error_Message_Action, DeliverItemPosition) + " " + string.Format(RequestReturnedStatuscode0, response.StatusCode);
          _messageService.Error(msg);
          return null;
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<CreatedDeliveryPositionEntry> CreateItemPosition(int deliveryId, CreateDeliveryItemPositionEntry createDeliveryItemPositionEntry)
    {
      string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(createDeliveryItemPositionEntry);

      var request = new RestRequest("delivery/{deliveryid}/itemposition", Method.Post)
        .AddUrlSegment("deliveryid", deliveryId)
        .AddHeader("Content-Type", "application/json")
        .AddJsonBody(strJson)
        .AddQueryParameter("userid", Token());

      try
      {
        var response = await Client.ExecutePostAsync<CreatedDeliveryPositionEntry>(request);
        if (response.IsSuccessful)
          return response.Data;
        else
        {
          string msg = string.Empty;
          if (response.Headers?.Count > 0)
            msg = BuildMessageFromHeader(response);
          else
            msg = string.Format(Error_Message_Action, DeliverItemPosition) + " " + string.Format(RequestReturnedStatuscode0, response.StatusCode);
          _messageService.Error(msg);
          return null;
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<CreatedDeliveryPositionEntry> CreatePosition(CreateItemDeliveryPositionEntry createItemDeliveryPositionEntry)
    {
      string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(createItemDeliveryPositionEntry);

      var request = new RestRequest("delivery/position", Method.Post)
        .AddHeader("Content-Type", "application/json")
        .AddJsonBody(strJson)
        .AddQueryParameter("userid", Token());

      try
      {
        var response = await Client.ExecutePostAsync<CreatedDeliveryPositionEntry>(request);
        if (response.IsSuccessful)
          return response.Data;
        else
        {
          string msg = string.Empty;
          if (response.Headers?.Count > 0)
            msg = BuildMessageFromHeader(response);
          else
            msg = string.Format(Error_Message_Action, DeliverItemPosition) + " " + string.Format(RequestReturnedStatuscode0, response.StatusCode);
          _messageService.Error(msg);
          return null;
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<BookingResult> DeletePosition(int positionId)
    {
      var request = new RestRequest("delivery/position", Method.Delete)
        .AddQueryParameter("positionid", positionId)
        .AddQueryParameter("userid", Token());

      try
      {
        var response = await Client.DeleteAsync(request);
        if (response.IsSuccessful)
          return new BookingResult(true, string.Empty);
        else
        {
          string msg = string.Empty;
          if (response.Headers?.Count > 0)
            msg = BuildMessageFromHeader(response);
          else
            msg = string.Format(Error_Message_Action, Resources.Localization.AppResources.DeletePosition) + " " + string.Format(RequestReturnedStatuscode0, response.StatusCode);
          return new BookingResult(false, msg);
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return new BookingResult(false, ex.Message);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return new BookingResult(false, ex.Message);
      }
    }

    public async Task<DeliverableDeliveryEntry> DeliverableDeliveryEntry(string deliveryCnr)
    {
      var request = new RestRequest("delivery/deliverable")
        .AddQueryParameter("deliveryCnr", deliveryCnr)
        .AddQueryParameter("userid", Token());

      try
      {
        DeliverableDeliveryEntry response = await Client.GetAsync<DeliverableDeliveryEntry>(request);
        return response;

      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<CreatedDeliveryPositionEntry> DeliverPositionFromOrder(CreateDeliveryPositionEntry deliveryPositionEntry)
    {
      string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(deliveryPositionEntry);

      var request = new RestRequest("delivery/positionfromorder", Method.Post)
        .AddHeader("Content-Type", "application/json")
        .AddJsonBody(strJson)
        .AddQueryParameter("userid", Token());
      try
      {
        var response = await Client.ExecutePostAsync<CreatedDeliveryPositionEntry>(request);
        if (response.IsSuccessful)
          return response.Data;
        else
        {
          string msg = string.Empty;
          if (response.Headers?.Count > 0)
            msg = BuildMessageFromHeader(response);
          else
            msg = string.Format(Error_Message_Action, AppResources.DeliverPositionFromOrder) + " " + string.Format(RequestReturnedStatuscode0, response.StatusCode);
          _messageService.Error(msg);
          return null;
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<DeliveryData> GetDeliveryData(int deliveryId)
    {
      var request = new RestRequest("delivery/{deliveryid}/positions")
        .AddUrlSegment("deliveryid", deliveryId)
        .AddQueryParameter("userid", Token());

      try
      {
        DeliveryData response = await Client.GetAsync<DeliveryData>(request);
        return response;

      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<DeliveryEntryList> GetDeliveryEntryList(string filterCnr, string filterCustomer, string filterOrderCnr, string filterStatus)
    {
      var request = new RestRequest("delivery/list")
        .AddQueryParameter("userid", Token());

      if (!string.IsNullOrEmpty(filterCnr))
        request.AddQueryParameter("filter_cnr", filterCnr);

      if (!string.IsNullOrEmpty(filterCustomer))
        request.AddQueryParameter("filter_customer", filterCustomer);

      if (!string.IsNullOrEmpty(filterOrderCnr))
        request.AddQueryParameter("filter_ordercnr", filterOrderCnr);

      if (!string.IsNullOrEmpty(filterStatus))
        request.AddQueryParameter("filter_status", filterStatus);

      try
      {
        DeliveryEntryList response = await Client.GetAsync<DeliveryEntryList>(request);
        return response;

      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<DeliveryEntryList> GetDeliveryEntryListForCustomer(string customerNumber)
    {
      var request = new RestRequest("delivery/customerdeliverables/{customercnr}")
        .AddUrlSegment("customercnr", customerNumber)
        .AddQueryParameter("userid", Token());

      try
      {
        DeliveryEntryList response = await Client.GetAsync<DeliveryEntryList>(request);
        return response;

      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<PrintResult> Print(int deliveryId)
    {
      var request = new RestRequest("delivery/print")
        .AddQueryParameter("deliveryid", deliveryId)
        .AddQueryParameter("userid", Token());

      return await DoPrintRequest(request);
    }

    public async Task<PrintResult> PrintDispatchLabel(int deliveryId, bool activateDelivery)
    {
      var request = new RestRequest("delivery/printdispatchlabel")
        .AddQueryParameter("deliveryid", deliveryId)
        .AddQueryParameter("activateDelivery", activateDelivery)
        .AddQueryParameter("userid", Token());

      return await DoPrintRequest(request);
    }

    public async Task<PrintResult> PrintGoodsIssueLabel(int deliveryId, bool activateDelivery)
    {
      var request = new RestRequest("delivery/printgoodsissuelabel")
        .AddQueryParameter("deliveryid", deliveryId)
        .AddQueryParameter("activateDelivery", activateDelivery)
        .AddQueryParameter("userid", Token());

      return await DoPrintRequest(request);
    }

    private async Task<PrintResult> DoPrintRequest(RestRequest request)
    {
      try
      {
        var ret = await Client.ExecuteGetAsync(request);
        if (ret.IsSuccessful)
          return new PrintResult(true, ret.StatusCode, string.Empty, 0);
        else
          if (ret.StatusCode == HttpStatusCode.ExpectationFailed)
        {
          return new PrintResult(false, ret.StatusCode, BuildMessageFromHeader(ret), GetHvExtendedErrorCode(ret));
        }
        return new PrintResult(false, ret.StatusCode, BuildMessageFromHeader(ret), GetHvExtendedErrorCode(ret));

      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return new PrintResult(false, HttpStatusCode.InternalServerError, ex.Message);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return new PrintResult(false, HttpStatusCode.InternalServerError, ex.Message);
      }
    }

  }
}
