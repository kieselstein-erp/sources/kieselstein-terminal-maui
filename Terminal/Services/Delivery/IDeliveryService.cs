﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;

namespace KieselsteinErp.Terminal.Services.Delivery
{
  public interface IDeliveryService
  {
    Task<CreatedDeliveryPositionEntry> DeliverPositionFromOrder(CreateDeliveryPositionEntry deliveryPositionEntry);
    Task<DeliveryEntryList> GetDeliveryEntryList(string filterCnr, string filterCustomer, string filterOrderCnr, string filterStatus);
    Task<DeliverableDeliveryEntry> DeliverableDeliveryEntry(string deliveryCnr);
    Task<DeliveryData> GetDeliveryData(int deliveryId);
    Task<CreatedDeliveryPositionEntry> CreateItemPosition(int deliveryId, CreateDeliveryItemPositionEntry createDeliveryItemPositionEntry);
    Task<CreatedDeliveryPositionEntry> CreatePosition(CreateItemDeliveryPositionEntry createDeliveryItemPositionEntry);
    Task<BookingResult> DeletePosition(int positionId);
    Task<PrintResult> Print(int deliveryId);
    Task<PrintResult> PrintDispatchLabel(int deliveryId, bool activateDelivery);
    Task<PrintResult> PrintGoodsIssueLabel(int deliveryId, bool activateDelivery);
    Task<DeliveryEntryList> GetDeliveryEntryListForCustomer(string customerNumber);
    Task<CreatedDeliveryPositionEntry> CreateCustomerPosition(int deliveryId, CreateItemCustomerDeliveryPositionEntry createItemCustomerDeliveryPositionEntry);
  }
}
