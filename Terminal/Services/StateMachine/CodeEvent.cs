﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services.Machine;
using KieselsteinErp.Terminal.Services.Order;
using KieselsteinErp.Terminal.Services.Production;
using KieselsteinErp.Terminal.Services.Worktime;
using KieselsteinErp.Terminal.ViewModels;

namespace KieselsteinErp.Terminal.Services.StateMachine
{
  public record CodeEvent
  {
  }

  public record UserAuthEvent : CodeEvent
  {

    public UserAuthEvent(string idCard)
    {
      IdCard = idCard;
    }
    public string IdCard { get; private set; }
  }

  public record UserLoginEvent : CodeEvent
  {
    public UserLoginEvent(string loginData)
    {
      LoginData = loginData;
    }
  
    public string LoginData { get; private set; }
  }

  public record ProductionEvent : CodeEvent
  {
    public ProductionEvent(ProductionEventType eventType)
    {
      EventType = eventType;
    }
    public ProductionEvent(ProductionEventType eventType, bool noUnidentify, string barcode) : this(eventType)
    {
      Barcode = barcode;
      NoUnidentify = noUnidentify;
    }
    public ProductionEvent(ProductionEventType eventType, bool noUnidentify, string barcode, string batchnr) : this(eventType, noUnidentify, barcode)
    {
      Batchnr = batchnr;
    }
    public ProductionEvent(ProductionEventType eventType, bool noUnidentify, decimal amount, string barcode) : this(eventType, noUnidentify, barcode)
    {
      Amount = amount;
    }
    public ProductionEvent(ProductionEventType eventType, bool noUnidentify, decimal amount, decimal amountDefect, string barcode, bool printLabel, string idCard) : this(eventType, noUnidentify, amount, barcode)
    {
      AmountDefect = amountDefect;
      PrintLabel = printLabel;
      IdCard = idCard;
    }
    public ProductionEvent(ProductionEventType eventType, bool noUnidentify, decimal amount, decimal amountDefect, string barcode, bool printLabel, List<IdentityAmountEntry> identities, string idCard) : this(eventType, noUnidentify, amount, amountDefect, barcode, printLabel, idCard)
    {
      Identities = identities;
    }
    public ProductionEvent(ProductionEventType eventType, bool noUnidentify, decimal amount, decimal amountDefect, string barcode, bool printLabel, List<IdentityAmountEntry> identities, List<ItemIdentityAmountEntry> assemblies, string idCard) : this(eventType, noUnidentify, amount, amountDefect, barcode, printLabel, identities, idCard)
    {
      Assemblies = assemblies;
    }
    public ProductionEvent(ProductionEventType eventType, bool noUnidentify, decimal amount, string barcode, string idCard) : this(eventType, noUnidentify, barcode)
    {
      Amount = amount;
      IdCard = idCard;
    }

    public ProductionEvent(ProductionEventType eventType, bool noUnidentify, List<PieceConfirmation> pieceConfirmations) : this(eventType)
    {
      NoUnidentify = noUnidentify;
      PieceConfirmations = pieceConfirmations;
    }

    public bool PrintLabel { get; private set; }
    public ProductionEventType EventType { get; private set; }

    public bool NoUnidentify { get; private set; }
    public decimal Amount { get; private set; }
    public decimal AmountDefect { get; private set; }
    public string Barcode { get; private set; }
    public string Batchnr { get; private set; } = null;
    public string IdCard { get; private set; }
    public List<IdentityAmountEntry> Identities { get; private set; }
    public List<ItemIdentityAmountEntry> Assemblies { get; private set; }
    public List<PieceConfirmation> PieceConfirmations { get; private set; }

  }

  public record OrderEvent : CodeEvent
  {
    public OrderEvent(OrderEventType eventType, string readerString)
    {
      EventType = eventType;
      ReaderString = readerString;
    }

    public OrderEventType EventType { get; private set; }

    public string ReaderString { get; private set; }
  }
  public record MachineEvent : CodeEvent
  {
    public MachineEvent(MachineEventType eventType, string readerString)
    {
      EventType = eventType;
      ReaderString = readerString;
    }
    public MachineEventType EventType { get; private set; }
    public string ReaderString { get; private set; }
  }

  public record WorkTimeEvent : CodeEvent
  {
    public WorkTimeEvent(WorkTimeEventType eventType)
    {
      EventType = eventType;
    }

    public WorkTimeEvent(WorkTimeEventType eventType, bool noUnidentify)
    {
      EventType = eventType;
      NoUnidentify = noUnidentify;
    }

    public WorkTimeEvent(WorkTimeEventType eventType, bool noUnidentify, ProductionWorkstep productionWorkstep, ProductionRecordingEntry worktimeToBook) : this(eventType, noUnidentify)
    {
      ProductionWorkstep = productionWorkstep;
      WorktimeToBook = worktimeToBook;
    }

    public WorkTimeEvent(WorkTimeEventType eventType, bool noUnidentify, int saldoOffset) : this(eventType, noUnidentify)
    {
      SaldoOffset = saldoOffset;
    }

    public WorkTimeEvent(WorkTimeEventType eventType, bool noUnidentify, string readerString) : this(eventType, noUnidentify)
    {
      ReaderString = readerString;
    }

    public WorkTimeEvent(WorkTimeEventType eventType, bool noUnidentify, List<PieceConfirmation> pieceConfirmations) : this(eventType, noUnidentify)
    {
      PieceConfirmations = pieceConfirmations;
    }

    public WorkTimeEventType EventType { get; private set; }
    public int SaldoOffset { get; private set; }
    public string ReaderString { get; private set; }
    public bool NoUnidentify { get; private set; }
    public ProductionWorkstep ProductionWorkstep { get; private set; }
    public ProductionRecordingEntry WorktimeToBook { get; private set; }
    public List<PieceConfirmation> PieceConfirmations { get; private set; }


  }

}
