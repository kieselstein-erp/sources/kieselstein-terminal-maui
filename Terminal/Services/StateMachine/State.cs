﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Services.StateMachine
{
  public class State
    {

        public State(StateType type, string userId, StaffEntry staffEntry)
        {
            Type = type;
            UserId = userId;
            StaffEntry = staffEntry;
        }

        public StateType Type
        { get; private set; }

        public string UserId
        { get; private set; }

        public StaffEntry StaffEntry
        { get; private set; }


    }


    public enum StateType
    {
        Unidentified,
        Identified
    }


}
