﻿namespace KieselsteinErp.Terminal.Services.StateMachine
{
    public interface IStateMachine
    {
        IObservable<State> State();
        Task OnCodeReceived(CodeEvent codeEvent);
    }
}
