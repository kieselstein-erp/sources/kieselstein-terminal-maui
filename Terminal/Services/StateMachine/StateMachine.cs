﻿using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Converter;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Resources.Localization;
using KieselsteinErp.Terminal.Services.Database;
using KieselsteinErp.Terminal.Services.Item;
using KieselsteinErp.Terminal.Services.Machine;
using KieselsteinErp.Terminal.Services.OfflineBooking;
using KieselsteinErp.Terminal.Services.Order;
using KieselsteinErp.Terminal.Services.Production;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.Staff;
using KieselsteinErp.Terminal.Services.User;
using KieselsteinErp.Terminal.Services.Worktime;
using KieselsteinErp.Terminal.Util;
using KieselsteinErp.Terminal.Views;
using Microsoft.Extensions.Logging;
using Open.Observable;
using System.Diagnostics;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.StateMachine
{
  public class StateMachine : IStateMachine
  {
    private readonly ILogger _logger;
    private readonly IWorktimeService _worktimeService;
    private readonly IStaffService _staffService;
    private readonly IMessageService _messageService;
    private readonly IProductionRestService _productionService;
    private readonly IMachineService _machineService;
    private readonly IOrderService _orderService;
    private readonly IItemV1Service _itemV1Service;
    private readonly ISettingsService _settingsService;
    private readonly IUserService _userService;
    private readonly IDatabaseService _databaseService;
    private string _lotString = null;
    private string _activityString = null;
    private string _machineIdNumber = null;
    private int _machineId = 0;
    private string _orderString = null;
    private bool _isOnline;
    private readonly ObservableValue<State> _observableState = new ObservableValue<State>();
    private bool _isMultipleLotWorkstepBookingActive = false;

    public StateMachine(ILogger<StateMachine> logger, IWorktimeService worktimeService, IStaffService staffService, IMessageService messageService, IProductionRestService productionService, IMachineService machineService,
        IUserService userService, IDatabaseService databaseService, IOrderService orderService, IItemV1Service itemV1Service, ISettingsService settingsService)
    {
      _logger = logger;
      _worktimeService = worktimeService;
      _staffService = staffService;
      _messageService = messageService;
      _productionService = productionService;
      _machineService = machineService;
      _userService = userService;
      _databaseService = databaseService;
      _orderService = orderService;
      _itemV1Service = itemV1Service;
      _settingsService = settingsService;

      _userService.OnLogonChanged += (s, e) =>
      {
        _isOnline = e;
        if (_isOnline) DeleteSavedInfoForOtherEvent(new CodeEvent());
      };
      _isOnline = !string.IsNullOrEmpty(_userService.Userid);
    }

    public IObservable<State> State()
    {
      return _observableState;
    }

    protected void ChangeState(State state)
    {
      _observableState.Post(state);
    }

    public async Task OnCodeReceived(CodeEvent codeEvent)
    {
#if WINDOWS
      if (ScreenSaverHelper.GetScreenSaverActive())
        if (ScreenSaverHelper.GetScreenSaverRunning())
          if (ScreenSaverHelper.GetForegroundWindow() != ((MauiWinUIWindow)App.Current.Windows[0].Handler.PlatformView).WindowHandle)
            ScreenSaverHelper.KillScreenSaver();
#endif
      var actual = _observableState.Value;
      if (codeEvent == null) throw new Exception("CodeEvent must not be null");
      else
      {
        if ((codeEvent.GetType() != typeof(UserAuthEvent) && codeEvent.GetType() != typeof(UserLoginEvent)) && (actual == null || actual.Type == StateType.Unidentified))
        {
          _messageService.Info(Prompt_Login);
          return;
        }
        //delete saved lot (needs activity or deliver), activity (needs lot) and machineIdNumber/machineId (needs lotWorkstep) if other event is called
        DeleteSavedInfoForOtherEvent(codeEvent);

        switch (codeEvent)
        {
          case UserAuthEvent userAuthEvent: //user auth event is the same for online and offline
            if (_isMultipleLotWorkstepBookingActive)
              ProductionEventBookMultipleWorksteps(new ProductionEvent(ProductionEventType.BookMultipleWorksteps));

            UserAuthEventHandler(userAuthEvent); break;

          case UserLoginEvent loginEvent:
            break;

          case OrderEvent orderEvent: //online and offline are handled in OrderEventHandler
            OrderEventHandler(orderEvent); break;

          case ProductionEvent productionEvent: //production events are not allowed offline
            if (_isOnline) ProductionEventHandler(productionEvent);
            else NotAllowedOffline(String.Format(Error_Not_Allowed_Offline, AppResources.Production));
            break;

          case MachineEvent machineEvent: //_machine event is the same for online and offline
            MachineEventHandler(machineEvent); break;

          case WorkTimeEvent workTimeEvent: //online and offline handled in the WorkTimeEventHandler
            WorkTimeEventHandler(workTimeEvent, actual); break;

          default:
            throw new NotImplementedException($"Unkown event {codeEvent}.");
        }
      }
    }

    private void DeleteSavedInfoForOtherEvent(CodeEvent codeEvent)
    {
      switch (codeEvent)
      {
        case ProductionEvent productionEvent:
          if (!(productionEvent.EventType == ProductionEventType.Deliver || productionEvent.EventType == ProductionEventType.ChangeAmount))
            _lotString = null;
          _activityString = null;
          _machineIdNumber = null;
          _machineId = 0;
          _orderString = null;
          break;

        case WorkTimeEvent workTimeEvent:
          if (workTimeEvent.EventType != WorkTimeEventType.Activity)
            _lotString = _orderString = null;
          if (workTimeEvent.EventType != WorkTimeEventType.Lot)
            _activityString = null;
          if (workTimeEvent.EventType != WorkTimeEventType.LotWorkstep)
          {
            _machineIdNumber = null;
            _machineId = 0;
          }
          break;

        default:
          _lotString = _activityString = _machineIdNumber = _orderString = null;
          _machineId = 0;
          break;
      }
    }

    #region "worktime events"
    private void WorkTimeEventHandler(WorkTimeEvent workTimeEvent, State actual)
    {
      if (_isMultipleLotWorkstepBookingActive)
      {
        if (workTimeEvent.EventType == WorkTimeEventType.LotWorkstep)
          WeakReferenceMessenger.Default.Send(workTimeEvent);
        return;
      }

      switch (workTimeEvent.EventType)
      {
        case WorkTimeEventType.Balance:
          if (_isOnline) WorkTimeEventBalance(workTimeEvent, actual);
          else NotAllowedOffline(Error_Balance_Report_Offline);
          break;

        case WorkTimeEventType.MonthlyReport:
          if (_isOnline) WorkTimeEventMonthlyReport(workTimeEvent, actual);
          else NotAllowedOffline(Error_Balance_Report_Offline);
          break;

        case WorkTimeEventType.Lot:
          if (_isOnline) WorkTimeEventLot(workTimeEvent, actual);
          else WorkTimeEventLotOffline(workTimeEvent, actual);
          break;

        case WorkTimeEventType.Activity:
          if (_isOnline) WorkTimeEventActivity(workTimeEvent, actual);
          else WorkTimeEventActivityOffline(workTimeEvent, actual);
          break;

        case WorkTimeEventType.OrderActivity:
          if (_isOnline) WorkTimeEventOrderActivity(workTimeEvent, actual);
          else WorkTimeEventOrderActivityOffline(workTimeEvent, actual);
          break;

        case WorkTimeEventType.LotActivity:
        case WorkTimeEventType.LotWorkstep:
          if (_isOnline) WorkTimeEventLotWorkstep(workTimeEvent, actual);
          else WorkTimeEventLotWorkstepOffline(workTimeEvent, actual);
          break;

        case WorkTimeEventType.PieceConfirmation:
          if (_isOnline) WorkTimeEventPieceConfirmation(workTimeEvent, actual);
          else
          {
            WeakReferenceMessenger.Default.Send(new LoadingCircleMessage(false));
            NotAllowedOffline(String.Format(Error_Not_Allowed_Offline, AppResources.PieceConfirmation));
          }
          break;

        case WorkTimeEventType.ContinueWorksteps:
          if (_isOnline) WorkTimeEventContinueWorksteps(workTimeEvent, actual);
          else NotAllowedOffline(ContinueOpenWorkstepsIsNotAllowedInOfflineMode);
          break;

        default:
          if (_isOnline) WorkTimeEventDefault(workTimeEvent, actual);
          else WorkTimeEventDefaultOffline(workTimeEvent, actual);
          break;
      }
    }

    private async void WorkTimeEventBalance(WorkTimeEvent workTimeEvent, State actual)
    {
      TimeBalanceResult result = await _worktimeService.GetTimeBalanceAsync(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, actual.StaffEntry.personalNr);
      if (result.Success)
        WeakReferenceMessenger.Default.Send(new SaldoMessage(result.TimebalanceEntry));
      else
      {
        if (string.IsNullOrEmpty(result.Message))
          _messageService.ShowToast(GetTimeBalanceFailed, CommunityToolkit.Maui.Core.ToastDuration.Short);
        else
          _messageService.Error(result.Message);
        Unidentify();
        WeakReferenceMessenger.Default.Send(new LoadingCircleMessage(false));
      }
    }

    private async void WorkTimeEventMonthlyReport(WorkTimeEvent workTimeEvent, State actual)
    {
      DateTime saldoDate = DateTime.Now.AddMonths(workTimeEvent.SaldoOffset);
      MonthlyReportEntry report = await _worktimeService.GetMonthlyReportAsync(saldoDate.Month,
                  saldoDate.Year, actual.StaffEntry.id, "THIS_PERSON", "NAME", true);
      // Debug.Print(report?.pdfContent?.Length + "Bytes");
      if (report?.pdfContent?.Length > 0)
      {
        try
        {
          using (FileStream fileStream = new FileStream("c:\\temp\\1.pdf", FileMode.Create, FileAccess.Write, FileShare.None))
          {
            byte[] buffer = System.Convert.FromBase64String(report.pdfContent);
            fileStream.Write(buffer, 0, buffer.Length);
          }

        }
        catch (Exception ex)
        {
          _logger.LogError(ex.Message, ex);
        }
        WeakReferenceMessenger.Default.Send(new PdfMessage(report.pdfContent));
      }
      else
      {
        _messageService.ShowToast(GetTimeBalanceFailed, CommunityToolkit.Maui.Core.ToastDuration.Short);
        WeakReferenceMessenger.Default.Send(new PdfMessage(string.Empty));	// send empty pdf to hide loading circle
      }
    }

    private async void WorkTimeEventLot(WorkTimeEvent workTimeEvent, State actual)
    {
      if (!await LotValid(workTimeEvent.ReaderString))
        return;
      _lotString = workTimeEvent.ReaderString;
      if (_activityString != null)
      {
        BookWorktime(actual, string.Format(BookLotActivity, _lotString, _activityString), "$L" + _lotString + _activityString);
        _lotString = _activityString = null;
      }
    }

    private async Task<bool> LotValid(string lotString)
    {
      ProductionEntry productionEntry = await _productionService.GetProductionEntry(lotString);
      if (productionEntry != null)
        WeakReferenceMessenger.Default.Send(new LotMessage(lotString, productionEntry));

      LotState lotState = await _productionService.LotValid(lotString);
      switch (lotState)
      {
        case LotState.NOTFOUND:
          _messageService.Error(string.Format(Error_Lot_Not_Found, lotString));
          return false;
        case LotState.NEW:
        case LotState.CANCELLED:
        case LotState.STOPPED:
          _messageService.Error(string.Format(Error_Booking_For_Lot_Not_Allowed, lotString));
          return false;
        case LotState.DONE:
          _messageService.Error(string.Format(Error_Lot_Already_Done, lotString));
          return false;
      }
      return true;
    }

    private async void WorkTimeEventLotOffline(WorkTimeEvent workTimeEvent, State actual)
    {
      _lotString = workTimeEvent.ReaderString;
      if (_activityString != null)
      {
        await SaveBookingOffline("$W" + _lotString + _activityString, OfflineBookingType.LotActivity, actual);
        _lotString = _activityString = null;
      }
    }

    private async void WorkTimeEventActivity(WorkTimeEvent workTimeEvent, State actual)
    {
      _activityString = workTimeEvent.ReaderString;
      if (_lotString != null)
      {
        BookWorktime(actual, string.Format(BookLotActivity, _lotString, _activityString), "$L" + _lotString + _activityString);
        _activityString = null;
      }
      else if (_orderString != null)
      {
        string orderActivity = "$U" + _orderString + _activityString;
        BookWorktime(actual, string.Format(BookOrderActivity, orderActivity), orderActivity);
        _activityString = null;
      }
      _lotString = _orderString = null;
    }
    private async void WorkTimeEventActivityOffline(WorkTimeEvent workTimeEvent, State actual)
    {
      _activityString = workTimeEvent.ReaderString;
      if (_lotString != null)
      {
        await SaveBookingOffline("$L" + _lotString + _activityString, OfflineBookingType.LotActivity, actual);
        _activityString = null;
      }
      else if (_orderString != null)
      {
        string orderActivity = "$U" + _orderString + _activityString;
        await SaveBookingOffline(orderActivity, OfflineBookingType.Order, actual);
        _activityString = null;
      }
      _lotString = _orderString = null;
    }

    private async void WorkTimeEventOrderActivity(WorkTimeEvent workTimeEvent, State actual)
    {
      string orderActivity = "$U" + workTimeEvent.ReaderString;
      BookWorktime(actual, string.Format(BookOrderActivity, orderActivity), orderActivity);
    }

    private async void WorkTimeEventOrderActivityOffline(WorkTimeEvent workTimeEvent, State actual)
    {
      string orderActivity = "$U" + workTimeEvent.ReaderString;
      await SaveBookingOffline(orderActivity, OfflineBookingType.Order, actual);
    }

    private async void WorkTimeEventLotWorkstep(WorkTimeEvent workTimeEvent, State actual)
    {
      //machineId = 0 is the default _machine when booking a Worktime
      //override _machine only allowed when machinecode in lotWorkstep barcode is ??
      if (_machineId != 0 && !workTimeEvent.ReaderString.Contains("??"))
      {
        _machineId = 0;
        _messageService.Info(Machine_Override_Ignored);
      }

      bool needCheckGhostShift = _settingsService.IsPieceVisible;
      string toastMessage = (_machineId == 0) ? string.Format(BookLotWorkstep, workTimeEvent.ReaderString.Replace("$V", "").Replace("$W", ""))
          : string.Format(BookLotWorkstepMachine, workTimeEvent.ReaderString.Replace("$V", "").Replace("$W", ""), _machineIdNumber);
      if (workTimeEvent.ReaderString.EndsWith("$SP"))
      {
        toastMessage = toastMessage.Replace("$SP", "");
        toastMessage += "\n" + MachineStopped;
        needCheckGhostShift = false;
      }
      else if (workTimeEvent.ReaderString.EndsWith("$FT"))
      {
        toastMessage = toastMessage.Replace("$FT", "");
        toastMessage += "\n" + Complete;
        needCheckGhostShift = false;
      }
      else if (workTimeEvent.ReaderString.EndsWith("$ML"))
      {
        toastMessage = toastMessage.Replace("$ML", "");
        toastMessage += "\n" + MeasuringMachine;
        needCheckGhostShift = false;
      }
      
      if (needCheckGhostShift)
        if (await CheckGostShiftForMachine(_machineId, workTimeEvent, actual, toastMessage)) return;

      BookWorktime(actual, toastMessage, workTimeEvent.ReaderString, _machineId, workTimeEvent.NoUnidentify);
      _machineId = 0;
      _machineIdNumber = null;
    }

    private async Task<bool> CheckGostShiftForMachine(int machineId, WorkTimeEvent workTimeEvent, State actual, string toastMessage)
    {
      List<MachineEntry> machines = await _machineService.GetMachineEntriesGostShiftAsync();
      if (machines == null || machines.Count == 0) return false;

      BarcodeInfo barcodeInfo = await _worktimeService.GetBarcodeInfoAsync(workTimeEvent.ReaderString);
      if (barcodeInfo == null) return false;

      MachineEntry machineEntry = null;
      if (machineId != 0)
        machineEntry = machines.FirstOrDefault(o => o.id == machineId);
      else
      {
        if (barcodeInfo != null && barcodeInfo.productionWorkstepEntry.machineId != 0)
          machineEntry = machines.FirstOrDefault(o => o.id == barcodeInfo.productionWorkstepEntry.machineId);
      }
      
      if (machineEntry != null)
      {
        // do piece confirmation for ghost shift workstep
        OpenWorkEntryList openWorks = await _productionService.OpenWorkEntries(barcodeInfo.productionEntry.cnr, null, null, true, 200);
        OpenWorkEntry openWorkEntry = null;
        if (openWorks != null && openWorks.entries != null && openWorks.entries.Count > 0)
        {
          openWorkEntry = openWorks.entries.FirstOrDefault(o => o.id == machineEntry.productionWorkplanId);
        }
        if (openWorkEntry != null)
        {
          OpenWorkEntryList openWorkEntryList = new() { entries = [] };
          openWorkEntryList.entries.Add(openWorkEntry);
          ProductionRecordingEntry worktimeToBook = _productionService.BuildProductionrecordingEntry(barcodeInfo.productionEntry.id, 
            machineEntry.id, barcodeInfo.productionWorkstepEntry.id, barcodeInfo.productionWorkstepEntry.itemEntry.id, actual.StaffEntry.id);
          ProductionEntry productionEntry = await _productionService.GetProductionEntry(openWorkEntry.productionCnr, true);
          if (productionEntry == null)
          {
            _logger.LogError("ProductionEntry is null for " + openWorkEntry.productionCnr);
            return false;
          }
          ProductionWorkstepEntry productionWorkstepEntry = productionEntry.productionWorksteps.entries.FirstOrDefault(o => o.id == openWorkEntry.id);
          long jobTimeMs = productionWorkstepEntry.jobTimeMs;
          toastMessage = string.Format(Booking_Succesfully, toastMessage, actual.StaffEntry.name);
          WeakReferenceMessenger.Default.Send(new PieceConfirmationMessage(openWorkEntryList, WorkTimeEventType.PieceConfirmation, workTimeEvent.NoUnidentify, worktimeToBook,
            new GhostShiftInfo(machineEntry, jobTimeMs, toastMessage)));
          return true;
        }
      }
      return false;
    }

    private async void WorkTimeEventLotWorkstepOffline(WorkTimeEvent workTimeEvent, State actual)
    {
      if (_machineId != 0 && !workTimeEvent.ReaderString.Contains("??"))
      {
        _machineId = 0;
        _messageService.Info(Machine_Override_Ignored);
      }
      await SaveBookingOffline(workTimeEvent.ReaderString, OfflineBookingType.Workstep, actual, _machineId);

      _machineId = 0;
      _machineIdNumber = null;
    }

    private async void WorkTimeEventPieceConfirmation(WorkTimeEvent workTimeEvent, State actual)
    {
      OpenWorkEntryList openWorkEntryList = await _productionService.OpenWorkEntriesForStaff(actual.StaffEntry.personalNr, null, null, false);
      WeakReferenceMessenger.Default.Send(new LoadingCircleMessage(false));
      if (openWorkEntryList != null && openWorkEntryList.entries?.Count > 0)
      {
        if (workTimeEvent.ProductionWorkstep != null)
        {
          List<OpenWorkEntry> list = openWorkEntryList.entries.Where(o => o.id == workTimeEvent.ProductionWorkstep.WorkstepId).ToList();
          openWorkEntryList.entries = list;
        }
      }

      if (openWorkEntryList != null && openWorkEntryList.entries?.Count > 0)
        WeakReferenceMessenger.Default.Send(new PieceConfirmationMessage(openWorkEntryList, workTimeEvent.EventType, workTimeEvent.NoUnidentify, workTimeEvent.WorktimeToBook));
      else
      {
        _messageService.Info(NoActiveWorkstepsForPieceConfirmation);
        Unidentify(workTimeEvent.NoUnidentify);
      }
    }

    private async void WorkTimeEventDefault(WorkTimeEvent workTimeEvent, State actual)
    {
      if (workTimeEvent.EventType == WorkTimeEventType.Coming)
        if (await _worktimeService.CheckLastGoing(actual.StaffEntry.personalNr) == false)
          _messageService.Warn(WorkTimeEventDefault_GoesBookingMissingInTheLastFewDays);

      BookWorktime(workTimeEvent, actual);
    }

    private async void WorkTimeEventDefaultOffline(WorkTimeEvent workTimeEvent, State actual)
    {
      OfflineBookingType type = EnumConverter.GetAs<OfflineBookingType, WorkTimeEventType>(workTimeEvent.EventType);
      await SaveBookingOffline(type.ToString(), type, actual);
    }

    private void WorkTimeEventContinueWorksteps(WorkTimeEvent workTimeEvent, State actual)
    {
      ProductionEventBookMultipleWorksteps(new ProductionEvent(ProductionEventType.BookMultipleWorksteps, workTimeEvent.NoUnidentify, workTimeEvent.PieceConfirmations));
    }

    #endregion

    #region "production events"

    private void ProductionEventHandler(ProductionEvent productionEvent)
    {
      if (_isMultipleLotWorkstepBookingActive)
      {
        if (productionEvent.EventType == ProductionEventType.BookMultipleWorksteps)
          ProductionEventBookMultipleWorksteps(productionEvent);
      }
      else
      {
        switch (productionEvent.EventType)
        {
          case ProductionEventType.Deliver:
            ProductionEventDeliver(); break;

          case ProductionEventType.DeliverAmount:
            ProductionEventDeliverAmount(productionEvent); break;

          case ProductionEventType.DeliverAmountIdentities:
            ProductionEventDeliverAmountIdentities(productionEvent); break;

          case ProductionEventType.ChangeAmount:
            ProductionEventChangeAmount(); break;

          case ProductionEventType.DoChangeAmount:
            ProductionEventDoChangeAmount(productionEvent); break;

          case ProductionEventType.BookMaterialToLot:
            ProductionEventBookMaterialToLot(productionEvent); break;

          case ProductionEventType.BookMultipleWorksteps:
            ProductionEventBookMultipleWorksteps(productionEvent); break;
        }
      }
    }

    private async void ProductionEventDeliver()
    {
      if (_lotString == null)
        _messageService.Error(Error_Scan_Lot_First);
      else
      {
        ProductionEntry entry = await _productionService.GetProductionEntry(_lotString);
        string preferredType = null;
        // for field "preferedType" api call item/list must be used! otherwise it is always null
        if ((entry != null) && (entry.itemEntry != null))
        {
          var items = await _itemV1Service.GetItemsAsync(entry.itemEntry.cnr, null);
          if (items != null && items.entries.Count > 0)
            preferredType = ((ItemV1Entry)items.entries[0]).preferredType;
        }

        WeakReferenceMessenger.Default.Send(new DeliverMessage(_lotString, preferredType, entry?.itemEntry, false));

        _lotString = null;
      }
    }

    private async void ProductionEventDeliverAmount(ProductionEvent productionEvent)
    {
      BookingResult result = null;
      if (string.IsNullOrWhiteSpace(productionEvent.IdCard))
        result = await _productionService.BookDeliver(productionEvent.Amount, productionEvent.AmountDefect, productionEvent.Barcode, productionEvent.PrintLabel, productionEvent.IdCard);
      else
        result = await _productionService.BookDeliver(productionEvent.Amount, productionEvent.Barcode, productionEvent.IdCard);

      if (result.Success)
      {
        _messageService.ShowToast(result.Message, CommunityToolkit.Maui.Core.ToastDuration.Short);
        if (!result.PrintSuccess && result.PrintRequested)
          _messageService.ShowToast(result.Message, CommunityToolkit.Maui.Core.ToastDuration.Short);
      }
      else
        _messageService.Error(result.Message);

      Unidentify(productionEvent.NoUnidentify);
    }

    private async void ProductionEventDeliverAmountIdentities(ProductionEvent productionEvent)
    {
      BookingResult result = await _productionService.BookDeliverIdentities(productionEvent.Amount, productionEvent.AmountDefect, productionEvent.Barcode, productionEvent.PrintLabel, productionEvent.IdCard, productionEvent.Identities, productionEvent.Assemblies);

      if (result.Success)
      {
        _messageService.ShowToast(result.Message, CommunityToolkit.Maui.Core.ToastDuration.Short);
        if (!result.PrintSuccess && result.PrintRequested)
          _messageService.ShowToast(result.Message, CommunityToolkit.Maui.Core.ToastDuration.Short);
      }
      else
        _messageService.Error(result.Message);

      Unidentify(productionEvent.NoUnidentify);
    }


    private async void ProductionEventChangeAmount()
    {
      if (_lotString == null)
      {
        _messageService.Error(Error_Scan_Lot_First);
        return;
      }
      decimal? amount = await _productionService.LotAmount(_lotString);
      if (amount == null)
        _messageService.Error(string.Format(Error_Lot_Not_Found, _lotString));
      else
        WeakReferenceMessenger.Default.Send(new ChangeAmountMessage((decimal)amount, _lotString));
      _lotString = null;
    }

    private async void ProductionEventDoChangeAmount(ProductionEvent productionEvent)
    {
      BookingResult result = await _productionService.ChangeLotAmount(productionEvent.Barcode, productionEvent.Amount);
      if (result.Success)
        _messageService.ShowToast(result.Message, CommunityToolkit.Maui.Core.ToastDuration.Short);
      else
        _messageService.Error(result.Message);

      Unidentify(productionEvent.NoUnidentify);
    }

    private async void ProductionEventBookMaterialToLot(ProductionEvent productionEvent)
    {
      if (!await LotValid(productionEvent.Barcode))
        return;
      WeakReferenceMessenger.Default.Send(new LoadingCircleMessage(true));
      ProductionEntry productionEntry = await _productionService.GetProductionEntry(productionEvent.Barcode);
      ProductionTargetMaterialEntryList materialList = await _productionService.GetMaterialList(productionEntry.id);

      WeakReferenceMessenger.Default.Send(new LoadingCircleMessage(false));
      WeakReferenceMessenger.Default.Send(new BookMaterialToLotMessage(productionEntry, materialList, productionEvent.Batchnr));
    }

    private void ProductionEventBookMultipleWorksteps(ProductionEvent productionEvent)
    {
      if (productionEvent.PieceConfirmations != null)
      {
        _isMultipleLotWorkstepBookingActive = true;
        WeakReferenceMessenger.Default.Send(new BookMultipleWorkstepsMessage(_isMultipleLotWorkstepBookingActive, productionEvent.PieceConfirmations));
      }
      else
      {
        _isMultipleLotWorkstepBookingActive = !_isMultipleLotWorkstepBookingActive;
        WeakReferenceMessenger.Default.Send(new BookMultipleWorkstepsMessage(_isMultipleLotWorkstepBookingActive));
      }
    }

    #endregion

    #region "order events"
    private async void OrderEventHandler(OrderEvent orderEvent)
    {
      if (_isOnline)
      {
        OrderEntry orderEntry = await _orderService.GetOrderEntryByCnr(orderEvent.ReaderString);
        OrderState orderState = _orderService.GetOrderState(orderEntry);
        //The RestAPI does not deliver the results "Storniert" and "Erledigt" at the moment, only NOTFOUND
        switch (orderState)
        {
          case OrderState.NOTFOUND:
            _messageService.Error(string.Format(Error_Order_Not_Found, orderEvent.ReaderString));
            return;
          case OrderState.Storniert:
            _messageService.Error(string.Format(Error_Booking_For_Order_Not_Allowed, orderEvent.ReaderString));
            return;
          case OrderState.Erledigt:
            _messageService.Error(string.Format(Error_Order_Already_Done, orderEvent.ReaderString));
            return;
        }
        WeakReferenceMessenger.Default.Send(new OrderMessage(orderEvent.ReaderString, orderEntry));
      }
      _orderString = orderEvent.ReaderString;
    }

    #endregion

    #region "machine events"
    private async void MachineEventHandler(MachineEvent machineEvent)
    {
      _machineIdNumber = machineEvent.ReaderString;
      _machineId = _machineService.GetMachineIdByNumber(_machineIdNumber);
      if (_machineId == 0)
        _messageService.Error(string.Format(Error_Machine_Not_Found, _machineIdNumber));
      else
        if (_isMultipleLotWorkstepBookingActive)
        WeakReferenceMessenger.Default.Send(new MachineOverrideMessage(_machineId));
    }

    #endregion

    private async void UserAuthEventHandler(UserAuthEvent userAuthEvent)
    {
      if (userAuthEvent.IdCard == null)
        Unidentify();
      else
      {
        if (_isOnline && _userService.Userid == null)
        {
          // online and not logged in, try to use idCard vor login
          _userService.LogonIdCard(userAuthEvent.IdCard);
        }
        else
        {
          var staffEntry = _staffService.FindStaffByIdentyCnr(userAuthEvent.IdCard);
          if (staffEntry != null)
            _observableState.Post(new State(StateType.Identified, userAuthEvent.IdCard, staffEntry));
          else
            _messageService.Error(string.Format(Error_Staff_Not_Found, userAuthEvent.IdCard));
        }
      }
    }

    private async Task BookWorktime(State actual, string toastMessage, string barcode, int machineId = 0, bool noUnidentify = false)
    {
      BookingResult result = await _worktimeService.BookWorktime(toastMessage, barcode, machineId, actual.UserId, DateTime.Now);
      if (result.Success)
      {
        _messageService.ShowToast(result.Message, CommunityToolkit.Maui.Core.ToastDuration.Short);
        if (barcode.EndsWith("$ML") && result.StatusCode == System.Net.HttpStatusCode.Accepted)
          _messageService.Info(MessuringMachineStartBeforeEndOfLastTask);

        Unidentify(noUnidentify);
      }
      else
        _messageService.Error(result.Message);
        //_errorMessage.Post(Error_Booking_Failed);
    }

    private async Task BookWorktime(WorkTimeEvent workTimeEvent, State actual)
    {
      if ((_settingsService.IsPieceVisible) && (workTimeEvent.EventType == WorkTimeEventType.Stopping || workTimeEvent.EventType == WorkTimeEventType.Going))
      {
        OpenWorkEntryList openWorkEntryList = await _productionService.OpenWorkEntriesForStaff(actual.StaffEntry.personalNr, null, null, false);
        if (openWorkEntryList != null && openWorkEntryList.entries?.Count > 0)
        {
          PieceConfirmationQuery pieceConfirmationQuery = new PieceConfirmationQuery() 
            { Personalnummer = actual.StaffEntry.personalNr, WorkTimeEvent = workTimeEvent, OpenWorkEntryList = openWorkEntryList, IsGhostShiftConfirmation = false };

          //Dictionary<string, object> query = new Dictionary<string, object>
          //{
          //  [nameof(StaffEntry.personalNr)] = actual.StaffEntry.personalNr,
          //  [nameof(WorkTimeEvent)] = workTimeEvent,
          //  [nameof(OpenWorkEntryList)] = openWorkEntryList,
          //};
          await MainThread.InvokeOnMainThreadAsync(async () =>
          {
            Debug.Print("Open");
            await Shell.Current.GoToAsync(nameof(PieceConfirmationPage), new Dictionary<string, object> { [nameof(PieceConfirmationQuery)] = pieceConfirmationQuery });
            Debug.Print("Closed");
          });
          return;
        }
      }
      if (workTimeEvent.EventType == WorkTimeEventType.Autonomous)
      {
        workTimeEvent = new WorkTimeEvent(WorkTimeEventType.Stopping, workTimeEvent.NoUnidentify);
      }
      BookingResult result = await _worktimeService.BookWorktime(workTimeEvent, actual.UserId, DateTime.Now);
      if (result.Success)
      {
        _messageService.ShowToast(result.Message, CommunityToolkit.Maui.Core.ToastDuration.Short);
        Unidentify(workTimeEvent.NoUnidentify);
      }
      else
        _messageService.Error(result.Message);
      //_errorMessage.Post(Error_Booking_Failed);
    }

    private async Task SaveBookingOffline(string barcode, OfflineBookingType bookingType, State actual, int machineOverwrite = 0)
    {
      if (await _databaseService.SaveBookingOffline(barcode, machineOverwrite, (int)bookingType, DateTime.Now, actual.StaffEntry.identityCnr))
      {
        _messageService.ShowToast(string.Format(BookingSuccessfulOffline, _staffService.GetFormatedStaffName(actual.StaffEntry.identityCnr)), CommunityToolkit.Maui.Core.ToastDuration.Short);
        Unidentify();
      }
      else
        _messageService.Error(BookingFailedOffline);
    }

    private void NotAllowedOffline(string errorMsg)
    {
      _messageService.Error(errorMsg);
      DeleteSavedInfoForOtherEvent(new CodeEvent());
    }

    private void Unidentify(bool noUnidentify = false)
    {
      if (!_userService.IsAppLogon && !noUnidentify)
        _observableState.Post(new State(StateType.Unidentified, null, null));

      WeakReferenceMessenger.Default.Send(new RefreshMessage(true));
    }
  }
}
