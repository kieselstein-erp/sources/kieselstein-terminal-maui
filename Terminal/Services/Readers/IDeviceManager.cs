﻿namespace KieselsteinErp.Terminal.Services.Readers
{
  public interface IDeviceManager
  {
    void CloseAll();
    bool IsMemor1();
    bool IsZebraTC21_26();
    bool IsScanDevice();
    void PressScanButton(bool value);
  }
}
