﻿using CommunityToolkit.Mvvm.Messaging;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models.ScanLabels;
using KieselsteinErp.Terminal.Services.Machine;
using KieselsteinErp.Terminal.Services.Order;
using KieselsteinErp.Terminal.Services.Production;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.StateMachine;
using KieselsteinErp.Terminal.Services.Worktime;
using Microsoft.Extensions.Logging;

namespace KieselsteinErp.Terminal.Services.Readers
{
  public class Decoder : IDecoder
  {
    private readonly ILogger _logger;
    private IStateMachine _stateMachine;
    private bool _allowItemBarcodeWithoutPrefix = false;

    public Decoder(ILogger<Decoder> logger, IStateMachine stateMachine, ISettingsService settingsService)
    {
      _logger = logger;
      _stateMachine = stateMachine;
      _allowItemBarcodeWithoutPrefix = settingsService.AllowItemBarcodeWithoutPrefix;
    }

    public CodeEvent Decode(string code)
    {
      if (code == null)
        throw new Exception("Code must not be null");

      _logger.LogDebug(string.Format("Barcode read: {0}", code));

      if (code.StartsWith("\n"))
        //Zebra Bluetooth Scanner
        code = code[1..];

      IScanLabel scanLabel = null;
      if (ScanLabel_VDA4992.IsValid(code))
        scanLabel = new ScanLabel_VDA4992();
      else if (ScanLabel_MATLabel2_6.IsValid(code))
        scanLabel = new ScanLabel_MATLabel2_6();
      else if (ScanLabel_VDA4992intern.IsValid(code))
        scanLabel = new ScanLabel_VDA4992intern();

      if (scanLabel != null)
      {
        scanLabel.Init(code);
        WeakReferenceMessenger.Default.Send(new ItemVDAMessage(scanLabel));
        return new CodeEvent();
      }
      else
      {
        switch (code)
        {
          case "$KOMMT":
            return new WorkTimeEvent(WorkTimeEventType.Coming);

          case "$GEHT":
            return new WorkTimeEvent(WorkTimeEventType.Going);

          case "$UNTER":
          case "$PAUSE":
            return new WorkTimeEvent(WorkTimeEventType.Pausing);

          case "$ENDE":
            return new WorkTimeEvent(WorkTimeEventType.Stopping);

          case "$ABLIEFERN":
            return new ProductionEvent(ProductionEventType.Deliver);

          case "$GROESSEAENDERN":
            return new ProductionEvent(ProductionEventType.ChangeAmount);

          case "$PLUS":
            return new ProductionEvent(ProductionEventType.BookMultipleWorksteps);

          case string s when s.StartsWith("$A"):
            return new OrderEvent(OrderEventType.Order, s.Substring(2));

          case string s when s.StartsWith("$I"):
            if (s.Contains("|"))
            {
              string[] c = s.Split("|");
              switch (c.Length)
              {
                case 1:
                  WeakReferenceMessenger.Default.Send(new ItemMessage(c[0].Substring(2), null));
                  break;

                case >=2:
                  if (c.Length > 2) 
                    _logger.LogWarning($"Wrong $I code: {s}");
                  WeakReferenceMessenger.Default.Send(new ItemMessage(c[0].Substring(2), c[1]));
                  break;

                default:
                  _logger.LogError($"Wrong $I code: {s}");
                  break;
              }
            }
            else
              WeakReferenceMessenger.Default.Send(new ItemMessage(s.Substring(2), null));

            return new CodeEvent();

          case string s when s.StartsWith("$L"):
            if (s.EndsWith("$MAT"))
              return new ProductionEvent(ProductionEventType.BookMaterialToLot, false, s.Substring(2, s.Length - 2 - 4));
            return new WorkTimeEvent(WorkTimeEventType.Lot, false, s.Substring(2));

          case string s when s.StartsWith("$M"):
            return new MachineEvent(MachineEventType.MachineSelect, s.Substring(2));

          case string s when s.StartsWith("$P"):
            return new UserAuthEvent(s.Substring(2));

          case string s when s.StartsWith("$T"):
            return new WorkTimeEvent(WorkTimeEventType.Activity, false, s.Substring(2));

          case string s when s.StartsWith("$U"):
            return new WorkTimeEvent(WorkTimeEventType.OrderActivity, false, s.Substring(2));

          case string s when s.StartsWith("$V"):
            return new WorkTimeEvent(WorkTimeEventType.LotWorkstep, false, s);

          case string s when s.StartsWith("$W"):
            return new WorkTimeEvent(WorkTimeEventType.LotActivity, false, s);

          case string s when s.StartsWith("$O"):
            WeakReferenceMessenger.Default.Send(new StockPlaceMessage(s.Substring(2)));
            return new CodeEvent();

          case string s when s.StartsWith("$C1"):
            WeakReferenceMessenger.Default.Send(new UserLogonMessage(s.Substring(3)));
            return new UserLoginEvent(s.Substring(3));

          case string s when s.StartsWith("$D"):
            WeakReferenceMessenger.Default.Send(new DeliveryNoteMessage(s.Substring(2)));
            return new CodeEvent();

          case string s when s.StartsWith("$K"):
            WeakReferenceMessenger.Default.Send(new CustomerNumberMessage(s.Substring(2)));
            return new CodeEvent();

          case string s when s.StartsWith("$X"):
            WeakReferenceMessenger.Default.Send(new PdfReportMessage(s.Substring(2, 1), s.Substring(3)));
            return new CodeEvent();

          case string s when s.StartsWith("$B"):
            WeakReferenceMessenger.Default.Send(new PurchaseOrderNumberMessage(s.Substring(2)));
            return new CodeEvent();

          case string s when s.EndsWith("@") && (s.Count(c => c == '\t') == 2):
            // special item label with amount and batch number
            string[] ss = s.Split("\t");
            if (ss.Length == 3)
            {
              decimal amount = Decimal.Parse(ss[1]);
              WeakReferenceMessenger.Default.Send(new ItemMessage(ss[0], ss[2].Replace("@", ""), amount));
              return new CodeEvent();
            }
            break;
        }

        if (code.Contains('$'))
          throw new NotImplementedException(string.Format("Barcode {0} not implemented!", code));
        else
          if (_allowItemBarcodeWithoutPrefix)
            WeakReferenceMessenger.Default.Send(new ItemMessage(code, true));
          WeakReferenceMessenger.Default.Send(new UnidentifiedBarcodeMessage(code));
        return new CodeEvent();

      }
    }
  }
}
