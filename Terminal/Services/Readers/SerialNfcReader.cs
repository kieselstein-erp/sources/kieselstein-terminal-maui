﻿using KieselsteinErp.Terminal.Services.Settings;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.IO.Ports;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.Readers
{
  public enum SERIAL_NFC_READER_TYPES
  {
    MifareOEM,
    PROMAG_PCR300,
    ELATEX_TWN3
  }

  public class SerialNfcReader : IReader
  {
    private bool _continue;
    private string _port;
    private SerialPort _serialPort;
    private SERIAL_NFC_READER_TYPES _nfcReaderType;
    private bool _reverse;

    Thread readThread;
    private readonly IMessageService _messageService;
    private readonly ILogger<SerialNfcReader> _logger;

    public event EventHandler<CodeReadEventArgs> OnCodeRead;

    //protected virtual void OnCodeRead(CodeReadEventArgs e)
    //{
    //    CodeRead?.Invoke(this, e);
    //}

    public SerialNfcReader(ISettingsService settingsService, IMessageService messageService, ILogger<SerialNfcReader> logger)
    {
      if (SupportsPlatform(DeviceInfo.Platform))
      {
        _messageService = messageService;
        _logger = logger;

        Init(settingsService.NfcReaderSerialPort, settingsService.NfcReaderType, settingsService.IsNfcReverse);
      }
    }

    private void Init(string port, string nfctype, bool reverse)
    {
      if (string.IsNullOrEmpty(port))
        return;
      
      if (SerialPort.GetPortNames().Contains(port))
      {
        _port = port;
        _reverse = reverse;
        _nfcReaderType = (SERIAL_NFC_READER_TYPES)Enum.Parse(typeof(SERIAL_NFC_READER_TYPES), nfctype);
        _serialPort = new SerialPort(_port, 9600)
        {
          ReadTimeout = 500,
          WriteTimeout = 500
        };
        _serialPort.Open();

        _continue = true;
        switch (_nfcReaderType)
        {
          case SERIAL_NFC_READER_TYPES.MifareOEM:
            {
              //_serialPort.DataReceived += _serialPort_DataReceivedMifare;
              readThread = new Thread(ReadMifare);
              readThread.Start();
            }
            break;

          case SERIAL_NFC_READER_TYPES.PROMAG_PCR300:
            {
              //_serialPort.DataReceived += _serialPort_DataReceivedPromag;
              readThread = new Thread(ReadPromag);
              readThread.Start();
            }
            break;

          case SERIAL_NFC_READER_TYPES.ELATEX_TWN3:
            {
              //_serialPort.DataReceived += _serialPort_DataReceivedElatec;
              readThread = new Thread(ReadElatec);
              readThread.Start();
            }
            break;
        }
      }
      else
      {
        _messageService.Error(string.Format(Port_Not_Available, port));
      }

    }

    //private string code = string.Empty;
    //private void _serialPort_DataReceivedElatec(object sender, SerialDataReceivedEventArgs e)
    //{
    //    while (_serialPort.IsOpen && (_serialPort.BytesToRead > 0))
    //    {
    //        char c = (char)_serialPort.ReadChar();
    //        if (c == CR)
    //        {
    //            Debug.WriteLine($"Read Code={code}");
    //            CodeReadEventArgs args = new CodeReadEventArgs() { Code = code };
    //            code = string.Empty;
    //            OnCodeRead(null, args);
    //        }
    //        else
    //        {
    //            code += c;
    //        }
    //    }
    //}

    //private const char STX = (char)2;
    //private const char LF = (char)10;
    //private const char CR = (char)13;
    //private bool stxflag = false;
    //private void _serialPort_DataReceivedPromag(object sender, SerialDataReceivedEventArgs e)
    //{
    //    while (_serialPort.IsOpen && (_serialPort.BytesToRead > 0))
    //    {
    //        char c = (char)_serialPort.ReadChar();
    //        if (c == STX)
    //        {
    //            code = string.Empty;
    //            stxflag = true;
    //        }
    //        else if (c == LF)
    //        {
    //            stxflag = false;
    //            _serialPort.Write("B");
    //            Debug.WriteLine($"Read Code={code}");
    //            CodeReadEventArgs args = new CodeReadEventArgs() { Code = code };
    //            OnCodeRead(null, args);
    //        }
    //        else if (stxflag && c != CR)
    //        {
    //            code += c;
    //        }
    //    }
    //}

    //private int[] buf = new int[4];
    //private int index = 0;
    //private void _serialPort_DataReceivedMifare(object sender, SerialDataReceivedEventArgs e)
    //{
    //    while (_serialPort.IsOpen && (_serialPort.BytesToRead > 0))
    //    {
    //        int b = _serialPort.ReadByte();
    //        buf[index++] = b;
    //        if (index == 4)
    //        {
    //            index = 0;
    //            string code = string.Empty;
    //            Array.Reverse(buf);
    //            foreach (byte bb in buf)
    //            {
    //                code += string.Format("{0:X2}", bb);
    //            }
    //            Debug.WriteLine($"Read Code={code}");
    //            CodeReadEventArgs args = new CodeReadEventArgs() { Code = code };
    //            OnCodeRead(null, args);
    //        }
    //    }
    //}

    public void Close()
    {
      _continue = false;
      readThread?.Join();
      _serialPort?.Close();
    }

    public string Port
    {
      get
      {
        return _port;
      }
    }

    public SERIAL_NFC_READER_TYPES NfcReaderType
    {
      get
      {
        return _nfcReaderType;
      }
    }
    public void ReadMifare()
    {
      bool errorflag = false;
      int[] buf = new int[4];
      int index = 0;
      while (_continue)
      {
        try
        {
          int b = _serialPort.ReadByte();
          errorflag = false;
          buf[index++] = b;
          if (index == 4)
          {
            index = 0;
            string code = string.Empty;

            // For reasons of compatibility with old data, the serial number may be inverted
            // bytes from mifare must be reversed for compatibilty with promag reader
            // Attention: older RFSolutions moduls support only 8 byte cards, 14 byte card codes are truncated
            if (!_reverse)
              Array.Reverse(buf);

            foreach (int bb in buf)
            {
              code += string.Format("{0:X2}", bb);
            }
            Debug.WriteLine($"Read Code={code}");
            CodeReadEventArgs e = new() { Code = code };
            OnCodeRead?.Invoke(this, e);
          }
        }
        catch (TimeoutException) { }
        catch (OperationCanceledException) { }
        catch (InvalidOperationException) 
        {
          errorflag = HandleReconnect(errorflag);
        }
      }
    }

    private const char STX = (char)2;
    private const char LF = (char)10;
    private const char CR = (char)13;

    public void ReadPromag()
    {
      bool errorflag = false;
      bool stxflag = false;
      string code = string.Empty;

      while (_continue)
      {
        try
        {
          char c = (char)_serialPort.ReadChar();
          errorflag = false;
          if (c == STX)
          {
            code = string.Empty;
            stxflag = true;
          }
          else if (c == LF)
          {
            stxflag = false;
            _serialPort.Write("B");
            Debug.WriteLine($"Read Code={code}");

            // For reasons of compatibility with old data, the serial number may be inverted 
            if (_reverse)
              code = ReverseHex(code);

            CodeReadEventArgs e = new() { Code = code };
            OnCodeRead?.Invoke(this, e);
          }
          else if (stxflag && c != CR)
          {
            code += c;
          }
        }
        catch (TimeoutException) { }
        catch (OperationCanceledException) { }
        catch (InvalidOperationException)
        {
          errorflag = HandleReconnect(errorflag);
        }
      }
    }

    private string ReverseHex(string code)
    {
      // only codes with length 8 or 14 are reversed
      if (string.IsNullOrEmpty(code)) return code;
      try
      {
        if (code.Length == 14)
        {
          UInt64 l = Convert.ToUInt64(code, 16);
          byte[] b = BitConverter.GetBytes(l);
          b = b.Reverse().ToArray();
          l = BitConverter.ToUInt64(b, 0);
          l = l >> 8;
          code = l.ToString($"X14");
        }
        else if (code.Length == 8)
        {
          UInt32 l = Convert.ToUInt32(code, 16);
          byte[] b = BitConverter.GetBytes(l);
          b = b.Reverse().ToArray();
          l = BitConverter.ToUInt32(b, 0);
          code = l.ToString($"X8");
        }
      }
      catch (Exception e)
      {
        _logger.LogError(e, $"Reverse failed for code: {code}");
      }
      return code;
    }

    public void ReadElatec()
    {
      bool errorflag = false;
      string code = string.Empty;

      while (_continue)
      {
        try
        {
          char c = (char)_serialPort.ReadChar();
          errorflag = false;
          if (c == CR)
          {
            Debug.WriteLine($"Read Code={code}");

            // For reasons of compatibility with old data, the serial number may be inverted
            if (_reverse)
              code = ReverseHex(code);

            CodeReadEventArgs e = new() { Code = code };
            code = string.Empty;
            OnCodeRead?.Invoke(this, e);
          }
          else
          {
            code += c;
          }
        }
        catch (TimeoutException) { }
        catch (OperationCanceledException) { }
        catch (InvalidOperationException)
        {
          errorflag = HandleReconnect(errorflag);
        }
      }
    }

    private bool HandleReconnect(bool errorflag)
    {
      if (!errorflag)
      {
        _messageService.Error(string.Format(Port_Not_Available, _serialPort.PortName));
        errorflag = true;
      }
      else
      {
        if (SerialPort.GetPortNames().Contains(_serialPort.PortName))
          try
          {
            _serialPort.Open();
            errorflag = false;
          }
          catch (Exception) { }
      }

      return errorflag;
    }

    public string[] GetAvailablePorts()
    {
      if (DeviceInfo.Platform == DevicePlatform.WinUI)
      {
        return SerialPort.GetPortNames();
      }
      else
      {
        throw new NotImplementedException("Serialports sind nur in Windows verfügbar");
      }
    }

    public static string[] GetAvailableSerialNfcReaderTypes()
    {
      return (string[])Enum.GetNames(typeof(SERIAL_NFC_READER_TYPES));
    }

    public bool NeedsDecoding()
    {
      return false;
    }

    public bool SupportsPlatform(DevicePlatform devicePlatform)
    {
      if (devicePlatform == DevicePlatform.WinUI)
        return true;
      else
        return false;
    }

  }
}
