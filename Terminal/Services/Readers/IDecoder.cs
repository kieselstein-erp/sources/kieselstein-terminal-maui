﻿using KieselsteinErp.Terminal.Services.StateMachine;

namespace KieselsteinErp.Terminal.Services.Readers
{
    public interface IDecoder
    {
        CodeEvent Decode(string code);
    }
}