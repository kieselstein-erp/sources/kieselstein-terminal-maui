﻿using InTheHand.Net;
using InTheHand.Net.Bluetooth;
using InTheHand.Net.Sockets;
using KieselsteinErp.Terminal.Services.Settings;
using System.Diagnostics;
using System.Net.Sockets;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.Readers
{
  public class BluetoothBarcodeReader : IReader
  {
    private readonly IMessageService _messageService;
    private readonly ISettingsService _settingsService;
    private bool _continue;
    private string _port;
    private SERIAL_BARCODE_READER_TYPES _readerType;

#nullable enable
    private NetworkStream? _networkStream;
    private Thread? _streamReader;
#nullable disable
    private BluetoothClient _bluetoothClient;
    private BluetoothAddress _deviceAddress;
    private Timer _connectTimer;
    private bool _connectErrorShown;

    private bool IsSocketAccessible => _bluetoothClient != null && _bluetoothClient.Connected;

    public event EventHandler<CodeReadEventArgs> OnCodeRead;

    public BluetoothBarcodeReader(ISettingsService settingsService, IMessageService messageService)
    {
      _messageService = messageService;
      if (SupportsPlatform(DeviceInfo.Platform))
        _settingsService = settingsService;
        Init(settingsService.BluetoothDeviceAddress, settingsService.BluetoothDeviceName, SERIAL_BARCODE_READER_TYPES.Barcode_1D);
    }

    private void Init(string deviceAddressString, string deviceName, SERIAL_BARCODE_READER_TYPES readerType)
    {
      Debug.Print($"Try Connect to {deviceName}");
      if (string.IsNullOrEmpty(deviceAddressString))
        return;

      var radio = BluetoothRadio.Default;

      _readerType = readerType;
      try
      {
        _deviceAddress = new BluetoothAddress(Convert.ToUInt64(deviceAddressString, 16));
      }
      catch (Exception ex)
      {
        _messageService.Error(string.Format(InvalidBluetoothAddress01, deviceAddressString, ex.Message));
        return;
      }

      try
      {
        _bluetoothClient = new BluetoothClient();
        _bluetoothClient.Connect(_deviceAddress, BluetoothService.SerialPort);

        if (_bluetoothClient.Connected)
        {
          _networkStream = _bluetoothClient.GetStream();
          if (!_networkStream.CanWrite)
          {
            _bluetoothClient.Close();
            _messageService.Error(BluetoothServiceErrorNetworkStreamNotWritable);
            return;
          }
          if (!_networkStream.CanRead)
          {
            _bluetoothClient.Close();
            _messageService.Error(BluetoothServiceErrorNetworkStreamNotReadable);
            return;
          }
          if (_connectErrorShown)
          {
            _connectErrorShown = false;
            _messageService.ShowToast(string.Format(Init_BluetoothReader0Connected, deviceName), CommunityToolkit.Maui.Core.ToastDuration.Short);
          }
          _streamReader = new Thread(ReadBarcode);
          _streamReader.Name = "BluetoothReader";
          _streamReader.IsBackground = true;
          _continue = true;
          _streamReader.Start();
        }
      }
      catch (Exception ex)
      {
        if (!_connectErrorShown)
        {
          _messageService.Error(string.Format(ConnectToBluetoothDevice0FailedRN1, deviceName, ex.Message));
          _connectErrorShown = true;
        }
        _connectTimer = new Timer(new TimerCallback(OnConnectTimer), null, 5000, Timeout.Infinite);
      }
    }

    /// <summary>
    /// if connect failed retry every 5s
    /// </summary>
    /// <param name="state"></param>
    private void OnConnectTimer(object state)
    {
      DisableConnectTimer();
      Init(_settingsService.BluetoothDeviceAddress, _settingsService.BluetoothDeviceName, SERIAL_BARCODE_READER_TYPES.Barcode_1D);
    }

    private void DisableConnectTimer()
    {
      _connectTimer?.Change(Timeout.Infinite, Timeout.Infinite);
    }

    public void Close()
    {
      _continue = false;
      DisableConnectTimer() ;
      _streamReader?.Join(1000);
      _bluetoothClient?.Close();
    }

    public string Port
    {
      get
      {
        return _port;
      }
    }

    private const char CR = (char)13;

    public void ReadBarcode()
    {
      while (IsSocketAccessible && _continue)
      {
        try
        {
          byte[] bytes = new byte[1024];
          int bytesRead = _networkStream.Read(bytes, 0, bytes.Length);
          if (bytesRead> 0)
          {
            string code = System.Text.Encoding.ASCII.GetString(bytes, 0, bytesRead);
            Debug.WriteLine($"Read Barcode={code}");
            CodeReadEventArgs e = new CodeReadEventArgs() { Code = code };
            OnCodeRead?.Invoke(this, e);
          }
        }
        catch (ObjectDisposedException ex)
        {
          Debug.WriteLine(ex.Message, "Exception");
        }
        catch (IOException ex)
        {
          Debug.WriteLine(ex.Message, "Exception");
        }
        catch (System.Runtime.InteropServices.COMException ex)
        {
          Debug.WriteLine(ex.Message, "Exception");
        }
      }
    }

    public static string[] GetAvailableSerialBarcodereaderTypes()
    {
      return (string[])Enum.GetNames(typeof(SERIAL_BARCODE_READER_TYPES));
    }

    public bool NeedsDecoding()
    {
      return true;
    }

    public bool SupportsPlatform(DevicePlatform devicePlatform)
    {
      if (devicePlatform == DevicePlatform.WinUI || devicePlatform == DevicePlatform.Android)
        return true;
      else
        return false;
    }
  }
}
