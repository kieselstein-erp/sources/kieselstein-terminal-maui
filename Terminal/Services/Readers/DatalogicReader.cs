﻿using CommunityToolkit.Mvvm.Messaging;
using KieselsteinErp.Terminal.Messages;

namespace KieselsteinErp.Terminal.Services.Readers
{
  public class DatalogicReader : IReader
  {
    public event EventHandler<CodeReadEventArgs> OnCodeRead;

    private bool _continue;

    public DatalogicReader() 
    {
      if (SupportsPlatform(DeviceInfo.Platform))
        WeakReferenceMessenger.Default.Register<DatalogicDecoderMessage>(this, HandleDecoderMessage);
    }

    private void HandleDecoderMessage(object recipient, DatalogicDecoderMessage message)
    {
      CodeReadEventArgs e = new CodeReadEventArgs { Code=message.Value };
      OnCodeRead?.Invoke(this, e);
    }

    public void Close()
    {
      _continue = false;
      WeakReferenceMessenger.Default.Unregister<DatalogicDecoderMessage>(this);
      //readThread?.Join();
    }

    public bool NeedsDecoding()
    {
      return true;
    }

    public bool SupportsPlatform(DevicePlatform devicePlatform)
    {
      if ((devicePlatform == DevicePlatform.Android) && (DeviceInfo.Manufacturer == "Datalogic") && (DeviceInfo.Model == "Memor 1"))
        return true;
      else
        return false;
    }

  }
}
