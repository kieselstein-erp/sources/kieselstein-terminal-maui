﻿using CommunityToolkit.Mvvm.Messaging;
using KieselsteinErp.Terminal.Messages;

namespace KieselsteinErp.Terminal.Services.Readers
{
  public class ZebraReader : IReader
  {
    public event EventHandler<CodeReadEventArgs> OnCodeRead;

    private bool _continue;

    public ZebraReader()
    {
      if (SupportsPlatform(DeviceInfo.Platform))
        WeakReferenceMessenger.Default.Register<ZebraDecoderMessage>(this, HandleDecoderMessage);
    }

    private void HandleDecoderMessage(object recipient, ZebraDecoderMessage message)
    {
      CodeReadEventArgs e = new CodeReadEventArgs { Code = message.Value };
      OnCodeRead?.Invoke(this, e);
    }

    public void Close()
    {
      _continue = false;
      WeakReferenceMessenger.Default.Unregister<ZebraDecoderMessage>(this);
      //readThread?.Join();
    }

    public bool NeedsDecoding()
    {
      return true;
    }

    public bool SupportsPlatform(DevicePlatform devicePlatform)
    {
      if ((devicePlatform == DevicePlatform.Android) && (DeviceInfo.Manufacturer == "Zebra Technologies") && ((DeviceInfo.Model == "TC21") 
        || (DeviceInfo.Model == "TC22") || (DeviceInfo.Model == "TC26") || (DeviceInfo.Model == "TC27")))
        return true;
      else
        return false;
    }
  }
}
