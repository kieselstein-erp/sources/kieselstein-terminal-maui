﻿using CommunityToolkit.Mvvm.Messaging;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Services.StateMachine;
using Microsoft.Extensions.Logging;

namespace KieselsteinErp.Terminal.Services.Readers
{
  public class DeviceManager : IDeviceManager
  {
    private readonly IStateMachine _stateMachine;
    private readonly ILogger _logger;
    private readonly IEnumerable<IReader> _readers;
    private readonly IDecoder _decoder;
    private readonly IMessageService _messageService;

    private bool _scanButtonPressed = false;

    public DeviceManager(ILogger<DeviceManager> logger, IStateMachine stateMachine, IEnumerable<IReader> readers, IDecoder decoder, IMessageService messageService)
    {
      _stateMachine = stateMachine;
      _messageService = messageService;
      _logger = logger;
      _readers = new List<IReader>(readers);
      _decoder = decoder;

      _logger.LogInformation("Following readers found: {reader}", _readers);

      foreach (var reader in _readers)
      {
        if (reader.SupportsPlatform(DeviceInfo.Platform))
          reader.OnCodeRead += OnCodeRead;
      }

      if (IsMemor1())
        WeakReferenceMessenger.Default.Register<ScanButtonMessage>(this, HandleScanButtonMessage);
    }

    public void CloseAll()
    {
      foreach (var reader in _readers)
      {
        reader.OnCodeRead -= OnCodeRead;
        reader?.Close();
      }
    }

    public bool IsMemor1()
    {
      return ((DeviceInfo.Platform == DevicePlatform.Android) && (DeviceInfo.Manufacturer == "Datalogic") && (DeviceInfo.Model == "Memor 1"));
    }

    public bool IsZebraTC21_26()
    {
      return ((DeviceInfo.Platform == DevicePlatform.Android) && (DeviceInfo.Manufacturer == "Zebra Technologies") && ((DeviceInfo.Model == "TC21") || (DeviceInfo.Model == "TC26")));
    }

    public bool IsScanDevice()
    {
      return (IsMemor1() || IsZebraTC21_26());
    }

    public void PressScanButton(bool value)
    {
      WeakReferenceMessenger.Default.Send(new ScanButtonMessage(true));
      _scanButtonPressed = true;
    }

    private void HandleScanButtonMessage(object recipient, ScanButtonMessage message)
    {
      if (!message.Value)
        _scanButtonPressed = false;
    }

    private void OnCodeRead(object sender, CodeReadEventArgs e)
    {
      if (_scanButtonPressed)
        WeakReferenceMessenger.Default.Send(new ScanButtonMessage(false));

      if (sender == null) throw new ArgumentNullException(nameof(sender), "sender must not be null!");
      if (e == null) return;

      if (((IReader)sender).NeedsDecoding())
        try
        {
          _stateMachine.OnCodeReceived(_decoder.Decode(e.Code));
        }
        catch (NotImplementedException ex)
        {
          _messageService.Info(ex.Message);
        }
      else
        _stateMachine.OnCodeReceived(new UserAuthEvent(e.Code));
    }
  }
}
