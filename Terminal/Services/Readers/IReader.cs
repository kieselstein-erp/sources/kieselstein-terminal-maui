﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KieselsteinErp.Terminal.Services.Readers
{
  public interface IReader
  {
    public event EventHandler<CodeReadEventArgs> OnCodeRead;
    public void Close();

    public bool NeedsDecoding();

    public bool SupportsPlatform(DevicePlatform devicePlatform);
  }
}
