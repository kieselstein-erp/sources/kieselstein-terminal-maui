﻿using CommunityToolkit.Mvvm.Messaging;
using KieselsteinErp.Terminal.Messages;

namespace KieselsteinErp.Terminal.Services.Readers
{
  public class CameraReader : IReader
  {
    public event EventHandler<CodeReadEventArgs> OnCodeRead;

    public CameraReader()
    {
      if (SupportsPlatform(DeviceInfo.Platform))
        WeakReferenceMessenger.Default.Register<CameraBarcodeMessage>(this, HandleDecoderMessage);
    }

    private void HandleDecoderMessage(object recipient, CameraBarcodeMessage message)
    {
      CodeReadEventArgs e = new CodeReadEventArgs { Code = message.Value };
      OnCodeRead?.Invoke(this, e);
    }

    public void Close()
    {
      WeakReferenceMessenger.Default.Unregister<CameraBarcodeMessage>(this);
    }

    public bool NeedsDecoding()
    {
      return true;
    }

    public bool SupportsPlatform(DevicePlatform devicePlatform)
    {
      if ((devicePlatform == DevicePlatform.Android) && (DeviceInfo.Manufacturer == "Datalogic") && (DeviceInfo.Model == "Memor 1"))
        return false;
      else
        return true;
    }
  }
}
