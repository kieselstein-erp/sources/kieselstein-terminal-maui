﻿using CommunityToolkit.Mvvm.ComponentModel;
using KieselsteinErp.Terminal.Services.Settings;
using Plugin.NFC;
using System.Text;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.Readers
{

  partial class NfcReader : ObservableObject, IReader
  {
    private readonly IMessageService _messageService;

    public event EventHandler<CodeReadEventArgs> OnCodeRead;

    public const string MIME_TYPE = "application/com.companyname.nfcsample";

    private bool _eventsAlreadySubscribed;
    private readonly bool _isDeviceiOS;
    private bool _makeReadOnly;
    private NFCNdefTypeFormat _type;
    private bool _reverse = false;

    [ObservableProperty]
    private bool deviceIsListening;
    [ObservableProperty]
    private bool nfcIsEnabled;

    public NfcReader(IMessageService messageService, ISettingsService settingsService)
    {
      _messageService = messageService;
      _isDeviceiOS = (DeviceInfo.Platform == DevicePlatform.iOS);

      if (!settingsService.NfcReaderEnabled)
        return;

      _reverse = settingsService.IsNfcReverse;

      // In order to support Mifare Classic 1K tags (read/write), you must set legacy mode to true.
      CrossNFC.Legacy = true;

      if (CrossNFC.IsSupported)
      {
        if (!CrossNFC.Current.IsAvailable)
          _messageService.Error(NFCIsNotAvailable);

        NfcIsEnabled = CrossNFC.Current.IsEnabled;
        if (!NfcIsEnabled)
          _messageService.Error(NFCIsOff);

        StartService();
      }
    }

    private async void StartService()
    {
      await AutoStartAsync().ConfigureAwait(false);
    }

    async Task AutoStartAsync()
    {
      // Some delay to prevent Java.Lang.IllegalStateException "Foreground dispatch can only be enabled when your activity is resumed" on Android
      await Task.Delay(500);
      await StartListeningIfNotiOS();
    }

    public void Close()
    {
      Task.Run(() => StopListening());
    }

    public bool NeedsDecoding()
    {
      return false;
    }


    /// <summary>
    /// Subscribe to the NFC events
    /// </summary>
    void SubscribeEvents()
    {
      if (_eventsAlreadySubscribed)
        UnsubscribeEvents();

      _eventsAlreadySubscribed = true;

      CrossNFC.Current.OnMessageReceived += Current_OnMessageReceived;
      CrossNFC.Current.OnMessagePublished += Current_OnMessagePublished;
      CrossNFC.Current.OnTagDiscovered += Current_OnTagDiscovered;
      CrossNFC.Current.OnNfcStatusChanged += Current_OnNfcStatusChanged;
      CrossNFC.Current.OnTagListeningStatusChanged += Current_OnTagListeningStatusChanged;

      if (_isDeviceiOS)
        CrossNFC.Current.OniOSReadingSessionCancelled += Current_OniOSReadingSessionCancelled;
    }

    void UnsubscribeEvents()
    {
      CrossNFC.Current.OnMessageReceived -= Current_OnMessageReceived;
      CrossNFC.Current.OnMessagePublished -= Current_OnMessagePublished;
      CrossNFC.Current.OnTagDiscovered -= Current_OnTagDiscovered;
      CrossNFC.Current.OnNfcStatusChanged -= Current_OnNfcStatusChanged;
      CrossNFC.Current.OnTagListeningStatusChanged -= Current_OnTagListeningStatusChanged;

      if (_isDeviceiOS)
        CrossNFC.Current.OniOSReadingSessionCancelled -= Current_OniOSReadingSessionCancelled;

      _eventsAlreadySubscribed = false;
    }

    /// <summary>
    /// Event raised when Listener Status has changed
    /// </summary>
    /// <param name="isListening"></param>
    void Current_OnTagListeningStatusChanged(bool isListening) => DeviceIsListening = isListening;

    /// <summary>
    /// Event raised when NFC Status has changed
    /// </summary>
    /// <param name="isEnabled">NFC status</param>
    void Current_OnNfcStatusChanged(bool isEnabled)
    {
      NfcIsEnabled = isEnabled;
      _messageService.Info( $"NFC has been {(isEnabled ? "enabled" : "disabled")}");
    }

    /// <summary>
    /// Event raised when a NDEF message is received
    /// </summary>
    /// <param name="tagInfo">Received <see cref="ITagInfo"/></param>
    void Current_OnMessageReceived(ITagInfo tagInfo)
    {
      if (tagInfo == null)
      {
        _messageService.Info("No tag found");
        return;
      }

      // Customized serial number
      var identifier = tagInfo.Identifier;
      // For reasons of compatibility with old data, the serial number is inverted by default
      byte[] b = _reverse ? identifier.ToArray() : identifier.Reverse().ToArray();
      var serialNumber = NFCUtils.ByteArrayToHexString(b, ":");
      var title = !string.IsNullOrWhiteSpace(serialNumber) ? $"Tag [{serialNumber}]" : "Tag Info";

      if (!string.IsNullOrEmpty(tagInfo.SerialNumber))
      {
        CodeReadEventArgs codeReadEventArgs = new CodeReadEventArgs
        {
          Code = NFCUtils.ByteArrayToHexString(b) // tagInfo.SerialNumber;
        };
        OnCodeRead(this, codeReadEventArgs);
      }
      else
        _messageService.Info("Unsupported tag (app)");

      //if (!tagInfo.IsSupported)
      //{
      //    DelayedAlert(short_delay, "InfoMessage", "Unsupported tag (app)", "OK"); 
      //}
      //else if (tagInfo.IsEmpty)
      //{
      //    DelayedAlert(short_delay, "InfoMessage", "Empty tag", "OK"); 
      //}
      //else
      //{
      //    var first = tagInfo.Records[0];
      //    DelayedAlert(short_delay, "InfoMessage", GetMessage(first), "OK"); 
      //}
    }


    /// <summary>
    /// Event raised when user cancelled NFC session on iOS 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void Current_OniOSReadingSessionCancelled(object sender, EventArgs e) => System.Diagnostics.Debug.WriteLine("iOS NFC Session has been cancelled");

    /// <summary>
    /// Event raised when data has been published on the tag
    /// </summary>
    /// <param name="tagInfo">Published <see cref="ITagInfo"/></param>
    async void Current_OnMessagePublished(ITagInfo tagInfo)
    {
      try
      {
        //ChkReadOnly.IsChecked = false;
        CrossNFC.Current.StopPublishing();
        if (tagInfo.IsEmpty)
          _messageService.Info("Formatting tag operation successful");
        else
          _messageService.Info("Writing tag operation successful");
      }
      catch (Exception ex)
      {
        _messageService.Error(ex.Message);
      }
    }

    /// <summary>
    /// Event raised when a NFC Tag is discovered
    /// </summary>
    /// <param name="tagInfo"><see cref="ITagInfo"/> to be published</param>
    /// <param name="format">Format the tag</param>
    async void Current_OnTagDiscovered(ITagInfo tagInfo, bool format)
    {
      if (!CrossNFC.Current.IsWritingTagSupported)
      {
        _messageService.Info("Writing tag is not supported on this device");
        return;
      }

      try
      {
        NFCNdefRecord record = null;
        switch (_type)
        {
          case NFCNdefTypeFormat.WellKnown:
            record = new NFCNdefRecord
            {
              TypeFormat = NFCNdefTypeFormat.WellKnown,
              MimeType = MIME_TYPE,
              Payload = NFCUtils.EncodeToByteArray("Plugin.NFC is awesome!"),
              LanguageCode = "en"
            };
            break;
          case NFCNdefTypeFormat.Uri:
            record = new NFCNdefRecord
            {
              TypeFormat = NFCNdefTypeFormat.Uri,
              Payload = NFCUtils.EncodeToByteArray("https://github.com/franckbour/Plugin.NFC")
            };
            break;
          case NFCNdefTypeFormat.Mime:
            record = new NFCNdefRecord
            {
              TypeFormat = NFCNdefTypeFormat.Mime,
              MimeType = MIME_TYPE,
              Payload = NFCUtils.EncodeToByteArray("Plugin.NFC is awesome!")
            };
            break;
          default:
            break;
        }

        if (!format && record == null)
          throw new Exception("Record can't be null.");

        tagInfo.Records = new[] { record };

        if (format)
          CrossNFC.Current.ClearMessage(tagInfo);
        else
        {
          CrossNFC.Current.PublishMessage(tagInfo, _makeReadOnly);
        }
      }
      catch (Exception ex)
      {
        _messageService.Error(ex.Message);
      }
    }

    /// <summary>
    /// Start listening for NFC Tags when "READ TAG" button is clicked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    async void Button_Clicked_StartListening(object sender, System.EventArgs e) => await BeginListening();

    /// <summary>
    /// Stop listening for NFC tags
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    async void Button_Clicked_StopListening(object sender, System.EventArgs e) => await StopListening();

    /// <summary>
    /// Returns the tag information from NDEF record
    /// </summary>
    /// <param name="record"><see cref="NFCNdefRecord"/></param>
    /// <returns>The tag information</returns>
    string GetMessage(NFCNdefRecord record)
    {
      var message = $"Message: {record.Message}";
      message += Environment.NewLine;
      message += $"RawMessage: {Encoding.UTF8.GetString(record.Payload)}";
      message += Environment.NewLine;
      message += $"Type: {record.TypeFormat}";

      if (!string.IsNullOrWhiteSpace(record.MimeType))
      {
        message += Environment.NewLine;
        message += $"MimeType: {record.MimeType}";
      }

      return message;
    }

    async Task StartListeningIfNotiOS()
    {
      if (_isDeviceiOS)
      {
        SubscribeEvents();
        return;
      }
      await BeginListening();
    }


    /// <summary>
    /// Task to safely start listening for NFC Tags
    /// </summary>
    /// <returns>The task to be performed</returns>
    async Task BeginListening()
    {
      try
      {
        MainThread.BeginInvokeOnMainThread(() =>
        {
          SubscribeEvents();
          CrossNFC.Current.StartListening();
        });
      }
      catch (Exception ex)
      {
        _messageService.Error(ex.Message);
      }
    }

    /// <summary>
    /// Task to safely stop listening for NFC tags
    /// </summary>
    /// <returns>The task to be performed</returns>
    async Task StopListening()
    {
      if (CrossNFC.IsSupported)
      {
        try
        {
          MainThread.BeginInvokeOnMainThread(() =>
          {
            CrossNFC.Current.StopListening();
            UnsubscribeEvents();
          });
        }
        catch (Exception ex)
        {
          _messageService.Error(ex.Message);
        }
      }
    }


    public bool SupportsPlatform(DevicePlatform devicePlatform)
    {
      if (devicePlatform == DevicePlatform.Android || devicePlatform == DevicePlatform.iOS)
        return true;
      else
        return false;
    }
  }

}
