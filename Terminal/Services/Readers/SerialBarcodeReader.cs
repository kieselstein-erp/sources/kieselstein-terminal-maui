﻿using KieselsteinErp.Terminal.Services.Settings;
using System.Diagnostics;
using System.IO.Ports;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.Readers
{
  public enum SERIAL_BARCODE_READER_TYPES
  {
    Barcode_1D
  }

  public class SerialBarcodeReader : IReader
  {
    private bool _continue;
    private string _port;
    private SerialPort _serialPort;
    private SERIAL_BARCODE_READER_TYPES _readerType;

    Thread readThread;

    public event EventHandler<CodeReadEventArgs> OnCodeRead;

    public SerialBarcodeReader(ISettingsService settingsService, IMessageService messageService)
    {
      _messageService = messageService;
      if (SupportsPlatform(DeviceInfo.Platform))
        Init(settingsService.BarcodeReaderSerialPort, SERIAL_BARCODE_READER_TYPES.Barcode_1D);
    }

    private void Init(string port, SERIAL_BARCODE_READER_TYPES readerType)
    {
      if (string.IsNullOrEmpty(port))
        return;

      if (SerialPort.GetPortNames().Contains(port))
      {
        _port = port;
        _readerType = readerType;
        if (string.IsNullOrEmpty(port))
          return;

        _serialPort = new SerialPort(_port, 9600)
        {
          ReadTimeout = 500,
          WriteTimeout = 500
        };
        try
        {
          _serialPort.Open();

          _continue = true;
          switch (readerType)
          {
            case SERIAL_BARCODE_READER_TYPES.Barcode_1D:
              {
                readThread = new Thread(ReadBarcode1D);
                readThread.Start();
              }
              break;

            default:
              throw new NotImplementedException($"Unknown BarcodeReaderType {readerType}");
          }
        }
        catch (Exception ex)
        {
          _messageService.Error(ex.Message);
        }
      }
      else
      {
        _messageService.Error(string.Format(Port_Not_Available, port));
      }
    }

    public void Close()
    {
      _continue = false;
      readThread?.Join();
      _serialPort?.Close();
    }

    public string Port
    {
      get
      {
        return _port;
      }
    }

    private const char CR = (char)13;
    private readonly IMessageService _messageService;

    public void ReadBarcode1D()
    {
      bool errorflag = false;
      string code = string.Empty;

      while (_continue)
      {
        try
        {
          char c = (char)_serialPort.ReadChar();
          errorflag = false;
          if (c == CR && !(code == null))
          {
            Debug.WriteLine($"Read Barcode={code}");
            CodeReadEventArgs e = new CodeReadEventArgs() { Code = code };
            code = string.Empty;
            OnCodeRead?.Invoke(this, e);
          }
          else
          {
            code += c;
          }
        }
        catch (TimeoutException) { }
        catch (OperationCanceledException) { }
        catch (InvalidOperationException)
        {
          errorflag = HandleReconnect(errorflag);
        }
      }
    }

    private bool HandleReconnect(bool errorflag)
    {
      if (!errorflag)
      {
        _messageService.Error(string.Format(Port_Not_Available, _serialPort.PortName));
        errorflag = true;
      }
      else
      {
        if (SerialPort.GetPortNames().Contains(_serialPort.PortName))
          try
          {
            _serialPort.Open();
            errorflag = false;
          }
          catch (Exception) { }
      }

      return errorflag;
    }

    public static string[] GetAvailableSerialBarcodereaderTypes()
    {
      return (string[])Enum.GetNames(typeof(SERIAL_BARCODE_READER_TYPES));
    }

    public bool NeedsDecoding()
    {
      return true;
    }

    public bool SupportsPlatform(DevicePlatform devicePlatform)
    {
      if (devicePlatform == DevicePlatform.WinUI)
        return true;
      else
        return false;
    }
  }
}
