﻿using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.User;
using RestSharp;

namespace KieselsteinErp.Terminal.Services
{
  public class RestBase : IDisposable
  {
    protected readonly RestClient Client;
    protected readonly RestClient ClientSoapcalls;
    protected readonly RestClient ClientLTO;
    protected readonly string ServerUrl;
    private readonly IUserService _userService;

    protected const int HV_ERROR_CODE_VALIDATION_FAILED = 9;
    protected const int HV_ERROR_CODE_UNKNOWN_ENTITY = 4;

    protected const string HV_ERROR_CODE = "x-hv-error-code";
    protected const string HV_ERROR_KEY = "x-hv-error-key";
    protected const string HV_ERROR_VALUE = "x-hv-error-value";

    public RestBase(ISettingsService settingsService, IUserService userService) : this(settingsService, userService, false)
    {
    }

    public RestBase(ISettingsService settingsService, IUserService userService, bool useV11)
    {
      ServerUrl = settingsService.ServerUrl;
      _userService = userService;

      if (string.IsNullOrWhiteSpace(ServerUrl))
        ServerUrl = "http://localhost:8280";

      Client = new RestClient(new RestClientOptions(BuildBaseUrl(useV11)));
      ClientSoapcalls = new RestClient(new RestClientOptions(BuildBaseUrlSoapcalls()));
      RestClientOptions options = new(BuildBaseUrl(useV11));
      options.MaxTimeout = 200000;
      ClientLTO = new RestClient(options);
    }

    public string BuildBaseUrl(bool useV11)
    {
      // Get base address from preferences 
      string baseAddress = ServerUrl;

      // Support both versions of REST-API
      if (useV11)
      {
        return $"{baseAddress}/kieselstein-rest/services/rest/api/v11/";
      }

      return $"{baseAddress}/kieselstein-rest/services/rest/api/v1/";

    }

    public string BuildBaseUrlSoapcalls()
    {
      // Get base address from preferences 
      string baseAddress = ServerUrl;

      return $"{baseAddress}/kieselstein-rest/services/rest/api/soapcalls/";

    }

    protected string Token()
    {
      return _userService.Userid;
    }

    protected string BuildMessageFromHeader(RestResponse response)
    {
      string msg = string.Format("Request returned Statuscode {0}\n", response.StatusCode);
      RestSharp.HeaderParameter p = response.Headers.FirstOrDefault((p) => p.Name == "x-hv-error-translated");
      
      if (!string.IsNullOrEmpty(p?.Value.ToString()))
        msg = p.Value.ToString();
      else
        foreach (RestSharp.HeaderParameter parameter in response.Headers)
          if (parameter.Name.StartsWith("x-hv-error"))
            msg += $"{parameter.Name}: {parameter.Value}\n";

      return msg;
    }

    protected string GetHeaderValue(RestResponse response, string name)
    {
      RestSharp.HeaderParameter p = response.Headers.FirstOrDefault((p) => p.Name == name);
      if (p != null)
        return p.Value.ToString();
      else
        return string.Empty;
    }

    protected int? GetHvErrorCode(RestResponse response)
    {
      RestSharp.HeaderParameter p = response.Headers.FirstOrDefault((p) => p.Name == "x-hv-error-code");
      if (p == null)
        return null;
      else
        return int.Parse(p.Value);
    }

    protected int? GetHvExtendedErrorCode(RestResponse response)
    {
      RestSharp.HeaderParameter p = response.Headers.FirstOrDefault((p) => p.Name == "x-hv-error-code-extended");
      if (p == null)
        return null;
      else
        return int.Parse(p.Value);
    }

    public void Dispose()
    {
      Client?.Dispose();
      ClientSoapcalls?.Dispose();
      GC.SuppressFinalize(this);
    }
  }
}
