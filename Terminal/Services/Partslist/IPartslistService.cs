﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Services.Partslist
{
  public interface IPartslistService
  {
    Task<List<ProductionGroupEntry>> GetProductionGroupEntriesAsync();

    Task<ProductionGroupEntry> GetProductionGroupEntryAsync(int id);
  }
}
