﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.User;
using Microsoft.Extensions.Logging;
using RestSharp;

namespace KieselsteinErp.Terminal.Services.Partslist
{
  public class PartslistRestService : RestBase, IPartslistService
  {
    private readonly ILogger _logger;
    private readonly IMessageService _messageService;

    public PartslistRestService(ILogger<PartslistRestService> logger, ISettingsService settingsService, IUserService userService, IMessageService messageService) : base(settingsService, userService)
    {
      _logger = logger;
      _messageService = messageService;
    }

    public async Task<List<ProductionGroupEntry>> GetProductionGroupEntriesAsync()
    {
      var request = new RestRequest("partlist/productiongroup")
        .AddQueryParameter("userid", Token());
      try
      {
        ProductionGroupEntryList response = await Client.GetAsync<ProductionGroupEntryList>(request);
        return response.entries;

      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return [];
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return [];
      }
    }

    public async Task<ProductionGroupEntry> GetProductionGroupEntryAsync(int id)
    {
      List<ProductionGroupEntry> entries = await GetProductionGroupEntriesAsync();
      if (entries == null) return null;
      return entries.Find(o => o.id == id);
    }
  }
}
