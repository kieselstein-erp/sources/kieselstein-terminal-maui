﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services.StateMachine;
using System.Runtime.CompilerServices;

namespace KieselsteinErp.Terminal.Services.Worktime
{

  public interface IWorktimeService
  {
    public const string ActivityComming = "KOMMT";
    public const string ActivityGoing = "GEHT";

    Task<BookingResult> BookWorktime(WorkTimeEvent workTimeEvent, string staffCnr, DateTime timestamp);
    Task<BookingResult> BookWorktime(string toastMessage, string barcode, int machineId, string staffCnr, DateTime timestamp);
    Task<TimeBalanceResult> GetTimeBalanceAsync(int? day, int? month, int? year, string forStaffCnr);
    Task<MonthlyReportEntry> GetMonthlyReportAsync(int? month, int? year, int personalid, string selectoption, string sortoption, bool toendofmonth);
    Task<List<ZeitdatenEntry>> GetWorktimeEntriesAsync(int day, int month, int year, string staffCnr, int limit = 100);
    Task<BookingResult> BookWorktimeGoodBad(WorkTimeEvent workTimeEvent, string staffCnr, GoodBadEntryList goodBadEntries, DateTime timestamp);
    Task<bool> CheckLastGoing(string staffCnr);
    Task<BookingResult> BookSpecialTime(string toastMessage, int? staffId, SpecialTimeType specialTimeType, DateTime fromDate, DateTime toDate, bool halfDay);
    Task<BookingResult> BookSpecialTime(string toastMessage, int? staffId, SpecialTimesEntry specialTimesEntry);
    Task<BookingResult> BookProductionRecordingAsync(ProductionRecordingEntry productionRecordingEntry);
    Task<BookingResult> StartStopMachineAsync(bool isStop, int machineId, int workstepId, int staffId, DateTime dateTime);
    Task<List<TimeDistributionEntry>> GetTimeDistributionEntriesAsync(int? staffId, string staffCnr);
    Task<BarcodeInfo> GetBarcodeInfoAsync(string barcode);
    Task<BookingResult> BookTimeDistributionAsync(TimeDistributionRecordingEntry timeDistributionRecordingEntry);

  }
}
