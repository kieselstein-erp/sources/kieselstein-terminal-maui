﻿namespace KieselsteinErp.Terminal.Services.Worktime;

public enum WorkTimeEventType
{
    Coming,
    Pausing,
    Going,
    Stopping,
    Balance,
    MonthlyReport,
    Lot,
    Activity,
    BookLotActivity,
    LotWorkstep,
    LotActivity,
    OrderActivity,
    PieceConfirmation,
    SpecialTimes,
    Autonomous,
    ContinueWorksteps
}