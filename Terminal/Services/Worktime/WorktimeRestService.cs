﻿using Kieselstein.Models;
using KieselsteinErp.Terminal.Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.Staff;
using KieselsteinErp.Terminal.Services.StateMachine;
using KieselsteinErp.Terminal.Services.User;
using KieselsteinErp.Terminal.Util;
using Microsoft.Extensions.Logging;
using RestSharp;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services.Worktime
{
  public class WorktimeRestService : RestBase, IWorktimeService
  {
    private readonly IStaffService _staffService;
    private readonly IMessageService _messageService;
    private readonly ILogger _logger;

    public WorktimeRestService(ILogger<WorktimeRestService> logger, ISettingsService settingsService,
                IUserService userService,
                IMessageService messageService,
                IStaffService staffService) : base(settingsService, userService)
    {
      _logger = logger;
      _staffService = staffService;
      _messageService = messageService;
    }

    public async Task<BookingResult> BookWorktime(WorkTimeEvent workTimeEvent, string identityCnr, DateTime timestamp)
    {
      var entry = BuildTimeRecordEntry(identityCnr, timestamp);
      string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(entry);
      return workTimeEvent.EventType switch
      {
        WorkTimeEventType.Coming => await BookAsync(strJson, "worktime/coming", CheckIn, identityCnr),
        WorkTimeEventType.Pausing => await BookAsync(strJson, "worktime/pausing", Break, identityCnr),
        WorkTimeEventType.Going => await BookAsync(strJson, "worktime/going", CheckOut, identityCnr),
        WorkTimeEventType.Stopping => await BookAsync(strJson, "worktime/stopping", Stopping, identityCnr),
        _ => throw new NotImplementedException()
      };
    }

    public async Task<BookingResult> BookWorktime(string toastMessage, string barcode, int machineId, string identityCnr, DateTime timestamp)
    {
      BarcodeRecordingEntry barcodeRecordingEntry = BuildBarcodeRecordEntry(identityCnr, barcode, machineId, timestamp);

      string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(barcodeRecordingEntry);

      return await BookAsync(strJson, "worktime/barcode", toastMessage, identityCnr);
    }

    private async Task<BookingResult> BookAsync(string strJson, string resource, string toastMessage, string identityCnr, Tuple<string, string> queryParam = null, int? staffId = null)
    {
      try
      {
        var request = new RestRequest(resource, Method.Post).AddJsonBody(strJson);

        if (queryParam != null)
        {
          request.AddQueryParameter(queryParam.Item1, queryParam.Item2);
        }

        // Kieselstein API call /barcode needs this additional header "Content-Type", "application/json"
        // default header is "ContentType",  "application/json; charset=utf-8" response is 500 internal server error
        // and also field names must start with lower case

        request.AddHeader("Content-Type", "application/json");

        var response = Client.ExecutePost(request);

        if (response.IsSuccessful)
        {
          string name = string.Empty;
          if (identityCnr != null)
            name = _staffService.GetFormatedStaffName(identityCnr);
          else if (staffId != null)
            name = _staffService.GetFormatedStaffName((int)staffId);

          return new BookingResult(true, string.Format(Booking_Succesfully, toastMessage, name), response.StatusCode);
        }
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
            {
              //_messageService.Error(msg);
              return new BookingResult(false, msg, response.StatusCode);
            }
          }
        }
        return new BookingResult(false, string.Format(Error_Message_Action, Booking) + "Request returned Statuscode " + response.StatusCode, response.StatusCode);
      }
      catch (HttpRequestException e)
      {
        _logger.LogError(e.Message, e);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
      }
      return new BookingResult(false, string.Format(Error_Message_Action, Booking));
    }

    private TimeRecordingEntry BuildTimeRecordEntry(string identityCnr, DateTime timestamp)
    {
      StaffEntry staffEntry = _staffService.FindStaffByIdentyCnr(identityCnr);
      return new TimeRecordingEntry()
      {
        userId = Token(),
        forStaffId = staffEntry.id,
        where = DeviceInfo.Current.Name,
        year = timestamp.Year,
        month = timestamp.Month,
        day = timestamp.Day,
        hour = timestamp.Hour,
        minute = timestamp.Minute,
        second = timestamp.Second
      };
    }

    private BarcodeRecordingEntry BuildBarcodeRecordEntry(string identityCnr, string barcode, int machineId, DateTime timestamp)
    {
      StaffEntry staffEntry = _staffService.FindStaffByIdentyCnr(identityCnr);
      return new BarcodeRecordingEntry()
      {
        machineId = machineId,
        barcode = barcode,
        userId = Token(),
        forStaffId = staffEntry.id,
        where = DeviceInfo.Current.Name,
        year = timestamp.Year,
        month = timestamp.Month,
        day = timestamp.Day,
        hour = timestamp.Hour,
        minute = timestamp.Minute,
        second = timestamp.Second
      };
    }

    /// <summary>
    /// Liefert das Zeitsaldo des angeforderten Benutzers inklusive seines verfügbaren Urlaubs
    ///
    /// GetTimeBalance (/api/v1/worktime/timebalance)
    /// </summary>
    /// <param name="day">der Tag (1-31)</param>
    /// <param name="forStaffCnr">ist die Personalnummer für die der Zeitsaldo abgerufen werden soll.</param>
    /// <param name="month">ist das Monat (1-12)</param>
    /// <param name="year">ist das Jahr für das der Zeitsaldo abgerufen werden soll</param>
    public async Task<TimeBalanceResult> GetTimeBalanceAsync(int? day, int? month, int? year, string forStaffCnr)
    {
      if (Connectivity.Current.NetworkAccess < NetworkAccess.Local)
        return null;

      var request = new RestRequest("worktime/timebalance")
          .AddQueryParameter("userid", Token())
          .AddQueryParameter("forStaffCnr", forStaffCnr);

      if (day != null) request.AddQueryParameter("day", day.ToString());
      if (month != null) request.AddQueryParameter("month", month.ToString());
      if (year != null) request.AddQueryParameter("year", year.ToString());

      try
      {
        var response = await Client.ExecuteGetAsync<TimeBalanceEntry>(request);
        if (response.IsSuccessful)
          return new TimeBalanceResult(response.Data);
        else
        {
          string msg = string.Empty;
          if (response.Headers?.Count > 0)
            msg = BuildMessageFromHeader(response);
          if (string.IsNullOrEmpty(msg))
            msg = response.ErrorMessage;
          return new TimeBalanceResult(false, msg, response.StatusCode);
        }
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    /// <summary>
    /// GetMonthlyReport (/api/v1/worktime/monthlyreport)
    /// </summary>
    /// <param name="month"> month as for java calendar. Thus, 0 (=Jan) to 11 (=Dez)</param>
    /// <param name="personalid"></param>
    /// <param name="selectoption"></param>
    /// <param name="toendofmonth"></param>
    /// <param name="year"></param>
    public async Task<MonthlyReportEntry> GetMonthlyReportAsync(int? month, int? year, int personalid, string selectoption, string sortoption, bool toendofmonth)
    {
      //if (Connectivity.Current.NetworkAccess < NetworkAccess.Local)
      //  return null;
      var request = new RestRequest("worktime/monthlyreport")
          .AddQueryParameter("selectoption", selectoption)
          .AddQueryParameter("sortoption", sortoption)
          .AddQueryParameter("toendofmonth", toendofmonth)
          .AddQueryParameter("personalid", personalid)
          .AddQueryParameter("userid", Token());

      if (month != null) request.AddQueryParameter("month", (month - 1).ToString());
      if (year != null) request.AddQueryParameter("year", year.ToString());

      try
      {
        var response = await Client.GetAsync<MonthlyReportEntry>(request);
        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }

    }

    public async Task<List<ZeitdatenEntry>> GetWorktimeEntriesAsync(int day, int month, int year, string staffCnr, int limit = 100)
    {
      var request = new RestRequest($"worktime/{year}/{month}/{day}").AddQueryParameter("forStaffCnr", staffCnr).AddQueryParameter("userId", Token()).AddQueryParameter("limit", limit);

      try
      {
        var response = await Client.GetAsync<List<ZeitdatenEntry>>(request);

        DateTime date = new DateTime(year, month, day);
        foreach (var entry in response)
          entry.date = date;

        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<BookingResult> BookWorktimeGoodBad(WorkTimeEvent workTimeEvent, string staffCnr, GoodBadEntryList goodBadEntries, DateTime timestamp)
    {
      StaffEntry staffEntry = _staffService.FindStaffByStaffCnr(staffCnr);
      if (staffEntry == null)
      {
        return new BookingResult(false, string.Format(NoStaffFoundFor0, staffCnr));
      }

      TimeRecordingGoodBadEntry entry = new()
      {
        userId = Token(),
        forStaffId = staffEntry.id,
        where = DeviceInfo.Current.Name,
        year = timestamp.Year,
        month = timestamp.Month,
        day = timestamp.Day,
        hour = timestamp.Hour,
        minute = timestamp.Minute,
        second = timestamp.Second
      };
      entry.isGoing = workTimeEvent.EventType == WorkTimeEventType.Going;
      entry.goodBadEntries = goodBadEntries;

      string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(entry);
      return workTimeEvent.EventType switch
      {
        WorkTimeEventType.Going => await BookAsync(strJson, "worktime/goodbad", PieceConfirmationAndGO, staffEntry.identityCnr),
        WorkTimeEventType.Stopping => await BookAsync(strJson, "worktime/goodbad", PieceConfirmationAndSTOP, staffEntry.identityCnr),
        WorkTimeEventType.PieceConfirmation => await BookAsync(strJson, "worktime/goodbad", PieceConfirmationAndSTOP, staffEntry.identityCnr),
        _ => throw new NotImplementedException()
      };
    }

    public async Task<bool> CheckLastGoing(string staffCnr)
    {
      DateTime date = DateTime.Now;
      while (date > DateTime.Now.AddDays(-7))
      {
        List<ZeitdatenEntry> list = await GetWorktimeEntriesAsync(date.Day, date.Month, date.Year, staffCnr);
        if (list != null)
        {
          list = [.. list.OrderByDescending(o => o.date).ThenByDescending(o => o.time)];
          foreach (ZeitdatenEntry zeitdatenEntry in list)
          {
            if (zeitdatenEntry.activityCnr != null && zeitdatenEntry.activityCnr.ToUpper().Equals(IWorktimeService.ActivityGoing))
              return true;
            if (zeitdatenEntry.activityCnr != null && zeitdatenEntry.activityCnr.ToUpper().Equals(IWorktimeService.ActivityComming))
              return false;
          }
        }
        date = date.AddDays(-1);
      }
      return true;
    }

    public async Task<BookingResult> BookSpecialTime(string toastMessage, int? staffId, SpecialTimeType specialTimeType, DateTime fromDate, DateTime toDate, bool halfDay)
    {
      SpecialTimesEntry specialTimesEntry = new SpecialTimesEntry
      {
        timeType = specialTimeType.AsSpecialTimesEnumConstant(),
        fromDateMs = DateTimeHelper.ToMillisecondsSinceUnixEpoch(fromDate),
        toDateMs = DateTimeHelper.ToMillisecondsSinceUnixEpoch(toDate),
        halfDay = halfDay
      };

      return await BookSpecialTime(toastMessage, staffId, specialTimesEntry);
    }

    public async Task<BookingResult> BookSpecialTime(string toastMessage, int? staffId, SpecialTimesEntry specialTimesEntry)
    {
      SpecialTimesEntryList specialTimes = new SpecialTimesEntryList();
      specialTimes.entries = [];
      specialTimes.entries.Add(specialTimesEntry);

      if (staffId != null)
        specialTimes.entries[0].forStaffId = (int)staffId;

      string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(specialTimes);

      return await BookAsync(strJson, "worktime/specialtimes", toastMessage, null, new Tuple<string, string>("userid", Token()), staffId);
    }

    public async Task<BookingResult> BookProductionRecordingAsync(ProductionRecordingEntry productionRecordingEntry)
    {
      try
      {
        productionRecordingEntry.userId = Token();
        string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(productionRecordingEntry);
        var request = new RestRequest("worktime/production", Method.Post).AddJsonBody(strJson);

        // Kieselstein API call /barcode needs this additional header "Content-Type", "application/json"
        // default header is "ContentType",  "application/json; charset=utf-8" response is 500 internal server error
        // and also field names must start with lower case

        request.AddHeader("Content-Type", "application/json");

        var response = await Client.ExecutePostAsync(request);

        if (response.IsSuccessful)
        {
          return new BookingResult(true, "");
        }
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
            {
              _messageService.Error(msg);
              return new BookingResult(false, msg);
            }
          }
        }
        return new BookingResult(false, string.Format(Error_Message_Action, Booking) + "Request returned Statuscode " + response.StatusCode);
      }
      catch (HttpRequestException e)
      {
        _logger.LogError(e.Message, e);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
      }
      return new BookingResult(false, string.Format(Error_Message_Action, Booking));
    }

    public async Task<BookingResult> StartStopMachineAsync(bool isStop, int machineId, int workstepId, int staffId, DateTime dateTime)
    {
      try
      {
        MachineRecordingEntry machineRecordingEntry = new()
        {
          machineId = machineId,
          productionWorkplanId = workstepId,
          machineRecordingType = isStop ? MachineRecordingTypeConstants.STOP : MachineRecordingTypeConstants.START,
          forStaffId = staffId,
          year = dateTime.Year,
          month = dateTime.Month,
          day = dateTime.Day,
          hour = dateTime.Hour,
          minute = dateTime.Minute,
          second = dateTime.Second,
          where = DeviceInfo.Current.Name,
          userId = Token()
        };

        string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(machineRecordingEntry);
        var request = new RestRequest("worktime/machine", Method.Post).AddJsonBody(strJson);

        // Kieselstein API call /barcode needs this additional header "Content-Type", "application/json"
        // default header is "ContentType",  "application/json; charset=utf-8" response is 500 internal server error
        // and also field names must start with lower case

        request.AddHeader("Content-Type", "application/json");

        var response = await Client.ExecutePostAsync(request);

        if (response.IsSuccessful)
        {
          return new BookingResult(true, "");
        }
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
            {
              _messageService.Error(msg);
              return new BookingResult(false, msg);
            }
          }
        }
        return new BookingResult(false, string.Format(Error_Message_Action, string.Format("machine {0}", machineRecordingEntry.machineRecordingType)) + "Request returned Statuscode " + response.StatusCode);
      }
      catch (HttpRequestException e)
      {
        _logger.LogError(e.Message, e);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
      }
      return new BookingResult(false, string.Format(Error_Message_Action, string.Format("machine {0}", isStop ? MachineRecordingTypeConstants.STOP : MachineRecordingTypeConstants.START)));
    }

    public async Task<List<TimeDistributionEntry>> GetTimeDistributionEntriesAsync(int? staffId, string staffCnr)
    {
      var request = new RestRequest($"worktime/timedistribution").AddQueryParameter("userid", Token());
      if (staffId != null)
        request.AddQueryParameter("forStaffId", (int)staffId);
      else
        request.AddQueryParameter("forStaffCnr", staffCnr);
      try
      {
        var response = await Client.GetAsync<List<TimeDistributionEntry>>(request);

        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<BarcodeInfo> GetBarcodeInfoAsync(string barcode)
    {
      var request = new RestRequest($"worktime/barcodeinfo").AddQueryParameter("userid", Token()).AddQueryParameter("barcode", barcode);
      try
      {
        var response = await Client.GetAsync<BarcodeInfo>(request);

        return response;
      }
      catch (HttpRequestException ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
        return null;
      }
    }

    public async Task<BookingResult> BookTimeDistributionAsync(TimeDistributionRecordingEntry timeDistributionRecordingEntry)
    {
      try
      {
        timeDistributionRecordingEntry.userId = Token();
        string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(timeDistributionRecordingEntry);
        var request = new RestRequest("worktime/timedistribution", Method.Post).AddJsonBody(strJson);

        // Kieselstein API call /barcode needs this additional header "Content-Type", "application/json"
        // default header is "ContentType",  "application/json; charset=utf-8" response is 500 internal server error
        // and also field names must start with lower case

        request.AddHeader("Content-Type", "application/json");

        var response = await Client.ExecutePostAsync(request);

        if (response.IsSuccessful)
        {
          return new BookingResult(true, "");
        }
        else
        {
          if (response.Headers?.Count > 0)
          {
            string msg = BuildMessageFromHeader(response);
            if (!string.IsNullOrEmpty(msg))
            {
              _messageService.Error(msg);
              return new BookingResult(false, msg);
            }
          }
        }
        return new BookingResult(false, string.Format(Error_Message_Action, Booking) + "Request returned Statuscode " + response.StatusCode);
      }
      catch (HttpRequestException e)
      {
        _logger.LogError(e.Message, e);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex.Message, ex);
      }
      return new BookingResult(false, string.Format(Error_Message_Action, Booking));
    }
  }
}
