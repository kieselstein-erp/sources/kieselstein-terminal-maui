﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Services.Worktime
{
  public enum SpecialTimeType
  {
    Holiday,
    TimeCompensation,
    Illness
  }

  public static class SpecialTimeTypeExtension
  {
    public static string AsSpecialTimesEnumConstant(this SpecialTimeType specialTimeType)
    {
      switch (specialTimeType)
      {
        case SpecialTimeType.Holiday: return SpecialTimesEnumConstants.Holiday;
        case SpecialTimeType.TimeCompensation: return SpecialTimesEnumConstants.TimeCompensation;
        case SpecialTimeType.Illness: return SpecialTimesEnumConstants.Illness;
      }
      return null;
    }
  }
}