﻿using System.Collections.Concurrent;
using CommunityToolkit.Maui.Alerts;
using CommunityToolkit.Maui.Core;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Services.Audio;
using Microsoft.Extensions.Logging;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Services
{
  public class QueuedMessageService : IMessageService
  {
    private readonly ILogger<QueuedMessageService> _logger;
    private readonly IAudioService _audioService;

    public BlockingCollection<Message> MessageQueue { get; } = new();

    public QueuedMessageService(ILogger<QueuedMessageService> logger, IAudioService audioService)
    {
      _logger = logger;
      _audioService = audioService;
    }

    public void Info(string message)
    {
      Enqueue(new Message(message, MessageType.Info));
    }

    public void Warn(string message)
    {
      Enqueue(new Message(message, MessageType.Warning));
    }

    public void Error(string message)
    {
      Enqueue(new Message(message, MessageType.Error));
    }

    public void Enqueue(Message message)
    {
      _logger.Log(LogLevelFrom(message), message.Text);
      MessageQueue.Add(message);
    }


    private static LogLevel LogLevelFrom(Message message)
    {
      return message.Type switch
      {
        MessageType.Info => LogLevel.Information,
        MessageType.Warning => LogLevel.Warning,
        MessageType.Error => LogLevel.Error,
        _ => throw new ArgumentOutOfRangeException(string.Format(CouldNotDetermineLogLevelForMessageType0, message.Type.ToString())),
      };
    }
    public void ShowToast(string text, ToastDuration duration)
    {
      double size = 24;
      if (DeviceInfo.Current.Platform == DevicePlatform.Android)
        size = 14;

      var toast = Toast.Make(text, duration, size);
      Application.Current.MainPage.Dispatcher.Dispatch(async () => await toast.Show());
      if (DeviceInfo.Platform != DevicePlatform.WinUI)
        _audioService.Play(MessageType.Info);
    }

    //public async Task ShowToastAsync(string text, ToastDuration duration)
    //{
    //  var toast = Toast.Make(text, duration);
    //  Application.Current.MainPage.Dispatcher.Dispatch(async () => await toast.Show());
    //}


  }
}
