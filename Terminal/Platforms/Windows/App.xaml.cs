﻿using System.Reflection;

// To learn more about WinUI, the WinUI project structure,
// and more about our project templates, see: http://aka.ms/winui-project-info.

namespace KieselsteinErp.Terminal.WinUI;

/// <summary>
/// Provides application-specific behavior to supplement the default Application class.
/// </summary>
public partial class App : MauiWinUIApplication
{
  private static Mutex mutex = new Mutex(true, Assembly.GetEntryAssembly().GetName().Name);

  /// <summary>
  /// Initializes the singleton application object.  This is the first line of authored code
  /// executed, and as such is the logical equivalent of main() or WinMain().
  /// </summary>
  public App()
	{
    if (!mutex.WaitOne(TimeSpan.Zero, true))
    {
      //Current.Quit();
      Environment.Exit(0);
    }

    this.InitializeComponent();
	}

	protected override MauiApp CreateMauiApp() => MauiProgram.CreateMauiApp();
}

