﻿using Android.Content;
using Android.OS;

namespace KieselsteinErp.Terminal.Platforms.Android
{
  public class ZebraProfile
  {
    private static string DATAWEDGE_SEND_ACTION = "com.symbol.datawedge.api.ACTION";
    private static string DATAWEDGE_SEND_CREATE_PROFILE = "com.symbol.datawedge.api.CREATE_PROFILE";
    private static string DATAWEDGE_SEND_SET_CONFIG = "com.symbol.datawedge.api.SET_CONFIG";
    private static string DATAWEDGE_PROFILE_NAME = "Kieselstein App";

    private static string DATAWEDGE_SEND_SET_SOFT_SCAN = "com.symbol.datawedge.api.SOFT_SCAN_TRIGGER";
    private static string DATAWEDGE_SEND_SET_SOFT_SCAN_TRIGGER = "TOGGLE_SCANNING";

    private static string DATAWEDGE_RETURN_ACTION = "com.symbol.datawedge.api.RESULT_ACTION";
    private static string DATAWEDGE_RETURN_CATEGORY = "android.intent.category.DEFAULT";
    private static string DATAWEDGE_EXTRA_SEND_RESULT = "SEND_RESULT";
    private static string DATAWEDGE_EXTRA_RESULT = "RESULT";
    private static string DATAWEDGE_EXTRA_COMMAND = "COMMAND";
    private static string DATAWEDGE_EXTRA_RESULT_INFO = "RESULT_INFO";
    private static string DATAWEDGE_EXTRA_RESULT_CODE = "RESULT_CODE";

    private static string DATAWEDGE_SCAN_EXTRA_DATA_STRING = "com.symbol.datawedge.data_string";
    private static string DATAWEDGE_SCAN_EXTRA_LABEL_TYPE = "com.symbol.datawedge.label_type";

    private static string DATAWEDGE_SEND_GET_VERSION = "com.symbol.datawedge.api.GET_VERSION_INFO";
    private static string DATAWEDGE_RETURN_VERSION = "com.symbol.datawedge.api.RESULT_GET_VERSION_INFO";
    private static string DATAWEDGE_RETURN_VERSION_DATAWEDGE = "DATAWEDGE";

    private static string DATAWEDGE_SEND_GET_ENUMERATE_SCANNERS = "com.symbol.datawedge.api.ENUMERATE_SCANNERS";
    private static string DATAWEDGE_RETURN_ENUMERATE_SCANNERS = "com.symbol.datawedge.api.RESULT_ENUMERATE_SCANNERS";

    private static string DATAWEDGE_SEND_GET_CONFIG = "com.symbol.datawedge.api.GET_CONFIG";
    private static string DATAWEDGE_RETURN_GET_CONFIG = "com.symbol.datawedge.api.RESULT_GET_CONFIG";

    private static string DATAWEDGE_SEND_GET_ACTIVE_PROFILE = "com.symbol.datawedge.api.GET_ACTIVE_PROFILE";
    private static string DATAWEDGE_RETURN_GET_ACTIVE_PROFILE = "com.symbol.datawedge.api.RESULT_GET_ACTIVE_PROFILE";

    private static string DATAWEDGE_SEND_SWITCH_SCANNER = "com.symbol.datawedge.api.SWITCH_SCANNER";

    private static string DATAWEDGE_SEND_SET_SCANNER_INPUT = "com.symbol.datawedge.api.SCANNER_INPUT_PLUGIN";
    private static string DATAWEDGE_SEND_SET_SCANNER_INPUT_ENABLE = "ENABLE_PLUGIN";
    private static string DATAWEDGE_SEND_SET_SCANNER_INPUT_DISABLE = "DISABLE_PLUGIN";


  public static void CreateProfile(Context context)
    {
      String profileName = DATAWEDGE_PROFILE_NAME;
      SendDataWedgeIntentWithExtra(context, DATAWEDGE_SEND_ACTION, DATAWEDGE_SEND_CREATE_PROFILE, profileName);

      //  Now configure that created profile to apply to our application
      Bundle profileConfig = new Bundle();
      profileConfig.PutString("PROFILE_NAME", DATAWEDGE_PROFILE_NAME);
      profileConfig.PutString("PROFILE_ENABLED", "true"); //  Seems these are all strings
      profileConfig.PutString("CONFIG_MODE", "UPDATE");
      Bundle barcodeConfig = new Bundle();
      barcodeConfig.PutString("PLUGIN_NAME", "BARCODE");
      barcodeConfig.PutString("RESET_CONFIG", "true"); //  This is the default but never hurts to specify
      Bundle barcodeProps = new Bundle();
      barcodeConfig.PutBundle("PARAM_LIST", barcodeProps);
      profileConfig.PutBundle("PLUGIN_CONFIG", barcodeConfig);
      Bundle appConfig = new Bundle();
      appConfig.PutString("PACKAGE_NAME", context.PackageName);      //  Associate the profile with this app
      appConfig.PutStringArray("ACTIVITY_LIST", new String[] { "*" });
      profileConfig.PutParcelableArray("APP_LIST", new Bundle[] { appConfig });
      SendDataWedgeIntentWithExtra(context, DATAWEDGE_SEND_ACTION, DATAWEDGE_SEND_SET_CONFIG, profileConfig);

      //  You can only configure one plugin at a time, we have done the barcode input, now do the intent output
      profileConfig.Remove("PLUGIN_CONFIG");
      Bundle intentConfig = new Bundle();
      intentConfig.PutString("PLUGIN_NAME", "INTENT");
      intentConfig.PutString("RESET_CONFIG", "true");
      Bundle intentProps = new Bundle();
      intentProps.PutString("intent_output_enabled", "true");
      intentProps.PutString("intent_action", DataWedgeReceiver.IntentAction);
      intentProps.PutString("intent_delivery", "2");
      intentConfig.PutBundle("PARAM_LIST", intentProps);
      profileConfig.PutBundle("PLUGIN_CONFIG", intentConfig);
      SendDataWedgeIntentWithExtra(context, DATAWEDGE_SEND_ACTION, DATAWEDGE_SEND_SET_CONFIG, profileConfig);

      // disable keystrokes
      //profileConfig.Remove("PLUGIN_CONFIG");
      Bundle intentConfig1 = new Bundle();
      intentConfig1.PutString("PLUGIN_NAME", "KEYSTROKE");
      intentConfig1.PutString("RESET_CONFIG", "true");
      Bundle intentProps1 = new Bundle();
      intentProps1.PutString("keystroke_output_enabled", "false");
      intentProps1.PutString("keystroke_action_char", "9"); // 0, 9 , 10, 13
      intentProps1.PutString("keystroke_delay_extended_ascii", "50");
      intentProps1.PutString("keystroke_delay_control_chars", "100");
      intentConfig1.PutBundle("PARAM_LIST", intentProps1);
      profileConfig.PutBundle("PLUGIN_CONFIG", intentConfig1);
      profileConfig.PutBundle("PLUGIN_CONFIG", intentConfig1);
      SendDataWedgeIntentWithExtra(context, DATAWEDGE_SEND_ACTION, DATAWEDGE_SEND_SET_CONFIG, profileConfig);

    }

    static void SendDataWedgeIntentWithExtra(Context context, String action, String extraKey, Bundle extras)
    {
      Intent dwIntent = new Intent();
      dwIntent.SetAction(action);
      dwIntent.PutExtra(extraKey, extras);
      context.SendBroadcast(dwIntent);
    }

    static void SendDataWedgeIntentWithExtra(Context context, String action, String extraKey, String extraValue)
    {
      Intent dwIntent = new Intent();
      dwIntent.SetAction(action);
      dwIntent.PutExtra(extraKey, extraValue);
      context.SendBroadcast(dwIntent);
    }

    static void SendScannerEnable(Context context)
    {
      SendDataWedgeIntentWithExtra(context, DATAWEDGE_SEND_ACTION, DATAWEDGE_SEND_SET_SCANNER_INPUT, DATAWEDGE_SEND_SET_SCANNER_INPUT_ENABLE);
    }

    static void SendScannerDisable(Context context)
    {
      SendDataWedgeIntentWithExtra(context, DATAWEDGE_SEND_ACTION, DATAWEDGE_SEND_SET_SCANNER_INPUT, DATAWEDGE_SEND_SET_SCANNER_INPUT_DISABLE);
    }

    public static void SendScanTrigger(Context context)
    {
      SendDataWedgeIntentWithExtra(context, DATAWEDGE_SEND_ACTION, DATAWEDGE_SEND_SET_SOFT_SCAN, DATAWEDGE_SEND_SET_SOFT_SCAN_TRIGGER);
    }
  }
}
