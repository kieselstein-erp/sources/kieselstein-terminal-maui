﻿using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Com.Datalogic.Decode;
using CommunityToolkit.Mvvm.Messaging;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Platforms.Android;
using Plugin.NFC;

namespace KieselsteinErp.Terminal;

[Activity(Theme = "@style/Maui.SplashTheme", MainLauncher = true, ScreenOrientation = ScreenOrientation.Unspecified, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize | ConfigChanges.Density)]
public class MainActivity : MauiAppCompatActivity, IReadListener, ITimeoutListener
{
  #region "datalogic"
  BarcodeManager _barcodeManager = null;
  #endregion

  #region "zebra"
  public static DataWedgeReceiver _broadcastReceiver = null;
  private bool _isScanbuttonRegistered = false;
  #endregion

  protected override void OnCreate(Bundle savedInstanceState)
  {
    // Plugin NFC : Initialisation
    CrossNFC.Init(this);

    base.OnCreate(savedInstanceState);

    InTheHand.AndroidActivity.CurrentActivity = this;
    RequestPermissions(new string[] { Manifest.Permission.AccessCoarseLocation, Manifest.Permission.BluetoothAdmin, Manifest.Permission.BluetoothConnect, Manifest.Permission.BluetoothScan }, 1);

    if (DeviceInfo.Manufacturer == "Zebra Technologies")
    {
      _broadcastReceiver = new DataWedgeReceiver();
      ZebraProfile.CreateProfile(Android.App.Application.Context);
    }

  }

  protected override void OnResume()
  {
    base.OnResume();

    // Plugin NFC: Restart NFC listening on resume (needed for Android 10+) 
    CrossNFC.OnResume();

    if (_barcodeManager == null)
      _barcodeManager = new BarcodeManager();

    try
    {
      _barcodeManager.AddReadListener(this);
      _barcodeManager.AddTimeoutListener(this);
    }
    catch (DecodeException e)
    {
      System.Diagnostics.Debug.WriteLine(e.Message);
    }
    if (!_isScanbuttonRegistered)
    {
      _isScanbuttonRegistered = true;
      WeakReferenceMessenger.Default.Register<ScanButtonMessage>(this, HandleScanButton);
    }

    if (DeviceInfo.Manufacturer == "Zebra Technologies")
    {
      if (null != _broadcastReceiver)
      {
        // Register the broadcast receiver
        IntentFilter filter = new IntentFilter(DataWedgeReceiver.IntentAction);
        filter.AddCategory(DataWedgeReceiver.IntentCategory);
        Android.App.Application.Context.RegisterReceiver(_broadcastReceiver, filter);
      }

    }
  }

  private void HandleScanButton(object recipient, ScanButtonMessage message)
  {
    // Datalogic Memor 1
    if (_barcodeManager != null)
    {
      if (message.Value)
        _barcodeManager.PressTrigger();
      else
        _barcodeManager.ReleaseTrigger();
    }
    // Zebra TC21/26
    if (_broadcastReceiver != null)
    {
      if (message.Value)
        ZebraProfile.SendScanTrigger(Android.App.Application.Context);
    }
  }

  protected override void OnPause()
  {
    base.OnPause();
    if (_barcodeManager != null)
      try
      {
        _barcodeManager.RemoveReadListener(this);
      }
      catch (Exception e)
      {
        System.Diagnostics.Debug.WriteLine(e.Message);

      }
    if (_isScanbuttonRegistered)
    {
      _isScanbuttonRegistered = false;
      WeakReferenceMessenger.Default.Unregister<ScanButtonMessage>(this);
    }

    if (DeviceInfo.Manufacturer == "Zebra Technologies")
      // Unregister the broadcast receiver
      Android.App.Application.Context.UnregisterReceiver(_broadcastReceiver);

  }

  protected override void OnNewIntent(Intent intent)
  {
    base.OnNewIntent(intent);

    // Plugin NFC: Tag Discovery Interception
    CrossNFC.OnNewIntent(intent);
  }

  public void OnRead(IDecodeResult p0)
  {
    WeakReferenceMessenger.Default.Send(new DatalogicDecoderMessage(p0.Text, p0.BarcodeID));
  }

  public void OnScanTimeout()
  {
    WeakReferenceMessenger.Default.Send(new ScanButtonMessage(false));
  }
}

