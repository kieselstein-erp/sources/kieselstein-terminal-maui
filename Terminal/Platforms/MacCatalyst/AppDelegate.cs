﻿using Foundation;
using KieselsteinErp.Terminal;

namespace KieselsteinErp.Terminal.Platforms.MacCatalyst;

[Register("AppDelegate")]
public class AppDelegate : MauiUIApplicationDelegate
{
    protected override MauiApp CreateMauiApp() => MauiProgram.CreateMauiApp();
}
