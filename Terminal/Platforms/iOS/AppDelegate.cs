﻿using Foundation;
using KieselsteinErp.Terminal;

namespace KieselsteinErp.Terminal.Platforms.iOS;

[Register("AppDelegate")]
public class AppDelegate : MauiUIApplicationDelegate
{
    protected override MauiApp CreateMauiApp() => MauiProgram.CreateMauiApp();
}
