using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Die Daten der Ger&auml;tekonfigurationTeile der Konfiguration k&ouml;nnen nur durch den HELIUM V Anwenderabge&auml;ndert werden.
    /// </summary>
    public class XmlNs0DeviceConfigEntry : XmlNs0BaseEntryCnr 
    {
        public string deviceTag { get; set; }

        /// <summary>
        /// Der Ger&auml;tetyp wie er im HELIUM V System definiert wurde
        /// </summary>
        public string deviceType { get; set; }

        /// <summary>
        /// Die "System" konfiguration. Diese Daten werden beim Einrichten des angegebenenGer&auml;tes innerhalb des HELIUM V Systems gesetzt und k&ouml;nnen auch nurvom HELIUM V System abge&auml;ndert werden.
        /// </summary>
        public string systemConfig { get; set; }

        /// <summary>
        /// Die benutzerspezifische Konfiguration. Diese kann vom API Anwender abge&auml;ndert werden.
        /// </summary>
        public string userConfig { get; set; }
    }
}