using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class GoodsReceiptPositionEntryList
    {
        /// <summary>
        /// Die (leere) Liste aller Wareneing&auml;nge
        /// </summary>
        public List<GoodsReceiptPositionEntry> entries { get; set; }
    }
}