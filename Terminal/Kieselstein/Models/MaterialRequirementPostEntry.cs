using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class MaterialRequirementPostEntry
    {
        public string userId { get; set; }

        public MaterialRequirementEntryList materialRequirementEntries { get; set; }

        public bool printSynchronisationPaper { get; set; }
    }
}