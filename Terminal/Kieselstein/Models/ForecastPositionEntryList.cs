using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ForecastPositionEntryList
    {
        public List<ForecastPositionEntry> entries { get; set; }
    }
}