namespace Kieselstein.Models
{
    public static class XmlNs0InvoiceCashPaymentPostEntrypaymentTypeConstants
    {
        public const string CASH = "CASH";

        public const string ATM = "ATM";

        public const string CREDITCARD = "CREDITCARD";
    }
}
