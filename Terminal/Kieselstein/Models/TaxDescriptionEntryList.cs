using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TaxDescriptionEntryList
    {
        public List<TaxDescriptionEntry> entries { get; set; }

        public long rowCount { get; set; }
    }
}