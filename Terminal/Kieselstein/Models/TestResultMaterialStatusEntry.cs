using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestResultMaterialStatusEntry : TestResultEntry 
    {
        public string materialStatus { get; set; }
    }
}