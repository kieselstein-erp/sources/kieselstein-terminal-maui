using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestPlanDimensionalTestEntry : TestPlanEntry 
    {
        public string itemCnrStrand { get; set; }

        public string itemDescriptionStrand { get; set; }

        public double value { get; set; }

        public double tolerance { get; set; }
    }
}