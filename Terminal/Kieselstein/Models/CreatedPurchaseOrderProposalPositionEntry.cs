using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CreatedPurchaseOrderProposalPositionEntry
    {
        /// <summary>
        /// Id der neu erzeugten Bestellvorschlagsposition
        /// </summary>
        public int proposalPositionId { get; set; }
    }
}