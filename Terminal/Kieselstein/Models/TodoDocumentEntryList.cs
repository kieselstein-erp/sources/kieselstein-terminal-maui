using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TodoDocumentEntryList
    {
        /// <summary>
        /// Die Anzahl der Dokumente
        /// </summary>
        public long rowCount { get; set; }

        /// <summary>
        /// Die (leere) Liste aller Dokumente eines Todos.
        /// </summary>
        public List<TodoDocumentEntry> entries { get; set; }
    }
}