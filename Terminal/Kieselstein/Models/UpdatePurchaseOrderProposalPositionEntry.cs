using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Repr&auml;sentiert die optional zu ver&auml;ndernden Eigenschafteneiner Position des Bestellvorschlags
    /// </summary>
    public class UpdatePurchaseOrderProposalPositionEntry
    {
        /// <summary>
        /// Die zu bestellende Menge der Position
        /// </summary>
        public double amount { get; set; }

        /// <summary>
        /// Der Liefertermin der Position
        /// </summary>
        public long deliveryDateMs { get; set; }
    }
}