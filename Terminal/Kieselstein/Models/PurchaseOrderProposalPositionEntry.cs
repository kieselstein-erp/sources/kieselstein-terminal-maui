using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PurchaseOrderProposalPositionEntry : BaseEntryId 
    {
        /// <summary>
        /// Artikelnummer der Position
        /// </summary>
        public string itemCnr { get; set; }

        /// <summary>
        /// Bestellmenge der Position
        /// </summary>
        public double amount { get; set; }

        /// <summary>
        /// Wurde dieser Artikel vorgemerkt?
        /// </summary>
        public bool noted { get; set; }

        /// <summary>
        /// Der Liefertermin der Position
        /// </summary>
        public long deliveryDateMs { get; set; }
    }
}