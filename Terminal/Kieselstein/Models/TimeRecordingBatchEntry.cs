using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TimeRecordingBatchEntry : TimeRecordingEntry 
    {
        public string recordingEnum { get; set; }

        public int HVID { get; set; }

        public string todoType { get; set; }

        public int hvDetailId { get; set; }

        /// <summary>
        /// Der maximal 80 Zeichen lange Kommentar zur BelegzeitbuchungGelangt in das kurze Bemerkungsfeld 
        /// </summary>
        public string remark { get; set; }

        /// <summary>
        /// Optionaler T&auml;tigkeitsartikelWird der Artikel angegeben, wird dieser anstatt dereventuell gesetzten hvDetailId verwendet.
        /// </summary>
        public string detailItemCnr { get; set; }
    }
}