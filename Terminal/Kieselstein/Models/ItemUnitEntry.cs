using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ItemUnitEntry : BaseEntryCnr 
    {
        /// <summary>
        /// Die (locale-abh&auml;ngige) Bezeichnung dieser Einheit
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// Die Anzahl der Dimensionen dieser Einheit0 ... kg, l1 ... cm, m2 ... cm2, m23 ... cm3, m3
        /// </summary>
        public int dimension { get; set; }
    }
}