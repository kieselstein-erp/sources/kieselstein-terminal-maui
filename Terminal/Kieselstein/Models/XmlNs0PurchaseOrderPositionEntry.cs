using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0PurchaseOrderPositionEntry : XmlNs0BaseEntryId 
    {
        public XmlNs0ItemV1Entry itemEntry { get; set; }

        public int itemId { get; set; }

        public long openQuantity { get; set; }

        public long quantity { get; set; }
    }
}