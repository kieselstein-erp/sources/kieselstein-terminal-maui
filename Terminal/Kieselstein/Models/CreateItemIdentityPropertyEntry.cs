using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CreateItemIdentityPropertyEntry : ItemPropertyEntry 
    {
        public string identity { get; set; }
    }
}