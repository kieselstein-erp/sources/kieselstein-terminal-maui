using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0MaterialWithdrawalEntry
    {
        /// <summary>
        /// Die MengeBei Identit&auml;tsbehafteten Artikeln (Seriennr/Chargenr) ist hierdie kummulierte Menge anzugeben
        /// </summary>
        public long amount { get; set; }

        /// <summary>
        /// Die Serien/Chargennummer-Infos
        /// </summary>
        public List<XmlNs0IdentityAmountEntry> identities { get; set; }

        /// <summary>
        /// Artikelnummer
        /// </summary>
        public string itemCnr { get; set; }

        /// <summary>
        /// Losnummer
        /// </summary>
        public string lotCnr { get; set; }

        /// <summary>
        /// Es handelt sich um eine tats&auml;chliche R&uuml;ckgabeBei einer tats&auml;chlichen R&uuml;ckgabe wird die Sollmenge des zuvorentnommenen Materials um die zur&uuml;ckgegebene Menge reduziert. Ansonstenwird die zur&uuml;ckgegebene Menge als Fehlmenge betrachtet.
        /// </summary>
    [JsonProperty("return")]
    public bool Return { get; set; }

        /// <summary>
        /// LagernummerEntweder die Lagernummer oder die Lager-Id muss angegeben werden
        /// </summary>
        public string stockCnr { get; set; }

        /// <summary>
        /// Lager-Id
        /// </summary>
        public int stockId { get; set; }

        /// <summary>
        /// Die Lossollmaterial-Id
        /// </summary>
        public int targetMaterialId { get; set; }
    }
}