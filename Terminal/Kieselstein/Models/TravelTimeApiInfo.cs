using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TravelTimeApiInfo
    {
        /// <summary>
        /// Liefert true wenn die API benutzt werden kann
        /// </summary>
        public bool enabled { get; set; }
    }
}