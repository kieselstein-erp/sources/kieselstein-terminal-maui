using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TodoDetailIdentEntry : XmlNs0TodoDetailEntry 
    {
        public long amount { get; set; }

        public string description { get; set; }

        public string description2 { get; set; }

        /// <summary>
        /// Dokumentenpflichtig?Handelt es sich beim zugrundeliegenden Artikel um einendokumentenpflichtigen Artikel?
        /// </summary>
        public bool documentObligation { get; set; }

        public bool done { get; set; }

        public long dueDateMs { get; set; }

        public string itemCnr { get; set; }

        public int itemId { get; set; }

        public string positionNr { get; set; }

        public long progressPercent { get; set; }

        public bool recordable { get; set; }

        public string subPositionNr { get; set; }

        public string travelInfo { get; set; }

        public string unitCnr { get; set; }
    }
}