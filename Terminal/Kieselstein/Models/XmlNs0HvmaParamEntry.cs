using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0HvmaParamEntry : XmlNs0BaseEntryCnr 
    {
        public string category { get; set; }

        public string value { get; set; }
    }
}