using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ContactEntryList
    {
        public List<ContactEntry> contactEntries { get; set; }
    }
}