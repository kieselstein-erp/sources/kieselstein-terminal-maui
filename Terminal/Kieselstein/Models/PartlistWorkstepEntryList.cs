using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PartlistWorkstepEntryList
    {
        public List<PartlistWorkstepEntry> entries { get; set; }
    }
}