using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class MachineAvailabilityEntryList
    {
        public List<MachineAvailabilityEntry> entries { get; set; }
    }
}