using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ProjectEntry : BaseEntryId 
    {
        public string cnr { get; set; }

        public string customerName { get; set; }

        public string category { get; set; }

        public string title { get; set; }

        public int customerPartnerId { get; set; }

        public string customerAddress { get; set; }

        public int priority { get; set; }

        public long deadlineMs { get; set; }

        public bool internalDone { get; set; }

        public string internalComment { get; set; }

        public string statusCnr { get; set; }
    }
}