using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class LogonTenantEntry : LogonEntry 
    {
        public string tenantCnr { get; set; }
    }
}