using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Repr&auml;sentiert eine zu liefernde Position
    /// </summary>
    public class CreateItemCustomerDeliveryPositionEntry : CreateItemDeliveryPositionEntry 
    {
        /// <summary>
        /// Die (optionale) Lieferschein-Id, auf die diese Position geliefert werden soll.Ab der zweiten Position m&uuml;ssen beide Properties angegeben werden. Wird nurdie customerNr und nicht die deliveryId angegeben, wird ein neuer Lieferscheinf&uuml;r den Kunden der durch die customerNr repr&auml;sentiert wird angelegt.F&uuml;r diesen Kunden wird der Lieferschein erzeugt. In der Folge kann danndie hier erzeugte Lieferschein-id angegeben werden.Wird eine deliveryId angegeben, wird gepr&uuml;ft, dass die customerId zugeh&ouml;rigist
        /// </summary>
        public int? deliveryId { get; set; }

        /// <summary>
        /// Die KundennummerSie ist nicht mit der customerId identisch, sondern wirdfortlaufend vom ERP vergeben, bzw. kann manuell vergeben werden. In beidenF&auml;llen muss der Parameter "KUNDE_MIT_NUMMER" im ERP gesetzt werden. 
        /// </summary>
        public int customerNr { get; set; }

        /// <summary>
        /// Die (optionale) Id des KundenWird die Id des Kunden angegeben, hat sie Vorrang vor einerebenfalls angegebenen Kundennummer
        /// </summary>
        public int? customerId { get; set; }
    }
}