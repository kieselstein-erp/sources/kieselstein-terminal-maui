using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ItemLockEntryList
    {
        public List<ItemLockEntry> entries { get; set; }
    }
}