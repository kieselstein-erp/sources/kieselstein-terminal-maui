using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0CustomerDetailEntry : XmlNs0CustomerEntry 
    {
        public XmlNs0ContactEntryList contactEntries { get; set; }

        /// <summary>
        /// Der Name des Landes
        /// </summary>
        public string countryName { get; set; }

        /// <summary>
        /// Die E-Mail Adresse
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// Die EORI des Kunden
        /// </summary>
        public string eori { get; set; }

        /// <summary>
        /// Die Faxnummer
        /// </summary>
        public string fax { get; set; }

        /// <summary>
        /// Die von HELIUM V formatierte OrtsbezeichnungBeispiel: "AT 5301 Eugendorf bei Salzburg"
        /// </summary>
        public string formattedCity { get; set; }

        /// <summary>
        /// Die von HELIUM V formatierte Anrede inkl. Titel
        /// </summary>
        public string formattedSalutation { get; set; }

        /// <summary>
        /// Die dritte Adresszeile (Abteilung)
        /// </summary>
        public string name3 { get; set; }

        /// <summary>
        /// Id des Partners
        /// </summary>
        public int partnerId { get; set; }

        /// <summary>
        /// Der Schl&uuml;ssel der PreislisteBeispiel: "Endkunde", "H&auml;ndler"
        /// </summary>
        public string pricelistCnr { get; set; }

        /// <summary>
        /// Die Bemerkung zum Kunden
        /// </summary>
        public string remark { get; set; }

        /// <summary>
        /// Die Strasse
        /// </summary>
        public string street { get; set; }

        /// <summary>
        /// Der nachgestellte Teil des Titel
        /// </summary>
        public string titlePostfix { get; set; }

        /// <summary>
        /// Der TitelJener Teil der vorangestellt wird
        /// </summary>
        public string titlePrefix { get; set; }

        /// <summary>
        /// Die UID des Kunden
        /// </summary>
        public string uid { get; set; }

        /// <summary>
        /// Die Homepage
        /// </summary>
        public string website { get; set; }
    }
}