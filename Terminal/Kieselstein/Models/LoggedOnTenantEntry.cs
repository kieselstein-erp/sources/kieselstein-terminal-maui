using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Repr&auml;sentiert die Antwort eines erfolgreichen Logons
    /// </summary>
    public class LoggedOnTenantEntry : LoggedOnEntry 
    {
        public bool valid { get; set; }

        public TenantEntryList possibleTenants { get; set; }
    }
}