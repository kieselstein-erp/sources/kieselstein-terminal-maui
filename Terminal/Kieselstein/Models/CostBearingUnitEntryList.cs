using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CostBearingUnitEntryList
    {
        public List<CostBearingUnitEntry> entries { get; set; }

        public long rowCount { get; set; }
    }
}