using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0EmitProductionEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Wurde das Los ausgegeben?
        /// </summary>
        public bool emitted { get; set; }

        public XmlNs0ItemHintEntryList itemHints { get; set; }

        public XmlNs0ItemLockEntryList itemLocks { get; set; }

        public bool locked { get; set; }

        public bool missingPartsWarning { get; set; }

        public string postMessage { get; set; }

        public string requiredPositionHints { get; set; }
    }
}