using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class WorkCalendarEntryList
    {
        /// <summary>
        /// Die (leere) Liste aller Kalendereintr&auml;ge
        /// </summary>
        public List<WorkCalendarEntry> entries { get; set; }
    }
}