using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class MaterialRequirementEntry
    {
        public int productionId { get; set; }

        public string productionCnr { get; set; }

        public double desiredQuantity { get; set; }

        public long desiredDateMs { get; set; }

        public bool additional { get; set; }

        public bool returned { get; set; }

        public string item { get; set; }

        public string itemDescription { get; set; }

        public string comment { get; set; }

        public int fromStaffId { get; set; }

        public long acquisitionTimeMs { get; set; }

        public string data { get; set; }
    }
}