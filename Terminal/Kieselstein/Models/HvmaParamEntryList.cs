using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class HvmaParamEntryList
    {
        public List<HvmaParamEntry> entries { get; set; }

        public long rowCount { get; set; }
    }
}