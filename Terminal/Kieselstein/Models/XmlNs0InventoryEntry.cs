using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0InventoryEntry : XmlNs0BaseEntryId 
    {
        public string name { get; set; }

        public XmlNs0StockEntry stockEntry { get; set; }

        public int stockId { get; set; }
    }
}