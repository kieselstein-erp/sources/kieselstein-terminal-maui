using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class HvmarechtEnumEntryList
    {
        public List<string> entries { get; set; }

        public long rowCount { get; set; }
    }
}