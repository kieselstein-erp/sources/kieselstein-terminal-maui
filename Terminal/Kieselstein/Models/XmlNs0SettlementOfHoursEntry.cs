using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0SettlementOfHoursEntry : XmlNs0BaseEntryId 
    {
        public string content { get; set; }

        public string description { get; set; }

        public string lastPagePng { get; set; }

        public string mimeType { get; set; }

        public string name { get; set; }

        public int serialNumber { get; set; }
    }
}