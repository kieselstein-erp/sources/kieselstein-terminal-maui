namespace Kieselstein.Models
{
    public static class LogMessageEntryseverityConstants
    {
        public const string NOTINITIALIZED = "NOTINITIALIZED";

        public const string DEBUG = "DEBUG";

        public const string INFO = "INFO";

        public const string WARN = "WARN";

        public const string ERROR = "ERROR";
    }
}
