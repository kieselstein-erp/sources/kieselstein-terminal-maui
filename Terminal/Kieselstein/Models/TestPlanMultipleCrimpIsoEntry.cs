using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestPlanMultipleCrimpIsoEntry : TestPlanCrimpIsoEntry 
    {
        public string itemCnrSecondStrand { get; set; }

        public string itemDescriptionSecondStrand { get; set; }

        public double strippingForceSecondStrand { get; set; }
    }
}