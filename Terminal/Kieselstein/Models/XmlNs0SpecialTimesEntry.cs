using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0SpecialTimesEntry
    {
        public long fromDateMs { get; set; }

        public bool halfDay { get; set; }

        public string timeType { get; set; }

        public long toDateMs { get; set; }
    }
}