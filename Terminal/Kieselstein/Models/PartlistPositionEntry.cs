using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PartlistPositionEntry : BaseEntryId 
    {
        public string cnr { get; set; }

        public string description { get; set; }

        public double amount { get; set; }

        public double calcPrice { get; set; }

        public string comment { get; set; }

        public string position { get; set; }

        public string unitCnr { get; set; }

        public double salesPrice { get; set; }

        public int itemId { get; set; }

        public int iSort { get; set; }

        public string customerItemCnr { get; set; }

        public bool itemHidden { get; set; }

        public bool itemLocked { get; set; }
    }
}