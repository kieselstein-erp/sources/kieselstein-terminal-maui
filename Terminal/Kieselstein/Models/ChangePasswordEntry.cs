using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ChangePasswordEntry
    {
        public string password { get; set; }
    }
}