using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PrintProductionEntry : BaseEntryId 
    {
        /// <summary>
        /// Wurde der Fertigungsbegleitschein gedruckt?
        /// </summary>
        public bool printProductionpaper { get; set; }

        /// <summary>
        /// Wurde die Ausgabeliste gedruckt?
        /// </summary>
        public bool printAnalysissheet { get; set; }
    }
}