using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ItemLockEntry : XmlNs0BaseEntryId 
    {
        public string cause { get; set; }

        public string description { get; set; }

        public int lockId { get; set; }

        public bool locked { get; set; }

        public bool lockedByProduction { get; set; }

        public bool lockedPartlist { get; set; }

        public bool lockedProduction { get; set; }

        public bool lockedPurchasing { get; set; }

        public bool lockedSale { get; set; }
    }
}