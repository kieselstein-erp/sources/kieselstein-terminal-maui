using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class LinecallDeliveryResultEntry
    {
        public int linecallId { get; set; }

        public int itemId { get; set; }

        public double openQuantity { get; set; }
    }
}