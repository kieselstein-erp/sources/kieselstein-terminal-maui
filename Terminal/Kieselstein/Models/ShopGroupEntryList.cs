using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ShopGroupEntryList
    {
        public List<ShopGroupEntry> entries { get; set; }
    }
}