namespace Kieselstein.Models
{
    public static class XmlNs0MachineRecordingTypeConstants
    {
        public const string START = "START";

        public const string STOP = "STOP";
    }
}
