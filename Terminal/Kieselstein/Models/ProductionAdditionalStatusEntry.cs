using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ProductionAdditionalStatusEntry
    {
        public string status { get; set; }

        public long dateMs { get; set; }
    }
}