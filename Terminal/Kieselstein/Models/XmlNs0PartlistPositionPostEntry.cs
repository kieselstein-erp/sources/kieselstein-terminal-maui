using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0PartlistPositionPostEntry : XmlNs0BaseEntryId 
    {
        public long amount { get; set; }

        public string comment { get; set; }

        public string itemCnr { get; set; }

        public string position { get; set; }

        public string unitCnr { get; set; }
    }
}