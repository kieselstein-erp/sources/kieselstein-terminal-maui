using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TimeRecordingBatchEntryList
    {
        /// <summary>
        /// Die (leere) Liste aller TimeRecordingEntry Eintr&auml;ge
        /// </summary>
        public List<TimeRecordingBatchEntry> entries { get; set; }

        /// <summary>
        /// Die Anzahl aller Datens&auml;tzeDie hier gelieferte Anzahl muss nicht zwangsl&auml;ufig mit derAnzahl der Listenelemente &uuml;bereinstimmen
        /// </summary>
        public long rowCount { get; set; }
    }
}