using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ProductionWorkstepEntryList
    {
        public List<XmlNs0ProductionWorkstepEntry> entries { get; set; }
    }
}