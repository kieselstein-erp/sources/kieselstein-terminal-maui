using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ItemCommentMediaInfoEntryList
    {
        public List<XmlNs0ItemCommentMediaInfoEntry> entries { get; set; }
    }
}