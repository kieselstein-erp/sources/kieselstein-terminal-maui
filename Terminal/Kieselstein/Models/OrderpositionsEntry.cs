using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class OrderpositionsEntry : OrderpositionEntry 
    {
        public int orderId { get; set; }
    }
}