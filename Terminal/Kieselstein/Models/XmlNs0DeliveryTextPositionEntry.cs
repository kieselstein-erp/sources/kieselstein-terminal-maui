using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0DeliveryTextPositionEntry : XmlNs0BaseEntryId 
    {
        public string text { get; set; }
    }
}