using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0DocumentInfoEntry : XmlNs0BaseEntryCnr 
    {
        /// <summary>
        /// Dateiname des Dokuments
        /// </summary>
        public string filename { get; set; }

        /// <summary>
        /// Name des Dokuments in der Dokumentenablage
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Gr&ouml;&szlig;e des Dokuments in Bytes
        /// </summary>
        public long size { get; set; }
    }
}