using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DayTypeEntry : BaseEntryId 
    {
        public string description { get; set; }
    }
}