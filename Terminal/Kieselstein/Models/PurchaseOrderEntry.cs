using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PurchaseOrderEntry : BaseEntryId 
    {
        /// <summary>
        /// Die Bestellnummer
        /// </summary>
        public string cnr { get; set; }

        /// <summary>
        /// Die Id des Lieferanten - bei dem die Bestellung aufgegeben worden ist.
        /// </summary>
        public int supplierId { get; set; }

        public string status { get; set; }

        public SupplierDetailEntry supplierEntry { get; set; }

        /// <summary>
        /// Der Lieferantenname
        /// </summary>
        public string supplierName { get; set; }

        /// <summary>
        /// Der Ort im Format "- ", also zum Beispiel "AT-5301 Eugendorf"
        /// </summary>
        public string supplierCity { get; set; }
    }
}