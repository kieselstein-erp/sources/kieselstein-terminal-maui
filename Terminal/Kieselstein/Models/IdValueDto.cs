using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class IdValueDto
    {
        public int id { get; set; }

        public string value { get; set; }
    }
}