namespace Kieselstein.Models
{
    public static class XmlNs0PositionDataEntrydataTypeConstants
    {
        public const string NOTINITIALIZED = "NOTINITIALIZED";

        public const string ITEM = "ITEM";

        public const string TEXT = "TEXT";
    }
}
