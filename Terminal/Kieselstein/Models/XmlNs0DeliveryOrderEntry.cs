using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0DeliveryOrderEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Die Lieferscheinnummer
        /// </summary>
        public string deliveryCnr { get; set; }

        public string status { get; set; }
    }
}