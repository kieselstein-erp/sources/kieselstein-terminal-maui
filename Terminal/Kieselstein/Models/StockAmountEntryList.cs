using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class StockAmountEntryList
    {
        /// <summary>
        /// Die Liste aller StockAmountEntry Eintr&auml;ge
        /// </summary>
        public List<StockAmountEntry> entries { get; set; }
    }
}