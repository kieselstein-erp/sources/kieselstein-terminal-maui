namespace Kieselstein.Models
{
    public static class XmlNs0SpecialTimesEntrytimeTypeConstants
    {
        public const string Holiday = "Holiday";

        public const string TimeCompensation = "TimeCompensation";

        public const string Illness = "Illness";
    }
}
