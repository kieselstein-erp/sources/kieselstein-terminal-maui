using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class SoapCallPersonalResult
    {
        /// <summary>
        /// 0 ... ok, -1 ... Los nicht gefunden , -4 ... Personal nicht gefunden
        /// </summary>
        public int resultCode { get; set; }
    }
}