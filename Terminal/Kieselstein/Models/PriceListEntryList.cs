using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PriceListEntryList
    {
        public List<PriceListEntry> entries { get; set; }

        /// <summary>
        /// Die Anzahl von Eintr&auml;gen in der Liste
        /// </summary>
        public long rowCount { get; set; }
    }
}