using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class StockEntryList
    {
        /// <summary>
        /// Eine Liste aller Lagereintr&auml;geEs sind nur jene Lager enthalten, auf die der angemeldete Benutzer Zugriff hat
        /// </summary>
        public List<StockEntry> entries { get; set; }
    }
}