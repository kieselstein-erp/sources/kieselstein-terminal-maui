using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PurchaseInvoiceEntry
    {
        public int orderId { get; set; }

        public double price { get; set; }

        public string currency { get; set; }

        public string paymentType { get; set; }

        public string description { get; set; }

        public string image { get; set; }

        public string imageType { get; set; }

        public long timestampMs { get; set; }
    }
}