using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PurchaseOrderProposalPositionEntryList
    {
        /// <summary>
        /// Die Liste aller PurchaseOrderProposalPositionEntry Eintr&auml;ge
        /// </summary>
        public List<PurchaseOrderProposalPositionEntry> entries { get; set; }

        /// <summary>
        /// Die Anzahl der Eintr&auml;ge
        /// </summary>
        public int rowCount { get; set; }
    }
}