using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0BaseEntryId
    {
        /// <summary>
        /// Die eindeutige Id des Eintrags
        /// </summary>
        public int id { get; set; }
    }
}