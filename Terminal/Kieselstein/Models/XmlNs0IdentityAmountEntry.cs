using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0IdentityAmountEntry
    {
        public long amount { get; set; }

        public string identity { get; set; }
    }
}