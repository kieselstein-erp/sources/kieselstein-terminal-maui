using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TodoDetailManualEntry : XmlNs0TodoDetailEntry 
    {
        public long amount { get; set; }

        public string description { get; set; }

        public string description2 { get; set; }

        public bool documentObligation { get; set; }

        public long dueDateMs { get; set; }

        public string itemCnr { get; set; }

        public int itemId { get; set; }

        public bool recordable { get; set; }

        public string unitCnr { get; set; }
    }
}