using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PackagingInfoEntryList
    {
        /// <summary>
        /// Die Liste aller packagingInfoEntry Eintr&auml;ge
        /// </summary>
        public List<PackagingInfoEntry> entries { get; set; }
    }
}