using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class MountingMethodEntry : BaseEntryId 
    {
        public string description { get; set; }

        public int itemId { get; set; }

        public int iSort { get; set; }
    }
}