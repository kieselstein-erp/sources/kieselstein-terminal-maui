namespace Kieselstein.Models
{
    public static class TestTypeEnumConstants
    {
        public const string CRIMPWITHISOLATION = "CRIMP_WITH_ISOLATION";

        public const string CRIMPWITHOUTISOLATION = "CRIMP_WITHOUT_ISOLATION";

        public const string FORCEMEASUREMENT = "FORCE_MEASUREMENT";

        public const string DIMENSIONALTEST = "DIMENSIONAL_TEST";

        public const string OPTICALTEST = "OPTICAL_TEST";

        public const string ELECTRICALTEST = "ELECTRICAL_TEST";

        public const string MULTIPLECRIMPWITHISOLATION = "MULTIPLE_CRIMP_WITH_ISOLATION";

        public const string MULTIPLECRIMPWITHOUTISOLATION = "MULTIPLE_CRIMP_WITHOUT_ISOLATION";

        public const string MATERIALSTATUS = "MATERIAL_STATUS";

        public const string OPENTEST = "OPEN_TEST";
    }
}
