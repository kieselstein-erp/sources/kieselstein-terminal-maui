namespace Kieselstein.Models
{
  /// <summary>
  /// Repr&auml;sentiert eine zu liefernde Position
  /// </summary>
  public class CreatedGoodsReceiptPositionReelEntry
    {
        /// <summary>
        /// Die noch offene Menge der Bestellung
        /// </summary>
        public decimal openQuantity { get; set; }

        public int purchaseOrderPositionId { get; set; }

        /// <summary>
        /// Die Id des Wareneingangs
        /// </summary>
        public int goodsReceiptId { get; set; }

        /// <summary>
        /// Die Id der Wareneingangsposition
        /// </summary>
        public int goodsReceiptPositionId { get; set; }

        public string identity { get; set; }

    public static implicit operator CreatedGoodsReceiptPositionReelEntry(CreatedGoodsReceiptPositionEntry v)
    {
      throw new NotImplementedException();
    }
  }
}