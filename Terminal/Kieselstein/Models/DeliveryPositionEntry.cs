using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DeliveryPositionEntry : BaseEntryId 
    {
        /// <summary>
        /// Die Artikelnummer
        /// </summary>
        public string itemCnr { get; set; }

        /// <summary>
        /// Die Id des Artikels
        /// </summary>
        public int itemId { get; set; }

        /// <summary>
        /// Die Artikelbezeichnung
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// Auftragsmenge
        /// </summary>
        public decimal amount { get; set; }

        /// <summary>
        /// Die Einheit des Artikels im Auftrag
        /// </summary>
        public string unitCnr { get; set; }

        public string itemProperty { get; set; }

        /// <summary>
        /// Die Liste der Chargen- bzw. Seriennummern
        /// </summary>
        public ItemIdentityEntryList itemIdentity { get; set; }

        /// <summary>
        /// Die LieferscheinId
        /// </summary>
        public int deliveryId { get; set; }

        public int orderPositionId { get; set; }
    }
}