using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Properties f&uuml;r die ZahlungDie Property cnr muss mit der eindeutigen Kassa-Nummer bef&uuml;llt werdenF&uuml;r eine erfolgreiche Zahlung m&uuml;ssen der Betrag #getAmount(),die Zahlungsart #getPaymentType() und die Kassanummer #getCnr()gesetzt werden.
    /// </summary>
    public class InvoiceCashPaymentPostEntry : BaseEntryCnr 
    {
        /// <summary>
        /// Der bezahlte Betrag
        /// </summary>
        public double amount { get; set; }

        public string paymentType { get; set; }
    }
}