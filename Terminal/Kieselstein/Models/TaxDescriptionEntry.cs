using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TaxDescriptionEntry : BaseEntryId 
    {
        /// <summary>
        /// Die (in die Client-Sprache &uuml;bersetzte) Mehrwertsteuersatzbezeichnung
        /// </summary>
        public string description { get; set; }
    }
}