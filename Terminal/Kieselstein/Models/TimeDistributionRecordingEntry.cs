﻿namespace Kieselstein.Models
{
  public class TimeDistributionRecordingEntry : TimeRecordingEntry
  {
    public int productionId { get; set; }
    public int? machineId { get; set; }
    public int productionWorkplanId { get; set; }
    public int workItemId { get; set; }

  }
}
