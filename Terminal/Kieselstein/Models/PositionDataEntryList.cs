using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PositionDataEntryList
    {
        public List<PositionDataEntry> entries { get; set; }
    }
}