using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PartlistPositionEntryList
    {
        public List<PartlistPositionEntry> list { get; set; }

        public double salesPrice { get; set; }
    }
}