using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class StockPlaceEntry : BaseEntryId 
    {
        public string name { get; set; }

        public ItemV1EntryList items { get; set; }
    }
}