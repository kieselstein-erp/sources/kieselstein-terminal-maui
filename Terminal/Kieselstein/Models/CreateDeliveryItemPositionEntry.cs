using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CreateDeliveryItemPositionEntry
    {
        /// <summary>
        /// Die (optionale) Artikel-IdWenn #getOrderPositionId() angegeben ist, dann kann dieAngabe von ItemCnr oder ItemId entfallen. Andernfalls muss entwederdie Id des Artikels (#getItemId()) oder die Artikelnummer(#getItemCnr() angegeben werden
        /// </summary>
        public int itemId { get; set; }

        /// <summary>
        /// Die (optionale) ArtikelnummerWenn #getOrderPositionId() angegeben ist, dann kann dieAngabe von ItemCnr oder ItemId entfallen. Andernfalls muss entwederdie Id des Artikels (#getItemId()) oder die Artikelnummer(#getItemCnr() angegeben werden
        /// </summary>
        public string itemCnr { get; set; }

        /// <summary>
        /// Die Liste der Chargen- bzw. Seriennummern
        /// </summary>
        public ItemIdentityEntryList itemIdentity { get; set; }

        /// <summary>
        /// Die zu lieferende MengeBei identit&auml;tsbehafteten Artikeln (also jene mit Serien- oderChargennummer) ist die Gesamtmenge aus den einzelnen Serien- oder Chargennummerntrotzdem anzugeben
        /// </summary>
        public decimal amount { get; set; }

        /// <summary>
        /// Die Id der Auftragsposition die geliefert werden sollIst die PositionId angegeben, wird eine etwaig vorhandene itemIdoder itemCnr ignoriert.Der Auftrag der Position muss dem Auftrag des Lieferscheinsentsprechen
        /// </summary>
        public int? orderPositionId { get; set; }
    }
}