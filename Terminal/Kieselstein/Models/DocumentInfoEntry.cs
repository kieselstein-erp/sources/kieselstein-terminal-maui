using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DocumentInfoEntry : BaseEntryCnr 
    {
        /// <summary>
        /// Name des Dokuments in der Dokumentenablage
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Dateiname des Dokuments
        /// </summary>
        public string filename { get; set; }

        /// <summary>
        /// Gr&ouml;&szlig;e des Dokuments in Bytes
        /// </summary>
        public long size { get; set; }
    }
}