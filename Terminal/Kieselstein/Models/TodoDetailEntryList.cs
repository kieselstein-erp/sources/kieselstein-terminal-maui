using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TodoDetailEntryList
    {
        /// <summary>
        /// Die (leere) Liste aller Todoeintr&auml;ge
        /// </summary>
        public List<TodoDetailEntry> entries { get; set; }

        public long rowCount { get; set; }
    }
}