using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class MachineGroupEntryList
    {
        public List<MachineGroupEntry> entries { get; set; }
    }
}