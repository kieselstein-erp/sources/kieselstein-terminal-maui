using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TestPlanEntry : XmlNs0BaseEntryId 
    {
        public string comment { get; set; }

        public string description { get; set; }

        public int productionId { get; set; }

        public int sortOrder { get; set; }

        public string testType { get; set; }
    }
}