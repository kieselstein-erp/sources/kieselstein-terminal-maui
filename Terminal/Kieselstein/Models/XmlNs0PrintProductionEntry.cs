using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0PrintProductionEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Wurde die Ausgabeliste gedruckt?
        /// </summary>
        public bool printAnalysissheet { get; set; }

        /// <summary>
        /// Wurde der Fertigungsbegleitschein gedruckt?
        /// </summary>
        public bool printProductionpaper { get; set; }
    }
}