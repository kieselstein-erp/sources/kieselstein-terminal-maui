using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class InventoryEntry : BaseEntryId 
    {
        public string name { get; set; }

        public int stockId { get; set; }

        public StockEntry stockEntry { get; set; }
    }
}