using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TestPlanCrimpEntry : XmlNs0TestPlanEntry 
    {
        public long crimpHeightWire { get; set; }

        public long crimpHeightWireTolerance { get; set; }

        public long crimpWidthWire { get; set; }

        public long crimpWidthWireTolerance { get; set; }

        public bool doublestrike { get; set; }

        public string itemCnrContact { get; set; }

        public string itemCnrStrand { get; set; }

        public string itemDescriptionContact { get; set; }

        public string itemDescriptionStrand { get; set; }

        public long strippingForceStrand { get; set; }
    }
}