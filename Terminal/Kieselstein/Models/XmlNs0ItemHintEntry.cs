using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ItemHintEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Die Artikelkommentarart des Hinweises
        /// </summary>
        public string cnr { get; set; }

        /// <summary>
        /// Der (html) Hinweistext
        /// </summary>
        public string content { get; set; }

        public string mimeType { get; set; }
    }
}