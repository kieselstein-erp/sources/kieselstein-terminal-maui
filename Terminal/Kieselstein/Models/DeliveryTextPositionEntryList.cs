using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DeliveryTextPositionEntryList
    {
        public List<DeliveryTextPositionEntry> entries { get; set; }
    }
}