using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DeliverySlipSignaturePostEntry : BaseEntryId 
    {
        /// <summary>
        /// Die (optionale) Bemerkung
        /// </summary>
        public string remark { get; set; }

        /// <summary>
        /// Das originale (zu unterschreibende) PDF
        /// </summary>
        public string pdfContent { get; set; }

        /// <summary>
        /// Die Unterschrift, die best&auml;tigt, dass daspdf gelesen/akzeptiert wurde.
        /// </summary>
        public string signatureContent { get; set; }

        /// <summary>
        /// Der Zeitpunkt der Unterschrift
        /// </summary>
        public long signatureMs { get; set; }

        public int serialNumber { get; set; }

        public string signerName { get; set; }
    }
}