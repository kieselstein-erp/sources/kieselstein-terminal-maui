using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0StaffEntry : XmlNs0BaseEntryId 
    {
        public string firstName { get; set; }

        public string identityCnr { get; set; }

        public string name { get; set; }

        public string personalNr { get; set; }

        public string shortMark { get; set; }
    }
}