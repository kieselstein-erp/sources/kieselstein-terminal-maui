using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ForecastDeliveryAddressEntry : XmlNs0BaseEntryId 
    {
        public int customerId { get; set; }

        public string deliveryAddress { get; set; }

        public string deliveryAddressShort { get; set; }

        public int forecastId { get; set; }

        public string pickingPrinterCnr { get; set; }

        public string pickingType { get; set; }
    }
}