using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0DeliveryPositionEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Auftragsmenge
        /// </summary>
        public long amount { get; set; }

        /// <summary>
        /// Die LieferscheinId
        /// </summary>
        public int deliveryId { get; set; }

        /// <summary>
        /// Die Artikelbezeichnung
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// Die Artikelnummer
        /// </summary>
        public string itemCnr { get; set; }

        /// <summary>
        /// Die Id des Artikels
        /// </summary>
        public int itemId { get; set; }

        /// <summary>
        /// Die Liste der Chargen- bzw. Seriennummern
        /// </summary>
        public XmlNs0ItemIdentityEntryList itemIdentity { get; set; }

        public string itemProperty { get; set; }

        public int orderPositionId { get; set; }

        /// <summary>
        /// Die Einheit des Artikels im Auftrag
        /// </summary>
        public string unitCnr { get; set; }
    }
}