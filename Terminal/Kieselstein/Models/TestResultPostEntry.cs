using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestResultPostEntry
    {
        public string userId { get; set; }

        public int productionDeliveryId { get; set; }

        public TestResultEntryList testResultEntries { get; set; }
    }
}