using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Die Handlagerbewegung
    /// </summary>
    public class XmlNs0StockMovementEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Die Menge des Artikels die bewegt werden sollHandelt es sich um chargen- oder seriennummernbehaftete Artikel isthier die Gesamtmenge anzugeben, die jeweiligen Teilmengen der Identit&auml;tsind unter identities anzugebenDie Menge muss angegeben werden
        /// </summary>
        public long amount { get; set; }

        /// <summary>
        /// Der Kommentar zu dieser LagerbewegungDer Kommentar muss angegeben werden
        /// </summary>
        public string comment { get; set; }

        /// <summary>
        /// Die Liste der Chargen- bzw. Seriennummern
        /// </summary>
        public XmlNs0ItemIdentityEntryList identities { get; set; }

        /// <summary>
        /// Der Preis des Artikels f&uuml;r die die jeweilige Lagerbuchung.Bei einer Zu- bzw. Umbuchung sollte das der Einstandspreis sein, bei einerAbbuchung der Verkaufspreis.Wird der Preis hier nicht angegeben, wird der gemittelte Gestehungspreis desLagers verwendet
        /// </summary>
        public long price { get; set; }
    }
}