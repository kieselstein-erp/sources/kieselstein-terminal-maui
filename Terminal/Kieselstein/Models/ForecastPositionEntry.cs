using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ForecastPositionEntry : BaseEntryId 
    {
        public ItemV1Entry itemEntry { get; set; }

        public double quantity { get; set; }

        public long dateMs { get; set; }

        public string ordernumber { get; set; }

        public List<LinecallEntry> linecallEntries { get; set; }

        public int staffId { get; set; }

        public string staffDescription { get; set; }

        public string productionCnr { get; set; }

        public string productType { get; set; }
    }
}