using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class GoodsReceiptEntryList
    {
        /// <summary>
        /// Die (leere) Liste aller Wareneing&auml;nge
        /// </summary>
        public List<GoodsReceiptEntry> entries { get; set; }
    }
}