using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestPlanForceMeasurementEntry : TestPlanEntry 
    {
        public string itemCnrContact { get; set; }

        public string itemDescriptionContact { get; set; }

        public string itemCnrStrand { get; set; }

        public string itemDescriptionStrand { get; set; }

        public double minimumValue { get; set; }
    }
}