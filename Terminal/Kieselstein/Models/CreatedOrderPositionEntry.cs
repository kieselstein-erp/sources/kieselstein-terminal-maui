using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Repr&auml;sentiert die Information &uuml;ber eine erzeugte Auftragspositon
    /// </summary>
    public class CreatedOrderPositionEntry
    {
        /// <summary>
        /// Die Id des Auftrags
        /// </summary>
        public int orderId { get; set; }

        /// <summary>
        /// Die Cnr des Auftrags
        /// </summary>
        public string orderCnr { get; set; }

        /// <summary>
        /// Die Id der Auftragsposition
        /// </summary>
        public int orderPositionId { get; set; }

        /// <summary>
        /// Die Menge der Auftragsposition
        /// </summary>
        public double amount { get; set; }
    }
}