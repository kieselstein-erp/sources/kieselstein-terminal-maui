using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0MountingMethodEntry : XmlNs0BaseEntryId 
    {
        public string description { get; set; }

        public int iSort { get; set; }

        public int itemId { get; set; }
    }
}