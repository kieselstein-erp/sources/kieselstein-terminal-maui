using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CustomerPricelistReportDto
    {
        public IdValueDto shopgroup { get; set; }

        public string itemRangeTo { get; set; }

        public bool withHidden { get; set; }

        public bool onlyWebshopItems { get; set; }

        public IdValueDto itemgroup { get; set; }

        public IdValueDto customer { get; set; }

        public List<CustomerPricelistItemDto> items { get; set; }

        public bool appliedQuantityScale { get; set; }

        public long priceValidityMs { get; set; }

        public IdValueDto itemclass { get; set; }

        public bool withBlocked { get; set; }

        public string itemRangeFrom { get; set; }

        public bool onlySpecialCondition { get; set; }

        public bool withClientLanguage { get; set; }
    }
}