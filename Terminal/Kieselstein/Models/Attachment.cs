using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class Attachment
    {
        public Dictionary<string, List<string>> headers { get; set; }

        public string contentId { get; set; }

        public Attachmentobject o { get; set; }

        public MediaType contentType { get; set; }

        public string dataHandler { get; set; }

        public ContentDisposition contentDisposition { get; set; }
    }
}