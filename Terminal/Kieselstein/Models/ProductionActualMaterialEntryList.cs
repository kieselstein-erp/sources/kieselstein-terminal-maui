﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Kieselstein.Models
{
  public class ProductionActualMaterialEntryList
  {
    public List<ProductionActualMaterialEntry> entries { get; set; }
  }
}
