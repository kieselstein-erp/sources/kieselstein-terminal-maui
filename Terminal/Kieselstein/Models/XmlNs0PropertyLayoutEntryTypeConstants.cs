namespace Kieselstein.Models
{
    public static class XmlNs0PropertyLayoutEntrytypeConstants
    {
        public const string LABEL = "LABEL";

        public const string TEXTFIELD = "TEXTFIELD";

        public const string TEXTAREA = "TEXTAREA";

        public const string EDITOR = "EDITOR";

        public const string CHECKBOX = "CHECKBOX";

        public const string PRINTBUTTON = "PRINTBUTTON";

        public const string EXECUTEBUTTON = "EXECUTEBUTTON";

        public const string COMBOBOX = "COMBOBOX";
    }
}
