using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0OrderpositionsEntry : XmlNs0OrderpositionEntry 
    {
        public int orderId { get; set; }
    }
}