using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0GoodsReceiptEntry : XmlNs0BaseEntryId 
    {
        public string deliverySlipCnr { get; set; }

        /// <summary>
        /// Die Id der zugrundeliegenden Bestellung
        /// </summary>
        public int purchaseOrderId { get; set; }
    }
}