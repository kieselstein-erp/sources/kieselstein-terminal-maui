namespace Kieselstein.Models
{
    public static class XmlNs0TodoDetailEntrydetailTypeConstants
    {
        public const string NOTINITIALIZED = "NOTINITIALIZED";

        public const string PROJECTDETAIL = "PROJECTDETAIL";

        public const string IDENT = "IDENT";

        public const string MANUAL = "MANUAL";

        public const string TEXT = "TEXT";

        public const string ZWS = "ZWS";
    }
}
