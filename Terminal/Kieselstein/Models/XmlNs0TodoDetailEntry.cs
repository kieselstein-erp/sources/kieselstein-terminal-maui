using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TodoDetailEntry : XmlNs0BaseEntryId 
    {
        public string detailType { get; set; }
    }
}