namespace Kieselstein.Models
{
    public static class XmlNs0ItemPropertyEnumConstants
    {
        public const string NOIDENTIY = "NOIDENTIY";

        public const string SERIALNR = "SERIALNR";

        public const string BATCHNR = "BATCHNR";
    }
}
