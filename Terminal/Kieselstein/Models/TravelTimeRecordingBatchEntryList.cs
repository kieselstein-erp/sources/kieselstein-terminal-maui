using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TravelTimeRecordingBatchEntryList
    {
        public List<TravelTimeRecordingBatchEntry> entries { get; set; }

        public long rowCount { get; set; }
    }
}