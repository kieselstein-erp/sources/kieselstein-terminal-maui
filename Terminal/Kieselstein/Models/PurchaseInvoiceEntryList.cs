using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PurchaseInvoiceEntryList
    {
        public List<PurchaseInvoiceEntry> entries { get; set; }

        public long rowCount { get; set; }
    }
}