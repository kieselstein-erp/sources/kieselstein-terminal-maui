using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TodoDetailProjectEntry : XmlNs0TodoDetailEntry 
    {
        public bool html { get; set; }

        public string subject { get; set; }

        public string text { get; set; }

        public long timeMs { get; set; }
    }
}