using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DeliverableDeliveryEntry : BaseEntryId 
    {
        /// <summary>
        /// Auftragsnummer
        /// </summary>
        public string cnr { get; set; }

        /// <summary>
        /// Ist der Lieferschein der noch geliefert werden darf/kannDas ist dann der Fall, wenn er ANGELEGT ist.
        /// </summary>
        public bool deliverable { get; set; }

        public string status { get; set; }
    }
}