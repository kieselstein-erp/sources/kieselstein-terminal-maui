namespace Kieselstein.Models
{
    public static class TestResultMaterialStatusEntrymaterialStatusConstants
    {
        public const string NOTSTARTED = "NOTSTARTED";

        public const string INCOMPLETE = "INCOMPLETE";

        public const string COMPLETE = "COMPLETE";
    }
}
