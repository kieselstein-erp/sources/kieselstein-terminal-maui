using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TodoDetailEntryList
    {
        /// <summary>
        /// Die (leere) Liste aller Todoeintr&auml;ge
        /// </summary>
        public List<XmlNs0TodoDetailEntry> entries { get; set; }

        public long rowCount { get; set; }
    }
}