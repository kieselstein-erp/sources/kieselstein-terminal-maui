namespace Kieselstein.Models
{
    public static class MachineRecordingTypeConstants
    {
        public const string START = "START";

        public const string STOP = "STOP";
    }
}
