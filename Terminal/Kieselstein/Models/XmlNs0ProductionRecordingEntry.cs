using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Die Zeitdaten einer T&auml;tigkeit
    /// </summary>
    public class XmlNs0ProductionRecordingEntry : XmlNs0DocumentRecordingEntry 
    {
        public int machineId { get; set; }

        public int productionId { get; set; }

        public int productionWorkplanId { get; set; }
    }
}