using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PartlistEntryList
    {
        public List<PartlistEntry> list { get; set; }
    }
}