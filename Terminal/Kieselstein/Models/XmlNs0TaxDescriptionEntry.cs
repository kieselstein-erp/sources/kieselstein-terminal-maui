using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TaxDescriptionEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Die (in die Client-Sprache &uuml;bersetzte) Mehrwertsteuersatzbezeichnung
        /// </summary>
        public string description { get; set; }
    }
}