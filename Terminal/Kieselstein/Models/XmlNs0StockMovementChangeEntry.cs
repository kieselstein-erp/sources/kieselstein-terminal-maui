using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Die Daten der Handlagerbewegung f&uuml;r eine Umbuchung
    /// </summary>
    public class XmlNs0StockMovementChangeEntry : XmlNs0StockMovementEntry 
    {
        /// <summary>
        /// Die Id des Ziellagers
        /// </summary>
        public int targetStockId { get; set; }
    }
}