﻿namespace KieselsteinErp.Terminal.Kieselstein.Models
{
  public class GoodBadEntryList
  {
    public GoodBadEntryList() { entries = new(); }
    public List<GoodBadEntry> entries {  get; set; }
  }
}