using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ProjectEntry : XmlNs0BaseEntryId 
    {
        public string category { get; set; }

        public string cnr { get; set; }

        public string customerAddress { get; set; }

        public string customerName { get; set; }

        public int customerPartnerId { get; set; }

        public long deadlineMs { get; set; }

        public string internalComment { get; set; }

        public bool internalDone { get; set; }

        public int priority { get; set; }

        public string statusCnr { get; set; }

        public string title { get; set; }
    }
}