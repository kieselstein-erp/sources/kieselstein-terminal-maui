using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TodoDetailZwsEntry : TodoDetailEntry 
    {
        public string text { get; set; }
    }
}