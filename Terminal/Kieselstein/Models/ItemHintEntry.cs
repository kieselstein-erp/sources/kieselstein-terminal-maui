using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ItemHintEntry : BaseEntryId 
    {
        /// <summary>
        /// Der (html) Hinweistext
        /// </summary>
        public string content { get; set; }

        public string mimeType { get; set; }

        /// <summary>
        /// Die Artikelkommentarart des Hinweises
        /// </summary>
        public string cnr { get; set; }
    }
}