using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class HvmaLogonEntry : LogonEntry 
    {
        public string licence { get; set; }

        public string resource { get; set; }
    }
}