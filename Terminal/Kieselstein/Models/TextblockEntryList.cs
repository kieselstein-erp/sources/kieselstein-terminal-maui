using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TextblockEntryList
    {
        public List<TextblockEntry> entries { get; set; }

        public long rowCount { get; set; }
    }
}