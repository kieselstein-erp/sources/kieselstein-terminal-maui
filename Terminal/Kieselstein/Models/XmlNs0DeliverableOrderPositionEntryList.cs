using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0DeliverableOrderPositionEntryList
    {
        public List<XmlNs0DeliverableOrderPositionEntry> entries { get; set; }
    }
}