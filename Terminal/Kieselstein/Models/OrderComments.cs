using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class OrderComments : BaseEntryId 
    {
        public string internalComment { get; set; }

        public string externalComment { get; set; }
    }
}