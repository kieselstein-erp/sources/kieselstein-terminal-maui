using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class MultipartBody
    {
        public MediaType type { get; set; }

        public List<Attachment> childAttachments { get; set; }

        public Attachment rootAttachment { get; set; }

        public List<Attachment> allAttachments { get; set; }
    }
}