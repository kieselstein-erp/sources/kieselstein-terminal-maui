using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PartlistEmailEntry
    {
        public string emailText { get; set; }
    }
}