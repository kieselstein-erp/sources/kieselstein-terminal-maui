using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PartlistPositionPutEntry : PartlistPositionPostEntry 
    {
        public int id { get; set; }
    }
}