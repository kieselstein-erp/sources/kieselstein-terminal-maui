using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0CarEntry : XmlNs0BaseEntryId 
    {
        public string description { get; set; }

        public string numberPlate { get; set; }
    }
}