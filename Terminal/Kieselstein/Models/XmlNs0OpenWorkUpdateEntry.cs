using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0OpenWorkUpdateEntry : XmlNs0BaseEntryId 
    {
        public long finishtimeMs { get; set; }

        public int machineId { get; set; }

        public int machineOffsetMs { get; set; }

        /// <summary>
        /// Die Los-Nummer
        /// </summary>
        public string productionCnr { get; set; }

        public long starttimeMs { get; set; }

        /// <summary>
        /// Die Zeit in ms seit dem 1.1.1970
        /// </summary>
        public long workItemStartDate { get; set; }
    }
}