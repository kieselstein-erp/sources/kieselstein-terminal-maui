using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class LocalPingResult
    {
        /// <summary>
        /// Liefert den Zeitstempel des API Servers
        /// </summary>
        public long apiTime { get; set; }

        /// <summary>
        /// Liefert die Build-Nummer des API Servers
        /// </summary>
        public int apiBuildNumber { get; set; }

        /// <summary>
        /// Liefert die Versionsnummer des API Servers
        /// </summary>
        public string apiVersion { get; set; }
    }
}