using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ProductionGroupEntry : XmlNs0BaseEntryId 
    {
        public string description { get; set; }

        public int sortOrder { get; set; }
    }
}