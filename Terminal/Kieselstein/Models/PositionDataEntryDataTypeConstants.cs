namespace Kieselstein.Models
{
    public static class PositionDataEntrydataTypeConstants
    {
        public const string NOTINITIALIZED = "NOTINITIALIZED";

        public const string ITEM = "ITEM";

        public const string TEXT = "TEXT";
    }
}
