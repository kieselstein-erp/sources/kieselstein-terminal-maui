using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class OpenWorkEntry : BaseEntryId 
    {
        public string productionCnr { get; set; }

        public string productionProjectNr { get; set; }

        public string partlistCnr { get; set; }

        public string partlistDescription { get; set; }

        public int workNumber { get; set; }

        public int? subWorkstepNumber { get; set; }

        public string workItemCnr { get; set; }

        public string workItemDescription { get; set; }

        public string workItemShortDescription { get; set; }

        public long workItemStartDate { get; set; }

        public int workItemStartCalendarWeek { get; set; }

        public int workItemStartCalendarYear { get; set; }

        public int machineOffsetMs { get; set; }

        public double duration { get; set; }

        public string machineCnr { get; set; }

        public string machineDescription { get; set; }

        public string materialCnr { get; set; }

        public string materialDescription { get; set; }

        public bool hasWorktime { get; set; }

        public string customerShortDescription { get; set; }

        public string abc { get; set; }

        public int machineId { get; set; }

        public int orderId { get; set; }

        public string orderCnr { get; set; }

        public bool overdue { get; set; }

        public long orderFinalDateMs { get; set; }

        public double progressPercent { get; set; }

        public double targetDuration { get; set; }

        public string partlistItemCnr { get; set; }

        public string partlistItemDescription { get; set; }

        public string partlistItemShortDescription { get; set; }

        /// <summary>
        /// Die bisher angefallene Ist-Zeit f&uuml;r diesen Arbeitsschritt/Arbeitsgang
        /// </summary>
        public double actualTime { get; set; }

        /// <summary>
        /// L&auml;sst sich die Startzeit verschieben?
        /// </summary>
        public bool starttimeMoveable { get; set; }

        /// <summary>
        /// L&auml;sst sich die Endezeit verschieben?
        /// </summary>
        public bool finishtimeMoveable { get; set; }

        public decimal openQuantity { get; set; }

        public long productionFinalDateMs { get; set; }

        public int priority { get; set; }
    }
}