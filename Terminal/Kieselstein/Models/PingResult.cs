using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PingResult
    {
        /// <summary>
        /// Zeitpunkt im ms (seit 1.1.1970) am API Server
        /// </summary>
        public long apiTime { get; set; }

        /// <summary>
        /// Zeitpunkt im ms (seit 1.1.1970) am HELIUMV Server
        /// </summary>
        public long serverTime { get; set; }

        /// <summary>
        /// HELIUM V Server Build Nummer
        /// </summary>
        public int serverBuildNumber { get; set; }

        /// <summary>
        /// HELIUM V Server Version Nummer
        /// </summary>
        public string serverVersionNumber { get; set; }

        /// <summary>
        /// Die Zeitspanne in ms die ben&ouml;tigt wurde, um eine Verbindung zum HELIUM V Server aufzunehmen unddie Serverinformationen zu erhalten
        /// </summary>
        public long serverDuration { get; set; }

        /// <summary>
        /// Die eindeutige Id des HELIUM V Servers
        /// </summary>
        public int serverId { get; set; }
    }
}