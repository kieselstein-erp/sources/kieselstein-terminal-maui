using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class StockEntry : BaseEntryId 
    {
        public string name { get; set; }

        public string typeCnr { get; set; }
    }
}