using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0PartlistPositionEntry : XmlNs0BaseEntryId 
    {
        public long amount { get; set; }

        public long calcPrice { get; set; }

        public string cnr { get; set; }

        public string comment { get; set; }

        public string customerItemCnr { get; set; }

        public string description { get; set; }

        public int iSort { get; set; }

        public bool itemHidden { get; set; }

        public int itemId { get; set; }

        public bool itemLocked { get; set; }

        public string position { get; set; }

        public long salesPrice { get; set; }

        public string unitCnr { get; set; }
    }
}