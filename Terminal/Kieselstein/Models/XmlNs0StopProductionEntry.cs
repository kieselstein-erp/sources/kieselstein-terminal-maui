using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0StopProductionEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Die (optionale) Fehlermeldung, die beim Benachrichtigen &uuml;ber dasStoppen der Produktion beim Fremdsystem entstanden ist
        /// </summary>
        public string postMessage { get; set; }

        /// <summary>
        /// Wurde das Los gestoppt?
        /// </summary>
        public bool stopped { get; set; }
    }
}