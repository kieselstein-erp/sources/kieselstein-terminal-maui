﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Kieselstein.Models
{
  public class TimeRecordingGoodBadEntry : TimeRecordingEntry
  {
    public bool isGoing {  get; set; }

    public GoodBadEntryList goodBadEntries { get; set; }
  }
}
