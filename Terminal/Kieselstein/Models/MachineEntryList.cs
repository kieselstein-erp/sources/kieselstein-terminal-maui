using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class MachineEntryList
    {
        public List<MachineEntry> entries { get; set; }
    }
}