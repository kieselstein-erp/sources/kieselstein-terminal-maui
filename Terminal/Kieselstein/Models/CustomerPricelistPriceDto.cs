using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CustomerPricelistPriceDto
    {
        public string currency { get; set; }

        public string pricetypKey { get; set; }

        public double amount { get; set; }

        public bool specialCondition { get; set; }

        public double fixPrice { get; set; }

        public double basePrice { get; set; }

        public double calculatedPrice { get; set; }

        public double discountRate { get; set; }
    }
}