using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TravelTimeRecordingBatchEntry
    {
        /// <summary>
        /// Das Jahr der Reise
        /// </summary>
        public int year { get; set; }

        /// <summary>
        /// Der Monat der Reise im Berech 1 - 12
        /// </summary>
        public int month { get; set; }

        /// <summary>
        /// Der Tag der Reise im Bereich 1 - 31
        /// </summary>
        public int day { get; set; }

        /// <summary>
        /// Die Stunde der Reise im Bereich 0 - 23
        /// </summary>
        public int hour { get; set; }

        /// <summary>
        /// Die Minute der Reise im Bereich 0 - 59
        /// </summary>
        public int minute { get; set; }

        /// <summary>
        /// Die (optionale) Sekunde der Reise im Bereich 0 - 59
        /// </summary>
        public int second { get; set; }

        /// <summary>
        /// Die Beleg-Id (des Auftrags)
        /// </summary>
        public int hvid { get; set; }

        public string todoType { get; set; }

        /// <summary>
        /// Ein (optionaler) Kommentar
        /// </summary>
        public string remark { get; set; }

        /// <summary>
        /// Die (optionale) Fahrzeug-IdWird ein Firmenfahrzeug verwendet, sollte die Iddes Fahrzeugs angegeben werden. Wird das private Fahrzeug,oder ein Mietfahrzeug verwendet, ist das Kennzeichenanzugeben
        /// </summary>
        public int carId { get; set; }

        /// <summary>
        /// Das (optionale) Kennzeichen des FahrzeugsWird ein Firmenfahrzeug verwendet, sollte die Iddes Fahrzeugs angegeben werden. Wird das private Fahrzeug,oder ein Mietfahrzeug verwendet, ist das Kennzeichenanzugeben und die Id leer zu lassen
        /// </summary>
        public string plateNumber { get; set; }

        /// <summary>
        /// Der (optionale) Tachostand zum Zeitpunkt dieser ReiseEr ist nur anzugeben, wenn man das Fahrzeug selbstgefahren hat (und es sich nat&uuml;rlich um ein Fahrzeuggehandelt hat). Bei Zug/Flugzeug ist der Tachostandnicht anzugeben.
        /// </summary>
        public int mileage { get; set; }

        /// <summary>
        /// Handelt es sich um den Beginn/Weiterfahrt der Reiseoder um das Ende?
        /// </summary>
        public bool startOfTravel { get; set; }

        /// <summary>
        /// Die Id der zu verwendenden Di&auml;t
        /// </summary>
        public int dailyAllowanceId { get; set; }
    }
}