using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class IdentityAmountEntry
    {
        public string identity { get; set; }

        public decimal amount { get; set; }
    }
}