using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class OrderAddressContact
    {
        public OrderAddress partnerAddress { get; set; }

        public OrderAddress contactAddress { get; set; }
    }
}