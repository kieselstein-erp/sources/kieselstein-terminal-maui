using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TodoEntryList
    {
        /// <summary>
        /// Die (leere) Liste aller Todoeintr&auml;ge
        /// </summary>
        public List<TodoEntry> entries { get; set; }

        public long rowCount { get; set; }

        public int serverId { get; set; }

        public int barcodeLength { get; set; }

        public int barcodeLengthOrderRelated { get; set; }

        public List<string> hvmaEnums { get; set; }

        public List<HvmaParamEntry> hvmaParams { get; set; }

        public List<string> judgeEnums { get; set; }
    }
}