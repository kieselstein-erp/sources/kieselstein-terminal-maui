using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0HvmaLogonEntry : XmlNs0LogonEntry 
    {
        public string licence { get; set; }

        public string resource { get; set; }
    }
}