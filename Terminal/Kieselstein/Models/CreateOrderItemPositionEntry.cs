using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CreateOrderItemPositionEntry
    {
        /// <summary>
        /// Die (optionale) Artikel-IdEntweder #getItemId() oder #getItemCnr() mussangegeben werden
        /// </summary>
        public int itemId { get; set; }

        /// <summary>
        /// Die zu lieferende MengeBei identit&auml;tsbehafteten Artikeln (also jene mit Serien- oderChargennummer) ist die Gesamtmenge aus den einzelnen Serien- oder Chargennummerntrotzdem anzugeben
        /// </summary>
        public double amount { get; set; }

        public string itemCnr { get; set; }

        /// <summary>
        /// Die Id des Kostentr&auml;gersDie Id des Kostentr&auml;gers ist dann und nur dann anzugeben,wenn die Zusatzfunktion "KOSTENTR&Auml;GER" f&uuml;r denjeweiligen Mandanten freigeschaltet ist. 
        /// </summary>
        public int costBearingUnitId { get; set; }
    }
}