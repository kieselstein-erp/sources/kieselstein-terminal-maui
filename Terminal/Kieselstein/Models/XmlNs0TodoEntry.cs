using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Beschreibt die Properties eines Todo-Eintrags
    /// </summary>
    public class XmlNs0TodoEntry : XmlNs0BaseEntryId 
    {
        public string cnr { get; set; }

        public string comment { get; set; }

        public XmlNs0TodoDetailEntryList details { get; set; }

        public XmlNs0TodoDocumentEntryList documents { get; set; }

        public long dueDateMs { get; set; }

        public string formattedDeliveryAddress { get; set; }

        public string manufacturingPlace { get; set; }

        /// <summary>
        /// Liefert die Auftragsnummer, die das Los erzeugt hat
        /// </summary>
        public string orderCnr { get; set; }

        public string partnerName { get; set; }

        /// <summary>
        /// Die (fr&uuml;heste) Beginnzeit des Loses/Auftrags
        /// </summary>
        public long startDateMs { get; set; }

        public string title { get; set; }

        public string type { get; set; }
    }
}