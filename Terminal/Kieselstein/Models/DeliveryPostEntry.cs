using KieselsteinErp.Terminal.Kieselstein.Models;
using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DeliveryPostEntry
    {
        /// <summary>
        /// Menge der LosablieferungHandelt es sich um eine Ablieferung mit Seriennr/Chargennr, so ist sie dieSumme der Mengen der identities
        /// </summary>
        public decimal amount { get; set; }

        public TestResultEntryList testResultEntries { get; set; }

        /// <summary>
        /// Ziellager der Losablieferung (optional) Wenn nicht angegeben, wird als Ziellager das im Los definierte verwendet.
        /// </summary>
        public int? stockId { get; set; }

        /// <summary>
        /// Ist der beim Logon ermittelte "Token"
        /// </summary>
        public string userId { get; set; }

        /// <summary>
        /// Initiiert eine ENDE Zeitbuchung (optional)Wenn auf true, dann muss zus&auml;tzlich das TimeRecordingEntrymitgeliefert werden, aus dem die erforderlichen Daten f&uuml;r die Zeitbuchung geholt werden.
        /// </summary>
        public bool bookStopping { get; set; }

        public TimeRecordingEntry timeRecordingEntry { get; set; }

        /// <summary>
        /// Initiiert eine Maschinenstoppzeitbuchung (optional)Wenn auf true, dann muss zus&auml;tzlich das TimeRecordingEntrymitgeliefert werden, aus dem die erforderlichen Daten f&uuml;r die Zeitbuchung geholt werden,und der Arbeitsgang aus dem Los productionWorkplanId.
        /// </summary>
        public bool bookMachineStopping { get; set; }

        /// <summary>
        /// Id des Arbeitsgang des Lossollarbeitsplans (optional)Erforderlich, wenn bookMachineStopping auf true ist.
        /// </summary>
        public int? productionWorkplanId { get; set; }

        /// <summary>
        /// Schrottmenge der Losablieferung (optional)Diese Menge wird auf das definierte Schrottlager gebucht.
        /// </summary>
        public decimal? scrapAmount { get; set; }

        /// <summary>
        /// Initiiert eine Aenderung der Losgr&ouml;&szlig;e um die Schrottmenge
        /// </summary>
        public bool increaseLotSizeByScrapAmount { get; set; }

        /// <summary>
        /// Die Liste der Seriennr/Chargennr Informationen (optional)Wenn angegeben, muss die Summe dieser Mengen die Menge der Losablieferung (amount) ergeben.
        /// </summary>
        public List<IdentityAmountEntry> identities { get; set; }

        /// <summary>
        /// Die Liste der Geräteseriennr Informationen (optional) Erforderlich wenn Zusatzfunktion Geräteseriennummern aktiviert ist.
        /// </summary>
        public List<ItemIdentityAmountEntry> deviceIdentities { get; set; }
  }
}