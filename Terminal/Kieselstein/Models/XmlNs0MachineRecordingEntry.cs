using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0MachineRecordingEntry : XmlNs0TimeRecordingEntry 
    {
        public int machineId { get; set; }

        public string machineRecordingType { get; set; }

        public int productionWorkplanId { get; set; }

        public string remark { get; set; }
    }
}