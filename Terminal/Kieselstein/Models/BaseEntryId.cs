using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class BaseEntryId
    {
        /// <summary>
        /// Die eindeutige Id des Eintrags
        /// </summary>
        public int id { get; set; }
    }
}