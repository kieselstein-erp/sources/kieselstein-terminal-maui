using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Die Metadaten einer Position.Id der PositionPositionsartisort
    /// </summary>
    public class XmlNs0PositionDataEntry : XmlNs0BaseEntryId 
    {
        public string dataType { get; set; }

        public int isort { get; set; }
    }
}