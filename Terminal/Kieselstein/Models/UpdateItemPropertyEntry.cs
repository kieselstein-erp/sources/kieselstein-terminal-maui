using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class UpdateItemPropertyEntry : UpdatePropertyEntry 
    {
        /// <summary>
        /// Id des Artikels dieser Eigenschaft (optional)Ist auch die Artikelnummer angegeben hat die Id Priorit&auml;t
        /// </summary>
        public int itemId { get; set; }

        /// <summary>
        /// Artikelnummer des Artikels dieser Eigenschaft (optional)
        /// </summary>
        public string itemCnr { get; set; }
    }
}