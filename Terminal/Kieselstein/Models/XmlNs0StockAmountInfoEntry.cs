using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0StockAmountInfoEntry
    {
        public long availableAmount { get; set; }

        public long missingAmount { get; set; }

        public long reservedAmount { get; set; }

        public long stockAmount { get; set; }
    }
}