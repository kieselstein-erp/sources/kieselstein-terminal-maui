using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0OrderComments : XmlNs0BaseEntryId 
    {
        public string externalComment { get; set; }

        public string internalComment { get; set; }
    }
}