using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Repr&auml;sentiert eine zu liefernde Position
    /// </summary>
    public class CreatedGoodsReceiptPositionEntry
    {
        /// <summary>
        /// Die noch offene Menge der Bestellung
        /// </summary>
        public decimal openQuantity { get; set; }

        public int purchaseOrderPositionId { get; set; }

        /// <summary>
        /// Die Id des Wareneingangs
        /// </summary>
        public int goodsReceiptId { get; set; }

        /// <summary>
        /// Die Id der Wareneingangsposition
        /// </summary>
        public int goodsReceiptPositionId { get; set; }
    }
}