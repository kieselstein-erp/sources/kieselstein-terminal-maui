using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class StockInfoEntry
    {
        public StockEntry stockEntry { get; set; }

        public StockPlaceEntryList stockplaceEntries { get; set; }
    }
}