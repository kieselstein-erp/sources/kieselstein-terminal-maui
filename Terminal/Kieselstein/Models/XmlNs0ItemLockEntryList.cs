using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ItemLockEntryList
    {
        public List<XmlNs0ItemLockEntry> entries { get; set; }
    }
}