using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TestPlanMultipleCrimpIsoEntry : XmlNs0TestPlanCrimpIsoEntry 
    {
        public string itemCnrSecondStrand { get; set; }

        public string itemDescriptionSecondStrand { get; set; }

        public long strippingForceSecondStrand { get; set; }
    }
}