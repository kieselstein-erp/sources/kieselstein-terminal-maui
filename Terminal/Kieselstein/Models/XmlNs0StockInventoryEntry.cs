using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0StockInventoryEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Lager-"Name"
        /// </summary>
        public string cnr { get; set; }

        public XmlNs0StockInfoItemEntryList entries { get; set; }
    }
}