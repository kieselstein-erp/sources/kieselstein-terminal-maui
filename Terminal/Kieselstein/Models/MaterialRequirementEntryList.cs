using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class MaterialRequirementEntryList
    {
        public List<MaterialRequirementEntry> entries { get; set; }
    }
}