using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0PartlistWorkstepEntryList
    {
        public List<XmlNs0PartlistWorkstepEntry> entries { get; set; }
    }
}