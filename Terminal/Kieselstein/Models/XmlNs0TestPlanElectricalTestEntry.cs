using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TestPlanElectricalTestEntry : XmlNs0TestPlanEntry 
    {
        public string itemCnr { get; set; }

        public string itemDescription { get; set; }
    }
}