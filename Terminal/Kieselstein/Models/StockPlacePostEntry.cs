using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class StockPlacePostEntry
    {
        /// <summary>
        /// Id des Lagerplatzes, optional wenn Name des Lagerplatzes &uuml;bergeben wird.Werden Id und Name &uuml;bergeben, wird die Id vorrangig behandelt.
        /// </summary>
        public int? stockplaceId { get; set; }

        /// <summary>
        /// Name des Lagerplatzes, optional wenn Id des Lagerplatzes &uuml;bergeben wird.Werden Id und Name &uuml;bergeben, wird die Id vorrangig behandelt.
        /// </summary>
        public string stockplaceName { get; set; }

        /// <summary>
        /// Id des Artikels, dem der Lagerplatz zugewiesen werden soll
        /// </summary>
        public int itemId { get; set; }
    }
}