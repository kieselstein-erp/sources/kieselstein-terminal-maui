using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CustomerDetailLoggedOnEntry
    {
        public CustomerDetailEntry detailEntry { get; set; }

        public PartnerEntry accountEntry { get; set; }

        public PartnerEntry representativeEntry { get; set; }
    }
}