using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ForecastPositionEntry : XmlNs0BaseEntryId 
    {
        public long dateMs { get; set; }

        public XmlNs0ItemV1Entry itemEntry { get; set; }

        public List<XmlNs0LinecallEntry> linecallEntries { get; set; }

        public string ordernumber { get; set; }

        public string productType { get; set; }

        public string productionCnr { get; set; }

        public long quantity { get; set; }

        public string staffDescription { get; set; }

        public int staffId { get; set; }
    }
}