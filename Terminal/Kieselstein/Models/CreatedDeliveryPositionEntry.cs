using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Repr&auml;sentiert die Information &uuml;ber die gelieferte Position
    /// </summary>
    public class CreatedDeliveryPositionEntry
    {
        /// <summary>
        /// Die Id des Lieferscheins
        /// </summary>
        public int deliveryId { get; set; }

        /// <summary>
        /// Die LieferscheinnummerWurde beim Erzeugen der Lieferscheinposition kein Lieferschein angegeben, soist hier die neu erstellte Lieferscheinnummer enthalten. Ansonsten diejenige,auf die geliefert werden sollte.
        /// </summary>
        public string deliveryCnr { get; set; }

        /// <summary>
        /// Die PositionId der Lieferscheinposition
        /// </summary>
        public int deliveryPositionId { get; set; }

        /// <summary>
        /// Die Id des Auftrags auf den sich die Lieferscheinposition, bzw. die Auftragposition bezieht.
        /// </summary>
        public int orderId { get; set; }

        /// <summary>
        /// Die Auftragsposition die (teil)geliefert werden soll
        /// </summary>
        public int orderPositionId { get; set; }

        /// <summary>
        /// Die insgesamt zu liefernde Menge
        /// </summary>
        public decimal amount { get; set; }

        /// <summary>
        /// Die Liste der Chargen- bzw. Seriennummern
        /// </summary>
        public ItemIdentityEntryList itemIdentity { get; set; }

        /// <summary>
        /// Die noch offene Menge im Auftrag
        /// </summary>
        public decimal openAmount { get; set; }

        /// <summary>
        /// Die f&uuml;r den Auftrag gelieferte Menge in diesem Lieferschein
        /// </summary>
        public decimal deliveredAmount { get; set; }
    }
}