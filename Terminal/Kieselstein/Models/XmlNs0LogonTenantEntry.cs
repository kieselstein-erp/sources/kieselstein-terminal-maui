using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0LogonTenantEntry : XmlNs0LogonEntry 
    {
        public string tenantCnr { get; set; }
    }
}