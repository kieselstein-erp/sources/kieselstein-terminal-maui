using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Repr&auml;sentiert eine zu liefernde Position
    /// </summary>
    public class CreateDeliveryPositionEntry
    {
        /// <summary>
        /// Die (optionale) Lieferschein-Id, auf die diese Position geliefert werden soll.Wird die Id nicht angegeben, wird ein neuer Lieferschein generiert und auf diesenneuen Lieferschein geliefert. Ansonsten wird der hier angegebene Lieferschein verwendet
        /// </summary>
        public int? deliveryId { get; set; }

        /// <summary>
        /// Die Auftragsnummer f&uuml;r die die Lieferung erfolgen soll
        /// </summary>
        public int orderId { get; set; }

        /// <summary>
        /// Die Auftragsposition die (teil)geliefert werden soll
        /// </summary>
        public int orderPositionId { get; set; }

        /// <summary>
        /// Die Liste der Chargen- bzw. Seriennummern
        /// </summary>
        public ItemIdentityEntryList itemIdentity { get; set; }

        /// <summary>
        /// Die zu lieferende MengeBei identit&auml;tsbehafteten Artikeln (also jene mit Serien- oderChargennummer) ist die Gesamtmenge aus den einzelnen Serien- oder Chargennummerntrotzdem anzugeben
        /// </summary>
        public decimal amount { get; set; }
    }
}