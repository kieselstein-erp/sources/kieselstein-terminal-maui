namespace Kieselstein.Models
{
    public static class TravelTimeRecordingBatchEntrytodoTypeConstants
    {
        public const string NOTINITIALIZED = "NOTINITIALIZED";

        public const string PROJECT = "PROJECT";

        public const string ORDER = "ORDER";

        public const string PRODUCTION = "PRODUCTION";
    }
}
