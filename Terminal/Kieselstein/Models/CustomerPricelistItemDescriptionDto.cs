using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CustomerPricelistItemDescriptionDto
    {
        public string name { get; set; }

        public string additionalName { get; set; }

        public string additionalName2 { get; set; }

        public string shortName { get; set; }
    }
}