using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestPlanMultipleCrimpEntry : TestPlanCrimpEntry 
    {
        public string itemCnrSecondStrand { get; set; }

        public string itemDescriptionSecondStrand { get; set; }

        public double strippingForceSecondStrand { get; set; }
    }
}