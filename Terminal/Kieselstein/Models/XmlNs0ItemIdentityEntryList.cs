using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Die Liste der Chargen- bzw. Seriennummern
    /// </summary>
    public class XmlNs0ItemIdentityEntryList
    {
        /// <summary>
        /// die (leere) Liste der Identit&auml;ten diesesArtikels. F&uuml;r einen chargennummernbehafteten Artikel dieChargennummer und die zugeh&ouml;rige Menge. F&uuml;r Seriennummerndie Menge 1 und die jeweilige Seriennummer
        /// </summary>
        public List<XmlNs0ItemIdentityEntry> entries { get; set; }
    }
}