using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ParameterRequestEntry : XmlNs0BaseEntryCnr 
    {
        public string category { get; set; }
    }
}