namespace Kieselstein.Models
{
    public static class XmlNs0ProductTypeConstants
    {
        public const string PICKING = "PICKING";

        public const string STOCK = "STOCK";
    }
}
