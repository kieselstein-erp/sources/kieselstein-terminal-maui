using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DailyAllowanceEntryList
    {
        public List<DailyAllowanceEntry> entries { get; set; }

        public long rowCount { get; set; }
    }
}