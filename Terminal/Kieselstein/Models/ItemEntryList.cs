using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ItemEntryList
    {
        /// <summary>
        /// Die Liste aller ItemGroupEntry Eintr&auml;ge
        /// </summary>
        public List<ItemEntry> entries { get; set; }

        public long rowCount { get; set; }
    }
}