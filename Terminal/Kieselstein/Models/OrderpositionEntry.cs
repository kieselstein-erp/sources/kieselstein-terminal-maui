using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class OrderpositionEntry : BaseEntryId 
    {
        public int positionNr { get; set; }

        public double amount { get; set; }

        public string unitCnr { get; set; }

        public string itemCnr { get; set; }

        public string description { get; set; }

        public double price { get; set; }

        public string status { get; set; }
    }
}