using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestResultOpticalTestEntry : TestResultEntry 
    {
        public bool executed { get; set; }
    }
}