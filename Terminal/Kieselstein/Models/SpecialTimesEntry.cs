using Newtonsoft.Json;

namespace Kieselstein.Models
{
  public class SpecialTimesEntry
  {
    public string timeType { get; set; }

    public long fromDateMs { get; set; }

    public long toDateMs { get; set; }

    public bool halfDay { get; set; }

    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public int? forStaffId { get; set; }

  }
}