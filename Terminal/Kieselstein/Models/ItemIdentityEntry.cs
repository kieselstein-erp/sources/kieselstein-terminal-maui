using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Die Seriennummer bzw. Chargennummer mit der zugehoerigen Menge
    /// </summary>
    public class ItemIdentityEntry
    {
        /// <summary>
        /// Die Serien- oder Chargennummer
        /// </summary>
        public string identity { get; set; }

        /// <summary>
        /// Die Menge dieser Seriennr (1) bzw. Chargennummer (>= 0)
        /// </summary>
        public decimal amount { get; set; }

        /// <summary>
        /// Die Versionsinformation dieser Serien- bzw. Chargennummer
        /// </summary>
        public string version { get; set; }

        /// <summary>
        /// Die Gebindenummer
        /// </summary>
        public string bundleIdentity { get; set; }

        /// <summary>
        /// Die Gebindemenge
        /// </summary>
        public double bundleAmount { get; set; }
    }
}