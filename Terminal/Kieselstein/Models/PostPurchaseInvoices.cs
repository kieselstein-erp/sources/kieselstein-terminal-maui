using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PostPurchaseInvoices
    {
        public PurchaseInvoiceEntryList invoices { get; set; }
    }
}