using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Beschreibt die Properties eines Todo-Eintrags
    /// </summary>
    public class TodoEntry : BaseEntryId 
    {
        public string type { get; set; }

        public string cnr { get; set; }

        public string partnerName { get; set; }

        public string title { get; set; }

        public long dueDateMs { get; set; }

        public string comment { get; set; }

        public TodoDetailEntryList details { get; set; }

        public string manufacturingPlace { get; set; }

        /// <summary>
        /// Die (fr&uuml;heste) Beginnzeit des Loses/Auftrags
        /// </summary>
        public long startDateMs { get; set; }

        /// <summary>
        /// Liefert die Auftragsnummer, die das Los erzeugt hat
        /// </summary>
        public string orderCnr { get; set; }

        public TodoDocumentEntryList documents { get; set; }

        public string formattedDeliveryAddress { get; set; }
    }
}