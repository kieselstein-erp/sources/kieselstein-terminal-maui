using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class WorkCalendarEntry : BaseEntryId 
    {
        /// <summary>
        /// Das Datum (ms seit 1.1.1970)
        /// </summary>
        public long date { get; set; }

        /// <summary>
        /// Die Id der Tagesart (Montag, Dienstag, ..., Feiertag, Betriebsurlaub)
        /// </summary>
        public int dayTypeId { get; set; }

        /// <summary>
        /// Die Bezeichnung der Tagesart
        /// </summary>
        public string dayTypeDescription { get; set; }

        /// <summary>
        /// Die Bezeichnung/Name des Feiertags
        /// </summary>
        public string description { get; set; }
    }
}