using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Repr&auml;sentiert eine zu liefernde Position
    /// </summary>
    public class CreateGoodsReceiptPositionEntry
    {
        /// <summary>
        /// Die Liste der Chargen- bzw. Seriennummern
        /// </summary>
        public ItemIdentityEntryList itemIdentity { get; set; }

        /// <summary>
        /// Die zu lieferende MengeBei identit&auml;tsbehafteten Artikeln (also jene mit Serien- oderChargennummer) ist die Gesamtmenge aus den einzelnen Serien- oder Chargennummerntrotzdem anzugeben
        /// </summary>
        public decimal amount { get; set; }

        public int purchaseOrderPositionId { get; set; }

        /// <summary>
        /// Die optionale Lieferscheinnummer des LieferantenEntweder die Lieferscheinnummer ist angegeben, oder die Id des WareneingangsIst noch kein Wareneingang mit dieser Lieferscheinnummer bekannt, wirdein neuer Wareneingang mit der Lieferscheinnummer angelegt
        /// </summary>
        public string deliverySlipCnr { get; set; }

        /// <summary>
        /// Die optionale Id des WareneingangsDie Wareneingangs-Id hat Vorrang vor der Lieferscheinnummer
        /// </summary>
        public Nullable<int> goodsReceiptId { get; set; }
    }
}