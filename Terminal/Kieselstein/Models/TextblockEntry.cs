using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TextblockEntry : BaseEntryId 
    {
        /// <summary>
        /// Die Kennung des Textbausteins
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// Ist der Textbaustein versteckt?
        /// </summary>
        public bool hidden { get; set; }

        public string mimeType { get; set; }

        /// <summary>
        /// Die Sprache in der der Textbaustein angelegt ist
        /// </summary>
        public string localeCnr { get; set; }

        /// <summary>
        /// Der Dateiname eines Bin&auml;rtextbausteins
        /// </summary>
        public string filename { get; set; }

        /// <summary>
        /// Der komplette Text des TextbausteinsDer Text kann auch Jasperstyle Auszeichnungen enthalten
        /// </summary>
        public string text { get; set; }

        /// <summary>
        /// Der bin&auml;re Inhalt des "Text"bausteins, sofern essich um einen bin&auml;rer Textbaustein handelt.
        /// </summary>
        public string blob { get; set; }
    }
}