using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0OrderpositionEntry : XmlNs0BaseEntryId 
    {
        public long amount { get; set; }

        public string description { get; set; }

        public string itemCnr { get; set; }

        public int positionNr { get; set; }

        public long price { get; set; }

        public string status { get; set; }

        public string unitCnr { get; set; }
    }
}