using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class LinecallDeliveryPostEntry
    {
        public string userId { get; set; }

        public int itemId { get; set; }

        public double quantity { get; set; }

        public string pickingType { get; set; }
    }
}