using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class HvmaParamEntry : BaseEntryCnr 
    {
        public string category { get; set; }

        public string value { get; set; }
    }
}