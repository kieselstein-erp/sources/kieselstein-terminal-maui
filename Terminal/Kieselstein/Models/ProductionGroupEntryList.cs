using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ProductionGroupEntryList
    {
        public List<ProductionGroupEntry> entries { get; set; }
    }
}