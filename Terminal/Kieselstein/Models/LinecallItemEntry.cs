using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class LinecallItemEntry
    {
        public ItemV1Entry itemEntry { get; set; }

        public double quantity { get; set; }

        public double openQuantity { get; set; }
    }
}