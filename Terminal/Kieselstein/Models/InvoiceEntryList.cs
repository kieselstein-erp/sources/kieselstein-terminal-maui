using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class InvoiceEntryList
    {
        /// <summary>
        /// Die Liste aller InvoiceEntry Eintr&auml;ge
        /// </summary>
        public List<InvoiceEntry> entries { get; set; }
    }
}