namespace Kieselstein.Models
{
    public static class AuslastungsAnzeigeDetailAgConstants
    {
        public const string STANDARD = "STANDARD";

        public const string PROJEKTKBEZ = "PROJEKT_KBEZ";

        public const string UNBEKANNT = "UNBEKANNT";
    }
}
