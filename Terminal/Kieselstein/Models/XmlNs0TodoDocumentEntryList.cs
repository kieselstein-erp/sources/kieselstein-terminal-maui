using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TodoDocumentEntryList
    {
        /// <summary>
        /// Die (leere) Liste aller Dokumente eines Todos.
        /// </summary>
        public List<XmlNs0TodoDocumentEntry> entries { get; set; }

        /// <summary>
        /// Die Anzahl der Dokumente
        /// </summary>
        public long rowCount { get; set; }
    }
}