using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class InvoiceEntry : BaseEntryId 
    {
        /// <summary>
        /// Die Rechnungsnummer
        /// </summary>
        public string cnr { get; set; }

        /// <summary>
        /// Der Kundenname
        /// </summary>
        public string customerName { get; set; }

        /// <summary>
        /// Der Ort im Format "- ", also zum Beispiel "AT-5301 Eugendorf"
        /// </summary>
        public string customerCity { get; set; }

        public string project { get; set; }

        /// <summary>
        /// Der Bruttobetrag der Rechnung
        /// </summary>
        public double grossValue { get; set; }

        /// <summary>
        /// Die W&auml;hrung in der die Rechnung ausgestellt ist
        /// </summary>
        public string currency { get; set; }

        /// <summary>
        /// Die Id des Rechnungskunden
        /// </summary>
        public int customerId { get; set; }

        /// <summary>
        /// Der Nettobetrag der Rechnung
        /// </summary>
        public double netValue { get; set; }

        /// <summary>
        /// Der noch offene Bruttobetrag der Rechnung
        /// </summary>
        public double openGrossValue { get; set; }

        public string status { get; set; }

        public CustomerDetailEntry customerEntry { get; set; }
    }
}