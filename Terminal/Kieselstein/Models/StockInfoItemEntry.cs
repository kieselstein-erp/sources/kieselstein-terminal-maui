using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class StockInfoItemEntry : BaseEntryId 
    {
        /// <summary>
        /// Die auf dem Lager befindliche Menge
        /// </summary>
        public double amount { get; set; }

        /// <summary>
        /// Der Lagermindestmenge
        /// </summary>
        public double minimum { get; set; }

        /// <summary>
        /// Die Lagersollmenge
        /// </summary>
        public double nominal { get; set; }
    }
}