using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestPlanEntryList
    {
        public List<TestPlanEntry> entries { get; set; }
    }
}