using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ItemEntryInternal : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Die St&uuml;cklistenart
        /// </summary>
        public string billOfMaterialType { get; set; }

        /// <summary>
        /// Die Kennung des Artikels (Artikelnummer)
        /// </summary>
        public string cnr { get; set; }

        /// <summary>
        /// Die Liste aller Artikelkommentare im Format "text/html"
        /// </summary>
        public List<string> comments { get; set; }

        /// <summary>
        /// Der Gestehungspreis
        /// </summary>
        public long costs { get; set; }

        /// <summary>
        /// Die Zusatzbezeichnung des Artikels
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// Die Zusatzbezeichnung2 des Artikels
        /// </summary>
        public string description2 { get; set; }

        public XmlNs0DocumentInfoEntryList documentInfoEntries { get; set; }

        /// <summary>
        /// Handelt es sich um einen chargennummerntragenden Artikel?
        /// </summary>
        public bool hasChargenr { get; set; }

        /// <summary>
        /// Handelt es sich um einen seriennummerntragenden Artikel?
        /// </summary>
        public bool hasSerialnr { get; set; }

        /// <summary>
        /// Ist es ein versteckter Artikel?
        /// </summary>
        public bool hidden { get; set; }

        /// <summary>
        /// Die Indexnummer
        /// </summary>
        public string index { get; set; }

        public XmlNs0ItemCommentMediaInfoEntryList itemCommentMediaInfoEntries { get; set; }

        /// <summary>
        /// Die Artikelklasse
        /// </summary>
        public string itemclassCnr { get; set; }

        /// <summary>
        /// Die Artikelgruppe
        /// </summary>
        public string itemgroupCnr { get; set; }

        /// <summary>
        /// Der Name des Artikels
        /// </summary>
        public string name { get; set; }

        public XmlNs0PackagingInfoEntryList packagingEntries { get; set; }

        public XmlNs0ProducerInfoEntry producerInfo { get; set; }

        /// <summary>
        /// Die Referenznummer
        /// </summary>
        public string referenceNumber { get; set; }

        /// <summary>
        /// Die Revisions"nummer"
        /// </summary>
        public string revision { get; set; }

        /// <summary>
        /// Die Kurzbezeichnung des Artikels
        /// </summary>
        public string shortName { get; set; }

        /// <summary>
        /// Der "verf&uuml;gbare" Lagerstand
        /// </summary>
        public long stockAmount { get; set; }

        public XmlNs0StockAmountInfoEntry stockAmountInfo { get; set; }

        public XmlNs0StockInfoEntryList stockplaceInfoEntries { get; set; }

        /// <summary>
        /// Die Artikelart
        /// </summary>
        public string typeCnr { get; set; }

        /// <summary>
        /// Die Einheit des Artikels
        /// </summary>
        public string unitCnr { get; set; }
    }
}