using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0StockEntry : XmlNs0BaseEntryId 
    {
        public string name { get; set; }

        public string typeCnr { get; set; }
    }
}