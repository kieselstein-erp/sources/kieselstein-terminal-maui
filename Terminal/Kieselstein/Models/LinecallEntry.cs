using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class LinecallEntry : BaseEntryId 
    {
        public double quantity { get; set; }

        public string line { get; set; }

        public string sector { get; set; }

        public string sectorDescription { get; set; }

        public long productionDateMs { get; set; }

        public string ordernumber { get; set; }

        public List<LinecallItemEntry> linecallItemEntries { get; set; }
    }
}