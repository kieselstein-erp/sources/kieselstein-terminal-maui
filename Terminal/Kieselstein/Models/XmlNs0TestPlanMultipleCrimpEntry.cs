using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TestPlanMultipleCrimpEntry : XmlNs0TestPlanCrimpEntry 
    {
        public string itemCnrSecondStrand { get; set; }

        public string itemDescriptionSecondStrand { get; set; }

        public long strippingForceSecondStrand { get; set; }
    }
}