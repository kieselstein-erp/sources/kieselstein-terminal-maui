using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0DocumentInfoEntryList
    {
        public List<XmlNs0DocumentInfoEntry> entries { get; set; }
    }
}