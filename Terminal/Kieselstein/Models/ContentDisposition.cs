using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ContentDisposition
    {
        public Dictionary<string, string> parameters { get; set; }

        public string type { get; set; }
    }
}