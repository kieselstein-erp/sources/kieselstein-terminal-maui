namespace Kieselstein.Models
{
    public static class XmlNs0DeliverableOrderEntrystatusConstants
    {
        public const string NOTINITIALIZED = "NOTINITIALIZED";

        public const string NEW = "NEW";

        public const string OPEN = "OPEN";

        public const string PARTIALLYDONE = "PARTIALLYDONE";

        public const string DONE = "DONE";

        public const string CANCELLED = "CANCELLED";
    }
}
