using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ProductionTargetMaterialEntryList
    {
        public List<ProductionTargetMaterialEntry> entries { get; set; }
    }
}