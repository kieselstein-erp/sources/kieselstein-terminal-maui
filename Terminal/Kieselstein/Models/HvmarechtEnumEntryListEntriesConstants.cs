namespace Kieselstein.Models
{
    public static class HvmarechtEnumEntryListentriesConstants
    {
        public const string Artikel = "Artikel";

        public const string Fertigung = "Fertigung";

        public const string Wareneingang = "Wareneingang";

        public const string Lieferung = "Lieferung";

        public const string Zeiterfassung = "Zeiterfassung";

        public const string Materialbedarf = "Materialbedarf";

        public const string DokumenteErfassen = "DokumenteErfassen";

        public const string Kommissionierliste = "Kommissionierliste";

        public const string Packliste = "Packliste";

        public const string DarfAuftragsKopfZeitBuchen = "DarfAuftragsKopfZeitBuchen";
    }
}
