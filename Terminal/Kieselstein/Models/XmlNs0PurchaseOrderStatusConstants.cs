namespace Kieselstein.Models
{
    public static class XmlNs0PurchaseOrderStatusConstants
    {
        public const string NOTINITIALIZED = "NOTINITIALIZED";

        public const string NEW = "NEW";

        public const string OPEN = "OPEN";

        public const string CONFIRMED = "CONFIRMED";

        public const string DELIVERED = "DELIVERED";

        public const string DONE = "DONE";

        public const string CANCELLED = "CANCELLED";

        public const string CALLOFFDONE = "CALLOFFDONE";

        public const string PARTLYDONE = "PARTLYDONE";
    }
}
