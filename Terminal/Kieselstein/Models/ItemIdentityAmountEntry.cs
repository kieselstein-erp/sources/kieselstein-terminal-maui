﻿using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Kieselstein.Models
{
  public class ItemIdentityAmountEntry : IdentityAmountEntry
  {
    public int itemId { get; set; }
  }
}
