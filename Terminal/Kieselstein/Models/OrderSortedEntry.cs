using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class OrderSortedEntry : BaseEntryId 
    {
        /// <summary>
        /// ist true wenn die Sortierung nach Artikelnummerdurchgef&uuml;hrt wurde.
        /// </summary>
        public bool sorted { get; set; }
    }
}