using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestPlanOpticalTestEntry : TestPlanEntry 
    {
        public string itemCnr { get; set; }

        public string itemDescription { get; set; }
    }
}