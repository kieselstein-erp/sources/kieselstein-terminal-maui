using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class StockAmountEntry
    {
        /// <summary>
        /// Die gesamte Menge des Artikels auf diesem Lager
        /// </summary>
        public decimal amount { get; set; }

        public StockEntry stock { get; set; }

        public ItemEntry item { get; set; }

        /// <summary>
        /// Die Liste der Chargen- bzw. Seriennummern
        /// </summary>
        public ItemIdentityEntryList itemIdentityList { get; set; }

        public StockPlaceEntryList stockplaceList { get; set; }
    }
}