using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class StockAmountInfoEntry
    {
        public decimal stockAmount { get; set; }

        public decimal reservedAmount { get; set; }

        public decimal missingAmount { get; set; }

        public decimal availableAmount { get; set; }
    }
}