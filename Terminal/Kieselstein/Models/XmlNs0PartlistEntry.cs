using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0PartlistEntry : XmlNs0BaseEntryId 
    {
        public string additionalDescription { get; set; }

        public string cnr { get; set; }

        public string customerItemCnr { get; set; }

        public string description { get; set; }

        public int lotCount { get; set; }

        public string statusCnr { get; set; }
    }
}