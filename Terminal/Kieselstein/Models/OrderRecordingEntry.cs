using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Die Zeitdaten einer T&auml;tigkeit
    /// </summary>
    public class OrderRecordingEntry : DocumentRecordingEntry 
    {
        /// <summary>
        /// Die Auftrags-Id muss gesetzt sein.
        /// </summary>
        public int orderId { get; set; }

        /// <summary>
        /// Die Positions-Id einer Auftragsposition muss gesetzt sein
        /// </summary>
        public int orderPositionId { get; set; }
    }
}