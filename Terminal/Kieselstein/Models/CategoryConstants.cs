namespace Kieselstein.Models
{
    public static class CategoryConstants
    {
        public const string DELIVERYNOTE = "DELIVERYNOTE";

        public const string ITEM = "ITEM";

        public const string ORDER = "ORDER";

        public const string PRODUCTION = "PRODUCTION";

        public const string PROJECT = "PROJECT";

        public const string PURCHASEINVOICE = "PURCHASEINVOICE";
    }
}
