using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CreateOrderTextblockPositionEntry
    {
        /// <summary>
        /// Die Id des Textblocks
        /// </summary>
        public int textblockId { get; set; }

        public string textblockCnr { get; set; }

        public string localeCnr { get; set; }
    }
}