using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0PackagingInfoEntryList
    {
        /// <summary>
        /// Die Liste aller packagingInfoEntry Eintr&auml;ge
        /// </summary>
        public List<XmlNs0PackagingInfoEntry> entries { get; set; }
    }
}