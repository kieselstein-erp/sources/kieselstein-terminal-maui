using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ProducerInfoEntry
    {
        /// <summary>
        /// Die Hersteller-Id
        /// </summary>
        public int producerId { get; set; }

        /// <summary>
        /// Die Hersteller-Artikelnummer
        /// </summary>
        public string itemCnr { get; set; }

        /// <summary>
        /// Die Bezeichnung des Artikels auf Herstellerseite
        /// </summary>
        public string itemDescription { get; set; }

        /// <summary>
        /// Der Barcode-Leadin des Herstellers
        /// </summary>
        public string barcodeLeadIn { get; set; }

        /// <summary>
        /// Gibt es eine Hersteller-InfoMessage?
        /// </summary>
        public bool hasInfo { get; set; }
    }
}