using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestPlanCrimpIsoEntry : TestPlanCrimpEntry 
    {
        public double crimpHeightIsolation { get; set; }

        public double crimpWidthIsolation { get; set; }

        public double crimpHeightIsolationTolerance { get; set; }

        public double crimpWidthIsolationTolerance { get; set; }
    }
}