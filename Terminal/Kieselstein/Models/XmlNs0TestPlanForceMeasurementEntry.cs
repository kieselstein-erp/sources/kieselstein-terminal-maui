using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TestPlanForceMeasurementEntry : XmlNs0TestPlanEntry 
    {
        public string itemCnrContact { get; set; }

        public string itemCnrStrand { get; set; }

        public string itemDescriptionContact { get; set; }

        public string itemDescriptionStrand { get; set; }

        public long minimumValue { get; set; }
    }
}