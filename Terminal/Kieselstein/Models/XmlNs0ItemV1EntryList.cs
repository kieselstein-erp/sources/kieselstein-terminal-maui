using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ItemV1EntryList
    {
        public List<XmlNs0ItemV1Entry> entries { get; set; }
    }
}