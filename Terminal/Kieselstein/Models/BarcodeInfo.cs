﻿namespace Kieselstein.Models
{
  public class BarcodeInfo
  {
    public ProductionEntry productionEntry { get; set; }
    public ProductionWorkstepEntry productionWorkstepEntry { get; set; }
    public int machineId { get; set; }
    public string WorkstepNumberFormatted => productionWorkstepEntry.subWorkstepNumber == null
        ? productionWorkstepEntry.workstepNumber.ToString("D") : productionWorkstepEntry.workstepNumber.ToString("D") + "." + ((int)productionWorkstepEntry.subWorkstepNumber).ToString("D");
  }
}
