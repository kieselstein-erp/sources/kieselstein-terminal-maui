using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0MachineEntry : XmlNs0BaseEntryId 
    {
        public string description { get; set; }

        public string identificationNumber { get; set; }

        public string inventoryNumber { get; set; }

        public string machineGroupDescription { get; set; }

        public int machineGroupISort { get; set; }

        public int machineGroupId { get; set; }

        public string machineGroupShortDescription { get; set; }

        public int personalIdStarter { get; set; }

        public int productionWorkplanId { get; set; }

        public long starttime { get; set; }
    }
}