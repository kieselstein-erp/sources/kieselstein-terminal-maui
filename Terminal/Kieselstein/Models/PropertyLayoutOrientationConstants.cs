namespace Kieselstein.Models
{
    public static class PropertyLayoutOrientationConstants
    {
        public const string NORTHWEST = "NORTHWEST";

        public const string NORTH = "NORTH";

        public const string NORTHEAST = "NORTHEAST";

        public const string WEST = "WEST";

        public const string CENTER = "CENTER";

        public const string EAST = "EAST";

        public const string SOUTHWEST = "SOUTHWEST";

        public const string SOUTH = "SOUTH";

        public const string SOUTHEAST = "SOUTHEAST";
    }
}
