using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TenantEntryList
    {
        /// <summary>
        /// Eine Liste aller Mandanten
        /// </summary>
        public List<TenantEntry> entries { get; set; }
    }
}