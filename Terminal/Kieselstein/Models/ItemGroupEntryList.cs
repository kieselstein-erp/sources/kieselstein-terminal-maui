using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ItemGroupEntryList
    {
        /// <summary>
        /// Die Liste aller ItemGroupEntry Eintr&auml;ge
        /// </summary>
        public List<ItemGroupEntry> entries { get; set; }
    }
}