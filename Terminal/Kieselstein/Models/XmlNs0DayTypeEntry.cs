using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0DayTypeEntry : XmlNs0BaseEntryId 
    {
        public string description { get; set; }
    }
}