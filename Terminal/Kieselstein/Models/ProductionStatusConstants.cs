namespace Kieselstein.Models
{
    public static class ProductionStatusConstants
    {
        public const string NOTINITIALIZED = "NOTINITIALIZED";

        public const string NEW = "NEW";

        public const string EDITED = "EDITED";

        public const string DONE = "DONE";

        public const string INPRODUCTION = "INPRODUCTION";

        public const string CANCELLED = "CANCELLED";

        public const string PARTIALLYDONE = "PARTIALLYDONE";

        public const string STOPPED = "STOPPED";
    }
}
