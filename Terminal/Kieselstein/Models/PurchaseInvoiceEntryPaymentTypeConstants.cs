namespace Kieselstein.Models
{
    public static class PurchaseInvoiceEntrypaymentTypeConstants
    {
        public const string NOPAYMENT = "NOPAYMENT";

        public const string ME = "ME";

        public const string CARD = "CARD";

        public const string OTHER = "OTHER";
    }
}
