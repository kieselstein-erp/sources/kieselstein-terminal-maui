using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0StockPlaceEntry : XmlNs0BaseEntryId 
    {
        public XmlNs0ItemV1EntryList items { get; set; }

        public string name { get; set; }
    }
}