using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0StockInfoEntry
    {
        public XmlNs0StockEntry stockEntry { get; set; }

        public XmlNs0StockPlaceEntryList stockplaceEntries { get; set; }
    }
}