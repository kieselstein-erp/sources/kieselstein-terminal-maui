using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ProductionEntry : BaseEntryId 
    {
        public string cnr { get; set; }

        public decimal amount { get; set; }

        public string orderOrItemCnr { get; set; }

        public long startDateMs { get; set; }

        public long endDateMs { get; set; }

        public string project { get; set; }

        public string comment { get; set; }

        public ItemV1Entry itemEntry { get; set; }

        public CustomerEntry customer { get; set; }

        public ProductionAdditionalStatusEntryList additionalStatuses { get; set; }

        public string status { get; set; }

        public int partlistId { get; set; }

        public PartlistWorkstepEntryList worksteps { get; set; }

        public ProductionTargetMaterialEntryList targetMaterials { get; set; }

        public decimal deliveredAmount { get; set; }

        public ProductionWorkstepEntryList productionWorksteps { get; set; }

        public int targetStockId { get; set; }

        public string customerName { get; set; }

        public string itemDescription { get; set; }

        public string itemDescription2 { get; set; }

        public DocumentInfoEntryList documentInfoEntries { get; set; }

        public ItemCommentMediaInfoEntryList itemCommentMediaInfoEntries { get; set; }

        public string itemCnr { get; set; }

        public string orderCnr { get; set; }

        public string manufactoringPlace { get; set; }

        public TestPlanEntryList testPlanEntries { get; set; }
    }
}