using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CurrencyEntryList
    {
        /// <summary>
        /// Die Liste aller W&auml;hrungen
        /// </summary>
        public List<CurrencyEntry> entries { get; set; }

        public long rowCount { get; set; }
    }
}