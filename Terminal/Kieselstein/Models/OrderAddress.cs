using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class OrderAddress : BaseEntryId 
    {
        public int partnerId { get; set; }

        /// <summary>
        /// Formatierte Ausgabe der Adresse
        /// </summary>
        public List<string> lines { get; set; }

        /// <summary>
        /// Die Anrede (Frau, Herr, Firma, ...)
        /// </summary>
        public string salutation { get; set; }

        /// <summary>
        /// Der vorangestellte Titel
        /// </summary>
        public string titel { get; set; }

        /// <summary>
        /// Der nachgestellte Titel
        /// </summary>
        public string titelSuffix { get; set; }

        public string name1Lastname { get; set; }

        public string name2Firstname { get; set; }

        public string name3Department { get; set; }

        public string postofficeBox { get; set; }

        public string street { get; set; }

        public string countryCode { get; set; }

        public string zipcode { get; set; }

        public string city { get; set; }

        public string email { get; set; }

        public string phone { get; set; }
    }
}