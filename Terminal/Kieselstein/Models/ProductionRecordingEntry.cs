using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Die Zeitdaten einer T&auml;tigkeit
    /// </summary>
    public class ProductionRecordingEntry : DocumentRecordingEntry 
    {
        public int productionId { get; set; }

        public int machineId { get; set; }

        public int productionWorkplanId { get; set; }
    }
}