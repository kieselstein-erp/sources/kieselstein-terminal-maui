using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class MachineAvailabilityEntry
    {
        public int machineId { get; set; }

        public int dayTypeId { get; set; }

        public double availabilityHours { get; set; }

        public string dayTypeDescription { get; set; }

        public long dateMs { get; set; }
    }
}