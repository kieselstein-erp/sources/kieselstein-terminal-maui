using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0StockInfoItemEntryList
    {
        public List<XmlNs0StockInfoItemEntry> entries { get; set; }
    }
}