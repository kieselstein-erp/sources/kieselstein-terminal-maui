using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CustomerPricelistItemDto
    {
        public CustomerPricelistItemDescriptionDto clientDescriptionDto { get; set; }

        public string itemMaterial { get; set; }

        public bool specialCondition { get; set; }

        public string name { get; set; }

        public IdValueDto item { get; set; }

        public double itemMaterialgewicht { get; set; }

        public string itemGroup { get; set; }

        public CustomerPricelistShopgroupDto shopgroupDto { get; set; }

        public IdValueDto itemGroupDto { get; set; }

        public string kundeartikelbezeichnung { get; set; }

        public CustomerPricelistItemDescriptionDto descriptionDto { get; set; }

        public string additionalName2 { get; set; }

        public string kundeartikelnummer { get; set; }

        public double itemKursMaterialzuschlag { get; set; }

        public bool hidden { get; set; }

        public List<CustomerPricelistPriceDto> prices { get; set; }

        public int VATId { get; set; }

        public string itemClass { get; set; }

        public string additionalName { get; set; }

        public string shortName { get; set; }

        public string unit { get; set; }
    }
}