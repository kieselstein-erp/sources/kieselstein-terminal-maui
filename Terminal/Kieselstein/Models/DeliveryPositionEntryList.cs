using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DeliveryPositionEntryList
    {
        public List<DeliveryPositionEntry> entries { get; set; }
    }
}