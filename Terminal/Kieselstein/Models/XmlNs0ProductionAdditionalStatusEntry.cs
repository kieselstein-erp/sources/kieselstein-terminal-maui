using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ProductionAdditionalStatusEntry
    {
        public long dateMs { get; set; }

        public string status { get; set; }
    }
}