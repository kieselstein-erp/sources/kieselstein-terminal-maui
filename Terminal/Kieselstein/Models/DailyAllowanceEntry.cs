using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DailyAllowanceEntry : BaseEntryId 
    {
        public string cnr { get; set; }

        public string description { get; set; }
    }
}