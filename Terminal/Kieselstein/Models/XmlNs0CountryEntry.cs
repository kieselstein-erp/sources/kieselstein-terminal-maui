using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0CountryEntry : XmlNs0BaseEntryId 
    {
        public string cnr { get; set; }

        public string currency { get; set; }

        public string name { get; set; }
    }
}