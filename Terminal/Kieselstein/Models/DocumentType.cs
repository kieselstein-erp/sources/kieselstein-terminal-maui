using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DocumentType
    {
        /// <summary>
        /// Die id
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// Der BelegnameBeispielsweise "Angebot", "Auftrag", "Projekt", "Los"
        /// </summary>
        public string documentName { get; set; }
    }
}