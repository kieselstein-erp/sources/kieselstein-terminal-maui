using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Die Zeitdaten einer T&auml;tigkeit
    /// </summary>
    public class XmlNs0DocumentRecordingEntry : XmlNs0TimeRecordingEntry 
    {
        /// <summary>
        /// Die erweiterte Bemerkung
        /// </summary>
        public string extendedRemark { get; set; }

        /// <summary>
        /// Die Bemerkung
        /// </summary>
        public string remark { get; set; }

        /// <summary>
        /// Die Id der T&auml;tigkeit bzw. des Arbeitszeitartikels
        /// </summary>
        public int workItemId { get; set; }
    }
}