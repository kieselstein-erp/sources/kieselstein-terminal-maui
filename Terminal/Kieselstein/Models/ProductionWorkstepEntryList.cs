using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ProductionWorkstepEntryList
    {
        public List<ProductionWorkstepEntry> entries { get; set; }
    }
}