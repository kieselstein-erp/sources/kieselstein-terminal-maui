using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DoneProductionEntry : BaseEntryId 
    {
        /// <summary>
        /// Ist das Los als erledigt markiert worden
        /// </summary>
        public bool done { get; set; }

        /// <summary>
        /// Wurde zu wenig abgeliefert?
        /// </summary>
        public bool missingAmount { get; set; }

        /// <summary>
        /// Die Losgr&ouml;&szlig;e
        /// </summary>
        public double amount { get; set; }

        /// <summary>
        /// Die abgelieferte Menge
        /// </summary>
        public double deliveredAmount { get; set; }

        /// <summary>
        /// Die (optionale) Fehlermeldung die beim Benachrichtigen&uuml;ber die &Auml;nderung des Losstatus beim Fremdsystementstanden ist
        /// </summary>
        public string postMessage { get; set; }

        public string nowProcessedBy { get; set; }
    }
}