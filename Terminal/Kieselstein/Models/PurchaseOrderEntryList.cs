using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PurchaseOrderEntryList
    {
        /// <summary>
        /// Die Liste aller PurchaseOrderEntry Eintr&auml;ge
        /// </summary>
        public List<PurchaseOrderEntry> entries { get; set; }

        /// <summary>
        /// Die Anzahl der Eintr&auml;ge
        /// </summary>
        public long rowCount { get; set; }
    }
}