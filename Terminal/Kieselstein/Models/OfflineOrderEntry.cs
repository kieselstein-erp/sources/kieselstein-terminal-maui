using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class OfflineOrderEntry
    {
        /// <summary>
        /// Die Liste aller Auftr&auml;ge entsprechend den Suchkriterien
        /// </summary>
        public List<OrderEntry> orders { get; set; }

        /// <summary>
        /// Die Liste aller Positionen f&uuml;r die gelieferten Auftr&auml;ge
        /// </summary>
        public List<OrderpositionsEntry> orderpositions { get; set; }

        /// <summary>
        /// Die Liste aller eindeutigen Adressen
        /// </summary>
        public List<OrderAddressContact> addresses { get; set; }
    }
}