﻿namespace KieselsteinErp.Terminal.Kieselstein.Models
{
  public class GoodBadEntry
  {
    public GoodBadEntry(int id, decimal goodAmount, decimal badAmount, bool finished)
    {
      this.workstepId = id;
      this.goodAmount = goodAmount;
      this.badAmount = badAmount;
      this.finished = finished;
    }

    public int workstepId {  get; set; }
    public decimal goodAmount { get; set; }
    public decimal badAmount { get; set; }
    public bool finished { get; set; }
  }
}