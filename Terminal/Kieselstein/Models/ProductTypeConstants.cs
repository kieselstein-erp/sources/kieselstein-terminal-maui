namespace Kieselstein.Models
{
    public static class ProductTypeConstants
    {
        public const string PICKING = "PICKING";

        public const string STOCK = "STOCK";
    }
}
