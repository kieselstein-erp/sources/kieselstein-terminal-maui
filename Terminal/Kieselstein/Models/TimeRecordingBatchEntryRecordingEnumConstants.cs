namespace Kieselstein.Models
{
    public static class TimeRecordingBatchEntryrecordingEnumConstants
    {
        public const string Start = "Start";

        public const string Stop = "Stop";

        public const string Pause = "Pause";

        public const string Coming = "Coming";

        public const string Leaving = "Leaving";

        public const string Text = "Text";

        public const string Media = "Media";
    }
}
