using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0CreateItemIdentityPropertyEntry : XmlNs0ItemPropertyEntry 
    {
        public string identity { get; set; }
    }
}