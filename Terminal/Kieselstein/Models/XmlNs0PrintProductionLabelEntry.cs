using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0PrintProductionLabelEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Wurde das Losetikett gedruckt?
        /// </summary>
        public bool printLabel { get; set; }
    }
}