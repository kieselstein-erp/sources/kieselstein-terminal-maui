using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ItemPropertyEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Der Inhalt. Kann auch "null" sein, wenn die Eigenschaft keinen Wert gesetzt hat.
        /// </summary>
        public string content { get; set; }

        /// <summary>
        /// Der Datentyp in der Form "java.lang.Integer", "java.lang.String", ...
        /// </summary>
        public string datatype { get; set; }

        /// <summary>
        /// Die ArtikelgruppenId
        /// </summary>
        public int itemgroupId { get; set; }

        /// <summary>
        /// Id der Beschreibung dieser Eigenschaft
        /// </summary>
        public int layoutId { get; set; }

        /// <summary>
        /// Ist diese Eigenschaft zwingend erforderlich
        /// </summary>
        public bool mandatory { get; set; }

        /// <summary>
        /// Der Name/Bezeichnung der Eigenschaft
        /// </summary>
        public string name { get; set; }
    }
}