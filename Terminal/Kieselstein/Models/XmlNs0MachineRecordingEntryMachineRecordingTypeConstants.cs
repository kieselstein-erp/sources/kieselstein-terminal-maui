namespace Kieselstein.Models
{
    public static class XmlNs0MachineRecordingEntrymachineRecordingTypeConstants
    {
        public const string START = "START";

        public const string STOP = "STOP";
    }
}
