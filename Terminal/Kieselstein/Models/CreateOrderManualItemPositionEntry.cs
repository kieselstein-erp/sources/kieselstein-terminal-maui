using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Die Datenstruktur zum Anlegen einer Handeingabe-Position
    /// </summary>
    public class CreateOrderManualItemPositionEntry
    {
        /// <summary>
        /// Die Bezeichnung des Handartikels in der Position
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// Die Zusatzbezeichnung des Handartikels
        /// </summary>
        public string description2 { get; set; }

        /// <summary>
        /// Die Einheit in der Artikel geliefert wird
        /// </summary>
        public string unitCnr { get; set; }

        /// <summary>
        /// Die zu lieferende Menge
        /// </summary>
        public double amount { get; set; }

        /// <summary>
        /// Der Nettoeinzelpreis einer Mengeneinheit (Beispiel: "St&uuml;ck")
        /// </summary>
        public double nettoPrice { get; set; }

        /// <summary>
        /// Die Id des Kostentr&auml;gersDie Id des Kostentr&auml;gers ist dann und nur dann anzugeben,wenn die Zusatzfunktion "KOSTENTR&Auml;GER" f&uuml;r denjeweiligen Mandanten freigeschaltet ist. 
        /// </summary>
        public int costBearingUnitId { get; set; }

        /// <summary>
        /// Die Id der MehrwertsteuersatzbezeichnungDie Satzbezeichnung (Beispiel: Allgemeine Waren,Lebensmittel und B&uuml;cher, reduzierte Steuer, ...)wird im Zusammenhang mit dem Belegdatum des Auftragsverwendet, um den korrekten Steuersatz zu ermittelnDie SystemApi stellt die verf&uuml;gbaren Satzbezeichnungenzur Verf&uuml;gungDiese Property muss gesetzt werden
        /// </summary>
        public int taxDescriptionId { get; set; }
    }
}