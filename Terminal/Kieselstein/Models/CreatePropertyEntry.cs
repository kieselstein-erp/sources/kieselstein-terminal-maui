using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CreatePropertyEntry
    {
        /// <summary>
        /// Die Id der zugeh&ouml;rigen Beschreibung @see PropertyLayoutEntry zu dem dieneue Eigenschaft erzeugt werden soll
        /// </summary>
        public int layoutId { get; set; }

        /// <summary>
        /// Inhalt der zu erzeugenden Eigenschaft.
        /// </summary>
        public string content { get; set; }
    }
}