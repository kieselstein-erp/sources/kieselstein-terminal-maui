using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestPlanCrimpEntry : TestPlanEntry 
    {
        public string itemCnrContact { get; set; }

        public string itemDescriptionContact { get; set; }

        public string itemCnrStrand { get; set; }

        public string itemDescriptionStrand { get; set; }

        public double crimpHeightWire { get; set; }

        public double crimpWidthWire { get; set; }

        public double strippingForceStrand { get; set; }

        public double crimpHeightWireTolerance { get; set; }

        public double crimpWidthWireTolerance { get; set; }

        public bool doublestrike { get; set; }
    }
}