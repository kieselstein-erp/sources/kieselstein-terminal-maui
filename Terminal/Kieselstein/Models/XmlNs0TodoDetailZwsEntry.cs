using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TodoDetailZwsEntry : XmlNs0TodoDetailEntry 
    {
        public string text { get; set; }
    }
}