using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CreateItemIdentityPropertyEntryList : ItemPropertyEntryList 
    {
        public string identity { get; set; }
    }
}