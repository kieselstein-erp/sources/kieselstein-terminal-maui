using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ForecastDeliveryAddressEntryList
    {
        public List<ForecastDeliveryAddressEntry> entries { get; set; }
    }
}