using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestResultEntry
    {
        public int productionTestPlanId { get; set; }

        public string testType { get; set; }
    }
}