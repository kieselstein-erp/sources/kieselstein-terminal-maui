using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0DailyAllowanceEntry : XmlNs0BaseEntryId 
    {
        public string cnr { get; set; }

        public string description { get; set; }
    }
}