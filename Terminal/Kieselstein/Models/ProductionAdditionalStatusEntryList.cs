using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ProductionAdditionalStatusEntryList
    {
        public List<ProductionAdditionalStatusEntry> entries { get; set; }
    }
}