using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Repr&auml;sentiert die Antwort eines erfolgreichen Logons
    /// </summary>
    public class LoggedOnEntry
    {
        /// <summary>
        /// Der vom HELIUM V System ermittelte "token". Sp&auml;ter auch als "userId"referenziert.
        /// </summary>
        public string token { get; set; }

        /// <summary>
        /// Der Mandant in dem sich der Benutzer angemeldet hat, bzw. vom Systemangemeldet wurde.
        /// </summary>
        public string client { get; set; }

        /// <summary>
        /// Die Locale ("deAT", "deDE", ...) die der Anmeldung zugewiesen worden ist
        /// </summary>
        public string localeString { get; set; }
    }
}