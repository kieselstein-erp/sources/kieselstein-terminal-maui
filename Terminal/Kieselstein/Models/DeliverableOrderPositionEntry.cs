using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DeliverableOrderPositionEntry : BaseEntryId 
    {
        /// <summary>
        /// Die Artikelnummer
        /// </summary>
        public string itemCnr { get; set; }

        /// <summary>
        /// Die Id des Artikels
        /// </summary>
        public int itemId { get; set; }

        /// <summary>
        /// Die Artikelbezeichnung
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// Auftragsmenge
        /// </summary>
        public decimal amount { get; set; }

        /// <summary>
        /// Die noch offene Menge des Artikels im Auftrag
        /// </summary>
        public decimal openAmount { get; set; }

        /// <summary>
        /// Die Einheit des Artikels im Auftrag
        /// </summary>
        public string unitCnr { get; set; }

        /// <summary>
        /// Der Positionsstatus
        /// </summary>
        public string status { get; set; }

        public string itemProperty { get; set; }

        /// <summary>
        /// Die Liste der Chargen- bzw. Seriennummern
        /// </summary>
        public ItemIdentityEntryList itemIdentity { get; set; }

        public StockInfoEntry stockinfoEntry { get; set; }
    }
}