namespace Kieselstein.Models
{
    public static class SelectoptionConstants
    {
        public const string ALL = "ALL";

        public const string EMPLOYER = "EMPLOYER";

        public const string MYDEPARTMENT = "MY_DEPARTMENT";

        public const string THISPERSON = "THIS_PERSON";

        public const string WORKER = "WORKER";
    }
}
