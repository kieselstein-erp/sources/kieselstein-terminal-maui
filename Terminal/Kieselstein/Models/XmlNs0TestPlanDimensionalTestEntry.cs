using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TestPlanDimensionalTestEntry : XmlNs0TestPlanEntry 
    {
        public string itemCnrStrand { get; set; }

        public string itemDescriptionStrand { get; set; }

        public long tolerance { get; set; }

        public long value { get; set; }
    }
}