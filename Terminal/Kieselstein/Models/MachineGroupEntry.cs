using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class MachineGroupEntry : BaseEntryId 
    {
        public string description { get; set; }

        public bool showPlanningView { get; set; }

        public string productionGroupDescription { get; set; }
    }
}