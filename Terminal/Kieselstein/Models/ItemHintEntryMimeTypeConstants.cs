namespace Kieselstein.Models
{
    public static class ItemHintEntrymimeTypeConstants
    {
        public const string TEXTHTML = "TEXTHTML";

        public const string IMAGEJPEG = "IMAGEJPEG";

        public const string IMAGEPNG = "IMAGEPNG";

        public const string APPPDF = "APPPDF";
    }
}
