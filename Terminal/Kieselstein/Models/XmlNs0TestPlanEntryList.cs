using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TestPlanEntryList
    {
        public List<XmlNs0TestPlanEntry> entries { get; set; }
    }
}