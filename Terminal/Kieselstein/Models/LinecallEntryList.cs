using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class LinecallEntryList
    {
        public List<LinecallEntry> entries { get; set; }
    }
}