using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ItemCommentMediaInfoEntryList
    {
        public List<ItemCommentMediaInfoEntry> entries { get; set; }
    }
}