using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestResultForceMeasurementEntry : TestResultEntry 
    {
        public double value { get; set; }
    }
}