using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ContactEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Der AdresstypBeispielsweise Lieferadresse oder Filialadresse
        /// </summary>
        public string addressType { get; set; }

        /// <summary>
        /// Die Stadt
        /// </summary>
        public string city { get; set; }

        /// <summary>
        /// Emailadresse des Ansprechpartners im Unternehmen
        /// </summary>
        public string companyEmail { get; set; }

        /// <summary>
        /// Telefonnummer des Ansprechpartners im Unternehmen
        /// </summary>
        public string companyPhone { get; set; }

        public XmlNs0ContactEntryList contactEntries { get; set; }

        /// <summary>
        /// Der zweistellige L&auml;ndercode (zum Beispiel: AT)
        /// </summary>
        public string countryCode { get; set; }

        /// <summary>
        /// Der Name des Landes
        /// </summary>
        public string countryName { get; set; }

        /// <summary>
        /// Abteilung
        /// </summary>
        public string department { get; set; }

        /// <summary>
        /// Die E-Mail Adresse
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// Die EORI des Partners
        /// </summary>
        public string eori { get; set; }

        /// <summary>
        /// Die Faxnummer
        /// </summary>
        public string fax { get; set; }

        /// <summary>
        /// Die von HELIUM V formatierte OrtsbezeichnungBeispiel: "AT 5301 Eugendorf bei Salzburg"
        /// </summary>
        public string formattedCity { get; set; }

        /// <summary>
        /// Die von HELIUM V formatierte Anrede inkl. Titel
        /// </summary>
        public string formattedSalutation { get; set; }

        /// <summary>
        /// Sortierung
        /// </summary>
        public int iSort { get; set; }

        /// <summary>
        /// Mobiltelefonnummer
        /// </summary>
        public string mobilePhone { get; set; }

        /// <summary>
        /// Die erste Adresszeile
        /// </summary>
        public string name1 { get; set; }

        /// <summary>
        /// Die zweite Adresszeile
        /// </summary>
        public string name2 { get; set; }

        /// <summary>
        /// Die dritte Adresszeile (Abteilung)
        /// </summary>
        public string name3 { get; set; }

        /// <summary>
        /// Die Id des Partners
        /// </summary>
        public int partnerId { get; set; }

        /// <summary>
        /// Die Telefonnummer
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// Die Bemerkung zum Partner
        /// </summary>
        public string remark { get; set; }

        /// <summary>
        /// Ansprechpartnerfunktion
        /// </summary>
        public string role { get; set; }

        /// <summary>
        /// Die Kurzbezeichnung
        /// </summary>
        public string sign { get; set; }

        /// <summary>
        /// Die Strasse
        /// </summary>
        public string street { get; set; }

        /// <summary>
        /// Der nachgestellte Teil des Titel
        /// </summary>
        public string titlePostfix { get; set; }

        /// <summary>
        /// Der TitelJener Teil der vorangestellt wird
        /// </summary>
        public string titlePrefix { get; set; }

        /// <summary>
        /// Die UID des Partners
        /// </summary>
        public string uid { get; set; }

        /// <summary>
        /// G&uuml;ltig-Ab Datum in Millisekunden
        /// </summary>
        public long validFromMs { get; set; }

        /// <summary>
        /// Die Homepage
        /// </summary>
        public string website { get; set; }

        /// <summary>
        /// Die Postleitzahl
        /// </summary>
        public string zipcode { get; set; }
    }
}