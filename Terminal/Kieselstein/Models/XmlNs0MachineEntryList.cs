using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0MachineEntryList
    {
        public List<XmlNs0MachineEntry> entries { get; set; }
    }
}