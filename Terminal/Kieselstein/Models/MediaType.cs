using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class MediaType
    {
        public bool wildcardType { get; set; }

        public string subtype { get; set; }

        public string type { get; set; }

        public bool wildcardSubtype { get; set; }

        public Dictionary<string, string> parameters { get; set; }
    }
}