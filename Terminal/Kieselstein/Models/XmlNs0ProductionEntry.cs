using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ProductionEntry : XmlNs0BaseEntryId 
    {
        public XmlNs0ProductionAdditionalStatusEntryList additionalStatuses { get; set; }

        public long amount { get; set; }

        public string cnr { get; set; }

        public string comment { get; set; }

        public XmlNs0CustomerEntry customer { get; set; }

        public string customerName { get; set; }

        public long deliveredAmount { get; set; }

        public XmlNs0DocumentInfoEntryList documentInfoEntries { get; set; }

        public long endDateMs { get; set; }

        public string itemCnr { get; set; }

        public XmlNs0ItemCommentMediaInfoEntryList itemCommentMediaInfoEntries { get; set; }

        public string itemDescription { get; set; }

        public string itemDescription2 { get; set; }

        public XmlNs0ItemV1Entry itemEntry { get; set; }

        public string manufactoringPlace { get; set; }

        public string orderCnr { get; set; }

        public string orderOrItemCnr { get; set; }

        public int partlistId { get; set; }

        public XmlNs0ProductionWorkstepEntryList productionWorksteps { get; set; }

        public string project { get; set; }

        public long startDateMs { get; set; }

        public string status { get; set; }

        public XmlNs0ProductionTargetMaterialEntryList targetMaterials { get; set; }

        public int targetStockId { get; set; }

        public XmlNs0TestPlanEntryList testPlanEntries { get; set; }

        public XmlNs0PartlistWorkstepEntryList worksteps { get; set; }
    }
}