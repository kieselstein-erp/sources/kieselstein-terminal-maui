using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class SynchEntry
    {
        public string userId { get; set; }

        /// <summary>
        /// Die Quelle der SynchronisationDies kann ein beliebiger Text sein, der aus Anwendersicht die Zuordnung zurSynchronisation erm&ouml;glicht.
        /// </summary>
        public string where { get; set; }
    }
}