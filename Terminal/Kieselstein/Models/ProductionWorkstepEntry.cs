namespace Kieselstein.Models
{
  public class ProductionWorkstepEntry : BaseEntryId
  {
    public int workstepNumber { get; set; }

    public long setupTimeMs { get; set; }

    public long jobTimeMs { get; set; }

    public string comment { get; set; }

    public ItemV1Entry itemEntry { get; set; }

    public int machineId { get; set; }

    public string text { get; set; }

    public string workstepType { get; set; }

    public bool hasFinished { get; set; }

    public double duration { get; set; }

    public int? subWorkstepNumber { get; set; }
  }
}