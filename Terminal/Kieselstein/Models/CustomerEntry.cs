using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CustomerEntry : BaseEntryId 
    {
        /// <summary>
        /// Die erste Adresszeile
        /// </summary>
        public string name1 { get; set; }

        /// <summary>
        /// Die zweite Adresszeile
        /// </summary>
        public string name2 { get; set; }

        /// <summary>
        /// Die Kurzbezeichnung
        /// </summary>
        public string sign { get; set; }

        /// <summary>
        /// Der zweistellige L&auml;ndercode (zum Beispiel: AT)
        /// </summary>
        public string countryCode { get; set; }

        /// <summary>
        /// Die Postleitzahl
        /// </summary>
        public string zipcode { get; set; }

        /// <summary>
        /// Die Telefonnummer
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// Die Stadt
        /// </summary>
        public string city { get; set; }

        /// <summary>
        /// Ist eine Lieferung an den Kunden erlaubt?
        /// </summary>
        public bool deliveryAllowed { get; set; }

        /// <summary>
        /// Die ABC Qualifizierung des Kunden
        /// </summary>
        public string classification { get; set; }

        /// <summary>
        /// Der AdresstypBeispielsweise Lieferadresse oder Filialadresse
        /// </summary>
        public string addressType { get; set; }

        /// <summary>
        /// Das Kurzzeichen des Vertreters (Provisionsempf&auml;ngers) im Haus f&uuml;r den Kunden
        /// </summary>
        public string representativeSign { get; set; }
    }
}