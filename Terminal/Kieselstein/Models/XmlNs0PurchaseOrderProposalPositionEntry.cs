using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0PurchaseOrderProposalPositionEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Bestellmenge der Position
        /// </summary>
        public long amount { get; set; }

        /// <summary>
        /// Der Liefertermin der Position
        /// </summary>
        public long deliveryDateMs { get; set; }

        /// <summary>
        /// Artikelnummer der Position
        /// </summary>
        public string itemCnr { get; set; }

        /// <summary>
        /// Wurde dieser Artikel vorgemerkt?
        /// </summary>
        public bool noted { get; set; }
    }
}