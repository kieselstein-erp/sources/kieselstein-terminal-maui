using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestPlanEntry : BaseEntryId 
    {
        public string testType { get; set; }

        public int productionId { get; set; }

        public int sortOrder { get; set; }

        public string description { get; set; }

        public string comment { get; set; }
    }
}