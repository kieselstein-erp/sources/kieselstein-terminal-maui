using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PackagingInfoEntry
    {
        /// <summary>
        /// Der (optionale) BarcodeDiese Property ist nicht gesetzt, wenn der Artikel in einerVerpackungsmenge vorliegt
        /// </summary>
        public string barcode { get; set; }

        /// <summary>
        /// Die Menge des Artikels innerhalb der VerpackungDie Kiste Mineralwasser hat den Barcode "ABC" undent&auml;lt 6 Flaschen.
        /// </summary>
        public double amount { get; set; }
    }
}