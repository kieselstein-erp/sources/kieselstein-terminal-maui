using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0LinecallItemEntry
    {
        public XmlNs0ItemV1Entry itemEntry { get; set; }

        public long openQuantity { get; set; }

        public long quantity { get; set; }
    }
}