using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class MachineEntry : BaseEntryId 
    {
        public string inventoryNumber { get; set; }

        public string description { get; set; }

        public string identificationNumber { get; set; }

        public int machineGroupId { get; set; }

        public string machineGroupDescription { get; set; }

        public int personalIdStarter { get; set; }

        public long starttime { get; set; }

        public int productionWorkplanId { get; set; }

        public string machineGroupShortDescription { get; set; }

        public int machineGroupISort { get; set; }
    }
}