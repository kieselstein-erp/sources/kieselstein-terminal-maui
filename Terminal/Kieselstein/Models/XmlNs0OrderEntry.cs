using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0OrderEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Die Auftragsnummer
        /// </summary>
        public string cnr { get; set; }

        /// <summary>
        /// Die Kundenadresse
        /// </summary>
        public string customerAddress { get; set; }

        public int customerContactId { get; set; }

        /// <summary>
        /// Die Id des Kunden (des Auftrags)
        /// </summary>
        public int customerId { get; set; }

        /// <summary>
        /// Die Id des Kunden der Lieferadresse
        /// </summary>
        public int customerIdDeliveryAddress { get; set; }

        /// <summary>
        /// Die Id des Kunden der Rechnungsadresse
        /// </summary>
        public int customerIdInvoiceAddress { get; set; }

        /// <summary>
        /// Der Kundenname
        /// </summary>
        public string customerName { get; set; }

        public int customerPartnerId { get; set; }

        public int deliveryContactId { get; set; }

        public long deliveryDateMs { get; set; }

        public int deliveryPartnerId { get; set; }

        public bool externalComment { get; set; }

        /// <summary>
        /// Der externe Kommentar zum AuftragDer externe Kommentar enth&auml;lt f&uuml;r den Kundenrelevante Information
        /// </summary>
        public string externalCommentText { get; set; }

        public bool internalComment { get; set; }

        /// <summary>
        /// Der interne Kommentar zum AuftragDer interne Kommentar ist &uuml;blicherweise f&uuml;r dieMitarbeiter gedacht
        /// </summary>
        public string internalCommentText { get; set; }

        /// <summary>
        /// Der Auftragsstatus (offen, angelegt, ...)
        /// </summary>
        public string orderState { get; set; }

        /// <summary>
        /// Der (technische) Auftrags-Typ (Freier Auftrag, Rahmenauftrag, Abrufauftrag, Wiederholend)
        /// </summary>
        public string orderType { get; set; }

        /// <summary>
        /// Die (optional) vorhandene "Projekt" oder "Bestell" Bezeichnung
        /// </summary>
        public string projectName { get; set; }

        /// <summary>
        /// Das Kurzzeichen des Vertreters im Haus f&uuml;r den Auftrag
        /// </summary>
        public string representativeSign { get; set; }
    }
}