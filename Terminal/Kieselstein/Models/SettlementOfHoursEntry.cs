using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class SettlementOfHoursEntry : BaseEntryId 
    {
        public string name { get; set; }

        public string mimeType { get; set; }

        public string content { get; set; }

        public string description { get; set; }

        public string lastPagePng { get; set; }

        public int serialNumber { get; set; }
    }
}