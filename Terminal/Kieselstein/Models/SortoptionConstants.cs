namespace Kieselstein.Models
{
    public static class SortoptionConstants
    {
        public const string COSTCENTER = "COSTCENTER";

        public const string DEPARTMENT = "DEPARTMENT";

        public const string DEPARTMENTCOSTCENTERNAME = "DEPARTMENT_COSTCENTER_NAME";

        public const string NAME = "NAME";

        public const string PERSONALCNR = "PERSONALCNR";
    }
}
