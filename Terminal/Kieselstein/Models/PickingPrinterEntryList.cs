using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PickingPrinterEntryList
    {
        public List<PickingPrinterEntry> entries { get; set; }
    }
}