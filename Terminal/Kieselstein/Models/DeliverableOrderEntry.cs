using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DeliverableOrderEntry : BaseEntryId 
    {
        /// <summary>
        /// Auftragsnummer
        /// </summary>
        public string cnr { get; set; }

        /// <summary>
        /// Ist das ein Auftrag der noch geliefert werden darf/kannDas ist dann der Fall, wenn er entweder OFFEN oder TEILGELIEFERT ist.
        /// </summary>
        public bool deliverable { get; set; }

        public string status { get; set; }

        public DeliveryOrderEntryList deliveryEntries { get; set; }

        public DeliverableOrderPositionEntryList positionEntries { get; set; }
    }
}