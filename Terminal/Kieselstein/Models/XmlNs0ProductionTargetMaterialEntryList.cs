using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ProductionTargetMaterialEntryList
    {
        public List<XmlNs0ProductionTargetMaterialEntry> entries { get; set; }
    }
}