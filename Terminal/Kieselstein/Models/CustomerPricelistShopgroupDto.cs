using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CustomerPricelistShopgroupDto
    {
        public int id { get; set; }

        public string name { get; set; }

        public string cnr { get; set; }
    }
}