using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TimeRecordingEntry
    {
        /// <summary>
        /// Der Tag im Bereich 1 - 31 f&uuml;r den die Buchung gelten soll.
        /// </summary>
        public int day { get; set; }

        /// <summary>
        /// Die (optionale) Personalnummer f&uuml;r die die Buchung giltWenn f&uuml;r eine andere Person gebucht werden soll, dann mu&szlig;entweder forStaffId oder forStaffCnr gesetzt werden
        /// </summary>
        public string forStaffCnr { get; set; }

        /// <summary>
        /// Die (optionale) PersonalId f&uuml;r die die Buchung gilt
        /// </summary>
        public int forStaffId { get; set; }

        /// <summary>
        /// Die Stunde im Bereich 0 - 23 f&uuml;r die die Buchung bestimmt ist.
        /// </summary>
        public int hour { get; set; }

        /// <summary>
        /// Die Minute im Bereich 0 - 59 f&uuml;r die die Buchung bestimmt ist.
        /// </summary>
        public int minute { get; set; }

        /// <summary>
        /// Der Monat im Bereich 1 - 12 f&uuml;r den die Buchung gelten soll
        /// </summary>
        public int month { get; set; }

        /// <summary>
        /// Die (optionale) Sekunde im Bereich 0 - 59 f&uuml;r die die Buchung bestimmt ist.
        /// </summary>
        public int second { get; set; }

        /// <summary>
        /// Die erforderliche BenutzerId desjenigen der am HELIUM V Server angemeldet ist.
        /// </summary>
        public string userId { get; set; }

        /// <summary>
        /// Die (optionale) Quelle der BuchungDies kann ein beliebiger Text sein, der aus Anwendersicht die Zuordnung zurBuchung erm&ouml;glicht.
        /// </summary>
        public string where { get; set; }

        /// <summary>
        /// Das Jahr f&uuml;r das die Buchung gelten soll
        /// </summary>
        public int year { get; set; }
    }
}