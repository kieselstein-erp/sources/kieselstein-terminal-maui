using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class OpenWorkUpdateEntryList
    {
        /// <summary>
        /// Die (leere) Liste aller offenen Arbeitsg&auml;nge
        /// </summary>
        public List<OpenWorkUpdateEntry> entries { get; set; }
    }
}