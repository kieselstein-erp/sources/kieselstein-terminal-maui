using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestResultCrimpingEntry : TestResultEntry 
    {
        public double crimpHeightWire { get; set; }

        public double crimpWidthWire { get; set; }

        public double strippingForceStrand { get; set; }

        public double strippingForceSecondStrand { get; set; }
    }
}