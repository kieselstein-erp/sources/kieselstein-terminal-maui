using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CountryEntry : BaseEntryId 
    {
        public string cnr { get; set; }

        public string name { get; set; }

        public string currency { get; set; }
    }
}