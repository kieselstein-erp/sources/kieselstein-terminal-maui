using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0DeliveryOrderEntryList
    {
        public List<XmlNs0DeliveryOrderEntry> entries { get; set; }
    }
}