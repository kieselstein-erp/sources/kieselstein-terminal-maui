using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0PropertyLayoutEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Spaltennummer im Layout
        /// </summary>
        public int column { get; set; }

        public string fill { get; set; }

        /// <summary>
        /// Abstand nach unten
        /// </summary>
        public int gapBottom { get; set; }

        /// <summary>
        /// Abstand nach links
        /// </summary>
        public int gapLeft { get; set; }

        /// <summary>
        /// Abstand nach rechts
        /// </summary>
        public int gapRight { get; set; }

        /// <summary>
        /// Abstand nach oben
        /// </summary>
        public int gapTop { get; set; }

        /// <summary>
        /// Ist dies Eigenschaft als &Uuml;berschrift definiert?
        /// </summary>
        public bool heading { get; set; }

        /// <summary>
        /// H&ouml;he der Komponente
        /// </summary>
        public int height { get; set; }

        /// <summary>
        /// Ist diese Eigenschaft zwingend erforderlich?
        /// </summary>
        public bool mandatory { get; set; }

        /// <summary>
        /// Eindeutiger Name
        /// </summary>
        public string name { get; set; }

        public string orientation { get; set; }

        /// <summary>
        /// Horizontales Padding der Komponente
        /// </summary>
        public int paddingX { get; set; }

        /// <summary>
        /// Vertikales Padding der Komponente
        /// </summary>
        public int paddingY { get; set; }

        /// <summary>
        /// Zeilennummer im Layout
        /// </summary>
        public int row { get; set; }

        /// <summary>
        /// Text der KomponenteIst type ein LABEL, CHECKBOX, PRINTBUTTONoder EXECUTEBUTTON dann wird gepr&uuml;ft, ob der Text (als Token) im Ressoure-Bundle&uuml;bersetzt werden kann. Sonst wird der Text geliefert.Der Text ist speziellerweise zu interpretieren bei:PropertyLayoutType#CHECKBOX: 0 .. die Checkbox ist gesetzt, oder 1 .. nicht gesetztPropertyLayoutType#COMBOBOX: Die Auswahlm&ouml;glichkeiten sind mit einer Pipe &#124; getrennt.
        /// </summary>
        public string text { get; set; }

        public string type { get; set; }

        /// <summary>
        /// Horizontale Gewichtung
        /// </summary>
        public long weightX { get; set; }

        /// <summary>
        /// Vertikale Gewichtung
        /// </summary>
        public long weightY { get; set; }

        /// <summary>
        /// Breite der Komponente
        /// </summary>
        public int width { get; set; }
    }
}