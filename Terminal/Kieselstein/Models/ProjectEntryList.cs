using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ProjectEntryList
    {
        /// <summary>
        /// Die (leere) Liste aller Projekte
        /// </summary>
        public List<ProjectEntry> entries { get; set; }
    }
}