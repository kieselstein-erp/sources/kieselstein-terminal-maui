using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class StockInfoEntryList
    {
        /// <summary>
        /// Enth&auml;lt Lager- und LagerplatzinformationenEs sind nur jene Lager enthalten, auf die der angemeldete Benutzer Zugriff hat
        /// </summary>
        public List<StockInfoEntry> entries { get; set; }
    }
}