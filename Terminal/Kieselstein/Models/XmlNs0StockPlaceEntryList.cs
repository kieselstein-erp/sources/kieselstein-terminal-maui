using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0StockPlaceEntryList
    {
        public List<XmlNs0StockPlaceEntry> entries { get; set; }
    }
}