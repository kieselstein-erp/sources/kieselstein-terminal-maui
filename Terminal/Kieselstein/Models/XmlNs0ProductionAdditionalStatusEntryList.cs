using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ProductionAdditionalStatusEntryList
    {
        public List<XmlNs0ProductionAdditionalStatusEntry> entries { get; set; }
    }
}