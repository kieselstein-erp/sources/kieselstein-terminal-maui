namespace Kieselstein.Models
{
    public static class DeliveryEntrystatusConstants
    {
        public const string NOTINITIALIZED = "NOTINITIALIZED";

        public const string NEW = "NEW";

        public const string OPEN = "OPEN";

        public const string DONE = "DONE";

        public const string CANCELLED = "CANCELLED";

        public const string CLEARED = "CLEARED";

        public const string COMPLETED = "COMPLETED";
    }
}
