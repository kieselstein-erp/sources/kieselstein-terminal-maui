using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ProductionGroupEntry : BaseEntryId 
    {
        public string description { get; set; }

        public int sortOrder { get; set; }
    }
}