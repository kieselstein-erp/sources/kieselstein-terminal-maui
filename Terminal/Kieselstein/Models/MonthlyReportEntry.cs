using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class MonthlyReportEntry
    {
        public string pdfContent { get; set; }

        public string lastPagePng { get; set; }
    }
}