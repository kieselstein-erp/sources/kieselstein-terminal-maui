using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TodoDetailTextEntry : TodoDetailEntry 
    {
        public string text { get; set; }

        public bool html { get; set; }
    }
}