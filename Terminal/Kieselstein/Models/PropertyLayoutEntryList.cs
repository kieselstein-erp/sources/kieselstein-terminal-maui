using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PropertyLayoutEntryList
    {
        public List<PropertyLayoutEntry> entries { get; set; }
    }
}