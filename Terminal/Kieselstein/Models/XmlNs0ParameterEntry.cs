using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ParameterEntry : XmlNs0BaseEntryCnr 
    {
        public string category { get; set; }

        public string datatype { get; set; }

        public string value { get; set; }
    }
}