using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ContactEntryList
    {
        public List<XmlNs0ContactEntry> contactEntries { get; set; }
    }
}