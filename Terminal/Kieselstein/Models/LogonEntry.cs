using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class LogonEntry
    {
        /// <summary>
        /// Das Kennwort f&uuml;r den Benutzer
        /// </summary>
        public string password { get; set; }

        /// <summary>
        /// Der (optionale) gew&uuml;nschte Mandant.Wird der Mandant nicht angegeben, wirdder innerhalb HELIUM V festgelegte Default-Mandant f&uuml;r diesen Benutzerverwendet
        /// </summary>
        public string client { get; set; }

        /// <summary>
        /// Die (optionale) Landes/SprachdefinitionDie ersten beiden Zeichen entsprechen der Sprache ("de", "en", ...),die folgenden beiden Zeichen entsprechen dem Land ("AT", "DE", "IT, ...)Es k&ouml;nnen nur Sprachen bzw. L&auml;nder ausgew&auml;hlt werden, dieauf dem HELIUM V System zur Verf&uuml;gung stehen
        /// </summary>
        public string localeString { get; set; }

        /// <summary>
        /// Der Benutzername
        /// </summary>
        public string username { get; set; }
    }
}