namespace Kieselstein.Models
{
    public static class XmlNs0ForecastDeliveryAddressEntrypickingTypeConstants
    {
        public const string NOTINITIALIZED = "NOTINITIALIZED";

        public const string CALLOFF = "CALLOFF";

        public const string ADDRESS = "ADDRESS";

        public const string ITEM = "ITEM";
    }
}
