using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0LogonIdCardEntry
    {
        public string client { get; set; }

        public string localeString { get; set; }
    }
}