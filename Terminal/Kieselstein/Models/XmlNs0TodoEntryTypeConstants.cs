namespace Kieselstein.Models
{
    public static class XmlNs0TodoEntryTypeConstants
    {
        public const string NOTINITIALIZED = "NOTINITIALIZED";

        public const string PROJECT = "PROJECT";

        public const string ORDER = "ORDER";

        public const string PRODUCTION = "PRODUCTION";
    }
}
