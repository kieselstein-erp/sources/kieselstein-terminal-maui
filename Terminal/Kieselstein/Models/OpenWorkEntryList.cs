using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class OpenWorkEntryList
    {
        /// <summary>
        /// Die (leere) Liste aller offenen Arbeitsg&auml;nge
        /// </summary>
        public List<OpenWorkEntry> entries { get; set; }
    }
}