namespace Kieselstein.Models
{
    public static class ForecastPositionEntryproductTypeConstants
    {
        public const string PICKING = "PICKING";

        public const string STOCK = "STOCK";
    }
}
