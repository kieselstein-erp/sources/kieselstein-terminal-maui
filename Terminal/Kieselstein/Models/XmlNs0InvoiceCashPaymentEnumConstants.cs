namespace Kieselstein.Models
{
    public static class XmlNs0InvoiceCashPaymentEnumConstants
    {
        public const string CASH = "CASH";

        public const string ATM = "ATM";

        public const string CREDITCARD = "CREDITCARD";
    }
}
