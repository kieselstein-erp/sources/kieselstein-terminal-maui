using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestResultEntryList
    {
        public List<TestResultEntry> entries { get; set; }
    }
}