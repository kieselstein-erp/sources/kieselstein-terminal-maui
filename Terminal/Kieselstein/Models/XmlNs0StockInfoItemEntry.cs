using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0StockInfoItemEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Die auf dem Lager befindliche Menge
        /// </summary>
        public long amount { get; set; }

        /// <summary>
        /// Der Lagermindestmenge
        /// </summary>
        public long minimum { get; set; }

        /// <summary>
        /// Die Lagersollmenge
        /// </summary>
        public long nominal { get; set; }
    }
}