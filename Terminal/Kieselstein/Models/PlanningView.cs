using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PlanningView
    {
        public MachineEntryList machineList { get; set; }

        public OpenWorkEntryList openWorkList { get; set; }

        public Dictionary<string, MachineAvailabilityEntryList> machineAvailabilityMap { get; set; }

        public MachineGroupEntryList machineGroupList { get; set; }

        public WorkCalendarEntryList holidayList { get; set; }

        public WorkCalendarEntryList plantHolidayList { get; set; }

        public string viewOpenWorkDetail { get; set; }

        public bool judgeWorkUnitChange { get; set; }

        public int dispatchingGridMinutes { get; set; }

        public int dispatchingBufferMinutes { get; set; }
    }
}