using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TodoDocumentEntry
    {
        public string content { get; set; }

        public string description { get; set; }

        public string documentType { get; set; }

        public string mimeType { get; set; }

        public string name { get; set; }
    }
}