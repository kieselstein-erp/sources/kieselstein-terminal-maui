﻿namespace Kieselstein.Models
{
  public class TimeDistributionEntry : BaseEntryId
  {
    public long tTimeMs { get; set; }
    public ProductionEntry productionEntry { get; set; }
    public ProductionWorkstepEntry productionWorkstepEntry { get; set; }
    public int? machineIdToUse { get; set; }
  }
}
