using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class StaffEntry : BaseEntryId 
    {
        public string personalNr { get; set; }

        public string identityCnr { get; set; }

        public string shortMark { get; set; }

        public string name { get; set; }

        public string firstName { get; set; }
    }
}