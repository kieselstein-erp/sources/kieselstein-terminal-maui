using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TextblockEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Der bin&auml;re Inhalt des "Text"bausteins, sofern essich um einen bin&auml;rer Textbaustein handelt.
        /// </summary>
        public string blob { get; set; }

        /// <summary>
        /// Die Kennung des Textbausteins
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// Der Dateiname eines Bin&auml;rtextbausteins
        /// </summary>
        public string filename { get; set; }

        /// <summary>
        /// Ist der Textbaustein versteckt?
        /// </summary>
        public bool hidden { get; set; }

        /// <summary>
        /// Die Sprache in der der Textbaustein angelegt ist
        /// </summary>
        public string localeCnr { get; set; }

        public string mimeType { get; set; }

        /// <summary>
        /// Der komplette Text des TextbausteinsDer Text kann auch Jasperstyle Auszeichnungen enthalten
        /// </summary>
        public string text { get; set; }
    }
}