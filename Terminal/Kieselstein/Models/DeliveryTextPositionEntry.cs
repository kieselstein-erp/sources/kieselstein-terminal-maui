using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DeliveryTextPositionEntry : BaseEntryId 
    {
        public string text { get; set; }
    }
}