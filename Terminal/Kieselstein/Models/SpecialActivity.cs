using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Eine Sondertaetigkeit
    /// </summary>
    public class SpecialActivity
    {
        /// <summary>
        /// Die id
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Die Bezeichnung der SondertaetigkeitBeispielsweise "KOMMT", "GEHT", "ARZT", "KRANK"
        /// </summary>
        public string activity { get; set; }
    }
}