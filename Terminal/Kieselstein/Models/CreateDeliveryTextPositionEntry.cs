using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CreateDeliveryTextPositionEntry
    {
        /// <summary>
        /// Der Positionstext
        /// </summary>
        public string text { get; set; }
    }
}