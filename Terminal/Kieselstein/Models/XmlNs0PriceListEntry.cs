using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0PriceListEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// true wenn die Preisliste aktiv ist
        /// </summary>
        public bool active { get; set; }

        /// <summary>
        /// die Kennung der Preisliste
        /// </summary>
        public string cnr { get; set; }

        /// <summary>
        /// die W&auml;hrung der Preisliste
        /// </summary>
        public string currency { get; set; }
    }
}