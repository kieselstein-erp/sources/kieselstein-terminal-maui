using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DeliveryData
    {
        public PositionDataEntryList positionData { get; set; }

        /// <summary>
        /// Die Id des Lieferscheins.
        /// </summary>
        public int deliveryId { get; set; }

        public DeliveryPositionEntryList itemPositions { get; set; }

        public DeliveryTextPositionEntryList textPositions { get; set; }
    }
}