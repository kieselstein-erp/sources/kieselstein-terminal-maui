using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ItemCommentMediaInfoEntry : XmlNs0BaseEntryId 
    {
        public string commentType { get; set; }

        public string commentTypeDescription { get; set; }

        public string content { get; set; }

        public string filename { get; set; }

        public int iSort { get; set; }

        public string mimeType { get; set; }

        public long size { get; set; }
    }
}