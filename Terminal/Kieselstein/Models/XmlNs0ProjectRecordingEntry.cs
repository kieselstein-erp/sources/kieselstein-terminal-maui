using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Die Zeitdaten einer T&auml;tigkeit
    /// </summary>
    public class XmlNs0ProjectRecordingEntry : XmlNs0DocumentRecordingEntry 
    {
        /// <summary>
        /// Die anzugebende Projekt-Id. Projekte k&ouml;nnen &uuml;ber die Resource project ermittelt werden.
        /// </summary>
        public int projectId { get; set; }
    }
}