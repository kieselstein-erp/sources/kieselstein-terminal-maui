using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CarEntry : BaseEntryId 
    {
        public string description { get; set; }

        public string numberPlate { get; set; }
    }
}