using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ItemGroupEntry : BaseEntryId 
    {
        /// <summary>
        /// Die Artikelgruppen-"Nummer"
        /// </summary>
        public string cnr { get; set; }

        /// <summary>
        /// Die Bezeichnung
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// Soll die R&uuml;ckgabe eines Leihartikels automatisch gebucht werden?
        /// </summary>
        public bool bookReturn { get; set; }

        /// <summary>
        /// Ist ein Lieferantenzertifikat notwendig
        /// </summary>
        public bool certificationRequired { get; set; }

        /// <summary>
        /// Die Vater-Artikelgruppe sofern vorhanden
        /// </summary>
        public int parentId { get; set; }
    }
}