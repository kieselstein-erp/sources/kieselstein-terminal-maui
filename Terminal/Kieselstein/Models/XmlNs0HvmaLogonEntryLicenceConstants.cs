namespace Kieselstein.Models
{
    public static class XmlNs0HvmaLogonEntrylicenceConstants
    {
        public const string Offline = "Offline";

        public const string Online = "Online";

        public const string Kpi = "Kpi";

        public const string WaWiSK = "WaWi_SK";
    }
}
