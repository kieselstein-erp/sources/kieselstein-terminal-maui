namespace Kieselstein.Models
{
    public static class InvoiceCashPaymentEnumConstants
    {
        public const string CASH = "CASH";

        public const string ATM = "ATM";

        public const string CREDITCARD = "CREDITCARD";
    }
}
