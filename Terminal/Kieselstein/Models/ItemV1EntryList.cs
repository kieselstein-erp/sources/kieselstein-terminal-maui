using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ItemV1EntryList
    {
        public List<ItemV1Entry> entries { get; set; }
    }
}