using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ProductionTargetMaterialEntry : BaseEntryId 
    {
        public int productionId { get; set; }

        public decimal amount { get; set; }

        public string unitCnr { get; set; }

        public string position { get; set; }

        public string comment { get; set; }

        public double price { get; set; }

        public int iSort { get; set; }

        public ItemV1Entry itemEntry { get; set; }

        public decimal amountIssued { get; set; }

        public int mountingMethodId { get; set; }

        public bool belatedWithdrawn { get; set; }
    }
}