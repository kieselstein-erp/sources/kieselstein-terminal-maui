using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0PartlistWorkstepEntry : XmlNs0BaseEntryId 
    {
        public string comment { get; set; }

        public XmlNs0ItemV1Entry itemEntry { get; set; }

        public long jobTimeMs { get; set; }

        public int machineId { get; set; }

        public long setupTimeMs { get; set; }

        public string text { get; set; }

        public string workplanComment { get; set; }

        public int workstepNumber { get; set; }

        public string workstepType { get; set; }
    }
}