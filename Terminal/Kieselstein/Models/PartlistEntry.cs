using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PartlistEntry : BaseEntryId 
    {
        public string cnr { get; set; }

        public string description { get; set; }

        public string additionalDescription { get; set; }

        public int lotCount { get; set; }

        public string statusCnr { get; set; }

        public string customerItemCnr { get; set; }
    }
}