namespace Kieselstein.Models
{
    public static class XmlNs0SpecialTimesEnumConstants
    {
        public const string Holiday = "Holiday";

        public const string TimeCompensation = "TimeCompensation";

        public const string Illness = "Illness";
    }
}
