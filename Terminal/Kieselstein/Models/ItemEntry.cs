using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ItemEntry : BaseEntryId 
    {
        /// <summary>
        /// Die Kennung des Artikels (Artikelnummer)
        /// </summary>
        public string cnr { get; set; }

        /// <summary>
        /// Die Zusatzbezeichnung des Artikels
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// Die Zusatzbezeichnung2 des Artikels
        /// </summary>
        public string description2 { get; set; }

        /// <summary>
        /// Der Name des Artikels
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Die Kurzbezeichnung des Artikels
        /// </summary>
        public string shortName { get; set; }

        /// <summary>
        /// Der "verf&uuml;gbare" Lagerstand
        /// </summary>
        public double stockAmount { get; set; }

        /// <summary>
        /// Der Gestehungspreis
        /// </summary>
        public double costs { get; set; }

        /// <summary>
        /// Die St&uuml;cklistenart
        /// </summary>
        public string billOfMaterialType { get; set; }

        /// <summary>
        /// Ist der Artikel verfÃ¼gbar bzw. gesperrt?
        /// </summary>
        public bool available { get; set; }

        /// <summary>
        /// Die Liste aller Artikelkommentare im Format "text/html"
        /// </summary>
        public List<string> comments { get; set; }

        /// <summary>
        /// Ist es ein versteckter Artikel?
        /// </summary>
        public bool hidden { get; set; }

        /// <summary>
        /// Die Einheit des Artikels
        /// </summary>
        public string unitCnr { get; set; }

        /// <summary>
        /// Die Artikelart
        /// </summary>
        public string typeCnr { get; set; }

        /// <summary>
        /// Die Artikelgruppe
        /// </summary>
        public string itemgroupCnr { get; set; }

        /// <summary>
        /// Die Artikelklasse
        /// </summary>
        public string itemclassCnr { get; set; }

        /// <summary>
        /// Die Revisions"nummer"
        /// </summary>
        public string revision { get; set; }

        public StockAmountInfoEntry stockAmountInfo { get; set; }

        /// <summary>
        /// Die Referenznummer
        /// </summary>
        public string referenceNumber { get; set; }

        /// <summary>
        /// Die Indexnummer
        /// </summary>
        public string index { get; set; }

        public string preferredType { get; set; }

        public string customerItemCnr { get; set; }
    }
}