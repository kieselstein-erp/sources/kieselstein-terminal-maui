using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestResultCrimpingIsolationEntry : TestResultCrimpingEntry 
    {
        public double crimpHeightIsolation { get; set; }

        public double crimpWidthIsolation { get; set; }
    }
}