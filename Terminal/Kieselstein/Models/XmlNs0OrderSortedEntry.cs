using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0OrderSortedEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// ist true wenn die Sortierung nach Artikelnummerdurchgef&uuml;hrt wurde.
        /// </summary>
        public bool sorted { get; set; }
    }
}