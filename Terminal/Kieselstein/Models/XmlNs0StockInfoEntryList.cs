using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0StockInfoEntryList
    {
        /// <summary>
        /// Enth&auml;lt Lager- und LagerplatzinformationenEs sind nur jene Lager enthalten, auf die der angemeldete Benutzer Zugriff hat
        /// </summary>
        public List<XmlNs0StockInfoEntry> entries { get; set; }
    }
}