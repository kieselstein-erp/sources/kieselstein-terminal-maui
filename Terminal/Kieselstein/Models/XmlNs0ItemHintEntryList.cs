using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ItemHintEntryList
    {
        public List<XmlNs0ItemHintEntry> entries { get; set; }
    }
}