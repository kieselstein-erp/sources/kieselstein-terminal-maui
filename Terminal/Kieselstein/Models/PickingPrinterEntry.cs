using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PickingPrinterEntry : BaseEntryId 
    {
        public string cnr { get; set; }

        public string description { get; set; }
    }
}