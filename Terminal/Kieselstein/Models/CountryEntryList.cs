using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class CountryEntryList
    {
        /// <summary>
        /// Die Liste aller L&auml;nder
        /// </summary>
        public List<CountryEntry> entries { get; set; }

        public long rowCount { get; set; }
    }
}