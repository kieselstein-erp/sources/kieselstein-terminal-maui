using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TestPlanCrimpIsoEntry : XmlNs0TestPlanCrimpEntry 
    {
        public long crimpHeightIsolation { get; set; }

        public long crimpHeightIsolationTolerance { get; set; }

        public long crimpWidthIsolation { get; set; }

        public long crimpWidthIsolationTolerance { get; set; }
    }
}