using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ZeitdatenEntry : XmlNs0BaseEntryId 
    {
        public string activityCnr { get; set; }

        public string description { get; set; }

        public string duration { get; set; }

        public string flags { get; set; }

        public string time { get; set; }

        public string where { get; set; }
    }
}