using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Repr&auml;sentiert die Antwort eines erfolgreichen Logons
    /// </summary>
    public class LoggedOnHvmaEntry : LoggedOnEntry 
    {
        public string resource { get; set; }
    }
}