using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class InventoryDataEntry
    {
        /// <summary>
        /// Die Artikelnummer
        /// </summary>
        public string itemCnr { get; set; }

        /// <summary>
        /// Die Menge des ArtikelsDie Menge ist optional, wenn es sich um einen Seriennr/Chargennr-behafteten Artikel handelt. In diesem Falle sind die Daten &uuml;berdie Identities zu &uuml;bermitteln.
        /// </summary>
        public decimal amount { get; set; }

        /// <summary>
        /// Die Liste der Seriennr/Chargennr Informationen
        /// </summary>
        public List<IdentityAmountEntry> identities { get; set; }
    }
}