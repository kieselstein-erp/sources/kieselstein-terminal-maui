using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TodoDocumentEntry
    {
        public string name { get; set; }

        public string mimeType { get; set; }

        public string content { get; set; }

        public string documentType { get; set; }

        public string description { get; set; }
    }
}