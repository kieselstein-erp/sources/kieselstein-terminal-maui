using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class OrderActivationEntry : BaseEntryId 
    {
        /// <summary>
        /// Berechnungszeitpunkt bei Erfolg
        /// </summary>
        public long calculationTimestampMs { get; set; }

        /// <summary>
        /// sollte false sein, ist true wenn der Auftragskundesteuerbehaftete Positionen erwartet, im Auftrag aber Ident/Handeingabepositionenohne Steuer enthalten sind.
        /// </summary>
        public bool customerExpectsVATorderHasNoneVATpositions { get; set; }

        /// <summary>
        /// sollte false sein, ist true wenn der Auftragskundesteuerfreie Positionen erwartet, im Auftrag aber Ident/Handeingabepositionenmit Steuer enthalten sind.
        /// </summary>
        public bool customerHasNoVATorderHasVATpositions { get; set; }
    }
}