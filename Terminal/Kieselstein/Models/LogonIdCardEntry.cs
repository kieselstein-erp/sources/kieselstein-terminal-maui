using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class LogonIdCardEntry
    {
        public string client { get; set; }

        public string localeString { get; set; }
    }
}