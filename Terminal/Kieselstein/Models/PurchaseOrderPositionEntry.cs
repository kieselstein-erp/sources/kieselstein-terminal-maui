using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PurchaseOrderPositionEntry : BaseEntryId 
    {
    public ItemV1Entry itemEntry { get; set; }

        public decimal openQuantity { get; set; }

        public int itemId { get; set; }

        public decimal quantity { get; set; }
    }
}