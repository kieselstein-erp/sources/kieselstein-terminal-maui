using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class LogMessageEntry
    {
        public string severity { get; set; }

        /// <summary>
        /// Der Nachrichtentext
        /// </summary>
        public string msg { get; set; }

        /// <summary>
        /// Die Zeit in ms zu der die Nachricht erzeugt wurde
        /// </summary>
        public long time { get; set; }
    }
}