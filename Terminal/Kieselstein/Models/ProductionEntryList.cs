using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ProductionEntryList
    {
        public List<ProductionEntry> entries { get; set; }
    }
}