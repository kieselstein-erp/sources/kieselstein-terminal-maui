using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    /// <summary>
    /// Repr&auml;sentiert eine zu erzeugende Position des Bestellvorschlags
    /// </summary>
    public class CreatePurchaseOrderProposalPositionEntry
    {
        /// <summary>
        /// Die (optionale) Artikel-IdEntweder die Artikel-Id oder die Artikelnummer (#getItemCnr() muss angegeben werden
        /// </summary>
        public int itemId { get; set; }

        /// <summary>
        /// Die (optionale) ArtikelnummerEntweder die Artikelnummer oder die Artikel-Id #getItemId() muss angegeben werden
        /// </summary>
        public string itemCnr { get; set; }

        /// <summary>
        /// Die zu bestellende Menge der Position
        /// </summary>
        public double amount { get; set; }

        /// <summary>
        /// Soll dieser Artikel vorgemerkt werden?Nicht vorgemerkte Positionen desselben Artikels k&ouml;nnen beliebig oft erzeugt werden.Wird die Position vorgemerkt, darf es noch keine vorgemerkte Position eines Artikels mit deritemId geben. Ist eine vorhanden, wird mit Status Code EXPECTATION_FAILED (417)geantwortet.Optionale Eigenschaft, wenn nicht angegeben, dann entsteht keine Vormerkung.
        /// </summary>
        public bool noted { get; set; }

        /// <summary>
        /// Der optionale Liefertermin der Position
        /// </summary>
        public long deliveryDateMs { get; set; }
    }
}