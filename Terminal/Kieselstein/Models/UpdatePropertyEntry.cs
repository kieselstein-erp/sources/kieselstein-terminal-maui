using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class UpdatePropertyEntry
    {
        /// <summary>
        /// Id der Eigenschaft
        /// </summary>
        public int propertyId { get; set; }

        /// <summary>
        /// Zu aktualisierender Inhalt der Eigenschaft. Kann leer oder auch "null" sein.
        /// </summary>
        public string content { get; set; }
    }
}