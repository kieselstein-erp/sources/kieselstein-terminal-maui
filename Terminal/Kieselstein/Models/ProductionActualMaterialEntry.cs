using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
  public class ProductionActualMaterialEntry : BaseEntryId 
    {
        public int productionId { get; set; }

        public decimal amount { get; set; }

        public string unitCnr { get; set; }

        public ItemV1Entry itemEntry { get; set; }

        public decimal amountIssued { get; set; }

        public bool belatedWithdrawn { get; set; }

        public List<IdentityAmountEntry> identities { get; set; }

    }
}