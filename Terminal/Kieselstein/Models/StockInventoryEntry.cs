using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class StockInventoryEntry : BaseEntryId 
    {
        /// <summary>
        /// Lager-"Name"
        /// </summary>
        public string cnr { get; set; }

        public StockInfoItemEntryList entries { get; set; }
    }
}