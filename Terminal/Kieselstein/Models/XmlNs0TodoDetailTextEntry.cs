using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0TodoDetailTextEntry : XmlNs0TodoDetailEntry 
    {
        public bool html { get; set; }

        public string text { get; set; }
    }
}