using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0BarcodeRecordingEntry : XmlNs0TimeRecordingEntry 
    {
        /// <summary>
        /// Barcode
        /// </summary>
        public string barcode { get; set; }

        /// <summary>
        /// Id einer Maschine zur &Uuml;bersteuerung einer Maschine aus Barcode.Optional je nach Barcode.
        /// </summary>
        public int machineId { get; set; }
    }
}