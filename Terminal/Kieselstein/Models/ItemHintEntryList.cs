using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ItemHintEntryList
    {
        public List<ItemHintEntry> entries { get; set; }
    }
}