using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class LogBarcodeEntry
    {
        /// <summary>
        /// Der (gelesene) Barcode
        /// </summary>
        public string barcode { get; set; }

        /// <summary>
        /// Die Zeit in ms zu der der Barcode gelesen wurdeIst die Zeit 0, wird automatisch die Server-Zeitverwendet.
        /// </summary>
        public long time { get; set; }
    }
}