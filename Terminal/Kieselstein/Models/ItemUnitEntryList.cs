using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ItemUnitEntryList
    {
        public List<ItemUnitEntry> entries { get; set; }

        public long rowCount { get; set; }
    }
}