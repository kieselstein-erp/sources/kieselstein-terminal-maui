namespace Kieselstein.Models
{
    public static class PropertyLayoutFillConstants
    {
        public const string HORIZONTAL = "HORIZONTAL";

        public const string VERTICAL = "VERTICAL";

        public const string BOTH = "BOTH";

        public const string NONE = "NONE";
    }
}
