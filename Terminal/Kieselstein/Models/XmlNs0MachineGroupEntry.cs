using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0MachineGroupEntry : XmlNs0BaseEntryId 
    {
        public string description { get; set; }

        public string productionGroupDescription { get; set; }

        public bool showPlanningView { get; set; }
    }
}