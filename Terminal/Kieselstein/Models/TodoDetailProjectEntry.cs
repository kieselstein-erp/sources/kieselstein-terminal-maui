using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TodoDetailProjectEntry : TodoDetailEntry 
    {
        public long timeMs { get; set; }

        public string text { get; set; }

        public bool html { get; set; }

        public string subject { get; set; }
    }
}