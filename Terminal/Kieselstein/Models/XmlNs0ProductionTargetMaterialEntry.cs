using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0ProductionTargetMaterialEntry : XmlNs0BaseEntryId 
    {
        public long amount { get; set; }

        public long amountIssued { get; set; }

        public bool belatedWithdrawn { get; set; }

        public string comment { get; set; }

        public int iSort { get; set; }

        public XmlNs0ItemV1Entry itemEntry { get; set; }

        public int mountingMethodId { get; set; }

        public string position { get; set; }

        public long price { get; set; }

        public int productionId { get; set; }

        public string unitCnr { get; set; }
    }
}