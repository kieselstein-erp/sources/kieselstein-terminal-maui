using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DocumentInfoEntryList
    {
        public List<DocumentInfoEntry> entries { get; set; }
    }
}