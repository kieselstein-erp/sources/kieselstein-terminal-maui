using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ItemPropertyEntryList
    {
        /// <summary>
        /// Die Liste aller ItemGroupEntry Eintr&auml;ge
        /// </summary>
        public List<ItemPropertyEntry> entries { get; set; }
    }
}