using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class StockInfoItemEntryList
    {
        public List<StockInfoItemEntry> entries { get; set; }
    }
}