using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PurchaseOrderPositionEntryList
    {
        public List<PurchaseOrderPositionEntry> entries { get; set; }
    }
}