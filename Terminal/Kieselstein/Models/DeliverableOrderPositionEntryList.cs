using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class DeliverableOrderPositionEntryList
    {
        public List<DeliverableOrderPositionEntry> entries { get; set; }
    }
}