namespace Kieselstein.Models
{
    public static class PlanningViewviewOpenWorkDetailConstants
    {
        public const string STANDARD = "STANDARD";

        public const string PROJEKTKBEZ = "PROJEKT_KBEZ";

        public const string UNBEKANNT = "UNBEKANNT";
    }
}
