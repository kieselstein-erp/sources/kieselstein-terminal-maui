using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0LinecallEntry : XmlNs0BaseEntryId 
    {
        public string line { get; set; }

        public List<XmlNs0LinecallItemEntry> linecallItemEntries { get; set; }

        public string ordernumber { get; set; }

        public long productionDateMs { get; set; }

        public long quantity { get; set; }

        public string sector { get; set; }

        public string sectorDescription { get; set; }
    }
}