using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ForecastDeliveryAddressEntry : BaseEntryId 
    {
        public int forecastId { get; set; }

        public int customerId { get; set; }

        public string deliveryAddress { get; set; }

        public string pickingType { get; set; }

        public string deliveryAddressShort { get; set; }

        public string pickingPrinterCnr { get; set; }
    }
}