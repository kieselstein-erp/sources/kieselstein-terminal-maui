using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class TestResultDimensionalTestEntry : TestResultEntry 
    {
        public double value { get; set; }
    }
}