namespace Kieselstein.Models
{
    public static class SpecialTimesEntrytimeTypeConstants
    {
        public const string Holiday = "Holiday";

        public const string TimeCompensation = "TimeCompensation";

        public const string Illness = "Illness";
    }
}
