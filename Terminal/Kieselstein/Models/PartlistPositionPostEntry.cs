using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class PartlistPositionPostEntry : BaseEntryId 
    {
        public string itemCnr { get; set; }

        public double amount { get; set; }

        public string unitCnr { get; set; }

        public string position { get; set; }

        public string comment { get; set; }
    }
}