namespace Kieselstein.Models
{
    public static class FilterPickingtypeConstants
    {
        public const string ADDRESS = "ADDRESS";

        public const string CALLOFF = "CALLOFF";

        public const string ITEM = "ITEM";

        public const string NOTINITIALIZED = "NOTINITIALIZED";
    }
}
