using Newtonsoft.Json;

namespace Kieselstein.Models
{
  public class OpenWorkUpdateEntry : BaseEntryId
  {
    /// <summary>
    /// Die Los-Nummer
    /// </summary>
    public string productionCnr { get; set; }

    /// <summary>
    /// Die Zeit in ms seit dem 1.1.1970
    /// </summary>
    public long workItemStartDate { get; set; }

    public int machineOffsetMs { get; set; }

    public int machineId { get; set; }

    public long starttimeMs { get; set; }

    public long finishtimeMs { get; set; }

    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public int? storageLocationId { get; set; }
  }
}