using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class MountingMethodEntryList
    {
        public List<MountingMethodEntry> entries { get; set; }
    }
}