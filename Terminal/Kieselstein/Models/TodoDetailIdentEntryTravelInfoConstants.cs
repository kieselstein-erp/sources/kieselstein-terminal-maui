namespace Kieselstein.Models
{
    public static class TodoDetailIdentEntrytravelInfoConstants
    {
        public const string No = "No";

        public const string Passive = "Passive";

        public const string Active = "Active";
    }
}
