using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class ShopGroupEntry : BaseEntryId 
    {
        /// <summary>
        /// die Shopgruppen-Kennung
        /// </summary>
        public string cnr { get; set; }

        /// <summary>
        /// die Bezeichnung der Shopgruppe
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// null (wenn kein Vorg&auml;nger) bzw. die Id der Vater-Shopgruppe
        /// </summary>
        public int parentId { get; set; }

        /// <summary>
        /// null (wenn kein Vorg&auml;nger) bzw. die Kennung der Vater-Shopgruppe
        /// </summary>
        public string parentCnr { get; set; }

        /// <summary>
        /// null (wenn kein Vorg&auml;nger) bzw. die Beschreibung der Vater-Shopgruppe
        /// </summary>
        public string parentDescription { get; set; }
    }
}