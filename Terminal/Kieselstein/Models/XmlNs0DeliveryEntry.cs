using System;
using System.Collections.Generic;

namespace Kieselstein.Models
{
    public class XmlNs0DeliveryEntry : XmlNs0BaseEntryId 
    {
        /// <summary>
        /// Der Ort im Format "- ", also zum Beispiel "AT-5301 Eugendorf", des Kunden - an den geliefert wurde.
        /// </summary>
        public string customerCity { get; set; }

        /// <summary>
        /// Die Id des Kunden - an den geliefert wurde.
        /// </summary>
        public int customerId { get; set; }

        /// <summary>
        /// Der Name des Kunden - an den geliefert wurde.
        /// </summary>
        public string customerName { get; set; }

        /// <summary>
        /// Die Lieferscheinnummer
        /// </summary>
        public string deliveryCnr { get; set; }

        public string status { get; set; }
    }
}