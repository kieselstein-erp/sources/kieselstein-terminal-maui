using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using RestEase;
using Kieselstein.Models;

namespace Kieselstein.Api
{
    /// <summary>
    /// Kieselstein REST API	     Homepage
    /// </summary>
    public interface IKieselsteinApi
    {
        /// <summary>
        /// ReceiveOrders (/api/beta/edifact/orders)
        /// </summary>
        /// <param name="token"></param>
        [Post("/api/beta/edifact/orders")]
        Task<Response<object>> ReceiveOrdersAsync([Query] string token);

        /// <summary>
        /// Eine Losablieferung durchf&uuml;hren
        ///
        /// BucheLosAblieferung (/api/soapcalls/personal/losablieferung)
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="idCard"></param>
        /// <param name="productionCnr">die Losnummer</param>
        /// <param name="serialnoreader">die (optionale) Seriennummer des (Barcode)Lesers</param>
        /// <param name="station">die (optionale) Station des (Barcode)Lesers</param>
        /// <param name="userid"></param>
        [Post("/api/soapcalls/personal/losablieferung")]
        Task<SoapCallPersonalResult> BucheLosAblieferungAsync([Query] int? amount, [Query] string idCard, [Query] string productionCnr, [Query] string serialnoreader, [Query] string station, [Query] string userid);

        /// <summary>
        /// Eine Losablieferung f&uuml;r einen Artikel mit Chargennummer durchf&uuml;hren
        ///
        /// BucheLosAblieferungCharge (/api/soapcalls/personal/losablieferungchg)
        /// </summary>
        /// <param name="amount">die Menge dieser Chargennummer</param>
        /// <param name="itemCnr">die Artikelnummer</param>
        /// <param name="productionCnr">die Losnummer</param>
        /// <param name="serialnr">die Chargennummer</param>
        /// <param name="station">die (optionale) Station des (Barcode)Lesers</param>
        /// <param name="userid">die Id vom Logon</param>
        [Post("/api/soapcalls/personal/losablieferungchg")]
        Task<SoapCallPersonalResult> BucheLosAblieferungChargeAsync([Query] string amount, [Query] string itemCnr, [Query] string productionCnr, [Query] string serialnr, [Query] string station, [Query] string userid);

        /// <summary>
        /// Eine Losablieferung f&uuml;r einen Artikel mit Seriennummer durchf&uuml;hren
        ///
        /// BucheLosAblieferungSeriennummer (/api/soapcalls/personal/losablieferungsnr)
        /// </summary>
        /// <param name="itemCnr">die Artikelnummer</param>
        /// <param name="productionCnr">die Losnummer</param>
        /// <param name="serialnr"></param>
        /// <param name="station">die (optionale) Station des (Barcode)Lesers</param>
        /// <param name="userid"></param>
        /// <param name="version">die Zusatzinformation (Version) zur Seriennummer</param>
        [Post("/api/soapcalls/personal/losablieferungsnr")]
        Task<SoapCallPersonalResult> BucheLosAblieferungSeriennummerAsync([Query] string itemCnr, [Query] string productionCnr, [Query] string serialnr, [Query] string station, [Query] string userid, [Query] string version);

        /// <summary>
        /// Eine Losgr&ouml;&szlig;en&auml;nderung durchf&uuml;hren
        ///
        /// BucheLosGroessenAenderung (/api/soapcalls/personal/losgroessenaenderung)
        /// </summary>
        /// <param name="amount">die neue Gr&ouml;&szlig;e des Loses</param>
        /// <param name="idCard">die Ausweisnummer jenes Mitarbeiters der die Buchung durchf&uuml;hrt</param>
        /// <param name="productionCnr">die Losnummer</param>
        /// <param name="serialnoreader">die (optionale) Seriennummer des (Barcode)Lesers</param>
        /// <param name="station">die (optionale) Station des (Barcode)Lesers</param>
        /// <param name="userid">die Id vom Logon</param>
        [Post("/api/soapcalls/personal/losgroessenaenderung")]
        Task<SoapCallPersonalResult> BucheLosGroessenAenderungAsync([Query] int? amount, [Query] string idCard, [Query] string productionCnr, [Query] string serialnoreader, [Query] string station, [Query] string userid);

        /// <summary>
        /// GetCars (/api/v1/car)
        /// </summary>
        /// <param name="filterWithHidden"></param>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/car")]
        Task<CarEntryList> GetCarsAsync([Query(Name = "filter_withHidden")] bool? filterWithHidden, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Eine Liste aller - den Filterkriterien entsprechenden - Kunden ermitteln.
        ///
        /// GetCustomers (/api/v1/customer)
        /// </summary>
        /// <param name="filterCity">schr&auml;nkt die Ausgabe auf jene Kunden ein, deren Adresse sich in derangegebenen Stadt befindet</param>
        /// <param name="filterCompany">schr&auml;nkt die Ausgabe auf jene Kunden ein, die den Firmennamen(filterCompany) beinhalten</param>
        /// <param name="filterExtendedSearch">schr&auml;nkt die Ausgabe auf jene Kunden ein, bei denen mitder erweiterten Suche der angegebene Text enthalten ist</param>
        /// <param name="filterWithCustomers">selektiert jene Eintr&auml;ge, bei denen der Datensatz alsKunde qualifiziert sind</param>
        /// <param name="filterWithHidden">mit  werden auch versteckte Kunden selektiert</param>
        /// <param name="filterWithProspectiveCustomers">selektiert jene Eintr&auml;ge, bei denen es sich umeinen Interessenten handelt.</param>
        /// <param name="limit">die Anzahl der zu liefernden Datens&auml;tze. Default sind 50</param>
        /// <param name="startIndex">der x-te Eintrag ab dem die Datens&auml;tze geliefert werden sollen</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde</param>
        [Get("/api/v1/customer")]
        Task<List<CustomerEntry>> GetCustomersAsync([Query(Name = "filter_city")] string filterCity, [Query(Name = "filter_company")] string filterCompany, [Query(Name = "filter_extendedSearch")] string filterExtendedSearch, [Query(Name = "filter_withCustomers")] bool? filterWithCustomers, [Query(Name = "filter_withHidden")] bool? filterWithHidden, [Query(Name = "filter_withProspectiveCustomers")] bool? filterWithProspectiveCustomers, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Die Kundendaten eines Kunden ermitteln, der &uuml;ber seine Anmeldung  bekannt ist.
        ///
        /// GetLoggedOnCustomer (/api/v1/customer/loggedon)
        /// </summary>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde</param>
        [Get("/api/v1/customer/loggedon")]
        Task<CustomerDetailLoggedOnEntry> GetLoggedOnCustomerAsync([Query] string userid);

        /// <summary>
        /// Die Kundenpreisliste &uuml;ber das Kurzzeichen des Kunden ermittelnAbh&auml;ngig vom Umfang der Artikel bzw. der Preisliste kann dieser Aufruf auchmehrere Sekunden oder sogar Minuten dauern. Die R&uuml;ckgabe kann auch mehrere MB gro&szlig; sein.
        ///
        /// GetPriceListCustomerShortName (/api/v1/customer/pricelist)
        /// </summary>
        /// <param name="customerShortname">ist das Kurzzeichen jenes Kunden f&uuml;r den die Preisliste ermitteltwerden soll. Gibt es f&uuml;r das angegebene Kurzzeichen mehr als einen Kunden, wird der "erste"(beliebige Reihenfolge) Kunde verwendet</param>
        /// <param name="filterItemclasscnr">optional nur Artikel mit dieser Artikelklassenummer</param>
        /// <param name="filterItemclassid">optional nur Artikel mit dieser ArtikelklassenId</param>
        /// <param name="filterItemgroupcnr">optional nur Artikel mit dieser Artikelgruppe selektieren</param>
        /// <param name="filterItemgroupid">optional nur Artikel mit dieser ArtikelgruppenId selektieren</param>
        /// <param name="filterOnlyForWebshop">mit true werden nur jene Artikel in die Preisliste aufgenommen, dieexplizit im Webshop verf&uuml;gbar sein sollen, also eine beliebige Shopgruppe innerhalb HELIUM V haben</param>
        /// <param name="filterOnlySpecialcondition">mit true werden nur die Artikel mit Sonderkondition in diePreisliste aufgenommen</param>
        /// <param name="filterStartItemcnr">optional nur Artikel ab dieser Artikelnummer</param>
        /// <param name="filterStopItemcnr">optional nur Artikel bis (einschlie&szlig;lich) zu dieser Artikelnummer</param>
        /// <param name="filterValiditydate">optional mit dem angebenen Preisg&uuml;ltigkeitsdatum im ISO8601 Format(Beispiel: JJJJ-MM-TT"T"hh:mm ... 2013-12-31T14:00Z oder 2013-12-31T14:59+01:00)</param>
        /// <param name="filterWithClientLanguage">mit true werden die Artikelbezeichnungen zus&auml;tzlich in derMandantensprache ausgegeben</param>
        /// <param name="filterWithHidden">mit true werden auch versteckte Artikel in die Preisliste aufgenommen</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde</param>
        [Get("/api/v1/customer/pricelist")]
        Task<CustomerPricelistReportDto> GetPriceListCustomerShortNameAsync([Query(Name = "customer_shortname")] string customerShortname, [Query(Name = "filter_itemclasscnr")] string filterItemclasscnr, [Query(Name = "filter_itemclassid")] int? filterItemclassid, [Query(Name = "filter_itemgroupcnr")] string filterItemgroupcnr, [Query(Name = "filter_itemgroupid")] int? filterItemgroupid, [Query(Name = "filter_onlyForWebshop")] bool? filterOnlyForWebshop, [Query(Name = "filter_onlySpecialcondition")] bool? filterOnlySpecialcondition, [Query(Name = "filter_start_itemcnr")] string filterStartItemcnr, [Query(Name = "filter_stop_itemcnr")] string filterStopItemcnr, [Query(Name = "filter_validitydate")] string filterValiditydate, [Query(Name = "filter_withClientLanguage")] bool? filterWithClientLanguage, [Query(Name = "filter_withHidden")] bool? filterWithHidden, [Query] string userid);

        /// <summary>
        /// Die Kundendaten eines Kunden ermitteln, der &uuml;ber seine Id bekannt ist.
        ///
        /// GetCustomer (/api/v1/customer/{customerid})
        /// </summary>
        /// <param name="customerid">ist die Id des Kunden f&uuml;r den die Daten ermittelt werden sollen.</param>
        /// <param name="addContacts">mit true werden auch die Ansprechpartner des Kunden geliefert</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde</param>
        [Get("/api/v1/customer/{customerid}")]
        Task<CustomerDetailEntry> GetCustomerAsync([Path] int? customerid, [Query] bool? addContacts, [Query] string userid);

        /// <summary>
        /// Die Kundenpreisliste &uuml;ber die Id des Kunden ermittelnAbh&auml;ngig vom Umfang der Artikel bzw. der Preisliste kann dieser Aufruf auchmehrere Sekunden oder sogar Minuten dauern. Die R&uuml;ckgabe kann auch mehrere MB gro&szlig; sein.
        ///
        /// GetPriceList (/api/v1/customer/{customerid}/pricelist)
        /// </summary>
        /// <param name="customerid">die Id des Kunden f&uuml;r den die Preisliste ermittelt werden soll</param>
        /// <param name="filterItemclasscnr">optional nur Artikel mit dieser Artikelklassenummer</param>
        /// <param name="filterItemclassid">optional nur Artikel mit dieser ArtikelklassenId</param>
        /// <param name="filterItemgroupcnr">optional nur Artikel mit dieser Artikelgruppe selektieren</param>
        /// <param name="filterItemgroupid">optional nur Artikel mit dieser ArtikelgruppenId selektieren</param>
        /// <param name="filterOnlyForWebshop">mit true werden nur jene Artikel in die Preisliste aufgenommen, dieexplizit im Webshop verf&uuml;gbar sein sollen, also eine beliebige Shopgruppe innerhalb HELIUM V haben</param>
        /// <param name="filterOnlySpecialcondition">mit true werden nur die Artikel mit Sonderkondition in diePreisliste aufgenommen</param>
        /// <param name="filterStartItemcnr">optional nur Artikel ab dieser Artikelnummer</param>
        /// <param name="filterStopItemcnr">optional nur Artikel bis (einschlie&szlig;lich) zu dieser Artikelnummer</param>
        /// <param name="filterValiditydate">optional mit dem angebenen Preisg&uuml;ltigkeitsdatum im ISO8601 Format(Beispiel: JJJJ-MM-TT"T"hh:mm ... 2013-12-31T14:00Z oder 2013-12-31T14:59+01:00)</param>
        /// <param name="filterWithClientLanguage">mit true werden die Artikelbezeichnungen zus&auml;tzlich in derMandantensprache ausgegeben</param>
        /// <param name="filterWithHidden">mit true werden auch versteckte Artikel in die Preisliste aufgenommen</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde</param>
        [Get("/api/v1/customer/{customerid}/pricelist")]
        Task<CustomerPricelistReportDto> GetPriceListAsync([Path] int? customerid, [Query(Name = "filter_itemclasscnr")] string filterItemclasscnr, [Query(Name = "filter_itemclassid")] int? filterItemclassid, [Query(Name = "filter_itemgroupcnr")] string filterItemgroupcnr, [Query(Name = "filter_itemgroupid")] int? filterItemgroupid, [Query(Name = "filter_onlyForWebshop")] bool? filterOnlyForWebshop, [Query(Name = "filter_onlySpecialcondition")] bool? filterOnlySpecialcondition, [Query(Name = "filter_start_itemcnr")] string filterStartItemcnr, [Query(Name = "filter_stop_itemcnr")] string filterStopItemcnr, [Query(Name = "filter_validitydate")] string filterValiditydate, [Query(Name = "filter_withClientLanguage")] bool? filterWithClientLanguage, [Query(Name = "filter_withHidden")] bool? filterWithHidden, [Query] string userid);

        /// <summary>
        /// Einen Lieferschein erzeugenWird customerCnr oder customerId angegeben, wird ein freier Lieferscheinf&uuml;r den angegebenen Kunden erzeugt. Bei einem freien Lieferscheind&uuml;rfen bis zu 10 offene Lieferscheine (dieses Kunden) angelegt werden.Wird orderCnr oder orderId angegeben, wird ein auftragsbezogenerLieferschein erzeugtGenerell wird &uuml;berpr&uuml;ft, ob der durch das token identifizierteHELIUM V Anwender das Recht hat, auf das Abbuchungslager des Lieferscheinszuzugreifen.
        ///
        /// CreateDelivery (/api/v1/delivery)
        /// </summary>
        /// <param name="customercnr">die (optionale) Kundennummer</param>
        /// <param name="customerid">die (optionale) Id des Kunden. Bei einem freienLieferschein muss entweder die customerCnr oder die customerIdangegeben werden</param>
        /// <param name="orderCnr">die (optionale) Auftragsnummer</param>
        /// <param name="orderid">die (optionale) Id des Auftrags. Bei einem auftragsbezogenenLieferschein muss entweder die orderCnr oder die orderId angegebenwerden.</param>
        /// <param name="userid">ist das beim Logon ermittelte "token"</param>
        [Post("/api/v1/delivery")]
        Task<int> CreateDeliveryAsync([Query] int? customercnr, [Query] int? customerid, [Query] string orderCnr, [Query] int? orderid, [Query] string userid);

        /// <summary>
        /// Die Liste aller angelegten Kundenlieferscheine des KundenDer Kunde wird durch seine "Kundennummer" identifiziert. Es werden nur jene Lieferscheineangezeigt, f&uuml;r dessen Lager der Anwender eine Berechtigung hat.
        ///
        /// GetCustomerDeliverableSlips (/api/v1/delivery/customerdeliverables/{customercnr})
        /// </summary>
        /// <param name="customercnr">Die Kundennummer muss immer angegeben werden. Achtung: customerCNrist die dem Kunden zugewiesene Kundennummer. Sie ist nicht mit der customerId zu verwechseln!</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/delivery/customerdeliverables/{customercnr}")]
        Task<DeliveryEntryList> GetCustomerDeliverableSlipsAsync([Path] int? customercnr, [Query] string userid);

        /// <summary>
        /// Die Liste der Positionen dieses KundenlieferscheinsDer Lieferschein muss sich noch im Status ANGELEGT befinden, ansonsten gibt es eineentsprechende Fehlermeldung (Expectation failed, falscher Status)
        ///
        /// GetCustomerDeliverableSlipPositions (/api/v1/delivery/customerdeliverables/{customercnr}/{deliveryid})
        /// </summary>
        /// <param name="customercnr">Die Kundennummer muss immer angegeben werden. Achtung: customerCNrist die dem Kunden zugewiesene Kundennummer. Sie ist nicht mit der customerId zu verwechseln!</param>
        /// <param name="deliveryid">die Id des Kundenlieferscheins</param>
        /// <param name="itemCnr">ist die (optionale) Artikelnummer auf den die Positionsinfo eingeschr&auml;nkt werden soll</param>
        /// <param name="itemid">ist die (optionale) Id des Artikels auf den die Positionsinfo eingeschr&auml;nkt werden soll</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/delivery/customerdeliverables/{customercnr}/{deliveryid}")]
        Task<DeliveryPositionEntryList> GetCustomerDeliverableSlipPositionsAsync([Path] int? customercnr, [Path] int? deliveryid, [Query] string itemCnr, [Query] int? itemid, [Query] string userid);

        /// <summary>
        /// Die Liste aller angelegten Kundenlieferscheine des KundenDer Kunde wird durch seine Id identifiziert. Es werden nur jene Lieferscheineangezeigt, f&uuml;r dessen Lager der Anwender eine Berechtigung hat.
        ///
        /// GetCustomerIdDeliverableSlips (/api/v1/delivery/customerdeliverablesid/{customerid})
        /// </summary>
        /// <param name="customerid">Die Id des Kunden muss immer angegeben werden.</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/delivery/customerdeliverablesid/{customerid}")]
        Task<DeliveryEntryList> GetCustomerIdDeliverableSlipsAsync([Path] int? customerid, [Query] string userid);

        /// <summary>
        /// Eine neue freie Lieferscheinposition f&uuml;r einen Kundenlieferschein erstellenBei Bedarf wird ein neuer Lieferschein angelegt. Dies wird dadurch mitgeteilt, dasspositionEntry.deliveryId null ist. Sollen f&uuml;r den Lieferschein weitere Positionenerfasst werden, muss die - hier erhaltene - deliveryId in den folgenden Anfragen angegebenwerden. Die Kundennummer - customerNr - muss immer angegeben werden. Achtung: customerNrist die dem Kunden zugewiesene Kundennummer. Sie ist nicht mit der customerId zu verwechseln!Der betroffene Lieferschein muss ein freier Lieferschein sein, d.h. erdarf keinen Bezug zu einem Auftrag haben.
        ///
        /// CreateCustomerPosition (/api/v1/delivery/customerposition)
        /// </summary>
        /// <param name="content">Repr&auml;sentiert eine zu liefernde Position</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Post("/api/v1/delivery/customerposition")]
        [Header("Content-Type", "application/json")]
        Task<CreatedDeliveryPositionEntry> CreateCustomerPositionAsync([Body] CreateItemCustomerDeliveryPositionEntry content, [Query] string userid);

        /// <summary>
        /// GetDeliveryEntry (/api/v1/delivery/deliverable)
        /// </summary>
        /// <param name="hvtoken"></param>
        /// <param name="deliveryCnr"></param>
        /// <param name="deliveryid"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/delivery/deliverable")]
        Task<DeliverableDeliveryEntry> GetDeliveryEntryAsync( [Query] string deliveryCnr, [Query] int? deliveryid, [Query] string userid);

        /// <summary>
        /// Eine neue freie Lieferscheinposition f&uuml;r einen Kundenlieferschein erstellenBei Bedarf wird ein neuer Lieferschein angelegt. Dies wird dadurch mitgeteilt, dasspositionEntry.deliveryId null ist. Sollen f&uuml;r den Lieferschein weitere Positionenerfasst werden, muss die - hier erhaltene - deliveryId in den folgenden Anfragen angegebenwerden. Die Kundennummer - customerNr - muss immer angegeben werden. Achtung: customerNrist die dem Kunden zugewiesene Kundennummer. Sie ist nicht mit der customerId zu verwechseln!Es wird automatisch zuerst nach offenen Forecast-Positionen f&uuml;r den angegbenenArtikel gesucht. Sollte es solche nicht geben, wird nach offenen Auftragspositionengesucht. Gibt es auch keine Auftragsfunktionen wird abgebrochen.Sollte der Fall eintreten, dass die zu buchende Menge auf mehrere Belegpositionen(Forecast-Position, Auftragposition) aufgeteilt wird, wird nur die letzte erzeugteLieferscheinposition und somit auch nur die dort gebuchte Menge als Ergebnis gebracht.Im HELIUM V Server muss die entsprechende Zusatzfunktionsberechtigung freigeschaltet sein.
        ///
        /// CreateForecastOrderCustomerPosition (/api/v1/delivery/forecastcustomerposition)
        /// </summary>
        /// <param name="content">Repr&auml;sentiert eine zu liefernde Position</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Post("/api/v1/delivery/forecastcustomerposition")]
        [Header("Content-Type", "application/json")]
        Task<CreatedDeliveryPositionEntry> CreateForecastOrderCustomerPositionAsync([Body] CreateItemCustomerDeliveryPositionEntry content, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Lieferscheine gem&auml;&szlig; den Filterkriterien
        ///
        /// GetListDeliveryEntry (/api/v1/delivery/list)
        /// </summary>
        /// <param name="filterCnr">(optional) schr&auml;nkt die Ausgabe auf Lieferscheine ein, die diesen Teil der Lieferscheinnummer beinhalten</param>
        /// <param name="filterCustomer">(optional) schr&auml;nkt die Ausgabe auf Lieferscheine ein, die f&uuml;r diesen Kundennamen ausgestellt wurden</param>
        /// <param name="filterOrdercnr">(optional) schr&auml;nkt die Ausgabe auf Lieferscheine ein, die mit diesem Auftrag verkn&uuml;pft sind</param>
        /// <param name="filterStatus">(optional) die zu filternden Status (aus DeliveryDocumentStatus) getrennt durch Komma</param>
        /// <param name="limit">(optional) die maximale Anzahl von Eintr&auml;gen die ermittelt werden sollen</param>
        /// <param name="startIndex">(optional) der StartIndex</param>
        /// <param name="userid">userId ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/delivery/list")]
        Task<DeliveryEntryList> GetListDeliveryEntryAsync([Query(Name = "filter_cnr")] string filterCnr, [Query(Name = "filter_customer")] string filterCustomer, [Query(Name = "filter_ordercnr")] string filterOrdercnr, [Query(Name = "filter_status")] string filterStatus, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Die angegebene LieferscheinpositionId l&ouml;schen
        ///
        /// RemovePosition (/api/v1/delivery/position)
        /// </summary>
        /// <param name="positionid">die zu entfernende LieferscheinpositionId</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Delete("/api/v1/delivery/position")]
        Task<Response<object>> RemovePositionAsync([Query] int? positionid, [Query] string userid);

        /// <summary>
        /// Eine neue Lieferscheinposition zu einem freien Lieferschein hinzuf&uuml;genFreier Lieferschein bedeutet, dass dieser Lieferschein keinen Bezug zueinem Auftrag hat.
        ///
        /// CreatePosition (/api/v1/delivery/position)
        /// </summary>
        /// <param name="content">Repr&auml;sentiert eine zu liefernde Artikelposition</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Post("/api/v1/delivery/position")]
        [Header("Content-Type", "application/json")]
        Task<CreatedDeliveryPositionEntry> CreatePositionAsync([Body] CreateItemDeliveryPositionEntry content, [Query] string userid);

        /// <summary>
        /// Eine bestehende freie Lieferscheinposition ab&auml;ndern
        ///
        /// UpdatePosition (/api/v1/delivery/position/{positionid})
        /// </summary>
        /// <param name="content">Repr&auml;sentiert eine zu liefernde Artikelposition</param>
        /// <param name="positionid">die Id der abzu&auml;ndernden Lieferscheinposition</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Put("/api/v1/delivery/position/{positionid}")]
        [Header("Content-Type", "application/json")]
        Task<CreatedDeliveryPositionEntry> UpdatePositionAsync([Body] CreateItemDeliveryPositionEntry content, [Path] int? positionid, [Query] string userid);

        /// <summary>
        /// Die durch die AuftragspositionId definierte Lieferscheinposition entfernenEs wird (derzeit) nur dann die Lieferscheinposition entfernt, wenn es genau eine(!) Lieferscheinpositionf&uuml;r die angegebene Auftragsposition gibt
        ///
        /// RemovePositionByOrder (/api/v1/delivery/positionfromorder)
        /// </summary>
        /// <param name="deliveryid">den Lieferschein den es betrifft</param>
        /// <param name="orderpositionid">die zu dieser AuftragspositionId zugeh&ouml;rige Lieferscheinposition</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Delete("/api/v1/delivery/positionfromorder")]
        Task<Response<object>> RemovePositionByOrderAsync([Query] int? deliveryid, [Query] int? orderpositionid, [Query] string userid);

        /// <summary>
        /// Eine auftragsbezogene Lieferscheinposition erzeugen/&auml;ndernGibt es f&uuml;r diese Auftragsposition noch keine Lieferscheinposition, wird dieseneu erzeugt. Ansonsten werden die Daten aus positionEntry zu der bereits bestehendenLieferscheinposition hinzugef&uuml;gt.
        ///
        /// CreateOrUpdatePositionFromOrder (/api/v1/delivery/positionfromorder)
        /// </summary>
        /// <param name="content">Repr&auml;sentiert eine zu liefernde Position</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Post("/api/v1/delivery/positionfromorder")]
        [Header("Content-Type", "application/json")]
        Task<CreatedDeliveryPositionEntry> CreateOrUpdatePositionFromOrderAsync([Body] CreateDeliveryPositionEntry content, [Query] string userid);

        /// <summary>
        /// Den Lieferschein als "geliefert" betrachten und ausdruckenDer Lieferschein wird auf dem Drucker ausgedruckt, der dem Arbeitsplatz (des mobilen Endger&auml;tes)zugewiesen worden ist
        ///
        /// PrintDelivery (/api/v1/delivery/print)
        /// </summary>
        /// <param name="deliveryid">die Id des Lieferscheins</param>
        /// <param name="sort"></param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/delivery/print")]
        Task<int> PrintDeliveryAsync([Query] int? deliveryid, [Query] string sort, [Query] string userid);

        /// <summary>
        /// Das Versandetikett des Lieferscheins ausdruckenDas Versandetikett wird auf dem Drucker ausgedruckt, der dem Arbeitsplatz (des mobilen Endger&auml;tes)zugewiesen worden ist
        ///
        /// PrintDispatchLabel (/api/v1/delivery/printdispatchlabel)
        /// </summary>
        /// <param name="activateDelivery">wenn true wird der Lieferschein auch berechnet und aktiviert</param>
        /// <param name="deliveryid">die Id des Lieferscheins</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/delivery/printdispatchlabel")]
        Task<int> PrintDispatchLabelAsync([Query] bool? activateDelivery, [Query] int? deliveryid, [Query] string userid);

        /// <summary>
        /// Das Warenausgangsetikett des Lieferscheins ausdruckenDas Warenausgangsetikett wird auf dem Drucker ausgedruckt, der dem Arbeitsplatz (des mobilen Endger&auml;tes)zugewiesen worden ist
        ///
        /// PrintGoodsIssueLabel (/api/v1/delivery/printgoodsissuelabel)
        /// </summary>
        /// <param name="activateDelivery">wenn true wird der Lieferschein auch berechnet und aktiviert</param>
        /// <param name="deliveryid">die Id des Lieferscheins</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/delivery/printgoodsissuelabel")]
        Task<int> PrintGoodsIssueLabelAsync([Query] bool? activateDelivery, [Query] int? deliveryid, [Query] string userid);

        /// <summary>
        /// GetDocument (/api/v1/delivery/{deliveryid}/document)
        /// </summary>
        /// <param name="deliveryid"></param>
        /// <param name="documentcnr"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/delivery/{deliveryid}/document")]
        Task<Response<object>> GetDocumentAsync([Path] int? deliveryid, [Query] string documentcnr, [Query] string userid);

        /// <summary>
        /// Legt Dateien in die Dokumentenablage eines Lieferscheins ab.Die maximale Upload-Gr&ouml;&szlig;e ist derzeit auf 500KiB gestellt
        ///
        /// CreateDocument (/api/v1/delivery/{deliveryid}/document)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="deliveryid">Id des Lieferscheins</param>
        /// <param name="grouping">ist die Gruppierung (optional)</param>
        /// <param name="keywords">sind die Schlagworte des Dokuments (optional)</param>
        /// <param name="securitylevel">ist die Sicherheitsstufe (optional)</param>
        /// <param name="type">ist die Belegart (optional)</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Post("/api/v1/delivery/{deliveryid}/document")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> CreateDocumentAsync([Body] MultipartBody content, [Path] int? deliveryid, [Query] string grouping, [Query] string keywords, [Query] long? securitylevel, [Query] string type, [Query] string userid);

        /// <summary>
        /// GetDocuments (/api/v1/delivery/{deliveryid}/document/list)
        /// </summary>
        /// <param name="deliveryid"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/delivery/{deliveryid}/document/list")]
        Task<DocumentInfoEntryList> GetDocumentsAsync([Path] int? deliveryid, [Query] string userid);

        /// <summary>
        /// Erzeugen einer Artikelposition im LieferscheinEs kann sich sowohl um einen freien Lieferschein als auch einenauftragsbezogenen handeln.Der Lieferschein muss im Status ANGELEGT sein.Wenn eine Auftragsposition geliefert werden soll, reicht es, dieorderPositionId und die Menge/Seriennr/Chargen anzugeben.Bei der Lieferung einer Auftragsposition wird bei mehrfachen Aufrufendieser Funktion immer zur Lieferscheinposition mit Auftragsbezug addiert.Bei einem freien Lieferschein, oder bei einem auftragsbezogenenLieferschein dessen Auftrag diesen Artikel nicht kennt, wird nur dann zur bestehendenLieferscheinposition addiert, wenn der ParameterLIEFERSCHEIN_POSITIONEN_MIT_RESTAPI_VERDICHTEN aktiviert ist,ansonsten werden neue Lieferscheinpositionen angelegt.Tipp: Verwenden Sie orderPositionId immer dann, wennder Artikel im Zuge des Auftrags geliefert werden soll. Verwenden SieitemId/itemCnr immer dann, wenn der Artikel zwar geliefertwerden soll, aber keinen Auftragsbezug hat.
        ///
        /// CreateIdentPosition (/api/v1/delivery/{deliveryid}/itemposition)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="deliveryid">ist die Id des Lieferscheins</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Post("/api/v1/delivery/{deliveryid}/itemposition")]
        [Header("Content-Type", "application/json")]
        Task<CreatedDeliveryPositionEntry> CreateIdentPositionAsync([Body] CreateDeliveryItemPositionEntry content, [Path] int? deliveryid, [Query] string userid);

        /// <summary>
        /// &Auml;ndert die angegebene Lieferscheinposition auf eine ArtikelpositionDer Lieferschein muss im Status ANGELEGT sein.Unterscheiden sich die Positionsarten der bereits bestehendenPosition und der abzu&auml;ndernden, so wird prinzipiell eineneue Position angelegt.Auch wenn es sich bei der bestehenden Position um eineArtikelposition handelt, kann nicht davonausgegangen werden, dass die Id der neuen Position die gleiche bleibt.Wird mit der Id der Position weitergearbeitet, ist die imResponse enthaltende deliveryPositionId zuverwenden.
        ///
        /// UpdateItemPosition (/api/v1/delivery/{deliveryid}/itemposition/{positionid})
        /// </summary>
        /// <param name="content"></param>
        /// <param name="deliveryid">ist die Id des Lieferscheins</param>
        /// <param name="positionid">ist die abzu&auml;ndernde Lieferscheinposition</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Put("/api/v1/delivery/{deliveryid}/itemposition/{positionid}")]
        [Header("Content-Type", "application/json")]
        Task<CreatedDeliveryPositionEntry> UpdateItemPositionAsync([Body] CreateDeliveryItemPositionEntry content, [Path] int? deliveryid, [Path] int? positionid, [Query] string userid);

        /// <summary>
        /// Die Liste der Positionen dieses KundenlieferscheinsDer Lieferschein muss sich noch im Status ANGELEGT befinden, ansonsten gibt es eineentsprechende Fehlermeldung (Expectation failed, falscher Status)Der Kunde darf keine keine Liefersperre haben. Falls er f&uuml;r dieLieferung gesperrt, wird dies mit Bad Request (KUNDE_GESPERRT) beantwortet.
        ///
        /// GetCustomerDeliverableSlipPositionsByDeliveryId (/api/v1/delivery/{deliveryid}/positions)
        /// </summary>
        /// <param name="deliveryid">die Id des Kundenlieferscheins</param>
        /// <param name="itemCnr">ist die (optionale) Artikelnummer auf dendie Positionsinfo eingeschr&auml;nkt werden soll</param>
        /// <param name="itemid">ist die (optionale) Id des Artikels auf dendie Positionsinfo eingeschr&auml;nkt werden soll</param>
        /// <param name="orderpositionid">ist die (optionale) Id der Auftragspositionauf die die Positionsinfo eingeschr&auml;nkt werden soll</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/delivery/{deliveryid}/positions")]
        Task<DeliveryData> GetCustomerDeliverableSlipPositionsByDeliveryIdAsync([Path] int? deliveryid, [Query] string itemCnr, [Query] int? itemid, [Query] int? orderpositionid, [Query] string userid);

        /// <summary>
        /// PrintDeliverySlipSignature (/api/v1/delivery/{deliveryid}/printsignature)
        /// </summary>
        /// <param name="deliveryid"></param>
        /// <param name="sort"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/delivery/{deliveryid}/printsignature")]
        Task<DeliverySlipSignatureEntry> PrintDeliverySlipSignatureAsync([Path] int? deliveryid, [Query] string sort, [Query] string userid);

        /// <summary>
        /// PostDeliverySlipSignature (/api/v1/delivery/{deliveryid}/printsignature)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="deliveryid"></param>
        /// <param name="userid"></param>
        [Post("/api/v1/delivery/{deliveryid}/printsignature")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> PostDeliverySlipSignatureAsync([Body] DeliverySlipSignaturePostEntry content, [Path] int? deliveryid, [Query] string userid);

        /// <summary>
        /// Eine Texteingabe-Position zu einem Lieferschein hinzuf&uuml;genEs kann sich sowohl um einen freien als auch einenauftragsbezogenen Lieferschein handeln.
        ///
        /// CreateTextPosition (/api/v1/delivery/{deliveryid}/textposition)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="deliveryid">ist die Id des Lieferscheins</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Post("/api/v1/delivery/{deliveryid}/textposition")]
        [Header("Content-Type", "application/json")]
        Task<CreatedDeliveryPositionEntry> CreateTextPositionAsync([Body] CreateDeliveryTextPositionEntry content, [Path] int? deliveryid, [Query] string userid);

        /// <summary>
        /// &Auml;ndert die angegebene Lieferscheinposition auf eineTextpositionDer Lieferschein muss im Status ANGELEGT sein.Unterscheiden sich die Positionsarten der bereits bestehendenPosition und der abzu&auml;ndernden, so wird prinzipiell eineneue Position angelegt.
        ///
        /// UpdateTextPosition (/api/v1/delivery/{deliveryid}/textposition/{positionid})
        /// </summary>
        /// <param name="content"></param>
        /// <param name="deliveryid">ist die Id des Lieferscheins</param>
        /// <param name="positionid">ist die abzu&auml;ndernde Lieferscheinposition</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Put("/api/v1/delivery/{deliveryid}/textposition/{positionid}")]
        [Header("Content-Type", "application/json")]
        Task<CreatedDeliveryPositionEntry> UpdateTextPositionAsync([Body] CreateDeliveryTextPositionEntry content, [Path] int? deliveryid, [Path] int? positionid, [Query] string userid);

        /// <summary>
        /// Die ger&auml;tespezifische Konfiguration ermitteln
        ///
        /// GetConfig (/api/v1/device/config)
        /// </summary>
        /// <param name="devicecnr">die eindeutige Ger&auml;tenummer</param>
        /// <param name="devicetag">eine optionale zus&auml;tzliche Kennung des Ger&auml;tes</param>
        /// <param name="devicetype">der Ger&auml;tetyp</param>
        /// <param name="userid">des bei HELIUM V angemeldeten API Benutzer</param>
        [Get("/api/v1/device/config")]
        Task<DeviceConfigEntry> GetConfigAsync([Query] string devicecnr, [Query] string devicetag, [Query] string devicetype, [Query] string userid);

        /// <summary>
        /// Ab&auml;ndern der benutzerdefinierten Ger&auml;tekonfiguration
        ///
        /// UpdateConfig (/api/v1/device/config)
        /// </summary>
        /// <param name="content">Die Daten der Ger&auml;tekonfigurationTeile der Konfiguration k&ouml;nnen nur durch den HELIUM V Anwenderabge&auml;ndert werden.</param>
        /// <param name="userid">des bei HELIUM V angemeldeten API Benutzer</param>
        [Put("/api/v1/device/config")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> UpdateConfigAsync([Body] DeviceConfigEntry content, [Query] string userid);

        /// <summary>
        /// Speichert Dokumente zu bestehenden Belegen ab, die &uuml;ber die Kategorie des Belegs undder Id oder Nummer des Belegs identifiziert werden. Es muss entweder die Id oder die Nummerdes Belegs &uuml;bermittelt werden.Die maximale Upload-Gr&ouml;&szlig;e ist derzeit auf 500KiB gestellt
        ///
        /// CreateDocument2 (/api/v1/document)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="category">ist die Kategorie des Belegs</param>
        /// <param name="cnr">ist die Nummer des Belegs (optional)</param>
        /// <param name="grouping">ist die Gruppierung (optional)</param>
        /// <param name="id">ist die Id des Belegs (optional)</param>
        /// <param name="keywords">sind die Schlagworte des Dokuments (optional)</param>
        /// <param name="securitylevel">ist die Sicherheitsstufe (optional)</param>
        /// <param name="type">ist die Belegart (optional)</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde</param>
        [Post("/api/v1/document")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> CreateDocument2Async([Body] MultipartBody content, [Query] string category, [Query] string cnr, [Query] string grouping, [Query] int? id, [Query] string keywords, [Query] long? securitylevel, [Query] string type, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Dokumente eines Belegs aus der Dokumentenablage ermitteln.Es werden alle Dokumente angef&uuml;hrt, die f&uuml;r der angemeldeten Benutzer sichtbar sind.Ein Element der Liste ent&auml;hlt aber nicht die tats&auml;chlichen Daten des Dokuments, sondernnur Metainformation. Der Beleg wird &uuml;ber die Id oder Nummer des Belegs identifiziert. Es muss entweder die Idoder die Nummer des Belegs &uuml;bermittelt werden.
        ///
        /// GetDocuments2 (/api/v1/document/list)
        /// </summary>
        /// <param name="category">ist die Kategorie des Belegs</param>
        /// <param name="cnr">ist die Nummer des Belegs (optional)</param>
        /// <param name="id">ist die Id des Belegs (optional)</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde</param>
        [Get("/api/v1/document/list")]
        Task<DocumentInfoEntryList> GetDocuments2Async([Query] string category, [Query] string cnr, [Query] int? id, [Query] string userid);

        /// <summary>
        /// GetCurrencies (/api/v1/finance/currency)
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/finance/currency")]
        Task<CurrencyEntryList> GetCurrenciesAsync([Query] int? limit, [Query] string startIndex, [Query] string userid);

        /// <summary>
        /// Liefert eine Liste aller Forecastlieferadressen
        ///
        /// GetDeliveryAddresses (/api/v1/forecast/deliveryaddress/list)
        /// </summary>
        /// <param name="filterDeliverable">mit true nur jene mit lieferbaren Positionen</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/forecast/deliveryaddress/list")]
        Task<ForecastDeliveryAddressEntryList> GetDeliveryAddressesAsync([Query(Name = "filter_deliverable")] bool? filterDeliverable, [Query] string userid);

        /// <summary>
        /// Liefert eine Liste aller Forecastpositionen zur gegebenen Lieferadresse
        ///
        /// GetPositionsByDeliveryAddress (/api/v1/forecast/deliveryaddress/{deliveryaddressid}/position/list)
        /// </summary>
        /// <param name="deliveryaddressid">Id der Forecastlieferadresse, gibt es die Forecastlieferadresse nicht, wird mit StatusCode NOT_FOUND (404) geantwortet</param>
        /// <param name="filterDeliverable">mit true nur lieferbare Positionen</param>
        /// <param name="filterPickingtype">der gew&uuml;nschte Kommissioniertyp</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/forecast/deliveryaddress/{deliveryaddressid}/position/list")]
        Task<ForecastPositionEntryList> GetPositionsByDeliveryAddressAsync([Path] int? deliveryaddressid, [Query(Name = "filter_deliverable")] bool? filterDeliverable, [Query(Name = "filter_pickingtype")] string filterPickingtype, [Query] string userid);

        /// <summary>
        /// Einen Linenabruf anhand seiner Id ermitteln
        ///
        /// FindLinecall (/api/v1/forecast/linecall/{linecallid})
        /// </summary>
        /// <param name="linecallid">Id des gesuchten Linienabrufs</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/forecast/linecall/{linecallid}")]
        Task<LinecallEntry> FindLinecallAsync([Path] int? linecallid, [Query] string userid);

        /// <summary>
        /// Erzeugt eine Ausgabe eines Artikels des LinienabrufsWurde die Produktion &uuml;ber die Resource /material/{positionid}/delivery noch nicht gestartet,wird mit StatusCode BAD_REQUEST (400) geantwortet
        ///
        /// CreateDelivery2 (/api/v1/forecast/linecall/{linecallid})
        /// </summary>
        /// <param name="content"></param>
        /// <param name="linecallid">Id des Linienabrufs, gibt es den Linienabruf nicht, wird mit StatusCode NOT_FOUND (404) geantwortet</param>
        [Post("/api/v1/forecast/linecall/{linecallid}")]
        [Header("Content-Type", "application/json")]
        Task<LinecallDeliveryResultEntry> CreateDelivery2Async([Body] LinecallDeliveryPostEntry content, [Path] int? linecallid);

        /// <summary>
        /// CreateDelivery3 (/api/v1/forecast/material/{positionid})
        /// </summary>
        /// <param name="positionid"></param>
        /// <param name="filterPickingtype"></param>
        /// <param name="userid"></param>
        [Post("/api/v1/forecast/material/{positionid}")]
        Task<Response<object>> CreateDelivery3Async([Path] int? positionid, [Query(Name = "filter_pickingtype")] string filterPickingtype, [Query] string userid);

        /// <summary>
        /// Liefert die zur Forecastposition zugeh&ouml;rigen Linienabrufe, mitsamt den zu liefernden Materialien
        ///
        /// GetDelivery (/api/v1/forecast/material/{positionid}/delivery)
        /// </summary>
        /// <param name="positionid">Id der Forecastposition, gibt es die Forecastposition nicht, wird mit StatusCode NOT_FOUND (404) geantwortet</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/forecast/material/{positionid}/delivery")]
        Task<LinecallEntryList> GetDeliveryAsync([Path] int? positionid, [Query] string userid);

        /// <summary>
        /// Startet die Produktion eines Positionsartikels
        ///
        /// StartDelivery (/api/v1/forecast/material/{positionid}/delivery)
        /// </summary>
        /// <param name="positionid">Id der Forecastposition, gibt es die Forecastposition nicht, wird mit StatusCode NOT_FOUND (404) geantwortet</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Post("/api/v1/forecast/material/{positionid}/delivery")]
        Task<ForecastPositionEntry> StartDeliveryAsync([Path] int? positionid, [Query] string userid);

        /// <summary>
        /// Liefert eine Liste aller definierten Drucker der Kommissionierung
        ///
        /// GetPickingPrinter (/api/v1/forecast/pickingprinter/list)
        /// </summary>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/forecast/pickingprinter/list")]
        Task<PickingPrinterEntryList> GetPickingPrinterAsync([Query] string userid);

        /// <summary>
        /// Liefert eine Liste aller Forecastpositionen
        ///
        /// GetPositions (/api/v1/forecast/position/list)
        /// </summary>
        /// <param name="filterDeliverable">mit true nur lieferbare Positionen</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/forecast/position/list")]
        Task<ForecastPositionEntryList> GetPositionsAsync([Query(Name = "filter_deliverable")] bool? filterDeliverable, [Query] string userid);

        /// <summary>
        /// Eine Forecastposition anhand seiner Id ermitteln
        ///
        /// FindForecastposition (/api/v1/forecast/position/{positionid})
        /// </summary>
        /// <param name="positionid">Id der gesuchten Forecastposition</param>
        /// <param name="filterDeliverable"></param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/forecast/position/{positionid}")]
        Task<ForecastPositionEntry> FindForecastpositionAsync([Path] int? positionid, [Query(Name = "filter_deliverable")] bool? filterDeliverable, [Query] string userid);

        /// <summary>
        /// Erzeugt eine Ausgabe eines Artikels der ForecastpositionWurde die Produktion &uuml;ber die Resource /material/{positionid}/delivery noch nicht gestartet,wird mit StatusCode BAD_REQUEST (400) geantwortet
        ///
        /// CreateForecastpositionDelivery (/api/v1/forecast/position/{positionid})
        /// </summary>
        /// <param name="content"></param>
        /// <param name="positionid">Id der Forecastposition, gibt es die Forecastposition nicht, wird mit StatusCode NOT_FOUND (404) geantwortet</param>
        [Post("/api/v1/forecast/position/{positionid}")]
        [Header("Content-Type", "application/json")]
        Task<ForecastPositionEntry> CreateForecastpositionDeliveryAsync([Body] LinecallDeliveryPostEntry content, [Path] int? positionid);

        /// <summary>
        /// Ein Versandetikett zu einer sich in Produktion befindlichen Forecastposition als JRPrint erhalten.
        ///
        /// PrintDispatchLabel2 (/api/v1/forecast/position/{positionid}/printdispatchlabel)
        /// </summary>
        /// <param name="positionid">Id der Forecastposition, gibt es die Forecastposition nicht, wird mit StatusCode NOT_FOUND (404) geantwortet</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/forecast/position/{positionid}/printdispatchlabel")]
        Task<Response<object>> PrintDispatchLabel2Async([Path] int? positionid, [Query] string userid);

        /// <summary>
        /// LogonHvma (/api/v1/hvma/logon)
        /// </summary>
        /// <param name="content"></param>
        [Post("/api/v1/hvma/logon")]
        [Header("Content-Type", "application/json")]
        Task<LoggedOnHvmaEntry> LogonHvmaAsync([Body] HvmaLogonEntry content);

        /// <summary>
        /// GetHvmaParams (/api/v1/hvma/param)
        /// </summary>
        /// <param name="hvtoken"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/hvma/param")]
        Task<HvmaParamEntryList> GetHvmaParamsAsync( [Query] string userid);

        /// <summary>
        /// Die Liste der verf&uuml;gbaren HVMARechte ausgeben
        ///
        /// GetHvmaRechte (/api/v1/hvma/privilege)
        /// </summary>
        /// <param name="hvtoken"></param>
        /// <param name="licenceCnr">ist optional. Ist der Parameter angegeben, werden dieim System verf&uuml;gbaren HVMA-Rechte f&uuml;r die angegebene Lizenzermittelt. Ist der Parameter nicht angegeben, werden die HVMA-Rechte desangemeldeten HVMA-Benutzers ausgegeben.</param>
        /// <param name="userid">ist der angemeldete Benutzer</param>
        [Get("/api/v1/hvma/privilege")]
        Task<HvmarechtEnumEntryList> GetHvmaRechteAsync( [Query] string licenceCnr, [Query] string userid);

        /// <summary>
        /// GetOpenInventories (/api/v1/inventory)
        /// </summary>
        /// <param name="userid"></param>
        [Get("/api/v1/inventory")]
        Task<List<InventoryEntry>> GetOpenInventoriesAsync([Query] string userid);

        /// <summary>
        /// CreateInventoryDataEntry (/api/v1/inventory/{inventoryid}/entry)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="inventoryid"></param>
        /// <param name="largeDifference"></param>
        /// <param name="userid"></param>
        [Post("/api/v1/inventory/{inventoryid}/entry")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> CreateInventoryDataEntryAsync([Body] InventoryDataEntry content, [Path] int? inventoryid, [Query] bool? largeDifference, [Query] string userid);

        /// <summary>
        /// UpdateInventoryDataEntry (/api/v1/inventory/{inventoryid}/entry)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="inventoryid"></param>
        /// <param name="changeAmountTo"></param>
        /// <param name="userid"></param>
        [Put("/api/v1/inventory/{inventoryid}/entry")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> UpdateInventoryDataEntryAsync([Body] InventoryDataEntry content, [Path] int? inventoryid, [Query] bool? changeAmountTo, [Query] string userid);

        /// <summary>
        /// Einen Eintrag in der Inventurliste erzeugen
        ///
        /// CreateInventoryEntry (/api/v1/inventory/{inventoryid}/entry/{itemid}/{amount})
        /// </summary>
        /// <param name="inventoryid">die Id der Inventur</param>
        /// <param name="itemid">die Id des Artikels</param>
        /// <param name="amount">die Menge</param>
        /// <param name="identity">die (optionale) Serien/Chargennummer sofern es ein entsprechender Artikel ist</param>
        /// <param name="largeDifference">ist true wenn gro&szlig;e Mengenabwechungen erlaubt sind</param>
        /// <param name="userid">der Token des aktuellen Benutzers</param>
        [Post("/api/v1/inventory/{inventoryid}/entry/{itemid}/{amount}")]
        Task<Response<object>> CreateInventoryEntryAsync([Path] int? inventoryid, [Path] int? itemid, [Path] string amount, [Query] string identity, [Query] bool? largeDifference, [Query] string userid);

        /// <summary>
        /// UpdateInventoryEntry (/api/v1/inventory/{inventoryid}/entry/{itemid}/{amount})
        /// </summary>
        /// <param name="inventoryid"></param>
        /// <param name="itemid"></param>
        /// <param name="amount"></param>
        /// <param name="changeAmountTo"></param>
        /// <param name="identity"></param>
        /// <param name="userid"></param>
        [Put("/api/v1/inventory/{inventoryid}/entry/{itemid}/{amount}")]
        Task<Response<object>> UpdateInventoryEntryAsync([Path] int? inventoryid, [Path] int? itemid, [Path] string amount, [Query] bool? changeAmountTo, [Query] string identity, [Query] string userid);

        /// <summary>
        /// Eine Liste aller - den Filterkriterien entsprechenden - Rechnungen ermitteln.
        ///
        /// GetInvoices (/api/v1/invoice)
        /// </summary>
        /// <param name="addCustomerDetail">mit true wird zus&auml;tzlich der CustomerDetailEntry des Kundenermittelt und ausgegeben</param>
        /// <param name="filterCnr">schr&auml;nkt die Ausgabe auf Rechnungen ein, die diesen Teil der Rechnungsnummer beinhalten</param>
        /// <param name="filterCustomer">schr&auml;nkt die Ausgabe auf Rechnungen ein, die f&uuml;r diesen Kundennamen ausgestellt wurden</param>
        /// <param name="filterOpen">mit true werden nur offene Rechnungen geliefert. Ohne Angabe bzw. mit falsewerden alle Rechnungen ausgegeben</param>
        /// <param name="filterProject">nur Rechnungen die diese Projekt- bzw. Bestellnummer beinhalten werden ausgegeben</param>
        /// <param name="filterStatisticsaddress">Einschr&auml;nkung auf die Statistikadresse</param>
        /// <param name="filterTextsearch">Rechnungen (und deren Positionen) die diesen Text beinhalten werden ausgegeben</param>
        /// <param name="limit">limit die Anzahl der zu liefernden Datens&auml;tze. Default sind 50</param>
        /// <param name="startIndex">startIndex der x-te Eintrag ab dem die Datens&auml;tze geliefert werden sollen</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde</param>
        [Get("/api/v1/invoice")]
        Task<InvoiceEntryList> GetInvoicesAsync([Query] bool? addCustomerDetail, [Query(Name = "filter_cnr")] string filterCnr, [Query(Name = "filter_customer")] string filterCustomer, [Query(Name = "filter_open")] bool? filterOpen, [Query(Name = "filter_project")] string filterProject, [Query(Name = "filter_statisticsaddress")] string filterStatisticsaddress, [Query(Name = "filter_textsearch")] string filterTextsearch, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Erstellt eine Zahlung f&uuml;r die angegebene Rechnung
        ///
        /// CreateCashPayment (/api/v1/invoice/{invoiceid}/cashpayment)
        /// </summary>
        /// <param name="content">Properties f&uuml;r die ZahlungDie Property cnr muss mit der eindeutigen Kassa-Nummer bef&uuml;llt werdenF&uuml;r eine erfolgreiche Zahlung m&uuml;ssen der Betrag #getAmount(),die Zahlungsart #getPaymentType() und die Kassanummer #getCnr()gesetzt werden.</param>
        /// <param name="invoiceid">ist die Id der Rechnung</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde</param>
        [Post("/api/v1/invoice/{invoiceid}/cashpayment")]
        [Header("Content-Type", "application/json")]
        Task<int> CreateCashPaymentAsync([Body] InvoiceCashPaymentPostEntry content, [Path] int? invoiceid, [Query] string userid);

        /// <summary>
        /// Speichert einen (zus&auml;tzlichen) Zahlungsbeleg zu einer bestehenden Zahlung ab.Die maximale Upload-Gr&ouml;&szlig;e ist derzeit auf 500KiB gestellt
        ///
        /// AttachDocumentToCashPayment (/api/v1/invoice/{invoiceid}/cashpayment/{paymentid})
        /// </summary>
        /// <param name="content"></param>
        /// <param name="invoiceid">ist die Id der Rechnung</param>
        /// <param name="paymentid">ist die Id der Zahlung</param>
        /// <param name="cnr">ist die eindeutige Nummer der Kassa</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde</param>
        [Post("/api/v1/invoice/{invoiceid}/cashpayment/{paymentid}")]
        [Header("Content-Type", "application/json")]
        Task<int> AttachDocumentToCashPaymentAsync([Body] MultipartBody content, [Path] int? invoiceid, [Path] int? paymentid, [Query] string cnr, [Query] string userid);

        /// <summary>
        /// L&ouml;scht eine zuvor durchgef&uuml;hrte ZahlungWird der Zahlbetrag f&uuml;r das angegebene Zahldatum f&uuml;r die angegebene Id derRechnung nicht gefunden, wird der Statuscode 404 zur&uuml;ckgeliefert. Konnte die Zahlungstorniert werden, wird 204 geantwortet.
        ///
        /// RemoveCashPayment (/api/v1/invoice/{invoiceid}/cashpayment/{year}/{month}/{day})
        /// </summary>
        /// <param name="invoiceid">ist die Id der Rechnung</param>
        /// <param name="year">das Jahr der Zahlung (beispielsweise: 2016)</param>
        /// <param name="month">das Monat der Zahlung (beginnend bei 1, beispielsweise: 7 f&uumlr Juli)</param>
        /// <param name="day">der Tag der Zahlung (beginnend bei 1, beispielsweise: 13 f&uuml;r den 13ten)</param>
        /// <param name="amount">der zu l&ouml;schende Zahlbetrag</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde</param>
        [Delete("/api/v1/invoice/{invoiceid}/cashpayment/{year}/{month}/{day}")]
        Task<Response<object>> RemoveCashPaymentAsync([Path] int? invoiceid, [Path] int? year, [Path] int? month, [Path] int? day, [Query] string amount, [Query] string userid);

        /// <summary>
        /// Einen Artikel anhand seiner Artikelnummer ermitteln
        ///
        /// FindItemByAttributes (/api/v1/item)
        /// </summary>
        /// <param name="addComments">(optional) mit true die Artikelkommentar der Art text/html ebenfalls liefern</param>
        /// <param name="addStockAmountInfos">(optional) mit true die allgemeinen Lagerstandsinformationen liefern</param>
        /// <param name="itemCnr">(optional) die gesuchte Artikelnummer</param>
        /// <param name="itemSerialnumber">(optional) die Seriennummer des ArtikelsEineindeutige Artikel k&ouml;nnen &uuml;ber ihre Seriennummer ermittelt werden. Dabei wirdzuerst im aktuellen Lagerstand gesucht, danach in den Abgangsbuchungen. Ist die cnrebenfalls angegeben, muss der Artikel der &uuml;ber die Seriennummer ermittelt wurde mit derangegebenen Artikelnummer &uuml;bereinstimmen.</param>
        /// <param name="userid">des bei HELIUM V angemeldeten API Benutzer</param>
        [Get("/api/v1/item")]
        Task<ItemEntry> FindItemByAttributesAsync([Query] bool? addComments, [Query] bool? addStockAmountInfos, [Query] string itemCnr, [Query] string itemSerialnumber, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Artikelgruppen ermitteln.
        ///
        /// GetItemGroups (/api/v1/item/groups)
        /// </summary>
        /// <param name="userid">der angemeldete HELIUM V Benutzer</param>
        [Get("/api/v1/item/groups")]
        Task<ItemGroupEntryList> GetItemGroupsAsync([Query] string userid);

        /// <summary>
        /// Eine Liste aller Artikel ermitteln.Das Ergebnis kann dabei durch Filter eingeschr&auml;nkt werden
        ///
        /// GetItems (/api/v1/item/list)
        /// </summary>
        /// <param name="filterCnr">die (optionale) Artikelnummer nach der die Suche eingeschr&auml;nkt werden soll</param>
        /// <param name="filterDeliverycnr">auf die (optionale) Lieferantennr. bzw Bezeichnung einschr&auml;nken</param>
        /// <param name="filterItemgroupclass">auf die (optionale) Artikelgruppe bzw. Artikelklasse einschr&auml;nken</param>
        /// <param name="filterItemreferencenr">auf die (optionale) Artikelreferenznummer einschr&auml;nken</param>
        /// <param name="filterTextsearch">der (optionale) Text der die Suche einschr&auml;nkt</param>
        /// <param name="filterWithHidden">mit true werden auch versteckte Artikel in die Suche einbezogen</param>
        /// <param name="limit">die maximale Anzahl von zur&uuml;ckgelieferten Datens&auml;tzen</param>
        /// <param name="startIndex">die Index-Nummer desjenigen Satzes mit dem begonnen werden soll</param>
        /// <param name="userid">des angemeldeten HELIUM V Benutzer</param>
        [Get("/api/v1/item/list")]
        Task<List<ItemEntry>> GetItemsAsync([Query(Name = "filter_cnr")] string filterCnr, [Query(Name = "filter_deliverycnr")] string filterDeliverycnr, [Query(Name = "filter_itemgroupclass")] string filterItemgroupclass, [Query(Name = "filter_itemreferencenr")] string filterItemreferencenr, [Query(Name = "filter_textsearch")] string filterTextsearch, [Query(Name = "filter_withHidden")] bool? filterWithHidden, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Artikeleigenschaften eines Artikels ermitteln
        ///
        /// GetItemProperties (/api/v1/item/properties)
        /// </summary>
        /// <param name="itemCnr">die gew&uuml;nschte Artikelnummer</param>
        /// <param name="userid">userId der angemeldete HELIUM V Benutzer</param>
        [Get("/api/v1/item/properties")]
        Task<ItemPropertyEntryList> GetItemPropertiesAsync([Query] string itemCnr, [Query] string userid);

        /// <summary>
        /// Liefert eine Liste aller Lagerst&auml;nde dieses ArtikelsEs werden nur Lager geliefert, die einen Lagerstand > 0 haben. Es werden nur jeneLagerst&auml;nde geliefert, f&uuml;r die der Benutzer das Recht hat das jeweilige Lager zu benutzen.
        ///
        /// GetStockAmount (/api/v1/item/stocks)
        /// </summary>
        /// <param name="itemCnr">die gesuchte Artikelnummer</param>
        /// <param name="returnItemInfo">mit true werden neben den Lagerst&auml;nden auch die Daten desbetreffenden Artikels zur&uuml;ckgeliefert.</param>
        /// <param name="userid">der angemeldete API Benutzer</param>
        [Get("/api/v1/item/stocks")]
        Task<List<StockAmountEntry>> GetStockAmountAsync([Query] string itemCnr, [Query] bool? returnItemInfo, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Artikeleigenschaften eines Artikels ermitteln.
        ///
        /// GetItemPropertiesFromId (/api/v1/item/{itemid}/properties)
        /// </summary>
        /// <param name="itemid">die Id des gew&uuml;nschten Artikels</param>
        /// <param name="userid">userId der angemeldete HELIUM V Benutzer</param>
        [Get("/api/v1/item/{itemid}/properties")]
        Task<ItemPropertyEntryList> GetItemPropertiesFromIdAsync([Path] int? itemid, [Query] string userid);

        /// <summary>
        /// Erm&ouml;glicht das Anmelden.
        ///
        /// Logon (/api/v1/logon)
        /// </summary>
        /// <param name="content"></param>
        [Post("/api/v1/logon")]
        [Header("Content-Type", "application/json")]
        Task<LoggedOnEntry> LogonAsync([Body] LogonEntry content);

        /// <summary>
        /// LogonExternal (/api/v1/logonapp)
        /// </summary>
        /// <param name="content"></param>
        [Post("/api/v1/logonapp")]
        [Header("Content-Type", "application/json")]
        Task<LoggedOnTenantEntry> LogonExternalAsync([Body] LogonTenantEntry content);

        /// <summary>
        /// Erm&ouml;glicht das Abmelden &uuml;ber die Ausweisnummer eines Personals.
        ///
        /// LogonIdCard (/api/v1/logonidcard/{idcard})
        /// </summary>
        /// <param name="content"></param>
        /// <param name="idcard">Ausweisnummer des anzumeldenden Personals</param>
        [Post("/api/v1/logonidcard/{idcard}")]
        [Header("Content-Type", "application/json")]
        Task<LoggedOnEntry> LogonIdCardAsync([Body] LogonIdCardEntry content, [Path] string idcard);

        /// <summary>
        /// Erm&ouml;glicht das Abmelden.
        ///
        /// Logout (/api/v1/logout)
        /// </summary>
        /// <param name="userid">wurde zuvor von einem "logon" ermittelt.</param>
        [Get("/api/v1/logout")]
        Task<Response<object>> LogoutAsync([Query] string userid);

        /// <summary>
        /// Erm&ouml;glicht das Abmelden.
        ///
        /// LogoutPathParam (/api/v1/logout/{token})
        /// </summary>
        /// <param name="token">wurde zuvor von einem "logon" ermittelt.</param>
        [Get("/api/v1/logout/{token}")]
        Task<Response<object>> LogoutPathParamAsync([Path] string token);

        /// <summary>
        /// Eine Liste aller verf&uuml;gbaren Maschinen
        ///
        /// GetMachines (/api/v1/machine)
        /// </summary>
        /// <param name="filterPlanningview">mit TRUE nur jene Maschinen liefern, die sich in einerMaschinengruppe befinden, die in der Planungsanzeige dargestellt werden soll</param>
        /// <param name="filterProductiongroupid"></param>
        /// <param name="filterStaffid">optional die Personal-Id jener Person, die eine laufende Maschinezuletzt gestartet hat</param>
        /// <param name="filterWithHidden"></param>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/machine")]
        Task<MachineEntryList> GetMachinesAsync([Query(Name = "filter_planningview")] bool? filterPlanningview, [Query(Name = "filter_productiongroupid")] int? filterProductiongroupid, [Query(Name = "filter_staffid")] int? filterStaffid, [Query(Name = "filter_withHidden")] bool? filterWithHidden, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Maschinengruppen
        ///
        /// GetMachineGroups (/api/v1/machine/groups)
        /// </summary>
        /// <param name="filterPlanningview">mit TRUE nur jene Maschinen liefern, die sich in einerMaschinengruppe befinden, die in der Planungsanzeige dargestellt werden soll</param>
        /// <param name="filterProductiongroupid"></param>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/machine/groups")]
        Task<MachineGroupEntryList> GetMachineGroupsAsync([Query(Name = "filter_planningview")] bool? filterPlanningview, [Query(Name = "filter_productiongroupid")] int? filterProductiongroupid, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// GetPlanningView (/api/v1/machine/planningview)
        /// </summary>
        /// <param name="dateMs"></param>
        /// <param name="days"></param>
        /// <param name="filterProductiongroupid"></param>
        /// <param name="filterStartdate"></param>
        /// <param name="filterWithHidden"></param>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        /// <param name="userid"></param>
        /// <param name="withDescription"></param>
        [Get("/api/v1/machine/planningview")]
        Task<PlanningView> GetPlanningViewAsync([Query] long? dateMs, [Query] int? days, [Query(Name = "filter_productiongroupid")] int? filterProductiongroupid, [Query(Name = "filter_startdate")] bool? filterStartdate, [Query(Name = "filter_withHidden")] bool? filterWithHidden, [Query] int? limit, [Query] int? startIndex, [Query] string userid, [Query(Name = "with_description")] bool? withDescription);

        /// <summary>
        /// GetAvaibilities (/api/v1/machine/{machineid}/availability)
        /// </summary>
        /// <param name="machineid"></param>
        /// <param name="dateMs"></param>
        /// <param name="days"></param>
        /// <param name="userid"></param>
        /// <param name="withDescription"></param>
        [Get("/api/v1/machine/{machineid}/availability")]
        Task<MachineAvailabilityEntryList> GetAvaibilitiesAsync([Path] int? machineid, [Query] long? dateMs, [Query] int? days, [Query] string userid, [Query(Name = "with_description")] bool? withDescription);

        /// <summary>
        /// Eine Liste aller Auftr&auml;ge liefern, die den Filterkriterien entsprechen
        ///
        /// GetOrders (/api/v1/order)
        /// </summary>
        /// <param name="filterCnr">die zu filternde Auftragsnummer</param>
        /// <param name="filterCustomer">der zu filternde Kundenname</param>
        /// <param name="filterMyopen"></param>
        /// <param name="filterProject">der zu filternde Projektname des Auftrags</param>
        /// <param name="filterRepresentativeSign">sofern angegeben, das/die zu filternde Kurzzeichen des Vertreters.Es k&ouml;nnen mehrere Kurzzeichen angegeben werden, wenn diese durch , (Komma) voneinandergetrennt werden. Beispiel: abc oder ab,cd</param>
        /// <param name="filterWithHidden">mit true werden auch versteckte Auftr&auml;ge geliefert</param>
        /// <param name="limit">die maximale Anzahl von Eintr&auml;gen die ermittelt werden sollen</param>
        /// <param name="startIndex">der StartIndex</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/order")]
        Task<List<OrderEntry>> GetOrdersAsync([Query(Name = "filter_cnr")] string filterCnr, [Query(Name = "filter_customer")] string filterCustomer, [Query(Name = "filter_myopen")] bool? filterMyopen, [Query(Name = "filter_project")] string filterProject, [Query(Name = "filter_representativeSign")] string filterRepresentativeSign, [Query(Name = "filter_withHidden")] bool? filterWithHidden, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Die Auftragskommentare ermittelnEiner der Parameter orderId oder orderCnr muss angegeben werden.
        ///
        /// GetOrderComments (/api/v1/order/comments)
        /// </summary>
        /// <param name="hvtoken">ist der beim Logon ermittelte Token</param>
        /// <param name="addExternalComment">wenn true oder nicht angegeben wird der externe Kommentar ermittelt</param>
        /// <param name="addInternalComment">wenn true oder nicht angegeben wird der interne Kommentar ermittelt</param>
        /// <param name="orderCnr">ist die (optionale) Auftragsnummer</param>
        /// <param name="orderid">ist die (optionale) Id des Auftrags</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/order/comments")]
        Task<OrderComments> GetOrderCommentsAsync( [Query] bool? addExternalComment, [Query] bool? addInternalComment, [Query] string orderCnr, [Query] int? orderid, [Query] string userid);

        /// <summary>
        /// Ermittelt den "Lieferstatus" eines Auftrags
        ///
        /// GetDeliverableOrderStatus (/api/v1/order/deliverable)
        /// </summary>
        /// <param name="hvtoken">ist der beim Logon ermittelte Token</param>
        /// <param name="orderCnr">ist die (optionale) Auftragsnummer.</param>
        /// <param name="orderid">ist die (optionale) Id des Auftrags. Ist auch die orderCnr angegeben, hat die orderId Priorit&auml;t</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/order/deliverable")]
        Task<DeliverableOrderEntry> GetDeliverableOrderStatusAsync( [Query] string orderCnr, [Query] int? orderid, [Query] string userid);

        /// <summary>
        /// Liefert die Auftragsdaten f&uuml;r eine sp&auml;tere Offline-VerarbeitungEs werden s&auml;mtliche den Kritieren entsprechenden Auftr&auml;ge geliefert.Zus&auml;tzlich die Positionen f&uuml;r dieser Auftr&auml;ge und die unterschiedlichenLieferadressen.
        ///
        /// GetOfflineOrders (/api/v1/order/offline)
        /// </summary>
        /// <param name="hvtoken">ist der beim Logon ermittelte Token. (optional)</param>
        /// <param name="filterCnr">die zu filternde Auftragsnummer</param>
        /// <param name="filterCustomer">der zu filternde Kundenname des Auftraggebers</param>
        /// <param name="filterDeliveryCustomer">der zu filternde Kundename des Empf&auml;ngers</param>
        /// <param name="filterProject">der zu filternde Projektname des Auftrags</param>
        /// <param name="filterRepresentativeSign">sofern angegeben, das/die zu filternde Kurzzeichen des Vertreters.Es k&ouml;nnen mehrere Kurzzeichen angegeben werden, wenn diese durch , (Komma) voneinandergetrennt werden. Beispiel: abc oder ab,cd</param>
        /// <param name="filterWithHidden">mit true werden auch versteckte Auftr&auml;ge geliefert</param>
        /// <param name="limit">die maximale Anzahl von zu liefernden Auftr&auml;gen</param>
        /// <param name="startIndex">der Startindex der Auftr&auml;ge</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/order/offline")]
        Task<OfflineOrderEntry> GetOfflineOrdersAsync( [Query(Name = "filter_cnr")] string filterCnr, [Query(Name = "filter_customer")] string filterCustomer, [Query(Name = "filter_delivery_customer")] string filterDeliveryCustomer, [Query(Name = "filter_project")] string filterProject, [Query(Name = "filter_representativeSign")] string filterRepresentativeSign, [Query(Name = "filter_withHidden")] bool? filterWithHidden, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Positionen die den Filterkriterien f&uuml;r Auftr&auml;ge entsprechen ermitteln
        ///
        /// GetOrderPositions (/api/v1/order/position)
        /// </summary>
        /// <param name="filterCnr">die zu filternde Auftragsnummer</param>
        /// <param name="filterCustomer">der zu filternde Kundenname</param>
        /// <param name="filterMyopen"></param>
        /// <param name="filterProject">der zu filternde Projektname des Auftrags</param>
        /// <param name="filterRepresentativeSign">sofern angegeben, das/die zu filternde Kurzzeichen des Vertreters.Es k&ouml;nnen mehrere Kurzzeichen angegeben werden, wenn diese durch , (Komma) voneinandergetrennt werden. Beispiel: abc oder ab,cd</param>
        /// <param name="filterWithHidden">mit true werden auch versteckte Auftr&auml;ge geliefert</param>
        /// <param name="limit">die maximale Anzahl von zu liefernden Auftr&auml;gen</param>
        /// <param name="startIndex">der Startindex der Auftr&auml;ge</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/order/position")]
        Task<List<OrderpositionsEntry>> GetOrderPositionsAsync([Query(Name = "filter_cnr")] string filterCnr, [Query(Name = "filter_customer")] string filterCustomer, [Query(Name = "filter_myopen")] bool? filterMyopen, [Query(Name = "filter_project")] string filterProject, [Query(Name = "filter_representativeSign")] string filterRepresentativeSign, [Query(Name = "filter_withHidden")] bool? filterWithHidden, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Die angegebene Auftragsposition l&ouml;schenDer Auftrag muss im Status ANGELEGT sein.
        ///
        /// RemovePosition2 (/api/v1/order/position/{positionid})
        /// </summary>
        /// <param name="positionid">die zu entfernende AuftragspositionId</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Delete("/api/v1/order/position/{positionid}")]
        Task<Response<object>> RemovePosition2Async([Path] int? positionid, [Query] string userid);

        /// <summary>
        /// Einen Auftrag aktivierenDer Auftrag wird vollst&auml;ndig durchgerechnet und derStatus des Auftrags auf OFFEN gestellt, sofern dies m&ouml;glich ist.Der Auftrag muss im Status ANGELEGT sein. Es wird das AUFTRAG_CUDRecht ben&ouml;tigt.Bitte stellen Sie sicher, dass der Auftrag alle jene Informationenenth&auml;lt, die bei einem etwaigen Druck der Auftragsbest&auml;tigungnotwendig sind. Zum Beispiel Preise in den Positionen, Steuers&auml;tze,Konditionen usw.Beim Aktivieren wird eine Belegpr&uuml;fung durchgef&uuml;hrt. Je nachgesetzten Parametern kann es verschiedenen Situation geben (Nullpreisein der Position erlaubt, steuerlose Positionen obwohl Steuer erwartetwird, steuerbehaftete Position obwohl steuerlose Positionen erwartetwerden, ...) die zum Abbruch der Aktivierung f&uuml;hren, oderEintr&auml;ge im OrderActivationEntry hinterlassen.
        ///
        /// PostActivateOrder (/api/v1/order/{orderid}/activate)
        /// </summary>
        /// <param name="orderid">ist die Id des zu aktivierenden Auftrags</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        /// <param name="verified">muss true sein um die Funktion ausf&uuml;hren zu k&ouml;nnen</param>
        [Post("/api/v1/order/{orderid}/activate")]
        Task<OrderActivationEntry> PostActivateOrderAsync([Path] int? orderid, [Query] string userid, [Query] bool? verified);

        /// <summary>
        /// Legt Dateien in die Dokumentenablage eines Auftrags ab.Die maximale Upload-Gr&ouml;&szlig;e ist derzeit auf 500KiB gestellt
        ///
        /// CreateDocument3 (/api/v1/order/{orderid}/document)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="orderid">Id des Auftrags</param>
        /// <param name="grouping">ist die Gruppierung (optional)</param>
        /// <param name="keywords">sind die Schlagworte des Dokuments (optional)</param>
        /// <param name="securitylevel">ist die Sicherheitsstufe (optional)</param>
        /// <param name="type">ist die Belegart (optional)</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Post("/api/v1/order/{orderid}/document")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> CreateDocument3Async([Body] MultipartBody content, [Path] int? orderid, [Query] string grouping, [Query] string keywords, [Query] long? securitylevel, [Query] string type, [Query] string userid);

        /// <summary>
        /// Eine Artikel-Position zu einem Auftrag hinzuf&uuml;genDer Auftrag muss im Status ANGELEGT sein.
        ///
        /// CreateIdentPosition2 (/api/v1/order/{orderid}/itemposition)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="orderid">ist die Id des Auftrags</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Post("/api/v1/order/{orderid}/itemposition")]
        [Header("Content-Type", "application/json")]
        Task<CreatedOrderPositionEntry> CreateIdentPosition2Async([Body] CreateOrderItemPositionEntry content, [Path] int? orderid, [Query] string userid);

        /// <summary>
        /// &Auml;ndert die angegebene Auftragsposition auf eine IdentpositionDer Auftrag muss im Status ANGELEGT sein.Unterscheiden sich die Positionsarten der bereits bestehendenPosition und der abzu&auml;ndernden, so wird prinzipiell eineneue Position angelegt.Auch wenn es sich bei der bestehenden Position um eineArtikelposition handelt, kann nicht davon ausgegangen werden,dass die Id der neuen Position die gleiche bleibt. Wird mit derId der Position weitergearbeitet, ist die im Response enthaltendeorderPositionId zu verwenden.
        ///
        /// UpdateIdentPosition (/api/v1/order/{orderid}/itemposition/{positionid})
        /// </summary>
        /// <param name="content"></param>
        /// <param name="orderid">ist die Id des Auftrags</param>
        /// <param name="positionid">ist die abzu&auml;ndernde Auftragsposition</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Put("/api/v1/order/{orderid}/itemposition/{positionid}")]
        [Header("Content-Type", "application/json")]
        Task<CreatedOrderPositionEntry> UpdateIdentPositionAsync([Body] CreateOrderItemPositionEntry content, [Path] int? orderid, [Path] int? positionid, [Query] string userid);

        /// <summary>
        /// Eine Handeingabe-Position zu einem Auftrag hinzuf&uuml;genDer Auftrag muss im Status ANGELEGT sein.
        ///
        /// CreateManualIdentPosition (/api/v1/order/{orderid}/manualitemposition)
        /// </summary>
        /// <param name="content">Die Datenstruktur zum Anlegen einer Handeingabe-Position</param>
        /// <param name="orderid">ist die Id des Auftrags</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Post("/api/v1/order/{orderid}/manualitemposition")]
        [Header("Content-Type", "application/json")]
        Task<CreatedOrderPositionEntry> CreateManualIdentPositionAsync([Body] CreateOrderManualItemPositionEntry content, [Path] int? orderid, [Query] string userid);

        /// <summary>
        /// &Auml;ndert die angegebene Auftragsposition auf eine HandeingabepositionDer Auftrag muss im Status ANGELEGT sein.Unterscheiden sich die Positionsarten der bereits bestehendenPosition und der abzu&auml;ndernden, so wird prinzipiell eineneue Position angelegt.Auch wenn es sich bei der bestehenden Position um eineHandeingabeposition handelt, kann nicht davon ausgegangen werden,dass die Id der neuen Position die gleiche bleibt. Wird mit derId der Position weitergearbeitet, ist die im Response enthaltendeorderPositionId zu verwenden.
        ///
        /// UpdateManualItemPosition (/api/v1/order/{orderid}/manualitemposition/{positionid})
        /// </summary>
        /// <param name="content">Die Datenstruktur zum Anlegen einer Handeingabe-Position</param>
        /// <param name="orderid">ist die Id des Auftrags</param>
        /// <param name="positionid">ist die abzu&auml;ndernde Auftragsposition</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Put("/api/v1/order/{orderid}/manualitemposition/{positionid}")]
        [Header("Content-Type", "application/json")]
        Task<CreatedOrderPositionEntry> UpdateManualItemPositionAsync([Body] CreateOrderManualItemPositionEntry content, [Path] int? orderid, [Path] int? positionid, [Query] string userid);

        /// <summary>
        /// Die Liste der Positionen f&uuml;r den angegeben Auftrag ermitteln
        ///
        /// GetPositions2 (/api/v1/order/{orderid}/position)
        /// </summary>
        /// <param name="orderid">ist die Auftrag-Id f&uuml;r die die Positionen ermittelt werden sollen</param>
        /// <param name="limit">ist die maximale Anzahl von zu liefernden Positionsdaten. Default 50</param>
        /// <param name="startIndex">ist der Index ab dem die Positionen geliefert werden sollen</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/order/{orderid}/position")]
        Task<List<OrderpositionEntry>> GetPositions2Async([Path] int? orderid, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// GetSettlementOfHours (/api/v1/order/{orderid}/settlementofhours)
        /// </summary>
        /// <param name="orderid"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/order/{orderid}/settlementofhours")]
        Task<SettlementOfHoursEntry> GetSettlementOfHoursAsync([Path] int? orderid, [Query] string userid);

        /// <summary>
        /// PostSettlementOfHoursSignature (/api/v1/order/{orderid}/settlementofhours)
        /// </summary>
        /// <param name="content"></param>
        [Post("/api/v1/order/{orderid}/settlementofhours")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> PostSettlementOfHoursSignatureAsync([Body] SettlementOfHoursSignatureEntry content, [Path] int orderid);

        /// <summary>
        /// Die Positionen eines Auftrags aufsteigend nach Artikelnummer sortierenDer Auftrag muss im Status ANGELEGT sein.
        ///
        /// PostSortOrderByItemnumber (/api/v1/order/{orderid}/sortbyitemnumber)
        /// </summary>
        /// <param name="orderid">ist die Id des zu sortierenden Auftrags</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Post("/api/v1/order/{orderid}/sortbyitemnumber")]
        Task<OrderSortedEntry> PostSortOrderByItemnumberAsync([Path] int? orderid, [Query] string userid);

        /// <summary>
        /// Eine Textbaustein-Position zu einem Auftrag hinzuf&uuml;genDer Auftrag muss im Status ANGELEGT sein.Es k&ouml;nnen auch Textbausteine verwendet werden, die BilderenthaltenBei auf Texten basierende Textbausteinen kann die localeCnrangegeben werden. Wird sie nicht angegeben, wird der Textbaustein mitder Locale des angemeldeten Benutzers verwendet. Der Textbaustein mussin dieser Locale existieren.
        ///
        /// CreateTextblockPosition (/api/v1/order/{orderid}/textblock)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="orderid">ist die Id des Auftrags</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Post("/api/v1/order/{orderid}/textblock")]
        [Header("Content-Type", "application/json")]
        Task<CreatedOrderPositionEntry> CreateTextblockPositionAsync([Body] CreateOrderTextblockPositionEntry content, [Path] int? orderid, [Query] string userid);

        /// <summary>
        /// Eine Texteingabe-Position zu einem Auftrag hinzuf&uuml;genDer Auftrag muss im Status ANGELEGT sein.
        ///
        /// CreateTextPosition2 (/api/v1/order/{orderid}/textposition)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="orderid">ist die Id des Auftrags</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Post("/api/v1/order/{orderid}/textposition")]
        [Header("Content-Type", "application/json")]
        Task<CreatedOrderPositionEntry> CreateTextPosition2Async([Body] CreateOrderTextPositionEntry content, [Path] int? orderid, [Query] string userid);

        /// <summary>
        /// GetPartlists (/api/v1/partlist/list)
        /// </summary>
        /// <param name="filterCnr"></param>
        /// <param name="filterTextsearch"></param>
        /// <param name="filterWithHidden"></param>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/partlist/list")]
        Task<PartlistEntryList> GetPartlistsAsync([Query(Name = "filter_cnr")] string filterCnr, [Query(Name = "filter_textsearch")] string filterTextsearch, [Query(Name = "filter_withHidden")] bool? filterWithHidden, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Die Liste aller Montagearten
        ///
        /// GetMountingMethods (/api/v1/partlist/mountingmethod)
        /// </summary>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/partlist/mountingmethod")]
        Task<MountingMethodEntryList> GetMountingMethodsAsync([Query] string userid);

        /// <summary>
        /// Die Liste aller Fertigungsgruppen
        ///
        /// GetProductionGroups (/api/v1/partlist/productiongroup)
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/partlist/productiongroup")]
        Task<ProductionGroupEntryList> GetProductionGroupsAsync([Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// GetBrowsePositions (/api/v1/partlist/{partlistid}/browseposition)
        /// </summary>
        /// <param name="partlistid"></param>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/partlist/{partlistid}/browseposition")]
        Task<PartlistPositionEntryList> GetBrowsePositionsAsync([Path] int? partlistid, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Eine Email eines Kunden zu einer Stueckliste erstellen
        ///
        /// SendEmail (/api/v1/partlist/{partlistid}/email)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="partlistid">die betreffende St&uuml;ckliste</param>
        /// <param name="userid">der angemeldete Benutzer</param>
        [Post("/api/v1/partlist/{partlistid}/email")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> SendEmailAsync([Body] PartlistEmailEntry content, [Path] int? partlistid, [Query] string userid);

        /// <summary>
        /// GetPositions3 (/api/v1/partlist/{partlistid}/position)
        /// </summary>
        /// <param name="partlistid"></param>
        /// <param name="price"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/partlist/{partlistid}/position")]
        Task<PartlistPositionEntryList> GetPositions3Async([Path] int? partlistid, [Query] bool? price, [Query] string userid);

        /// <summary>
        /// CreatePosition2 (/api/v1/partlist/{partlistid}/position)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="partlistid"></param>
        /// <param name="userid"></param>
        [Post("/api/v1/partlist/{partlistid}/position")]
        [Header("Content-Type", "application/json")]
        Task<PartlistPositionEntry> CreatePosition2Async([Body] PartlistPositionPostEntry content, [Path] int? partlistid, [Query] string userid);

        /// <summary>
        /// UpdatePosition2 (/api/v1/partlist/{partlistid}/position)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="partlistid"></param>
        /// <param name="userid"></param>
        [Put("/api/v1/partlist/{partlistid}/position")]
        [Header("Content-Type", "application/json")]
        Task<PartlistPositionEntry> UpdatePosition2Async([Body] PartlistPositionPostEntry content, [Path] int? partlistid, [Query] string userid);

        /// <summary>
        /// Eine St&uuml;cklistenposition entfernen
        ///
        /// RemovePosition3 (/api/v1/partlist/{partlistid}/position/{positionid})
        /// </summary>
        /// <param name="partlistid">die St&uuml;cklisten-Id die die Position enth&auml;lt</param>
        /// <param name="positionid">die Position die entfernt werden soll</param>
        /// <param name="userid">der angemeldete Benutzer</param>
        [Delete("/api/v1/partlist/{partlistid}/position/{positionid}")]
        Task<Response<object>> RemovePosition3Async([Path] int? partlistid, [Path] int? positionid, [Query] string userid);

        /// <summary>
        /// Eine St&uuml;cliste als PDF 'drucken', bzw. empfangen
        ///
        /// PrintPartlist (/api/v1/partlist/{partlistid}/print)
        /// </summary>
        /// <param name="partlistid">die zu druckende St&uuml;ckliste</param>
        /// <param name="userid">der angemeldete Benutzer</param>
        [Get("/api/v1/partlist/{partlistid}/print")]
        Task<Response<object>> PrintPartlistAsync([Path] int? partlistid, [Query] string userid);

        /// <summary>
        /// Die Liste der Arbeitsg&auml;nge des Arbeitsplans
        ///
        /// GetWorksteps (/api/v1/partlist/{partlistid}/workstep)
        /// </summary>
        /// <param name="partlistid">die betreffende St&uuml;ckliste</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/partlist/{partlistid}/workstep")]
        Task<PartlistWorkstepEntryList> GetWorkstepsAsync([Path] int? partlistid, [Query] string userid);

        /// <summary>
        /// ChangePassword (/api/v1/password)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="userid"></param>
        [Post("/api/v1/password")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> ChangePasswordAsync([Body] ChangePasswordEntry content, [Query] string userid);

        /// <summary>
        /// Ein Los anhand seiner Losnummer ermitteln
        ///
        /// FindProductionByAttributes (/api/v1/production)
        /// </summary>
        /// <param name="addAdditionalStatus">(optional) mit true die Loszusatzstatus mitliefern</param>
        /// <param name="addCommentsMedia">(optional) mit true die Infos &uuml;ber die Artikelkommentare des St&uuml;cklistenartikels liefern.Es werden dabei aber nur jene Artikelkommentare ber&uuml;cksichtigt, die der Fertigung zugewiesen wurden.</param>
        /// <param name="addCustomer">(optional) mit true den Kunden dieses Loses ebenfalls liefern</param>
        /// <param name="addDeliveredAmount">(optional) mit true die abgelieferte Menge liefern</param>
        /// <param name="addDocuments">(optional) mit true die Infos &uuml;ber die vom angemeldeten Benutzersichtbaren Dokumente aus der Dokumentenablage liefern</param>
        /// <param name="addPartlistItem">(optional) mit true den zugrundeliegenden Artikel der verwendeten St&uuml;ckliste ebenfalls liefern</param>
        /// <param name="addProductionWorksteps">(optional) mit true die die Arbeitsg&auml;nge des Loses liefern</param>
        /// <param name="addTargetMaterials">(optional) mit true die Lossollmaterialen mitliefern</param>
        /// <param name="addTestPlan">(optional) mit true den Lospr&uuml;fplan des angegebenen Loses mitliefern</param>
        /// <param name="addWorksteps">(optional) mit true die Arbeitsg&auml;nge der St&uuml;ckliste mitliefern</param>
        /// <param name="productionCnr">die (optional) Losnummer</param>
        /// <param name="productionid">die (optional) Id des Loses. Ist auch die cnr angegeben, hat die id Priorit&auml;t</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/production")]
        Task<ProductionEntry> FindProductionByAttributesAsync([Query] bool? addAdditionalStatus, [Query] bool? addCommentsMedia, [Query] bool? addCustomer, [Query] bool? addDeliveredAmount, [Query] bool? addDocuments, [Query] bool? addPartlistItem, [Query] bool? addProductionWorksteps, [Query] bool? addTargetMaterials, [Query] bool? addTestPlan, [Query] bool? addWorksteps, [Query] string productionCnr, [Query] int? productionid, [Query] string userid);

        /// <summary>
        /// &Auml;ndert die Losgr&ouml;&szlig;e
        ///
        /// ChangeProductionOrderSize (/api/v1/production/changeproductionordersize)
        /// </summary>
        /// <param name="productionCnr">ist die (optionale) Losnummer.</param>
        /// <param name="productionid">ist die (optionale) Id des Loses. Ist die Id angegeben, hat sie Vorrang.</param>
        /// <param name="productionordersize">die neue Losgr&ouml;&szlig;e</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Put("/api/v1/production/changeproductionordersize")]
        Task<int> ChangeProductionOrderSizeAsync([Query] string productionCnr, [Query] int? productionid, [Query] string productionordersize, [Query] string userid);

        /// <summary>
        /// Setzt das Los in den Status "IN PRODUKTION" sofern es im AUSGEGEBEN Status ist
        ///
        /// ChangeToProduction (/api/v1/production/changetoproduction)
        /// </summary>
        /// <param name="productionCnr">ist die (optionale) Losnummer.</param>
        /// <param name="productionid">ist die (optionale) Id des Loses. Ist die Id angegeben, hat sie Vorrang.</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Post("/api/v1/production/changetoproduction")]
        Task<int> ChangeToProductionAsync([Query] string productionCnr, [Query] int? productionid, [Query] string userid);

        /// <summary>
        /// Den Arbeitsgang als fertig markieren.Es gibt derzeit keine weitere &UUml;berpr&uuml;fung ob das Los storniert ist.
        ///
        /// UpdateDoneOpenWorkEntryList (/api/v1/production/doneopenworklist)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="hvtoken"></param>
        /// <param name="userid"></param>
        [Put("/api/v1/production/doneopenworklist")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> UpdateDoneOpenWorkEntryListAsync([Body] OpenWorkUpdateEntryList content,  [Query] string userid);

        /// <summary>
        /// Eine Los als erledigt markierenDas Los wird nur dann als erledigt markiert, wenn die abgelieferte Mengezumindest der Losgr&ouml;&szlig;e entspricht. Andernfalls wird im Resultdie property missingAmount auf true gesetzt. In diesem Fallewird das Los nicht erledigt, au&szlig;er es wird mittels overrideNotCompleted&uuml;bersteuert.Gibt es auf dem Los eine offene Zeitbuchung, wird der Name der betroffenenPerson in der property processedBy angegeben. Das Los wird dann nicht erledigt.Dies gelingt erst, wenn mittels ignoreProcessedBy &uuml;bersteuertwird.
        ///
        /// DoneProduction (/api/v1/production/doneproduction)
        /// </summary>
        /// <param name="ignoreProcessedBy">mit true wird eine eventuell vorhandene offene Zeitbuchungauf das Los ignoriert.</param>
        /// <param name="overrideNotCompleted">mit true kann die Mengenpr&uuml;fung &uuml;bersteuertwerden. Eine zu geringe Losablieferung wird dann also ignoriert.</param>
        /// <param name="productionCnr">ist die (optionale) Losnummer.</param>
        /// <param name="productionid">ist die (optionale) Id des Loses. Ist die Id angegeben, hat sie Vorrang.</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Post("/api/v1/production/doneproduction")]
        Task<DoneProductionEntry> DoneProductionAsync([Query] bool? ignoreProcessedBy, [Query] bool? overrideNotCompleted, [Query] string productionCnr, [Query] int? productionid, [Query] string userid);

        /// <summary>
        /// Ein Los ausgeben
        ///
        /// EmitProduction (/api/v1/production/emitproduction)
        /// </summary>
        /// <param name="overrideLockWarning">true wenn eventuell vorhandene Artikelsperren ignoriert werden sollen,false sind Sperren vorhanden, wird das Los nicht ausgegeben</param>
        /// <param name="overrideMissingPartsWarning">true wenn das Los auch ausgegeben werden soll, sofernFehlmengen entst&uuml;nden. Bei false wird das Los nur ausgegeben, wenn entweder keine Fehlmengeentst&uuml;nde, oder der Systemparameter "FEHLMENGE_ENTSTEHEN_WARNUNG" auf 0 steht.</param>
        /// <param name="productionCnr">ist die (optionale) Losnummer.</param>
        /// <param name="productionid">ist die (optionale) Id des Loses. Ist die Id angegeben, hat sie Vorrang.</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Post("/api/v1/production/emitproduction")]
        Task<EmitProductionEntry> EmitProductionAsync([Query] bool? overrideLockWarning, [Query] bool? overrideMissingPartsWarning, [Query] string productionCnr, [Query] int? productionid, [Query] string userid);

        /// <summary>
        /// GetListProductions (/api/v1/production/list/{userid})
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="filterCnr"></param>
        /// <param name="filterCustomer"></param>
        /// <param name="filterItemcnr"></param>
        /// <param name="filterProductiongroup"></param>
        /// <param name="filterProject"></param>
        /// <param name="filterStatus"></param>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        [Get("/api/v1/production/list/{userid}")]
        Task<ProductionEntryList> GetListProductionsAsync([Path] string userid, [Query(Name = "filter_cnr")] string filterCnr, [Query(Name = "filter_customer")] string filterCustomer, [Query(Name = "filter_itemcnr")] string filterItemcnr, [Query(Name = "filter_productiongroup")] string filterProductiongroup, [Query(Name = "filter_project")] string filterProject, [Query(Name = "filter_status")] string filterStatus, [Query] int? limit, [Query] int? startIndex);

        /// <summary>
        /// CreateMaterialRequirements (/api/v1/production/materialrequirements)
        /// </summary>
        /// <param name="content"></param>
        [Post("/api/v1/production/materialrequirements")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> CreateMaterialRequirementsAsync([Body] MaterialRequirementPostEntry content);

        /// <summary>
        /// Eine Materialentnahme bzw. -r&uuml;ckgabe nachtr&auml;glich durchf&uuml;hrenIm Gegensatz zum HELIUM V Client d&uuml;rfen nur Materialienzur&uuml;ckgegeben werden, die auch tats&auml;chlich ausgegeben wordensind.ist materialEntry.targetMaterialId gesetzt, wird eine Entnahme/R&uuml;ckgabe aufdie damit angegebene Sollposition durchgef&uuml;hrt, ansonsten wird diese ohneohne Bezug zu einer Sollposition durchgef&uuml;hrt.
        ///
        /// BucheMaterialNachtraeglichVomLagerAb (/api/v1/production/materialwithdrawal)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="hvtoken">ist der beim Logon ermittelte Token. (optional)</param>
        /// <param name="userId">ist der beim Logon ermittelte "token"</param>
        [Post("/api/v1/production/materialwithdrawal")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> BucheMaterialNachtraeglichVomLagerAbAsync([Body] MaterialWithdrawalEntry content,  [Query] string userId);

        /// <summary>
        /// Eine Liste aller offenen Arbeitsg&auml;nge ermitteln
        ///
        /// GetOpenWorkEntries (/api/v1/production/openwork)
        /// </summary>
        /// <param name="filterCnr"></param>
        /// <param name="filterCustomer"></param>
        /// <param name="filterEnddate"></param>
        /// <param name="filterInproductiononly">liefert bei Aktivierung nur offene Arbeitsg&auml;nge aus Losen, diesich in Produktion befinden</param>
        /// <param name="filterItemcnr"></param>
        /// <param name="filterMachinegroup"></param>
        /// <param name="filterNextworkstep">liefert bei Aktivierung nur den n&auml;chsten nicht fertigen Arbeitsgang</param>
        /// <param name="filterStartdate"></param>
        /// <param name="limit">die maximale Anzahl von Eintr&auml;gen</param>
        /// <param name="startIndex"></param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/production/openwork")]
        Task<OpenWorkEntryList> GetOpenWorkEntriesAsync([Query(Name = "filter_cnr")] string filterCnr, [Query(Name = "filter_customer")] string filterCustomer, [Query(Name = "filter_enddate")] long? filterEnddate, [Query(Name = "filter_inproductiononly")] bool? filterInproductiononly, [Query(Name = "filter_itemcnr")] string filterItemcnr, [Query(Name = "filter_machinegroup")] int? filterMachinegroup, [Query(Name = "filter_nextworkstep")] bool? filterNextworkstep, [Query(Name = "filter_startdate")] long? filterStartdate, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Einen offen Arbeitsgang modifizierenEs kann die Resource (Maschine), die Startzeit und der Tagesversatz abge&auml;ndert werden.
        ///
        /// UpdateOpenWorkEntry (/api/v1/production/openwork)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="hvtoken">ist der (optionale) beim Logon ermittelte "Token"</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token". Entweder headerUserId oder userId muss gesetzt sein</param>
        [Put("/api/v1/production/openwork")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> UpdateOpenWorkEntryAsync([Body] OpenWorkUpdateEntry content,  [Query] string userid);

        /// <summary>
        /// UpdateOpenWorkEntryList (/api/v1/production/openworklist)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="hvtoken"></param>
        /// <param name="userid"></param>
        [Put("/api/v1/production/openworklist")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> UpdateOpenWorkEntryListAsync([Body] OpenWorkUpdateEntryList content,  [Query] string userid);

        /// <summary>
        /// Druckt Etiketten der Fertigung &uuml;ber den Server aus.
        ///
        /// PrintLabels (/api/v1/production/printlabels)
        /// </summary>
        /// <param name="printLabel">(optional) mit true den Druck der Losetiketten ausl&ouml;sen</param>
        /// <param name="printLabelComment">(optional) Kommentar des Losetiketts</param>
        /// <param name="printLabelCopies">(optional) die Anzahl der Exemplare des Losetiketts. Wenn nicht angegeben ist sie 1.</param>
        /// <param name="printLabelQuantity">(optional) die Mengenangabe des Losetiketts. Wenn nicht angegeben ist sie die Menge der letzten Losablieferung.</param>
        /// <param name="printerLabel">(optional) Drucker &uuml;ber den das Losetikett gedruckt werden soll.Wenn nicht angegeben wird der mobile Default-Etikettendrucker (siehe Mandanten- und Arbeitsplatzparameter) verwendet.</param>
        /// <param name="productionCnr">Ist die (optionale) Losnummer.</param>
        /// <param name="productionid">Ist die (optionale) Id des Loses. Ist die Id angegeben, hat sie Vorrang.</param>
        /// <param name="userid">Ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/production/printlabels")]
        Task<PrintProductionLabelEntry> PrintLabelsAsync([Query] bool? printLabel, [Query] string printLabelComment, [Query] int? printLabelCopies, [Query] string printLabelQuantity, [Query] string printerLabel, [Query] string productionCnr, [Query] int? productionid, [Query] string userid);

        /// <summary>
        /// PrintPapers (/api/v1/production/printpapers)
        /// </summary>
        /// <param name="printAnalysissheet"></param>
        /// <param name="printProductionsupplynote"></param>
        /// <param name="printerAnalysissheet"></param>
        /// <param name="printerProductionsupplynote"></param>
        /// <param name="productionCnr"></param>
        /// <param name="productionid"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/production/printpapers")]
        Task<PrintProductionEntry> PrintPapersAsync([Query] bool? printAnalysissheet, [Query] bool? printProductionsupplynote, [Query] string printerAnalysissheet, [Query] string printerProductionsupplynote, [Query] string productionCnr, [Query] int? productionid, [Query] string userid);

        /// <summary>
        /// Ein Los stoppen
        ///
        /// StopProduction (/api/v1/production/stopproduction)
        /// </summary>
        /// <param name="productionCnr">ist die (optionale) Losnummer.</param>
        /// <param name="productionid">ist die (optionale) Id des Loses. Ist die Id angegeben, hat sie Vorrang.</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Post("/api/v1/production/stopproduction")]
        Task<StopProductionEntry> StopProductionAsync([Query] string productionCnr, [Query] int? productionid, [Query] string userid);

        /// <summary>
        /// Pru&uuml;fergebnisse einer Losablieferung erzeugen
        ///
        /// CreateTestResults (/api/v1/production/testresult)
        /// </summary>
        /// <param name="content"></param>
        [Post("/api/v1/production/testresult")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> CreateTestResultsAsync([Body] TestResultPostEntry content);

        /// <summary>
        /// GetCommentsMedia (/api/v1/production/{productionid}/commentmedia/list)
        /// </summary>
        /// <param name="productionid"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/production/{productionid}/commentmedia/list")]
        Task<ItemCommentMediaInfoEntryList> GetCommentsMediaAsync([Path] int? productionid, [Query] string userid);

        /// <summary>
        /// GetCommentMedia (/api/v1/production/{productionid}/commentmedia/{itemcommentid})
        /// </summary>
        /// <param name="productionid"></param>
        /// <param name="itemcommentid"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/production/{productionid}/commentmedia/{itemcommentid}")]
        Task<Response<object>> GetCommentMediaAsync([Path] int? productionid, [Path] int? itemcommentid, [Query] string userid);

        /// <summary>
        /// Den Losproduktionsbeginn oder (auch) Losproduktionsende auf neue Datumswerte setzen
        ///
        /// PutMoveProductionDate (/api/v1/production/{productionid}/dates)
        /// </summary>
        /// <param name="productionid">die Id des zu &auml;ndernden Loses</param>
        /// <param name="finish">(optionaler) neuer Endetermin. Mindestens einer der beidenTermine muss angegeben werden. Wird finishTimeMs nicht angegeben, so wird deraktuelle Endetermin verwendet</param>
        /// <param name="start">(optionaler) neuer Beginntermin. Mindestens einer der beidenTermine muss angegeben werden. Wird startTimeMs nicht angegeben, so wird deraktuelle Beginntermin verwendet</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Put("/api/v1/production/{productionid}/dates")]
        Task<Response<object>> PutMoveProductionDateAsync([Path] int? productionid, [Query] long? finish, [Query] long? start, [Query] string userid);

        /// <summary>
        /// Erzeugt eine Losablieferung des angebenen Loses
        ///
        /// CreateDelivery4 (/api/v1/production/{productionid}/delivery)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="productionid">Id des Loses</param>
        [Post("/api/v1/production/{productionid}/delivery")]
        [Header("Content-Type", "application/json")]
        Task<int> CreateDelivery4Async([Body] DeliveryPostEntry content, [Path] int? productionid);

        /// <summary>
        /// GetDocument2 (/api/v1/production/{productionid}/document)
        /// </summary>
        /// <param name="productionid"></param>
        /// <param name="documentcnr"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/production/{productionid}/document")]
        Task<Response<object>> GetDocument2Async([Path] int? productionid, [Query] string documentcnr, [Query] string userid);

        /// <summary>
        /// Legt Dateien in die Dokumentenablage eines Loses ab.Die maximale Upload-Gr&ouml;&szlig;e ist derzeit auf 500KiB gestellt
        ///
        /// CreateDocument4 (/api/v1/production/{productionid}/document)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="productionid">ist die Id des Loses</param>
        /// <param name="grouping">ist die Gruppierung (optional)</param>
        /// <param name="keywords">sind die Schlagworte des Dokuments (optional)</param>
        /// <param name="securitylevel">ist die Sicherheitsstufe (optional)</param>
        /// <param name="type">ist die Belegart (optional)</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Post("/api/v1/production/{productionid}/document")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> CreateDocument4Async([Body] MultipartBody content, [Path] int? productionid, [Query] string grouping, [Query] string keywords, [Query] long? securitylevel, [Query] string type, [Query] string userid);

        /// <summary>
        /// GetDocuments3 (/api/v1/production/{productionid}/document/list)
        /// </summary>
        /// <param name="productionid"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/production/{productionid}/document/list")]
        Task<DocumentInfoEntryList> GetDocuments3Async([Path] int? productionid, [Query] string userid);

        /// <summary>
        /// Etiketten eines Loses als JRPrint erhalten.
        ///
        /// PrintLabel (/api/v1/production/{productionid}/printlabel)
        /// </summary>
        /// <param name="productionid">das Los des zu druckenden Verpackungsetiketts</param>
        /// <param name="amount">Menge</param>
        /// <param name="copies">Anzahl der Exemplare</param>
        /// <param name="reportType">(optional) Name einer Reportvariante des Losetiketts</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/production/{productionid}/printlabel")]
        Task<Response<object>> PrintLabelAsync([Path] int? productionid, [Query] string amount, [Query] int? copies, [Query] string reportType, [Query] string userid);

        /// <summary>
        /// Ein Verpackungsetikett eines Loses als JRPrint erhalten.
        ///
        /// PrintPackingLabel (/api/v1/production/{productionid}/printpackinglabel)
        /// </summary>
        /// <param name="productionid">das Los des zu druckenden Verpackungsetiketts</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/production/{productionid}/printpackinglabel")]
        Task<Response<object>> PrintPackingLabelAsync([Path] int? productionid, [Query] string userid);

        /// <summary>
        /// Einen Fertigungsbegleitschein eines Loses als JRPrint erhalten.
        ///
        /// PrintProductionSupplyNote (/api/v1/production/{productionid}/printproductionsupplynote)
        /// </summary>
        /// <param name="productionid">das zu druckende Los</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/production/{productionid}/printproductionsupplynote")]
        Task<Response<object>> PrintProductionSupplyNoteAsync([Path] int? productionid, [Query] string userid);

        /// <summary>
        /// Ermittelt die Lossollmaterialien eines Loses
        ///
        /// GetTargetMaterials (/api/v1/production/{productionid}/targetmaterial)
        /// </summary>
        /// <param name="productionid">die Id des Loses</param>
        /// <param name="addStockPlaceInfos">(optional) mit true die Infos der zugewiesenen Lagerpl&auml;tze liefern.Ber&uuml;cksichtigt werden dabei nur jene L&auml;ger, die im betreffenden Los zur Loslagerentnahme definiert sindund f&uuml;r die der Benutzer das Recht hat, das jeweilige Lager zu benutzen.</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/production/{productionid}/targetmaterial")]
        Task<ProductionTargetMaterialEntryList> GetTargetMaterialsAsync([Path] int? productionid, [Query] bool? addStockPlaceInfos, [Query] string userid);

        /// <summary>
        /// Ermittelt die Lossollmaterialien des Artikels f&uuml;r das Los
        ///
        /// GetTargetMaterialsForItemId (/api/v1/production/{productionid}/targetmaterial/{itemid})
        /// </summary>
        /// <param name="productionid">die Id des Loses</param>
        /// <param name="itemid">die Id des gesuchten Artikels</param>
        /// <param name="addStockPlaceInfos">(optional) mit true die Infos der zugewiesenen Lagerpl&auml;tze liefern.Ber&uuml;cksichtigt werden dabei nur jene L&auml;ger, die im betreffenden Los zur Loslagerentnahme definiert sindund f&uuml;r die der Benutzer das Recht hat, das jeweilige Lager zu benutzen.</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/production/{productionid}/targetmaterial/{itemid}")]
        Task<ProductionTargetMaterialEntryList> GetTargetMaterialsForItemIdAsync([Path] int? productionid, [Path] int? itemid, [Query] bool? addStockPlaceInfos, [Query] string userid);

        /// <summary>
        /// Ermittelt den Lospr&uuml;fplan des angegebenen Loses
        ///
        /// GetTestPlan (/api/v1/production/{productionid}/testplan)
        /// </summary>
        /// <param name="productionid">Id des Loses</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/production/{productionid}/testplan")]
        Task<TestPlanEntryList> GetTestPlanAsync([Path] int? productionid, [Query] string userid);

        /// <summary>
        /// Ermittelt und liefert eine Liste von Maschinen, die f&uuml;r den Losarbeitsgangeingesetzt werden d&uuml;rfen
        ///
        /// GetAvailableMachinesForProductionWorkstep (/api/v1/production/{productionid}/workstep/{productionworkstepid}/machine)
        /// </summary>
        /// <param name="productionid">die Id des Loses des Arbeitsgangs</param>
        /// <param name="productionworkstepid">Id des Arbeitsgangs des Loses</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Get("/api/v1/production/{productionid}/workstep/{productionworkstepid}/machine")]
        Task<MachineEntryList> GetAvailableMachinesForProductionWorkstepAsync([Path] int? productionid, [Path] int? productionworkstepid, [Query] string userid);

        /// <summary>
        /// Die Maschine eines Losarbeitsgangs setzen. Die vorher gesetzte Maschine wird- sofern vorhanden - gestoppt.
        ///
        /// UpdateMachineOfProductionWorkstep (/api/v1/production/{productionid}/workstep/{productionworkstepid}/machine)
        /// </summary>
        /// <param name="productionid">die Id des Loses des Arbeitsgangs</param>
        /// <param name="productionworkstepid">Id des Arbeitsgangs des Loses</param>
        /// <param name="machineid">die Id der neu zu setzenden Maschine des Losarbeitsgangs</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Put("/api/v1/production/{productionid}/workstep/{productionworkstepid}/machine")]
        Task<Response<object>> UpdateMachineOfProductionWorkstepAsync([Path] int? productionid, [Path] int? productionworkstepid, [Query] int? machineid, [Query] string userid);

        /// <summary>
        /// Die Liste der den Filterkriterien entsprechenden Lose
        ///
        /// GetProductions (/api/v1/production/{userid})
        /// </summary>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        /// <param name="filterCnr">die zu filternde Losnummer</param>
        /// <param name="filterCustomer">der zu filternde Kundenname</param>
        /// <param name="filterItemcnr">die zu filternde Artikelnummer</param>
        /// <param name="filterProductiongroup">die zu filternde Fertigungsgruppe</param>
        /// <param name="filterProject">der zu filternde Projektname</param>
        /// <param name="limit">die maximale Anzahl von Eintr&auml;gen die ermittelt werdensollen</param>
        /// <param name="startIndex">der StartIndex</param>
        [Get("/api/v1/production/{userid}")]
        Task<List<ProductionEntry>> GetProductionsAsync([Path] string userid, [Query(Name = "filter_cnr")] string filterCnr, [Query(Name = "filter_customer")] string filterCustomer, [Query(Name = "filter_itemcnr")] string filterItemcnr, [Query(Name = "filter_productiongroup")] string filterProductiongroup, [Query(Name = "filter_project")] string filterProject, [Query] int? limit, [Query] int? startIndex);

        /// <summary>
        /// GetProjects (/api/v1/project)
        /// </summary>
        /// <param name="filterCnr"></param>
        /// <param name="filterCompany"></param>
        /// <param name="filterMyopen"></param>
        /// <param name="filterWithCancelled"></param>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/project")]
        Task<ProjectEntryList> GetProjectsAsync([Query(Name = "filter_cnr")] string filterCnr, [Query(Name = "filter_company")] string filterCompany, [Query(Name = "filter_myopen")] bool? filterMyopen, [Query(Name = "filter_withCancelled")] bool? filterWithCancelled, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// CreateDocument5 (/api/v1/project/{projectid}/document)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="projectid"></param>
        /// <param name="grouping"></param>
        /// <param name="keywords"></param>
        /// <param name="securitylevel"></param>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        [Post("/api/v1/project/{projectid}/document")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> CreateDocument5Async([Body] MultipartBody content, [Path] int? projectid, [Query] string grouping, [Query] string keywords, [Query] long? securitylevel, [Query] string type, [Query] string userid);

        /// <summary>
        /// Ermittelt die komplette Beschreibung der CHARGENEIGENSCHAFTENdieses Artikels
        ///
        /// GetItemIdentityPropertyLayout (/api/v1/property/identity/{itemid}/layouts)
        /// </summary>
        /// <param name="hvtoken">ist der beim Logon ermittelte "token" (optional)</param>
        /// <param name="itemid">die Id des betreffenden Artikels</param>
        /// <param name="userid">ist der beim Logon ermittelte "token" (optional)</param>
        [Get("/api/v1/property/identity/{itemid}/layouts")]
        Task<PropertyLayoutEntryList> GetItemIdentityPropertyLayoutAsync( [Path] int? itemid, [Query] string userid);

        /// <summary>
        /// Die CHARGENEIGENSCHAFTEN des Artikels und seiner Charge / Seriennummerermitteln
        ///
        /// GetItemIdentityProperties (/api/v1/property/identity/{itemid}/properties)
        /// </summary>
        /// <param name="content">Die Seriennummer bzw. Chargennummer mit der zugehoerigen Menge</param>
        /// <param name="hvtoken">ist der beim Logon ermittelte "token" (optional)</param>
        /// <param name="itemid">die Id des betreffenden Artikels</param>
        /// <param name="userid">ist der beim Logon ermittelte "token" (optional)</param>
        [Get("/api/v1/property/identity/{itemid}/properties")]
        [Header("Content-Type", "application/json")]
        Task<ItemPropertyEntryList> GetItemIdentityPropertiesAsync([Body] ItemIdentityEntry content,  [Path] int? itemid, [Query] string userid);

        /// <summary>
        /// Die CHARGENEIGENSCHAFT des angegebenen Artikels(und der zugeh&ouml;rigen Charge/Seriennummer) neu setzenSollte es bereits Eigenschaften gegeben haben, werden diese zuvor entferntund dann jene Eigenschaften gesetzt, die mit diesem Aufruf angegeben worden sind.
        ///
        /// PostItemIdentityProperties (/api/v1/property/identity/{itemid}/properties)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="hvtoken">ist der beim Logon ermittelte "token" (optional)</param>
        /// <param name="itemid">die Id des betreffenden Artikels</param>
        /// <param name="userid">ist der beim Logon ermittelte "token" (optional)</param>
        [Post("/api/v1/property/identity/{itemid}/properties")]
        [Header("Content-Type", "application/json")]
        Task<ItemPropertyEntryList> PostItemIdentityPropertiesAsync([Body] CreateItemIdentityPropertyEntryList content,  [Path] int? itemid, [Query] string userid);

        /// <summary>
        /// Die CHARGENEIGENSCHAFT des angegebenen Artikels (und der zugeh&ouml;rigen Chargen/Seriennummer)neu setzenEs werden dabei nur jene Eigenschaften neu gesetzt, die in diesem Aufruf mit ihrerlayoutId angegeben worden sind.
        ///
        /// PutItemIdentityProperties (/api/v1/property/identity/{itemid}/properties)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="hvtoken">ist der beim Logon ermittelte "token" (optional)</param>
        /// <param name="itemid">itemId die Id des betreffenden Artikels</param>
        /// <param name="userid">ist der beim Logon ermittelte "token" (optional)</param>
        [Put("/api/v1/property/identity/{itemid}/properties")]
        [Header("Content-Type", "application/json")]
        Task<ItemPropertyEntryList> PutItemIdentityPropertiesAsync([Body] CreateItemIdentityPropertyEntryList content,  [Path] int? itemid, [Query] string userid);

        /// <summary>
        /// Eine bestimmte Eigenschaft der CHARGENEIGENSCHAFT des angegebenen Artikels(und der zugeh&ouml;rigen Charge/Seriennummer) ermitteln
        ///
        /// GetItemIdentityProperty (/api/v1/property/identity/{itemid}/properties/{layoutid})
        /// </summary>
        /// <param name="content">Die Seriennummer bzw. Chargennummer mit der zugehoerigen Menge</param>
        /// <param name="hvtoken">ist der beim Logon ermittelte "token" (optional)</param>
        /// <param name="itemid">die Id des betreffenden Artikels</param>
        /// <param name="layoutid">eine der Ids die &uuml;ber .../layouts ermittelt wurde</param>
        /// <param name="userid">ist der beim Logon ermittelte "token" (optional)</param>
        [Get("/api/v1/property/identity/{itemid}/properties/{layoutid}")]
        [Header("Content-Type", "application/json")]
        Task<ItemPropertyEntry> GetItemIdentityPropertyAsync([Body] ItemIdentityEntry content,  [Path] int? itemid, [Path] int? layoutid, [Query] string userid);

        /// <summary>
        /// Die CHARGENEIGENSCHAFT des angegebenen Artikels (und der zugeh&ouml;rigen Chargen/Seriennummer)neu setzenEs wird dabei nur die angegebene Eigenschaften neu gesetzt, die in diesem Aufruf mit ihrerlayoutId angegeben worden ist.
        ///
        /// PutItemIdentityProperty (/api/v1/property/identity/{itemid}/properties/{layoutid})
        /// </summary>
        /// <param name="content"></param>
        /// <param name="hvtoken">ist der beim Logon ermittelte "token" (optional)</param>
        /// <param name="itemid">itemId die Id des betreffenden Artikels</param>
        /// <param name="layoutid">die Id der Beschreibung des Datenfeldes dessen Wert neu gesetzt werden soll</param>
        /// <param name="userid">ist der beim Logon ermittelte "token" (optional)</param>
        [Put("/api/v1/property/identity/{itemid}/properties/{layoutid}")]
        [Header("Content-Type", "application/json")]
        Task<ItemPropertyEntryList> PutItemIdentityPropertyAsync([Body] CreateItemIdentityPropertyEntry content,  [Path] int? itemid, [Path] int? layoutid, [Query] string userid);

        /// <summary>
        /// Ermittelt die komplette Beschreibung der ARTIKELEIGENSCHAFTENdieses Artikels
        ///
        /// GetItemPropertyLayout (/api/v1/property/item/{itemid}/layouts)
        /// </summary>
        /// <param name="hvtoken">ist der beim Logon ermittelte "token" (optional)</param>
        /// <param name="itemid">die Id des betreffenden Artikels</param>
        /// <param name="userid">ist der beim Logon ermittelte "token" (optional)</param>
        [Get("/api/v1/property/item/{itemid}/layouts")]
        Task<PropertyLayoutEntryList> GetItemPropertyLayoutAsync( [Path] int? itemid, [Query] string userid);

        /// <summary>
        /// CreateMobileInvoices (/api/v1/purchaseinvoice/mobilebatch)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="userid"></param>
        [Post("/api/v1/purchaseinvoice/mobilebatch")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> CreateMobileInvoicesAsync([Body] PostPurchaseInvoices content, [Query] string userid);

        /// <summary>
        /// Legt Dateien in die Dokumentenablage eines Eingangsrechnung ab.Die maximale Upload-Gr&ouml;&szlig;e ist derzeit auf 500KiB gestellt
        ///
        /// CreateDocument6 (/api/v1/purchaseinvoice/{purchaseInvoiceid}/document)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="purchaseInvoiceid">ist die Id der Eingangsrechnung</param>
        /// <param name="grouping">ist die Gruppierung (optional)</param>
        /// <param name="keywords">sind die Schlagworte des Dokuments (optional)</param>
        /// <param name="securitylevel">ist die Sicherheitsstufe (optional)</param>
        /// <param name="type">ist die Belegart (optional)</param>
        /// <param name="userid">ist der beim Logon ermittelte "Token"</param>
        [Post("/api/v1/purchaseinvoice/{purchaseInvoiceid}/document")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> CreateDocument6Async([Body] MultipartBody content, [Path] int? purchaseInvoiceid, [Query] string grouping, [Query] string keywords, [Query] long? securitylevel, [Query] string type, [Query] string userid);

        /// <summary>
        /// Eine (offene) Bestellung ermittelnIst die Bestellnummer - entweder die Bestellnummer oder die Id - bekannt, wirdnur diese Bestellnummer betrachtet. Ist die Bestellnummer unbekannt, aber daf&uuml;rdie Lieferantennummer k&ouml;nnen dar&uuml;ber die Bestellungen ermittelt werden.Die purchaseOrderId wird immer als erstes herangezogen, sofern sieangegeben wird. Fehlt sie, aber die Bestellnummer ist angegeben, so wirddiese verwendet. Fehlt auch diese, wird die Lieferantennummer herangezogen. F&uuml;rdie Lieferantennummer k&ouml;nnen mehrere (offene) Bestellungen vorhanden sein.
        ///
        /// GetPurchaseOrder (/api/v1/purchaseorder)
        /// </summary>
        /// <param name="hvtoken">ist der (optionale) beim Logon ermittelte "token" im Header</param>
        /// <param name="addSupplierDetail">(optional) mit true wird zus&auml;tzlich der SupplierDetailEntrydes Lieferanten ermittelt und ausgegeben</param>
        /// <param name="filterReceiptpossible">mit true werden nur Bestellungenermittelt, die im Status OFFEN, BESTAETIGT oder TEILERLEDIGT haben. Mit falsewerden alle Bestellungen ermittelt, die der purchaseOrderId, purchaseOrderCnr odersupplierCnr entsprechen.</param>
        /// <param name="purchaseOrderCnr">ist die (optionale) gesuchte HELIUM V Bestellnummer</param>
        /// <param name="purchaseOrderid">ist die (optionale) Id der gesuchtes HELIUM V Bestellung</param>
        /// <param name="supplierCnr">ist die (optionale) Lieferantennummer</param>
        /// <param name="userid">ist der (optionale) beim Logon ermittelte "token"</param>
        [Get("/api/v1/purchaseorder")]
        Task<PurchaseOrderEntryList> GetPurchaseOrderAsync( [Query] bool? addSupplierDetail, [Query(Name = "filter_receiptpossible")] bool? filterReceiptpossible, [Query] string purchaseOrderCnr, [Query] int? purchaseOrderid, [Query] string supplierCnr, [Query] string userid);

        /// <summary>
        /// Information &uuml;ber einen etwaigen Wareneingang zu einer Bestellung ermittelnEs muss entweder die purchaseOrderCnr oder die purchaseOrderIdangegeben werden. Existiert bereits ein Wareneingang mit der angegebenen Lieferscheinnummer,so werden die Daten zum Wareneingang &uuml;bermittelt, ansonsten wird ein 404-Statuszur&uuml;ckgeliefert.
        ///
        /// GetGoodsReceipt (/api/v1/purchaseorder/goodsreceipt)
        /// </summary>
        /// <param name="hvtoken">ist der (optionale) beim Logon ermittelte "token" im Header</param>
        /// <param name="deliveryslipcnr">ist die Lieferantenlieferscheinnummer f&uuml;r die der Wareneingangermittelt werden soll</param>
        /// <param name="filterReceiptpossible"></param>
        /// <param name="purchaseOrderCnr">ist die (optionale) Bestellnummer</param>
        /// <param name="purchaseOrderid">ist die (optionale) Id der Bestellnummer.</param>
        /// <param name="userid">ist der (optionale) beim Logon ermittelte "token"</param>
        [Get("/api/v1/purchaseorder/goodsreceipt")]
        Task<GoodsReceiptEntry> GetGoodsReceiptAsync( [Query] string deliveryslipcnr, [Query(Name = "filter_receiptpossible")] bool? filterReceiptpossible, [Query] string purchaseOrderCnr, [Query] int? purchaseOrderid, [Query] string userid);

        /// <summary>
        /// Eine Wareneingangsposition erstellen
        ///
        /// CreateGoodsReceipt (/api/v1/purchaseorder/goodsreceipt)
        /// </summary>
        /// <param name="content">Repr&auml;sentiert eine zu liefernde Position</param>
        /// <param name="hvtoken">ist der (optionale) beim Logon ermittelte "token" im Header</param>
        /// <param name="filterReceiptpossible">wenn true wird der Wareneingang nurdann positiv verarbeitet, wenn f&uuml;r die entsprechende Bestellposition tats&auml;chlichnoch eine offene Menge existiert. Mit false kann eine beabsichtigte&Uuml;berlieferung durchgef&uuml;hrt werden.</param>
        /// <param name="userid">ist der (optionale) beim Logon ermittelte "token"</param>
        [Post("/api/v1/purchaseorder/goodsreceipt")]
        [Header("Content-Type", "application/json")]
        Task<CreatedGoodsReceiptPositionEntry> CreateGoodsReceiptAsync([Body] CreateGoodsReceiptPositionEntry content,  [Query(Name = "filter_receiptpossible")] bool? filterReceiptpossible, [Query] string userid);

        /// <summary>
        /// Legt Dateien in die Dokumentenablage eines Wareneingangs ab.Die maximale Upload-Gr&ouml;&szlig;e ist derzeit auf 6500KiB gestelltEntweder headerToken oder userId mussangegeben werden. 
        ///
        /// CreateDocumentViaDeliverySlipCnr (/api/v1/purchaseorder/goodsreceipt/deliveryslipcnr/document)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="hvtoken">ist der (optionale) beim Logon ermittelte "token" im Header</param>
        /// <param name="deliveryslipcnr">ist die Lieferscheinnummer des Lieferanten. Gibt es bereitseinen Wareneingang mit dieser Lieferscheinnummer, wird dieser verwendet. Ansonstenwird ein Wareneingang mit dieser Lieferscheinnummer angelegt.</param>
        /// <param name="filterReceiptpossible">Bei true werden die Dokumente zur Bestellunghinzugef&uul;gt sofern sie sich im Status OFFEN, BESTAETIGT oder TEILERLEDIGT befindet. Mit falsewird die Verarbeitung abgebrochen, wenn sich die Bestellung nicht im obigen Status befindet</param>
        /// <param name="grouping">ist die Gruppierung (optional, "Archiv", "Dokumente", "Gruppe-0", ... vom HELIUM V Anwender definierte Gruppen)</param>
        /// <param name="keywords">sind die Schlagworte des Dokuments (optional)</param>
        /// <param name="purchaseOrderid">ist die Id der Bestellung</param>
        /// <param name="securitylevel">ist die Sicherheitsstufe (optional, 0 = keine, 1 = niedrig, 2 = mittel, 3 = hoch, 99 = Archiv)</param>
        /// <param name="type">ist die Belegart (optional, "Auftragsbest&auml;tigung", "E-Mail", "kopiertes Dokument",... vom HELIUM V Anwender definierte Belegarten)</param>
        /// <param name="userid">ist der (optionale) beim Logon ermittelte "token"</param>
        [Post("/api/v1/purchaseorder/goodsreceipt/deliveryslipcnr/document")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> CreateDocumentViaDeliverySlipCnrAsync([Body] MultipartBody content,  [Query] string deliveryslipcnr, [Query(Name = "filter_receiptpossible")] bool? filterReceiptpossible, [Query] string grouping, [Query] string keywords, [Query] int? purchaseOrderid, [Query] long? securitylevel, [Query] string type, [Query] string userid);

        /// <summary>
        /// Legt Dateien in die Dokumentenablage eines Wareneingangs ab.Die maximale Upload-Gr&ouml;&szlig;e ist derzeit auf 6500KiB gestelltEntweder headerToken oder userId mussangegeben werden. 
        ///
        /// CreateDocument7 (/api/v1/purchaseorder/goodsreceipt/{receiptid}/document)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="hvtoken">ist der (optionale) beim Logon ermittelte "token" im Header</param>
        /// <param name="receiptid">Id des Wareneingangs</param>
        /// <param name="grouping">ist die Gruppierung (optional, "Archiv", "Dokumente", "Gruppe-0", ... vom HELIUM V Anwender definierte Gruppen)</param>
        /// <param name="keywords">sind die Schlagworte des Dokuments (optional)</param>
        /// <param name="securitylevel">ist die Sicherheitsstufe (optional, 0 = keine, 1 = niedrig, 2 = mittel, 3 = hoch, 99 = Archiv)</param>
        /// <param name="type">ist die Belegart (optional, "Auftragsbest&auml;tigung", "E-Mail", "kopiertes Dokument",... vom HELIUM V Anwender definierte Belegarten)</param>
        /// <param name="userid">ist der (optionale) beim Logon ermittelte "token"</param>
        [Post("/api/v1/purchaseorder/goodsreceipt/{receiptid}/document")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> CreateDocument7Async([Body] MultipartBody content,  [Path] int? receiptid, [Query] string grouping, [Query] string keywords, [Query] long? securitylevel, [Query] string type, [Query] string userid);

        /// <summary>
        /// Liefert die Liste aller Wareneingangspositionen
        ///
        /// GetGoodsReceiptPositions (/api/v1/purchaseorder/goodsreceipt/{receiptid}/position)
        /// </summary>
        /// <param name="hvtoken">ist der (optionale) beim Logon ermittelte "token" im Header</param>
        /// <param name="receiptid">ist die Id des Wareneingangs</param>
        /// <param name="userid">ist der (optionale) beim Logon ermittelte "token"</param>
        [Get("/api/v1/purchaseorder/goodsreceipt/{receiptid}/position")]
        Task<GoodsReceiptPositionEntryList> GetGoodsReceiptPositionsAsync( [Path] int? receiptid, [Query] string userid);

        /// <summary>
        /// Das Wareneingangspositions-Etikett druckenWird keine Identity (Chargen- oder Seriennummer) angegeben, werdenf&uuml;r alle Positionen des Wareneingangs die Etiketten gedruckt.Ansonsten nur f&uuml;r jene Positionen auf die die Idenity zutrifft.
        ///
        /// PrintGoodsReceiptPosition (/api/v1/purchaseorder/goodsreceipt/{receiptid}/position/{positionid}/print)
        /// </summary>
        /// <param name="hvtoken">ist der (optionale) beim Logon ermittelte "token" im Header</param>
        /// <param name="receiptid">ist die Id des Wareneingangs</param>
        /// <param name="positionid">ist die Id der Wareneingangsposition</param>
        /// <param name="chargenr">ist die (optionale) Chargennummer f&uuml;r die das/dieEtiketten gedruckt werden sollen.</param>
        /// <param name="userid">ist der (optionale) beim Logon ermittelte "token"</param>
        [Get("/api/v1/purchaseorder/goodsreceipt/{receiptid}/position/{positionid}/print")]
        Task<int> PrintGoodsReceiptPositionAsync( [Path] int? receiptid, [Path] int? positionid, [Query] string chargenr, [Query] string userid);

        /// <summary>
        /// CreateGoodsReceiptReel (/api/v1/purchaseorder/goodsreceiptreel)
        /// </summary>
        /// <param name="content">Repr&auml;sentiert eine zu liefernde Position</param>
        /// <param name="hvtoken"></param>
        /// <param name="filterReceiptpossible"></param>
        /// <param name="userid"></param>
        [Post("/api/v1/purchaseorder/goodsreceiptreel")]
        [Header("Content-Type", "application/json")]
        Task<CreatedGoodsReceiptPositionReelEntry> CreateGoodsReceiptReelAsync([Body] CreateGoodsReceiptPositionReelEntry content,  [Query(Name = "filter_receiptpossible")] bool? filterReceiptpossible, [Query] string userid);

        /// <summary>
        /// Ermittelt die Liste aller Wareneinga&auml;nge einer BestellungEntweder headerToken oder userId mussangegeben werden. Entweder purchaseOrderCnr oder purchaseOrderIdmuss angegeben werden. 
        ///
        /// GetGoodsReceipts (/api/v1/purchaseorder/goodsreceipts)
        /// </summary>
        /// <param name="hvtoken">ist der (optionale) beim Logon ermittelte "token" im Header</param>
        /// <param name="deliveryslipcnr">die (optionale) externe Lieferscheinnummer auf dieeingeschr&auml;nkt werden soll. Wird sie nicht angegeben, werden alleWareneing&auml;nge die f&uuml;r die Bestellung bekannt sind &uuml;bermittelt</param>
        /// <param name="filterReceiptpossible">mit true werden nur Bestellungenermittelt, die im Status OFFEN, BESTAETIGT oder TEILERLEDIGT haben. Mit falseist der Status der Bestellung egal</param>
        /// <param name="purchaseOrderCnr">die Bestellnummer (optional)</param>
        /// <param name="purchaseOrderid">die Id der Bestellnummer (optional)</param>
        /// <param name="userid">ist der (optionale) beim Logon ermittelte "token"</param>
        [Get("/api/v1/purchaseorder/goodsreceipts")]
        Task<GoodsReceiptEntryList> GetGoodsReceiptsAsync( [Query] string deliveryslipcnr, [Query(Name = "filter_receiptpossible")] bool? filterReceiptpossible, [Query] string purchaseOrderCnr, [Query] int? purchaseOrderid, [Query] string userid);

        /// <summary>
        /// Artikelinformationen zu einer Bestellposition ermittelnGenerell gilt, dass die Artikelnummer zu in einer der Bestellpositionenin der angegebenen Bestellungen geh&ouml;ren muss. Zuerst wird die Artikelnummerals Artikelnummer des Artikellieferanten interpretiert. Ist sie dort unbekannt,wird sie als Herstellerartikelnummer interpretiert. Ist sie auch dort unbekannt,wird sie als Artikelnummer ("Artikel CNR") interpretiert.
        ///
        /// GetItemInfo (/api/v1/purchaseorder/item)
        /// </summary>
        /// <param name="hvtoken">ist der (optionale) beim Logon ermittelte "token" im Header</param>
        /// <param name="filterReceiptpossible">true wenn nur noch Positionen in der Bestellungber&uuml;cksichtigt werden sollen, die noch offen sind.</param>
        /// <param name="itemCnr">die gesuchte Artikelnummer</param>
        /// <param name="purchaseOrderid">ist die Id der Bestellung</param>
        /// <param name="userid">ist der (optionale) beim Logon ermittelte "token"</param>
        [Get("/api/v1/purchaseorder/item")]
        Task<PurchaseOrderPositionEntry> GetItemInfoAsync( [Query(Name = "filter_receiptpossible")] bool? filterReceiptpossible, [Query] string itemCnr, [Query] int? purchaseOrderid, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Bestellpositionen (ber&uuml;cksichtigt werden nur Ident-Artikel oder Handeingaben)
        ///
        /// GetListItemInfo (/api/v1/purchaseorder/itemlist)
        /// </summary>
        /// <param name="hvtoken">ist der (optionale) beim Logon ermittelte "token" im Header</param>
        /// <param name="filterReceiptpossible">true wenn nur noch Positionen in der Bestellungber&uuml;cksichtigt werden sollen, die noch offen sind.</param>
        /// <param name="purchaseOrderid">ist die Id der Bestellung</param>
        /// <param name="userid">userId ist der (optionale) beim Logon ermittelte "token"</param>
        [Get("/api/v1/purchaseorder/itemlist")]
        Task<PurchaseOrderPositionEntryList> GetListItemInfoAsync( [Query(Name = "filter_receiptpossible")] bool? filterReceiptpossible, [Query] int? purchaseOrderid, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Bestellungen gem&auml;&szlig; den Filterkriterien
        ///
        /// GetPurchaseOrderList (/api/v1/purchaseorder/list)
        /// </summary>
        /// <param name="filterCnr">(optional) schr&auml;nkt die Ausgabe auf Bestellungen ein, die diesen Teil der Bestellnummer beinhalten</param>
        /// <param name="filterItemcnr">(optional) schr&auml;nkt die Ausgabe auf Bestellungen ein, die Positionen mit diesem Artikel beinhalten</param>
        /// <param name="filterStatus">(optional) die zu filternden Status (aus PurchaseOrderStatus) getrennt durch Komma</param>
        /// <param name="filterSupplier">(optional) schr&auml;nkt die Ausgabe auf Bestellungen ein, die f&uuml;r diesen Lieferantennamen ausgestellt wurden</param>
        /// <param name="limit">(optional) die maximale Anzahl von Eintr&auml;gen die ermittelt werden sollen</param>
        /// <param name="startIndex">(optional) der StartIndex</param>
        /// <param name="userid">userId ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/purchaseorder/list")]
        Task<PurchaseOrderEntryList> GetPurchaseOrderListAsync([Query(Name = "filter_cnr")] string filterCnr, [Query(Name = "filter_itemcnr")] string filterItemcnr, [Query(Name = "filter_status")] string filterStatus, [Query(Name = "filter_supplier")] string filterSupplier, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Positionen eines Bestellvorschlags gem&auml;&szlig; den Filterkriterien
        ///
        /// GetPurchaseOrderProposalPositionList (/api/v1/purchaseorder/proposal)
        /// </summary>
        /// <param name="filterItemcnr">(optional) schr&auml;nkt die Ausgabe auf diese zu filternde Artikelnummer ein</param>
        /// <param name="filterNoted">(optional) wenn true werden nur als vorgemerkt markierte Positionen geliefert,wenn false nur alle nicht vorgemerkten Positionen</param>
        /// <param name="limit">(optional) die maximale Anzahl von Eintr&auml;gen die ermittelt werden sollen</param>
        /// <param name="startIndex">(optional) der StartIndex</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/purchaseorder/proposal")]
        Task<PurchaseOrderProposalPositionEntryList> GetPurchaseOrderProposalPositionListAsync([Query(Name = "filter_itemcnr")] string filterItemcnr, [Query(Name = "filter_noted")] bool? filterNoted, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Eine Position des Bestellvorschlags erstellenWenn diese Position vorgemerkt werden soll (&uuml;ber die Property CreatePurchaseOrderProposalPositionEntry.noted)dann darf es noch keine vorgemerkte Bestellvorschlagsposition des Artikels geben, f&uuml;r den eine neue Position erzeugt werden soll.Ist dies der Fall wird der Request mit einem EXPECTATION_FAILED (417) abgelehnt. Im Header X_HV_ADDITIONAL_ERROR_VALUEin dieser Response ist dabei die Id der bereits existenten Bestellvorschlagsposition enthalten.Wird die Position nicht vorgemerkt, ist eine beliebige Anzahl an Positionen des jeweiligen Artikels erlaubt.
        ///
        /// CreatePurchaseOrderProposalPosition (/api/v1/purchaseorder/proposalposition)
        /// </summary>
        /// <param name="content">Repr&auml;sentiert eine zu erzeugende Position des Bestellvorschlags</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Post("/api/v1/purchaseorder/proposalposition")]
        [Header("Content-Type", "application/json")]
        Task<CreatedPurchaseOrderProposalPositionEntry> CreatePurchaseOrderProposalPositionAsync([Body] CreatePurchaseOrderProposalPositionEntry content, [Query] string userid);

        /// <summary>
        /// Eine bestehende Position des Bestellvorschlags l&ouml;schen
        ///
        /// RemovePurchaseOrderProposalPosition (/api/v1/purchaseorder/proposalposition/{proposalpositionid})
        /// </summary>
        /// <param name="proposalpositionid">Id der abzu&auml;ndernden Position des Bestellvorschlags</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Delete("/api/v1/purchaseorder/proposalposition/{proposalpositionid}")]
        Task<Response<object>> RemovePurchaseOrderProposalPositionAsync([Path] int? proposalpositionid, [Query] string userid);

        /// <summary>
        /// Liefert eine Position des Bestellvorschlags
        ///
        /// GetPurchaseOrderProposalPosition (/api/v1/purchaseorder/proposalposition/{proposalpositionid})
        /// </summary>
        /// <param name="proposalpositionid">Id der angefragten Position des Bestellvorschlags</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Get("/api/v1/purchaseorder/proposalposition/{proposalpositionid}")]
        Task<PurchaseOrderProposalPositionEntry> GetPurchaseOrderProposalPositionAsync([Path] int? proposalpositionid, [Query] string userid);

        /// <summary>
        /// Eine bestehende Position des Bestellvorschlags ab&auml;ndern
        ///
        /// UpdatePurchaseOrderProposalPosition (/api/v1/purchaseorder/proposalposition/{proposalpositionid})
        /// </summary>
        /// <param name="content">Repr&auml;sentiert die optional zu ver&auml;ndernden Eigenschafteneiner Position des Bestellvorschlags</param>
        /// <param name="proposalpositionid">Id der abzu&auml;ndernden Position des Bestellvorschlags</param>
        /// <param name="userid">ist der beim Logon ermittelte "token"</param>
        [Put("/api/v1/purchaseorder/proposalposition/{proposalpositionid}")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> UpdatePurchaseOrderProposalPositionAsync([Body] UpdatePurchaseOrderProposalPositionEntry content, [Path] int? proposalpositionid, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Mitarbeiter ermitteln
        ///
        /// GetStaff (/api/v1/staff)
        /// </summary>
        /// <param name="limit">die Anzahl der auszugebenden Datens&auml;tze. Wird der Parameter nicht angegeben,so werden bis zu 50 Datens&auml;tze geliefert. Wird 0 angegeben, werden alle S&auml;tzeausgegeben</param>
        /// <param name="startIndex">ist jene (staffEntry.)Id ab der die Auflistung erfolgen soll. Speziell beiseitenweiser Auflistung - Parameter limit ist gesetzt - hilfreich um den Startdatensatz derSeite zu erhalten</param>
        /// <param name="userid">des am HELIUM V Servers angemeldeten Benutzers</param>
        [Get("/api/v1/staff")]
        Task<List<StaffEntry>> GetStaffAsync([Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Lager
        ///
        /// GetStockList (/api/v1/stock)
        /// </summary>
        /// <param name="userid">des bei HELIUM V angemeldeten API Benutzer</param>
        [Get("/api/v1/stock")]
        Task<StockEntryList> GetStockListAsync([Query] string userid);

        /// <summary>
        /// Einen Lagerabgang durchf&uuml;hrenDer Anwender (userId) muss das Recht haben, Handlagerbewegungendurchzuf&uuml;hren. Ebenso muss dieser das Recht haben, auf dasabzubuchende Lager zuzugreifen.
        ///
        /// CreateStockMovementIssue (/api/v1/stock/{stockid}/movement/{itemid})
        /// </summary>
        /// <param name="content">Die Handlagerbewegung</param>
        /// <param name="stockid">ist die Id des Lagers von dem abgebucht wird. Der Anwender muss das Recht haben auf diesesLager zuzugreifen</param>
        /// <param name="itemid">ist die Id des zuzubuchenden Artikels. Es muss sich um einenlagerbewirtschafteten Identartikel handeln</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde.Dieser Anwender ben&ouml;tigt das Recht Handlagerbewegungen durchf&uuml;hren zu d&uuml;rfen</param>
        [Delete("/api/v1/stock/{stockid}/movement/{itemid}")]
        [Header("Content-Type", "application/json")]
        Task<int> CreateStockMovementIssueAsync([Body] StockMovementEntry content, [Path] int? stockid, [Path] int? itemid, [Query] string userid);

        /// <summary>
        /// Einen Lagerzugang durchf&uuml;hrenDer Anwender (userId) muss das Recht haben, Handlagerbewegungendurchzuf&uuml;hren. Ebenso muss dieser das Recht haben, auf daszuzubuchende Lager zuzugreifen.
        ///
        /// CreateStockMovementReceipt (/api/v1/stock/{stockid}/movement/{itemid})
        /// </summary>
        /// <param name="content">Die Handlagerbewegung</param>
        /// <param name="stockid">ist die Id des Lagers auf das zugebucht wird. Der Anwender muss das Recht haben auf diesesLager zugreifen zu d&uuml;rfen</param>
        /// <param name="itemid">ist die Id des zuzubuchenden Artikels. Es muss sich um einenlagerbewirtschafteten Identartikel handeln</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde.Dieser Anwender ben&ouml;tigt das Recht Handlagerbewegungen durchf&uuml;hren zu d&uuml;rfen</param>
        [Post("/api/v1/stock/{stockid}/movement/{itemid}")]
        [Header("Content-Type", "application/json")]
        Task<int> CreateStockMovementReceiptAsync([Body] StockMovementEntry content, [Path] int? stockid, [Path] int? itemid, [Query] string userid);

        /// <summary>
        /// Eine Lagerumbuchung durchf&uuml;hrenEs wird eine Abbuchung und eine Zubuchung durchgef&auuml;hrt
        ///
        /// CreateStockMovementChange (/api/v1/stock/{stockid}/movement/{itemid})
        /// </summary>
        /// <param name="content">Die Daten der Handlagerbewegung f&uuml;r eine Umbuchung</param>
        /// <param name="stockid">ist die Id des Lagers vom dem abgebucht wird. Der Anwender muss das Recht haben auf diesesLager zuzugreifen</param>
        /// <param name="itemid">ist die Id des umzubuchenden Artikels. Es muss sich um einenlagerbewirtschafteten Identartikel handeln</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde.Dieser Anwender ben&ouml;tigt das Recht Handlagerbewegungen durchf&uuml;hren zu d&uuml;rfen</param>
        [Put("/api/v1/stock/{stockid}/movement/{itemid}")]
        [Header("Content-Type", "application/json")]
        Task<int> CreateStockMovementChangeAsync([Body] StockMovementChangeEntry content, [Path] int? stockid, [Path] int? itemid, [Query] string userid);

        /// <summary>
        /// Lagerplatz optional &uuml;ber seine Id oder seinen Namen finden.Der Lagerplatz enth&auml;lt auch eine Liste aller Artikel, die diesem zugeordnet sind.
        ///
        /// FindStockPlace (/api/v1/stock/{stockid}/place)
        /// </summary>
        /// <param name="stockid">ist die Id des Lagers in dem sich der Lagerplatz befindet</param>
        /// <param name="addStockAmountInfos">(optional) f&uuml;gt Lagerinformation hinzu, der Lagerstand darin ist jener des Lagers der Ressource (stockid)</param>
        /// <param name="stockplaceid">(optional) ist die Id des Lagerplatzes</param>
        /// <param name="stockplacename">(optional) ist der Name des Lagerplatzes</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde</param>
        [Get("/api/v1/stock/{stockid}/place")]
        Task<StockPlaceEntry> FindStockPlaceAsync([Path] int? stockid, [Query] bool? addStockAmountInfos, [Query] int? stockplaceid, [Query] string stockplacename, [Query] string userid);

        /// <summary>
        /// Ordnet einen Artikel zu einem existierenden Lagerplatz zu.
        ///
        /// CreateStockPlace (/api/v1/stock/{stockid}/place)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="stockid">ist die Id des Lagers in dem sich der Lagerplatz befindet</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde</param>
        [Post("/api/v1/stock/{stockid}/place")]
        [Header("Content-Type", "application/json")]
        Task<int> CreateStockPlaceAsync([Body] StockPlacePostEntry content, [Path] int? stockid, [Query] string userid);

        /// <summary>
        /// L&ouml;scht die Zuordnung eines Artikel zu einem Lagerplatz
        ///
        /// DeleteStockPlace (/api/v1/stock/{stockid}/place/{stockplaceid})
        /// </summary>
        /// <param name="stockid">ist die Id des Lagers in dem sich der Lagerplatz befindet</param>
        /// <param name="stockplaceid">ist die Id des Lagerplatzes</param>
        /// <param name="itemid">ist die Id des Artikels</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde</param>
        [Delete("/api/v1/stock/{stockid}/place/{stockplaceid}")]
        Task<Response<object>> DeleteStockPlaceAsync([Path] int? stockid, [Path] int? stockplaceid, [Query] int? itemid, [Query] string userid);

        /// <summary>
        /// Eine Liste aller im System befindlichen Kostentr&auml;ger ermitteln
        ///
        /// GetCostBearingUnits (/api/v1/system/costbearingunit)
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/system/costbearingunit")]
        Task<CostBearingUnitEntryList> GetCostBearingUnitsAsync([Query] int? limit, [Query] string startIndex, [Query] string userid);

        /// <summary>
        /// Die Liste aller im System bekannten L&auml;nder
        ///
        /// GetCountries (/api/v1/system/country)
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/system/country")]
        Task<CountryEntryList> GetCountriesAsync([Query] int? limit, [Query] string startIndex, [Query] string userid);

        /// <summary>
        /// Eine Liste aller im System befindlichen Einheiten (Artikel)description wird dabei in der Sprache geliefert, mitder der API-Anwender angemeldet ist. Sollte keine &Uuml;bersetzungf&uuml;r die Sprache verf&uuml;gbar sein, ist die Property nicht vorhanden/leer.
        ///
        /// GetItemUnits (/api/v1/system/itemunit)
        /// </summary>
        /// <param name="cnr">die optionale Einheit die ermittelt werden soll</param>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/system/itemunit")]
        Task<ItemUnitEntryList> GetItemUnitsAsync([Query] string cnr, [Query] int? limit, [Query] string startIndex, [Query] string userid);

        /// <summary>
        /// Pr&uuml;ft, ob ein Zugriff auf den API-Server m&ouml;glich ist.Liefert zus&auml;tzlich Informationen &uuml;ber den API Server
        ///
        /// Localping (/api/v1/system/localping)
        /// </summary>
        [Get("/api/v1/system/localping")]
        Task<LocalPingResult> LocalpingAsync();

        /// <summary>
        /// Einen Log-Eintrag erzeugen
        ///
        /// LogMessage (/api/v1/system/log)
        /// </summary>
        /// <param name="content"></param>
        [Post("/api/v1/system/log")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> LogMessageAsync([Body] LogMessageEntry content);

        /// <summary>
        /// Einen "unbekannten" Barcode protokollieren
        ///
        /// LogBarcode (/api/v1/system/logbarcode)
        /// </summary>
        /// <param name="content"></param>
        [Post("/api/v1/system/logbarcode")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> LogBarcodeAsync([Body] LogBarcodeEntry content);

        /// <summary>
        /// Pr&uuml;ft, ob vom API-Server ein Zugriff auf den HELIUM V Server m&ouml;glich ist.Liefert zus&auml;tzlich Informationen &uuml;ber den verbundenen HELIUM V Server
        ///
        /// Ping (/api/v1/system/ping)
        /// </summary>
        [Get("/api/v1/system/ping")]
        Task<PingResult> PingAsync();

        /// <summary>
        /// Eine Liste aller im System befindlichen Mehrwertsteuersatzbezeichnungen
        ///
        /// GetTaxDescriptions (/api/v1/system/taxdescription)
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/system/taxdescription")]
        Task<TaxDescriptionEntryList> GetTaxDescriptionsAsync([Query] int? limit, [Query] string startIndex, [Query] string userid);

        /// <summary>
        /// Eine Liste aller im System befindlichen Textbausteine ermitteln
        ///
        /// GetTextBlocks (/api/v1/system/textblock)
        /// </summary>
        /// <param name="addContents">wenn true werden auch die Inhalte der Textbausteine- also die Texte, oder Bildinhalte - &uuml;bermittelt</param>
        /// <param name="filterWithHidden"></param>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/system/textblock")]
        Task<TextblockEntryList> GetTextBlocksAsync([Query] bool? addContents, [Query(Name = "filter_withHidden")] bool? filterWithHidden, [Query] int? limit, [Query] string startIndex, [Query] string userid);

        /// <summary>
        /// GetTodos (/api/v1/todo)
        /// </summary>
        /// <param name="filterPartnername"></param>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/todo")]
        Task<TodoEntryList> GetTodosAsync([Query(Name = "filter_partnername")] string filterPartnername, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// GetApiInfo (/api/v1/traveltime/api)
        /// </summary>
        /// <param name="userid"></param>
        [Get("/api/v1/traveltime/api")]
        Task<TravelTimeApiInfo> GetApiInfoAsync([Query] string userid);

        /// <summary>
        /// BookBatch (/api/v1/traveltime/batch)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="userid"></param>
        [Post("/api/v1/traveltime/batch")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> BookBatchAsync([Body] TravelTimeRecordingBatchEntryList content, [Query] string userid);

        /// <summary>
        /// GetDailyAllowances (/api/v1/traveltime/dailyallowance)
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="startIndex"></param>
        /// <param name="userid"></param>
        [Get("/api/v1/traveltime/dailyallowance")]
        Task<DailyAllowanceEntryList> GetDailyAllowancesAsync([Query] int? limit, [Query] string startIndex, [Query] string userid);

        /// <summary>
        /// Liefert eine Liste aller verf&uuml;gbaren T&auml;tigkeiten (Arbeitszeitartikel) die innerhalb der Zeiterfassungdurchgef&uuml;hrt werden k&ouml;nnen. 
        ///
        /// GetActivities (/api/v1/worktime/activities)
        /// </summary>
        /// <param name="filterCnr">(optional) die Sondert&auml;tigkeiten auf diese Kennung einschr&auml;nken</param>
        /// <param name="limit">(optional) die maximale Anzahl von gelieferten Eintr&auml;gen. Default ist 50.</param>
        /// <param name="startIndex">(optional) die Id (eines ItemEntry Eintrags, mit dem die Liste beginnen soll</param>
        /// <param name="userid">des am HELIUM V Servers angemeldeten Benutzers</param>
        [Get("/api/v1/worktime/activities")]
        Task<List<ItemEntry>> GetActivitiesAsync([Query(Name = "filter_cnr")] string filterCnr, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Eine HELIUM V Barcode Zeitbuchung erzeugen.
        ///
        /// BookBarcode (/api/v1/worktime/barcode)
        /// </summary>
        /// <param name="content"></param>
        [Post("/api/v1/worktime/barcode")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> BookBarcodeAsync([Body] BarcodeRecordingEntry content);

        /// <summary>
        /// BookBatch2 (/api/v1/worktime/batch)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="userid"></param>
        [Post("/api/v1/worktime/batch")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> BookBatch2Async([Body] TimeRecordingBatchEntryList content, [Query] string userid);

        /// <summary>
        /// Eine KOMMT-Buchung durchf&uuml;hren.
        ///
        /// BookComing (/api/v1/worktime/coming)
        /// </summary>
        /// <param name="content"></param>
        [Post("/api/v1/worktime/coming")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> BookComingAsync([Body] TimeRecordingEntry content);

        /// <summary>
        /// Liefert eine Liste aller verf&uuml;gbaren Belegarten die f&uuml;r die Zeiterfassung verwendet werden k&ouml;nnen.Belegarten sind typischerweise Angebot, Auftrag, Los oder Projekt
        ///
        /// GetDocumentTypes (/api/v1/worktime/documenttypes)
        /// </summary>
        /// <param name="userid">der am HELIUM V Server angemeldete Benutzer</param>
        [Get("/api/v1/worktime/documenttypes")]
        Task<List<DocumentType>> GetDocumentTypesAsync([Query] string userid);

        /// <summary>
        /// Eine GEHT-Buchung durchf&uuml;hren.
        ///
        /// BookGoing (/api/v1/worktime/going)
        /// </summary>
        /// <param name="content"></param>
        [Post("/api/v1/worktime/going")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> BookGoingAsync([Body] TimeRecordingEntry content);

        /// <summary>
        /// Ein Start oder Stop einer Maschine durchf&uuml;hren.Die anzugebende Maschine-Id kann &uuml;ber die Resource machine ermittelt werden 
        ///
        /// BookMachine (/api/v1/worktime/machine)
        /// </summary>
        /// <param name="content"></param>
        [Post("/api/v1/worktime/machine")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> BookMachineAsync([Body] MachineRecordingEntry content);

        /// <summary>
        /// GetMonthlyReport (/api/v1/worktime/monthlyreport)
        /// </summary>
        /// <param name="filterWithHidden"></param>
        /// <param name="month"></param>
        /// <param name="personalid"></param>
        /// <param name="selectoption"></param>
        /// <param name="sortoption"></param>
        /// <param name="toendofmonth"></param>
        /// <param name="userid"></param>
        /// <param name="year"></param>
        [Get("/api/v1/worktime/monthlyreport")]
        Task<MonthlyReportEntry> GetMonthlyReportAsync([Query(Name = "filter_withHidden")] bool? filterWithHidden, [Query] int? month, [Query] int? personalid, [Query] string selectoption, [Query] string sortoption, [Query] bool? toendofmonth, [Query] string userid, [Query] int? year);

        /// <summary>
        /// Eine (Beginn) Buchung mit Auftragsbezug erzeugen.Die anzugebende Order-Id kann &uuml;ber die Resource order ermittelt werden
        ///
        /// BookOrder (/api/v1/worktime/order)
        /// </summary>
        /// <param name="content">Die Zeitdaten einer T&auml;tigkeit</param>
        [Post("/api/v1/worktime/order")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> BookOrderAsync([Body] OrderRecordingEntry content);

        /// <summary>
        /// Eine PAUSE (Unterbrechung)-Buchung durchf&uuml;hren.Eine Pause (zum Beispiel Mittagspause) wird durch zwei PAUSEBuchungen erzielt.
        ///
        /// BookPausing (/api/v1/worktime/pausing)
        /// </summary>
        /// <param name="content"></param>
        [Post("/api/v1/worktime/pausing")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> BookPausingAsync([Body] TimeRecordingEntry content);

        /// <summary>
        /// Eine (Beginn) Buchung eines Los durchf&uuml;hren.
        ///
        /// BookProduction (/api/v1/worktime/production)
        /// </summary>
        /// <param name="content">Die Zeitdaten einer T&auml;tigkeit</param>
        [Post("/api/v1/worktime/production")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> BookProductionAsync([Body] ProductionRecordingEntry content);

        /// <summary>
        /// Eine (Beginn) Buchung eines Projekts durchf&uuml;hren.
        ///
        /// BookProject (/api/v1/worktime/project)
        /// </summary>
        /// <param name="content">Die Zeitdaten einer T&auml;tigkeit</param>
        [Post("/api/v1/worktime/project")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> BookProjectAsync([Body] ProjectRecordingEntry content);

        /// <summary>
        /// Liefert eine Liste aller verf&uuml;gbaren Sondert&auml;tigkeiten die f&uuml;r die Zeiterfassungzur Verf&uuml;gung stehen.Sondert&auml;tigkeiten sind T&auml;tigkeiten wie "KOMMT", "GEHT", "ARZT", "BEH&Ouml;RDE", ...Es k&ouml;nnen nur jene Sondert&auml;tigkeiten gebucht werden, die laut HELIUM V Konfigurationf&uuml;r den Anwender beziehungsweise dessen Benutzerrolle zur Verf&uuml;gung steht.
        ///
        /// GetSpecialActivities (/api/v1/worktime/specialactivities)
        /// </summary>
        /// <param name="userid"></param>
        [Get("/api/v1/worktime/specialactivities")]
        Task<List<SpecialActivity>> GetSpecialActivitiesAsync([Query] string userid);

        /// <summary>
        /// BookSpecialTimes (/api/v1/worktime/specialtimes)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="userid"></param>
        [Post("/api/v1/worktime/specialtimes")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> BookSpecialTimesAsync([Body] SpecialTimesEntryList content, [Query] string userid);

        /// <summary>
        /// Eine ENDE Buchung durchf&uuml;hrenEine Belegbuchung wie beispielsweise Auftrags-, Projekt oder Los-Buchung beenden
        ///
        /// BookStopping (/api/v1/worktime/stopping)
        /// </summary>
        /// <param name="content"></param>
        [Post("/api/v1/worktime/stopping")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> BookStoppingAsync([Body] TimeRecordingEntry content);

        /// <summary>
        /// Liefert das Zeitsaldo des angeforderten Benutzers inklusive seines verf&uuml;gbaren Urlaubs
        ///
        /// GetTimeBalance (/api/v1/worktime/timebalance)
        /// </summary>
        /// <param name="day">der Tag (1-31)</param>
        /// <param name="forStaffCnr">ist die optionale Personalnummer f&uuml;r die der Zeitsaldo abgerufen werden soll.Sind sowohl forStaffId als auch forStaffCnr angegeben, wird forStaffId verwendet. Sind beidenicht angegeben, wird das Zeitsaldo des angemeldeten Benutzer abgerufen.</param>
        /// <param name="forStaffId">ist die optionale PersonalId f&uuml;r die der Zeitsaldo abgerufen werden soll.Der angemeldete Benutzer kann f&uuml;r jene Personen f&uuml;r die er ausreichend Rechte hat Zeitsalden abrufen.</param>
        /// <param name="month">ist das Monat (1-12)</param>
        /// <param name="userid">der angemeldete Benutzer</param>
        /// <param name="year">ist das Jahr f&uuml;r das der Zeitsaldo abgerufen werden soll</param>
        [Get("/api/v1/worktime/timebalance")]
        Task<TimeBalanceEntry> GetTimeBalanceAsync([Query] int? day, [Query] string forStaffCnr, [Query] int? forStaffId, [Query] int? month, [Query] string userid, [Query] int? year);

        /// <summary>
        /// Eine Zeitbuchung l&ouml;schen
        ///
        /// RemoveWorktime (/api/v1/worktime/{worktimeId})
        /// </summary>
        /// <param name="worktimeId">die Id der zu l&ouml;schenden Buchung.Die Zeitbuchungen (und damit auch deren Id)kann &uuml;ber die GET Methode ermittelt werden.</param>
        /// <param name="forStaffCnr">ist die optionale Personalnummer f&uuml;r die gel&ouml;scht werden soll.Sind sowohl forStaffId als auch forStaffCnr angegeben, wird forStaffId verwendet. Sind beidenicht angegeben, wird das L&ouml;schen mit dem angemeldeten Benutzer durchgef&uuml;hrt</param>
        /// <param name="forStaffId">ist die optionale PersonalId f&uuml;r die gel&ouml;scht werden soll.Der angemeldete Benutzer kann f&uuml;r jene Personen f&uuml;r die er ausreichend Rechte hat Zeitbuchungen l&ouml;schen.</param>
        /// <param name="userId">der angemeldete Benutzer</param>
        [Delete("/api/v1/worktime/{worktimeId}")]
        Task<Response<object>> RemoveWorktimeAsync([Path] int? worktimeId, [Query] string forStaffCnr, [Query] int? forStaffId, [Query] string userId);

        /// <summary>
        /// Liefert alle ZeitdatenEntry f&uuml;r den angegebenen Tag.
        ///
        /// GetWorktimeEntries (/api/v1/worktime/{year}/{month}/{day})
        /// </summary>
        /// <param name="year">ist das Jahr f&uuml;r das die Zeitdaten abgerufen werden sollen</param>
        /// <param name="month">ist das Monat (1-12)</param>
        /// <param name="day">der Tag (1-31)</param>
        /// <param name="forStaffCnr">ist jene Personalnummer f&uuml;r welche die Zeitdaten abgerufen werden soll. Kann auch leer sein.Ist sowohl forStaffId als auch forStaffCnr angegeben, gilt forStaffId</param>
        /// <param name="forStaffId">ist jene Benutzer-Id f&uuml;r welche die Zeitdaten abgerufen werden sollen. Kann auch leer sein</param>
        /// <param name="limit">Ist die maximale Anzahl an Datens&auml;tzen. Default 50.</param>
        /// <param name="userId">enth&auml;lt den angemeldeten Benutzer</param>
        [Get("/api/v1/worktime/{year}/{month}/{day}")]
        Task<List<ZeitdatenEntry>> GetWorktimeEntriesAsync([Path] int? year, [Path] int? month, [Path] int? day, [Query] string forStaffCnr, [Query] int? forStaffId, [Query] int? limit, [Query] string userId);

        /// <summary>
        /// Einen Artikel anhand seiner Artikelnummer ermitteln
        ///
        /// FindItemByAttributes2 (/api/v11/item)
        /// </summary>
        /// <param name="addComments">(optional) mit true die Artikelkommentar der Art text/html ebenfalls liefern</param>
        /// <param name="addStockAmountInfos">(optional) mit true die allgemeinen Lagerstandsinformationen liefern</param>
        /// <param name="itemCnr">(optional) die gesuchte Artikelnummer</param>
        /// <param name="itemSerialnumber">(optional) die Seriennummer des ArtikelsEineindeutige Artikel k&ouml;nnen &uuml;ber ihre Seriennummer ermittelt werden. Dabei wirdzuerst im aktuellen Lagerstand gesucht, danach in den Abgangsbuchungen. Ist die cnrebenfalls angegeben, muss der Artikel der &uuml;ber die Seriennummer ermittelt wurde mit derangegebenen Artikelnummer &uuml;bereinstimmen.</param>
        /// <param name="userid">des bei HELIUM V angemeldeten API Benutzer</param>
        [Get("/api/v11/item")]
        Task<ItemEntry> FindItemByAttributes2Async([Query] bool? addComments, [Query] bool? addStockAmountInfos, [Query] string itemCnr, [Query] string itemSerialnumber, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Artikelgruppen ermittelnDiese Methode ist f&uuml;r die Verwendung in Listboxen/Comboboxen gedacht
        ///
        /// GetItemGroupsList (/api/v11/item/grouplist)
        /// </summary>
        /// <param name="userid">des angemeldeten HELIUM V Benutzer</param>
        [Get("/api/v11/item/grouplist")]
        Task<ItemGroupEntryList> GetItemGroupsListAsync([Query] string userid);

        /// <summary>
        /// Eine Liste aller Artikelgruppen ermitteln.
        ///
        /// GetItemGroups2 (/api/v11/item/groups)
        /// </summary>
        /// <param name="userid">der angemeldete HELIUM V Benutzer</param>
        [Get("/api/v11/item/groups")]
        Task<ItemGroupEntryList> GetItemGroups2Async([Query] string userid);

        /// <summary>
        /// Liefert eine (leere) Liste aller Artikel die die angegebene Herstellernummer exakt enthaltenEs werden alle Artikel geliefert, die die angegebene Herstellernummer enthalten, egalum welchen Hersteller es sich handelt. Beginnt die hier angegebene Nummer mit einembekannten (Barcode)Lead-In, wird dieser vor der Suche entfernt. 
        ///
        /// FindItemV1ByManufacturerBarcode (/api/v11/item/itemslistmanufacturer)
        /// </summary>
        /// <param name="addComments">(optional) mit true die Artikelkommentar der Art text/html ebenfalls liefern</param>
        /// <param name="addCommentsMedia">(optional) mit true die Infos &uuml;ber die Artikelkommentare liefern</param>
        /// <param name="addDocuments">(optional) mit true die Infos &uuml;ber die vom angemeldeten Benutzersichtbaren Dokumente aus der Dokumentenablage liefern</param>
        /// <param name="addProducerInfos">(optional) mit true die herstellerspezifischen Infos (Artikelnummer,Bezeichnung des Herstellers, Leadin, ...) liefern</param>
        /// <param name="addStockAmountInfos">(optional) mit true die allgemeinen Lagerstandsinformationen liefern</param>
        /// <param name="addStockPlaceInfos">(optional) mit true die Infos der zugewiesenen Lagerpl&auml;tze liefern</param>
        /// <param name="manufacturerbarcode">die Herstellernummer</param>
        /// <param name="userid">des angemeldeten HELIUM V Benutzers</param>
        [Get("/api/v11/item/itemslistmanufacturer")]
        Task<ItemV1EntryList> FindItemV1ByManufacturerBarcodeAsync([Query] bool? addComments, [Query] bool? addCommentsMedia, [Query] bool? addDocuments, [Query] bool? addProducerInfos, [Query] bool? addStockAmountInfos, [Query] bool? addStockPlaceInfos, [Query] string manufacturerbarcode, [Query] string userid);

        /// <summary>
        /// Einen Artikel anhand seiner Artikelnummer ermitteln
        ///
        /// FindItemV1ByAttributes (/api/v11/item/itemv1)
        /// </summary>
        /// <param name="addComments">(optional) mit true die Artikelkommentar der Art text/html ebenfalls liefern</param>
        /// <param name="addCommentsMedia">(optional) mit true die Infos &uuml;ber die Artikelkommentare liefern</param>
        /// <param name="addDocuments">(optional) mit true die Infos &uuml;ber die vom angemeldeten Benutzersichtbaren Dokumente aus der Dokumentenablage liefern</param>
        /// <param name="addProducerInfos">(optional) mit true die herstellerspezifischen Infos (Artikelnummer,Bezeichnung des Herstellers, Leadin, ...) liefern</param>
        /// <param name="addStockAmountInfos">(optional) mit true die allgemeinen Lagerstandsinformationen liefern</param>
        /// <param name="addStockPlaceInfos">(optional) mit true die Infos der zugewiesenen Lagerpl&auml;tze liefern</param>
        /// <param name="itemBarcode">(optional) der EAN / Barcode des Artikels</param>
        /// <param name="itemCnr">(optional) die gesuchte Artikelnummer</param>
        /// <param name="itemSerialnumber">(optional) die Seriennummer des ArtikelsEineindeutige Artikel k&ouml;nnen &uuml;ber ihre Seriennummer ermittelt werden. Dabei wirdzuerst im aktuellen Lagerstand gesucht, danach in den Abgangsbuchungen. Ist die cnrebenfalls angegeben, muss der Artikel der &uuml;ber die Seriennummer ermittelt wurde mit derangegebenen Artikelnummer &uuml;bereinstimmen.</param>
        /// <param name="userid">des bei HELIUM V angemeldeten API Benutzer</param>
        [Get("/api/v11/item/itemv1")]
        Task<ItemV1Entry> FindItemV1ByAttributesAsync([Query] bool? addComments, [Query] bool? addCommentsMedia, [Query] bool? addDocuments, [Query] bool? addProducerInfos, [Query] bool? addStockAmountInfos, [Query] bool? addStockPlaceInfos, [Query] string itemBarcode, [Query] string itemCnr, [Query] string itemSerialnumber, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Artikel ermitteln.Das Ergebnis kann dabei durch Filter eingeschr&auml;nkt werden
        ///
        /// GetItemsList (/api/v11/item/list)
        /// </summary>
        /// <param name="orderby">definiert die Sortierung der Daten. Es k&ouml;nnen durch Komma getrennt mehrereDatenfelder angegeben werden. Getrennt durch Leerzeichen kann "asc" (aufsteigend) oder auch "desc" (absteigend)sortiert werden. "cnr desc" oder auch "cnr asc, customerItemCnr desc", oder auch "cnr, customerItemCnr desc"</param>
        /// <param name="filterCnr">die (optionale) Artikelnummer nach der die Suche eingeschr&auml;nkt werden soll</param>
        /// <param name="filterCustomeritemcnr">die (optionale) Kundenartikelnummer nach der gesucht werden kann</param>
        /// <param name="filterDeliverycnr">auf die (optionale) Lieferantennr. bzw Bezeichnung einschr&auml;nken</param>
        /// <param name="filterItemgroupclass">auf die (optionale) Artikelgruppe bzw. Artikelklasse einschr&auml;nken</param>
        /// <param name="filterItemgroupid">die (optionale) IId der Artikelgruppe in der die Artikel gesucht werden. Diecnr wird aus der angegebenen iid ermittelt und dann auch f&uuml;r die Artikelklasse verwendet</param>
        /// <param name="filterItemgroupids"></param>
        /// <param name="filterItemreferencenr">auf die (optionale) Artikelreferenznummer einschr&auml;nken</param>
        /// <param name="filterTextsearch">der (optionale) Text der die Suche einschr&auml;nkt</param>
        /// <param name="filterWithHidden">mit true werden auch versteckte Artikel in die Suche einbezogen</param>
        /// <param name="limit">die maximale Anzahl von zur&uuml;ckgelieferten Datens&auml;tzen</param>
        /// <param name="startIndex">die Index-Nummer desjenigen Satzes mit dem begonnen werden soll</param>
        /// <param name="userid">des angemeldeten HELIUM V Benutzer</param>
        [Get("/api/v11/item/list")]
        Task<ItemEntryList> GetItemsListAsync([Query] string orderby, [Query(Name = "filter_cnr")] string filterCnr, [Query(Name = "filter_customeritemcnr")] string filterCustomeritemcnr, [Query(Name = "filter_deliverycnr")] string filterDeliverycnr, [Query(Name = "filter_itemgroupclass")] string filterItemgroupclass, [Query(Name = "filter_itemgroupid")] int? filterItemgroupid, [Query(Name = "filter_itemgroupids")] string filterItemgroupids, [Query(Name = "filter_itemreferencenr")] string filterItemreferencenr, [Query(Name = "filter_textsearch")] string filterTextsearch, [Query(Name = "filter_withHidden")] bool? filterWithHidden, [Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Die Artikelpreisliste ermittelnAbh&auml;ngig vom Umfang der Artikel bzw. der Preisliste kann dieser Aufruf auchmehrere Sekunden oder sogar Minuten dauern. Die R&uuml;ckgabe kann auch mehrere MB gro&szlig; sein.
        ///
        /// GetPriceList2 (/api/v11/item/pricelist)
        /// </summary>
        /// <param name="filterItemclasscnr">optional nur Artikel mit dieser Artikelklassenummer</param>
        /// <param name="filterItemclassid">optional nur Artikel mit dieser ArtikelklassenId</param>
        /// <param name="filterItemgroupcnr">optional nur Artikel mit dieser Artikelgruppe selektieren</param>
        /// <param name="filterItemgroupid">optional nur Artikel mit dieser ArtikelgruppenId selektieren</param>
        /// <param name="filterShopgroupcnr">optional nur Artikel mit dieser Shopgruppekennung (oder Kindern dieserShopgruppe) selektieren</param>
        /// <param name="filterShopgroupid">optional nur Artikel mit dieser ShopgruppenId (oder Kindern dieserShopgruppenId) selektieren</param>
        /// <param name="filterStartItemcnr">optional nur Artikel ab dieser Artikelnummer</param>
        /// <param name="filterStopItemcnr">optional nur Artikel bis (einschlie&szlig;lich) zu dieser Artikelnummer</param>
        /// <param name="filterValiditydate">optional mit dem angebenen Preisg&uuml;ltigkeitsdatum im ISO8601 Format(Beispiel: JJJJ-MM-TT"T"hh:mm ... 2013-12-31T14:00Z oder 2013-12-31T14:59+01:00)</param>
        /// <param name="filterWithHidden">mit true werden auch versteckte Artikel in die Preisliste aufgenommen</param>
        /// <param name="pricelistCnr">wird die Preislistenkennung angegeben, wird der Preis entsprechend derPreisliste ausgegeben. Wird keine Kennung angegeben, wird die "Verkaufspreisbasis" desArtikels ausgegeben</param>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde</param>
        [Get("/api/v11/item/pricelist")]
        Task<CustomerPricelistReportDto> GetPriceList2Async([Query(Name = "filter_itemclasscnr")] string filterItemclasscnr, [Query(Name = "filter_itemclassid")] int? filterItemclassid, [Query(Name = "filter_itemgroupcnr")] string filterItemgroupcnr, [Query(Name = "filter_itemgroupid")] int? filterItemgroupid, [Query(Name = "filter_shopgroupcnr")] string filterShopgroupcnr, [Query(Name = "filter_shopgroupid")] int? filterShopgroupid, [Query(Name = "filter_start_itemcnr")] string filterStartItemcnr, [Query(Name = "filter_stop_itemcnr")] string filterStopItemcnr, [Query(Name = "filter_validitydate")] string filterValiditydate, [Query(Name = "filter_withHidden")] bool? filterWithHidden, [Query] string pricelistCnr, [Query] string userid);

        /// <summary>
        /// Eine (leere) Liste aller verf&uuml;gbaren Preislisten
        ///
        /// GetPriceLists (/api/v11/item/pricelists)
        /// </summary>
        /// <param name="userid">ist der Token der durch die Anmeldung (login) erhalten wurde</param>
        [Get("/api/v11/item/pricelists")]
        Task<PriceListEntryList> GetPriceListsAsync([Query] string userid);

        /// <summary>
        /// Eine Liste aller Artikeleigenschaften eines Artikels ermitteln
        ///
        /// GetItemProperties2 (/api/v11/item/properties)
        /// </summary>
        /// <param name="itemCnr">die gew&uuml;nschte Artikelnummer</param>
        /// <param name="userid">userId der angemeldete HELIUM V Benutzer</param>
        [Get("/api/v11/item/properties")]
        Task<ItemPropertyEntryList> GetItemProperties2Async([Query] string itemCnr, [Query] string userid);

        /// <summary>
        /// Erzeugt eine neue Eigenschaft eines Artikels &uuml;ber die Id der Beschreibung.Ist zur Id der Beschreibung bereits eine entsprechende Eigenschaft existent, so wird mit StatusCode CONFLICT (409) geantwortet.Eine Ausnahme bilden Eigenschaften, die mit ihrem Inhalt dem Defaultwert ihrer Beschreibung entsprechen.In diesem Fall wird diese Eigenschaft aktualisiert.
        ///
        /// CreateItemProperty (/api/v11/item/property)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="userid">des angemeldeten HELIUM V Benutzers</param>
        [Post("/api/v11/item/property")]
        [Header("Content-Type", "application/json")]
        Task<ItemPropertyEntry> CreateItemPropertyAsync([Body] CreateItemPropertyEntry content, [Query] string userid);

        /// <summary>
        /// Aktualisiert die Daten einer Eigenschaft eines Artikels.Die Eigenschaft muss bereits existieren, sonst wird mit StatusCode NOT_FOUND (404) geantwortet
        ///
        /// UpdateItemProperty (/api/v11/item/property)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="userid">des angemeldeten HELIUM V Benutzers</param>
        [Put("/api/v11/item/property")]
        [Header("Content-Type", "application/json")]
        Task<ItemPropertyEntry> UpdateItemPropertyAsync([Body] UpdateItemPropertyEntry content, [Query] string userid);

        /// <summary>
        /// Liefert die angeforderte Eigenschaft eines Artikels.Der Artikel kann optional &uuml;ber die Id oder Artikelnummer bestimmt werden.
        ///
        /// GetItemProperty (/api/v11/item/property/{propertyid})
        /// </summary>
        /// <param name="propertyid">Id der Eigenschaft des Artikels</param>
        /// <param name="itemCnr">(optional) Artikelnummer</param>
        /// <param name="itemid">(optional) Id des Artikels. Ist auch die Artikelnummer angegeben hat die Id Priorit&auml;t</param>
        /// <param name="userid">des angemeldeten HELIUM V Benutzers</param>
        [Get("/api/v11/item/property/{propertyid}")]
        Task<ItemPropertyEntry> GetItemPropertyAsync([Path] int? propertyid, [Query] string itemCnr, [Query] int? itemid, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Beschreibungen der Eigenschaften des Artikels.Der Artikel kann optional &uuml;ber die Id oder Artikelnummer bestimmt werden.
        ///
        /// GetPropertyLayouts (/api/v11/item/propertylayouts)
        /// </summary>
        /// <param name="itemCnr">(optional) Artikelnummer</param>
        /// <param name="itemid">(optional) Id des Artikels. Ist auch die Artikelnummer angegeben hat die Id Priorit&auml;t</param>
        /// <param name="userid">des angemeldeten HELIUM V Benutzers</param>
        [Get("/api/v11/item/propertylayouts")]
        Task<PropertyLayoutEntryList> GetPropertyLayoutsAsync([Query] string itemCnr, [Query] int? itemid, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Shopgruppen ermitteln
        ///
        /// GetShopGroupsList (/api/v11/item/shopgrouplist)
        /// </summary>
        /// <param name="limit">(optional) die maximale Anzahl von zu liefernden Shopgruppen. Bei 0 werden alle geliefert</param>
        /// <param name="startIndex">(optional) die Index-Nummer desjenigen Satzes mit dem begonnen werden soll</param>
        /// <param name="userid">des angemeldeten HELIUM V Benutzer</param>
        [Get("/api/v11/item/shopgrouplist")]
        Task<ShopGroupEntryList> GetShopGroupsListAsync([Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Liefert eine Liste aller Lagerst&auml;nde dieses ArtikelsEs werden nur Lager geliefert, die einen Lagerstand > 0 haben. Es werden nur jeneLagerst&auml;nde geliefert, f&uuml;r die der Benutzer das Recht hat das jeweilige Lager zu benutzen.
        ///
        /// GetStockAmount2 (/api/v11/item/stocks)
        /// </summary>
        /// <param name="itemCnr">die gesuchte Artikelnummer</param>
        /// <param name="returnItemInfo">mit true werden neben den Lagerst&auml;nden auch die Daten desbetreffenden Artikels zur&uuml;ckgeliefert.</param>
        /// <param name="userid">der angemeldete API Benutzer</param>
        [Get("/api/v11/item/stocks")]
        Task<List<StockAmountEntry>> GetStockAmount2Async([Query] string itemCnr, [Query] bool? returnItemInfo, [Query] string userid);

        /// <summary>
        /// Liefert eine Liste aller Lagerst&auml;nde dieses ArtikelsEs werden nur Lager geliefert, die einen Lagerstand > 0 haben. Es werden nur jeneLagerst&auml;nde geliefert, f&uuml;r die der Benutzer das Recht hat das jeweilige Lager zu benutzen.
        ///
        /// GetStockAmountList (/api/v11/item/stockslist)
        /// </summary>
        /// <param name="addStockPlaceInfos">(optional) mit true die Infos der zugewiesenen Lagerpl&auml;tze liefern</param>
        /// <param name="itemCnr">die gesuchte Artikelnummer</param>
        /// <param name="returnAllStocks">(optional) mit true auch jene Lager liefern, die einen Lagerstand = 0 haben,f&uuml;r die der Benutzer aber das Recht hat das jeweilige Lager zu benutzen.</param>
        /// <param name="returnItemInfo">mit true werden neben den Lagerst&auml;nden auch die Daten desbetreffenden Artikels zur&uuml;ckgeliefert.</param>
        /// <param name="userid">der angemeldete API Benutzer</param>
        [Get("/api/v11/item/stockslist")]
        Task<StockAmountEntryList> GetStockAmountListAsync([Query] bool? addStockPlaceInfos, [Query] string itemCnr, [Query] bool? returnAllStocks, [Query] bool? returnItemInfo, [Query] string userid);

        /// <summary>
        /// GetCommentsMedia2 (/api/v11/item/{itemid}/commentmedia/list)
        /// </summary>
        /// <param name="itemid"></param>
        /// <param name="userid"></param>
        [Get("/api/v11/item/{itemid}/commentmedia/list")]
        Task<ItemCommentMediaInfoEntryList> GetCommentsMedia2Async([Path] int? itemid, [Query] string userid);

        /// <summary>
        /// GetCommentMedia2 (/api/v11/item/{itemid}/commentmedia/{itemcommentid})
        /// </summary>
        /// <param name="itemid"></param>
        /// <param name="itemcommentid"></param>
        /// <param name="userid"></param>
        [Get("/api/v11/item/{itemid}/commentmedia/{itemcommentid}")]
        Task<Response<object>> GetCommentMedia2Async([Path] int? itemid, [Path] int? itemcommentid, [Query] string userid);

        /// <summary>
        /// Verteilt die Restmenge der Charge zu gleichen Teilen auf ihre Verwendung in den Losenund liefert als Ergebnis eine Liste der betroffenen Lose.Die Lose in der Liste sind nur mit ihrer id und cnr bef&uuml;llt.F&uuml;r Detailinformation der Lose sind die Ressourcen der ProductionApi zu verwenden.Ist die Chargennummer nicht mehr auf Lager, so wird eine leere Liste retourniert.
        ///
        /// DiscardRemainingChargenumber (/api/v11/item/{itemid}/discardremaining)
        /// </summary>
        /// <param name="itemid">Id des Artikels</param>
        /// <param name="chargenr">Chargennummer des Artikels</param>
        /// <param name="userid">des angemeldeten HELIUM V Benutzer</param>
        [Put("/api/v11/item/{itemid}/discardremaining")]
        Task<ProductionEntryList> DiscardRemainingChargenumberAsync([Path] int? itemid, [Query] string chargenr, [Query] string userid);

        /// <summary>
        /// Ein Dokument anhand seiner Cnr eines Artikels ermitteln
        ///
        /// GetDocument3 (/api/v11/item/{itemid}/document)
        /// </summary>
        /// <param name="itemid">Id des Artikels</param>
        /// <param name="documentcnr">Id des Dokumentes</param>
        /// <param name="userid">des angemeldeten HELIUM V Benutzers</param>
        [Get("/api/v11/item/{itemid}/document")]
        Task<Response<object>> GetDocument3Async([Path] int? itemid, [Query] string documentcnr, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Dokumente des Artikels aus der Dokumentenablage ermitteln.Es werden alle Dokumente angef&uuml;hrt, die f&uuml;r der angemeldeten Benutzer sichtbar sind.Ein Element der Liste ent&auml;hlt aber nicht die tats&auml;chlichen Daten des Dokuments, sondernnur Metainformation.
        ///
        /// GetDocuments4 (/api/v11/item/{itemid}/document/list)
        /// </summary>
        /// <param name="itemid">Id des Artikels</param>
        /// <param name="userid">des angemeldeten HELIUM V Benutzers</param>
        [Get("/api/v11/item/{itemid}/document/list")]
        Task<DocumentInfoEntryList> GetDocuments4Async([Path] int? itemid, [Query] string userid);

        /// <summary>
        /// GetPrice (/api/v11/item/{itemid}/price)
        /// </summary>
        /// <param name="itemid"></param>
        /// <param name="amount"></param>
        /// <param name="customerid"></param>
        /// <param name="unit"></param>
        /// <param name="userid"></param>
        [Get("/api/v11/item/{itemid}/price")]
        Task<double> GetPriceAsync([Path] int? itemid, [Query] string amount, [Query] int? customerid, [Query] string unit, [Query] string userid);

        /// <summary>
        /// Das Etikett des Artikels ausdruckenDas Artikeletikett wird auf dem Drucker ausgedruckt, der dem Arbeitsplatz (des mobilen Endger&auml;tes)zugewiesen worden ist
        ///
        /// PrintLabel2 (/api/v11/item/{itemid}/printlabel)
        /// </summary>
        /// <param name="itemid">Id des Artikels</param>
        /// <param name="amount">(optional) Menge des Artikels</param>
        /// <param name="chargenr">(optional) Serien-/Chargennummer des Artikels</param>
        /// <param name="copies">(optional) Anzahl der Exemplare. Wenn nicht angegeben wird 1 Exemplar gedruckt</param>
        /// <param name="identitystockid">(optional) Id des Lagers der Serien-/Chargennummer, f&uuml;r den Andruck des Lagerstandes der Serien-/Chargennummer</param>
        /// <param name="userid">des angemeldeten HELIUM V Benutzers</param>
        [Get("/api/v11/item/{itemid}/printlabel")]
        Task<int> PrintLabel2Async([Path] int? itemid, [Query] string amount, [Query] string chargenr, [Query] int? copies, [Query] int? identitystockid, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Artikeleigenschaften eines Artikels ermitteln.
        ///
        /// GetItemPropertiesFromId2 (/api/v11/item/{itemid}/properties)
        /// </summary>
        /// <param name="itemid">die Id des gew&uuml;nschten Artikels</param>
        /// <param name="userid">userId der angemeldete HELIUM V Benutzer</param>
        [Get("/api/v11/item/{itemid}/properties")]
        Task<ItemPropertyEntryList> GetItemPropertiesFromId2Async([Path] int? itemid, [Query] string userid);

        /// <summary>
        /// Eine Liste aller Mitarbeiter ermitteln
        ///
        /// GetStaff2 (/api/v11/staff/list)
        /// </summary>
        /// <param name="limit">die Anzahl der auszugebenden Datens&auml;tze. Wird der Parameter nicht angegeben,so werden bis zu 50 Datens&auml;tze geliefert. Wird 0 angegeben, werden alle S&auml;tzeausgegeben</param>
        /// <param name="startIndex">ist jene (staffEntry.)Id ab der die Auflistung erfolgen soll. Speziell beiseitenweiser Auflistung - Parameter limit ist gesetzt - hilfreich um den Startdatensatz derSeite zu erhalten</param>
        /// <param name="userid">des am HELIUM V Servers angemeldeten Benutzers</param>
        [Get("/api/v11/staff/list")]
        Task<List<StaffEntry>> GetStaff2Async([Query] int? limit, [Query] int? startIndex, [Query] string userid);

        /// <summary>
        /// Der Mitarbeiter hat synchronisiert.
        ///
        /// Synch (/api/v11/staff/synch)
        /// </summary>
        /// <param name="content"></param>
        [Post("/api/v11/staff/synch")]
        [Header("Content-Type", "application/json")]
        Task<Response<object>> SynchAsync([Body] SynchEntry content);

        /// <summary>
        /// Einen Mitarbeiter liefern.
        ///
        /// FindStaff (/api/v11/staff/{userid})
        /// </summary>
        /// <param name="userid">des am HELIUM V Servers angemeldeten Benutzers</param>
        /// <param name="forStaffCnr">ist die optionale Personalnummer des Mitarbeiters, der abgerufen werden soll.</param>
        /// <param name="forStaffId">ist die optionale PersonalId des Mitarbeiters, der abgerufen werden soll.</param>
        /// <param name="forStaffIdCard">ist die optionale Ausweisnummer des Mitarbeiters, der abgerufen werden soll.Sind zumindest zwei der drei Parameter forStaffId, forStaffCnr und forStaffIdCard angegeben,werden die Parameter nach dieser Reihenfolge priorisiert verwendet. Sind alle drei nicht angegeben,wird der Mitarbeiter geliefert, der dem angemeldeten Benutzer zugewiesen ist.</param>
        [Get("/api/v11/staff/{userid}")]
        Task<StaffEntry> FindStaffAsync([Path] string userid, [Query] string forStaffCnr, [Query] int? forStaffId, [Query] string forStaffIdCard);
    }
}