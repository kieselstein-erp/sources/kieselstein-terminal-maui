using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.Messaging;
using KieselsteinErp.Terminal.Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services;
using System.Collections.ObjectModel;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Popups;

public partial class DeviceDeliveryIdentityAmountEntryPopup : Popup
{
  private string _itemCnr = null;
  private readonly IMessageService _messageService;
  private ObservableCollection<DeviceAssemblyEntry> _deviceAssemblies = new ObservableCollection<DeviceAssemblyEntry>();

  public string Title { get; set; }

  public DeviceDeliveryIdentityAmountEntryPopup(IMessageService messageService, string itemCnr, string serialNumber, List<DeviceAssemblyEntry> assemblies)
  {
    InitializeComponent();
    _messageService = messageService;
    _itemCnr = itemCnr;
    Title = itemCnr;
    BindingContext = this;

    serialNumberEntry.Text = serialNumber;
    if (assemblies != null)
      foreach (DeviceAssemblyEntry assembly in assemblies)
        _deviceAssemblies.Add(assembly);

    if (assemblies.Count > 3)
    {
      double maxheight = Microsoft.Maui.Devices.DeviceDisplay.MainDisplayInfo.Height;
      double maxwidth = Microsoft.Maui.Devices.DeviceDisplay.MainDisplayInfo.Width;
      this.Size = new Size(Math.Min(maxwidth, 800), Math.Min(maxheight, 500 + 90 * (assemblies.Count - 3)));
    }

    assemblyList.ItemsSource = _deviceAssemblies;
    okButton.IsEnabled = assemblies.Count == 0;

    if (!WeakReferenceMessenger.Default.IsRegistered<ItemMessage>(this))
      WeakReferenceMessenger.Default.Register<ItemMessage>(this, HandleItemMessage);
    if (!WeakReferenceMessenger.Default.IsRegistered<UnidentifiedBarcodeMessage>(this))
      WeakReferenceMessenger.Default.Register<UnidentifiedBarcodeMessage>(this, HandleUnidentifiedBarcodeMessage);
  }

  private void HandleUnidentifiedBarcodeMessage(object recipient, UnidentifiedBarcodeMessage message)
  {
    MainThread.BeginInvokeOnMainThread(async () =>
    {
      await DoUnidentifiedBarcodeMessage(recipient, message);
    });
  }

  private async Task DoUnidentifiedBarcodeMessage(object recipient, UnidentifiedBarcodeMessage message)
  {
    if (!string.IsNullOrEmpty(serialNumberEntry.Text))
    {
      bool ret = await Application.Current.MainPage.DisplayAlert(Title, HandleUnidentifiedBarcodeMessage_SerialnumberAllreadySetReplaceWithNewOne, Yes, No);
      if (!ret)
        return;
    }
      serialNumberEntry.Text = message.Value;
      CheckOkEnable();
  }

  private void HandleItemMessage(object recipient, ItemMessage message)
  {
    MainThread.BeginInvokeOnMainThread(async () =>
    {
      await DoItemMessage(recipient, message);
    });
  }

  private async Task DoItemMessage(object recipient, ItemMessage message)
  {
    if (string.IsNullOrEmpty(message.Value.BatchNr))
    {
      _messageService.Warn(HandleItemMessage_BarcodeContainsNoSerialNumber);
      return;
    }

    if (Title == message.Value.ItemCnr)
    {
      // is device, set serialnumber
      HandleUnidentifiedBarcodeMessage(recipient, new UnidentifiedBarcodeMessage(message.Value.BatchNr));
      return;
    }

    // check if device contains assembly with itemcnr
    DeviceAssemblyEntry assemblyEntry = _deviceAssemblies.FirstOrDefault((o) => o.ItemCnr == message.Value.ItemCnr && o.Serialnumbers.Contains(message.Value.BatchNr));
    if (assemblyEntry == null)
      _messageService.Error(string.Format(HandleItemMessage_Component0IsNotPresentInTheDevice, message.Value.ItemCnr));
    else
    {
      assemblyList.ScrollTo(assemblyEntry, ScrollToPosition.MakeVisible, true);
      if (!assemblyEntry.Serialnumbers.Contains(message.Value.BatchNr))
      {
        _messageService.Error(string.Format(HandleItemMessage_Serialnumber0NotAvailableForItem1, message.Value.BatchNr, message.Value.ItemCnr));
        return;
      }  
      if (assemblyEntry.Serialnumber != null && assemblyEntry.Serialnumber != message.Value.BatchNr)
      {
        bool ret = await Application.Current.MainPage.DisplayAlert(string.Format(HandleItemMessage_Item0, message.Value.ItemCnr), HandleItemMessage_TheSerialNumberForTheAssemblyIsDifferentReplaceWithNewOnes, Yes, No);
        if (!ret)
          return;
      }
      MainThread.BeginInvokeOnMainThread(() =>
      {
        assemblyEntry.Serialnumber = message.Value.BatchNr;
        CheckOkEnable();
      });
    }
  }

  private void CheckOkEnable()
  {
    bool enable = true;
    if (string.IsNullOrEmpty(serialNumberEntry.Text))
      enable = false;
    else
      foreach (var assemblyEntry in _deviceAssemblies)
      {
        if (string.IsNullOrEmpty(assemblyEntry.Serialnumber))
        {
          enable = false;
          break;
        }
      }
    okButton.IsEnabled = enable;
  }

  private void OnRemoveButtonClicked(object sender, EventArgs e)
  {
    if (sender is Button button && button.CommandParameter is DeviceAssemblyEntry assemblyEntry)
    {
      assemblyEntry.Serialnumber = null;
      CheckOkEnable();
    }
  }

  void OkButtonClicked(object sender, EventArgs e)
  {
    WeakReferenceMessenger.Default.UnregisterAll(this);
    List<ItemIdentityAmountEntry> list = new List<ItemIdentityAmountEntry>();
    foreach (var assemblyEntry in _deviceAssemblies)
      list.Add(new ItemIdentityAmountEntry { itemId = assemblyEntry.Item.id, amount = 1, identity = assemblyEntry.Serialnumber });
    Close(new PopupDeviceDeliveryResult(false, serialNumberEntry.Text, list, printLabelSwitch.IsToggled));
  }

  void CancelButtonClicked(object sender, EventArgs e)
  {
    WeakReferenceMessenger.Default.UnregisterAll(this);
    Close();
  }

  private void IdentityEntry_Completed(object sender, EventArgs e)
  {
    CheckOkEnable();
  }

  private void Picker_SelectedIndexChanged(object sender, EventArgs e)
  {
    CheckOkEnable();
  }
}