using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models.ScanLabels;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Util;
using System.Collections.ObjectModel;
using System.Globalization;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Popups;

public partial class IdentityAmountEntryPopup : Popup
{
  //private static Type me = typeof(IdentityAmountEntryPopup);

  private string _itemCnr = null;
  private readonly IMessageService _messageService;
  private readonly bool _useReelId;
  private readonly bool _hasBatchnumber;

  private readonly ObservableCollection<IdentityAmountEntry> _identityAmountEntries = [];
  public ObservableCollection<IdentityAmountEntry> IdentityAmountEntries { get { return _identityAmountEntries; } }

  private readonly ObservableCollection<IdentityAmountEntry> _identityToChoose = [];
  public ObservableCollection<IdentityAmountEntry> IdentityToChoose { get { return _identityToChoose; } }

  public string IdentityText { get; set; }
  public IdentityAmountEntry SelectedIdentityItem { get; set; } 
  public string Title { get; set; }

  public IdentityAmountEntryPopup(IMessageService messageService, bool useReelId, bool isChargenr, string itemCnr, string batchNr, List<IdentityAmountEntry> oldIdentityAmountEntires, List<IdentityAmountEntry> availableIdentities = null)
  {
    InitializeComponent();
    _messageService = messageService;
    _itemCnr = itemCnr;
    _hasBatchnumber = isChargenr;
    _useReelId = useReelId;
    Title = itemCnr;

    AmountEntry.IsVisible = isChargenr;
    IdentityEntry.Completed += OnAddButtonClicked;
    AmountEntry.CompletedCommand = new Command(AddIdentity);
    IdentityText = batchNr;

    if (oldIdentityAmountEntires != null)
      foreach (IdentityAmountEntry identityAmountEntry in oldIdentityAmountEntires)
        IdentityAmountEntries.Add(identityAmountEntry);

    if (!string.IsNullOrWhiteSpace(batchNr))
    {
      IdentityEntry.IsEnabled = false;
      if (!isChargenr)
        AddIdentity();
    }

    if (availableIdentities != null)
    {
      foreach (var identity in availableIdentities)
      { 
        IdentityToChoose.Add(identity);
      }
      if (IdentityToChoose.Count > 0)
      {
        IdentityEntry.IsVisible = false;
        IdentityPicker.IsVisible = true;
        IdentityPicker.ItemDisplayBinding = new Binding("identity");
        IdentityPicker.ItemsSource = IdentityToChoose;
      }
    }

    BindingContext = this;
    if (!WeakReferenceMessenger.Default.IsRegistered<ItemMessage>(this))
      WeakReferenceMessenger.Default.Register<ItemMessage>(this, HandleItemMessage);
    if (!WeakReferenceMessenger.Default.IsRegistered<UnidentifiedBarcodeMessage>(this))
      WeakReferenceMessenger.Default.Register<UnidentifiedBarcodeMessage>(this, HandleUnidentifiedBarcodeMessage);
    if (!WeakReferenceMessenger.Default.IsRegistered<ItemVDAMessage>(this)) 
      WeakReferenceMessenger.Default.Register<ItemVDAMessage>(this, HandleItemVDAMessage);

    
  }

  private void HandleUnidentifiedBarcodeMessage(object recipient, UnidentifiedBarcodeMessage message)
  {
    if (_itemCnr == message.Value) return;  // skip if is same as item number

    HandleItemMessage(recipient, new ItemMessage(_itemCnr, message.Value));
  }

  private void HandleItemVDAMessage(object recipient, ItemVDAMessage message)
  {
    VdaItem item = message.Value.ScanLabel.ToVdaItem();
    HandleItemMessage(recipient, new ItemMessage(item.Cnr, (_useReelId && _hasBatchnumber) ? item.BatchNumberReelId() : item.BatchNumber1, item.Amount));
  }

  private void HandleItemMessage(object recipient, ItemMessage message)
  {
    if (message.Value.IsUnidentified) return;

    if (message.Value.ItemCnr.Equals(_itemCnr))
    {
      MainThread.BeginInvokeOnMainThread(() =>
      {
        IdentityEntry.IsEnabled = false;
        IdentityEntry.Text = message.Value.BatchNr;
        if (!AmountEntry.IsVisible)
          AddIdentity();
      });
    }
    else
    {
      _messageService.Warn(string.Format("Item {0} does not match", message.Value.ItemCnr));
    }
  }

  private void AddIdentity()
  {
    string identity = null;
    decimal amount = 0;
    IdentityEntry.IsEnabled = true;
    if (IdentityPicker.IsVisible && SelectedIdentityItem != null)
    {
      identity = SelectedIdentityItem.identity;
      if (AmountEntry.IsVisible && !string.IsNullOrWhiteSpace(AmountEntry.Text))
      {         
        // with batch number
        amount = (decimal)DecimalHelper.ConvertDecimal(AmountEntry.Text);
        if (SelectedIdentityItem.amount <= amount)
        {
          amount = SelectedIdentityItem.amount;
          IdentityToChoose.Remove(SelectedIdentityItem);
        }
        else
        {
          SelectedIdentityItem.amount -= amount;
        }
      }
      else if (!AmountEntry.IsVisible)
      {
        // with serial number
        amount = SelectedIdentityItem.amount;
        IdentityToChoose.Remove(SelectedIdentityItem);
      }
    }
    else
    {
      identity = IdentityEntry.Text;
      // with batch number
      if (AmountEntry.IsVisible && !string.IsNullOrWhiteSpace(AmountEntry.Text))
      {
        amount = (decimal)DecimalHelper.ConvertDecimal(AmountEntry.Text);
      }
      else if (!AmountEntry.IsVisible)
      {
        // with serial number
        amount = 1;
      }
    }
    if (!string.IsNullOrWhiteSpace(identity) && amount != 0)
    {
      foreach (var identityEntry in IdentityAmountEntries)
        if (identityEntry.identity.Equals(identity))
        {
          if (AmountEntry.IsVisible)
          { //isBatchnr
            IdentityAmountEntry newEntry = new IdentityAmountEntry() { identity = identity, amount = amount + identityEntry.amount };
            IdentityAmountEntries.Remove(identityEntry);
            IdentityAmountEntries.Add(newEntry);
            IdentityEntry.Text = string.Empty;
          }
          else
            _messageService.Error(string.Format(SerialnrAlreadyTaken, identity));
          return;
        }

      IdentityAmountEntries.Add(new IdentityAmountEntry() { identity = identity, amount = amount });
      IdentityEntry.Text = string.Empty;
      AmountEntry.Text = string.Empty;
    }
  }
  private void OnAddButtonClicked(object sender, EventArgs e)
  {
    AddIdentity();
  }

  private void OnRemoveButtonClicked(object sender, EventArgs e)
  {
    if (sender is Button button && button.CommandParameter is IdentityAmountEntry identityAmount)
    {
      if (AmountEntry.IsVisible)
      {
        // with batch number
        IdentityAmountEntry old = IdentityToChoose.FirstOrDefault(x => x.identity == identityAmount.identity);
        if (old != null)
          old.amount += identityAmount.amount;
        else
          IdentityToChoose.Add(identityAmount);
      }
      else
      {
        // with serial number
        IdentityToChoose.Add(identityAmount);
      }

      IdentityAmountEntries.Remove(identityAmount);
    }
  }

  void OkButtonClicked(object sender, EventArgs e)
  {
    WeakReferenceMessenger.Default.UnregisterAll(this);
    if (!string.IsNullOrWhiteSpace(IdentityEntry.Text) && decimal.TryParse(AmountEntry.Text, out decimal o))
      AddIdentity();
    Close(IdentityAmountEntries.ToList());
  }

  void CancelButtonClicked(object sender, EventArgs e)
  {
    WeakReferenceMessenger.Default.UnregisterAll(this);
    Close(null);
  }

  private void IdentityPicker_SelectedIndexChanged(object sender, EventArgs e)
  {
    if (SelectedIdentityItem == null)
      AmountEntry.MaxValue = 1;
    else
      AmountEntry.MaxValue = SelectedIdentityItem.amount;
  }
}