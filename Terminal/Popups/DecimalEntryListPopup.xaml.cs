using CommunityToolkit.Maui.Core.Platform;
using CommunityToolkit.Maui.Views;
using KieselsteinErp.Terminal.Controls;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services;
using System.Collections.ObjectModel;
using System.Diagnostics;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Popups
{
  public partial class DecimalEntryListPopup : Popup
  {
    private readonly IMessageService _messageService;
    ObservableCollection<NumericEntryInfo> numericEntries = new();
    public ObservableCollection<NumericEntryInfo> NumericEntries { get { return numericEntries; } }

    private string _infoMessage;
    public string InfoMessage { get { return _infoMessage; } }

    private bool _isInfoVisible = false;
    public bool IsInfoVisible { get { return _isInfoVisible; } }

    public NumericEntryInfo EntryInfo1 { get { return numericEntries[0]; } }
    public NumericEntryInfo EntryInfo2 {  get { return numericEntries[1]; } }

    //    public DecimalEntryListPopup(IMessageService messageService, NumericEntryInfoList numericEntryInfos, bool hasCheckbox, string checkboxLabel, string infoMessage)
    public DecimalEntryListPopup(IMessageService messageService, NumericEntryInfoList numericEntryInfos, string infoMessage)
    {
      InitializeComponent();
      _infoMessage = infoMessage;
      _isInfoVisible = !string.IsNullOrEmpty(_infoMessage);
      _messageService = messageService;
      foreach (NumericEntryInfo info in numericEntryInfos.Infos)
        numericEntries.Add(info);

      //TODO: actual implementation in restapi prints to server printer not to workstation defined printer
      //if (Checkbox.IsVisible = hasCheckbox)
      //{
      //  CheckboxLabel.Text = checkboxLabel;
      //  this.Checkbox.IsToggled = _isInfoVisible;
      //}
      //CheckboxGrid.ColumnDefinitions[0].Width = new GridLength(numericEntryInfos.Infos[0].RelativeLabelWidth, GridUnitType.Star);
      //CheckboxGrid.ColumnDefinitions[1].Width = new GridLength(1 - numericEntryInfos.Infos[0].RelativeLabelWidth, GridUnitType.Star);
      this.Opened += DecimalEntryListPopup_Opened;
      BindingContext = this;
    }

    private async void DecimalEntryListPopup_Opened(object sender, CommunityToolkit.Maui.Core.PopupOpenedEventArgs e)
    {
      if (entry1.Text == "0")
        entry1.Text = string.Empty;
      if (entry2.Text == "0")
        entry2.Text = string.Empty;
      //entry1.Focus();
      if (!KeyboardExtensions.IsSoftKeyboardShowing(entry1))
        await KeyboardExtensions.ShowKeyboardAsync(entry1, default);
    }

    void OkButtonClicked(object sender, EventArgs e)
    {
      if (AreEntriesValid())
      {
        List<decimal> entryTexts = new();
        foreach (var info in NumericEntries)
        {
          entryTexts.Add(info.Amount);
        }
        //Close(new PopupListResult(false, entryTexts, Checkbox.IsVisible ? Checkbox.IsToggled : false));
        Close(new PopupListResult(false, entryTexts, false));
      }
      else
      {
        _messageService.Error(Error_No_Valid_Input);
      }
    }
    void CloseButtonClicked(object sender, EventArgs e) => Close(new PopupListResult(true, null, false));

    private bool AreEntriesValid()
    {
      foreach (var numericEntry in NumericEntries)
      {
        if (numericEntry.Amount < numericEntry.MinValue || numericEntry.Amount > numericEntry.MaxValue)
        {
          return false;
        }
      }
      return true;
    }

    private void NumericEntry_Completed(object sender, EventArgs e)
    {
      var entry = sender as NumericEntry;
      var index = (entry.Parent as VerticalStackLayout).IndexOf(entry);
      if (index == 0)
        entry2.Focus();
      else
        OkButtonClicked(sender, e);
    }
  }
}
