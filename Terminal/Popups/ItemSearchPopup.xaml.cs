using CommunityToolkit.Maui.Core.Platform;
using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.Input;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services.Item;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace KieselsteinErp.Terminal.Popups;

public partial class ItemSearchPopup : Popup, INotifyPropertyChanged
{

  private readonly IItemV1Service _itemV1Service;

  private string _itemCnr = null;
  public string ItemCnr { get => _itemCnr; set => _itemCnr = value; }

  private ItemV1Entry _selectedItem = null;
  public ItemV1Entry SelectedItem { get => _selectedItem; set => _selectedItem = value; }

  ObservableCollection<ItemV1Entry> _searchResults = new ObservableCollection<ItemV1Entry>();
  public ObservableCollection<ItemV1Entry> SearchResults { get { return _searchResults; } }

  private bool _isLoadingCircleVisible = false;
  public bool IsLoadingCircleVisible
  {
    get => _isLoadingCircleVisible;
    set { _isLoadingCircleVisible = value; OnPropertyChanged(nameof(IsLoadingCircleVisible)); }
  }

  public ItemSearchPopup(IItemV1Service itemV1Service)
  {
    InitializeComponent();
    _itemV1Service = itemV1Service;
    BindingContext = this;
    this.PropertyChanged += OnPropertyChanged;
  }

  private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
  {
    if (e.PropertyName == nameof(ItemCnr))
      if (string.IsNullOrEmpty(_itemCnr))
        SearchResults.Clear();

  }

  [RelayCommand]
  private async Task SearchAsync(string text)
  {
    //ClearAll();
    IsLoadingCircleVisible = true;
    try
    {
      ItemV1EntryList itemV1EntryList = await _itemV1Service.GetItemsWithCnrOrTextOrDeliverCnr(text + "%");
      if (itemV1EntryList != null)
      {
        SearchResults.Clear();
        foreach (ItemV1Entry item in itemV1EntryList.entries)
          SearchResults.Add(item);
      }
      if (searchBar.IsSoftKeyboardShowing())
        await searchBar.HideKeyboardAsync();
    }
    finally 
    { IsLoadingCircleVisible = false; }
  }

  [RelayCommand]
  private void ListTapped()
  {
    Close(new PopupResult(false, SelectedItem.cnr));
  }

  [RelayCommand]
  private void Cancel()
  {
    Close(new PopupResult(true, null));
  }

}