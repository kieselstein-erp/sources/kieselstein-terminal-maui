using CommunityToolkit.Maui.Core.Platform;
using CommunityToolkit.Maui.Views;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services;

namespace KieselsteinErp.Terminal.Popups;

public partial class PasswordEntryPopup : Popup
{
  private readonly IMessageService _messageService;

  public PasswordEntryPopup()
  {
    _messageService = Application.Current.Handler.MauiContext.Services.GetService<IMessageService>(); ;
    InitializeComponent();
    this.Opened += PasswordEntryPopup_Opened;
  }

  private async void PasswordEntryPopup_Opened(object sender, CommunityToolkit.Maui.Core.PopupOpenedEventArgs e)
  {
    //this.PasswordEntry.Focus();

    if (!KeyboardExtensions.IsSoftKeyboardShowing(this.PasswordEntry))
      await KeyboardExtensions.ShowKeyboardAsync(this.PasswordEntry, default);
  }

  private void OkButtonClicked(object sender, EventArgs e) => Close(new PopupResult(false, PasswordEntry.Text));
  private void CloseButtonClicked(object sender, EventArgs e) => Close(new PopupResult(true, PasswordEntry.Text));
  private void PasswordEntry_Completed(object sender, EventArgs e) => OkButtonClicked(sender, e);
}