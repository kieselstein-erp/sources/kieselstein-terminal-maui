using CommunityToolkit.Maui.Views;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services.Settings;
using KieselsteinErp.Terminal.Services.Worktime;
using KieselsteinErp.Terminal.Util;
using System.ComponentModel;

namespace KieselsteinErp.Terminal.Popups;

public partial class RequestsPopup : Popup, INotifyPropertyChanged
{
  private bool _vacation = false;
  public bool Vacation
  {
    get => _vacation;
    set { _vacation = value; OnPropertyChanged(nameof(Vacation)); }
  }

  private bool _timeCompensation = false;
  public bool TimeCompensation
  {
    get => _timeCompensation;
    set { _timeCompensation = value; OnPropertyChanged(nameof(TimeCompensation));
}
  }
  private bool _illness = false;
  public bool Illness
  {
    get => _illness;
    set { _illness = value; OnPropertyChanged(nameof(Illness)); }
  }

  private bool _fullDay = true;
  public bool FullDay
  {
    get => _fullDay;
    set { _fullDay = value; OnPropertyChanged(nameof(FullDay)); }
  }

  private bool _halfDay = false;
  public bool HalfDay 
  {
    get => _halfDay;
    set { _halfDay = value; OnPropertyChanged(nameof(HalfDay)); }
  }

  private DateTime _dateFrom = DateTime.Today.AddDays(1);
  public DateTime DateFrom 
  {
    get => _dateFrom;
    set { _dateFrom = value; OnPropertyChanged(nameof(DateFrom));} 
  }

  private DateTime _dateTo = DateTime.Today.AddDays(1);
  public DateTime DateTo
  {
    get => _dateTo;
    set { _dateTo = value; OnPropertyChanged(nameof(DateTo)); }
  }

  private DateTime _minimumDate = DateTime.Today.AddDays(1);
  public DateTime MinimumDate
  {
    get => _minimumDate;
    set { _minimumDate = value; OnPropertyChanged(nameof(MinimumDate)); }
  }

  private bool _isValid = false;
  public bool IsValid
  {
    get => _isValid;
    set { _isValid = value; OnPropertyChanged(nameof(IsValid)); }
  }

  private bool _allowSickRequest = false;
  public bool AllowSickRequest
  {
    get => _allowSickRequest;
    set { _allowSickRequest = value; OnPropertyChanged(nameof(AllowSickRequest)); }
  }

  public RequestsPopup(ISettingsService settingsService)
  {
    InitializeComponent();
    AllowSickRequest = settingsService.AllowSickRequest;
    BindingContext = this;
    this.PropertyChanged += OnChanged;
  }

  private void OnChanged(object sender, PropertyChangedEventArgs e)
  {
    if (e.PropertyName == nameof(DateFrom))
    {
      if (DateTo < DateFrom)
      {
        DateTo = DateFrom;
      }
    }
    if (e.PropertyName == nameof(DateTo))
    {
      if (DateFrom > DateTo)
      {
        DateFrom = DateTo;
      }
    }
    if (e.PropertyName != nameof(IsValid))
      CheckValid();
  }

  private void CheckValid()
  {
    bool valid = true;
    if (DateTo < DateFrom) valid = false;
    if (DateFrom > DateTo) valid = false;
    if (!Vacation && !TimeCompensation && !Illness) valid = false;
    IsValid = valid;
  }

  private void okButton_Clicked(object sender, EventArgs e)
  {
    SpecialTimesEntry entry = new SpecialTimesEntry();
    SpecialTimeType type = SpecialTimeType.Holiday;

    if (Vacation)
    {
      entry.timeType = SpecialTimesEnumConstants.Holiday;
      type = SpecialTimeType.Holiday;
    }
    if (TimeCompensation)
    {
      entry.timeType = SpecialTimesEnumConstants.TimeCompensation;
      type = SpecialTimeType.TimeCompensation;
    }
    if (Illness)
    {
      entry.timeType = SpecialTimesEnumConstants.Illness;
      type = SpecialTimeType.Illness;
    }

    entry.halfDay = HalfDay;
    entry.fromDateMs = DateTimeHelper.ToMillisecondsSinceUnixEpoch(DateFrom);
    entry.toDateMs = DateTimeHelper.ToMillisecondsSinceUnixEpoch(DateTo);

    this.PropertyChanged -= OnChanged;

    Close(new PopupRequestsResult(false, type, entry));
  }

  private void cancelButton_Clicked(object sender, EventArgs e)
  {
    this.PropertyChanged -= OnChanged;

    Close(new PopupRequestsResult(true));
  }

}