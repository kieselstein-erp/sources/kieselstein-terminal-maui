using Camera.MAUI.ZXingHelper;
using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.Messaging;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Audio;
using Microsoft.Extensions.Logging;
using ZXing;

namespace KieselsteinErp.Terminal.Popups;

public partial class BarcodeCameraPopup : Popup
{
  private string _barcode;
  private BarcodeFormat _barcodeFormat;
  private readonly IAudioService _audioService;
  private readonly IMessageService _messageService;
  private readonly ILogger _logger;

  public BarcodeCameraPopup()
  {
    _audioService = Application.Current.MainPage.Handler.MauiContext.Services.GetService<IAudioService>();
    _messageService = Application.Current.MainPage.Handler.MauiContext.Services.GetService<IMessageService>();
    ILoggerFactory factory = Application.Current.MainPage.Handler.MauiContext.Services.GetService<ILoggerFactory>();
    _logger = factory?.CreateLogger<BarcodeCameraPopup>();

    InitializeComponent();
    this.BindingContext = this;
  }

  //private void OkButtonClicked(object sender, EventArgs e)
  //{
  //  WeakReferenceMessenger.Default.Send(new CameraBarcodeMessage(_barcode, _barcodeFormat));
  //  Close(new PopupResult(false, _barcode));
  //}

  private void CloseButtonClicked(object sender, EventArgs e) => Close(new PopupResult(true, string.Empty));

  private void cameraView_BarcodeDetected(object sender, BarcodeEventArgs args)
  {
    if (cameraView != null)
    {
      cameraView.BarCodeDetectionEnabled = false;
      _barcode = args.Result[0].Text;
      _barcodeFormat = args.Result[0].BarcodeFormat;
      _audioService?.Play(Messages.MessageType.Info);
      //MainThread.InvokeOnMainThreadAsync(() => OkButton.IsEnabled = true);

      WeakReferenceMessenger.Default.Send(new CameraBarcodeMessage(_barcode, _barcodeFormat));
    }
    Close(new PopupResult(false, _barcode));
  }

  private void cameraView_CamerasLoaded(object sender, EventArgs e)
  {
    if (cameraView.Cameras.Count > 0)
    {
      cameraView.Camera = cameraView.Cameras.First();

      cameraView.BarCodeOptions = new BarcodeDecodeOptions()
      {
        AutoRotate = true,
        ReadMultipleCodes = false,
        TryHarder = true,
        TryInverted = true,
        PossibleFormats = { BarcodeFormat.QR_CODE, BarcodeFormat.CODE_39, BarcodeFormat.CODE_128,  BarcodeFormat.EAN_13, BarcodeFormat.DATA_MATRIX }
      };
      cameraView.BarCodeDetectionFrameRate = 10;
      cameraView.BarCodeDetectionMaxThreads = 5;
      cameraView.ControlBarcodeResultDuplicate = true;

      cameraView.BarCodeDetectionEnabled = true;
      MainThread.BeginInvokeOnMainThread(async () =>
      {
        try
        {
          //Task.Delay(500).Wait();
          await cameraView.StopCameraAsync();
          await cameraView.StartCameraAsync();
        }
        catch (Exception ex)
        {
          _logger?.LogError(ex.Message);
          _logger?.LogTrace(ex.StackTrace);
        }
      });
    }
    else
    {
      _messageService?.Warn("No camera for barcode reading available");
    }
  }
}