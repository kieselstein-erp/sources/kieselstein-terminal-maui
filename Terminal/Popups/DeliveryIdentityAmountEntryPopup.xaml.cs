using CommunityToolkit.Maui.Views;
using CommunityToolkit.Mvvm.Messaging;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Util;
using System.Collections.ObjectModel;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Popups;

public partial class DeliveryIdentityAmountEntryPopup : Popup
{
  private string _itemCnr = null;
  private bool _isActive = false;
  private readonly IMessageService _messageService;
  private ObservableCollection<IdentityAmountEntry> identityAmountEntries = new ObservableCollection<IdentityAmountEntry>();
  
  public string Title { get; set; }

  public DeliveryIdentityAmountEntryPopup(IMessageService messageService, bool isChargenr, string itemCnr, string batchNr, List<IdentityAmountEntry> oldIdentityAmountEntires)
  {
    InitializeComponent();
    _messageService = messageService;
    _itemCnr = itemCnr;
    Title = itemCnr;
    BindingContext = this;

    AmountEntry.IsVisible = isChargenr;
    IdentityEntry.Completed += OnAddButtonClicked;
    AmountEntry.CompletedCommand = new Command(AddIdentity);
    IdentityEntry.Text = batchNr;
    if (oldIdentityAmountEntires != null)
      foreach (IdentityAmountEntry identityAmountEntry in oldIdentityAmountEntires)
        identityAmountEntries.Add(identityAmountEntry);
    if (!string.IsNullOrWhiteSpace(batchNr))
    {
      IdentityEntry.IsEnabled = false;
      if (!isChargenr)
        AddIdentity();
    }
    SerialnrList.ItemsSource = identityAmountEntries;
    WeakReferenceMessenger.Default.Register<ItemMessage>(this, HandleItemMessage);
    WeakReferenceMessenger.Default.Register<UnidentifiedBarcodeMessage>(this, HandleUnidentifiedBarcodeMessage);
    _isActive = true;
  }

  private void HandleUnidentifiedBarcodeMessage(object recipient, UnidentifiedBarcodeMessage message)
  {
    HandleItemMessage(recipient, new ItemMessage(_itemCnr, message.Value));
  }

  private void HandleItemMessage(object recipient, ItemMessage message)
  {
    if (!_isActive)
      return;
    if (message.Value.ItemCnr.Equals(_itemCnr))
    {
      MainThread.BeginInvokeOnMainThread(() =>
      {
        IdentityEntry.IsEnabled = false;
        IdentityEntry.Text = message.Value.BatchNr;
        if (!AmountEntry.IsVisible)
          AddIdentity();
      });
    }
  }

  private void AddIdentity()
  {
    IdentityEntry.IsEnabled = true;
    string identity = IdentityEntry.Text;
    decimal amount = (decimal)(!string.IsNullOrWhiteSpace(AmountEntry.Text) ? DecimalHelper.ConvertDecimal(AmountEntry.Text) : 1);
    if (!string.IsNullOrWhiteSpace(identity))
    {
      foreach (var identityEntry in identityAmountEntries)
        if (identityEntry.identity.Equals(identity))
        {
          if (AmountEntry.IsVisible)
          { //isBatchnr
            IdentityAmountEntry newEntry = new IdentityAmountEntry() { identity = identity, amount = amount + identityEntry.amount };
            identityAmountEntries.Remove(identityEntry);
            identityAmountEntries.Add(newEntry);
            IdentityEntry.Text = string.Empty;
          }
          else
            _messageService.Error(string.Format(SerialnrAlreadyTaken, identity));
          return;
        }

      identityAmountEntries.Add(new IdentityAmountEntry() { identity = identity, amount = amount });
      IdentityEntry.Text = string.Empty;
    }
  }
  private void OnAddButtonClicked(object sender, EventArgs e)
  {
    AddIdentity();
  }

  private void OnRemoveButtonClicked(object sender, EventArgs e)
  {
    if (sender is Button button && button.CommandParameter is IdentityAmountEntry identityAmount)
    {
      identityAmountEntries.Remove(identityAmount);
    }
  }

  void OkButtonClicked(object sender, EventArgs e)
  {
    if (!string.IsNullOrWhiteSpace(IdentityEntry.Text) && decimal.TryParse(AmountEntry.Text, out decimal o))
      AddIdentity();
    _isActive = false;
    Close(new PopupDeliveryResult(false, identityAmountEntries.ToList(), 0, Checkbox.IsToggled));
  }

  void CancelButtonClicked(object sender, EventArgs e)
  {
    _isActive = false;
    Close(null);
  }
}