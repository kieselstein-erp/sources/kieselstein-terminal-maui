using CommunityToolkit.Maui.Core.Platform;
using CommunityToolkit.Maui.Views;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Services;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Popups;

public partial class DecimalEntryPopup : Popup
{
  private readonly IMessageService _messageService;

  private string _infoMessage;
  public string InfoMessage { get { return _infoMessage; } }

  private bool _isInfoVisible = false;
  public bool IsInfoVisible { get { return _isInfoVisible; } }

  public DecimalEntryPopup(IMessageService messageService, int decimalPlaces, string infoMessage)
  {
    InitializeComponent();
    this.Opened += DecimalEntryPopup_Opened;
    _messageService = messageService;
    _infoMessage = infoMessage;
    _isInfoVisible = !string.IsNullOrEmpty(_infoMessage);
    entry.DecimalPlaces = decimalPlaces;
    entry.Text = "";
    BindingContext = this;
  }

  private async void DecimalEntryPopup_Opened(object sender, CommunityToolkit.Maui.Core.PopupOpenedEventArgs e)
  {
    //this.entry.Focus();
    if (!KeyboardExtensions.IsSoftKeyboardShowing(entry))
      await KeyboardExtensions.ShowKeyboardAsync(entry, default);
  }

  public DecimalEntryPopup(IMessageService messageService, int decimalPlaces, decimal amount, string infoMessage) : this(messageService, decimalPlaces, infoMessage)
  {
    if (amount != 0)
      if ((int)amount == amount)
        entry.Text = ((int)amount).ToString();
      else
        entry.Text = amount.ToString();

    entry.Focus();
  }

  public DecimalEntryPopup(IMessageService messageService, int decimalPlaces, decimal minValue, decimal maxValue, string infoMessage) : this(messageService, decimalPlaces, infoMessage)
  {
    entry.MinValue = minValue;
    entry.MaxValue = maxValue;
    entry.Text = "";
    entry.Focus();
  }

  public DecimalEntryPopup(IMessageService messageService, int decimalPlaces, decimal amount, decimal minValue, decimal maxValue, string infoMessage) : this(messageService, decimalPlaces, minValue, maxValue, infoMessage)
  {
    if (amount != 0)
      entry.Text = amount.ToString();
    entry.Focus();
  }
  public DecimalEntryPopup(IMessageService messageService, int decimalPlaces, decimal amount, decimal minValue, decimal maxValue, string unit, string infoMessage) : this(messageService, decimalPlaces, amount, minValue, maxValue, infoMessage)
  {
    entry.Unit = unit;
    entry.Focus();
  }

  void OkButtonClicked(object sender, EventArgs e)
  {
    if (IsValid())
      Close(new PopupResult(false, entry.Text));
    else
      _messageService.Error(Error_No_Valid_Input);
  }

  void CloseButtonClicked(object sender, EventArgs e) => Close(new PopupResult(true, entry.Text));

  private bool IsValid()
  {
    return decimal.TryParse(entry.Text, out decimal result);
  }

  private void NumericEntryCompleted(object sender, EventArgs e)
  {
    OkButtonClicked(sender, e);
  }

}