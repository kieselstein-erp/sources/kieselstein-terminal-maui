using CommunityToolkit.Maui.Views;
using KieselsteinErp.Terminal.Models;

namespace KieselsteinErp.Terminal.Popups;

public partial class MultipleButtonsPopup : Popup
{
  public MultipleButtonsPopup()
  {
    InitializeComponent();
  }

  public MultipleButtonsPopup(string title, string message, string[] buttons) : this() 
  {
    LabelTitle.Text = title;
    LabelMessage.Text = message;
    if (buttons.Length >= 1)  Button1.Text = buttons[0];
    if (buttons.Length >= 2)  Button2.Text = buttons[1];
    if (buttons.Length >= 3)  Button3.Text = buttons[2];
  }

  private void Button1_Clicked(object sender, EventArgs e)
  {
    Close(new PopupResult(false, "1"));
  }

  private void Button2_Clicked(object sender, EventArgs e)
  {
    Close(new PopupResult(false, "2"));
  }

  private void Button3_Clicked(object sender, EventArgs e)
  {
    Close(new PopupResult(false, "3"));
  }
}