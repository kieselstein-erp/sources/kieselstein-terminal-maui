using CommunityToolkit.Maui.Views;
using KieselsteinErp.Terminal.Models;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Popups;

public partial class ButtonsSelectionPopup : Popup
{
  public List<string> Locations { get; private set; }
  public string Title { get; private set; }
  public string CancelButtonText { get; private set; } 

  public ButtonsSelectionPopup(List<StorageLocation> storageLocations)
  {
    InitializeComponent();

    Locations = [];
    foreach (StorageLocation storageLocation in storageLocations)
      Locations.Add(storageLocation.Cnr);
    Title = SelectStorageLocation;
    CancelButtonText = NoStorageLocation;
    this.BindingContext = this;
  }

  private void LocationButton_Clicked(object sender, EventArgs e)
  {
    string res = (sender as Button).Text;
    Close(new PopupResult(false, res));
  }

  private void CancelButton_Clicked(object sender, EventArgs e)
  {
    Close(new PopupResult(true, ""));
  }
}