﻿using Com.Datalogic.Decode;
using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
  public class DatalogicDecoderMessage : ValueChangedMessage<string>
  {
    private readonly BarcodeID _barcodeID;
    public DatalogicDecoderMessage(string value, BarcodeID barcodeID) : base(value) 
    {
      _barcodeID = barcodeID;
    }

    public BarcodeID BarcodeID { get => _barcodeID; }
  }
}
