﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Messages
{
  public class LotMessage(string value, ProductionEntry productionEntry) : ValueChangedMessage<string>(value)
  {
    private readonly ProductionEntry _productionEntry = productionEntry;

    public ProductionEntry productionEntry { get => _productionEntry; }
  }
}
