﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
  public class UserLogonMessage : ValueChangedMessage<string>
  {
    public UserLogonMessage(string value) : base(value) { }

  }
}
