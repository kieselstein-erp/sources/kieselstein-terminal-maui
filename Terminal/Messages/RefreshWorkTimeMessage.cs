﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
  public class RefreshWorkTimeMessage : ValueChangedMessage<bool>
  {
    public RefreshWorkTimeMessage(bool value) : base(value) { }
  }
}
