﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
  class ScanButtonMessage : ValueChangedMessage<bool>
  {
    public ScanButtonMessage(bool value) : base(value) { }

  }
}
