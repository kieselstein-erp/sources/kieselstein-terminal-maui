﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using KieselsteinErp.Terminal.Models;

namespace KieselsteinErp.Terminal.Messages
{
  public class BookMultipleWorkstepsMessage : ValueChangedMessage<bool>
  {
    public BookMultipleWorkstepsMessage(bool isMultipleLotWorkstepBookingActive) : base(isMultipleLotWorkstepBookingActive) { }
    public BookMultipleWorkstepsMessage(bool isMultipleLotWorkstepBookingActive, List<PieceConfirmation> pieceConfirmations) : base(isMultipleLotWorkstepBookingActive)
    {
      PieceConfirmations = pieceConfirmations;
    }

    public List<PieceConfirmation> PieceConfirmations { get; private set; }
  }
}
