﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
  public class PdfMessage : ValueChangedMessage<string>
  {
    public PdfMessage(string value) : base(value) { }
  }
}
