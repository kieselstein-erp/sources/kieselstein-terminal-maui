﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using KieselsteinErp.Terminal.Models;

namespace KieselsteinErp.Terminal.Messages
{
  public class ItemMessage : ValueChangedMessage<ItemMessageModel>
  {
    public ItemMessage(string itemCnr, bool isUnidentified) : base(new ItemMessageModel(itemCnr, isUnidentified)) { }

    public ItemMessage(string itemCnr, string batchNr) : base(new ItemMessageModel(itemCnr, batchNr)) { }

    public ItemMessage(string itemCnr, string batchNr, decimal amount) : base(new ItemMessageModel(itemCnr, batchNr, amount)) { }

  }
}
