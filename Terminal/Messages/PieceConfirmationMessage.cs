﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Services.Worktime;

namespace KieselsteinErp.Terminal.Messages
{
  public class PieceConfirmationMessage : ValueChangedMessage<OpenWorkEntryList>
  {
    private readonly WorkTimeEventType _workTimeEventType;
    private readonly bool _noUnídentify;
    private readonly ProductionRecordingEntry _worktimeToBook;
    private readonly bool _isGhostShiftConfirmation = false;
    private readonly GhostShiftInfo _ghostShiftInfo;

    public PieceConfirmationMessage(OpenWorkEntryList value, WorkTimeEventType workTimeEventType, bool noUnidentify, ProductionRecordingEntry worktimeToBook) : base(value)
    {
      _workTimeEventType = workTimeEventType;
      _noUnídentify = noUnidentify;
      _worktimeToBook = worktimeToBook;
    }

    public PieceConfirmationMessage(OpenWorkEntryList value, WorkTimeEventType workTimeEventType, bool noUnidentify, ProductionRecordingEntry worktimeToBook, GhostShiftInfo ghostShiftInfo)
      : this(value, workTimeEventType, noUnidentify, worktimeToBook)
    {
      _isGhostShiftConfirmation = true;
      _ghostShiftInfo = ghostShiftInfo;
    }

    public WorkTimeEventType WorkTimeEventType { get { return _workTimeEventType; } }
    public bool NoUnidentify { get { return _noUnídentify; } }
    public ProductionRecordingEntry WorktimeToBook { get { return _worktimeToBook; } }
    public bool IsGhostShiftConfirmation { get { return _isGhostShiftConfirmation; } }
    public GhostShiftInfo GhostShiftInfo { get { return _ghostShiftInfo; } }
  }

  public class GhostShiftInfo
  {
    private readonly string _toastMessage;
    private readonly MachineEntry _machineEntry;
    private readonly long _jobTimeMs;

    public GhostShiftInfo(MachineEntry machineEntry, long jobTimeMs, string toastMessage)
    {
      _machineEntry = machineEntry;
      _jobTimeMs = jobTimeMs;
      _toastMessage = toastMessage;
    }

    public MachineEntry MachineEntry { get { return _machineEntry; } }
    public long JobTimeMs { get { return _jobTimeMs; } }
    public string ToastMessage { get { return _toastMessage; } }
  }
}
