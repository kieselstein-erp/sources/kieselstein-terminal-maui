﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
  public class PdfReportMessage : ValueChangedMessage<string>
  {

    public enum EType
    {
      ItemMasterSheet = 0,              // Item mastersheet (Artikelstammblatt)
      ProductionOrderFinalCosting = 1,  // Production order final costing (Losnachkalkulation)
      ProductionInformationSheet = 2    // Production information sheet (Produktionsinformationen)
    }

    private readonly string _parameter;
    public PdfReportMessage(string type, string parameter) : base(type)
    {
      _parameter = parameter;
    }

    public string Parameter { get => _parameter; }

    public EType Type { get => (EType) int.Parse(base.Value); }
  }
}
