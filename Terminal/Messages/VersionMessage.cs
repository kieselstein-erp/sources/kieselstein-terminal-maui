﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
  public class VersionMessage : ValueChangedMessage<string>
  {
    private Version _actualVersion;
    private Version _minVersion;

    public VersionMessage(Version actualVersion, Version minVersion, string message) : base(message)
    {
      _actualVersion = actualVersion;
      _minVersion = minVersion;
    }

    public Version ActualVersion { get => _actualVersion; }
    public Version MinVersion { get => _minVersion; }
  }
}
