﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using Kieselstein.Models;
using KieselsteinErp.Terminal.Models;

namespace KieselsteinErp.Terminal.Messages
{
	public class BookMaterialToLotMessage : ValueChangedMessage<BookMaterialToLotMessageModel>
	{
		public BookMaterialToLotMessage(ProductionEntry productionEntry, ProductionTargetMaterialEntryList materialEntryList, string batchnr) : base(new BookMaterialToLotMessageModel(productionEntry, materialEntryList, batchnr)) { }
	}
}
