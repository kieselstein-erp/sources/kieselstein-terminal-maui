﻿using Com.Datalogic.Decode;
using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
  public class ZebraDecoderMessage : ValueChangedMessage<string>
  {
    private readonly string _barcodeType;
    public ZebraDecoderMessage(string value, string barcodeType) : base(value)
    {
      _barcodeType = barcodeType;
    }

    public string BarcodeType { get => _barcodeType; }
  }
}
