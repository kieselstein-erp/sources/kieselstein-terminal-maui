﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
  public class StockPlaceMessage : ValueChangedMessage<string>
	{
		public StockPlaceMessage(string value) : base(value) { }
	}
}
