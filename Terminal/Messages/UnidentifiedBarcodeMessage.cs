﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
  public class UnidentifiedBarcodeMessage : ValueChangedMessage<string>
	{
		public UnidentifiedBarcodeMessage(string barcode) : base(barcode) { }
	}
}
