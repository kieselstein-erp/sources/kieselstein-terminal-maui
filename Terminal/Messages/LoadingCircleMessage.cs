﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
  public class LoadingCircleMessage : ValueChangedMessage<bool>
  {
    public LoadingCircleMessage(bool value) : base(value) { }
  }
}
