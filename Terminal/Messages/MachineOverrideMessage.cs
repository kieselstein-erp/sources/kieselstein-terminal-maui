﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
  public class MachineOverrideMessage : ValueChangedMessage<int>
  {
    public MachineOverrideMessage(int machineId) : base(machineId) { }
  }
}
