﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using ZXing;

namespace KieselsteinErp.Terminal.Messages
{
  public class CameraBarcodeMessage : ValueChangedMessage<string>
  {
    private readonly BarcodeFormat _barcodeFormat;

    public CameraBarcodeMessage(string value, BarcodeFormat barcodeFormat) : base(value)
    {
      _barcodeFormat = barcodeFormat;
    }

    public BarcodeFormat BarcodeFormat { get => _barcodeFormat; }

  }
}
