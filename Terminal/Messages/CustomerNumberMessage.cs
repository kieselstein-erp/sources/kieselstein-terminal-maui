﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
  public class CustomerNumberMessage : ValueChangedMessage<string>
  {
    public CustomerNumberMessage(string value) : base(value) { }
  }
}
