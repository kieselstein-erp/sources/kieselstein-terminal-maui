﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Messages
{
  public class OrderMessage(string value, OrderEntry orderEntry) : ValueChangedMessage<string>(value)
  {
    private readonly OrderEntry _orderEntry = orderEntry;

    public OrderEntry OrderEntry { get => _orderEntry; }
  }
}
