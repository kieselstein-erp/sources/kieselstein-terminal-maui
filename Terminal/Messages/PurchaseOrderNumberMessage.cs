﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
  public class PurchaseOrderNumberMessage : ValueChangedMessage<string>
  {
    public PurchaseOrderNumberMessage(string value) : base(value) { }
  }
}
