﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
  public class DeliveryNoteMessage : ValueChangedMessage<string>
  {
    public DeliveryNoteMessage(string value) : base(value) { }
  }
}
