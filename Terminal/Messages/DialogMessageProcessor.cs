﻿using KieselsteinErp.Terminal.Services;
using KieselsteinErp.Terminal.Services.Audio;
using KieselsteinErp.Terminal.Services.UiState;
using KieselsteinErp.Terminal.Util;
using static KieselsteinErp.Terminal.Resources.Localization.AppResources;

namespace KieselsteinErp.Terminal.Messages
{
    /// <summary>
    /// Takes messages from the message service and displays (some of) them as dialogs. It uses a timer to query the
    /// queue of messages, and outputs all of them.
    /// </summary>
    public class DialogMessageProcessor
  {
    private readonly IUiReadyService _uiReadyService;
    private readonly ITranslator _translator;

    // ReSharper disable once NotAccessedField.Local
    // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable 
    private readonly Thread _processingThread;

    private readonly QueuedMessageService _messageService;
    private readonly IAudioService _audioService;

    public DialogMessageProcessor(IUiReadyService uiReadyService, QueuedMessageService messageService, ITranslator translator, IAudioService audioService)
    {
      _uiReadyService = uiReadyService;
      _translator = translator;
      _messageService = Objects.ArgNotNull(messageService, nameof(messageService));
      _audioService = audioService;

      _processingThread = new Thread(DispatchAllMessages)
      {
        IsBackground = true
      };
      _processingThread.Start();
    }

    public void DispatchAllMessages()
    {
      _uiReadyService.AwaitReady();
      var mainPage = Application.Current?.MainPage;
      if (mainPage == null)
      {
        throw new Exception("No main page!");
      }

      foreach (var message in _messageService.MessageQueue.GetConsumingEnumerable())
      {
        var type = _translator.Translated(message.Type);
        _audioService.Play(message.Type);
        mainPage.Dispatcher.Dispatch(() => mainPage.DisplayAlert(type, message.Text, Ok));
      }

    }
  }
}
