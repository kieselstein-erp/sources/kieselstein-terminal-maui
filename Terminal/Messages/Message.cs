﻿namespace KieselsteinErp.Terminal.Messages
{
  public enum MessageType
    {
        Info,
        Warning,
        Error
    }

    /// <summary>
    /// a message send from terminal services
    /// </summary>
    public record Message
    {
        public MessageType Type { get; }

        public string Text { get; }

        public Message(string text) : this(text, MessageType.Info) { }

        public Message(string text, MessageType type)
        {
            ArgumentNullException.ThrowIfNull(text, nameof(text));
            ArgumentNullException.ThrowIfNull(type, nameof(type));

            Type = type;
            Text = text;
        }

    }
}
