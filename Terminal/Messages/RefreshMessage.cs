﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
  public class RefreshMessage : ValueChangedMessage<bool>
  {
    public RefreshMessage(bool value) : base(value) { }
  }
}
