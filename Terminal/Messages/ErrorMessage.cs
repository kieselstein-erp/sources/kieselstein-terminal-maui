﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace KieselsteinErp.Terminal.Messages
{
    public class ErrorMessage : ValueChangedMessage<string>
    {
        public ErrorMessage(string value) : base(value) { }
    }
}
