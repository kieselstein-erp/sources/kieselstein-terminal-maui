﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Messages
{
	public class SaldoMessage : ValueChangedMessage<TimeBalanceEntry>
	{
		public SaldoMessage(TimeBalanceEntry value) : base(value) { }
	}
}
