﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using KieselsteinErp.Terminal.Models;
using KieselsteinErp.Terminal.Models.ScanLabels;

namespace KieselsteinErp.Terminal.Messages
{
  public class ItemVDAMessage : ValueChangedMessage<ItemVDAMessageModel>
  {

    public ItemVDAMessage(IScanLabel scanLabel) : base(new ItemVDAMessageModel(scanLabel)) { }

  }
}
