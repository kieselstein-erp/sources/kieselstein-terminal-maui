﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using KieselsteinErp.Terminal.Models;

namespace KieselsteinErp.Terminal.Messages
{
  public class ChangeAmountMessage : ValueChangedMessage<LotStringAmount>
	{
		public ChangeAmountMessage(decimal amount, string lotString) : base(new LotStringAmount(amount, lotString)) { }
	}
}
