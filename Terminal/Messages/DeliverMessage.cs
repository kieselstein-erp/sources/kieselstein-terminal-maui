﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using Kieselstein.Models;

namespace KieselsteinErp.Terminal.Messages
{
  public class DeliverMessage : ValueChangedMessage<string>
  {
    private readonly string _preferedType;
    private readonly ItemV1Entry _itemV1Entry;
    private readonly bool _noInidentify;

    public DeliverMessage(string value, string preferedType, ItemV1Entry itemV1Entry, bool noUnidentify) : base(value) 
    {
      _preferedType = preferedType;
      _itemV1Entry = itemV1Entry;
      _noInidentify = noUnidentify;
    }

    public string PreferedType { get => _preferedType; }
    public ItemV1Entry ItemV1Entry { get => _itemV1Entry; }
    public bool NoUnidentify { get => _noInidentify;}
  }
}
