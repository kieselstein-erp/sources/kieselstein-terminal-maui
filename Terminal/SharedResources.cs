﻿namespace KieselsteinErp.Terminal
{
    static class SharedResources
    {
        public static readonly Color EntryTextColor = Colors.AntiqueWhite;
        public static readonly Color LabelTextColor = Colors.DarkOliveGreen;
        public static readonly Brush ButtonBackground = Brush.Brown;
    }
}
