# This scripts expects a file with a gitlab api token in the following location.
# At token can be retrieved here: https://gitlab.com/-/profile/personal_access_tokens
$tokenFile = "$HOME\.gitlab-release-token"
if (! (Test-Path -Path $tokenFile -PathType Leaf)) {
    throw "Gitlab token file does not exist (expected at " + $tokenFile + ")"
}
$token = Get-Content -Path $tokenFile -TotalCount 1

# This is the project id of the kieselstein-terminal-maui project (It is visible just below the project title on the gitlab repo page)
$projectId = "45170536"
$releaseUri = "https://gitlab.com/api/v4/projects/$projectId/releases"

# Get list of  Releases
Invoke-RestMethod -Method Get -Headers @{ 'PRIVATE-TOKEN'="$token" } -Uri $releaseUri
