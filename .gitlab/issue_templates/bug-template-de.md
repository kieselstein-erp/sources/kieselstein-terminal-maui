## Zusammenfassung
(Fasse den aufgetretenen Fehler kurz zusammen)

## Schritte zum Reproduzieren
Bitte eine genaue Beschreibung wie das Problem reproduziert werden kann. Wir benötigen dafür auch die Info, mit welchen Daten dies möglich ist.

Leider: Wenn der Fehler mit deiner Beschreibung nicht reproduziert werden kann, können wir uns auch nicht darum kümmern.

## Modul, Maske, Terminal, API-Aufruf, Bericht
(In welchem Modul, Maske, Terminal, API-Aufruf bzw. Bericht tritt der Fehler auf)

## Wie ist das aktuelle Fehlerverhalten?
(Was tatsächlich passiert)

## Was ist das erwartete richtige Verhalten?
(Was hast du stattdessen erwartet)

## Relevante Log-Dateien und/oder Screenshots
(Füge alle relevaten Log-Dateien hier ein - verwende bitte code-Blöcke (```) um die Ausgabe zu formatieren, Logeinträge und Programmcode sind sonst sehr mühsam zu lesen..)

## Mögliche Korrekturen
(Wenn möglich freuen wir uns über einen Link auf die Codezeile, die möglicherweise für das Problem verantwortlich ist.)

/label ~"type::Bug" ~"status::0 - open" ~"priority::normal" 
