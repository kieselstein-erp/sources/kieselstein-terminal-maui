# Kieselstein Terminal

## Description
Kieselstein Terminal is a time tracking application for [Kieselstein ERP](https://gitlab.com/kieselstein-erp/sources/kieselstein) OpenSource ERP system.


## Installation

### Windows

- Download latest release from [releases](https://gitlab.com/kieselstein-erp/sources/kieselstein-terminal-maui/-/releases)
- Unpack downloaded file to a folder
![content](content-terminal-release.png "unzipped content of release")
- run script 'install.ps1' from folder 'AppPackages\Terminal_x.x.x.x_Test' with powershell and follow the instructions on screen

### Please note:

The code certificate is currently only self-signed, so the installation may require additional measures to complete the setup.
- enable developer mode
![developer mode](developer-mode.png "enable developer mode")
- set execution policy for powershell

    open adminstrative powershell, enter the following command and confirm with Y:

    *Set-ExecutionPolicy -ExecutionPolicy RemoteSigned*

Previous versions must be uninstalled manually via Settings - Apps

The powershell script 'install.ps1' must not be run in an administrative powershell.

Windows 10:
When using Terminal in FullScreen mode make sure that Setting "When i'm using an app in full-screen mode" is switched to "OFF". You will find this setting at "System" - "Notifications & actions" - "Focus assist" - "Automatic rules".

(On german Windows 10: "System" - "Benachrichtungsassistent" - "Automatische Regeln": Wenn ich eine App im Vollbildmodus verwende" auf "AUS" setzen.)
(On german Windows 11: "System" - "Benachrichtigungen" - "Bitte nicht stören automatisch aktivieren": Bei Verwendung eine App im Vollbildmodus ... abschalten.)

Windows 10/11:
FullScreen mode: disable swiping from edges to open windows action center
    HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PolicyManager\default\LockDown\AllowEdgeSwipe
    change "Value" from 1 to 0 and reboot

### Android
- ToDo

## Usage

Please note:
- User Terminal is used for logon to the REST-API. Please set the appropriate rights in Kieselstein ERP.

For more details [see](https://docs.kieselstein-erp.org/docs/installation/04_ze_terminal/) and also [see](https://docs.kieselstein-erp.org/docs/installation/05_kes_app/)

## License

There is no agreed license on this project yet. The copyright is with the authors as documented [in the copyright notice](./COPYRIGHT.md).


